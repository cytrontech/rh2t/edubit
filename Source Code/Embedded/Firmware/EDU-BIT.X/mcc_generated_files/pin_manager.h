/**
  @Generated Pin Manager Header File

  @Company:
    Microchip Technology Inc.

  @File Name:
    pin_manager.h

  @Summary:
    This is the Pin Manager file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  @Description
    This header file provides APIs for driver for .
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.78
        Device            :  PIC16F1937
        Driver Version    :  2.11
    The generated drivers are tested against the following:
        Compiler          :  XC8 2.05 and above
        MPLAB 	          :  MPLAB X 5.20	
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

#ifndef PIN_MANAGER_H
#define PIN_MANAGER_H

/**
  Section: Included Files
*/

#include <xc.h>

#define INPUT   1
#define OUTPUT  0

#define HIGH    1
#define LOW     0

#define ANALOG      1
#define DIGITAL     0

#define PULL_UP_ENABLED      1
#define PULL_UP_DISABLED     0

// get/set AN_P0 aliases
#define AN_P0_TRIS                 TRISAbits.TRISA0
#define AN_P0_LAT                  LATAbits.LATA0
#define AN_P0_PORT                 PORTAbits.RA0
#define AN_P0_ANS                  ANSELAbits.ANSA0
#define AN_P0_SetHigh()            do { LATAbits.LATA0 = 1; } while(0)
#define AN_P0_SetLow()             do { LATAbits.LATA0 = 0; } while(0)
#define AN_P0_Toggle()             do { LATAbits.LATA0 = ~LATAbits.LATA0; } while(0)
#define AN_P0_GetValue()           PORTAbits.RA0
#define AN_P0_SetDigitalInput()    do { TRISAbits.TRISA0 = 1; } while(0)
#define AN_P0_SetDigitalOutput()   do { TRISAbits.TRISA0 = 0; } while(0)
#define AN_P0_SetAnalogMode()      do { ANSELAbits.ANSA0 = 1; } while(0)
#define AN_P0_SetDigitalMode()     do { ANSELAbits.ANSA0 = 0; } while(0)

// get/set AN_P1 aliases
#define AN_P1_TRIS                 TRISAbits.TRISA1
#define AN_P1_LAT                  LATAbits.LATA1
#define AN_P1_PORT                 PORTAbits.RA1
#define AN_P1_ANS                  ANSELAbits.ANSA1
#define AN_P1_SetHigh()            do { LATAbits.LATA1 = 1; } while(0)
#define AN_P1_SetLow()             do { LATAbits.LATA1 = 0; } while(0)
#define AN_P1_Toggle()             do { LATAbits.LATA1 = ~LATAbits.LATA1; } while(0)
#define AN_P1_GetValue()           PORTAbits.RA1
#define AN_P1_SetDigitalInput()    do { TRISAbits.TRISA1 = 1; } while(0)
#define AN_P1_SetDigitalOutput()   do { TRISAbits.TRISA1 = 0; } while(0)
#define AN_P1_SetAnalogMode()      do { ANSELAbits.ANSA1 = 1; } while(0)
#define AN_P1_SetDigitalMode()     do { ANSELAbits.ANSA1 = 0; } while(0)

// get/set AN_P2 aliases
#define AN_P2_TRIS                 TRISAbits.TRISA2
#define AN_P2_LAT                  LATAbits.LATA2
#define AN_P2_PORT                 PORTAbits.RA2
#define AN_P2_ANS                  ANSELAbits.ANSA2
#define AN_P2_SetHigh()            do { LATAbits.LATA2 = 1; } while(0)
#define AN_P2_SetLow()             do { LATAbits.LATA2 = 0; } while(0)
#define AN_P2_Toggle()             do { LATAbits.LATA2 = ~LATAbits.LATA2; } while(0)
#define AN_P2_GetValue()           PORTAbits.RA2
#define AN_P2_SetDigitalInput()    do { TRISAbits.TRISA2 = 1; } while(0)
#define AN_P2_SetDigitalOutput()   do { TRISAbits.TRISA2 = 0; } while(0)
#define AN_P2_SetAnalogMode()      do { ANSELAbits.ANSA2 = 1; } while(0)
#define AN_P2_SetDigitalMode()     do { ANSELAbits.ANSA2 = 0; } while(0)

// get/set AN_VBAT aliases
#define AN_VBAT_TRIS                 TRISAbits.TRISA3
#define AN_VBAT_LAT                  LATAbits.LATA3
#define AN_VBAT_PORT                 PORTAbits.RA3
#define AN_VBAT_ANS                  ANSELAbits.ANSA3
#define AN_VBAT_SetHigh()            do { LATAbits.LATA3 = 1; } while(0)
#define AN_VBAT_SetLow()             do { LATAbits.LATA3 = 0; } while(0)
#define AN_VBAT_Toggle()             do { LATAbits.LATA3 = ~LATAbits.LATA3; } while(0)
#define AN_VBAT_GetValue()           PORTAbits.RA3
#define AN_VBAT_SetDigitalInput()    do { TRISAbits.TRISA3 = 1; } while(0)
#define AN_VBAT_SetDigitalOutput()   do { TRISAbits.TRISA3 = 0; } while(0)
#define AN_VBAT_SetAnalogMode()      do { ANSELAbits.ANSA3 = 1; } while(0)
#define AN_VBAT_SetDigitalMode()     do { ANSELAbits.ANSA3 = 0; } while(0)

// get/set P2_L5 aliases
#define P2_L5_TRIS                 TRISAbits.TRISA4
#define P2_L5_LAT                  LATAbits.LATA4
#define P2_L5_PORT                 PORTAbits.RA4
#define P2_L5_ANS                  ANSELAbits.ANSA4
#define P2_L5_SetHigh()            do { LATAbits.LATA4 = 1; } while(0)
#define P2_L5_SetLow()             do { LATAbits.LATA4 = 0; } while(0)
#define P2_L5_Toggle()             do { LATAbits.LATA4 = ~LATAbits.LATA4; } while(0)
#define P2_L5_GetValue()           PORTAbits.RA4
#define P2_L5_SetDigitalInput()    do { TRISAbits.TRISA4 = 1; } while(0)
#define P2_L5_SetDigitalOutput()   do { TRISAbits.TRISA4 = 0; } while(0)
#define P2_L5_SetAnalogMode()      do { ANSELAbits.ANSA4 = 1; } while(0)
#define P2_L5_SetDigitalMode()     do { ANSELAbits.ANSA4 = 0; } while(0)

// get/set P2_L6 aliases
#define P2_L6_TRIS                 TRISAbits.TRISA5
#define P2_L6_LAT                  LATAbits.LATA5
#define P2_L6_PORT                 PORTAbits.RA5
#define P2_L6_ANS                  ANSELAbits.ANSA5
#define P2_L6_SetHigh()            do { LATAbits.LATA5 = 1; } while(0)
#define P2_L6_SetLow()             do { LATAbits.LATA5 = 0; } while(0)
#define P2_L6_Toggle()             do { LATAbits.LATA5 = ~LATAbits.LATA5; } while(0)
#define P2_L6_GetValue()           PORTAbits.RA5
#define P2_L6_SetDigitalInput()    do { TRISAbits.TRISA5 = 1; } while(0)
#define P2_L6_SetDigitalOutput()   do { TRISAbits.TRISA5 = 0; } while(0)
#define P2_L6_SetAnalogMode()      do { ANSELAbits.ANSA5 = 1; } while(0)
#define P2_L6_SetDigitalMode()     do { ANSELAbits.ANSA5 = 0; } while(0)

// get/set SRV2 aliases
#define SRV2_TRIS                 TRISAbits.TRISA6
#define SRV2_LAT                  LATAbits.LATA6
#define SRV2_PORT                 PORTAbits.RA6
#define SRV2_SetHigh()            do { LATAbits.LATA6 = 1; } while(0)
#define SRV2_SetLow()             do { LATAbits.LATA6 = 0; } while(0)
#define SRV2_Toggle()             do { LATAbits.LATA6 = ~LATAbits.LATA6; } while(0)
#define SRV2_GetValue()           PORTAbits.RA6
#define SRV2_SetDigitalInput()    do { TRISAbits.TRISA6 = 1; } while(0)
#define SRV2_SetDigitalOutput()   do { TRISAbits.TRISA6 = 0; } while(0)

// get/set SRV1 aliases
#define SRV1_TRIS                 TRISAbits.TRISA7
#define SRV1_LAT                  LATAbits.LATA7
#define SRV1_PORT                 PORTAbits.RA7
#define SRV1_SetHigh()            do { LATAbits.LATA7 = 1; } while(0)
#define SRV1_SetLow()             do { LATAbits.LATA7 = 0; } while(0)
#define SRV1_Toggle()             do { LATAbits.LATA7 = ~LATAbits.LATA7; } while(0)
#define SRV1_GetValue()           PORTAbits.RA7
#define SRV1_SetDigitalInput()    do { TRISAbits.TRISA7 = 1; } while(0)
#define SRV1_SetDigitalOutput()   do { TRISAbits.TRISA7 = 0; } while(0)

// get/set P0_L0 aliases
#define P0_L0_TRIS                 TRISBbits.TRISB0
#define P0_L0_LAT                  LATBbits.LATB0
#define P0_L0_PORT                 PORTBbits.RB0
#define P0_L0_WPU                  WPUBbits.WPUB0
#define P0_L0_ANS                  ANSELBbits.ANSB0
#define P0_L0_SetHigh()            do { LATBbits.LATB0 = 1; } while(0)
#define P0_L0_SetLow()             do { LATBbits.LATB0 = 0; } while(0)
#define P0_L0_Toggle()             do { LATBbits.LATB0 = ~LATBbits.LATB0; } while(0)
#define P0_L0_GetValue()           PORTBbits.RB0
#define P0_L0_SetDigitalInput()    do { TRISBbits.TRISB0 = 1; } while(0)
#define P0_L0_SetDigitalOutput()   do { TRISBbits.TRISB0 = 0; } while(0)
#define P0_L0_SetPullup()          do { WPUBbits.WPUB0 = 1; } while(0)
#define P0_L0_ResetPullup()        do { WPUBbits.WPUB0 = 0; } while(0)
#define P0_L0_SetAnalogMode()      do { ANSELBbits.ANSB0 = 1; } while(0)
#define P0_L0_SetDigitalMode()     do { ANSELBbits.ANSB0 = 0; } while(0)

// get/set P0_L1 aliases
#define P0_L1_TRIS                 TRISBbits.TRISB1
#define P0_L1_LAT                  LATBbits.LATB1
#define P0_L1_PORT                 PORTBbits.RB1
#define P0_L1_WPU                  WPUBbits.WPUB1
#define P0_L1_ANS                  ANSELBbits.ANSB1
#define P0_L1_SetHigh()            do { LATBbits.LATB1 = 1; } while(0)
#define P0_L1_SetLow()             do { LATBbits.LATB1 = 0; } while(0)
#define P0_L1_Toggle()             do { LATBbits.LATB1 = ~LATBbits.LATB1; } while(0)
#define P0_L1_GetValue()           PORTBbits.RB1
#define P0_L1_SetDigitalInput()    do { TRISBbits.TRISB1 = 1; } while(0)
#define P0_L1_SetDigitalOutput()   do { TRISBbits.TRISB1 = 0; } while(0)
#define P0_L1_SetPullup()          do { WPUBbits.WPUB1 = 1; } while(0)
#define P0_L1_ResetPullup()        do { WPUBbits.WPUB1 = 0; } while(0)
#define P0_L1_SetAnalogMode()      do { ANSELBbits.ANSB1 = 1; } while(0)
#define P0_L1_SetDigitalMode()     do { ANSELBbits.ANSB1 = 0; } while(0)

// get/set P0_L2 aliases
#define P0_L2_TRIS                 TRISBbits.TRISB2
#define P0_L2_LAT                  LATBbits.LATB2
#define P0_L2_PORT                 PORTBbits.RB2
#define P0_L2_WPU                  WPUBbits.WPUB2
#define P0_L2_ANS                  ANSELBbits.ANSB2
#define P0_L2_SetHigh()            do { LATBbits.LATB2 = 1; } while(0)
#define P0_L2_SetLow()             do { LATBbits.LATB2 = 0; } while(0)
#define P0_L2_Toggle()             do { LATBbits.LATB2 = ~LATBbits.LATB2; } while(0)
#define P0_L2_GetValue()           PORTBbits.RB2
#define P0_L2_SetDigitalInput()    do { TRISBbits.TRISB2 = 1; } while(0)
#define P0_L2_SetDigitalOutput()   do { TRISBbits.TRISB2 = 0; } while(0)
#define P0_L2_SetPullup()          do { WPUBbits.WPUB2 = 1; } while(0)
#define P0_L2_ResetPullup()        do { WPUBbits.WPUB2 = 0; } while(0)
#define P0_L2_SetAnalogMode()      do { ANSELBbits.ANSB2 = 1; } while(0)
#define P0_L2_SetDigitalMode()     do { ANSELBbits.ANSB2 = 0; } while(0)

// get/set P0_L3 aliases
#define P0_L3_TRIS                 TRISBbits.TRISB3
#define P0_L3_LAT                  LATBbits.LATB3
#define P0_L3_PORT                 PORTBbits.RB3
#define P0_L3_WPU                  WPUBbits.WPUB3
#define P0_L3_ANS                  ANSELBbits.ANSB3
#define P0_L3_SetHigh()            do { LATBbits.LATB3 = 1; } while(0)
#define P0_L3_SetLow()             do { LATBbits.LATB3 = 0; } while(0)
#define P0_L3_Toggle()             do { LATBbits.LATB3 = ~LATBbits.LATB3; } while(0)
#define P0_L3_GetValue()           PORTBbits.RB3
#define P0_L3_SetDigitalInput()    do { TRISBbits.TRISB3 = 1; } while(0)
#define P0_L3_SetDigitalOutput()   do { TRISBbits.TRISB3 = 0; } while(0)
#define P0_L3_SetPullup()          do { WPUBbits.WPUB3 = 1; } while(0)
#define P0_L3_ResetPullup()        do { WPUBbits.WPUB3 = 0; } while(0)
#define P0_L3_SetAnalogMode()      do { ANSELBbits.ANSB3 = 1; } while(0)
#define P0_L3_SetDigitalMode()     do { ANSELBbits.ANSB3 = 0; } while(0)

// get/set P0_L4 aliases
#define P0_L4_TRIS                 TRISBbits.TRISB4
#define P0_L4_LAT                  LATBbits.LATB4
#define P0_L4_PORT                 PORTBbits.RB4
#define P0_L4_WPU                  WPUBbits.WPUB4
#define P0_L4_ANS                  ANSELBbits.ANSB4
#define P0_L4_SetHigh()            do { LATBbits.LATB4 = 1; } while(0)
#define P0_L4_SetLow()             do { LATBbits.LATB4 = 0; } while(0)
#define P0_L4_Toggle()             do { LATBbits.LATB4 = ~LATBbits.LATB4; } while(0)
#define P0_L4_GetValue()           PORTBbits.RB4
#define P0_L4_SetDigitalInput()    do { TRISBbits.TRISB4 = 1; } while(0)
#define P0_L4_SetDigitalOutput()   do { TRISBbits.TRISB4 = 0; } while(0)
#define P0_L4_SetPullup()          do { WPUBbits.WPUB4 = 1; } while(0)
#define P0_L4_ResetPullup()        do { WPUBbits.WPUB4 = 0; } while(0)
#define P0_L4_SetAnalogMode()      do { ANSELBbits.ANSB4 = 1; } while(0)
#define P0_L4_SetDigitalMode()     do { ANSELBbits.ANSB4 = 0; } while(0)

// get/set P0_L5 aliases
#define P0_L5_TRIS                 TRISBbits.TRISB5
#define P0_L5_LAT                  LATBbits.LATB5
#define P0_L5_PORT                 PORTBbits.RB5
#define P0_L5_WPU                  WPUBbits.WPUB5
#define P0_L5_ANS                  ANSELBbits.ANSB5
#define P0_L5_SetHigh()            do { LATBbits.LATB5 = 1; } while(0)
#define P0_L5_SetLow()             do { LATBbits.LATB5 = 0; } while(0)
#define P0_L5_Toggle()             do { LATBbits.LATB5 = ~LATBbits.LATB5; } while(0)
#define P0_L5_GetValue()           PORTBbits.RB5
#define P0_L5_SetDigitalInput()    do { TRISBbits.TRISB5 = 1; } while(0)
#define P0_L5_SetDigitalOutput()   do { TRISBbits.TRISB5 = 0; } while(0)
#define P0_L5_SetPullup()          do { WPUBbits.WPUB5 = 1; } while(0)
#define P0_L5_ResetPullup()        do { WPUBbits.WPUB5 = 0; } while(0)
#define P0_L5_SetAnalogMode()      do { ANSELBbits.ANSB5 = 1; } while(0)
#define P0_L5_SetDigitalMode()     do { ANSELBbits.ANSB5 = 0; } while(0)

// get/set P0_L6 aliases
#define P0_L6_TRIS                 TRISBbits.TRISB6
#define P0_L6_LAT                  LATBbits.LATB6
#define P0_L6_PORT                 PORTBbits.RB6
#define P0_L6_WPU                  WPUBbits.WPUB6
#define P0_L6_SetHigh()            do { LATBbits.LATB6 = 1; } while(0)
#define P0_L6_SetLow()             do { LATBbits.LATB6 = 0; } while(0)
#define P0_L6_Toggle()             do { LATBbits.LATB6 = ~LATBbits.LATB6; } while(0)
#define P0_L6_GetValue()           PORTBbits.RB6
#define P0_L6_SetDigitalInput()    do { TRISBbits.TRISB6 = 1; } while(0)
#define P0_L6_SetDigitalOutput()   do { TRISBbits.TRISB6 = 0; } while(0)
#define P0_L6_SetPullup()          do { WPUBbits.WPUB6 = 1; } while(0)
#define P0_L6_ResetPullup()        do { WPUBbits.WPUB6 = 0; } while(0)

// get/set P2_L4 aliases
#define P2_L4_TRIS                 TRISBbits.TRISB7
#define P2_L4_LAT                  LATBbits.LATB7
#define P2_L4_PORT                 PORTBbits.RB7
#define P2_L4_WPU                  WPUBbits.WPUB7
#define P2_L4_SetHigh()            do { LATBbits.LATB7 = 1; } while(0)
#define P2_L4_SetLow()             do { LATBbits.LATB7 = 0; } while(0)
#define P2_L4_Toggle()             do { LATBbits.LATB7 = ~LATBbits.LATB7; } while(0)
#define P2_L4_GetValue()           PORTBbits.RB7
#define P2_L4_SetDigitalInput()    do { TRISBbits.TRISB7 = 1; } while(0)
#define P2_L4_SetDigitalOutput()   do { TRISBbits.TRISB7 = 0; } while(0)
#define P2_L4_SetPullup()          do { WPUBbits.WPUB7 = 1; } while(0)
#define P2_L4_ResetPullup()        do { WPUBbits.WPUB7 = 0; } while(0)

// get/set SRV3 aliases
#define SRV3_TRIS                 TRISCbits.TRISC0
#define SRV3_LAT                  LATCbits.LATC0
#define SRV3_PORT                 PORTCbits.RC0
#define SRV3_SetHigh()            do { LATCbits.LATC0 = 1; } while(0)
#define SRV3_SetLow()             do { LATCbits.LATC0 = 0; } while(0)
#define SRV3_Toggle()             do { LATCbits.LATC0 = ~LATCbits.LATC0; } while(0)
#define SRV3_GetValue()           PORTCbits.RC0
#define SRV3_SetDigitalInput()    do { TRISCbits.TRISC0 = 1; } while(0)
#define SRV3_SetDigitalOutput()   do { TRISCbits.TRISC0 = 0; } while(0)

// get/set RC1 procedures
#define RC1_SetHigh()            do { LATCbits.LATC1 = 1; } while(0)
#define RC1_SetLow()             do { LATCbits.LATC1 = 0; } while(0)
#define RC1_Toggle()             do { LATCbits.LATC1 = ~LATCbits.LATC1; } while(0)
#define RC1_GetValue()              PORTCbits.RC1
#define RC1_SetDigitalInput()    do { TRISCbits.TRISC1 = 1; } while(0)
#define RC1_SetDigitalOutput()   do { TRISCbits.TRISC1 = 0; } while(0)

// get/set RC2 procedures
#define RC2_SetHigh()            do { LATCbits.LATC2 = 1; } while(0)
#define RC2_SetLow()             do { LATCbits.LATC2 = 0; } while(0)
#define RC2_Toggle()             do { LATCbits.LATC2 = ~LATCbits.LATC2; } while(0)
#define RC2_GetValue()              PORTCbits.RC2
#define RC2_SetDigitalInput()    do { TRISCbits.TRISC2 = 1; } while(0)
#define RC2_SetDigitalOutput()   do { TRISCbits.TRISC2 = 0; } while(0)

// get/set RC3 procedures
#define RC3_SetHigh()            do { LATCbits.LATC3 = 1; } while(0)
#define RC3_SetLow()             do { LATCbits.LATC3 = 0; } while(0)
#define RC3_Toggle()             do { LATCbits.LATC3 = ~LATCbits.LATC3; } while(0)
#define RC3_GetValue()              PORTCbits.RC3
#define RC3_SetDigitalInput()    do { TRISCbits.TRISC3 = 1; } while(0)
#define RC3_SetDigitalOutput()   do { TRISCbits.TRISC3 = 0; } while(0)

// get/set RC4 procedures
#define RC4_SetHigh()            do { LATCbits.LATC4 = 1; } while(0)
#define RC4_SetLow()             do { LATCbits.LATC4 = 0; } while(0)
#define RC4_Toggle()             do { LATCbits.LATC4 = ~LATCbits.LATC4; } while(0)
#define RC4_GetValue()              PORTCbits.RC4
#define RC4_SetDigitalInput()    do { TRISCbits.TRISC4 = 1; } while(0)
#define RC4_SetDigitalOutput()   do { TRISCbits.TRISC4 = 0; } while(0)

// get/set P2_L0 aliases
#define P2_L0_TRIS                 TRISCbits.TRISC5
#define P2_L0_LAT                  LATCbits.LATC5
#define P2_L0_PORT                 PORTCbits.RC5
#define P2_L0_SetHigh()            do { LATCbits.LATC5 = 1; } while(0)
#define P2_L0_SetLow()             do { LATCbits.LATC5 = 0; } while(0)
#define P2_L0_Toggle()             do { LATCbits.LATC5 = ~LATCbits.LATC5; } while(0)
#define P2_L0_GetValue()           PORTCbits.RC5
#define P2_L0_SetDigitalInput()    do { TRISCbits.TRISC5 = 1; } while(0)
#define P2_L0_SetDigitalOutput()   do { TRISCbits.TRISC5 = 0; } while(0)

// get/set P2_L1 aliases
#define P2_L1_TRIS                 TRISCbits.TRISC6
#define P2_L1_LAT                  LATCbits.LATC6
#define P2_L1_PORT                 PORTCbits.RC6
#define P2_L1_SetHigh()            do { LATCbits.LATC6 = 1; } while(0)
#define P2_L1_SetLow()             do { LATCbits.LATC6 = 0; } while(0)
#define P2_L1_Toggle()             do { LATCbits.LATC6 = ~LATCbits.LATC6; } while(0)
#define P2_L1_GetValue()           PORTCbits.RC6
#define P2_L1_SetDigitalInput()    do { TRISCbits.TRISC6 = 1; } while(0)
#define P2_L1_SetDigitalOutput()   do { TRISCbits.TRISC6 = 0; } while(0)

// get/set P2_L2 aliases
#define P2_L2_TRIS                 TRISCbits.TRISC7
#define P2_L2_LAT                  LATCbits.LATC7
#define P2_L2_PORT                 PORTCbits.RC7
#define P2_L2_SetHigh()            do { LATCbits.LATC7 = 1; } while(0)
#define P2_L2_SetLow()             do { LATCbits.LATC7 = 0; } while(0)
#define P2_L2_Toggle()             do { LATCbits.LATC7 = ~LATCbits.LATC7; } while(0)
#define P2_L2_GetValue()           PORTCbits.RC7
#define P2_L2_SetDigitalInput()    do { TRISCbits.TRISC7 = 1; } while(0)
#define P2_L2_SetDigitalOutput()   do { TRISCbits.TRISC7 = 0; } while(0)

// get/set P1_L0 aliases
#define P1_L0_TRIS                 TRISDbits.TRISD0
#define P1_L0_LAT                  LATDbits.LATD0
#define P1_L0_PORT                 PORTDbits.RD0
#define P1_L0_ANS                  ANSELDbits.ANSD0
#define P1_L0_SetHigh()            do { LATDbits.LATD0 = 1; } while(0)
#define P1_L0_SetLow()             do { LATDbits.LATD0 = 0; } while(0)
#define P1_L0_Toggle()             do { LATDbits.LATD0 = ~LATDbits.LATD0; } while(0)
#define P1_L0_GetValue()           PORTDbits.RD0
#define P1_L0_SetDigitalInput()    do { TRISDbits.TRISD0 = 1; } while(0)
#define P1_L0_SetDigitalOutput()   do { TRISDbits.TRISD0 = 0; } while(0)
#define P1_L0_SetAnalogMode()      do { ANSELDbits.ANSD0 = 1; } while(0)
#define P1_L0_SetDigitalMode()     do { ANSELDbits.ANSD0 = 0; } while(0)

// get/set P1_L1 aliases
#define P1_L1_TRIS                 TRISDbits.TRISD1
#define P1_L1_LAT                  LATDbits.LATD1
#define P1_L1_PORT                 PORTDbits.RD1
#define P1_L1_ANS                  ANSELDbits.ANSD1
#define P1_L1_SetHigh()            do { LATDbits.LATD1 = 1; } while(0)
#define P1_L1_SetLow()             do { LATDbits.LATD1 = 0; } while(0)
#define P1_L1_Toggle()             do { LATDbits.LATD1 = ~LATDbits.LATD1; } while(0)
#define P1_L1_GetValue()           PORTDbits.RD1
#define P1_L1_SetDigitalInput()    do { TRISDbits.TRISD1 = 1; } while(0)
#define P1_L1_SetDigitalOutput()   do { TRISDbits.TRISD1 = 0; } while(0)
#define P1_L1_SetAnalogMode()      do { ANSELDbits.ANSD1 = 1; } while(0)
#define P1_L1_SetDigitalMode()     do { ANSELDbits.ANSD1 = 0; } while(0)

// get/set P1_L2 aliases
#define P1_L2_TRIS                 TRISDbits.TRISD2
#define P1_L2_LAT                  LATDbits.LATD2
#define P1_L2_PORT                 PORTDbits.RD2
#define P1_L2_ANS                  ANSELDbits.ANSD2
#define P1_L2_SetHigh()            do { LATDbits.LATD2 = 1; } while(0)
#define P1_L2_SetLow()             do { LATDbits.LATD2 = 0; } while(0)
#define P1_L2_Toggle()             do { LATDbits.LATD2 = ~LATDbits.LATD2; } while(0)
#define P1_L2_GetValue()           PORTDbits.RD2
#define P1_L2_SetDigitalInput()    do { TRISDbits.TRISD2 = 1; } while(0)
#define P1_L2_SetDigitalOutput()   do { TRISDbits.TRISD2 = 0; } while(0)
#define P1_L2_SetAnalogMode()      do { ANSELDbits.ANSD2 = 1; } while(0)
#define P1_L2_SetDigitalMode()     do { ANSELDbits.ANSD2 = 0; } while(0)

// get/set P1_L3 aliases
#define P1_L3_TRIS                 TRISDbits.TRISD3
#define P1_L3_LAT                  LATDbits.LATD3
#define P1_L3_PORT                 PORTDbits.RD3
#define P1_L3_ANS                  ANSELDbits.ANSD3
#define P1_L3_SetHigh()            do { LATDbits.LATD3 = 1; } while(0)
#define P1_L3_SetLow()             do { LATDbits.LATD3 = 0; } while(0)
#define P1_L3_Toggle()             do { LATDbits.LATD3 = ~LATDbits.LATD3; } while(0)
#define P1_L3_GetValue()           PORTDbits.RD3
#define P1_L3_SetDigitalInput()    do { TRISDbits.TRISD3 = 1; } while(0)
#define P1_L3_SetDigitalOutput()   do { TRISDbits.TRISD3 = 0; } while(0)
#define P1_L3_SetAnalogMode()      do { ANSELDbits.ANSD3 = 1; } while(0)
#define P1_L3_SetDigitalMode()     do { ANSELDbits.ANSD3 = 0; } while(0)

// get/set P1_L4 aliases
#define P1_L4_TRIS                 TRISDbits.TRISD4
#define P1_L4_LAT                  LATDbits.LATD4
#define P1_L4_PORT                 PORTDbits.RD4
#define P1_L4_ANS                  ANSELDbits.ANSD4
#define P1_L4_SetHigh()            do { LATDbits.LATD4 = 1; } while(0)
#define P1_L4_SetLow()             do { LATDbits.LATD4 = 0; } while(0)
#define P1_L4_Toggle()             do { LATDbits.LATD4 = ~LATDbits.LATD4; } while(0)
#define P1_L4_GetValue()           PORTDbits.RD4
#define P1_L4_SetDigitalInput()    do { TRISDbits.TRISD4 = 1; } while(0)
#define P1_L4_SetDigitalOutput()   do { TRISDbits.TRISD4 = 0; } while(0)
#define P1_L4_SetAnalogMode()      do { ANSELDbits.ANSD4 = 1; } while(0)
#define P1_L4_SetDigitalMode()     do { ANSELDbits.ANSD4 = 0; } while(0)

// get/set P1_L5 aliases
#define P1_L5_TRIS                 TRISDbits.TRISD5
#define P1_L5_LAT                  LATDbits.LATD5
#define P1_L5_PORT                 PORTDbits.RD5
#define P1_L5_ANS                  ANSELDbits.ANSD5
#define P1_L5_SetHigh()            do { LATDbits.LATD5 = 1; } while(0)
#define P1_L5_SetLow()             do { LATDbits.LATD5 = 0; } while(0)
#define P1_L5_Toggle()             do { LATDbits.LATD5 = ~LATDbits.LATD5; } while(0)
#define P1_L5_GetValue()           PORTDbits.RD5
#define P1_L5_SetDigitalInput()    do { TRISDbits.TRISD5 = 1; } while(0)
#define P1_L5_SetDigitalOutput()   do { TRISDbits.TRISD5 = 0; } while(0)
#define P1_L5_SetAnalogMode()      do { ANSELDbits.ANSD5 = 1; } while(0)
#define P1_L5_SetDigitalMode()     do { ANSELDbits.ANSD5 = 0; } while(0)

// get/set P1_L6 aliases
#define P1_L6_TRIS                 TRISDbits.TRISD6
#define P1_L6_LAT                  LATDbits.LATD6
#define P1_L6_PORT                 PORTDbits.RD6
#define P1_L6_ANS                  ANSELDbits.ANSD6
#define P1_L6_SetHigh()            do { LATDbits.LATD6 = 1; } while(0)
#define P1_L6_SetLow()             do { LATDbits.LATD6 = 0; } while(0)
#define P1_L6_Toggle()             do { LATDbits.LATD6 = ~LATDbits.LATD6; } while(0)
#define P1_L6_GetValue()           PORTDbits.RD6
#define P1_L6_SetDigitalInput()    do { TRISDbits.TRISD6 = 1; } while(0)
#define P1_L6_SetDigitalOutput()   do { TRISDbits.TRISD6 = 0; } while(0)
#define P1_L6_SetAnalogMode()      do { ANSELDbits.ANSD6 = 1; } while(0)
#define P1_L6_SetDigitalMode()     do { ANSELDbits.ANSD6 = 0; } while(0)

// get/set P2_L3 aliases
#define P2_L3_TRIS                 TRISDbits.TRISD7
#define P2_L3_LAT                  LATDbits.LATD7
#define P2_L3_PORT                 PORTDbits.RD7
#define P2_L3_ANS                  ANSELDbits.ANSD7
#define P2_L3_SetHigh()            do { LATDbits.LATD7 = 1; } while(0)
#define P2_L3_SetLow()             do { LATDbits.LATD7 = 0; } while(0)
#define P2_L3_Toggle()             do { LATDbits.LATD7 = ~LATDbits.LATD7; } while(0)
#define P2_L3_GetValue()           PORTDbits.RD7
#define P2_L3_SetDigitalInput()    do { TRISDbits.TRISD7 = 1; } while(0)
#define P2_L3_SetDigitalOutput()   do { TRISDbits.TRISD7 = 0; } while(0)
#define P2_L3_SetAnalogMode()      do { ANSELDbits.ANSD7 = 1; } while(0)
#define P2_L3_SetDigitalMode()     do { ANSELDbits.ANSD7 = 0; } while(0)

// get/set RE0 procedures
#define RE0_SetHigh()            do { LATEbits.LATE0 = 1; } while(0)
#define RE0_SetLow()             do { LATEbits.LATE0 = 0; } while(0)
#define RE0_Toggle()             do { LATEbits.LATE0 = ~LATEbits.LATE0; } while(0)
#define RE0_GetValue()              PORTEbits.RE0
#define RE0_SetDigitalInput()    do { TRISEbits.TRISE0 = 1; } while(0)
#define RE0_SetDigitalOutput()   do { TRISEbits.TRISE0 = 0; } while(0)
#define RE0_SetAnalogMode()         do { ANSELEbits.ANSE0 = 1; } while(0)
#define RE0_SetDigitalMode()        do { ANSELEbits.ANSE0 = 0; } while(0)

// get/set LED_LOWBATT aliases
#define LED_LOWBATT_TRIS                 TRISEbits.TRISE1
#define LED_LOWBATT_LAT                  LATEbits.LATE1
#define LED_LOWBATT_PORT                 PORTEbits.RE1
#define LED_LOWBATT_ANS                  ANSELEbits.ANSE1
#define LED_LOWBATT_SetHigh()            do { LATEbits.LATE1 = 1; } while(0)
#define LED_LOWBATT_SetLow()             do { LATEbits.LATE1 = 0; } while(0)
#define LED_LOWBATT_Toggle()             do { LATEbits.LATE1 = ~LATEbits.LATE1; } while(0)
#define LED_LOWBATT_GetValue()           PORTEbits.RE1
#define LED_LOWBATT_SetDigitalInput()    do { TRISEbits.TRISE1 = 1; } while(0)
#define LED_LOWBATT_SetDigitalOutput()   do { TRISEbits.TRISE1 = 0; } while(0)
#define LED_LOWBATT_SetAnalogMode()      do { ANSELEbits.ANSE1 = 1; } while(0)
#define LED_LOWBATT_SetDigitalMode()     do { ANSELEbits.ANSE1 = 0; } while(0)

// get/set RE2 procedures
#define RE2_SetHigh()            do { LATEbits.LATE2 = 1; } while(0)
#define RE2_SetLow()             do { LATEbits.LATE2 = 0; } while(0)
#define RE2_Toggle()             do { LATEbits.LATE2 = ~LATEbits.LATE2; } while(0)
#define RE2_GetValue()              PORTEbits.RE2
#define RE2_SetDigitalInput()    do { TRISEbits.TRISE2 = 1; } while(0)
#define RE2_SetDigitalOutput()   do { TRISEbits.TRISE2 = 0; } while(0)
#define RE2_SetAnalogMode()         do { ANSELEbits.ANSE2 = 1; } while(0)
#define RE2_SetDigitalMode()        do { ANSELEbits.ANSE2 = 0; } while(0)


/**
   @Param
    none
   @Returns
    none
   @Description
    GPIO and peripheral I/O initialization
   @Example
    PIN_MANAGER_Initialize();
 */
void PIN_MANAGER_Initialize (void);

/**
 * @Param
    none
 * @Returns
    none
 * @Description
    Interrupt on Change Handling routine
 * @Example
    PIN_MANAGER_IOC();
 */
void PIN_MANAGER_IOC(void);



#endif // PIN_MANAGER_H
/**
 End of File
*/