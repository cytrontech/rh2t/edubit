/*******************************************************************************
 * This file provides the functions to control the LEDs.
 *
 * Company: Cytron Technologies Sdn Bhd
 * Website: http://www.cytron.io
 * Email:   support@cytron.io
 *******************************************************************************/

#ifndef LED_H
#define	LED_H

#include "mcc_generated_files/mcc.h"
#include "variables.h"



/*******************************************************************************
 * PUBLIC FUNCTION PROTOTYPES                                                  *
 *******************************************************************************/

void clearAllLeds(void);
void showStartupPattern(void);
void setAnLeds(ADC_CHANNEL adcChannel);
void setLowBattLed(bool lowbatt);



#endif	/* LED_H */

