/*******************************************************************************
 * This file declares all the public global variables.
 *
 * Company: Cytron Technologies Sdn Bhd
 * Website: http://www.cytron.io
 * Email:   support@cytron.io
 *******************************************************************************/

#include "variables.h"



/*******************************************************************************
 * GLOBAL VARIABLES
 *******************************************************************************/

// Power state.
static volatile bool isPowerOn = false;
static volatile bool isLowBatt = false;
static volatile bool isOvervoltage = false;

// Voltage threshold.
static volatile uint16_t lowbattUpperThreshold = LOW_BATT_UTH;
static volatile uint16_t lowbattLowerThreshold = LOW_BATT_LTH;
static volatile uint16_t overvoltageThreshold = OVERVOLTAGE_TH;

// ADC Result.
static volatile uint16_t adcResults[TOTAL_ADC_CHANNELS] = {0};

// I2C Registers.
static volatile uint8_t i2cRegisters[TOTAL_I2C_REG] = {
    0,      // Firmware Revision (Placeholder)
    0,      // Servo 1
    0,      // Servo 2
    0,      // Servo 3
    0,      // M1A
    0,      // M1B
    0,      // M2A
    0,      // M2B
    37,     // Lowbatt Upper Threshold
    36,     // Lowbatt Lower Threshold
    70,     // Overvoltae Threshold
    0,      // Vin Voltage (Placeholder)
    0,      // Power On State (Placeholder)
    0,      // Low Batt State (Placeholder)
    0       // Overvoltage State (Placeholder)
};

// Timer tick in ms.
static volatile uint32_t timerTickMs = 0;



/*******************************************************************************
 * GETTER & SETTER FUNCTIONS
 *******************************************************************************/

bool getPowerOnState(void)
{
    return isPowerOn;
}

void setPowerOnState(bool state)
{
    isPowerOn = state;
}



bool getLowBattState(void)
{
    return isLowBatt;
}

void setLowBattState(bool state)
{
    isLowBatt = state;
}



bool getOvervoltageState(void)
{
    return isOvervoltage;
}

void setOvervoltageState(bool state)
{
    isOvervoltage = state;
}



uint16_t getLowbattUpperThreshold(void)
{
    // Disable I2C slave interrupt.
    PIE1bits.SSPIE = 0;
    
    uint16_t value = lowbattUpperThreshold;
    
    // Enable I2C slave interrupt.
    PIE1bits.SSPIE = 1;
    
    return value;
}

uint16_t getLowbattLowerThreshold(void)
{
    // Disable I2C slave interrupt.
    PIE1bits.SSPIE = 0;
    
    uint16_t value = lowbattLowerThreshold;
    
    // Enable I2C slave interrupt.
    PIE1bits.SSPIE = 1;
    
    return value;
}

uint16_t getOvervoltageThreshold(void)
{
    // Disable I2C slave interrupt.
    PIE1bits.SSPIE = 0;
    
    uint16_t value = overvoltageThreshold;
    
    // Enable I2C slave interrupt.
    PIE1bits.SSPIE = 1;
    
    return value;
}



uint16_t getAdcResults(ADC_CHANNEL channel)
{
    // Disable ADC interrupt.
    PIE1bits.ADIE = 0;
    
    uint16_t value = adcResults[channel];
    
    // Enable ADC interrupt.
    PIE1bits.ADIE = 1;
    
    return value;
}

void setAdcResults(ADC_CHANNEL channel, uint16_t value)
{
    adcResults[channel] = value;
}



uint8_t getI2cRegister(I2C_REG reg)
{
    uint8_t value = 0;
    
    if ((uint8_t)reg < TOTAL_I2C_REG) {
        switch (reg) {
            case I2C_REG_FIRMWARE_REV:
                value = FIRMWARE_REV;
                break;
                
            case I2C_REG_VIN:
                value = (uint8_t)( (uint32_t)getAdcResults(ADC_VIN) * (122UL * 33UL) / (22UL * 1023UL) );
                break;
                
            case I2C_REG_PWR_STATE:
                value = getPowerOnState();
                break;
                
            case I2C_REG_LB_STATE:
                value = getLowBattState();
                break;
                
            case I2C_REG_OV_STATE:
                value = getOvervoltageState();
                break;
                
            default:
                value = i2cRegisters[reg];
                break;
        }
    }
    
    return value;
}

void setI2cRegister(I2C_REG reg, uint8_t value)
{
    if ((uint8_t)reg < TOTAL_I2C_REG) {
        
        switch (reg) {
            case I2C_REG_M1A:
                PWM5_LoadDutyValue(value);
                i2cRegisters[reg] = value;
                break;
                
            case I2C_REG_M1B:
                EPWM3_LoadDutyValue(value);
                i2cRegisters[reg] = value;
                break;
                
            case I2C_REG_M2A:
                EPWM1_LoadDutyValue(value);
                i2cRegisters[reg] = value;
                break;
                
            case I2C_REG_M2B:
                EPWM2_LoadDutyValue(value);
                i2cRegisters[reg] = value;
                break;
                
            case I2C_REG_LB_UTH:
                lowbattUpperThreshold = (uint16_t)( (uint32_t)value * (1023UL * 100UL * 22UL) / (122UL * 3300UL) );
                i2cRegisters[reg] = value;
                break;
                
            case I2C_REG_LB_LTH:
                lowbattLowerThreshold = (uint16_t)( (uint32_t)value * (1023UL * 100UL * 22UL) / (122UL * 3300UL) );
                i2cRegisters[reg] = value;
                break;
                
            case I2C_REG_OV_TH:
                overvoltageThreshold = (uint16_t)( (uint32_t)value * (1023UL * 100UL * 22UL) / (122UL * 3300UL) );
                i2cRegisters[reg] = value;
                break;
                
            default:
                i2cRegisters[reg] = value;
                break;
        }
    }
}



uint32_t getTimerTickMs(void)
{
    // Disable TMR6 interrupt.
    PIE3bits.TMR6IE = 0;
    
    uint32_t value = timerTickMs;
    
    // Enable TMR6 interrupt.
    PIE3bits.TMR6IE = 1;
    
    return value;
}

void incTimerTickMs(void)
{
    timerTickMs++;
}

