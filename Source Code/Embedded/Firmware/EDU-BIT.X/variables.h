/*******************************************************************************
 * This file declares all the public global variables.
 *
 * Company: Cytron Technologies Sdn Bhd
 * Website: http://www.cytron.io
 * Email:   support@cytron.io
 *******************************************************************************/

#ifndef VARIABLES_H
#define	VARIABLES_H

#include "mcc_generated_files/mcc.h"



/*******************************************************************************
 * PUBLIC DEFINITIONS
 *******************************************************************************/

// Firmware revision.
#define FIRMWARE_REV    3

// Number of I2C registers.
#define TOTAL_I2C_REG   15

// Index for I2C registers.
typedef enum {
    I2C_REG_FIRMWARE_REV    = 0x00,
    I2C_REG_SRV1            = 0x01,
    I2C_REG_SRV2            = 0x02,
    I2C_REG_SRV3            = 0x03,
    I2C_REG_M1A             = 0x04,
    I2C_REG_M1B             = 0x05,
    I2C_REG_M2A             = 0x06,
    I2C_REG_M2B             = 0x07,
    I2C_REG_LB_UTH          = 0x08,
    I2C_REG_LB_LTH          = 0x09,
    I2C_REG_OV_TH           = 0x0A,
    I2C_REG_VIN             = 0x0B,
    I2C_REG_PWR_STATE       = 0x0C,
    I2C_REG_LB_STATE        = 0x0D,
    I2C_REG_OV_STATE        = 0x0E,
} I2C_REG;



// Number of ADC channels.
#define TOTAL_ADC_CHANNELS  4

// ADC Channel.
typedef enum {
    ADC_P0      = 0x00,
    ADC_P1      = 0x01,
    ADC_P2      = 0x02,
    ADC_VIN     = 0x03
} ADC_CHANNEL;



// Threshold for power off.
// Below this voltage, means the board is off.
// Value = 1023 * Voltage(mV) * 22 / 122 / 3300
#define PWROFF_TH  (uint16_t)(1023UL * 1000UL * 22UL / 122UL / 3300UL)      // 1.0V

// Threshold to blink the Low Batt LED (Raw ADC value).
// Using upper and lower threshold for schmitt trigger.
// Value = 1023 * Voltage(mV) * 22 / 122 / 3300
#define LOW_BATT_UTH    (uint16_t)(1023UL * 3700UL * 22UL / 122UL / 3300UL)      // 3.7V
#define LOW_BATT_LTH    (uint16_t)(1023UL * 3600UL * 22UL / 122UL / 3300UL)      // 3.6V

// Threshold to cut off power for overvoltage protection (Raw ADC value).
// Value = 1023 * Voltage(mV) * 22 / 122 / 3300
#define OVERVOLTAGE_TH  (uint16_t)(1023UL * 7000UL * 22UL / 122UL / 3300UL)      // 7.0V



/*******************************************************************************
 * PUBLIC FUNCTION PROTOTYPES
 *******************************************************************************/

bool getPowerOnState(void);
void setPowerOnState(bool state);

bool getLowBattState(void);
void setLowBattState(bool state);

bool getOvervoltageState(void);
void setOvervoltageState(bool state);

uint16_t getLowbattUpperThreshold(void);
uint16_t getLowbattLowerThreshold(void);
uint16_t getOvervoltageThreshold(void);

uint16_t getAdcResults(ADC_CHANNEL channel);
void setAdcResults(ADC_CHANNEL channel, uint16_t value);

uint8_t getI2cRegister(I2C_REG reg);
void setI2cRegister(I2C_REG reg, uint8_t value);

uint32_t getTimerTickMs(void);
void incTimerTickMs(void);



#endif	/* VARIABLES_H */
