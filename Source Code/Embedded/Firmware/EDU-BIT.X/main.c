/*******************************************************************************
 * Main program for EDU:BIT.
 *
 * Company: Cytron Technologies Sdn Bhd
 * Website: http://www.cytron.com.my
 * Email:   support@cytron.io
 *******************************************************************************/

#include "mcc_generated_files/mcc.h"
#include "variables.h"
#include "led.h"



/*******************************************************************************
 * PRIVATE DEFINITION
 *******************************************************************************/

// Macro to turn on/off the power MOSFET using weak pull up.
#define PWR_ON()  WPUEbits.WPUE3 = 1
#define PWR_OFF() WPUEbits.WPUE3 = 0



/*******************************************************************************
 * MAIN FUNCTION
 *******************************************************************************/
void main(void)
{
    // Initialize the device.
    SYSTEM_Initialize();
    
    // Open the I2C.
    I2C_Open();

    // Enable the Global Interrupts.
    INTERRUPT_GlobalInterruptEnable();

    // Enable the Peripheral Interrupts.
    INTERRUPT_PeripheralInterruptEnable();
    
    // Enable the global weak pull up.
    OPTION_REGbits.nWPUEN = 0;
    
    
    
    bool lastSwState = false;
    
    while (1) {
        // Clear the WDT.
        // Timeout = 130ms.
        CLRWDT();
        
        // Read the input voltage.
        uint16_t vinAdcResult = getAdcResults(ADC_VIN);
        
        // Switch is off.
        if (vinAdcResult < PWROFF_TH) {
            PWR_OFF();
            clearAllLeds();
            
            // Save the switch state.
            lastSwState = false;
            setPowerOnState(false);
            setLowBattState(false);
            setOvervoltageState(false);
        }
        
        // Switch is on.
        else {
            // Save the power state (For I2C).
            setPowerOnState(true);
            
            // If the switch is just turned on.
            if (lastSwState == false) {
                // Wait for 20ms until the voltage stabilize.
                __delay_ms(20);
            }
            
            // Show LED level for P0, P1, P2 and low batt state if the switch has been turned on for a while.
            else {
                setAnLeds(ADC_P0);
                setAnLeds(ADC_P1);
                setAnLeds(ADC_P2);
                
                // Is it low batt?
                // Schmitt trigger for the lowbatt state.
                static bool lowbatt = false;
                if (vinAdcResult <= getLowbattLowerThreshold()) {
                    lowbatt = true;
                }
                else if (vinAdcResult >= getLowbattUpperThreshold()) {
                    lowbatt = false;
                }
                
                setLowBattState(lowbatt);
                
                // Blink the Low Batt LED when voltage is low.
                setLowBattLed(lowbatt);
            }
            
            
            // Turn on power only if it's not overvoltage.
            if (vinAdcResult < getOvervoltageThreshold()) {
                PWR_ON();
                setOvervoltageState(false);
            }
            else {
                PWR_OFF();
                setOvervoltageState(true);
            }
            
            
            // Show the startup LED pattern if the switch is just turned on.
            if (lastSwState == false) {
                showStartupPattern();
    
                // Save the switch state.
                lastSwState = true;
            }
            
        }
        
        
        
    }
}
