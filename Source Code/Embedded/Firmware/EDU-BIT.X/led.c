/*******************************************************************************
 * This file provides the functions to control the LEDs.
 *
 * Company: Cytron Technologies Sdn Bhd
 * Website: http://www.cytron.io
 * Email:   support@cytron.io
 *******************************************************************************/

#include "led.h"
#include "variables.h"



/*******************************************************************************
 * PRIVATE CONSTANT DEFINITION                                                 *
 *******************************************************************************/

// Low Batt LED blink duty cycle (ms).
#define LOWBATT_LED_ONTIME      100
#define LOWBATT_LED_OFFTIME     900

// Delay for startup pattern.
#define STARTUP_PATTERN_DELAY   25



/*******************************************************************************
 * FUNCTION: clearAllLeds
 *
 * PARAMETERS:
 * ~ void
 *
 * RETURN:
 * ~ void
 *
 * DESCRIPTIONS:
 * Clear all the LEDs.
 *
 *******************************************************************************/
void clearAllLeds(void)
{
    LED_LOWBATT_SetLow();
    
    P0_L0_SetLow();
    P0_L1_SetLow();
    P0_L2_SetLow();
    P0_L3_SetLow();
    P0_L4_SetLow();
    P0_L5_SetLow();
    P0_L6_SetLow();

    P1_L0_SetLow();
    P1_L1_SetLow();
    P1_L2_SetLow();
    P1_L3_SetLow();
    P1_L4_SetLow();
    P1_L5_SetLow();
    P1_L6_SetLow();

    P2_L0_SetLow();
    P2_L1_SetLow();
    P2_L2_SetLow();
    P2_L3_SetLow();
    P2_L4_SetLow();
    P2_L5_SetLow();
    P2_L6_SetLow();
}



/*******************************************************************************
 * FUNCTION: showStartupPattern
 *
 * PARAMETERS:
 * ~ void
 *
 * RETURN:
 * ~ void
 *
 * DESCRIPTIONS:
 * Show running light pattern during startup.
 *
 *******************************************************************************/
void showStartupPattern(void)
{
    // Turn on all LEDs.
    P0_L0_SetHigh();
    P0_L1_SetHigh();
    P0_L2_SetHigh();
    P0_L3_SetHigh();
    P0_L4_SetHigh();
    P0_L5_SetHigh();
    P0_L6_SetHigh();
    
    P1_L0_SetHigh();
    P1_L1_SetHigh();
    P1_L2_SetHigh();
    P1_L3_SetHigh();
    P1_L4_SetHigh();
    P1_L5_SetHigh();
    P1_L6_SetHigh();
    
    P2_L0_SetHigh();
    P2_L1_SetHigh();
    P2_L2_SetHigh();
    P2_L3_SetHigh();
    P2_L4_SetHigh();
    P2_L5_SetHigh();
    P2_L6_SetHigh();
    
    
    // Wait for 500ms.
    uint32_t timestamp = getTimerTickMs();
    while (getTimerTickMs() - timestamp < 500) {
        CLRWDT();
    }
    
    
    // Off all LEDs for 500ms.
    clearAllLeds();
    
    timestamp = getTimerTickMs();
    while (getTimerTickMs() - timestamp < 500) {
        CLRWDT();
    }
    
    // Start the running light.
    P0_L0_SetHigh();    CLRWDT();   __delay_ms(STARTUP_PATTERN_DELAY);
    P0_L1_SetHigh();    CLRWDT();   __delay_ms(STARTUP_PATTERN_DELAY);
    P0_L2_SetHigh();    CLRWDT();   __delay_ms(STARTUP_PATTERN_DELAY);
    P0_L3_SetHigh();    CLRWDT();   __delay_ms(STARTUP_PATTERN_DELAY);
    P0_L4_SetHigh();    CLRWDT();   __delay_ms(STARTUP_PATTERN_DELAY);
    P0_L5_SetHigh();    CLRWDT();   __delay_ms(STARTUP_PATTERN_DELAY);
    P0_L6_SetHigh();    CLRWDT();   __delay_ms(STARTUP_PATTERN_DELAY);
    
    P1_L0_SetHigh();    CLRWDT();   __delay_ms(STARTUP_PATTERN_DELAY);
    P1_L1_SetHigh();    CLRWDT();   __delay_ms(STARTUP_PATTERN_DELAY);
    P1_L2_SetHigh();    CLRWDT();   __delay_ms(STARTUP_PATTERN_DELAY);
    P1_L3_SetHigh();    CLRWDT();   __delay_ms(STARTUP_PATTERN_DELAY);
    P1_L4_SetHigh();    CLRWDT();   __delay_ms(STARTUP_PATTERN_DELAY);
    P1_L5_SetHigh();    CLRWDT();   __delay_ms(STARTUP_PATTERN_DELAY);
    P1_L6_SetHigh();    CLRWDT();   __delay_ms(STARTUP_PATTERN_DELAY);
    
    P2_L0_SetHigh();    CLRWDT();   __delay_ms(STARTUP_PATTERN_DELAY);
    P2_L1_SetHigh();    CLRWDT();   __delay_ms(STARTUP_PATTERN_DELAY);
    P2_L2_SetHigh();    CLRWDT();   __delay_ms(STARTUP_PATTERN_DELAY);
    P2_L3_SetHigh();    CLRWDT();   __delay_ms(STARTUP_PATTERN_DELAY);
    P2_L4_SetHigh();    CLRWDT();   __delay_ms(STARTUP_PATTERN_DELAY);
    P2_L5_SetHigh();    CLRWDT();   __delay_ms(STARTUP_PATTERN_DELAY);
    P2_L6_SetHigh();    CLRWDT();   __delay_ms(STARTUP_PATTERN_DELAY);
    
    // Wait for 300ms.
    timestamp = getTimerTickMs();
    while (getTimerTickMs() - timestamp < 300) {
        CLRWDT();
    }
    
    P0_L6_SetLow(); P1_L6_SetLow(); P2_L6_SetLow(); CLRWDT();   __delay_ms(STARTUP_PATTERN_DELAY);
    P0_L5_SetLow(); P1_L5_SetLow(); P2_L5_SetLow(); CLRWDT();   __delay_ms(STARTUP_PATTERN_DELAY);
    P0_L4_SetLow(); P1_L4_SetLow(); P2_L4_SetLow(); CLRWDT();   __delay_ms(STARTUP_PATTERN_DELAY);
    P0_L3_SetLow(); P1_L3_SetLow(); P2_L3_SetLow(); CLRWDT();   __delay_ms(STARTUP_PATTERN_DELAY);
    P0_L2_SetLow(); P1_L2_SetLow(); P2_L2_SetLow(); CLRWDT();   __delay_ms(STARTUP_PATTERN_DELAY);
    P0_L1_SetLow(); P1_L1_SetLow(); P2_L1_SetLow(); CLRWDT();   __delay_ms(STARTUP_PATTERN_DELAY);
    P0_L0_SetLow(); P1_L0_SetLow(); P2_L0_SetLow(); CLRWDT();   __delay_ms(STARTUP_PATTERN_DELAY);
    
    // Wait for 300ms.
    timestamp = getTimerTickMs();
    while (getTimerTickMs() - timestamp < 300) {
        CLRWDT();
    }
}



/*******************************************************************************
 * FUNCTION: setAnLeds
 *
 * PARAMETERS:
 * ~ adcChannel - ADC channel.
 *
 * RETURN:
 * ~ void
 *
 * DESCRIPTIONS:
 * Control the LEDs to indicate the voltage level.
 *
 *******************************************************************************/
void setAnLeds(ADC_CHANNEL adcChannel)
{
    // Get the LEDs state based on voltage.
    uint16_t adcResult = getAdcResults(adcChannel);
    bool ledState[7] = {0};
    
    if (adcResult > (1023 * 1 / 8)) {
        ledState[0] = 1;
    }
    
    if (adcResult > (1023 * 2 / 8)) {
        ledState[1] = 1;
    }
    
    if (adcResult > (1023 * 3 / 8)) {
        ledState[2] = 1;
    }
    
    if (adcResult > (1023 * 4 / 8)) {
        ledState[3] = 1;
    }
    
    if (adcResult > (1023 * 5 / 8)) {
        ledState[4] = 1;
    }
    
    if (adcResult > (1023 * 6 / 8)) {
        ledState[5] = 1;
    }
    
    if (adcResult > (1023 * 7 / 8)) {
        ledState[6] = 1;
    }
    
    // Set the LEDs according to the state.
    switch (adcChannel) {
        case ADC_P0:
            P0_L0_LAT = ledState[0];
            P0_L1_LAT = ledState[1];
            P0_L2_LAT = ledState[2];
            P0_L3_LAT = ledState[3];
            P0_L4_LAT = ledState[4];
            P0_L5_LAT = ledState[5];
            P0_L6_LAT = ledState[6];
            break;
            
        case ADC_P1:
            P1_L0_LAT = ledState[0];
            P1_L1_LAT = ledState[1];
            P1_L2_LAT = ledState[2];
            P1_L3_LAT = ledState[3];
            P1_L4_LAT = ledState[4];
            P1_L5_LAT = ledState[5];
            P1_L6_LAT = ledState[6];
            break;
            
        case ADC_P2:
            P2_L0_LAT = ledState[0];
            P2_L1_LAT = ledState[1];
            P2_L2_LAT = ledState[2];
            P2_L3_LAT = ledState[3];
            P2_L4_LAT = ledState[4];
            P2_L5_LAT = ledState[5];
            P2_L6_LAT = ledState[6];
            break;
            
        default:
            break;
    }
}



/*******************************************************************************
 * FUNCTION: setLowBattLed
 *
 * PARAMETERS:
 * ~ lowbatt    - Is it low batt?
 *
 * RETURN:
 * ~ void
 *
 * DESCRIPTIONS:
 * Blink the Low Batt LED when battery voltage is low.
 * This is a non blocking function and should be called repetively.
 *
 *******************************************************************************/
void setLowBattLed(bool lowbatt)
{
    // Blink the LED if battery voltage is low.
    static uint32_t timestamp = 0;
    if (lowbatt) {
        uint32_t delay = (LED_LOWBATT_LAT)? LOWBATT_LED_ONTIME : LOWBATT_LED_OFFTIME;
        
        uint32_t now = getTimerTickMs();
        if (now - timestamp >= delay) {
            LED_LOWBATT_LAT ^= 1;
            timestamp = now;
        }
    }
    
    // Battery voltage is good.
    else {
        LED_LOWBATT_LAT = 0;
        timestamp = 0;
    }
}

