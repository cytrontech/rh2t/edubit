<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.7.0">
<drawing>
<settings>
<setting alwaysvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="3" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="3" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="5" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="5" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="6" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="6" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="no" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="no" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="no" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="no" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="no" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="no" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="no" active="yes"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="no" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="no" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="no" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="no" active="yes"/>
<layer number="134" name="silk_top" color="7" fill="1" visible="no" active="no"/>
<layer number="135" name="silk_bottom" color="7" fill="1" visible="no" active="no"/>
<layer number="136" name="silktop" color="7" fill="1" visible="no" active="yes"/>
<layer number="137" name="silkbottom" color="7" fill="1" visible="no" active="yes"/>
<layer number="138" name="mbTest" color="7" fill="1" visible="no" active="yes"/>
<layer number="139" name="mtKeepout" color="7" fill="1" visible="no" active="yes"/>
<layer number="140" name="mbKeepout" color="7" fill="1" visible="no" active="yes"/>
<layer number="141" name="mtRestrict" color="7" fill="1" visible="no" active="yes"/>
<layer number="142" name="mbRestrict" color="7" fill="1" visible="no" active="yes"/>
<layer number="143" name="mvRestrict" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="145" name="DrillLegend_01-16" color="7" fill="1" visible="no" active="yes"/>
<layer number="146" name="DrillLegend_01-20" color="7" fill="1" visible="no" active="yes"/>
<layer number="147" name="mMeasures" color="7" fill="1" visible="no" active="yes"/>
<layer number="148" name="mDocument" color="7" fill="1" visible="no" active="yes"/>
<layer number="149" name="mReference" color="7" fill="1" visible="no" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="191" name="mNets" color="7" fill="1" visible="no" active="yes"/>
<layer number="192" name="mBusses" color="7" fill="1" visible="no" active="yes"/>
<layer number="193" name="mPins" color="7" fill="1" visible="no" active="yes"/>
<layer number="194" name="mSymbols" color="7" fill="1" visible="no" active="yes"/>
<layer number="195" name="mNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="196" name="mValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="no" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="no" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="no" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="Misc">
<description>Misc thing which is not a component. Such as fiducial, mounting hole, logo, etc.</description>
<packages>
<package name="TP-1.5MM">
<description>Test Point - 1.5mm&lt;br&gt;
&lt;br&gt;
Created by: Wai Weng</description>
<smd name="P$1" x="0" y="0" dx="1.5" dy="1.5" layer="1" roundness="100" thermals="no" cream="no"/>
<text x="1.016" y="0" size="0.762" layer="21" font="vector" ratio="15" align="center-left">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="BORDER-A4">
<wire x1="0" y1="190.5" x2="279.4" y2="190.5" width="0.4064" layer="94"/>
<wire x1="279.4" y1="190.5" x2="279.4" y2="0" width="0.4064" layer="94"/>
<wire x1="0" y1="190.5" x2="0" y2="0" width="0.4064" layer="94"/>
<wire x1="0" y1="0" x2="279.4" y2="0" width="0.4064" layer="94"/>
</symbol>
<symbol name="DATA-FIELD">
<wire x1="0" y1="0" x2="71.12" y2="0" width="0.254" layer="94"/>
<wire x1="101.6" y1="15.24" x2="80.01" y2="15.24" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="7.62" width="0.254" layer="94"/>
<wire x1="0" y1="7.62" x2="71.12" y2="7.62" width="0.254" layer="94"/>
<wire x1="0" y1="7.62" x2="0" y2="15.24" width="0.254" layer="94"/>
<wire x1="101.6" y1="15.24" x2="101.6" y2="7.62" width="0.254" layer="94"/>
<wire x1="71.12" y1="7.62" x2="71.12" y2="0" width="0.254" layer="94"/>
<wire x1="71.12" y1="7.62" x2="80.01" y2="7.62" width="0.254" layer="94"/>
<wire x1="71.12" y1="0" x2="101.6" y2="0" width="0.254" layer="94"/>
<wire x1="80.01" y1="15.24" x2="80.01" y2="7.62" width="0.254" layer="94"/>
<wire x1="80.01" y1="15.24" x2="0" y2="15.24" width="0.254" layer="94"/>
<wire x1="80.01" y1="7.62" x2="101.6" y2="7.62" width="0.254" layer="94"/>
<wire x1="101.6" y1="7.62" x2="101.6" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="15.24" x2="0" y2="22.86" width="0.254" layer="94"/>
<wire x1="0" y1="22.86" x2="101.6" y2="22.86" width="0.254" layer="94"/>
<wire x1="101.6" y1="22.86" x2="101.6" y2="15.24" width="0.254" layer="94"/>
<text x="1.27" y="2.54" size="2.54" layer="94" font="vector">Date:</text>
<text x="12.7" y="2.54" size="2.54" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="72.39" y="2.54" size="2.54" layer="94" font="vector">Sheet:</text>
<text x="86.36" y="2.54" size="2.54" layer="94" font="vector">&gt;SHEET</text>
<text x="81.28" y="10.16" size="2.54" layer="94" font="vector">REV:</text>
<text x="1.27" y="17.78" size="2.54" layer="94" font="vector">Name:</text>
<text x="1.27" y="10.16" size="2.54" layer="94" font="vector">Product Code: </text>
<text x="30.48" y="10.16" size="2.54" layer="94" font="vector">&gt;DRAWING_NAME</text>
</symbol>
<symbol name="CYTRON-LOGO">
<rectangle x1="14.183359375" y1="-0.00914375" x2="14.4526" y2="0.00914375" layer="94"/>
<rectangle x1="14.1097" y1="0.00914375" x2="14.526259375" y2="0.027178125" layer="94"/>
<rectangle x1="14.056359375" y1="0.027178125" x2="14.5796" y2="0.0452125" layer="94"/>
<rectangle x1="14.0208" y1="0.0452125" x2="14.615159375" y2="0.063246875" layer="94"/>
<rectangle x1="14.00048125" y1="0.063246875" x2="14.63548125" y2="0.08128125" layer="94"/>
<rectangle x1="13.9827" y1="0.08128125" x2="14.671040625" y2="0.0993125" layer="94"/>
<rectangle x1="13.96491875" y1="0.0993125" x2="14.68881875" y2="0.117603125" layer="94"/>
<rectangle x1="13.96491875" y1="0.117603125" x2="14.7066" y2="0.1356375" layer="94"/>
<rectangle x1="13.947140625" y1="0.1356375" x2="14.72438125" y2="0.15366875" layer="94"/>
<rectangle x1="13.947140625" y1="0.15366875" x2="14.72438125" y2="0.171703125" layer="94"/>
<rectangle x1="13.947140625" y1="0.171703125" x2="14.742159375" y2="0.1897375" layer="94"/>
<rectangle x1="13.929359375" y1="0.1897375" x2="14.30781875" y2="0.208025" layer="94"/>
<rectangle x1="14.399259375" y1="0.1897375" x2="14.759940625" y2="0.208025" layer="94"/>
<rectangle x1="13.929359375" y1="0.208025" x2="14.2367" y2="0.226059375" layer="94"/>
<rectangle x1="14.47291875" y1="0.208025" x2="14.759940625" y2="0.226059375" layer="94"/>
<rectangle x1="13.929359375" y1="0.226059375" x2="14.21891875" y2="0.24409375" layer="94"/>
<rectangle x1="14.50848125" y1="0.226059375" x2="14.780259375" y2="0.24409375" layer="94"/>
<rectangle x1="13.929359375" y1="0.24409375" x2="14.21891875" y2="0.262128125" layer="94"/>
<rectangle x1="14.526259375" y1="0.24409375" x2="14.780259375" y2="0.262128125" layer="94"/>
<rectangle x1="13.929359375" y1="0.262128125" x2="14.21891875" y2="0.2801625" layer="94"/>
<rectangle x1="14.544040625" y1="0.262128125" x2="14.780259375" y2="0.2801625" layer="94"/>
<rectangle x1="13.929359375" y1="0.2801625" x2="14.21891875" y2="0.298196875" layer="94"/>
<rectangle x1="14.544040625" y1="0.2801625" x2="14.798040625" y2="0.298196875" layer="94"/>
<rectangle x1="13.929359375" y1="0.298196875" x2="14.145259375" y2="0.316484375" layer="94"/>
<rectangle x1="14.56181875" y1="0.298196875" x2="14.798040625" y2="0.316484375" layer="94"/>
<rectangle x1="13.929359375" y1="0.316484375" x2="14.0208" y2="0.33451875" layer="94"/>
<rectangle x1="14.56181875" y1="0.316484375" x2="14.798040625" y2="0.33451875" layer="94"/>
<rectangle x1="14.5796" y1="0.33451875" x2="14.81581875" y2="0.352553125" layer="94"/>
<rectangle x1="14.5796" y1="0.352553125" x2="14.81581875" y2="0.3705875" layer="94"/>
<rectangle x1="14.5796" y1="0.3705875" x2="14.81581875" y2="0.38861875" layer="94"/>
<rectangle x1="6.588759375" y1="0.38861875" x2="6.8072" y2="0.406653125" layer="94"/>
<rectangle x1="7.711440625" y1="0.38861875" x2="7.927340625" y2="0.406653125" layer="94"/>
<rectangle x1="11.27251875" y1="0.38861875" x2="11.48841875" y2="0.406653125" layer="94"/>
<rectangle x1="13.078459375" y1="0.38861875" x2="13.2969" y2="0.406653125" layer="94"/>
<rectangle x1="14.5796" y1="0.38861875" x2="14.81581875" y2="0.406653125" layer="94"/>
<rectangle x1="16.04518125" y1="0.38861875" x2="16.26108125" y2="0.406653125" layer="94"/>
<rectangle x1="17.129759375" y1="0.38861875" x2="17.38121875" y2="0.406653125" layer="94"/>
<rectangle x1="5.539740625" y1="0.406653125" x2="5.793740625" y2="0.424940625" layer="94"/>
<rectangle x1="6.517640625" y1="0.406653125" x2="6.860540625" y2="0.424940625" layer="94"/>
<rectangle x1="7.655559375" y1="0.406653125" x2="7.998459375" y2="0.424940625" layer="94"/>
<rectangle x1="8.54201875" y1="0.406653125" x2="8.778240625" y2="0.424940625" layer="94"/>
<rectangle x1="9.13891875" y1="0.406653125" x2="9.39291875" y2="0.424940625" layer="94"/>
<rectangle x1="9.7536" y1="0.406653125" x2="9.98728125" y2="0.424940625" layer="94"/>
<rectangle x1="10.3505" y1="0.406653125" x2="10.601959375" y2="0.424940625" layer="94"/>
<rectangle x1="11.216640625" y1="0.406653125" x2="11.541759375" y2="0.424940625" layer="94"/>
<rectangle x1="12.156440625" y1="0.406653125" x2="12.410440625" y2="0.424940625" layer="94"/>
<rectangle x1="13.02511875" y1="0.406653125" x2="13.350240625" y2="0.424940625" layer="94"/>
<rectangle x1="14.2367" y1="0.406653125" x2="14.3637" y2="0.424940625" layer="94"/>
<rectangle x1="14.5796" y1="0.406653125" x2="14.8336" y2="0.424940625" layer="94"/>
<rectangle x1="15.1765" y1="0.406653125" x2="15.4305" y2="0.424940625" layer="94"/>
<rectangle x1="15.9893" y1="0.406653125" x2="16.334740625" y2="0.424940625" layer="94"/>
<rectangle x1="17.0561" y1="0.406653125" x2="17.45488125" y2="0.424940625" layer="94"/>
<rectangle x1="5.539740625" y1="0.424940625" x2="5.793740625" y2="0.442975" layer="94"/>
<rectangle x1="6.48208125" y1="0.424940625" x2="6.91388125" y2="0.442975" layer="94"/>
<rectangle x1="7.62" y1="0.424940625" x2="8.036559375" y2="0.442975" layer="94"/>
<rectangle x1="8.54201875" y1="0.424940625" x2="8.778240625" y2="0.442975" layer="94"/>
<rectangle x1="9.13891875" y1="0.424940625" x2="9.39291875" y2="0.442975" layer="94"/>
<rectangle x1="9.7536" y1="0.424940625" x2="9.98728125" y2="0.442975" layer="94"/>
<rectangle x1="10.3505" y1="0.424940625" x2="10.601959375" y2="0.442975" layer="94"/>
<rectangle x1="11.1633" y1="0.424940625" x2="11.597640625" y2="0.442975" layer="94"/>
<rectangle x1="12.156440625" y1="0.424940625" x2="12.410440625" y2="0.442975" layer="94"/>
<rectangle x1="12.989559375" y1="0.424940625" x2="13.40611875" y2="0.442975" layer="94"/>
<rectangle x1="14.183359375" y1="0.424940625" x2="14.417040625" y2="0.442975" layer="94"/>
<rectangle x1="14.5796" y1="0.424940625" x2="14.8336" y2="0.442975" layer="94"/>
<rectangle x1="15.1765" y1="0.424940625" x2="15.4305" y2="0.442975" layer="94"/>
<rectangle x1="15.953740625" y1="0.424940625" x2="16.3703" y2="0.442975" layer="94"/>
<rectangle x1="17.020540625" y1="0.424940625" x2="17.490440625" y2="0.442975" layer="94"/>
<rectangle x1="5.539740625" y1="0.442975" x2="5.81151875" y2="0.461009375" layer="94"/>
<rectangle x1="6.461759375" y1="0.442975" x2="6.95198125" y2="0.461009375" layer="94"/>
<rectangle x1="7.584440625" y1="0.442975" x2="8.07211875" y2="0.461009375" layer="94"/>
<rectangle x1="8.54201875" y1="0.442975" x2="8.79601875" y2="0.461009375" layer="94"/>
<rectangle x1="9.1567" y1="0.442975" x2="9.39291875" y2="0.461009375" layer="94"/>
<rectangle x1="9.7536" y1="0.442975" x2="9.98728125" y2="0.461009375" layer="94"/>
<rectangle x1="10.3505" y1="0.442975" x2="10.601959375" y2="0.461009375" layer="94"/>
<rectangle x1="11.127740625" y1="0.442975" x2="11.6332" y2="0.461009375" layer="94"/>
<rectangle x1="12.176759375" y1="0.442975" x2="12.410440625" y2="0.461009375" layer="94"/>
<rectangle x1="12.954" y1="0.442975" x2="13.44168125" y2="0.461009375" layer="94"/>
<rectangle x1="14.145259375" y1="0.442975" x2="14.47291875" y2="0.461009375" layer="94"/>
<rectangle x1="14.59738125" y1="0.442975" x2="14.8336" y2="0.461009375" layer="94"/>
<rectangle x1="15.19428125" y1="0.442975" x2="15.4305" y2="0.461009375" layer="94"/>
<rectangle x1="15.91818125" y1="0.442975" x2="16.405859375" y2="0.461009375" layer="94"/>
<rectangle x1="16.98498125" y1="0.442975" x2="17.526" y2="0.461009375" layer="94"/>
<rectangle x1="5.539740625" y1="0.461009375" x2="5.81151875" y2="0.47904375" layer="94"/>
<rectangle x1="6.4262" y1="0.461009375" x2="6.969759375" y2="0.47904375" layer="94"/>
<rectangle x1="7.566659375" y1="0.461009375" x2="8.10768125" y2="0.47904375" layer="94"/>
<rectangle x1="8.54201875" y1="0.461009375" x2="8.79601875" y2="0.47904375" layer="94"/>
<rectangle x1="9.1567" y1="0.461009375" x2="9.39291875" y2="0.47904375" layer="94"/>
<rectangle x1="9.7536" y1="0.461009375" x2="9.98728125" y2="0.47904375" layer="94"/>
<rectangle x1="10.36828125" y1="0.461009375" x2="10.601959375" y2="0.47904375" layer="94"/>
<rectangle x1="11.109959375" y1="0.461009375" x2="11.65098125" y2="0.47904375" layer="94"/>
<rectangle x1="12.176759375" y1="0.461009375" x2="12.410440625" y2="0.47904375" layer="94"/>
<rectangle x1="12.9159" y1="0.461009375" x2="13.477240625" y2="0.47904375" layer="94"/>
<rectangle x1="14.1097" y1="0.461009375" x2="14.4907" y2="0.47904375" layer="94"/>
<rectangle x1="14.59738125" y1="0.461009375" x2="14.8336" y2="0.47904375" layer="94"/>
<rectangle x1="15.19428125" y1="0.461009375" x2="15.4305" y2="0.47904375" layer="94"/>
<rectangle x1="15.9004" y1="0.461009375" x2="16.44141875" y2="0.47904375" layer="94"/>
<rectangle x1="16.9672" y1="0.461009375" x2="17.5641" y2="0.47904375" layer="94"/>
<rectangle x1="5.560059375" y1="0.47904375" x2="5.81151875" y2="0.497078125" layer="94"/>
<rectangle x1="6.40841875" y1="0.47904375" x2="7.00531875" y2="0.497078125" layer="94"/>
<rectangle x1="7.54888125" y1="0.47904375" x2="8.125459375" y2="0.497078125" layer="94"/>
<rectangle x1="8.54201875" y1="0.47904375" x2="8.79601875" y2="0.497078125" layer="94"/>
<rectangle x1="9.1567" y1="0.47904375" x2="9.39291875" y2="0.497078125" layer="94"/>
<rectangle x1="9.7536" y1="0.47904375" x2="10.0076" y2="0.497078125" layer="94"/>
<rectangle x1="10.36828125" y1="0.47904375" x2="10.601959375" y2="0.497078125" layer="94"/>
<rectangle x1="11.09218125" y1="0.47904375" x2="11.686540625" y2="0.497078125" layer="94"/>
<rectangle x1="12.176759375" y1="0.47904375" x2="12.410440625" y2="0.497078125" layer="94"/>
<rectangle x1="12.89811875" y1="0.47904375" x2="13.49501875" y2="0.497078125" layer="94"/>
<rectangle x1="14.09191875" y1="0.47904375" x2="14.526259375" y2="0.497078125" layer="94"/>
<rectangle x1="14.59738125" y1="0.47904375" x2="14.8336" y2="0.497078125" layer="94"/>
<rectangle x1="15.19428125" y1="0.47904375" x2="15.4305" y2="0.497078125" layer="94"/>
<rectangle x1="15.88261875" y1="0.47904375" x2="16.4592" y2="0.497078125" layer="94"/>
<rectangle x1="16.94941875" y1="0.47904375" x2="17.688559375" y2="0.497078125" layer="94"/>
<rectangle x1="5.560059375" y1="0.497078125" x2="5.81151875" y2="0.5151125" layer="94"/>
<rectangle x1="6.390640625" y1="0.497078125" x2="7.0231" y2="0.5151125" layer="94"/>
<rectangle x1="7.528559375" y1="0.497078125" x2="8.143240625" y2="0.5151125" layer="94"/>
<rectangle x1="8.5598" y1="0.497078125" x2="8.79601875" y2="0.5151125" layer="94"/>
<rectangle x1="9.1567" y1="0.497078125" x2="9.4107" y2="0.5151125" layer="94"/>
<rectangle x1="9.7536" y1="0.497078125" x2="10.0076" y2="0.5151125" layer="94"/>
<rectangle x1="10.36828125" y1="0.497078125" x2="10.601959375" y2="0.5151125" layer="94"/>
<rectangle x1="11.071859375" y1="0.497078125" x2="11.706859375" y2="0.5151125" layer="94"/>
<rectangle x1="12.176759375" y1="0.497078125" x2="12.410440625" y2="0.5151125" layer="94"/>
<rectangle x1="12.880340625" y1="0.497078125" x2="13.5128" y2="0.5151125" layer="94"/>
<rectangle x1="14.074140625" y1="0.497078125" x2="14.544040625" y2="0.5151125" layer="94"/>
<rectangle x1="14.59738125" y1="0.497078125" x2="14.85138125" y2="0.5151125" layer="94"/>
<rectangle x1="15.19428125" y1="0.497078125" x2="15.44828125" y2="0.5151125" layer="94"/>
<rectangle x1="15.864840625" y1="0.497078125" x2="16.47951875" y2="0.5151125" layer="94"/>
<rectangle x1="16.9291" y1="0.497078125" x2="17.815559375" y2="0.5151125" layer="94"/>
<rectangle x1="5.560059375" y1="0.5151125" x2="5.81151875" y2="0.5334" layer="94"/>
<rectangle x1="6.390640625" y1="0.5151125" x2="7.04088125" y2="0.5334" layer="94"/>
<rectangle x1="7.51078125" y1="0.5151125" x2="8.163559375" y2="0.5334" layer="94"/>
<rectangle x1="8.5598" y1="0.5151125" x2="8.79601875" y2="0.5334" layer="94"/>
<rectangle x1="9.1567" y1="0.5151125" x2="9.4107" y2="0.5334" layer="94"/>
<rectangle x1="9.7536" y1="0.5151125" x2="10.0076" y2="0.5334" layer="94"/>
<rectangle x1="10.36828125" y1="0.5151125" x2="10.619740625" y2="0.5334" layer="94"/>
<rectangle x1="11.05408125" y1="0.5151125" x2="11.724640625" y2="0.5334" layer="94"/>
<rectangle x1="12.176759375" y1="0.5151125" x2="12.42821875" y2="0.5334" layer="94"/>
<rectangle x1="12.862559375" y1="0.5151125" x2="13.53058125" y2="0.5334" layer="94"/>
<rectangle x1="14.074140625" y1="0.5151125" x2="14.56181875" y2="0.5334" layer="94"/>
<rectangle x1="14.59738125" y1="0.5151125" x2="14.85138125" y2="0.5334" layer="94"/>
<rectangle x1="15.19428125" y1="0.5151125" x2="15.44828125" y2="0.5334" layer="94"/>
<rectangle x1="15.84451875" y1="0.5151125" x2="16.4973" y2="0.5334" layer="94"/>
<rectangle x1="16.91131875" y1="0.5151125" x2="17.942559375" y2="0.5334" layer="94"/>
<rectangle x1="5.560059375" y1="0.5334" x2="5.81151875" y2="0.551434375" layer="94"/>
<rectangle x1="6.372859375" y1="0.5334" x2="7.058659375" y2="0.551434375" layer="94"/>
<rectangle x1="7.51078125" y1="0.5334" x2="8.181340625" y2="0.551434375" layer="94"/>
<rectangle x1="8.5598" y1="0.5334" x2="8.79601875" y2="0.551434375" layer="94"/>
<rectangle x1="9.1567" y1="0.5334" x2="9.4107" y2="0.551434375" layer="94"/>
<rectangle x1="9.77138125" y1="0.5334" x2="10.0076" y2="0.551434375" layer="94"/>
<rectangle x1="10.36828125" y1="0.5334" x2="10.619740625" y2="0.551434375" layer="94"/>
<rectangle x1="11.0363" y1="0.5334" x2="11.74241875" y2="0.551434375" layer="94"/>
<rectangle x1="12.176759375" y1="0.5334" x2="12.42821875" y2="0.551434375" layer="94"/>
<rectangle x1="12.84478125" y1="0.5334" x2="13.56868125" y2="0.551434375" layer="94"/>
<rectangle x1="14.056359375" y1="0.5334" x2="14.5796" y2="0.551434375" layer="94"/>
<rectangle x1="14.59738125" y1="0.5334" x2="14.85138125" y2="0.551434375" layer="94"/>
<rectangle x1="15.19428125" y1="0.5334" x2="15.44828125" y2="0.551434375" layer="94"/>
<rectangle x1="15.84451875" y1="0.5334" x2="16.51508125" y2="0.551434375" layer="94"/>
<rectangle x1="16.893540625" y1="0.5334" x2="18.05178125" y2="0.551434375" layer="94"/>
<rectangle x1="5.560059375" y1="0.551434375" x2="5.81151875" y2="0.56946875" layer="94"/>
<rectangle x1="6.35508125" y1="0.551434375" x2="7.076440625" y2="0.56946875" layer="94"/>
<rectangle x1="7.493" y1="0.551434375" x2="8.19911875" y2="0.56946875" layer="94"/>
<rectangle x1="8.5598" y1="0.551434375" x2="8.79601875" y2="0.56946875" layer="94"/>
<rectangle x1="9.17448125" y1="0.551434375" x2="9.4107" y2="0.56946875" layer="94"/>
<rectangle x1="9.77138125" y1="0.551434375" x2="10.0076" y2="0.56946875" layer="94"/>
<rectangle x1="10.36828125" y1="0.551434375" x2="10.619740625" y2="0.56946875" layer="94"/>
<rectangle x1="11.01851875" y1="0.551434375" x2="11.7602" y2="0.56946875" layer="94"/>
<rectangle x1="12.176759375" y1="0.551434375" x2="12.42821875" y2="0.56946875" layer="94"/>
<rectangle x1="12.84478125" y1="0.551434375" x2="13.56868125" y2="0.56946875" layer="94"/>
<rectangle x1="14.03858125" y1="0.551434375" x2="14.59738125" y2="0.56946875" layer="94"/>
<rectangle x1="14.615159375" y1="0.551434375" x2="14.85138125" y2="0.56946875" layer="94"/>
<rectangle x1="15.212059375" y1="0.551434375" x2="15.44828125" y2="0.56946875" layer="94"/>
<rectangle x1="15.826740625" y1="0.551434375" x2="16.532859375" y2="0.56946875" layer="94"/>
<rectangle x1="16.893540625" y1="0.551434375" x2="17.653" y2="0.56946875" layer="94"/>
<rectangle x1="17.726659375" y1="0.551434375" x2="18.158459375" y2="0.56946875" layer="94"/>
<rectangle x1="5.560059375" y1="0.56946875" x2="5.8293" y2="0.587503125" layer="94"/>
<rectangle x1="6.35508125" y1="0.56946875" x2="7.076440625" y2="0.587503125" layer="94"/>
<rectangle x1="7.493" y1="0.56946875" x2="8.2169" y2="0.587503125" layer="94"/>
<rectangle x1="8.5598" y1="0.56946875" x2="8.8138" y2="0.587503125" layer="94"/>
<rectangle x1="9.17448125" y1="0.56946875" x2="9.4107" y2="0.587503125" layer="94"/>
<rectangle x1="9.77138125" y1="0.56946875" x2="10.0076" y2="0.587503125" layer="94"/>
<rectangle x1="10.386059375" y1="0.56946875" x2="10.619740625" y2="0.587503125" layer="94"/>
<rectangle x1="11.01851875" y1="0.56946875" x2="11.77798125" y2="0.587503125" layer="94"/>
<rectangle x1="12.194540625" y1="0.56946875" x2="12.42821875" y2="0.587503125" layer="94"/>
<rectangle x1="12.827" y1="0.56946875" x2="13.586459375" y2="0.587503125" layer="94"/>
<rectangle x1="14.03858125" y1="0.56946875" x2="14.85138125" y2="0.587503125" layer="94"/>
<rectangle x1="15.212059375" y1="0.56946875" x2="15.44828125" y2="0.587503125" layer="94"/>
<rectangle x1="15.826740625" y1="0.56946875" x2="16.550640625" y2="0.587503125" layer="94"/>
<rectangle x1="16.875759375" y1="0.56946875" x2="17.67078125" y2="0.587503125" layer="94"/>
<rectangle x1="17.79778125" y1="0.56946875" x2="18.26768125" y2="0.587503125" layer="94"/>
<rectangle x1="5.577840625" y1="0.587503125" x2="5.8293" y2="0.6055375" layer="94"/>
<rectangle x1="6.35508125" y1="0.587503125" x2="6.66241875" y2="0.6055375" layer="94"/>
<rectangle x1="6.78941875" y1="0.587503125" x2="7.096759375" y2="0.6055375" layer="94"/>
<rectangle x1="7.47521875" y1="0.587503125" x2="8.23468125" y2="0.6055375" layer="94"/>
<rectangle x1="8.5598" y1="0.587503125" x2="8.8138" y2="0.6055375" layer="94"/>
<rectangle x1="9.17448125" y1="0.587503125" x2="9.4107" y2="0.6055375" layer="94"/>
<rectangle x1="9.77138125" y1="0.587503125" x2="10.02538125" y2="0.6055375" layer="94"/>
<rectangle x1="10.386059375" y1="0.587503125" x2="10.619740625" y2="0.6055375" layer="94"/>
<rectangle x1="11.000740625" y1="0.587503125" x2="11.795759375" y2="0.6055375" layer="94"/>
<rectangle x1="12.194540625" y1="0.587503125" x2="12.42821875" y2="0.6055375" layer="94"/>
<rectangle x1="12.827" y1="0.587503125" x2="13.604240625" y2="0.6055375" layer="94"/>
<rectangle x1="14.0208" y1="0.587503125" x2="14.85138125" y2="0.6055375" layer="94"/>
<rectangle x1="15.212059375" y1="0.587503125" x2="15.44828125" y2="0.6055375" layer="94"/>
<rectangle x1="15.808959375" y1="0.587503125" x2="16.13408125" y2="0.6055375" layer="94"/>
<rectangle x1="16.26108125" y1="0.587503125" x2="16.56841875" y2="0.6055375" layer="94"/>
<rectangle x1="16.875759375" y1="0.587503125" x2="17.1831" y2="0.6055375" layer="94"/>
<rectangle x1="17.401540625" y1="0.587503125" x2="17.67078125" y2="0.6055375" layer="94"/>
<rectangle x1="17.871440625" y1="0.587503125" x2="18.3769" y2="0.6055375" layer="94"/>
<rectangle x1="5.577840625" y1="0.6055375" x2="5.8293" y2="0.623825" layer="94"/>
<rectangle x1="6.3373" y1="0.6055375" x2="6.644640625" y2="0.623825" layer="94"/>
<rectangle x1="6.82498125" y1="0.6055375" x2="7.114540625" y2="0.623825" layer="94"/>
<rectangle x1="7.47521875" y1="0.6055375" x2="7.800340625" y2="0.623825" layer="94"/>
<rectangle x1="7.927340625" y1="0.6055375" x2="8.23468125" y2="0.623825" layer="94"/>
<rectangle x1="8.5598" y1="0.6055375" x2="8.8138" y2="0.623825" layer="94"/>
<rectangle x1="9.17448125" y1="0.6055375" x2="9.4107" y2="0.623825" layer="94"/>
<rectangle x1="9.77138125" y1="0.6055375" x2="10.02538125" y2="0.623825" layer="94"/>
<rectangle x1="10.386059375" y1="0.6055375" x2="10.619740625" y2="0.623825" layer="94"/>
<rectangle x1="11.000740625" y1="0.6055375" x2="11.343640625" y2="0.623825" layer="94"/>
<rectangle x1="11.470640625" y1="0.6055375" x2="11.813540625" y2="0.623825" layer="94"/>
<rectangle x1="12.194540625" y1="0.6055375" x2="12.42821875" y2="0.623825" layer="94"/>
<rectangle x1="12.80921875" y1="0.6055375" x2="13.15211875" y2="0.623825" layer="94"/>
<rectangle x1="13.27911875" y1="0.6055375" x2="13.62201875" y2="0.623825" layer="94"/>
<rectangle x1="14.0208" y1="0.6055375" x2="14.869159375" y2="0.623825" layer="94"/>
<rectangle x1="15.212059375" y1="0.6055375" x2="15.44828125" y2="0.623825" layer="94"/>
<rectangle x1="15.808959375" y1="0.6055375" x2="16.09851875" y2="0.623825" layer="94"/>
<rectangle x1="16.296640625" y1="0.6055375" x2="16.56841875" y2="0.623825" layer="94"/>
<rectangle x1="16.875759375" y1="0.6055375" x2="17.16531875" y2="0.623825" layer="94"/>
<rectangle x1="17.41931875" y1="0.6055375" x2="17.688559375" y2="0.623825" layer="94"/>
<rectangle x1="17.942559375" y1="0.6055375" x2="18.48611875" y2="0.623825" layer="94"/>
<rectangle x1="5.577840625" y1="0.623825" x2="5.8293" y2="0.641859375" layer="94"/>
<rectangle x1="6.3373" y1="0.623825" x2="6.606540625" y2="0.641859375" layer="94"/>
<rectangle x1="6.842759375" y1="0.623825" x2="7.114540625" y2="0.641859375" layer="94"/>
<rectangle x1="7.457440625" y1="0.623825" x2="7.76478125" y2="0.641859375" layer="94"/>
<rectangle x1="7.9629" y1="0.623825" x2="8.252459375" y2="0.641859375" layer="94"/>
<rectangle x1="8.57758125" y1="0.623825" x2="8.8138" y2="0.641859375" layer="94"/>
<rectangle x1="9.17448125" y1="0.623825" x2="9.42848125" y2="0.641859375" layer="94"/>
<rectangle x1="9.77138125" y1="0.623825" x2="10.02538125" y2="0.641859375" layer="94"/>
<rectangle x1="10.386059375" y1="0.623825" x2="10.619740625" y2="0.641859375" layer="94"/>
<rectangle x1="11.000740625" y1="0.623825" x2="11.30808125" y2="0.641859375" layer="94"/>
<rectangle x1="11.5062" y1="0.623825" x2="11.83131875" y2="0.641859375" layer="94"/>
<rectangle x1="12.194540625" y1="0.623825" x2="12.446" y2="0.641859375" layer="94"/>
<rectangle x1="12.80921875" y1="0.623825" x2="13.116559375" y2="0.641859375" layer="94"/>
<rectangle x1="13.332459375" y1="0.623825" x2="13.6398" y2="0.641859375" layer="94"/>
<rectangle x1="14.0208" y1="0.623825" x2="14.3637" y2="0.641859375" layer="94"/>
<rectangle x1="14.4526" y1="0.623825" x2="14.869159375" y2="0.641859375" layer="94"/>
<rectangle x1="15.212059375" y1="0.623825" x2="15.466059375" y2="0.641859375" layer="94"/>
<rectangle x1="15.808959375" y1="0.623825" x2="16.080740625" y2="0.641859375" layer="94"/>
<rectangle x1="16.31441875" y1="0.623825" x2="16.5862" y2="0.641859375" layer="94"/>
<rectangle x1="16.85798125" y1="0.623825" x2="17.147540625" y2="0.641859375" layer="94"/>
<rectangle x1="17.45488125" y1="0.623825" x2="17.688559375" y2="0.641859375" layer="94"/>
<rectangle x1="18.034" y1="0.623825" x2="18.57501875" y2="0.641859375" layer="94"/>
<rectangle x1="5.577840625" y1="0.641859375" x2="5.8293" y2="0.659890625" layer="94"/>
<rectangle x1="6.3373" y1="0.641859375" x2="6.606540625" y2="0.659890625" layer="94"/>
<rectangle x1="6.860540625" y1="0.641859375" x2="7.13231875" y2="0.659890625" layer="94"/>
<rectangle x1="7.457440625" y1="0.641859375" x2="7.747" y2="0.659890625" layer="94"/>
<rectangle x1="7.98068125" y1="0.641859375" x2="8.252459375" y2="0.659890625" layer="94"/>
<rectangle x1="8.57758125" y1="0.641859375" x2="8.8138" y2="0.659890625" layer="94"/>
<rectangle x1="9.17448125" y1="0.641859375" x2="9.42848125" y2="0.659890625" layer="94"/>
<rectangle x1="9.789159375" y1="0.641859375" x2="10.02538125" y2="0.659890625" layer="94"/>
<rectangle x1="10.386059375" y1="0.641859375" x2="10.640059375" y2="0.659890625" layer="94"/>
<rectangle x1="10.982959375" y1="0.641859375" x2="11.2903" y2="0.659890625" layer="94"/>
<rectangle x1="11.541759375" y1="0.641859375" x2="11.83131875" y2="0.659890625" layer="94"/>
<rectangle x1="12.194540625" y1="0.641859375" x2="12.446" y2="0.659890625" layer="94"/>
<rectangle x1="12.80921875" y1="0.641859375" x2="13.09878125" y2="0.659890625" layer="94"/>
<rectangle x1="13.350240625" y1="0.641859375" x2="13.6398" y2="0.659890625" layer="94"/>
<rectangle x1="14.00048125" y1="0.641859375" x2="14.328140625" y2="0.659890625" layer="94"/>
<rectangle x1="14.50848125" y1="0.641859375" x2="14.869159375" y2="0.659890625" layer="94"/>
<rectangle x1="15.212059375" y1="0.641859375" x2="15.466059375" y2="0.659890625" layer="94"/>
<rectangle x1="15.79118125" y1="0.641859375" x2="16.062959375" y2="0.659890625" layer="94"/>
<rectangle x1="16.334740625" y1="0.641859375" x2="16.5862" y2="0.659890625" layer="94"/>
<rectangle x1="16.85798125" y1="0.641859375" x2="17.129759375" y2="0.659890625" layer="94"/>
<rectangle x1="17.45488125" y1="0.641859375" x2="17.70888125" y2="0.659890625" layer="94"/>
<rectangle x1="18.10511875" y1="0.641859375" x2="18.684240625" y2="0.659890625" layer="94"/>
<rectangle x1="5.577840625" y1="0.659890625" x2="5.8293" y2="0.677925" layer="94"/>
<rectangle x1="6.31951875" y1="0.659890625" x2="6.588759375" y2="0.677925" layer="94"/>
<rectangle x1="6.87831875" y1="0.659890625" x2="7.13231875" y2="0.677925" layer="94"/>
<rectangle x1="7.457440625" y1="0.659890625" x2="7.72921875" y2="0.677925" layer="94"/>
<rectangle x1="7.998459375" y1="0.659890625" x2="8.270240625" y2="0.677925" layer="94"/>
<rectangle x1="8.57758125" y1="0.659890625" x2="8.8138" y2="0.677925" layer="94"/>
<rectangle x1="9.17448125" y1="0.659890625" x2="9.42848125" y2="0.677925" layer="94"/>
<rectangle x1="9.789159375" y1="0.659890625" x2="10.02538125" y2="0.677925" layer="94"/>
<rectangle x1="10.386059375" y1="0.659890625" x2="10.640059375" y2="0.677925" layer="94"/>
<rectangle x1="10.982959375" y1="0.659890625" x2="11.27251875" y2="0.677925" layer="94"/>
<rectangle x1="11.56208125" y1="0.659890625" x2="11.8491" y2="0.677925" layer="94"/>
<rectangle x1="12.194540625" y1="0.659890625" x2="12.446" y2="0.677925" layer="94"/>
<rectangle x1="12.791440625" y1="0.659890625" x2="13.078459375" y2="0.677925" layer="94"/>
<rectangle x1="13.36801875" y1="0.659890625" x2="13.65758125" y2="0.677925" layer="94"/>
<rectangle x1="14.00048125" y1="0.659890625" x2="14.290040625" y2="0.677925" layer="94"/>
<rectangle x1="14.526259375" y1="0.659890625" x2="14.869159375" y2="0.677925" layer="94"/>
<rectangle x1="15.212059375" y1="0.659890625" x2="15.466059375" y2="0.677925" layer="94"/>
<rectangle x1="15.79118125" y1="0.659890625" x2="16.04518125" y2="0.677925" layer="94"/>
<rectangle x1="16.35251875" y1="0.659890625" x2="16.60398125" y2="0.677925" layer="94"/>
<rectangle x1="16.85798125" y1="0.659890625" x2="17.11198125" y2="0.677925" layer="94"/>
<rectangle x1="17.472659375" y1="0.659890625" x2="17.70888125" y2="0.677925" layer="94"/>
<rectangle x1="18.17878125" y1="0.659890625" x2="18.773140625" y2="0.677925" layer="94"/>
<rectangle x1="5.577840625" y1="0.677925" x2="5.84708125" y2="0.695959375" layer="94"/>
<rectangle x1="6.31951875" y1="0.677925" x2="6.57098125" y2="0.695959375" layer="94"/>
<rectangle x1="6.8961" y1="0.677925" x2="7.1501" y2="0.695959375" layer="94"/>
<rectangle x1="7.457440625" y1="0.677925" x2="7.72921875" y2="0.695959375" layer="94"/>
<rectangle x1="8.01878125" y1="0.677925" x2="8.270240625" y2="0.695959375" layer="94"/>
<rectangle x1="8.57758125" y1="0.677925" x2="8.83158125" y2="0.695959375" layer="94"/>
<rectangle x1="9.192259375" y1="0.677925" x2="9.42848125" y2="0.695959375" layer="94"/>
<rectangle x1="9.789159375" y1="0.677925" x2="10.02538125" y2="0.695959375" layer="94"/>
<rectangle x1="10.386059375" y1="0.677925" x2="10.640059375" y2="0.695959375" layer="94"/>
<rectangle x1="10.982959375" y1="0.677925" x2="11.254740625" y2="0.695959375" layer="94"/>
<rectangle x1="11.579859375" y1="0.677925" x2="11.8491" y2="0.695959375" layer="94"/>
<rectangle x1="12.21231875" y1="0.677925" x2="12.446" y2="0.695959375" layer="94"/>
<rectangle x1="12.791440625" y1="0.677925" x2="13.06068125" y2="0.695959375" layer="94"/>
<rectangle x1="13.40611875" y1="0.677925" x2="13.675359375" y2="0.695959375" layer="94"/>
<rectangle x1="14.00048125" y1="0.677925" x2="14.290040625" y2="0.695959375" layer="94"/>
<rectangle x1="14.544040625" y1="0.677925" x2="14.869159375" y2="0.695959375" layer="94"/>
<rectangle x1="15.229840625" y1="0.677925" x2="15.466059375" y2="0.695959375" layer="94"/>
<rectangle x1="15.79118125" y1="0.677925" x2="16.04518125" y2="0.695959375" layer="94"/>
<rectangle x1="16.35251875" y1="0.677925" x2="16.60398125" y2="0.695959375" layer="94"/>
<rectangle x1="16.85798125" y1="0.677925" x2="17.11198125" y2="0.695959375" layer="94"/>
<rectangle x1="17.472659375" y1="0.677925" x2="17.726659375" y2="0.695959375" layer="94"/>
<rectangle x1="18.26768125" y1="0.677925" x2="18.86458125" y2="0.695959375" layer="94"/>
<rectangle x1="5.577840625" y1="0.695959375" x2="5.84708125" y2="0.71399375" layer="94"/>
<rectangle x1="6.31951875" y1="0.695959375" x2="6.57098125" y2="0.71399375" layer="94"/>
<rectangle x1="6.8961" y1="0.695959375" x2="7.058659375" y2="0.71399375" layer="94"/>
<rectangle x1="7.439659375" y1="0.695959375" x2="7.711440625" y2="0.71399375" layer="94"/>
<rectangle x1="8.036559375" y1="0.695959375" x2="8.28801875" y2="0.71399375" layer="94"/>
<rectangle x1="8.57758125" y1="0.695959375" x2="8.83158125" y2="0.71399375" layer="94"/>
<rectangle x1="9.192259375" y1="0.695959375" x2="9.42848125" y2="0.71399375" layer="94"/>
<rectangle x1="9.789159375" y1="0.695959375" x2="10.02538125" y2="0.71399375" layer="94"/>
<rectangle x1="10.403840625" y1="0.695959375" x2="10.640059375" y2="0.71399375" layer="94"/>
<rectangle x1="10.982959375" y1="0.695959375" x2="11.254740625" y2="0.71399375" layer="94"/>
<rectangle x1="11.597640625" y1="0.695959375" x2="11.86941875" y2="0.71399375" layer="94"/>
<rectangle x1="12.21231875" y1="0.695959375" x2="12.446" y2="0.71399375" layer="94"/>
<rectangle x1="12.791440625" y1="0.695959375" x2="13.06068125" y2="0.71399375" layer="94"/>
<rectangle x1="13.40611875" y1="0.695959375" x2="13.675359375" y2="0.71399375" layer="94"/>
<rectangle x1="14.00048125" y1="0.695959375" x2="14.272259375" y2="0.71399375" layer="94"/>
<rectangle x1="14.5796" y1="0.695959375" x2="14.869159375" y2="0.71399375" layer="94"/>
<rectangle x1="15.229840625" y1="0.695959375" x2="15.466059375" y2="0.71399375" layer="94"/>
<rectangle x1="15.79118125" y1="0.695959375" x2="16.04518125" y2="0.71399375" layer="94"/>
<rectangle x1="16.3703" y1="0.695959375" x2="16.532859375" y2="0.71399375" layer="94"/>
<rectangle x1="16.893540625" y1="0.695959375" x2="17.11198125" y2="0.71399375" layer="94"/>
<rectangle x1="17.472659375" y1="0.695959375" x2="17.726659375" y2="0.71399375" layer="94"/>
<rectangle x1="18.341340625" y1="0.695959375" x2="18.95601875" y2="0.71399375" layer="94"/>
<rectangle x1="5.59561875" y1="0.71399375" x2="5.84708125" y2="0.73228125" layer="94"/>
<rectangle x1="6.31951875" y1="0.71399375" x2="6.57098125" y2="0.73228125" layer="94"/>
<rectangle x1="6.91388125" y1="0.71399375" x2="6.969759375" y2="0.73228125" layer="94"/>
<rectangle x1="7.439659375" y1="0.71399375" x2="7.711440625" y2="0.73228125" layer="94"/>
<rectangle x1="8.036559375" y1="0.71399375" x2="8.28801875" y2="0.73228125" layer="94"/>
<rectangle x1="8.57758125" y1="0.71399375" x2="8.83158125" y2="0.73228125" layer="94"/>
<rectangle x1="9.192259375" y1="0.71399375" x2="9.42848125" y2="0.73228125" layer="94"/>
<rectangle x1="9.789159375" y1="0.71399375" x2="10.043159375" y2="0.73228125" layer="94"/>
<rectangle x1="10.403840625" y1="0.71399375" x2="10.640059375" y2="0.73228125" layer="94"/>
<rectangle x1="10.96518125" y1="0.71399375" x2="11.23441875" y2="0.73228125" layer="94"/>
<rectangle x1="11.61541875" y1="0.71399375" x2="11.8872" y2="0.73228125" layer="94"/>
<rectangle x1="12.21231875" y1="0.71399375" x2="12.446" y2="0.73228125" layer="94"/>
<rectangle x1="12.791440625" y1="0.71399375" x2="13.0429" y2="0.73228125" layer="94"/>
<rectangle x1="13.4239" y1="0.71399375" x2="13.693140625" y2="0.73228125" layer="94"/>
<rectangle x1="13.9827" y1="0.71399375" x2="14.25448125" y2="0.73228125" layer="94"/>
<rectangle x1="14.5796" y1="0.71399375" x2="14.869159375" y2="0.73228125" layer="94"/>
<rectangle x1="15.229840625" y1="0.71399375" x2="15.466059375" y2="0.73228125" layer="94"/>
<rectangle x1="15.79118125" y1="0.71399375" x2="16.0274" y2="0.73228125" layer="94"/>
<rectangle x1="16.3703" y1="0.71399375" x2="16.423640625" y2="0.73228125" layer="94"/>
<rectangle x1="17.002759375" y1="0.71399375" x2="17.11198125" y2="0.73228125" layer="94"/>
<rectangle x1="17.472659375" y1="0.71399375" x2="17.726659375" y2="0.73228125" layer="94"/>
<rectangle x1="18.412459375" y1="0.71399375" x2="19.04491875" y2="0.73228125" layer="94"/>
<rectangle x1="5.59561875" y1="0.73228125" x2="5.84708125" y2="0.750315625" layer="94"/>
<rectangle x1="6.31951875" y1="0.73228125" x2="6.57098125" y2="0.750315625" layer="94"/>
<rectangle x1="7.439659375" y1="0.73228125" x2="7.711440625" y2="0.750315625" layer="94"/>
<rectangle x1="8.054340625" y1="0.73228125" x2="8.3058" y2="0.750315625" layer="94"/>
<rectangle x1="8.595359375" y1="0.73228125" x2="8.83158125" y2="0.750315625" layer="94"/>
<rectangle x1="9.192259375" y1="0.73228125" x2="9.446259375" y2="0.750315625" layer="94"/>
<rectangle x1="9.789159375" y1="0.73228125" x2="10.043159375" y2="0.750315625" layer="94"/>
<rectangle x1="10.403840625" y1="0.73228125" x2="10.640059375" y2="0.750315625" layer="94"/>
<rectangle x1="10.96518125" y1="0.73228125" x2="11.23441875" y2="0.750315625" layer="94"/>
<rectangle x1="11.6332" y1="0.73228125" x2="11.8872" y2="0.750315625" layer="94"/>
<rectangle x1="12.21231875" y1="0.73228125" x2="12.446" y2="0.750315625" layer="94"/>
<rectangle x1="12.791440625" y1="0.73228125" x2="13.0429" y2="0.750315625" layer="94"/>
<rectangle x1="13.44168125" y1="0.73228125" x2="13.693140625" y2="0.750315625" layer="94"/>
<rectangle x1="13.9827" y1="0.73228125" x2="14.25448125" y2="0.750315625" layer="94"/>
<rectangle x1="14.59738125" y1="0.73228125" x2="14.886940625" y2="0.750315625" layer="94"/>
<rectangle x1="15.229840625" y1="0.73228125" x2="15.483840625" y2="0.750315625" layer="94"/>
<rectangle x1="15.79118125" y1="0.73228125" x2="16.0274" y2="0.750315625" layer="94"/>
<rectangle x1="17.472659375" y1="0.73228125" x2="17.726659375" y2="0.750315625" layer="94"/>
<rectangle x1="18.48611875" y1="0.73228125" x2="19.136359375" y2="0.750315625" layer="94"/>
<rectangle x1="5.59561875" y1="0.750315625" x2="5.84708125" y2="0.76835" layer="94"/>
<rectangle x1="6.31951875" y1="0.750315625" x2="6.5532" y2="0.76835" layer="94"/>
<rectangle x1="7.439659375" y1="0.750315625" x2="7.69111875" y2="0.76835" layer="94"/>
<rectangle x1="8.054340625" y1="0.750315625" x2="8.28801875" y2="0.76835" layer="94"/>
<rectangle x1="8.595359375" y1="0.750315625" x2="8.83158125" y2="0.76835" layer="94"/>
<rectangle x1="9.192259375" y1="0.750315625" x2="9.446259375" y2="0.76835" layer="94"/>
<rectangle x1="9.789159375" y1="0.750315625" x2="10.043159375" y2="0.76835" layer="94"/>
<rectangle x1="10.403840625" y1="0.750315625" x2="10.657840625" y2="0.76835" layer="94"/>
<rectangle x1="10.96518125" y1="0.750315625" x2="11.216640625" y2="0.76835" layer="94"/>
<rectangle x1="11.6332" y1="0.750315625" x2="11.8872" y2="0.76835" layer="94"/>
<rectangle x1="12.21231875" y1="0.750315625" x2="12.46378125" y2="0.76835" layer="94"/>
<rectangle x1="12.791440625" y1="0.750315625" x2="13.0429" y2="0.76835" layer="94"/>
<rectangle x1="13.44168125" y1="0.750315625" x2="13.713459375" y2="0.76835" layer="94"/>
<rectangle x1="13.9827" y1="0.750315625" x2="14.25448125" y2="0.76835" layer="94"/>
<rectangle x1="14.615159375" y1="0.750315625" x2="14.886940625" y2="0.76835" layer="94"/>
<rectangle x1="15.229840625" y1="0.750315625" x2="15.483840625" y2="0.76835" layer="94"/>
<rectangle x1="15.7734" y1="0.750315625" x2="16.0274" y2="0.76835" layer="94"/>
<rectangle x1="17.45488125" y1="0.750315625" x2="17.726659375" y2="0.76835" layer="94"/>
<rectangle x1="18.57501875" y1="0.750315625" x2="19.225259375" y2="0.76835" layer="94"/>
<rectangle x1="5.59561875" y1="0.76835" x2="5.84708125" y2="0.786384375" layer="94"/>
<rectangle x1="6.31951875" y1="0.76835" x2="6.5532" y2="0.786384375" layer="94"/>
<rectangle x1="7.439659375" y1="0.76835" x2="7.69111875" y2="0.786384375" layer="94"/>
<rectangle x1="8.07211875" y1="0.76835" x2="8.19911875" y2="0.786384375" layer="94"/>
<rectangle x1="8.595359375" y1="0.76835" x2="8.83158125" y2="0.786384375" layer="94"/>
<rectangle x1="9.192259375" y1="0.76835" x2="9.446259375" y2="0.786384375" layer="94"/>
<rectangle x1="9.806940625" y1="0.76835" x2="10.043159375" y2="0.786384375" layer="94"/>
<rectangle x1="10.403840625" y1="0.76835" x2="10.657840625" y2="0.786384375" layer="94"/>
<rectangle x1="10.96518125" y1="0.76835" x2="11.216640625" y2="0.786384375" layer="94"/>
<rectangle x1="11.65098125" y1="0.76835" x2="11.90498125" y2="0.786384375" layer="94"/>
<rectangle x1="12.21231875" y1="0.76835" x2="12.46378125" y2="0.786384375" layer="94"/>
<rectangle x1="12.77111875" y1="0.76835" x2="13.0429" y2="0.786384375" layer="94"/>
<rectangle x1="13.459459375" y1="0.76835" x2="13.713459375" y2="0.786384375" layer="94"/>
<rectangle x1="13.9827" y1="0.76835" x2="14.25448125" y2="0.786384375" layer="94"/>
<rectangle x1="14.615159375" y1="0.76835" x2="14.886940625" y2="0.786384375" layer="94"/>
<rectangle x1="15.229840625" y1="0.76835" x2="15.483840625" y2="0.786384375" layer="94"/>
<rectangle x1="15.7734" y1="0.76835" x2="16.0274" y2="0.786384375" layer="94"/>
<rectangle x1="17.401540625" y1="0.76835" x2="17.726659375" y2="0.786384375" layer="94"/>
<rectangle x1="18.64868125" y1="0.76835" x2="19.29891875" y2="0.786384375" layer="94"/>
<rectangle x1="5.59561875" y1="0.786384375" x2="5.84708125" y2="0.80441875" layer="94"/>
<rectangle x1="6.31951875" y1="0.786384375" x2="6.5532" y2="0.80441875" layer="94"/>
<rectangle x1="7.439659375" y1="0.786384375" x2="7.69111875" y2="0.80441875" layer="94"/>
<rectangle x1="8.07211875" y1="0.786384375" x2="8.0899" y2="0.80441875" layer="94"/>
<rectangle x1="8.595359375" y1="0.786384375" x2="8.83158125" y2="0.80441875" layer="94"/>
<rectangle x1="9.210040625" y1="0.786384375" x2="9.446259375" y2="0.80441875" layer="94"/>
<rectangle x1="9.806940625" y1="0.786384375" x2="10.043159375" y2="0.80441875" layer="94"/>
<rectangle x1="10.403840625" y1="0.786384375" x2="10.657840625" y2="0.80441875" layer="94"/>
<rectangle x1="10.96518125" y1="0.786384375" x2="11.216640625" y2="0.80441875" layer="94"/>
<rectangle x1="11.65098125" y1="0.786384375" x2="11.90498125" y2="0.80441875" layer="94"/>
<rectangle x1="12.21231875" y1="0.786384375" x2="12.46378125" y2="0.80441875" layer="94"/>
<rectangle x1="12.77111875" y1="0.786384375" x2="13.02511875" y2="0.80441875" layer="94"/>
<rectangle x1="13.459459375" y1="0.786384375" x2="13.713459375" y2="0.80441875" layer="94"/>
<rectangle x1="13.9827" y1="0.786384375" x2="14.2367" y2="0.80441875" layer="94"/>
<rectangle x1="14.63548125" y1="0.786384375" x2="14.886940625" y2="0.80441875" layer="94"/>
<rectangle x1="15.250159375" y1="0.786384375" x2="15.483840625" y2="0.80441875" layer="94"/>
<rectangle x1="15.7734" y1="0.786384375" x2="16.0274" y2="0.80441875" layer="94"/>
<rectangle x1="17.345659375" y1="0.786384375" x2="17.726659375" y2="0.80441875" layer="94"/>
<rectangle x1="18.7198" y1="0.786384375" x2="19.38781875" y2="0.80441875" layer="94"/>
<rectangle x1="5.59561875" y1="0.80441875" x2="5.8674" y2="0.822453125" layer="94"/>
<rectangle x1="6.31951875" y1="0.80441875" x2="6.5532" y2="0.822453125" layer="94"/>
<rectangle x1="7.439659375" y1="0.80441875" x2="7.69111875" y2="0.822453125" layer="94"/>
<rectangle x1="8.595359375" y1="0.80441875" x2="8.849359375" y2="0.822453125" layer="94"/>
<rectangle x1="9.210040625" y1="0.80441875" x2="9.446259375" y2="0.822453125" layer="94"/>
<rectangle x1="9.806940625" y1="0.80441875" x2="10.043159375" y2="0.822453125" layer="94"/>
<rectangle x1="10.42161875" y1="0.80441875" x2="10.657840625" y2="0.822453125" layer="94"/>
<rectangle x1="10.96518125" y1="0.80441875" x2="11.216640625" y2="0.822453125" layer="94"/>
<rectangle x1="11.668759375" y1="0.80441875" x2="11.922759375" y2="0.822453125" layer="94"/>
<rectangle x1="12.2301" y1="0.80441875" x2="12.46378125" y2="0.822453125" layer="94"/>
<rectangle x1="12.77111875" y1="0.80441875" x2="13.02511875" y2="0.822453125" layer="94"/>
<rectangle x1="13.477240625" y1="0.80441875" x2="13.731240625" y2="0.822453125" layer="94"/>
<rectangle x1="13.9827" y1="0.80441875" x2="14.2367" y2="0.822453125" layer="94"/>
<rectangle x1="14.63548125" y1="0.80441875" x2="14.886940625" y2="0.822453125" layer="94"/>
<rectangle x1="15.250159375" y1="0.80441875" x2="15.483840625" y2="0.822453125" layer="94"/>
<rectangle x1="15.7734" y1="0.80441875" x2="16.0274" y2="0.822453125" layer="94"/>
<rectangle x1="17.274540625" y1="0.80441875" x2="17.726659375" y2="0.822453125" layer="94"/>
<rectangle x1="18.793459375" y1="0.80441875" x2="19.46148125" y2="0.822453125" layer="94"/>
<rectangle x1="5.6134" y1="0.822453125" x2="5.8674" y2="0.840740625" layer="94"/>
<rectangle x1="6.31951875" y1="0.822453125" x2="6.57098125" y2="0.840740625" layer="94"/>
<rectangle x1="7.439659375" y1="0.822453125" x2="7.69111875" y2="0.840740625" layer="94"/>
<rectangle x1="8.595359375" y1="0.822453125" x2="8.849359375" y2="0.840740625" layer="94"/>
<rectangle x1="9.210040625" y1="0.822453125" x2="9.446259375" y2="0.840740625" layer="94"/>
<rectangle x1="9.806940625" y1="0.822453125" x2="10.060940625" y2="0.840740625" layer="94"/>
<rectangle x1="10.42161875" y1="0.822453125" x2="10.657840625" y2="0.840740625" layer="94"/>
<rectangle x1="10.96518125" y1="0.822453125" x2="11.216640625" y2="0.840740625" layer="94"/>
<rectangle x1="11.668759375" y1="0.822453125" x2="11.922759375" y2="0.840740625" layer="94"/>
<rectangle x1="12.2301" y1="0.822453125" x2="12.46378125" y2="0.840740625" layer="94"/>
<rectangle x1="12.77111875" y1="0.822453125" x2="13.02511875" y2="0.840740625" layer="94"/>
<rectangle x1="13.477240625" y1="0.822453125" x2="13.731240625" y2="0.840740625" layer="94"/>
<rectangle x1="13.9827" y1="0.822453125" x2="14.2367" y2="0.840740625" layer="94"/>
<rectangle x1="14.653259375" y1="0.822453125" x2="14.886940625" y2="0.840740625" layer="94"/>
<rectangle x1="15.250159375" y1="0.822453125" x2="15.483840625" y2="0.840740625" layer="94"/>
<rectangle x1="15.79118125" y1="0.822453125" x2="16.0274" y2="0.840740625" layer="94"/>
<rectangle x1="17.218659375" y1="0.822453125" x2="17.726659375" y2="0.840740625" layer="94"/>
<rectangle x1="18.86458125" y1="0.822453125" x2="19.55291875" y2="0.840740625" layer="94"/>
<rectangle x1="5.6134" y1="0.840740625" x2="5.8674" y2="0.858775" layer="94"/>
<rectangle x1="6.31951875" y1="0.840740625" x2="7.185659375" y2="0.858775" layer="94"/>
<rectangle x1="7.439659375" y1="0.840740625" x2="7.69111875" y2="0.858775" layer="94"/>
<rectangle x1="8.595359375" y1="0.840740625" x2="8.849359375" y2="0.858775" layer="94"/>
<rectangle x1="9.210040625" y1="0.840740625" x2="9.464040625" y2="0.858775" layer="94"/>
<rectangle x1="9.806940625" y1="0.840740625" x2="10.060940625" y2="0.858775" layer="94"/>
<rectangle x1="10.42161875" y1="0.840740625" x2="10.657840625" y2="0.858775" layer="94"/>
<rectangle x1="10.96518125" y1="0.840740625" x2="11.216640625" y2="0.858775" layer="94"/>
<rectangle x1="11.668759375" y1="0.840740625" x2="11.922759375" y2="0.858775" layer="94"/>
<rectangle x1="12.2301" y1="0.840740625" x2="12.46378125" y2="0.858775" layer="94"/>
<rectangle x1="12.77111875" y1="0.840740625" x2="13.02511875" y2="0.858775" layer="94"/>
<rectangle x1="13.477240625" y1="0.840740625" x2="13.731240625" y2="0.858775" layer="94"/>
<rectangle x1="13.9827" y1="0.840740625" x2="14.2367" y2="0.858775" layer="94"/>
<rectangle x1="14.653259375" y1="0.840740625" x2="14.90471875" y2="0.858775" layer="94"/>
<rectangle x1="15.250159375" y1="0.840740625" x2="15.483840625" y2="0.858775" layer="94"/>
<rectangle x1="15.79118125" y1="0.840740625" x2="16.659859375" y2="0.858775" layer="94"/>
<rectangle x1="17.16531875" y1="0.840740625" x2="17.726659375" y2="0.858775" layer="94"/>
<rectangle x1="18.938240625" y1="0.840740625" x2="19.624040625" y2="0.858775" layer="94"/>
<rectangle x1="5.6134" y1="0.858775" x2="5.8674" y2="0.876809375" layer="94"/>
<rectangle x1="6.31951875" y1="0.858775" x2="7.185659375" y2="0.876809375" layer="94"/>
<rectangle x1="7.439659375" y1="0.858775" x2="7.69111875" y2="0.876809375" layer="94"/>
<rectangle x1="8.613140625" y1="0.858775" x2="8.849359375" y2="0.876809375" layer="94"/>
<rectangle x1="9.210040625" y1="0.858775" x2="9.464040625" y2="0.876809375" layer="94"/>
<rectangle x1="9.806940625" y1="0.858775" x2="10.060940625" y2="0.876809375" layer="94"/>
<rectangle x1="10.42161875" y1="0.858775" x2="10.657840625" y2="0.876809375" layer="94"/>
<rectangle x1="10.96518125" y1="0.858775" x2="11.216640625" y2="0.876809375" layer="94"/>
<rectangle x1="11.668759375" y1="0.858775" x2="11.922759375" y2="0.876809375" layer="94"/>
<rectangle x1="12.2301" y1="0.858775" x2="12.4841" y2="0.876809375" layer="94"/>
<rectangle x1="12.791440625" y1="0.858775" x2="13.02511875" y2="0.876809375" layer="94"/>
<rectangle x1="13.49501875" y1="0.858775" x2="13.731240625" y2="0.876809375" layer="94"/>
<rectangle x1="13.9827" y1="0.858775" x2="14.2367" y2="0.876809375" layer="94"/>
<rectangle x1="14.653259375" y1="0.858775" x2="14.90471875" y2="0.876809375" layer="94"/>
<rectangle x1="15.250159375" y1="0.858775" x2="15.50161875" y2="0.876809375" layer="94"/>
<rectangle x1="15.79118125" y1="0.858775" x2="16.659859375" y2="0.876809375" layer="94"/>
<rectangle x1="17.11198125" y1="0.858775" x2="17.726659375" y2="0.876809375" layer="94"/>
<rectangle x1="18.99158125" y1="0.858775" x2="19.695159375" y2="0.876809375" layer="94"/>
<rectangle x1="5.6134" y1="0.876809375" x2="5.8674" y2="0.894840625" layer="94"/>
<rectangle x1="6.31951875" y1="0.876809375" x2="7.185659375" y2="0.894840625" layer="94"/>
<rectangle x1="7.457440625" y1="0.876809375" x2="7.69111875" y2="0.894840625" layer="94"/>
<rectangle x1="8.613140625" y1="0.876809375" x2="8.849359375" y2="0.894840625" layer="94"/>
<rectangle x1="9.210040625" y1="0.876809375" x2="9.464040625" y2="0.894840625" layer="94"/>
<rectangle x1="9.82471875" y1="0.876809375" x2="10.060940625" y2="0.894840625" layer="94"/>
<rectangle x1="10.42161875" y1="0.876809375" x2="10.67561875" y2="0.894840625" layer="94"/>
<rectangle x1="10.96518125" y1="0.876809375" x2="11.216640625" y2="0.894840625" layer="94"/>
<rectangle x1="11.686540625" y1="0.876809375" x2="11.922759375" y2="0.894840625" layer="94"/>
<rectangle x1="12.2301" y1="0.876809375" x2="12.4841" y2="0.894840625" layer="94"/>
<rectangle x1="12.791440625" y1="0.876809375" x2="13.02511875" y2="0.894840625" layer="94"/>
<rectangle x1="13.49501875" y1="0.876809375" x2="13.74901875" y2="0.894840625" layer="94"/>
<rectangle x1="14.00048125" y1="0.876809375" x2="14.2367" y2="0.894840625" layer="94"/>
<rectangle x1="14.653259375" y1="0.876809375" x2="14.90471875" y2="0.894840625" layer="94"/>
<rectangle x1="15.250159375" y1="0.876809375" x2="15.50161875" y2="0.894840625" layer="94"/>
<rectangle x1="15.79118125" y1="0.876809375" x2="16.659859375" y2="0.894840625" layer="94"/>
<rectangle x1="17.07388125" y1="0.876809375" x2="17.70888125" y2="0.894840625" layer="94"/>
<rectangle x1="19.0627" y1="0.876809375" x2="19.7866" y2="0.894840625" layer="94"/>
<rectangle x1="5.6134" y1="0.894840625" x2="5.8674" y2="0.912875" layer="94"/>
<rectangle x1="6.31951875" y1="0.894840625" x2="7.203440625" y2="0.912875" layer="94"/>
<rectangle x1="7.457440625" y1="0.894840625" x2="7.711440625" y2="0.912875" layer="94"/>
<rectangle x1="8.613140625" y1="0.894840625" x2="8.849359375" y2="0.912875" layer="94"/>
<rectangle x1="9.22781875" y1="0.894840625" x2="9.464040625" y2="0.912875" layer="94"/>
<rectangle x1="9.82471875" y1="0.894840625" x2="10.060940625" y2="0.912875" layer="94"/>
<rectangle x1="10.42161875" y1="0.894840625" x2="10.67561875" y2="0.912875" layer="94"/>
<rectangle x1="10.982959375" y1="0.894840625" x2="11.216640625" y2="0.912875" layer="94"/>
<rectangle x1="11.686540625" y1="0.894840625" x2="11.940540625" y2="0.912875" layer="94"/>
<rectangle x1="12.2301" y1="0.894840625" x2="12.4841" y2="0.912875" layer="94"/>
<rectangle x1="12.791440625" y1="0.894840625" x2="13.0429" y2="0.912875" layer="94"/>
<rectangle x1="13.49501875" y1="0.894840625" x2="13.74901875" y2="0.912875" layer="94"/>
<rectangle x1="14.00048125" y1="0.894840625" x2="14.25448125" y2="0.912875" layer="94"/>
<rectangle x1="14.671040625" y1="0.894840625" x2="14.90471875" y2="0.912875" layer="94"/>
<rectangle x1="15.250159375" y1="0.894840625" x2="15.50161875" y2="0.912875" layer="94"/>
<rectangle x1="15.79118125" y1="0.894840625" x2="16.659859375" y2="0.912875" layer="94"/>
<rectangle x1="17.0561" y1="0.894840625" x2="17.70888125" y2="0.912875" layer="94"/>
<rectangle x1="19.136359375" y1="0.894840625" x2="19.860259375" y2="0.912875" layer="94"/>
<rectangle x1="5.6134" y1="0.912875" x2="5.88518125" y2="0.930909375" layer="94"/>
<rectangle x1="6.31951875" y1="0.912875" x2="7.203440625" y2="0.930909375" layer="94"/>
<rectangle x1="7.457440625" y1="0.912875" x2="7.711440625" y2="0.930909375" layer="94"/>
<rectangle x1="8.613140625" y1="0.912875" x2="8.867140625" y2="0.930909375" layer="94"/>
<rectangle x1="9.22781875" y1="0.912875" x2="9.464040625" y2="0.930909375" layer="94"/>
<rectangle x1="9.82471875" y1="0.912875" x2="10.060940625" y2="0.930909375" layer="94"/>
<rectangle x1="10.42161875" y1="0.912875" x2="10.67561875" y2="0.930909375" layer="94"/>
<rectangle x1="10.982959375" y1="0.912875" x2="11.216640625" y2="0.930909375" layer="94"/>
<rectangle x1="11.686540625" y1="0.912875" x2="11.940540625" y2="0.930909375" layer="94"/>
<rectangle x1="12.24788125" y1="0.912875" x2="12.4841" y2="0.930909375" layer="94"/>
<rectangle x1="12.791440625" y1="0.912875" x2="13.0429" y2="0.930909375" layer="94"/>
<rectangle x1="13.49501875" y1="0.912875" x2="13.74901875" y2="0.930909375" layer="94"/>
<rectangle x1="14.00048125" y1="0.912875" x2="14.25448125" y2="0.930909375" layer="94"/>
<rectangle x1="14.671040625" y1="0.912875" x2="14.90471875" y2="0.930909375" layer="94"/>
<rectangle x1="15.267940625" y1="0.912875" x2="15.50161875" y2="0.930909375" layer="94"/>
<rectangle x1="15.79118125" y1="0.912875" x2="16.659859375" y2="0.930909375" layer="94"/>
<rectangle x1="17.020540625" y1="0.912875" x2="17.688559375" y2="0.930909375" layer="94"/>
<rectangle x1="19.20748125" y1="0.912875" x2="19.93138125" y2="0.930909375" layer="94"/>
<rectangle x1="5.63118125" y1="0.930909375" x2="5.88518125" y2="0.949196875" layer="94"/>
<rectangle x1="6.3373" y1="0.930909375" x2="7.203440625" y2="0.949196875" layer="94"/>
<rectangle x1="7.457440625" y1="0.930909375" x2="7.711440625" y2="0.949196875" layer="94"/>
<rectangle x1="8.613140625" y1="0.930909375" x2="8.867140625" y2="0.949196875" layer="94"/>
<rectangle x1="9.22781875" y1="0.930909375" x2="9.464040625" y2="0.949196875" layer="94"/>
<rectangle x1="9.82471875" y1="0.930909375" x2="10.07871875" y2="0.949196875" layer="94"/>
<rectangle x1="10.4394" y1="0.930909375" x2="10.67561875" y2="0.949196875" layer="94"/>
<rectangle x1="10.982959375" y1="0.930909375" x2="11.23441875" y2="0.949196875" layer="94"/>
<rectangle x1="11.686540625" y1="0.930909375" x2="11.940540625" y2="0.949196875" layer="94"/>
<rectangle x1="12.24788125" y1="0.930909375" x2="12.4841" y2="0.949196875" layer="94"/>
<rectangle x1="12.791440625" y1="0.930909375" x2="13.0429" y2="0.949196875" layer="94"/>
<rectangle x1="13.49501875" y1="0.930909375" x2="13.74901875" y2="0.949196875" layer="94"/>
<rectangle x1="14.00048125" y1="0.930909375" x2="14.25448125" y2="0.949196875" layer="94"/>
<rectangle x1="14.671040625" y1="0.930909375" x2="14.90471875" y2="0.949196875" layer="94"/>
<rectangle x1="15.267940625" y1="0.930909375" x2="15.50161875" y2="0.949196875" layer="94"/>
<rectangle x1="15.79118125" y1="0.930909375" x2="16.659859375" y2="0.949196875" layer="94"/>
<rectangle x1="17.002759375" y1="0.930909375" x2="17.67078125" y2="0.949196875" layer="94"/>
<rectangle x1="19.263359375" y1="0.930909375" x2="20.0025" y2="0.949196875" layer="94"/>
<rectangle x1="5.63118125" y1="0.949196875" x2="5.88518125" y2="0.96723125" layer="94"/>
<rectangle x1="6.3373" y1="0.949196875" x2="7.203440625" y2="0.96723125" layer="94"/>
<rectangle x1="7.457440625" y1="0.949196875" x2="7.711440625" y2="0.96723125" layer="94"/>
<rectangle x1="8.613140625" y1="0.949196875" x2="8.867140625" y2="0.96723125" layer="94"/>
<rectangle x1="9.22781875" y1="0.949196875" x2="9.464040625" y2="0.96723125" layer="94"/>
<rectangle x1="9.82471875" y1="0.949196875" x2="10.07871875" y2="0.96723125" layer="94"/>
<rectangle x1="10.4394" y1="0.949196875" x2="10.67561875" y2="0.96723125" layer="94"/>
<rectangle x1="10.982959375" y1="0.949196875" x2="11.23441875" y2="0.96723125" layer="94"/>
<rectangle x1="11.686540625" y1="0.949196875" x2="11.940540625" y2="0.96723125" layer="94"/>
<rectangle x1="12.24788125" y1="0.949196875" x2="12.4841" y2="0.96723125" layer="94"/>
<rectangle x1="12.791440625" y1="0.949196875" x2="13.0429" y2="0.96723125" layer="94"/>
<rectangle x1="13.5128" y1="0.949196875" x2="13.74901875" y2="0.96723125" layer="94"/>
<rectangle x1="14.00048125" y1="0.949196875" x2="14.25448125" y2="0.96723125" layer="94"/>
<rectangle x1="14.671040625" y1="0.949196875" x2="14.90471875" y2="0.96723125" layer="94"/>
<rectangle x1="15.267940625" y1="0.949196875" x2="15.50161875" y2="0.96723125" layer="94"/>
<rectangle x1="15.808959375" y1="0.949196875" x2="16.659859375" y2="0.96723125" layer="94"/>
<rectangle x1="16.98498125" y1="0.949196875" x2="17.653" y2="0.96723125" layer="94"/>
<rectangle x1="19.33448125" y1="0.949196875" x2="20.076159375" y2="0.96723125" layer="94"/>
<rectangle x1="5.63118125" y1="0.96723125" x2="5.88518125" y2="0.985265625" layer="94"/>
<rectangle x1="6.3373" y1="0.96723125" x2="7.203440625" y2="0.985265625" layer="94"/>
<rectangle x1="7.457440625" y1="0.96723125" x2="7.711440625" y2="0.985265625" layer="94"/>
<rectangle x1="8.633459375" y1="0.96723125" x2="8.867140625" y2="0.985265625" layer="94"/>
<rectangle x1="9.22781875" y1="0.96723125" x2="9.48181875" y2="0.985265625" layer="94"/>
<rectangle x1="9.82471875" y1="0.96723125" x2="10.07871875" y2="0.985265625" layer="94"/>
<rectangle x1="10.4394" y1="0.96723125" x2="10.67561875" y2="0.985265625" layer="94"/>
<rectangle x1="10.982959375" y1="0.96723125" x2="11.23441875" y2="0.985265625" layer="94"/>
<rectangle x1="11.686540625" y1="0.96723125" x2="11.940540625" y2="0.985265625" layer="94"/>
<rectangle x1="12.24788125" y1="0.96723125" x2="12.50188125" y2="0.985265625" layer="94"/>
<rectangle x1="12.791440625" y1="0.96723125" x2="13.0429" y2="0.985265625" layer="94"/>
<rectangle x1="13.5128" y1="0.96723125" x2="13.74901875" y2="0.985265625" layer="94"/>
<rectangle x1="14.00048125" y1="0.96723125" x2="14.25448125" y2="0.985265625" layer="94"/>
<rectangle x1="14.671040625" y1="0.96723125" x2="14.9225" y2="0.985265625" layer="94"/>
<rectangle x1="15.267940625" y1="0.96723125" x2="15.5194" y2="0.985265625" layer="94"/>
<rectangle x1="15.808959375" y1="0.96723125" x2="16.677640625" y2="0.985265625" layer="94"/>
<rectangle x1="16.98498125" y1="0.96723125" x2="17.617440625" y2="0.985265625" layer="94"/>
<rectangle x1="19.408140625" y1="0.96723125" x2="20.1295" y2="0.985265625" layer="94"/>
<rectangle x1="5.63118125" y1="0.985265625" x2="5.88518125" y2="1.0033" layer="94"/>
<rectangle x1="6.3373" y1="0.985265625" x2="7.203440625" y2="1.0033" layer="94"/>
<rectangle x1="7.47521875" y1="0.985265625" x2="7.711440625" y2="1.0033" layer="94"/>
<rectangle x1="8.633459375" y1="0.985265625" x2="8.867140625" y2="1.0033" layer="94"/>
<rectangle x1="9.22781875" y1="0.985265625" x2="9.48181875" y2="1.0033" layer="94"/>
<rectangle x1="9.82471875" y1="0.985265625" x2="10.07871875" y2="1.0033" layer="94"/>
<rectangle x1="10.4394" y1="0.985265625" x2="10.6934" y2="1.0033" layer="94"/>
<rectangle x1="10.982959375" y1="0.985265625" x2="11.23441875" y2="1.0033" layer="94"/>
<rectangle x1="11.686540625" y1="0.985265625" x2="11.940540625" y2="1.0033" layer="94"/>
<rectangle x1="12.24788125" y1="0.985265625" x2="12.50188125" y2="1.0033" layer="94"/>
<rectangle x1="12.80921875" y1="0.985265625" x2="13.0429" y2="1.0033" layer="94"/>
<rectangle x1="13.5128" y1="0.985265625" x2="13.7668" y2="1.0033" layer="94"/>
<rectangle x1="14.0208" y1="0.985265625" x2="14.25448125" y2="1.0033" layer="94"/>
<rectangle x1="14.671040625" y1="0.985265625" x2="14.9225" y2="1.0033" layer="94"/>
<rectangle x1="15.267940625" y1="0.985265625" x2="15.5194" y2="1.0033" layer="94"/>
<rectangle x1="15.808959375" y1="0.985265625" x2="16.677640625" y2="1.0033" layer="94"/>
<rectangle x1="16.9672" y1="0.985265625" x2="17.599659375" y2="1.0033" layer="94"/>
<rectangle x1="19.46148125" y1="0.985265625" x2="20.203159375" y2="1.0033" layer="94"/>
<rectangle x1="5.63118125" y1="1.0033" x2="5.88518125" y2="1.021334375" layer="94"/>
<rectangle x1="6.35508125" y1="1.0033" x2="6.606540625" y2="1.021334375" layer="94"/>
<rectangle x1="6.969759375" y1="1.0033" x2="7.203440625" y2="1.021334375" layer="94"/>
<rectangle x1="7.47521875" y1="1.0033" x2="7.72921875" y2="1.021334375" layer="94"/>
<rectangle x1="8.633459375" y1="1.0033" x2="8.88491875" y2="1.021334375" layer="94"/>
<rectangle x1="9.22781875" y1="1.0033" x2="9.48181875" y2="1.021334375" layer="94"/>
<rectangle x1="9.8425" y1="1.0033" x2="10.07871875" y2="1.021334375" layer="94"/>
<rectangle x1="10.4394" y1="1.0033" x2="10.6934" y2="1.021334375" layer="94"/>
<rectangle x1="11.000740625" y1="1.0033" x2="11.254740625" y2="1.021334375" layer="94"/>
<rectangle x1="11.706859375" y1="1.0033" x2="11.940540625" y2="1.021334375" layer="94"/>
<rectangle x1="12.24788125" y1="1.0033" x2="12.50188125" y2="1.021334375" layer="94"/>
<rectangle x1="12.80921875" y1="1.0033" x2="13.06068125" y2="1.021334375" layer="94"/>
<rectangle x1="13.5128" y1="1.0033" x2="13.7668" y2="1.021334375" layer="94"/>
<rectangle x1="14.0208" y1="1.0033" x2="14.272259375" y2="1.021334375" layer="94"/>
<rectangle x1="14.671040625" y1="1.0033" x2="14.9225" y2="1.021334375" layer="94"/>
<rectangle x1="15.267940625" y1="1.0033" x2="15.5194" y2="1.021334375" layer="94"/>
<rectangle x1="15.808959375" y1="1.0033" x2="16.062959375" y2="1.021334375" layer="94"/>
<rectangle x1="16.423640625" y1="1.0033" x2="16.677640625" y2="1.021334375" layer="94"/>
<rectangle x1="16.9672" y1="1.0033" x2="17.54378125" y2="1.021334375" layer="94"/>
<rectangle x1="19.51481875" y1="1.0033" x2="20.27428125" y2="1.021334375" layer="94"/>
<rectangle x1="5.63118125" y1="1.021334375" x2="5.902959375" y2="1.039621875" layer="94"/>
<rectangle x1="6.35508125" y1="1.021334375" x2="6.606540625" y2="1.039621875" layer="94"/>
<rectangle x1="6.969759375" y1="1.021334375" x2="7.203440625" y2="1.039621875" layer="94"/>
<rectangle x1="7.47521875" y1="1.021334375" x2="7.72921875" y2="1.039621875" layer="94"/>
<rectangle x1="8.633459375" y1="1.021334375" x2="8.88491875" y2="1.039621875" layer="94"/>
<rectangle x1="9.248140625" y1="1.021334375" x2="9.48181875" y2="1.039621875" layer="94"/>
<rectangle x1="9.8425" y1="1.021334375" x2="10.0965" y2="1.039621875" layer="94"/>
<rectangle x1="10.4394" y1="1.021334375" x2="10.6934" y2="1.039621875" layer="94"/>
<rectangle x1="11.000740625" y1="1.021334375" x2="11.254740625" y2="1.039621875" layer="94"/>
<rectangle x1="11.686540625" y1="1.021334375" x2="11.940540625" y2="1.039621875" layer="94"/>
<rectangle x1="12.265659375" y1="1.021334375" x2="12.50188125" y2="1.039621875" layer="94"/>
<rectangle x1="12.80921875" y1="1.021334375" x2="13.06068125" y2="1.039621875" layer="94"/>
<rectangle x1="13.5128" y1="1.021334375" x2="13.7668" y2="1.039621875" layer="94"/>
<rectangle x1="14.0208" y1="1.021334375" x2="14.272259375" y2="1.039621875" layer="94"/>
<rectangle x1="14.68881875" y1="1.021334375" x2="14.9225" y2="1.039621875" layer="94"/>
<rectangle x1="15.28571875" y1="1.021334375" x2="15.5194" y2="1.039621875" layer="94"/>
<rectangle x1="15.808959375" y1="1.021334375" x2="16.062959375" y2="1.039621875" layer="94"/>
<rectangle x1="16.44141875" y1="1.021334375" x2="16.677640625" y2="1.039621875" layer="94"/>
<rectangle x1="16.9672" y1="1.021334375" x2="17.50821875" y2="1.039621875" layer="94"/>
<rectangle x1="19.58848125" y1="1.021334375" x2="20.330159375" y2="1.039621875" layer="94"/>
<rectangle x1="5.63118125" y1="1.039621875" x2="5.902959375" y2="1.05765625" layer="94"/>
<rectangle x1="6.35508125" y1="1.039621875" x2="6.606540625" y2="1.05765625" layer="94"/>
<rectangle x1="6.969759375" y1="1.039621875" x2="7.203440625" y2="1.05765625" layer="94"/>
<rectangle x1="7.47521875" y1="1.039621875" x2="7.72921875" y2="1.05765625" layer="94"/>
<rectangle x1="8.633459375" y1="1.039621875" x2="8.88491875" y2="1.05765625" layer="94"/>
<rectangle x1="9.248140625" y1="1.039621875" x2="9.48181875" y2="1.05765625" layer="94"/>
<rectangle x1="9.8425" y1="1.039621875" x2="10.0965" y2="1.05765625" layer="94"/>
<rectangle x1="10.4394" y1="1.039621875" x2="10.6934" y2="1.05765625" layer="94"/>
<rectangle x1="11.000740625" y1="1.039621875" x2="11.254740625" y2="1.05765625" layer="94"/>
<rectangle x1="11.686540625" y1="1.039621875" x2="11.940540625" y2="1.05765625" layer="94"/>
<rectangle x1="12.265659375" y1="1.039621875" x2="12.50188125" y2="1.05765625" layer="94"/>
<rectangle x1="12.827" y1="1.039621875" x2="13.078459375" y2="1.05765625" layer="94"/>
<rectangle x1="13.5128" y1="1.039621875" x2="13.7668" y2="1.05765625" layer="94"/>
<rectangle x1="14.0208" y1="1.039621875" x2="14.272259375" y2="1.05765625" layer="94"/>
<rectangle x1="14.671040625" y1="1.039621875" x2="14.9225" y2="1.05765625" layer="94"/>
<rectangle x1="15.28571875" y1="1.039621875" x2="15.5194" y2="1.05765625" layer="94"/>
<rectangle x1="15.826740625" y1="1.039621875" x2="16.062959375" y2="1.05765625" layer="94"/>
<rectangle x1="16.44141875" y1="1.039621875" x2="16.677640625" y2="1.05765625" layer="94"/>
<rectangle x1="16.94941875" y1="1.039621875" x2="17.4371" y2="1.05765625" layer="94"/>
<rectangle x1="19.6596" y1="1.039621875" x2="20.40128125" y2="1.05765625" layer="94"/>
<rectangle x1="5.648959375" y1="1.05765625" x2="5.902959375" y2="1.075690625" layer="94"/>
<rectangle x1="6.35508125" y1="1.05765625" x2="6.606540625" y2="1.075690625" layer="94"/>
<rectangle x1="6.969759375" y1="1.05765625" x2="7.203440625" y2="1.075690625" layer="94"/>
<rectangle x1="7.493" y1="1.05765625" x2="7.747" y2="1.075690625" layer="94"/>
<rectangle x1="8.633459375" y1="1.05765625" x2="8.88491875" y2="1.075690625" layer="94"/>
<rectangle x1="9.248140625" y1="1.05765625" x2="9.48181875" y2="1.075690625" layer="94"/>
<rectangle x1="9.8425" y1="1.05765625" x2="10.0965" y2="1.075690625" layer="94"/>
<rectangle x1="10.4394" y1="1.05765625" x2="10.6934" y2="1.075690625" layer="94"/>
<rectangle x1="11.01851875" y1="1.05765625" x2="11.27251875" y2="1.075690625" layer="94"/>
<rectangle x1="11.686540625" y1="1.05765625" x2="11.940540625" y2="1.075690625" layer="94"/>
<rectangle x1="12.265659375" y1="1.05765625" x2="12.50188125" y2="1.075690625" layer="94"/>
<rectangle x1="12.827" y1="1.05765625" x2="13.078459375" y2="1.075690625" layer="94"/>
<rectangle x1="13.5128" y1="1.05765625" x2="13.7668" y2="1.075690625" layer="94"/>
<rectangle x1="14.03858125" y1="1.05765625" x2="14.272259375" y2="1.075690625" layer="94"/>
<rectangle x1="14.671040625" y1="1.05765625" x2="14.9225" y2="1.075690625" layer="94"/>
<rectangle x1="15.28571875" y1="1.05765625" x2="15.5194" y2="1.075690625" layer="94"/>
<rectangle x1="15.826740625" y1="1.05765625" x2="16.080740625" y2="1.075690625" layer="94"/>
<rectangle x1="16.44141875" y1="1.05765625" x2="16.677640625" y2="1.075690625" layer="94"/>
<rectangle x1="16.94941875" y1="1.05765625" x2="17.38121875" y2="1.075690625" layer="94"/>
<rectangle x1="19.71548125" y1="1.05765625" x2="20.4724" y2="1.075690625" layer="94"/>
<rectangle x1="5.648959375" y1="1.075690625" x2="5.902959375" y2="1.093725" layer="94"/>
<rectangle x1="6.372859375" y1="1.075690625" x2="6.606540625" y2="1.093725" layer="94"/>
<rectangle x1="6.969759375" y1="1.075690625" x2="7.203440625" y2="1.093725" layer="94"/>
<rectangle x1="7.493" y1="1.075690625" x2="7.747" y2="1.093725" layer="94"/>
<rectangle x1="8.633459375" y1="1.075690625" x2="8.9027" y2="1.093725" layer="94"/>
<rectangle x1="9.248140625" y1="1.075690625" x2="9.48181875" y2="1.093725" layer="94"/>
<rectangle x1="9.8425" y1="1.075690625" x2="10.11428125" y2="1.093725" layer="94"/>
<rectangle x1="10.45718125" y1="1.075690625" x2="10.6934" y2="1.093725" layer="94"/>
<rectangle x1="11.01851875" y1="1.075690625" x2="11.27251875" y2="1.093725" layer="94"/>
<rectangle x1="11.686540625" y1="1.075690625" x2="11.940540625" y2="1.093725" layer="94"/>
<rectangle x1="12.265659375" y1="1.075690625" x2="12.50188125" y2="1.093725" layer="94"/>
<rectangle x1="12.827" y1="1.075690625" x2="13.09878125" y2="1.093725" layer="94"/>
<rectangle x1="13.49501875" y1="1.075690625" x2="13.7668" y2="1.093725" layer="94"/>
<rectangle x1="14.03858125" y1="1.075690625" x2="14.290040625" y2="1.093725" layer="94"/>
<rectangle x1="14.671040625" y1="1.075690625" x2="14.94281875" y2="1.093725" layer="94"/>
<rectangle x1="15.28571875" y1="1.075690625" x2="15.53718125" y2="1.093725" layer="94"/>
<rectangle x1="15.826740625" y1="1.075690625" x2="16.080740625" y2="1.093725" layer="94"/>
<rectangle x1="16.44141875" y1="1.075690625" x2="16.677640625" y2="1.093725" layer="94"/>
<rectangle x1="16.94941875" y1="1.075690625" x2="17.3101" y2="1.093725" layer="94"/>
<rectangle x1="19.76881875" y1="1.075690625" x2="20.52828125" y2="1.093725" layer="94"/>
<rectangle x1="5.648959375" y1="1.093725" x2="5.902959375" y2="1.111759375" layer="94"/>
<rectangle x1="6.372859375" y1="1.093725" x2="6.626859375" y2="1.111759375" layer="94"/>
<rectangle x1="6.969759375" y1="1.093725" x2="7.203440625" y2="1.111759375" layer="94"/>
<rectangle x1="7.493" y1="1.093725" x2="7.747" y2="1.111759375" layer="94"/>
<rectangle x1="8.10768125" y1="1.093725" x2="8.181340625" y2="1.111759375" layer="94"/>
<rectangle x1="8.651240625" y1="1.093725" x2="8.9027" y2="1.111759375" layer="94"/>
<rectangle x1="9.248140625" y1="1.093725" x2="9.4996" y2="1.111759375" layer="94"/>
<rectangle x1="9.8425" y1="1.093725" x2="10.11428125" y2="1.111759375" layer="94"/>
<rectangle x1="10.45718125" y1="1.093725" x2="10.6934" y2="1.111759375" layer="94"/>
<rectangle x1="11.01851875" y1="1.093725" x2="11.2903" y2="1.111759375" layer="94"/>
<rectangle x1="11.686540625" y1="1.093725" x2="11.940540625" y2="1.111759375" layer="94"/>
<rectangle x1="12.265659375" y1="1.093725" x2="12.519659375" y2="1.111759375" layer="94"/>
<rectangle x1="12.84478125" y1="1.093725" x2="13.09878125" y2="1.111759375" layer="94"/>
<rectangle x1="13.49501875" y1="1.093725" x2="13.74901875" y2="1.111759375" layer="94"/>
<rectangle x1="14.03858125" y1="1.093725" x2="14.290040625" y2="1.111759375" layer="94"/>
<rectangle x1="14.671040625" y1="1.093725" x2="14.94281875" y2="1.111759375" layer="94"/>
<rectangle x1="15.28571875" y1="1.093725" x2="15.53718125" y2="1.111759375" layer="94"/>
<rectangle x1="15.84451875" y1="1.093725" x2="16.080740625" y2="1.111759375" layer="94"/>
<rectangle x1="16.44141875" y1="1.093725" x2="16.677640625" y2="1.111759375" layer="94"/>
<rectangle x1="16.94941875" y1="1.093725" x2="17.256759375" y2="1.111759375" layer="94"/>
<rectangle x1="19.839940625" y1="1.093725" x2="20.58161875" y2="1.111759375" layer="94"/>
<rectangle x1="5.648959375" y1="1.111759375" x2="5.902959375" y2="1.129790625" layer="94"/>
<rectangle x1="6.390640625" y1="1.111759375" x2="6.626859375" y2="1.129790625" layer="94"/>
<rectangle x1="6.969759375" y1="1.111759375" x2="7.203440625" y2="1.129790625" layer="94"/>
<rectangle x1="7.51078125" y1="1.111759375" x2="7.76478125" y2="1.129790625" layer="94"/>
<rectangle x1="8.10768125" y1="1.111759375" x2="8.270240625" y2="1.129790625" layer="94"/>
<rectangle x1="8.651240625" y1="1.111759375" x2="8.92048125" y2="1.129790625" layer="94"/>
<rectangle x1="9.248140625" y1="1.111759375" x2="9.4996" y2="1.129790625" layer="94"/>
<rectangle x1="9.86281875" y1="1.111759375" x2="10.11428125" y2="1.129790625" layer="94"/>
<rectangle x1="10.45718125" y1="1.111759375" x2="10.71118125" y2="1.129790625" layer="94"/>
<rectangle x1="11.0363" y1="1.111759375" x2="11.2903" y2="1.129790625" layer="94"/>
<rectangle x1="11.686540625" y1="1.111759375" x2="11.940540625" y2="1.129790625" layer="94"/>
<rectangle x1="12.265659375" y1="1.111759375" x2="12.519659375" y2="1.129790625" layer="94"/>
<rectangle x1="12.84478125" y1="1.111759375" x2="13.116559375" y2="1.129790625" layer="94"/>
<rectangle x1="13.49501875" y1="1.111759375" x2="13.74901875" y2="1.129790625" layer="94"/>
<rectangle x1="14.056359375" y1="1.111759375" x2="14.30781875" y2="1.129790625" layer="94"/>
<rectangle x1="14.671040625" y1="1.111759375" x2="14.94281875" y2="1.129790625" layer="94"/>
<rectangle x1="15.28571875" y1="1.111759375" x2="15.53718125" y2="1.129790625" layer="94"/>
<rectangle x1="15.84451875" y1="1.111759375" x2="16.09851875" y2="1.129790625" layer="94"/>
<rectangle x1="16.44141875" y1="1.111759375" x2="16.677640625" y2="1.129790625" layer="94"/>
<rectangle x1="16.94941875" y1="1.111759375" x2="17.218659375" y2="1.129790625" layer="94"/>
<rectangle x1="19.89581875" y1="1.111759375" x2="20.65528125" y2="1.129790625" layer="94"/>
<rectangle x1="5.648959375" y1="1.129790625" x2="5.902959375" y2="1.14808125" layer="94"/>
<rectangle x1="6.390640625" y1="1.129790625" x2="6.644640625" y2="1.14808125" layer="94"/>
<rectangle x1="6.969759375" y1="1.129790625" x2="7.203440625" y2="1.14808125" layer="94"/>
<rectangle x1="7.51078125" y1="1.129790625" x2="7.76478125" y2="1.14808125" layer="94"/>
<rectangle x1="8.10768125" y1="1.129790625" x2="8.3439" y2="1.14808125" layer="94"/>
<rectangle x1="8.651240625" y1="1.129790625" x2="8.92048125" y2="1.14808125" layer="94"/>
<rectangle x1="9.248140625" y1="1.129790625" x2="9.4996" y2="1.14808125" layer="94"/>
<rectangle x1="9.86281875" y1="1.129790625" x2="10.132059375" y2="1.14808125" layer="94"/>
<rectangle x1="10.45718125" y1="1.129790625" x2="10.71118125" y2="1.14808125" layer="94"/>
<rectangle x1="11.0363" y1="1.129790625" x2="11.30808125" y2="1.14808125" layer="94"/>
<rectangle x1="11.668759375" y1="1.129790625" x2="11.940540625" y2="1.14808125" layer="94"/>
<rectangle x1="12.265659375" y1="1.129790625" x2="12.519659375" y2="1.14808125" layer="94"/>
<rectangle x1="12.862559375" y1="1.129790625" x2="13.116559375" y2="1.14808125" layer="94"/>
<rectangle x1="13.477240625" y1="1.129790625" x2="13.74901875" y2="1.14808125" layer="94"/>
<rectangle x1="14.056359375" y1="1.129790625" x2="14.30781875" y2="1.14808125" layer="94"/>
<rectangle x1="14.671040625" y1="1.129790625" x2="14.94281875" y2="1.14808125" layer="94"/>
<rectangle x1="15.28571875" y1="1.129790625" x2="15.53718125" y2="1.14808125" layer="94"/>
<rectangle x1="15.864840625" y1="1.129790625" x2="16.09851875" y2="1.14808125" layer="94"/>
<rectangle x1="16.44141875" y1="1.129790625" x2="16.659859375" y2="1.14808125" layer="94"/>
<rectangle x1="16.94941875" y1="1.129790625" x2="17.20088125" y2="1.14808125" layer="94"/>
<rectangle x1="19.949159375" y1="1.129790625" x2="20.70861875" y2="1.14808125" layer="94"/>
<rectangle x1="5.648959375" y1="1.14808125" x2="5.920740625" y2="1.1661125" layer="94"/>
<rectangle x1="6.390640625" y1="1.14808125" x2="6.644640625" y2="1.1661125" layer="94"/>
<rectangle x1="6.969759375" y1="1.14808125" x2="7.203440625" y2="1.1661125" layer="94"/>
<rectangle x1="7.528559375" y1="1.14808125" x2="7.782559375" y2="1.1661125" layer="94"/>
<rectangle x1="8.0899" y1="1.14808125" x2="8.3439" y2="1.1661125" layer="94"/>
<rectangle x1="8.651240625" y1="1.14808125" x2="8.9408" y2="1.1661125" layer="94"/>
<rectangle x1="9.248140625" y1="1.14808125" x2="9.4996" y2="1.1661125" layer="94"/>
<rectangle x1="9.86281875" y1="1.14808125" x2="10.132059375" y2="1.1661125" layer="94"/>
<rectangle x1="10.4394" y1="1.14808125" x2="10.71118125" y2="1.1661125" layer="94"/>
<rectangle x1="11.05408125" y1="1.14808125" x2="11.325859375" y2="1.1661125" layer="94"/>
<rectangle x1="11.668759375" y1="1.14808125" x2="11.940540625" y2="1.1661125" layer="94"/>
<rectangle x1="12.283440625" y1="1.14808125" x2="12.519659375" y2="1.1661125" layer="94"/>
<rectangle x1="12.862559375" y1="1.14808125" x2="13.134340625" y2="1.1661125" layer="94"/>
<rectangle x1="13.477240625" y1="1.14808125" x2="13.74901875" y2="1.1661125" layer="94"/>
<rectangle x1="14.056359375" y1="1.14808125" x2="14.328140625" y2="1.1661125" layer="94"/>
<rectangle x1="14.653259375" y1="1.14808125" x2="14.94281875" y2="1.1661125" layer="94"/>
<rectangle x1="15.3035" y1="1.14808125" x2="15.53718125" y2="1.1661125" layer="94"/>
<rectangle x1="15.864840625" y1="1.14808125" x2="16.1163" y2="1.1661125" layer="94"/>
<rectangle x1="16.423640625" y1="1.14808125" x2="16.659859375" y2="1.1661125" layer="94"/>
<rectangle x1="16.9672" y1="1.14808125" x2="17.1831" y2="1.1661125" layer="94"/>
<rectangle x1="17.526" y1="1.14808125" x2="17.58188125" y2="1.1661125" layer="94"/>
<rectangle x1="20.0025" y1="1.14808125" x2="20.779740625" y2="1.1661125" layer="94"/>
<rectangle x1="5.666740625" y1="1.1661125" x2="5.920740625" y2="1.184146875" layer="94"/>
<rectangle x1="6.40841875" y1="1.1661125" x2="6.66241875" y2="1.184146875" layer="94"/>
<rectangle x1="6.95198125" y1="1.1661125" x2="7.185659375" y2="1.184146875" layer="94"/>
<rectangle x1="7.528559375" y1="1.1661125" x2="7.800340625" y2="1.184146875" layer="94"/>
<rectangle x1="8.0899" y1="1.1661125" x2="8.3439" y2="1.184146875" layer="94"/>
<rectangle x1="8.651240625" y1="1.1661125" x2="8.9408" y2="1.184146875" layer="94"/>
<rectangle x1="9.248140625" y1="1.1661125" x2="9.4996" y2="1.184146875" layer="94"/>
<rectangle x1="9.86281875" y1="1.1661125" x2="10.149840625" y2="1.184146875" layer="94"/>
<rectangle x1="10.4394" y1="1.1661125" x2="10.71118125" y2="1.184146875" layer="94"/>
<rectangle x1="11.071859375" y1="1.1661125" x2="11.343640625" y2="1.184146875" layer="94"/>
<rectangle x1="11.65098125" y1="1.1661125" x2="11.922759375" y2="1.184146875" layer="94"/>
<rectangle x1="12.283440625" y1="1.1661125" x2="12.519659375" y2="1.184146875" layer="94"/>
<rectangle x1="12.880340625" y1="1.1661125" x2="13.15211875" y2="1.184146875" layer="94"/>
<rectangle x1="13.459459375" y1="1.1661125" x2="13.74901875" y2="1.184146875" layer="94"/>
<rectangle x1="14.074140625" y1="1.1661125" x2="14.34591875" y2="1.184146875" layer="94"/>
<rectangle x1="14.653259375" y1="1.1661125" x2="14.94281875" y2="1.184146875" layer="94"/>
<rectangle x1="15.3035" y1="1.1661125" x2="15.53718125" y2="1.184146875" layer="94"/>
<rectangle x1="15.88261875" y1="1.1661125" x2="16.13408125" y2="1.184146875" layer="94"/>
<rectangle x1="16.423640625" y1="1.1661125" x2="16.659859375" y2="1.184146875" layer="94"/>
<rectangle x1="16.9672" y1="1.1661125" x2="17.1831" y2="1.184146875" layer="94"/>
<rectangle x1="17.526" y1="1.1661125" x2="17.688559375" y2="1.184146875" layer="94"/>
<rectangle x1="20.05838125" y1="1.1661125" x2="20.83561875" y2="1.184146875" layer="94"/>
<rectangle x1="5.666740625" y1="1.184146875" x2="5.920740625" y2="1.20218125" layer="94"/>
<rectangle x1="6.40841875" y1="1.184146875" x2="6.6802" y2="1.20218125" layer="94"/>
<rectangle x1="6.95198125" y1="1.184146875" x2="7.185659375" y2="1.20218125" layer="94"/>
<rectangle x1="7.54888125" y1="1.184146875" x2="7.81811875" y2="1.20218125" layer="94"/>
<rectangle x1="8.0899" y1="1.184146875" x2="8.3439" y2="1.20218125" layer="94"/>
<rectangle x1="8.651240625" y1="1.184146875" x2="8.95858125" y2="1.20218125" layer="94"/>
<rectangle x1="9.22781875" y1="1.184146875" x2="9.4996" y2="1.20218125" layer="94"/>
<rectangle x1="9.86281875" y1="1.184146875" x2="10.170159375" y2="1.20218125" layer="94"/>
<rectangle x1="10.4394" y1="1.184146875" x2="10.71118125" y2="1.20218125" layer="94"/>
<rectangle x1="11.071859375" y1="1.184146875" x2="11.36141875" y2="1.20218125" layer="94"/>
<rectangle x1="11.6332" y1="1.184146875" x2="11.922759375" y2="1.20218125" layer="94"/>
<rectangle x1="12.283440625" y1="1.184146875" x2="12.519659375" y2="1.20218125" layer="94"/>
<rectangle x1="12.880340625" y1="1.184146875" x2="13.1699" y2="1.20218125" layer="94"/>
<rectangle x1="13.44168125" y1="1.184146875" x2="13.731240625" y2="1.20218125" layer="94"/>
<rectangle x1="14.074140625" y1="1.184146875" x2="14.3637" y2="1.20218125" layer="94"/>
<rectangle x1="14.63548125" y1="1.184146875" x2="14.9606" y2="1.20218125" layer="94"/>
<rectangle x1="15.3035" y1="1.184146875" x2="15.53718125" y2="1.20218125" layer="94"/>
<rectangle x1="15.88261875" y1="1.184146875" x2="16.151859375" y2="1.20218125" layer="94"/>
<rectangle x1="16.405859375" y1="1.184146875" x2="16.659859375" y2="1.20218125" layer="94"/>
<rectangle x1="16.9672" y1="1.184146875" x2="17.1831" y2="1.20218125" layer="94"/>
<rectangle x1="17.526" y1="1.184146875" x2="17.76221875" y2="1.20218125" layer="94"/>
<rectangle x1="20.11171875" y1="1.184146875" x2="20.888959375" y2="1.20218125" layer="94"/>
<rectangle x1="5.666740625" y1="1.20218125" x2="5.920740625" y2="1.220215625" layer="94"/>
<rectangle x1="6.4262" y1="1.20218125" x2="6.69798125" y2="1.220215625" layer="94"/>
<rectangle x1="6.9342" y1="1.20218125" x2="7.185659375" y2="1.220215625" layer="94"/>
<rectangle x1="7.54888125" y1="1.20218125" x2="7.8359" y2="1.220215625" layer="94"/>
<rectangle x1="8.07211875" y1="1.20218125" x2="8.3439" y2="1.220215625" layer="94"/>
<rectangle x1="8.66901875" y1="1.20218125" x2="8.994140625" y2="1.220215625" layer="94"/>
<rectangle x1="9.22781875" y1="1.20218125" x2="9.4996" y2="1.220215625" layer="94"/>
<rectangle x1="9.86281875" y1="1.20218125" x2="10.187940625" y2="1.220215625" layer="94"/>
<rectangle x1="10.42161875" y1="1.20218125" x2="10.71118125" y2="1.220215625" layer="94"/>
<rectangle x1="11.09218125" y1="1.20218125" x2="11.39951875" y2="1.220215625" layer="94"/>
<rectangle x1="11.61541875" y1="1.20218125" x2="11.922759375" y2="1.220215625" layer="94"/>
<rectangle x1="12.283440625" y1="1.20218125" x2="12.537440625" y2="1.220215625" layer="94"/>
<rectangle x1="12.89811875" y1="1.20218125" x2="13.205459375" y2="1.220215625" layer="94"/>
<rectangle x1="13.4239" y1="1.20218125" x2="13.731240625" y2="1.220215625" layer="94"/>
<rectangle x1="14.09191875" y1="1.20218125" x2="14.38148125" y2="1.220215625" layer="94"/>
<rectangle x1="14.615159375" y1="1.20218125" x2="14.9606" y2="1.220215625" layer="94"/>
<rectangle x1="15.3035" y1="1.20218125" x2="15.5575" y2="1.220215625" layer="94"/>
<rectangle x1="15.9004" y1="1.20218125" x2="16.17218125" y2="1.220215625" layer="94"/>
<rectangle x1="16.38808125" y1="1.20218125" x2="16.659859375" y2="1.220215625" layer="94"/>
<rectangle x1="16.9672" y1="1.20218125" x2="17.20088125" y2="1.220215625" layer="94"/>
<rectangle x1="17.50821875" y1="1.20218125" x2="17.76221875" y2="1.220215625" layer="94"/>
<rectangle x1="20.1676" y1="1.20218125" x2="20.944840625" y2="1.220215625" layer="94"/>
<rectangle x1="5.666740625" y1="1.220215625" x2="5.920740625" y2="1.23825" layer="94"/>
<rectangle x1="6.44398125" y1="1.220215625" x2="6.733540625" y2="1.23825" layer="94"/>
<rectangle x1="6.91388125" y1="1.220215625" x2="7.185659375" y2="1.23825" layer="94"/>
<rectangle x1="7.566659375" y1="1.220215625" x2="7.874" y2="1.23825" layer="94"/>
<rectangle x1="8.036559375" y1="1.220215625" x2="8.3439" y2="1.23825" layer="94"/>
<rectangle x1="8.66901875" y1="1.220215625" x2="9.01191875" y2="1.23825" layer="94"/>
<rectangle x1="9.210040625" y1="1.220215625" x2="9.4996" y2="1.23825" layer="94"/>
<rectangle x1="9.8806" y1="1.220215625" x2="10.2235" y2="1.23825" layer="94"/>
<rectangle x1="10.403840625" y1="1.220215625" x2="10.71118125" y2="1.23825" layer="94"/>
<rectangle x1="11.09218125" y1="1.220215625" x2="11.43508125" y2="1.23825" layer="94"/>
<rectangle x1="11.579859375" y1="1.220215625" x2="11.90498125" y2="1.23825" layer="94"/>
<rectangle x1="12.283440625" y1="1.220215625" x2="12.537440625" y2="1.23825" layer="94"/>
<rectangle x1="12.9159" y1="1.220215625" x2="13.243559375" y2="1.23825" layer="94"/>
<rectangle x1="13.3858" y1="1.220215625" x2="13.731240625" y2="1.23825" layer="94"/>
<rectangle x1="14.09191875" y1="1.220215625" x2="14.399259375" y2="1.23825" layer="94"/>
<rectangle x1="14.59738125" y1="1.220215625" x2="14.9606" y2="1.23825" layer="94"/>
<rectangle x1="15.3035" y1="1.220215625" x2="15.5575" y2="1.23825" layer="94"/>
<rectangle x1="15.9004" y1="1.220215625" x2="16.189959375" y2="1.23825" layer="94"/>
<rectangle x1="16.3703" y1="1.220215625" x2="16.64208125" y2="1.23825" layer="94"/>
<rectangle x1="16.98498125" y1="1.220215625" x2="17.218659375" y2="1.23825" layer="94"/>
<rectangle x1="17.490440625" y1="1.220215625" x2="17.76221875" y2="1.23825" layer="94"/>
<rectangle x1="20.220940625" y1="1.220215625" x2="20.99818125" y2="1.23825" layer="94"/>
<rectangle x1="5.666740625" y1="1.23825" x2="5.920740625" y2="1.2565375" layer="94"/>
<rectangle x1="6.44398125" y1="1.23825" x2="6.7691" y2="1.2565375" layer="94"/>
<rectangle x1="6.87831875" y1="1.23825" x2="7.16788125" y2="1.2565375" layer="94"/>
<rectangle x1="7.584440625" y1="1.23825" x2="7.927340625" y2="1.2565375" layer="94"/>
<rectangle x1="7.998459375" y1="1.23825" x2="8.32611875" y2="1.2565375" layer="94"/>
<rectangle x1="8.66901875" y1="1.23825" x2="9.04748125" y2="1.2565375" layer="94"/>
<rectangle x1="9.17448125" y1="1.23825" x2="9.4996" y2="1.2565375" layer="94"/>
<rectangle x1="9.8806" y1="1.23825" x2="10.259059375" y2="1.2565375" layer="94"/>
<rectangle x1="10.36828125" y1="1.23825" x2="10.71118125" y2="1.2565375" layer="94"/>
<rectangle x1="11.109959375" y1="1.23825" x2="11.90498125" y2="1.2565375" layer="94"/>
<rectangle x1="12.283440625" y1="1.23825" x2="12.537440625" y2="1.2565375" layer="94"/>
<rectangle x1="12.9159" y1="1.23825" x2="13.713459375" y2="1.2565375" layer="94"/>
<rectangle x1="14.1097" y1="1.23825" x2="14.4526" y2="1.2565375" layer="94"/>
<rectangle x1="14.56181875" y1="1.23825" x2="14.9606" y2="1.2565375" layer="94"/>
<rectangle x1="15.3035" y1="1.23825" x2="15.5575" y2="1.2565375" layer="94"/>
<rectangle x1="15.91818125" y1="1.23825" x2="16.22551875" y2="1.2565375" layer="94"/>
<rectangle x1="16.334740625" y1="1.23825" x2="16.64208125" y2="1.2565375" layer="94"/>
<rectangle x1="16.98498125" y1="1.23825" x2="17.236440625" y2="1.2565375" layer="94"/>
<rectangle x1="17.472659375" y1="1.23825" x2="17.76221875" y2="1.2565375" layer="94"/>
<rectangle x1="20.27428125" y1="1.23825" x2="21.05151875" y2="1.2565375" layer="94"/>
<rectangle x1="5.666740625" y1="1.2565375" x2="5.93851875" y2="1.274571875" layer="94"/>
<rectangle x1="6.461759375" y1="1.2565375" x2="7.16788125" y2="1.274571875" layer="94"/>
<rectangle x1="7.584440625" y1="1.2565375" x2="8.32611875" y2="1.274571875" layer="94"/>
<rectangle x1="8.66901875" y1="1.2565375" x2="9.4996" y2="1.274571875" layer="94"/>
<rectangle x1="9.8806" y1="1.2565375" x2="10.71118125" y2="1.274571875" layer="94"/>
<rectangle x1="11.127740625" y1="1.2565375" x2="11.8872" y2="1.274571875" layer="94"/>
<rectangle x1="12.30121875" y1="1.2565375" x2="12.537440625" y2="1.274571875" layer="94"/>
<rectangle x1="12.93621875" y1="1.2565375" x2="13.713459375" y2="1.274571875" layer="94"/>
<rectangle x1="14.12748125" y1="1.2565375" x2="14.9606" y2="1.274571875" layer="94"/>
<rectangle x1="15.32128125" y1="1.2565375" x2="15.5575" y2="1.274571875" layer="94"/>
<rectangle x1="15.935959375" y1="1.2565375" x2="16.64208125" y2="1.274571875" layer="94"/>
<rectangle x1="17.002759375" y1="1.2565375" x2="17.32788125" y2="1.274571875" layer="94"/>
<rectangle x1="17.41931875" y1="1.2565375" x2="17.76221875" y2="1.274571875" layer="94"/>
<rectangle x1="20.330159375" y1="1.2565375" x2="21.1074" y2="1.274571875" layer="94"/>
<rectangle x1="5.666740625" y1="1.274571875" x2="5.93851875" y2="1.29260625" layer="94"/>
<rectangle x1="6.48208125" y1="1.274571875" x2="7.1501" y2="1.29260625" layer="94"/>
<rectangle x1="7.60221875" y1="1.274571875" x2="8.32611875" y2="1.29260625" layer="94"/>
<rectangle x1="8.66901875" y1="1.274571875" x2="9.48181875" y2="1.29260625" layer="94"/>
<rectangle x1="9.8806" y1="1.274571875" x2="10.6934" y2="1.29260625" layer="94"/>
<rectangle x1="11.14551875" y1="1.274571875" x2="11.8872" y2="1.29260625" layer="94"/>
<rectangle x1="12.30121875" y1="1.274571875" x2="12.537440625" y2="1.29260625" layer="94"/>
<rectangle x1="12.954" y1="1.274571875" x2="13.693140625" y2="1.29260625" layer="94"/>
<rectangle x1="14.12748125" y1="1.274571875" x2="14.9606" y2="1.29260625" layer="94"/>
<rectangle x1="15.32128125" y1="1.274571875" x2="15.5575" y2="1.29260625" layer="94"/>
<rectangle x1="15.935959375" y1="1.274571875" x2="16.621759375" y2="1.29260625" layer="94"/>
<rectangle x1="17.002759375" y1="1.274571875" x2="17.744440625" y2="1.29260625" layer="94"/>
<rectangle x1="20.3835" y1="1.274571875" x2="21.160740625" y2="1.29260625" layer="94"/>
<rectangle x1="5.68451875" y1="1.29260625" x2="5.93851875" y2="1.310640625" layer="94"/>
<rectangle x1="6.499859375" y1="1.29260625" x2="7.1501" y2="1.310640625" layer="94"/>
<rectangle x1="7.62" y1="1.29260625" x2="8.3058" y2="1.310640625" layer="94"/>
<rectangle x1="8.66901875" y1="1.29260625" x2="9.48181875" y2="1.310640625" layer="94"/>
<rectangle x1="9.8806" y1="1.29260625" x2="10.6934" y2="1.310640625" layer="94"/>
<rectangle x1="11.1633" y1="1.29260625" x2="11.86941875" y2="1.310640625" layer="94"/>
<rectangle x1="12.30121875" y1="1.29260625" x2="12.537440625" y2="1.310640625" layer="94"/>
<rectangle x1="12.97178125" y1="1.29260625" x2="13.675359375" y2="1.310640625" layer="94"/>
<rectangle x1="14.145259375" y1="1.29260625" x2="14.9606" y2="1.310640625" layer="94"/>
<rectangle x1="15.32128125" y1="1.29260625" x2="15.5575" y2="1.310640625" layer="94"/>
<rectangle x1="15.953740625" y1="1.29260625" x2="16.621759375" y2="1.310640625" layer="94"/>
<rectangle x1="17.020540625" y1="1.29260625" x2="17.744440625" y2="1.310640625" layer="94"/>
<rectangle x1="20.436840625" y1="1.29260625" x2="21.21408125" y2="1.310640625" layer="94"/>
<rectangle x1="5.68451875" y1="1.310640625" x2="5.93851875" y2="1.328675" layer="94"/>
<rectangle x1="6.517640625" y1="1.310640625" x2="7.13231875" y2="1.328675" layer="94"/>
<rectangle x1="7.63778125" y1="1.310640625" x2="8.28801875" y2="1.328675" layer="94"/>
<rectangle x1="8.6868" y1="1.310640625" x2="8.92048125" y2="1.328675" layer="94"/>
<rectangle x1="8.9408" y1="1.310640625" x2="9.48181875" y2="1.328675" layer="94"/>
<rectangle x1="9.8806" y1="1.310640625" x2="10.11428125" y2="1.328675" layer="94"/>
<rectangle x1="10.132059375" y1="1.310640625" x2="10.6934" y2="1.328675" layer="94"/>
<rectangle x1="11.18108125" y1="1.310640625" x2="11.8491" y2="1.328675" layer="94"/>
<rectangle x1="12.30121875" y1="1.310640625" x2="12.537440625" y2="1.328675" layer="94"/>
<rectangle x1="12.989559375" y1="1.310640625" x2="13.675359375" y2="1.328675" layer="94"/>
<rectangle x1="14.16558125" y1="1.310640625" x2="14.72438125" y2="1.328675" layer="94"/>
<rectangle x1="14.742159375" y1="1.310640625" x2="14.97838125" y2="1.328675" layer="94"/>
<rectangle x1="15.32128125" y1="1.310640625" x2="15.57528125" y2="1.328675" layer="94"/>
<rectangle x1="15.97151875" y1="1.310640625" x2="16.60398125" y2="1.328675" layer="94"/>
<rectangle x1="17.03831875" y1="1.310640625" x2="17.726659375" y2="1.328675" layer="94"/>
<rectangle x1="20.49271875" y1="1.310640625" x2="21.269959375" y2="1.328675" layer="94"/>
<rectangle x1="5.68451875" y1="1.328675" x2="5.93851875" y2="1.346709375" layer="94"/>
<rectangle x1="6.53541875" y1="1.328675" x2="7.13231875" y2="1.346709375" layer="94"/>
<rectangle x1="7.655559375" y1="1.328675" x2="8.28801875" y2="1.346709375" layer="94"/>
<rectangle x1="8.6868" y1="1.328675" x2="8.92048125" y2="1.346709375" layer="94"/>
<rectangle x1="8.95858125" y1="1.328675" x2="9.464040625" y2="1.346709375" layer="94"/>
<rectangle x1="9.8806" y1="1.328675" x2="10.11428125" y2="1.346709375" layer="94"/>
<rectangle x1="10.149840625" y1="1.328675" x2="10.67561875" y2="1.346709375" layer="94"/>
<rectangle x1="11.198859375" y1="1.328675" x2="11.8491" y2="1.346709375" layer="94"/>
<rectangle x1="12.30121875" y1="1.328675" x2="12.55521875" y2="1.346709375" layer="94"/>
<rectangle x1="13.007340625" y1="1.328675" x2="13.65758125" y2="1.346709375" layer="94"/>
<rectangle x1="14.183359375" y1="1.328675" x2="14.7066" y2="1.346709375" layer="94"/>
<rectangle x1="14.742159375" y1="1.328675" x2="14.97838125" y2="1.346709375" layer="94"/>
<rectangle x1="15.32128125" y1="1.328675" x2="15.57528125" y2="1.346709375" layer="94"/>
<rectangle x1="16.00708125" y1="1.328675" x2="16.5862" y2="1.346709375" layer="94"/>
<rectangle x1="17.0561" y1="1.328675" x2="17.726659375" y2="1.346709375" layer="94"/>
<rectangle x1="20.546059375" y1="1.328675" x2="21.3233" y2="1.346709375" layer="94"/>
<rectangle x1="5.68451875" y1="1.346709375" x2="5.93851875" y2="1.364996875" layer="94"/>
<rectangle x1="6.5532" y1="1.346709375" x2="7.114540625" y2="1.364996875" layer="94"/>
<rectangle x1="7.673340625" y1="1.346709375" x2="8.270240625" y2="1.364996875" layer="94"/>
<rectangle x1="8.6868" y1="1.346709375" x2="8.92048125" y2="1.364996875" layer="94"/>
<rectangle x1="8.976359375" y1="1.346709375" x2="9.464040625" y2="1.364996875" layer="94"/>
<rectangle x1="9.89838125" y1="1.346709375" x2="10.11428125" y2="1.364996875" layer="94"/>
<rectangle x1="10.187940625" y1="1.346709375" x2="10.67561875" y2="1.364996875" layer="94"/>
<rectangle x1="11.216640625" y1="1.346709375" x2="11.83131875" y2="1.364996875" layer="94"/>
<rectangle x1="12.30121875" y1="1.346709375" x2="12.55521875" y2="1.364996875" layer="94"/>
<rectangle x1="13.02511875" y1="1.346709375" x2="13.6398" y2="1.364996875" layer="94"/>
<rectangle x1="14.201140625" y1="1.346709375" x2="14.68881875" y2="1.364996875" layer="94"/>
<rectangle x1="14.742159375" y1="1.346709375" x2="14.97838125" y2="1.364996875" layer="94"/>
<rectangle x1="15.32128125" y1="1.346709375" x2="15.57528125" y2="1.364996875" layer="94"/>
<rectangle x1="16.0274" y1="1.346709375" x2="16.56841875" y2="1.364996875" layer="94"/>
<rectangle x1="17.07388125" y1="1.346709375" x2="17.70888125" y2="1.364996875" layer="94"/>
<rectangle x1="20.5994" y1="1.346709375" x2="21.358859375" y2="1.364996875" layer="94"/>
<rectangle x1="5.68451875" y1="1.364996875" x2="5.93851875" y2="1.38303125" layer="94"/>
<rectangle x1="6.57098125" y1="1.364996875" x2="7.096759375" y2="1.38303125" layer="94"/>
<rectangle x1="7.711440625" y1="1.364996875" x2="8.252459375" y2="1.38303125" layer="94"/>
<rectangle x1="8.6868" y1="1.364996875" x2="8.92048125" y2="1.38303125" layer="94"/>
<rectangle x1="8.994140625" y1="1.364996875" x2="9.446259375" y2="1.38303125" layer="94"/>
<rectangle x1="9.89838125" y1="1.364996875" x2="10.11428125" y2="1.38303125" layer="94"/>
<rectangle x1="10.20571875" y1="1.364996875" x2="10.657840625" y2="1.38303125" layer="94"/>
<rectangle x1="11.254740625" y1="1.364996875" x2="11.813540625" y2="1.38303125" layer="94"/>
<rectangle x1="12.30121875" y1="1.364996875" x2="12.55521875" y2="1.38303125" layer="94"/>
<rectangle x1="13.06068125" y1="1.364996875" x2="13.62201875" y2="1.38303125" layer="94"/>
<rectangle x1="14.2367" y1="1.364996875" x2="14.68881875" y2="1.38303125" layer="94"/>
<rectangle x1="14.742159375" y1="1.364996875" x2="14.97838125" y2="1.38303125" layer="94"/>
<rectangle x1="15.339059375" y1="1.364996875" x2="15.57528125" y2="1.38303125" layer="94"/>
<rectangle x1="16.04518125" y1="1.364996875" x2="16.550640625" y2="1.38303125" layer="94"/>
<rectangle x1="17.0942" y1="1.364996875" x2="17.688559375" y2="1.38303125" layer="94"/>
<rectangle x1="20.6375" y1="1.364996875" x2="21.414740625" y2="1.38303125" layer="94"/>
<rectangle x1="5.68451875" y1="1.38303125" x2="5.9563" y2="1.4010625" layer="94"/>
<rectangle x1="6.606540625" y1="1.38303125" x2="7.076440625" y2="1.4010625" layer="94"/>
<rectangle x1="7.72921875" y1="1.38303125" x2="8.23468125" y2="1.4010625" layer="94"/>
<rectangle x1="8.6868" y1="1.38303125" x2="8.9408" y2="1.4010625" layer="94"/>
<rectangle x1="9.0297" y1="1.38303125" x2="9.42848125" y2="1.4010625" layer="94"/>
<rectangle x1="9.89838125" y1="1.38303125" x2="10.11428125" y2="1.4010625" layer="94"/>
<rectangle x1="10.2235" y1="1.38303125" x2="10.640059375" y2="1.4010625" layer="94"/>
<rectangle x1="11.27251875" y1="1.38303125" x2="11.77798125" y2="1.4010625" layer="94"/>
<rectangle x1="12.321540625" y1="1.38303125" x2="12.55521875" y2="1.4010625" layer="94"/>
<rectangle x1="13.09878125" y1="1.38303125" x2="13.586459375" y2="1.4010625" layer="94"/>
<rectangle x1="14.25448125" y1="1.38303125" x2="14.671040625" y2="1.4010625" layer="94"/>
<rectangle x1="14.759940625" y1="1.38303125" x2="14.97838125" y2="1.4010625" layer="94"/>
<rectangle x1="15.339059375" y1="1.38303125" x2="15.57528125" y2="1.4010625" layer="94"/>
<rectangle x1="16.062959375" y1="1.38303125" x2="16.532859375" y2="1.4010625" layer="94"/>
<rectangle x1="17.11198125" y1="1.38303125" x2="17.67078125" y2="1.4010625" layer="94"/>
<rectangle x1="20.690840625" y1="1.38303125" x2="21.46808125" y2="1.4010625" layer="94"/>
<rectangle x1="5.704840625" y1="1.4010625" x2="5.9563" y2="1.419096875" layer="94"/>
<rectangle x1="6.626859375" y1="1.4010625" x2="7.04088125" y2="1.419096875" layer="94"/>
<rectangle x1="7.76478125" y1="1.4010625" x2="8.2169" y2="1.419096875" layer="94"/>
<rectangle x1="8.6868" y1="1.4010625" x2="8.9408" y2="1.419096875" layer="94"/>
<rectangle x1="9.04748125" y1="1.4010625" x2="9.4107" y2="1.419096875" layer="94"/>
<rectangle x1="9.89838125" y1="1.4010625" x2="10.132059375" y2="1.419096875" layer="94"/>
<rectangle x1="10.259059375" y1="1.4010625" x2="10.619740625" y2="1.419096875" layer="94"/>
<rectangle x1="11.30808125" y1="1.4010625" x2="11.7602" y2="1.419096875" layer="94"/>
<rectangle x1="12.321540625" y1="1.4010625" x2="12.55521875" y2="1.419096875" layer="94"/>
<rectangle x1="13.134340625" y1="1.4010625" x2="13.56868125" y2="1.419096875" layer="94"/>
<rectangle x1="14.290040625" y1="1.4010625" x2="14.63548125" y2="1.419096875" layer="94"/>
<rectangle x1="14.759940625" y1="1.4010625" x2="14.97838125" y2="1.419096875" layer="94"/>
<rectangle x1="15.339059375" y1="1.4010625" x2="15.57528125" y2="1.419096875" layer="94"/>
<rectangle x1="16.09851875" y1="1.4010625" x2="16.51508125" y2="1.419096875" layer="94"/>
<rectangle x1="17.147540625" y1="1.4010625" x2="17.653" y2="1.419096875" layer="94"/>
<rectangle x1="20.7264" y1="1.4010625" x2="21.52141875" y2="1.419096875" layer="94"/>
<rectangle x1="5.704840625" y1="1.419096875" x2="5.9563" y2="1.43713125" layer="94"/>
<rectangle x1="6.6802" y1="1.419096875" x2="7.00531875" y2="1.43713125" layer="94"/>
<rectangle x1="7.800340625" y1="1.419096875" x2="8.181340625" y2="1.43713125" layer="94"/>
<rectangle x1="8.6868" y1="1.419096875" x2="8.9408" y2="1.43713125" layer="94"/>
<rectangle x1="9.08558125" y1="1.419096875" x2="9.3726" y2="1.43713125" layer="94"/>
<rectangle x1="9.89838125" y1="1.419096875" x2="10.132059375" y2="1.43713125" layer="94"/>
<rectangle x1="10.29461875" y1="1.419096875" x2="10.58418125" y2="1.43713125" layer="94"/>
<rectangle x1="11.36141875" y1="1.419096875" x2="11.724640625" y2="1.43713125" layer="94"/>
<rectangle x1="12.321540625" y1="1.419096875" x2="12.55521875" y2="1.43713125" layer="94"/>
<rectangle x1="13.1699" y1="1.419096875" x2="13.53058125" y2="1.43713125" layer="94"/>
<rectangle x1="14.328140625" y1="1.419096875" x2="14.615159375" y2="1.43713125" layer="94"/>
<rectangle x1="14.759940625" y1="1.419096875" x2="14.996159375" y2="1.43713125" layer="94"/>
<rectangle x1="15.339059375" y1="1.419096875" x2="15.57528125" y2="1.43713125" layer="94"/>
<rectangle x1="16.13408125" y1="1.419096875" x2="16.47951875" y2="1.43713125" layer="94"/>
<rectangle x1="17.20088125" y1="1.419096875" x2="17.617440625" y2="1.43713125" layer="94"/>
<rectangle x1="20.779740625" y1="1.419096875" x2="21.55951875" y2="1.43713125" layer="94"/>
<rectangle x1="5.704840625" y1="1.43713125" x2="5.9563" y2="1.45541875" layer="94"/>
<rectangle x1="6.733540625" y1="1.43713125" x2="6.95198125" y2="1.45541875" layer="94"/>
<rectangle x1="7.874" y1="1.43713125" x2="8.10768125" y2="1.45541875" layer="94"/>
<rectangle x1="8.70458125" y1="1.43713125" x2="8.9408" y2="1.45541875" layer="94"/>
<rectangle x1="9.13891875" y1="1.43713125" x2="9.337040625" y2="1.45541875" layer="94"/>
<rectangle x1="10.3505" y1="1.43713125" x2="10.54861875" y2="1.45541875" layer="94"/>
<rectangle x1="11.4173" y1="1.43713125" x2="11.668759375" y2="1.45541875" layer="94"/>
<rectangle x1="12.321540625" y1="1.43713125" x2="12.573" y2="1.45541875" layer="94"/>
<rectangle x1="13.223240625" y1="1.43713125" x2="13.477240625" y2="1.45541875" layer="94"/>
<rectangle x1="14.38148125" y1="1.43713125" x2="14.56181875" y2="1.45541875" layer="94"/>
<rectangle x1="16.189959375" y1="1.43713125" x2="16.423640625" y2="1.45541875" layer="94"/>
<rectangle x1="17.256759375" y1="1.43713125" x2="17.54378125" y2="1.45541875" layer="94"/>
<rectangle x1="20.83561875" y1="1.43713125" x2="21.612859375" y2="1.45541875" layer="94"/>
<rectangle x1="3.533140625" y1="1.45541875" x2="3.787140625" y2="1.473453125" layer="94"/>
<rectangle x1="5.704840625" y1="1.45541875" x2="5.9563" y2="1.473453125" layer="94"/>
<rectangle x1="8.70458125" y1="1.45541875" x2="8.9408" y2="1.473453125" layer="94"/>
<rectangle x1="12.321540625" y1="1.45541875" x2="12.573" y2="1.473453125" layer="94"/>
<rectangle x1="20.87118125" y1="1.45541875" x2="21.64841875" y2="1.473453125" layer="94"/>
<rectangle x1="3.3909" y1="1.473453125" x2="3.93191875" y2="1.4914875" layer="94"/>
<rectangle x1="5.704840625" y1="1.473453125" x2="5.9563" y2="1.4914875" layer="94"/>
<rectangle x1="8.70458125" y1="1.473453125" x2="8.9408" y2="1.4914875" layer="94"/>
<rectangle x1="12.321540625" y1="1.473453125" x2="12.573" y2="1.4914875" layer="94"/>
<rectangle x1="20.92451875" y1="1.473453125" x2="21.701759375" y2="1.4914875" layer="94"/>
<rectangle x1="3.299459375" y1="1.4914875" x2="4.023359375" y2="1.509521875" layer="94"/>
<rectangle x1="5.704840625" y1="1.4914875" x2="5.97408125" y2="1.509521875" layer="94"/>
<rectangle x1="8.70458125" y1="1.4914875" x2="8.95858125" y2="1.509521875" layer="94"/>
<rectangle x1="12.33931875" y1="1.4914875" x2="12.573" y2="1.509521875" layer="94"/>
<rectangle x1="20.96261875" y1="1.4914875" x2="21.739859375" y2="1.509521875" layer="94"/>
<rectangle x1="3.2639" y1="1.509521875" x2="4.09448125" y2="1.52755625" layer="94"/>
<rectangle x1="5.704840625" y1="1.509521875" x2="5.97408125" y2="1.52755625" layer="94"/>
<rectangle x1="8.70458125" y1="1.509521875" x2="8.95858125" y2="1.52755625" layer="94"/>
<rectangle x1="12.33931875" y1="1.509521875" x2="12.573" y2="1.52755625" layer="94"/>
<rectangle x1="21.015959375" y1="1.509521875" x2="21.7932" y2="1.52755625" layer="94"/>
<rectangle x1="3.2639" y1="1.52755625" x2="4.14781875" y2="1.545590625" layer="94"/>
<rectangle x1="5.72261875" y1="1.52755625" x2="5.97408125" y2="1.545590625" layer="94"/>
<rectangle x1="8.70458125" y1="1.52755625" x2="8.95858125" y2="1.545590625" layer="94"/>
<rectangle x1="12.33931875" y1="1.52755625" x2="12.573" y2="1.545590625" layer="94"/>
<rectangle x1="21.05151875" y1="1.52755625" x2="21.828759375" y2="1.545590625" layer="94"/>
<rectangle x1="3.28168125" y1="1.545590625" x2="4.2037" y2="1.563878125" layer="94"/>
<rectangle x1="5.72261875" y1="1.545590625" x2="5.97408125" y2="1.563878125" layer="94"/>
<rectangle x1="8.722359375" y1="1.545590625" x2="8.95858125" y2="1.563878125" layer="94"/>
<rectangle x1="12.33931875" y1="1.545590625" x2="12.59078125" y2="1.563878125" layer="94"/>
<rectangle x1="21.08708125" y1="1.545590625" x2="21.884640625" y2="1.563878125" layer="94"/>
<rectangle x1="3.28168125" y1="1.563878125" x2="4.257040625" y2="1.5819125" layer="94"/>
<rectangle x1="5.72261875" y1="1.563878125" x2="5.97408125" y2="1.5819125" layer="94"/>
<rectangle x1="8.722359375" y1="1.563878125" x2="8.95858125" y2="1.5819125" layer="94"/>
<rectangle x1="12.33931875" y1="1.563878125" x2="12.59078125" y2="1.5819125" layer="94"/>
<rectangle x1="21.142959375" y1="1.563878125" x2="21.9202" y2="1.5819125" layer="94"/>
<rectangle x1="3.28168125" y1="1.5819125" x2="4.2926" y2="1.599946875" layer="94"/>
<rectangle x1="5.72261875" y1="1.5819125" x2="5.991859375" y2="1.599946875" layer="94"/>
<rectangle x1="8.722359375" y1="1.5819125" x2="8.95858125" y2="1.599946875" layer="94"/>
<rectangle x1="12.33931875" y1="1.5819125" x2="12.59078125" y2="1.599946875" layer="94"/>
<rectangle x1="15.356840625" y1="1.5819125" x2="15.610840625" y2="1.599946875" layer="94"/>
<rectangle x1="21.17851875" y1="1.5819125" x2="21.955759375" y2="1.599946875" layer="94"/>
<rectangle x1="3.28168125" y1="1.599946875" x2="4.3307" y2="1.61798125" layer="94"/>
<rectangle x1="5.34161875" y1="1.599946875" x2="6.372859375" y2="1.61798125" layer="94"/>
<rectangle x1="8.722359375" y1="1.599946875" x2="8.95858125" y2="1.61798125" layer="94"/>
<rectangle x1="12.33931875" y1="1.599946875" x2="12.59078125" y2="1.61798125" layer="94"/>
<rectangle x1="15.37461875" y1="1.599946875" x2="15.610840625" y2="1.61798125" layer="94"/>
<rectangle x1="21.21408125" y1="1.599946875" x2="21.99131875" y2="1.61798125" layer="94"/>
<rectangle x1="3.299459375" y1="1.61798125" x2="4.366259375" y2="1.6360125" layer="94"/>
<rectangle x1="5.34161875" y1="1.61798125" x2="6.372859375" y2="1.6360125" layer="94"/>
<rectangle x1="8.722359375" y1="1.61798125" x2="8.976359375" y2="1.6360125" layer="94"/>
<rectangle x1="12.3571" y1="1.61798125" x2="12.59078125" y2="1.6360125" layer="94"/>
<rectangle x1="15.37461875" y1="1.61798125" x2="15.610840625" y2="1.6360125" layer="94"/>
<rectangle x1="21.269959375" y1="1.61798125" x2="22.0472" y2="1.6360125" layer="94"/>
<rectangle x1="3.299459375" y1="1.6360125" x2="4.40181875" y2="1.654046875" layer="94"/>
<rectangle x1="5.34161875" y1="1.6360125" x2="6.372859375" y2="1.654046875" layer="94"/>
<rectangle x1="8.722359375" y1="1.6360125" x2="8.976359375" y2="1.654046875" layer="94"/>
<rectangle x1="12.3571" y1="1.6360125" x2="12.59078125" y2="1.654046875" layer="94"/>
<rectangle x1="15.37461875" y1="1.6360125" x2="15.610840625" y2="1.654046875" layer="94"/>
<rectangle x1="21.30551875" y1="1.6360125" x2="22.082759375" y2="1.654046875" layer="94"/>
<rectangle x1="3.299459375" y1="1.654046875" x2="4.4196" y2="1.6723375" layer="94"/>
<rectangle x1="5.34161875" y1="1.654046875" x2="6.372859375" y2="1.6723375" layer="94"/>
<rectangle x1="8.722359375" y1="1.654046875" x2="8.976359375" y2="1.6723375" layer="94"/>
<rectangle x1="12.3571" y1="1.654046875" x2="12.59078125" y2="1.6723375" layer="94"/>
<rectangle x1="15.37461875" y1="1.654046875" x2="15.610840625" y2="1.6723375" layer="94"/>
<rectangle x1="21.34108125" y1="1.654046875" x2="22.11831875" y2="1.6723375" layer="94"/>
<rectangle x1="3.299459375" y1="1.6723375" x2="4.455159375" y2="1.69036875" layer="94"/>
<rectangle x1="5.3594" y1="1.6723375" x2="6.372859375" y2="1.69036875" layer="94"/>
<rectangle x1="8.740140625" y1="1.6723375" x2="8.976359375" y2="1.69036875" layer="94"/>
<rectangle x1="12.3571" y1="1.6723375" x2="12.608559375" y2="1.69036875" layer="94"/>
<rectangle x1="15.37461875" y1="1.6723375" x2="15.62861875" y2="1.69036875" layer="94"/>
<rectangle x1="21.376640625" y1="1.6723375" x2="22.15388125" y2="1.69036875" layer="94"/>
<rectangle x1="3.317240625" y1="1.69036875" x2="4.47548125" y2="1.708403125" layer="94"/>
<rectangle x1="5.3594" y1="1.69036875" x2="6.390640625" y2="1.708403125" layer="94"/>
<rectangle x1="8.740140625" y1="1.69036875" x2="8.976359375" y2="1.708403125" layer="94"/>
<rectangle x1="12.3571" y1="1.69036875" x2="12.608559375" y2="1.708403125" layer="94"/>
<rectangle x1="15.37461875" y1="1.69036875" x2="15.62861875" y2="1.708403125" layer="94"/>
<rectangle x1="21.414740625" y1="1.69036875" x2="22.19198125" y2="1.708403125" layer="94"/>
<rectangle x1="3.317240625" y1="1.708403125" x2="4.511040625" y2="1.7264375" layer="94"/>
<rectangle x1="5.3594" y1="1.708403125" x2="6.390640625" y2="1.7264375" layer="94"/>
<rectangle x1="8.740140625" y1="1.708403125" x2="8.976359375" y2="1.7264375" layer="94"/>
<rectangle x1="12.3571" y1="1.708403125" x2="12.608559375" y2="1.7264375" layer="94"/>
<rectangle x1="15.37461875" y1="1.708403125" x2="15.62861875" y2="1.7264375" layer="94"/>
<rectangle x1="21.4503" y1="1.708403125" x2="22.227540625" y2="1.7264375" layer="94"/>
<rectangle x1="3.317240625" y1="1.7264375" x2="4.52881875" y2="1.744471875" layer="94"/>
<rectangle x1="5.3594" y1="1.7264375" x2="6.390640625" y2="1.744471875" layer="94"/>
<rectangle x1="8.740140625" y1="1.7264375" x2="8.994140625" y2="1.744471875" layer="94"/>
<rectangle x1="12.37488125" y1="1.7264375" x2="12.608559375" y2="1.744471875" layer="94"/>
<rectangle x1="15.3924" y1="1.7264375" x2="15.62861875" y2="1.744471875" layer="94"/>
<rectangle x1="21.485859375" y1="1.7264375" x2="22.28088125" y2="1.744471875" layer="94"/>
<rectangle x1="3.317240625" y1="1.744471875" x2="4.5466" y2="1.762759375" layer="94"/>
<rectangle x1="5.3594" y1="1.744471875" x2="6.390640625" y2="1.762759375" layer="94"/>
<rectangle x1="8.740140625" y1="1.744471875" x2="8.994140625" y2="1.762759375" layer="94"/>
<rectangle x1="12.37488125" y1="1.744471875" x2="12.608559375" y2="1.762759375" layer="94"/>
<rectangle x1="15.3924" y1="1.744471875" x2="15.62861875" y2="1.762759375" layer="94"/>
<rectangle x1="21.52141875" y1="1.744471875" x2="22.316440625" y2="1.762759375" layer="94"/>
<rectangle x1="3.33501875" y1="1.762759375" x2="4.56438125" y2="1.78079375" layer="94"/>
<rectangle x1="5.3594" y1="1.762759375" x2="6.390640625" y2="1.78079375" layer="94"/>
<rectangle x1="8.740140625" y1="1.762759375" x2="8.994140625" y2="1.78079375" layer="94"/>
<rectangle x1="12.37488125" y1="1.762759375" x2="12.608559375" y2="1.78079375" layer="94"/>
<rectangle x1="15.3924" y1="1.762759375" x2="15.62861875" y2="1.78079375" layer="94"/>
<rectangle x1="21.55951875" y1="1.762759375" x2="22.354540625" y2="1.78079375" layer="94"/>
<rectangle x1="3.33501875" y1="1.78079375" x2="4.599940625" y2="1.798828125" layer="94"/>
<rectangle x1="5.37718125" y1="1.78079375" x2="6.390640625" y2="1.798828125" layer="94"/>
<rectangle x1="8.75791875" y1="1.78079375" x2="8.994140625" y2="1.798828125" layer="94"/>
<rectangle x1="12.37488125" y1="1.78079375" x2="12.62888125" y2="1.798828125" layer="94"/>
<rectangle x1="15.3924" y1="1.78079375" x2="15.6464" y2="1.798828125" layer="94"/>
<rectangle x1="21.59508125" y1="1.78079375" x2="22.37231875" y2="1.798828125" layer="94"/>
<rectangle x1="3.33501875" y1="1.798828125" x2="4.620259375" y2="1.8168625" layer="94"/>
<rectangle x1="5.37718125" y1="1.798828125" x2="6.390640625" y2="1.8168625" layer="94"/>
<rectangle x1="8.75791875" y1="1.798828125" x2="8.994140625" y2="1.8168625" layer="94"/>
<rectangle x1="12.37488125" y1="1.798828125" x2="12.62888125" y2="1.8168625" layer="94"/>
<rectangle x1="15.3924" y1="1.798828125" x2="15.6464" y2="1.8168625" layer="94"/>
<rectangle x1="21.630640625" y1="1.798828125" x2="22.40788125" y2="1.8168625" layer="94"/>
<rectangle x1="3.33501875" y1="1.8168625" x2="4.638040625" y2="1.834896875" layer="94"/>
<rectangle x1="5.37718125" y1="1.8168625" x2="6.40841875" y2="1.834896875" layer="94"/>
<rectangle x1="8.75791875" y1="1.8168625" x2="8.994140625" y2="1.834896875" layer="94"/>
<rectangle x1="12.37488125" y1="1.8168625" x2="12.62888125" y2="1.834896875" layer="94"/>
<rectangle x1="15.3924" y1="1.8168625" x2="15.6464" y2="1.834896875" layer="94"/>
<rectangle x1="21.6662" y1="1.8168625" x2="22.443440625" y2="1.834896875" layer="94"/>
<rectangle x1="3.3528" y1="1.834896875" x2="4.65581875" y2="1.85293125" layer="94"/>
<rectangle x1="21.701759375" y1="1.834896875" x2="22.481540625" y2="1.85293125" layer="94"/>
<rectangle x1="3.3528" y1="1.85293125" x2="4.6736" y2="1.87121875" layer="94"/>
<rectangle x1="21.739859375" y1="1.85293125" x2="22.5171" y2="1.87121875" layer="94"/>
<rectangle x1="3.3528" y1="1.87121875" x2="4.69138125" y2="1.889253125" layer="94"/>
<rectangle x1="21.77541875" y1="1.87121875" x2="22.552659375" y2="1.889253125" layer="94"/>
<rectangle x1="3.37058125" y1="1.889253125" x2="4.709159375" y2="1.9072875" layer="94"/>
<rectangle x1="21.7932" y1="1.889253125" x2="22.570440625" y2="1.9072875" layer="94"/>
<rectangle x1="3.37058125" y1="1.9072875" x2="4.726940625" y2="1.92531875" layer="94"/>
<rectangle x1="21.828759375" y1="1.9072875" x2="22.606" y2="1.92531875" layer="94"/>
<rectangle x1="3.37058125" y1="1.92531875" x2="4.74471875" y2="1.943353125" layer="94"/>
<rectangle x1="21.866859375" y1="1.92531875" x2="22.6441" y2="1.943353125" layer="94"/>
<rectangle x1="3.37058125" y1="1.943353125" x2="4.7625" y2="1.9613875" layer="94"/>
<rectangle x1="21.884640625" y1="1.943353125" x2="22.679659375" y2="1.9613875" layer="94"/>
<rectangle x1="3.3909" y1="1.9613875" x2="4.7625" y2="1.979675" layer="94"/>
<rectangle x1="21.9202" y1="1.9613875" x2="22.71521875" y2="1.979675" layer="94"/>
<rectangle x1="3.3909" y1="1.979675" x2="4.78281875" y2="1.997709375" layer="94"/>
<rectangle x1="21.955759375" y1="1.979675" x2="22.733" y2="1.997709375" layer="94"/>
<rectangle x1="3.3909" y1="1.997709375" x2="4.8006" y2="2.01574375" layer="94"/>
<rectangle x1="21.973540625" y1="1.997709375" x2="22.768559375" y2="2.01574375" layer="94"/>
<rectangle x1="3.3909" y1="2.01574375" x2="4.81838125" y2="2.033778125" layer="94"/>
<rectangle x1="22.0091" y1="2.01574375" x2="22.78888125" y2="2.033778125" layer="94"/>
<rectangle x1="3.40868125" y1="2.033778125" x2="4.836159375" y2="2.0518125" layer="94"/>
<rectangle x1="22.0472" y1="2.033778125" x2="22.824440625" y2="2.0518125" layer="94"/>
<rectangle x1="3.40868125" y1="2.0518125" x2="4.853940625" y2="2.069846875" layer="94"/>
<rectangle x1="22.06498125" y1="2.0518125" x2="22.84221875" y2="2.069846875" layer="94"/>
<rectangle x1="3.40868125" y1="2.069846875" x2="4.853940625" y2="2.088134375" layer="94"/>
<rectangle x1="22.100540625" y1="2.069846875" x2="22.87778125" y2="2.088134375" layer="94"/>
<rectangle x1="3.40868125" y1="2.088134375" x2="4.87171875" y2="2.10616875" layer="94"/>
<rectangle x1="22.11831875" y1="2.088134375" x2="22.913340625" y2="2.10616875" layer="94"/>
<rectangle x1="3.426459375" y1="2.10616875" x2="3.553459375" y2="2.124203125" layer="94"/>
<rectangle x1="3.8608" y1="2.10616875" x2="4.8895" y2="2.124203125" layer="94"/>
<rectangle x1="22.1361" y1="2.10616875" x2="22.93111875" y2="2.124203125" layer="94"/>
<rectangle x1="3.426459375" y1="2.124203125" x2="3.46201875" y2="2.1422375" layer="94"/>
<rectangle x1="3.93191875" y1="2.124203125" x2="4.90728125" y2="2.1422375" layer="94"/>
<rectangle x1="22.1742" y1="2.124203125" x2="22.951440625" y2="2.1422375" layer="94"/>
<rectangle x1="3.985259375" y1="2.1422375" x2="4.9276" y2="2.16026875" layer="94"/>
<rectangle x1="22.19198125" y1="2.1422375" x2="22.987" y2="2.16026875" layer="94"/>
<rectangle x1="4.023359375" y1="2.16026875" x2="4.9276" y2="2.178559375" layer="94"/>
<rectangle x1="22.227540625" y1="2.16026875" x2="23.022559375" y2="2.178559375" layer="94"/>
<rectangle x1="4.05891875" y1="2.178559375" x2="4.94538125" y2="2.196590625" layer="94"/>
<rectangle x1="22.24531875" y1="2.178559375" x2="23.040340625" y2="2.196590625" layer="94"/>
<rectangle x1="4.09448125" y1="2.196590625" x2="4.963159375" y2="2.214625" layer="94"/>
<rectangle x1="22.28088125" y1="2.196590625" x2="23.05811875" y2="2.214625" layer="94"/>
<rectangle x1="4.112259375" y1="2.214625" x2="4.980940625" y2="2.232659375" layer="94"/>
<rectangle x1="22.298659375" y1="2.214625" x2="23.09621875" y2="2.232659375" layer="94"/>
<rectangle x1="4.130040625" y1="2.232659375" x2="4.980940625" y2="2.25069375" layer="94"/>
<rectangle x1="22.316440625" y1="2.232659375" x2="23.114" y2="2.25069375" layer="94"/>
<rectangle x1="4.168140625" y1="2.25069375" x2="4.99871875" y2="2.268728125" layer="94"/>
<rectangle x1="22.336759375" y1="2.25069375" x2="23.13178125" y2="2.268728125" layer="94"/>
<rectangle x1="4.18591875" y1="2.268728125" x2="5.0165" y2="2.287015625" layer="94"/>
<rectangle x1="22.354540625" y1="2.268728125" x2="23.149559375" y2="2.287015625" layer="94"/>
<rectangle x1="4.2037" y1="2.287015625" x2="5.03428125" y2="2.30505" layer="94"/>
<rectangle x1="22.3901" y1="2.287015625" x2="23.18511875" y2="2.30505" layer="94"/>
<rectangle x1="4.22148125" y1="2.30505" x2="5.03428125" y2="2.323084375" layer="94"/>
<rectangle x1="22.40788125" y1="2.30505" x2="23.2029" y2="2.323084375" layer="94"/>
<rectangle x1="4.239259375" y1="2.323084375" x2="5.052059375" y2="2.34111875" layer="94"/>
<rectangle x1="22.425659375" y1="2.323084375" x2="23.22068125" y2="2.34111875" layer="94"/>
<rectangle x1="4.257040625" y1="2.34111875" x2="5.069840625" y2="2.359153125" layer="94"/>
<rectangle x1="22.46121875" y1="2.34111875" x2="23.25878125" y2="2.359153125" layer="94"/>
<rectangle x1="4.27481875" y1="2.359153125" x2="5.069840625" y2="2.3771875" layer="94"/>
<rectangle x1="22.481540625" y1="2.359153125" x2="23.276559375" y2="2.3771875" layer="94"/>
<rectangle x1="4.2926" y1="2.3771875" x2="5.090159375" y2="2.395475" layer="94"/>
<rectangle x1="22.49931875" y1="2.3771875" x2="23.294340625" y2="2.395475" layer="94"/>
<rectangle x1="4.31291875" y1="2.395475" x2="5.107940625" y2="2.413509375" layer="94"/>
<rectangle x1="22.5171" y1="2.395475" x2="23.31211875" y2="2.413509375" layer="94"/>
<rectangle x1="4.3307" y1="2.413509375" x2="5.12571875" y2="2.431540625" layer="94"/>
<rectangle x1="22.53488125" y1="2.413509375" x2="23.3299" y2="2.431540625" layer="94"/>
<rectangle x1="4.3307" y1="2.431540625" x2="5.12571875" y2="2.449575" layer="94"/>
<rectangle x1="22.552659375" y1="2.431540625" x2="23.34768125" y2="2.449575" layer="94"/>
<rectangle x1="4.34848125" y1="2.449575" x2="5.1435" y2="2.467609375" layer="94"/>
<rectangle x1="22.570440625" y1="2.449575" x2="23.365459375" y2="2.467609375" layer="94"/>
<rectangle x1="4.366259375" y1="2.467609375" x2="5.16128125" y2="2.48564375" layer="94"/>
<rectangle x1="22.58821875" y1="2.467609375" x2="23.383240625" y2="2.48564375" layer="94"/>
<rectangle x1="4.384040625" y1="2.48564375" x2="5.16128125" y2="2.50393125" layer="94"/>
<rectangle x1="22.606" y1="2.48564375" x2="23.421340625" y2="2.50393125" layer="94"/>
<rectangle x1="4.40181875" y1="2.50393125" x2="5.179059375" y2="2.521965625" layer="94"/>
<rectangle x1="22.62378125" y1="2.50393125" x2="23.43911875" y2="2.521965625" layer="94"/>
<rectangle x1="4.40181875" y1="2.521965625" x2="5.196840625" y2="2.54" layer="94"/>
<rectangle x1="22.6441" y1="2.521965625" x2="23.43911875" y2="2.54" layer="94"/>
<rectangle x1="4.4196" y1="2.54" x2="5.196840625" y2="2.558034375" layer="94"/>
<rectangle x1="22.66188125" y1="2.54" x2="23.47468125" y2="2.558034375" layer="94"/>
<rectangle x1="4.43738125" y1="2.558034375" x2="5.21461875" y2="2.57606875" layer="94"/>
<rectangle x1="22.679659375" y1="2.558034375" x2="23.47468125" y2="2.57606875" layer="94"/>
<rectangle x1="4.455159375" y1="2.57606875" x2="5.2324" y2="2.59435625" layer="94"/>
<rectangle x1="22.697440625" y1="2.57606875" x2="23.492459375" y2="2.59435625" layer="94"/>
<rectangle x1="4.455159375" y1="2.59435625" x2="5.2324" y2="2.612390625" layer="94"/>
<rectangle x1="22.71521875" y1="2.59435625" x2="23.510240625" y2="2.612390625" layer="94"/>
<rectangle x1="4.47548125" y1="2.612390625" x2="5.25271875" y2="2.630425" layer="94"/>
<rectangle x1="22.733" y1="2.612390625" x2="23.52801875" y2="2.630425" layer="94"/>
<rectangle x1="1.038859375" y1="2.630425" x2="1.3462" y2="2.648459375" layer="94"/>
<rectangle x1="4.493259375" y1="2.630425" x2="5.2705" y2="2.648459375" layer="94"/>
<rectangle x1="7.800340625" y1="2.630425" x2="7.94511875" y2="2.648459375" layer="94"/>
<rectangle x1="12.827" y1="2.630425" x2="13.007340625" y2="2.648459375" layer="94"/>
<rectangle x1="22.733" y1="2.630425" x2="23.5458" y2="2.648459375" layer="94"/>
<rectangle x1="0.89408125" y1="2.648459375" x2="1.508759375" y2="2.666490625" layer="94"/>
<rectangle x1="4.493259375" y1="2.648459375" x2="5.28828125" y2="2.666490625" layer="94"/>
<rectangle x1="7.673340625" y1="2.648459375" x2="8.125459375" y2="2.666490625" layer="94"/>
<rectangle x1="12.68221875" y1="2.648459375" x2="13.18768125" y2="2.666490625" layer="94"/>
<rectangle x1="22.75078125" y1="2.648459375" x2="23.56611875" y2="2.666490625" layer="94"/>
<rectangle x1="0.80518125" y1="2.666490625" x2="1.61798125" y2="2.684525" layer="94"/>
<rectangle x1="4.511040625" y1="2.666490625" x2="5.28828125" y2="2.684525" layer="94"/>
<rectangle x1="7.60221875" y1="2.666490625" x2="8.23468125" y2="2.684525" layer="94"/>
<rectangle x1="12.59078125" y1="2.666490625" x2="13.27911875" y2="2.684525" layer="94"/>
<rectangle x1="22.768559375" y1="2.666490625" x2="23.5839" y2="2.684525" layer="94"/>
<rectangle x1="0.73151875" y1="2.684525" x2="1.6891" y2="2.7028125" layer="94"/>
<rectangle x1="4.511040625" y1="2.684525" x2="5.306059375" y2="2.7028125" layer="94"/>
<rectangle x1="7.566659375" y1="2.684525" x2="8.3058" y2="2.7028125" layer="94"/>
<rectangle x1="12.537440625" y1="2.684525" x2="13.36801875" y2="2.7028125" layer="94"/>
<rectangle x1="22.78888125" y1="2.684525" x2="23.60168125" y2="2.7028125" layer="94"/>
<rectangle x1="0.67818125" y1="2.7028125" x2="1.762759375" y2="2.720846875" layer="94"/>
<rectangle x1="4.52881875" y1="2.7028125" x2="5.323840625" y2="2.720846875" layer="94"/>
<rectangle x1="7.528559375" y1="2.7028125" x2="8.379459375" y2="2.720846875" layer="94"/>
<rectangle x1="9.210040625" y1="2.7028125" x2="9.95171875" y2="2.720846875" layer="94"/>
<rectangle x1="12.4841" y1="2.7028125" x2="13.4239" y2="2.720846875" layer="94"/>
<rectangle x1="15.28571875" y1="2.7028125" x2="16.0274" y2="2.720846875" layer="94"/>
<rectangle x1="17.11198125" y1="2.7028125" x2="17.85111875" y2="2.720846875" layer="94"/>
<rectangle x1="22.806659375" y1="2.7028125" x2="23.619459375" y2="2.720846875" layer="94"/>
<rectangle x1="0.624840625" y1="2.720846875" x2="1.8161" y2="2.73888125" layer="94"/>
<rectangle x1="4.52881875" y1="2.720846875" x2="5.323840625" y2="2.73888125" layer="94"/>
<rectangle x1="7.493" y1="2.720846875" x2="8.4328" y2="2.73888125" layer="94"/>
<rectangle x1="9.22781875" y1="2.720846875" x2="9.9695" y2="2.73888125" layer="94"/>
<rectangle x1="12.42821875" y1="2.720846875" x2="13.477240625" y2="2.73888125" layer="94"/>
<rectangle x1="15.3035" y1="2.720846875" x2="16.04518125" y2="2.73888125" layer="94"/>
<rectangle x1="17.129759375" y1="2.720846875" x2="17.871440625" y2="2.73888125" layer="94"/>
<rectangle x1="22.806659375" y1="2.720846875" x2="23.619459375" y2="2.73888125" layer="94"/>
<rectangle x1="0.586740625" y1="2.73888125" x2="1.87198125" y2="2.756915625" layer="94"/>
<rectangle x1="4.52881875" y1="2.73888125" x2="5.34161875" y2="2.756915625" layer="94"/>
<rectangle x1="7.47521875" y1="2.73888125" x2="8.4709" y2="2.756915625" layer="94"/>
<rectangle x1="9.22781875" y1="2.73888125" x2="9.9695" y2="2.756915625" layer="94"/>
<rectangle x1="12.392659375" y1="2.73888125" x2="13.53058125" y2="2.756915625" layer="94"/>
<rectangle x1="15.3035" y1="2.73888125" x2="16.04518125" y2="2.756915625" layer="94"/>
<rectangle x1="17.129759375" y1="2.73888125" x2="17.871440625" y2="2.756915625" layer="94"/>
<rectangle x1="22.824440625" y1="2.73888125" x2="23.637240625" y2="2.756915625" layer="94"/>
<rectangle x1="0.55118125" y1="2.756915625" x2="1.92531875" y2="2.77495" layer="94"/>
<rectangle x1="4.52881875" y1="2.756915625" x2="5.3594" y2="2.77495" layer="94"/>
<rectangle x1="7.457440625" y1="2.756915625" x2="8.506459375" y2="2.77495" layer="94"/>
<rectangle x1="9.248140625" y1="2.756915625" x2="9.98728125" y2="2.77495" layer="94"/>
<rectangle x1="12.3571" y1="2.756915625" x2="13.586459375" y2="2.77495" layer="94"/>
<rectangle x1="15.32128125" y1="2.756915625" x2="16.062959375" y2="2.77495" layer="94"/>
<rectangle x1="17.147540625" y1="2.756915625" x2="17.88921875" y2="2.77495" layer="94"/>
<rectangle x1="22.84221875" y1="2.756915625" x2="23.65501875" y2="2.77495" layer="94"/>
<rectangle x1="0.51561875" y1="2.77495" x2="1.978659375" y2="2.792984375" layer="94"/>
<rectangle x1="4.52881875" y1="2.77495" x2="5.3594" y2="2.792984375" layer="94"/>
<rectangle x1="7.439659375" y1="2.77495" x2="8.506459375" y2="2.792984375" layer="94"/>
<rectangle x1="9.248140625" y1="2.77495" x2="9.98728125" y2="2.792984375" layer="94"/>
<rectangle x1="12.321540625" y1="2.77495" x2="13.62201875" y2="2.792984375" layer="94"/>
<rectangle x1="15.32128125" y1="2.77495" x2="16.062959375" y2="2.792984375" layer="94"/>
<rectangle x1="17.147540625" y1="2.77495" x2="17.88921875" y2="2.792984375" layer="94"/>
<rectangle x1="22.86" y1="2.77495" x2="23.6728" y2="2.792984375" layer="94"/>
<rectangle x1="0.497840625" y1="2.792984375" x2="2.016759375" y2="2.811271875" layer="94"/>
<rectangle x1="4.52881875" y1="2.792984375" x2="5.37718125" y2="2.811271875" layer="94"/>
<rectangle x1="7.42188125" y1="2.792984375" x2="8.506459375" y2="2.811271875" layer="94"/>
<rectangle x1="9.248140625" y1="2.792984375" x2="9.98728125" y2="2.811271875" layer="94"/>
<rectangle x1="12.30121875" y1="2.792984375" x2="13.65758125" y2="2.811271875" layer="94"/>
<rectangle x1="15.32128125" y1="2.792984375" x2="16.062959375" y2="2.811271875" layer="94"/>
<rectangle x1="17.147540625" y1="2.792984375" x2="17.88921875" y2="2.811271875" layer="94"/>
<rectangle x1="22.86" y1="2.792984375" x2="23.6728" y2="2.811271875" layer="94"/>
<rectangle x1="0.459740625" y1="2.811271875" x2="2.0701" y2="2.82930625" layer="94"/>
<rectangle x1="4.52881875" y1="2.811271875" x2="5.3975" y2="2.82930625" layer="94"/>
<rectangle x1="7.4041" y1="2.811271875" x2="8.524240625" y2="2.82930625" layer="94"/>
<rectangle x1="9.26591875" y1="2.811271875" x2="10.0076" y2="2.82930625" layer="94"/>
<rectangle x1="12.265659375" y1="2.811271875" x2="13.693140625" y2="2.82930625" layer="94"/>
<rectangle x1="15.339059375" y1="2.811271875" x2="16.080740625" y2="2.82930625" layer="94"/>
<rectangle x1="17.16531875" y1="2.811271875" x2="17.907" y2="2.82930625" layer="94"/>
<rectangle x1="22.87778125" y1="2.811271875" x2="23.69058125" y2="2.82930625" layer="94"/>
<rectangle x1="0.42418125" y1="2.82930625" x2="2.105659375" y2="2.847340625" layer="94"/>
<rectangle x1="4.52881875" y1="2.82930625" x2="5.41528125" y2="2.847340625" layer="94"/>
<rectangle x1="7.38378125" y1="2.82930625" x2="8.524240625" y2="2.847340625" layer="94"/>
<rectangle x1="9.26591875" y1="2.82930625" x2="10.0076" y2="2.847340625" layer="94"/>
<rectangle x1="12.24788125" y1="2.82930625" x2="13.731240625" y2="2.847340625" layer="94"/>
<rectangle x1="15.339059375" y1="2.82930625" x2="16.080740625" y2="2.847340625" layer="94"/>
<rectangle x1="17.16531875" y1="2.82930625" x2="17.907" y2="2.847340625" layer="94"/>
<rectangle x1="22.87778125" y1="2.82930625" x2="23.7109" y2="2.847340625" layer="94"/>
<rectangle x1="0.4064" y1="2.847340625" x2="2.14121875" y2="2.865375" layer="94"/>
<rectangle x1="4.52881875" y1="2.847340625" x2="5.41528125" y2="2.865375" layer="94"/>
<rectangle x1="7.38378125" y1="2.847340625" x2="8.524240625" y2="2.865375" layer="94"/>
<rectangle x1="9.26591875" y1="2.847340625" x2="10.0076" y2="2.865375" layer="94"/>
<rectangle x1="12.21231875" y1="2.847340625" x2="13.7668" y2="2.865375" layer="94"/>
<rectangle x1="15.339059375" y1="2.847340625" x2="16.080740625" y2="2.865375" layer="94"/>
<rectangle x1="17.16531875" y1="2.847340625" x2="17.907" y2="2.865375" layer="94"/>
<rectangle x1="22.895559375" y1="2.847340625" x2="23.72868125" y2="2.865375" layer="94"/>
<rectangle x1="0.38861875" y1="2.865375" x2="2.17931875" y2="2.883409375" layer="94"/>
<rectangle x1="4.52881875" y1="2.865375" x2="5.433059375" y2="2.883409375" layer="94"/>
<rectangle x1="7.38378125" y1="2.865375" x2="8.54201875" y2="2.883409375" layer="94"/>
<rectangle x1="9.2837" y1="2.865375" x2="10.02538125" y2="2.883409375" layer="94"/>
<rectangle x1="12.194540625" y1="2.865375" x2="13.802359375" y2="2.883409375" layer="94"/>
<rectangle x1="15.356840625" y1="2.865375" x2="16.09851875" y2="2.883409375" layer="94"/>
<rectangle x1="17.1831" y1="2.865375" x2="17.92478125" y2="2.883409375" layer="94"/>
<rectangle x1="22.913340625" y1="2.865375" x2="23.72868125" y2="2.883409375" layer="94"/>
<rectangle x1="0.370840625" y1="2.883409375" x2="2.21488125" y2="2.901440625" layer="94"/>
<rectangle x1="4.52881875" y1="2.883409375" x2="5.450840625" y2="2.901440625" layer="94"/>
<rectangle x1="7.366" y1="2.883409375" x2="8.54201875" y2="2.901440625" layer="94"/>
<rectangle x1="9.2837" y1="2.883409375" x2="10.02538125" y2="2.901440625" layer="94"/>
<rectangle x1="12.176759375" y1="2.883409375" x2="13.83791875" y2="2.901440625" layer="94"/>
<rectangle x1="15.356840625" y1="2.883409375" x2="16.09851875" y2="2.901440625" layer="94"/>
<rectangle x1="17.1831" y1="2.883409375" x2="17.92478125" y2="2.901440625" layer="94"/>
<rectangle x1="22.913340625" y1="2.883409375" x2="23.746459375" y2="2.901440625" layer="94"/>
<rectangle x1="0.33528125" y1="2.901440625" x2="2.232659375" y2="2.91973125" layer="94"/>
<rectangle x1="4.52881875" y1="2.901440625" x2="5.450840625" y2="2.91973125" layer="94"/>
<rectangle x1="7.366" y1="2.901440625" x2="8.54201875" y2="2.91973125" layer="94"/>
<rectangle x1="9.2837" y1="2.901440625" x2="10.02538125" y2="2.91973125" layer="94"/>
<rectangle x1="12.156440625" y1="2.901440625" x2="13.87601875" y2="2.91973125" layer="94"/>
<rectangle x1="15.356840625" y1="2.901440625" x2="16.09851875" y2="2.91973125" layer="94"/>
<rectangle x1="17.1831" y1="2.901440625" x2="17.92478125" y2="2.91973125" layer="94"/>
<rectangle x1="22.93111875" y1="2.901440625" x2="23.764240625" y2="2.91973125" layer="94"/>
<rectangle x1="0.3175" y1="2.91973125" x2="2.26821875" y2="2.9377625" layer="94"/>
<rectangle x1="4.52881875" y1="2.91973125" x2="5.46861875" y2="2.9377625" layer="94"/>
<rectangle x1="7.366" y1="2.91973125" x2="8.54201875" y2="2.9377625" layer="94"/>
<rectangle x1="9.30148125" y1="2.91973125" x2="10.043159375" y2="2.9377625" layer="94"/>
<rectangle x1="12.138659375" y1="2.91973125" x2="13.8938" y2="2.9377625" layer="94"/>
<rectangle x1="15.37461875" y1="2.91973125" x2="16.1163" y2="2.9377625" layer="94"/>
<rectangle x1="17.20088125" y1="2.91973125" x2="17.942559375" y2="2.9377625" layer="94"/>
<rectangle x1="22.93111875" y1="2.91973125" x2="23.764240625" y2="2.9377625" layer="94"/>
<rectangle x1="0.29718125" y1="2.9377625" x2="2.30378125" y2="2.955796875" layer="94"/>
<rectangle x1="4.52881875" y1="2.9377625" x2="5.4864" y2="2.955796875" layer="94"/>
<rectangle x1="7.366" y1="2.9377625" x2="8.5598" y2="2.955796875" layer="94"/>
<rectangle x1="9.30148125" y1="2.9377625" x2="10.043159375" y2="2.955796875" layer="94"/>
<rectangle x1="12.12088125" y1="2.9377625" x2="13.929359375" y2="2.955796875" layer="94"/>
<rectangle x1="15.37461875" y1="2.9377625" x2="16.1163" y2="2.955796875" layer="94"/>
<rectangle x1="17.20088125" y1="2.9377625" x2="17.942559375" y2="2.955796875" layer="94"/>
<rectangle x1="22.951440625" y1="2.9377625" x2="23.78201875" y2="2.955796875" layer="94"/>
<rectangle x1="0.2794" y1="2.955796875" x2="2.34188125" y2="2.97383125" layer="94"/>
<rectangle x1="4.52881875" y1="2.955796875" x2="5.4864" y2="2.97383125" layer="94"/>
<rectangle x1="7.34821875" y1="2.955796875" x2="8.5598" y2="2.97383125" layer="94"/>
<rectangle x1="9.319259375" y1="2.955796875" x2="10.060940625" y2="2.97383125" layer="94"/>
<rectangle x1="12.1031" y1="2.955796875" x2="13.947140625" y2="2.97383125" layer="94"/>
<rectangle x1="15.3924" y1="2.955796875" x2="16.13408125" y2="2.97383125" layer="94"/>
<rectangle x1="17.218659375" y1="2.955796875" x2="17.960340625" y2="2.97383125" layer="94"/>
<rectangle x1="22.96921875" y1="2.955796875" x2="23.7998" y2="2.97383125" layer="94"/>
<rectangle x1="0.2794" y1="2.97383125" x2="2.359659375" y2="2.991865625" layer="94"/>
<rectangle x1="4.52881875" y1="2.97383125" x2="5.50418125" y2="2.991865625" layer="94"/>
<rectangle x1="7.34821875" y1="2.97383125" x2="8.5598" y2="2.991865625" layer="94"/>
<rectangle x1="9.319259375" y1="2.97383125" x2="10.060940625" y2="2.991865625" layer="94"/>
<rectangle x1="12.08531875" y1="2.97383125" x2="13.9827" y2="2.991865625" layer="94"/>
<rectangle x1="15.3924" y1="2.97383125" x2="16.13408125" y2="2.991865625" layer="94"/>
<rectangle x1="17.218659375" y1="2.97383125" x2="17.960340625" y2="2.991865625" layer="94"/>
<rectangle x1="22.96921875" y1="2.97383125" x2="23.7998" y2="2.991865625" layer="94"/>
<rectangle x1="0.26161875" y1="2.991865625" x2="2.39521875" y2="3.010153125" layer="94"/>
<rectangle x1="4.52881875" y1="2.991865625" x2="5.521959375" y2="3.010153125" layer="94"/>
<rectangle x1="7.34821875" y1="2.991865625" x2="8.5598" y2="3.010153125" layer="94"/>
<rectangle x1="9.319259375" y1="2.991865625" x2="10.060940625" y2="3.010153125" layer="94"/>
<rectangle x1="12.067540625" y1="2.991865625" x2="14.00048125" y2="3.010153125" layer="94"/>
<rectangle x1="15.3924" y1="2.991865625" x2="16.13408125" y2="3.010153125" layer="94"/>
<rectangle x1="17.218659375" y1="2.991865625" x2="17.960340625" y2="3.010153125" layer="94"/>
<rectangle x1="22.987" y1="2.991865625" x2="23.81758125" y2="3.010153125" layer="94"/>
<rectangle x1="0.243840625" y1="3.010153125" x2="2.413" y2="3.0281875" layer="94"/>
<rectangle x1="4.52881875" y1="3.010153125" x2="5.539740625" y2="3.0281875" layer="94"/>
<rectangle x1="7.34821875" y1="3.010153125" x2="8.57758125" y2="3.0281875" layer="94"/>
<rectangle x1="9.337040625" y1="3.010153125" x2="10.07871875" y2="3.0281875" layer="94"/>
<rectangle x1="12.067540625" y1="3.010153125" x2="14.03858125" y2="3.0281875" layer="94"/>
<rectangle x1="15.41271875" y1="3.010153125" x2="16.151859375" y2="3.0281875" layer="94"/>
<rectangle x1="17.236440625" y1="3.010153125" x2="17.97811875" y2="3.0281875" layer="94"/>
<rectangle x1="22.987" y1="3.010153125" x2="23.81758125" y2="3.0281875" layer="94"/>
<rectangle x1="0.226059375" y1="3.0281875" x2="2.43078125" y2="3.046221875" layer="94"/>
<rectangle x1="4.52881875" y1="3.0281875" x2="5.539740625" y2="3.046221875" layer="94"/>
<rectangle x1="7.34821875" y1="3.0281875" x2="8.57758125" y2="3.046221875" layer="94"/>
<rectangle x1="9.337040625" y1="3.0281875" x2="10.07871875" y2="3.046221875" layer="94"/>
<rectangle x1="12.049759375" y1="3.0281875" x2="14.056359375" y2="3.046221875" layer="94"/>
<rectangle x1="15.41271875" y1="3.0281875" x2="16.151859375" y2="3.046221875" layer="94"/>
<rectangle x1="17.236440625" y1="3.0281875" x2="17.97811875" y2="3.046221875" layer="94"/>
<rectangle x1="23.00478125" y1="3.0281875" x2="23.835359375" y2="3.046221875" layer="94"/>
<rectangle x1="0.20828125" y1="3.046221875" x2="2.46888125" y2="3.06425625" layer="94"/>
<rectangle x1="4.52881875" y1="3.046221875" x2="5.560059375" y2="3.06425625" layer="94"/>
<rectangle x1="7.34821875" y1="3.046221875" x2="8.57758125" y2="3.06425625" layer="94"/>
<rectangle x1="9.337040625" y1="3.046221875" x2="10.07871875" y2="3.06425625" layer="94"/>
<rectangle x1="12.03198125" y1="3.046221875" x2="14.074140625" y2="3.06425625" layer="94"/>
<rectangle x1="15.41271875" y1="3.046221875" x2="16.151859375" y2="3.06425625" layer="94"/>
<rectangle x1="17.236440625" y1="3.046221875" x2="17.97811875" y2="3.06425625" layer="94"/>
<rectangle x1="23.00478125" y1="3.046221875" x2="23.835359375" y2="3.06425625" layer="94"/>
<rectangle x1="0.20828125" y1="3.06425625" x2="2.486659375" y2="3.082290625" layer="94"/>
<rectangle x1="4.52881875" y1="3.06425625" x2="5.577840625" y2="3.082290625" layer="94"/>
<rectangle x1="7.34821875" y1="3.06425625" x2="8.57758125" y2="3.082290625" layer="94"/>
<rectangle x1="9.35481875" y1="3.06425625" x2="10.0965" y2="3.082290625" layer="94"/>
<rectangle x1="12.03198125" y1="3.06425625" x2="14.1097" y2="3.082290625" layer="94"/>
<rectangle x1="15.4305" y1="3.06425625" x2="16.17218125" y2="3.082290625" layer="94"/>
<rectangle x1="17.256759375" y1="3.06425625" x2="17.9959" y2="3.082290625" layer="94"/>
<rectangle x1="23.022559375" y1="3.06425625" x2="23.853140625" y2="3.082290625" layer="94"/>
<rectangle x1="0.1905" y1="3.082290625" x2="2.504440625" y2="3.100325" layer="94"/>
<rectangle x1="4.52881875" y1="3.082290625" x2="5.577840625" y2="3.100325" layer="94"/>
<rectangle x1="7.366" y1="3.082290625" x2="8.595359375" y2="3.100325" layer="94"/>
<rectangle x1="9.35481875" y1="3.082290625" x2="10.0965" y2="3.100325" layer="94"/>
<rectangle x1="12.0142" y1="3.082290625" x2="14.12748125" y2="3.100325" layer="94"/>
<rectangle x1="15.4305" y1="3.082290625" x2="16.17218125" y2="3.100325" layer="94"/>
<rectangle x1="17.256759375" y1="3.082290625" x2="17.9959" y2="3.100325" layer="94"/>
<rectangle x1="23.022559375" y1="3.082290625" x2="23.853140625" y2="3.100325" layer="94"/>
<rectangle x1="0.17271875" y1="3.100325" x2="2.54" y2="3.1186125" layer="94"/>
<rectangle x1="4.52881875" y1="3.100325" x2="5.59561875" y2="3.1186125" layer="94"/>
<rectangle x1="7.366" y1="3.100325" x2="8.595359375" y2="3.1186125" layer="94"/>
<rectangle x1="9.35481875" y1="3.100325" x2="10.0965" y2="3.1186125" layer="94"/>
<rectangle x1="12.0142" y1="3.100325" x2="14.145259375" y2="3.1186125" layer="94"/>
<rectangle x1="15.4305" y1="3.100325" x2="16.17218125" y2="3.1186125" layer="94"/>
<rectangle x1="17.256759375" y1="3.100325" x2="18.01621875" y2="3.1186125" layer="94"/>
<rectangle x1="23.022559375" y1="3.100325" x2="23.873459375" y2="3.1186125" layer="94"/>
<rectangle x1="0.17271875" y1="3.1186125" x2="2.55778125" y2="3.136646875" layer="94"/>
<rectangle x1="4.52881875" y1="3.1186125" x2="5.6134" y2="3.136646875" layer="94"/>
<rectangle x1="7.366" y1="3.1186125" x2="8.595359375" y2="3.136646875" layer="94"/>
<rectangle x1="9.3726" y1="3.1186125" x2="10.11428125" y2="3.136646875" layer="94"/>
<rectangle x1="11.99388125" y1="3.1186125" x2="14.183359375" y2="3.136646875" layer="94"/>
<rectangle x1="15.44828125" y1="3.1186125" x2="16.189959375" y2="3.136646875" layer="94"/>
<rectangle x1="17.274540625" y1="3.1186125" x2="18.01621875" y2="3.136646875" layer="94"/>
<rectangle x1="23.040340625" y1="3.1186125" x2="23.873459375" y2="3.136646875" layer="94"/>
<rectangle x1="0.1524" y1="3.136646875" x2="2.575559375" y2="3.15468125" layer="94"/>
<rectangle x1="4.52881875" y1="3.136646875" x2="5.6134" y2="3.15468125" layer="94"/>
<rectangle x1="7.366" y1="3.136646875" x2="8.595359375" y2="3.15468125" layer="94"/>
<rectangle x1="9.3726" y1="3.136646875" x2="10.11428125" y2="3.15468125" layer="94"/>
<rectangle x1="11.99388125" y1="3.136646875" x2="14.201140625" y2="3.15468125" layer="94"/>
<rectangle x1="15.44828125" y1="3.136646875" x2="16.189959375" y2="3.15468125" layer="94"/>
<rectangle x1="17.274540625" y1="3.136646875" x2="18.01621875" y2="3.15468125" layer="94"/>
<rectangle x1="23.040340625" y1="3.136646875" x2="23.891240625" y2="3.15468125" layer="94"/>
<rectangle x1="0.1524" y1="3.15468125" x2="2.61111875" y2="3.1727125" layer="94"/>
<rectangle x1="4.52881875" y1="3.15468125" x2="5.63118125" y2="3.1727125" layer="94"/>
<rectangle x1="7.366" y1="3.15468125" x2="8.613140625" y2="3.1727125" layer="94"/>
<rectangle x1="9.39291875" y1="3.15468125" x2="10.132059375" y2="3.1727125" layer="94"/>
<rectangle x1="11.9761" y1="3.15468125" x2="14.21891875" y2="3.1727125" layer="94"/>
<rectangle x1="15.466059375" y1="3.15468125" x2="16.207740625" y2="3.1727125" layer="94"/>
<rectangle x1="17.29231875" y1="3.15468125" x2="18.034" y2="3.1727125" layer="94"/>
<rectangle x1="23.040340625" y1="3.15468125" x2="23.891240625" y2="3.1727125" layer="94"/>
<rectangle x1="0.13461875" y1="3.1727125" x2="2.631440625" y2="3.190746875" layer="94"/>
<rectangle x1="4.52881875" y1="3.1727125" x2="5.648959375" y2="3.190746875" layer="94"/>
<rectangle x1="7.366" y1="3.1727125" x2="8.613140625" y2="3.190746875" layer="94"/>
<rectangle x1="9.39291875" y1="3.1727125" x2="10.132059375" y2="3.190746875" layer="94"/>
<rectangle x1="11.9761" y1="3.1727125" x2="14.2367" y2="3.190746875" layer="94"/>
<rectangle x1="15.466059375" y1="3.1727125" x2="16.207740625" y2="3.190746875" layer="94"/>
<rectangle x1="17.29231875" y1="3.1727125" x2="18.034" y2="3.190746875" layer="94"/>
<rectangle x1="23.05811875" y1="3.1727125" x2="23.90901875" y2="3.190746875" layer="94"/>
<rectangle x1="0.116840625" y1="3.190746875" x2="2.64921875" y2="3.20878125" layer="94"/>
<rectangle x1="4.52881875" y1="3.190746875" x2="5.666740625" y2="3.20878125" layer="94"/>
<rectangle x1="7.366" y1="3.190746875" x2="8.613140625" y2="3.20878125" layer="94"/>
<rectangle x1="9.39291875" y1="3.190746875" x2="10.132059375" y2="3.20878125" layer="94"/>
<rectangle x1="11.9761" y1="3.190746875" x2="14.25448125" y2="3.20878125" layer="94"/>
<rectangle x1="15.466059375" y1="3.190746875" x2="16.207740625" y2="3.20878125" layer="94"/>
<rectangle x1="17.29231875" y1="3.190746875" x2="18.034" y2="3.20878125" layer="94"/>
<rectangle x1="23.05811875" y1="3.190746875" x2="23.90901875" y2="3.20878125" layer="94"/>
<rectangle x1="0.116840625" y1="3.20878125" x2="2.667" y2="3.22706875" layer="94"/>
<rectangle x1="4.52881875" y1="3.20878125" x2="5.666740625" y2="3.22706875" layer="94"/>
<rectangle x1="7.38378125" y1="3.20878125" x2="8.633459375" y2="3.22706875" layer="94"/>
<rectangle x1="9.4107" y1="3.20878125" x2="10.149840625" y2="3.22706875" layer="94"/>
<rectangle x1="11.95831875" y1="3.20878125" x2="14.272259375" y2="3.22706875" layer="94"/>
<rectangle x1="15.483840625" y1="3.20878125" x2="16.22551875" y2="3.22706875" layer="94"/>
<rectangle x1="17.3101" y1="3.20878125" x2="18.05178125" y2="3.22706875" layer="94"/>
<rectangle x1="23.05811875" y1="3.20878125" x2="23.90901875" y2="3.22706875" layer="94"/>
<rectangle x1="0.099059375" y1="3.22706875" x2="2.68478125" y2="3.245103125" layer="94"/>
<rectangle x1="4.52881875" y1="3.22706875" x2="5.68451875" y2="3.245103125" layer="94"/>
<rectangle x1="7.38378125" y1="3.22706875" x2="8.633459375" y2="3.245103125" layer="94"/>
<rectangle x1="9.4107" y1="3.22706875" x2="10.149840625" y2="3.245103125" layer="94"/>
<rectangle x1="11.95831875" y1="3.22706875" x2="14.290040625" y2="3.245103125" layer="94"/>
<rectangle x1="15.483840625" y1="3.22706875" x2="16.22551875" y2="3.245103125" layer="94"/>
<rectangle x1="17.3101" y1="3.22706875" x2="18.05178125" y2="3.245103125" layer="94"/>
<rectangle x1="23.0759" y1="3.22706875" x2="23.9268" y2="3.245103125" layer="94"/>
<rectangle x1="0.099059375" y1="3.245103125" x2="2.702559375" y2="3.2631375" layer="94"/>
<rectangle x1="4.52881875" y1="3.245103125" x2="5.704840625" y2="3.2631375" layer="94"/>
<rectangle x1="7.38378125" y1="3.245103125" x2="8.633459375" y2="3.2631375" layer="94"/>
<rectangle x1="9.4107" y1="3.245103125" x2="10.149840625" y2="3.2631375" layer="94"/>
<rectangle x1="11.95831875" y1="3.245103125" x2="14.30781875" y2="3.2631375" layer="94"/>
<rectangle x1="15.483840625" y1="3.245103125" x2="16.22551875" y2="3.2631375" layer="94"/>
<rectangle x1="17.3101" y1="3.245103125" x2="18.05178125" y2="3.2631375" layer="94"/>
<rectangle x1="23.0759" y1="3.245103125" x2="23.9268" y2="3.2631375" layer="94"/>
<rectangle x1="0.099059375" y1="3.2631375" x2="2.720340625" y2="3.281171875" layer="94"/>
<rectangle x1="4.52881875" y1="3.2631375" x2="5.704840625" y2="3.281171875" layer="94"/>
<rectangle x1="7.38378125" y1="3.2631375" x2="8.633459375" y2="3.281171875" layer="94"/>
<rectangle x1="9.42848125" y1="3.2631375" x2="10.170159375" y2="3.281171875" layer="94"/>
<rectangle x1="11.940540625" y1="3.2631375" x2="14.328140625" y2="3.281171875" layer="94"/>
<rectangle x1="15.50161875" y1="3.2631375" x2="16.2433" y2="3.281171875" layer="94"/>
<rectangle x1="17.32788125" y1="3.2631375" x2="18.069559375" y2="3.281171875" layer="94"/>
<rectangle x1="23.0759" y1="3.2631375" x2="23.94458125" y2="3.281171875" layer="94"/>
<rectangle x1="0.08128125" y1="3.281171875" x2="2.73811875" y2="3.29920625" layer="94"/>
<rectangle x1="4.52881875" y1="3.281171875" x2="5.72261875" y2="3.29920625" layer="94"/>
<rectangle x1="7.4041" y1="3.281171875" x2="8.651240625" y2="3.29920625" layer="94"/>
<rectangle x1="9.42848125" y1="3.281171875" x2="10.170159375" y2="3.29920625" layer="94"/>
<rectangle x1="11.940540625" y1="3.281171875" x2="14.3637" y2="3.29920625" layer="94"/>
<rectangle x1="15.50161875" y1="3.281171875" x2="16.2433" y2="3.29920625" layer="94"/>
<rectangle x1="17.32788125" y1="3.281171875" x2="18.069559375" y2="3.29920625" layer="94"/>
<rectangle x1="23.0759" y1="3.281171875" x2="23.94458125" y2="3.29920625" layer="94"/>
<rectangle x1="0.08128125" y1="3.29920625" x2="2.7559" y2="3.317240625" layer="94"/>
<rectangle x1="4.52881875" y1="3.29920625" x2="5.7404" y2="3.317240625" layer="94"/>
<rectangle x1="7.4041" y1="3.29920625" x2="8.3058" y2="3.317240625" layer="94"/>
<rectangle x1="8.379459375" y1="3.29920625" x2="8.651240625" y2="3.317240625" layer="94"/>
<rectangle x1="9.42848125" y1="3.29920625" x2="10.187940625" y2="3.317240625" layer="94"/>
<rectangle x1="11.940540625" y1="3.29920625" x2="13.02511875" y2="3.317240625" layer="94"/>
<rectangle x1="13.31468125" y1="3.29920625" x2="14.38148125" y2="3.317240625" layer="94"/>
<rectangle x1="15.50161875" y1="3.29920625" x2="16.26108125" y2="3.317240625" layer="94"/>
<rectangle x1="17.32788125" y1="3.29920625" x2="18.087340625" y2="3.317240625" layer="94"/>
<rectangle x1="23.09621875" y1="3.29920625" x2="23.94458125" y2="3.317240625" layer="94"/>
<rectangle x1="0.0635" y1="3.317240625" x2="2.77621875" y2="3.335528125" layer="94"/>
<rectangle x1="4.52881875" y1="3.317240625" x2="5.75818125" y2="3.335528125" layer="94"/>
<rectangle x1="7.4041" y1="3.317240625" x2="8.252459375" y2="3.335528125" layer="94"/>
<rectangle x1="8.48868125" y1="3.317240625" x2="8.651240625" y2="3.335528125" layer="94"/>
<rectangle x1="9.446259375" y1="3.317240625" x2="10.187940625" y2="3.335528125" layer="94"/>
<rectangle x1="11.940540625" y1="3.317240625" x2="12.97178125" y2="3.335528125" layer="94"/>
<rectangle x1="13.3858" y1="3.317240625" x2="14.399259375" y2="3.335528125" layer="94"/>
<rectangle x1="15.5194" y1="3.317240625" x2="16.26108125" y2="3.335528125" layer="94"/>
<rectangle x1="17.345659375" y1="3.317240625" x2="18.087340625" y2="3.335528125" layer="94"/>
<rectangle x1="23.09621875" y1="3.317240625" x2="23.94458125" y2="3.335528125" layer="94"/>
<rectangle x1="0.0635" y1="3.335528125" x2="2.794" y2="3.3535625" layer="94"/>
<rectangle x1="4.52881875" y1="3.335528125" x2="5.75818125" y2="3.3535625" layer="94"/>
<rectangle x1="7.42188125" y1="3.335528125" x2="8.23468125" y2="3.3535625" layer="94"/>
<rectangle x1="8.5598" y1="3.335528125" x2="8.651240625" y2="3.3535625" layer="94"/>
<rectangle x1="9.446259375" y1="3.335528125" x2="10.187940625" y2="3.3535625" layer="94"/>
<rectangle x1="11.940540625" y1="3.335528125" x2="12.93621875" y2="3.3535625" layer="94"/>
<rectangle x1="13.4239" y1="3.335528125" x2="14.417040625" y2="3.3535625" layer="94"/>
<rectangle x1="15.5194" y1="3.335528125" x2="16.26108125" y2="3.3535625" layer="94"/>
<rectangle x1="17.345659375" y1="3.335528125" x2="18.087340625" y2="3.3535625" layer="94"/>
<rectangle x1="23.09621875" y1="3.335528125" x2="23.962359375" y2="3.3535625" layer="94"/>
<rectangle x1="0.0635" y1="3.3535625" x2="1.381759375" y2="3.371596875" layer="94"/>
<rectangle x1="1.526540625" y1="3.3535625" x2="2.81178125" y2="3.371596875" layer="94"/>
<rectangle x1="4.52881875" y1="3.3535625" x2="5.775959375" y2="3.371596875" layer="94"/>
<rectangle x1="7.42188125" y1="3.3535625" x2="8.2169" y2="3.371596875" layer="94"/>
<rectangle x1="8.613140625" y1="3.3535625" x2="8.66901875" y2="3.371596875" layer="94"/>
<rectangle x1="9.464040625" y1="3.3535625" x2="10.20571875" y2="3.371596875" layer="94"/>
<rectangle x1="11.922759375" y1="3.3535625" x2="12.89811875" y2="3.371596875" layer="94"/>
<rectangle x1="13.477240625" y1="3.3535625" x2="14.417040625" y2="3.371596875" layer="94"/>
<rectangle x1="15.53718125" y1="3.3535625" x2="16.278859375" y2="3.371596875" layer="94"/>
<rectangle x1="17.363440625" y1="3.3535625" x2="18.10511875" y2="3.371596875" layer="94"/>
<rectangle x1="23.09621875" y1="3.3535625" x2="23.962359375" y2="3.371596875" layer="94"/>
<rectangle x1="0.04571875" y1="3.371596875" x2="1.2573" y2="3.38963125" layer="94"/>
<rectangle x1="1.635759375" y1="3.371596875" x2="2.829559375" y2="3.38963125" layer="94"/>
<rectangle x1="4.52881875" y1="3.371596875" x2="5.793740625" y2="3.38963125" layer="94"/>
<rectangle x1="7.42188125" y1="3.371596875" x2="8.2169" y2="3.38963125" layer="94"/>
<rectangle x1="9.464040625" y1="3.371596875" x2="10.20571875" y2="3.38963125" layer="94"/>
<rectangle x1="11.922759375" y1="3.371596875" x2="12.880340625" y2="3.38963125" layer="94"/>
<rectangle x1="13.5128" y1="3.371596875" x2="14.43481875" y2="3.38963125" layer="94"/>
<rectangle x1="15.53718125" y1="3.371596875" x2="16.278859375" y2="3.38963125" layer="94"/>
<rectangle x1="17.363440625" y1="3.371596875" x2="18.10511875" y2="3.38963125" layer="94"/>
<rectangle x1="23.09621875" y1="3.371596875" x2="23.962359375" y2="3.38963125" layer="94"/>
<rectangle x1="0.04571875" y1="3.38963125" x2="1.183640625" y2="3.4076625" layer="94"/>
<rectangle x1="1.70941875" y1="3.38963125" x2="2.847340625" y2="3.4076625" layer="94"/>
<rectangle x1="4.52881875" y1="3.38963125" x2="5.793740625" y2="3.4076625" layer="94"/>
<rectangle x1="7.42188125" y1="3.38963125" x2="8.19911875" y2="3.4076625" layer="94"/>
<rectangle x1="9.464040625" y1="3.38963125" x2="10.20571875" y2="3.4076625" layer="94"/>
<rectangle x1="11.922759375" y1="3.38963125" x2="12.862559375" y2="3.4076625" layer="94"/>
<rectangle x1="13.5509" y1="3.38963125" x2="14.4526" y2="3.4076625" layer="94"/>
<rectangle x1="15.53718125" y1="3.38963125" x2="16.278859375" y2="3.4076625" layer="94"/>
<rectangle x1="17.363440625" y1="3.38963125" x2="18.10511875" y2="3.4076625" layer="94"/>
<rectangle x1="23.09621875" y1="3.38963125" x2="23.962359375" y2="3.4076625" layer="94"/>
<rectangle x1="0.04571875" y1="3.4076625" x2="1.14808125" y2="3.425953125" layer="94"/>
<rectangle x1="1.762759375" y1="3.4076625" x2="2.86511875" y2="3.425953125" layer="94"/>
<rectangle x1="4.52881875" y1="3.4076625" x2="5.81151875" y2="3.425953125" layer="94"/>
<rectangle x1="7.439659375" y1="3.4076625" x2="8.19911875" y2="3.425953125" layer="94"/>
<rectangle x1="9.48181875" y1="3.4076625" x2="10.2235" y2="3.425953125" layer="94"/>
<rectangle x1="11.922759375" y1="3.4076625" x2="12.84478125" y2="3.425953125" layer="94"/>
<rectangle x1="13.586459375" y1="3.4076625" x2="14.47291875" y2="3.425953125" layer="94"/>
<rectangle x1="15.5575" y1="3.4076625" x2="16.296640625" y2="3.425953125" layer="94"/>
<rectangle x1="17.38121875" y1="3.4076625" x2="18.1229" y2="3.425953125" layer="94"/>
<rectangle x1="23.114" y1="3.4076625" x2="23.980140625" y2="3.425953125" layer="94"/>
<rectangle x1="0.04571875" y1="3.425953125" x2="1.11251875" y2="3.4439875" layer="94"/>
<rectangle x1="1.8161" y1="3.425953125" x2="2.8829" y2="3.4439875" layer="94"/>
<rectangle x1="4.52881875" y1="3.425953125" x2="5.8293" y2="3.4439875" layer="94"/>
<rectangle x1="7.439659375" y1="3.425953125" x2="8.19911875" y2="3.4439875" layer="94"/>
<rectangle x1="9.48181875" y1="3.425953125" x2="10.2235" y2="3.4439875" layer="94"/>
<rectangle x1="11.922759375" y1="3.425953125" x2="12.827" y2="3.4439875" layer="94"/>
<rectangle x1="13.604240625" y1="3.425953125" x2="14.4907" y2="3.4439875" layer="94"/>
<rectangle x1="15.5575" y1="3.425953125" x2="16.296640625" y2="3.4439875" layer="94"/>
<rectangle x1="17.38121875" y1="3.425953125" x2="18.1229" y2="3.4439875" layer="94"/>
<rectangle x1="23.114" y1="3.425953125" x2="23.980140625" y2="3.4439875" layer="94"/>
<rectangle x1="0.027940625" y1="3.4439875" x2="1.07441875" y2="3.46201875" layer="94"/>
<rectangle x1="1.8542" y1="3.4439875" x2="2.90068125" y2="3.46201875" layer="94"/>
<rectangle x1="4.52881875" y1="3.4439875" x2="5.8293" y2="3.46201875" layer="94"/>
<rectangle x1="7.439659375" y1="3.4439875" x2="8.19911875" y2="3.46201875" layer="94"/>
<rectangle x1="9.48181875" y1="3.4439875" x2="10.2235" y2="3.46201875" layer="94"/>
<rectangle x1="11.922759375" y1="3.4439875" x2="12.80921875" y2="3.46201875" layer="94"/>
<rectangle x1="13.6398" y1="3.4439875" x2="14.50848125" y2="3.46201875" layer="94"/>
<rectangle x1="15.5575" y1="3.4439875" x2="16.296640625" y2="3.46201875" layer="94"/>
<rectangle x1="17.38121875" y1="3.4439875" x2="18.1229" y2="3.46201875" layer="94"/>
<rectangle x1="23.114" y1="3.4439875" x2="23.980140625" y2="3.46201875" layer="94"/>
<rectangle x1="0.027940625" y1="3.46201875" x2="1.056640625" y2="3.480053125" layer="94"/>
<rectangle x1="1.889759375" y1="3.46201875" x2="2.918459375" y2="3.480053125" layer="94"/>
<rectangle x1="4.52881875" y1="3.46201875" x2="5.84708125" y2="3.480053125" layer="94"/>
<rectangle x1="7.457440625" y1="3.46201875" x2="8.19911875" y2="3.480053125" layer="94"/>
<rectangle x1="9.4996" y1="3.46201875" x2="10.24128125" y2="3.480053125" layer="94"/>
<rectangle x1="11.922759375" y1="3.46201875" x2="12.791440625" y2="3.480053125" layer="94"/>
<rectangle x1="13.65758125" y1="3.46201875" x2="14.526259375" y2="3.480053125" layer="94"/>
<rectangle x1="15.57528125" y1="3.46201875" x2="16.31441875" y2="3.480053125" layer="94"/>
<rectangle x1="17.401540625" y1="3.46201875" x2="18.14068125" y2="3.480053125" layer="94"/>
<rectangle x1="23.114" y1="3.46201875" x2="23.980140625" y2="3.480053125" layer="94"/>
<rectangle x1="0.027940625" y1="3.480053125" x2="1.02108125" y2="3.4980875" layer="94"/>
<rectangle x1="1.92531875" y1="3.480053125" x2="2.918459375" y2="3.4980875" layer="94"/>
<rectangle x1="4.52881875" y1="3.480053125" x2="5.8674" y2="3.4980875" layer="94"/>
<rectangle x1="7.457440625" y1="3.480053125" x2="8.2169" y2="3.4980875" layer="94"/>
<rectangle x1="9.4996" y1="3.480053125" x2="10.24128125" y2="3.4980875" layer="94"/>
<rectangle x1="11.922759375" y1="3.480053125" x2="12.791440625" y2="3.4980875" layer="94"/>
<rectangle x1="13.693140625" y1="3.480053125" x2="14.544040625" y2="3.4980875" layer="94"/>
<rectangle x1="15.57528125" y1="3.480053125" x2="16.31441875" y2="3.4980875" layer="94"/>
<rectangle x1="17.401540625" y1="3.480053125" x2="18.14068125" y2="3.4980875" layer="94"/>
<rectangle x1="23.114" y1="3.480053125" x2="23.99791875" y2="3.4980875" layer="94"/>
<rectangle x1="0.027940625" y1="3.4980875" x2="1.0033" y2="3.516121875" layer="94"/>
<rectangle x1="1.96088125" y1="3.4980875" x2="2.93878125" y2="3.516121875" layer="94"/>
<rectangle x1="4.52881875" y1="3.4980875" x2="5.88518125" y2="3.516121875" layer="94"/>
<rectangle x1="7.457440625" y1="3.4980875" x2="8.2169" y2="3.516121875" layer="94"/>
<rectangle x1="9.4996" y1="3.4980875" x2="10.259059375" y2="3.516121875" layer="94"/>
<rectangle x1="11.922759375" y1="3.4980875" x2="12.77111875" y2="3.516121875" layer="94"/>
<rectangle x1="13.713459375" y1="3.4980875" x2="14.544040625" y2="3.516121875" layer="94"/>
<rectangle x1="15.57528125" y1="3.4980875" x2="16.334740625" y2="3.516121875" layer="94"/>
<rectangle x1="17.41931875" y1="3.4980875" x2="18.158459375" y2="3.516121875" layer="94"/>
<rectangle x1="23.114" y1="3.4980875" x2="23.99791875" y2="3.516121875" layer="94"/>
<rectangle x1="0.027940625" y1="3.516121875" x2="0.98551875" y2="3.534409375" layer="94"/>
<rectangle x1="1.978659375" y1="3.516121875" x2="2.956559375" y2="3.534409375" layer="94"/>
<rectangle x1="4.52881875" y1="3.516121875" x2="5.88518125" y2="3.534409375" layer="94"/>
<rectangle x1="7.47521875" y1="3.516121875" x2="8.2169" y2="3.534409375" layer="94"/>
<rectangle x1="9.51738125" y1="3.516121875" x2="10.259059375" y2="3.534409375" layer="94"/>
<rectangle x1="11.922759375" y1="3.516121875" x2="12.77111875" y2="3.534409375" layer="94"/>
<rectangle x1="13.731240625" y1="3.516121875" x2="14.56181875" y2="3.534409375" layer="94"/>
<rectangle x1="15.593059375" y1="3.516121875" x2="16.334740625" y2="3.534409375" layer="94"/>
<rectangle x1="17.41931875" y1="3.516121875" x2="18.158459375" y2="3.534409375" layer="94"/>
<rectangle x1="23.114" y1="3.516121875" x2="23.99791875" y2="3.534409375" layer="94"/>
<rectangle x1="0.010159375" y1="3.534409375" x2="0.967740625" y2="3.55244375" layer="94"/>
<rectangle x1="2.016759375" y1="3.534409375" x2="2.974340625" y2="3.55244375" layer="94"/>
<rectangle x1="4.52881875" y1="3.534409375" x2="5.902959375" y2="3.55244375" layer="94"/>
<rectangle x1="7.47521875" y1="3.534409375" x2="8.2169" y2="3.55244375" layer="94"/>
<rectangle x1="9.51738125" y1="3.534409375" x2="10.259059375" y2="3.55244375" layer="94"/>
<rectangle x1="11.922759375" y1="3.534409375" x2="12.753340625" y2="3.55244375" layer="94"/>
<rectangle x1="13.74901875" y1="3.534409375" x2="14.5796" y2="3.55244375" layer="94"/>
<rectangle x1="15.593059375" y1="3.534409375" x2="16.334740625" y2="3.55244375" layer="94"/>
<rectangle x1="17.41931875" y1="3.534409375" x2="18.158459375" y2="3.55244375" layer="94"/>
<rectangle x1="23.114" y1="3.534409375" x2="23.99791875" y2="3.55244375" layer="94"/>
<rectangle x1="0.010159375" y1="3.55244375" x2="0.949959375" y2="3.570478125" layer="94"/>
<rectangle x1="2.034540625" y1="3.55244375" x2="2.99211875" y2="3.570478125" layer="94"/>
<rectangle x1="4.52881875" y1="3.55244375" x2="5.920740625" y2="3.570478125" layer="94"/>
<rectangle x1="7.47521875" y1="3.55244375" x2="8.23468125" y2="3.570478125" layer="94"/>
<rectangle x1="9.535159375" y1="3.55244375" x2="10.276840625" y2="3.570478125" layer="94"/>
<rectangle x1="11.922759375" y1="3.55244375" x2="12.753340625" y2="3.570478125" layer="94"/>
<rectangle x1="13.7668" y1="3.55244375" x2="14.59738125" y2="3.570478125" layer="94"/>
<rectangle x1="15.610840625" y1="3.55244375" x2="16.35251875" y2="3.570478125" layer="94"/>
<rectangle x1="17.4371" y1="3.55244375" x2="18.17878125" y2="3.570478125" layer="94"/>
<rectangle x1="23.114" y1="3.55244375" x2="23.99791875" y2="3.570478125" layer="94"/>
<rectangle x1="0.010159375" y1="3.570478125" x2="0.949959375" y2="3.5885125" layer="94"/>
<rectangle x1="2.0701" y1="3.570478125" x2="3.0099" y2="3.5885125" layer="94"/>
<rectangle x1="4.52881875" y1="3.570478125" x2="5.920740625" y2="3.5885125" layer="94"/>
<rectangle x1="7.493" y1="3.570478125" x2="8.23468125" y2="3.5885125" layer="94"/>
<rectangle x1="9.535159375" y1="3.570478125" x2="10.276840625" y2="3.5885125" layer="94"/>
<rectangle x1="11.922759375" y1="3.570478125" x2="12.735559375" y2="3.5885125" layer="94"/>
<rectangle x1="13.78458125" y1="3.570478125" x2="14.615159375" y2="3.5885125" layer="94"/>
<rectangle x1="15.610840625" y1="3.570478125" x2="16.35251875" y2="3.5885125" layer="94"/>
<rectangle x1="17.4371" y1="3.570478125" x2="18.17878125" y2="3.5885125" layer="94"/>
<rectangle x1="23.114" y1="3.570478125" x2="23.99791875" y2="3.5885125" layer="94"/>
<rectangle x1="0.010159375" y1="3.5885125" x2="0.93218125" y2="3.606546875" layer="94"/>
<rectangle x1="2.08788125" y1="3.5885125" x2="3.0099" y2="3.606546875" layer="94"/>
<rectangle x1="4.52881875" y1="3.5885125" x2="5.93851875" y2="3.606546875" layer="94"/>
<rectangle x1="7.493" y1="3.5885125" x2="8.23468125" y2="3.606546875" layer="94"/>
<rectangle x1="9.535159375" y1="3.5885125" x2="10.276840625" y2="3.606546875" layer="94"/>
<rectangle x1="11.922759375" y1="3.5885125" x2="12.735559375" y2="3.606546875" layer="94"/>
<rectangle x1="13.802359375" y1="3.5885125" x2="14.615159375" y2="3.606546875" layer="94"/>
<rectangle x1="15.610840625" y1="3.5885125" x2="16.35251875" y2="3.606546875" layer="94"/>
<rectangle x1="17.4371" y1="3.5885125" x2="18.17878125" y2="3.606546875" layer="94"/>
<rectangle x1="23.114" y1="3.5885125" x2="23.99791875" y2="3.606546875" layer="94"/>
<rectangle x1="0.010159375" y1="3.606546875" x2="0.911859375" y2="3.62458125" layer="94"/>
<rectangle x1="2.105659375" y1="3.606546875" x2="3.02768125" y2="3.62458125" layer="94"/>
<rectangle x1="4.52881875" y1="3.606546875" x2="5.2324" y2="3.62458125" layer="94"/>
<rectangle x1="5.25271875" y1="3.606546875" x2="5.9563" y2="3.62458125" layer="94"/>
<rectangle x1="7.51078125" y1="3.606546875" x2="8.252459375" y2="3.62458125" layer="94"/>
<rectangle x1="9.55548125" y1="3.606546875" x2="10.29461875" y2="3.62458125" layer="94"/>
<rectangle x1="11.922759375" y1="3.606546875" x2="12.735559375" y2="3.62458125" layer="94"/>
<rectangle x1="13.820140625" y1="3.606546875" x2="14.63548125" y2="3.62458125" layer="94"/>
<rectangle x1="15.62861875" y1="3.606546875" x2="16.3703" y2="3.62458125" layer="94"/>
<rectangle x1="17.45488125" y1="3.606546875" x2="18.196559375" y2="3.62458125" layer="94"/>
<rectangle x1="23.114" y1="3.606546875" x2="23.99791875" y2="3.62458125" layer="94"/>
<rectangle x1="0.010159375" y1="3.62458125" x2="0.911859375" y2="3.64286875" layer="94"/>
<rectangle x1="2.14121875" y1="3.62458125" x2="3.045459375" y2="3.64286875" layer="94"/>
<rectangle x1="4.52881875" y1="3.62458125" x2="5.2324" y2="3.64286875" layer="94"/>
<rectangle x1="5.2705" y1="3.62458125" x2="5.9563" y2="3.64286875" layer="94"/>
<rectangle x1="7.51078125" y1="3.62458125" x2="8.252459375" y2="3.64286875" layer="94"/>
<rectangle x1="9.55548125" y1="3.62458125" x2="10.29461875" y2="3.64286875" layer="94"/>
<rectangle x1="11.922759375" y1="3.62458125" x2="12.735559375" y2="3.64286875" layer="94"/>
<rectangle x1="13.83791875" y1="3.62458125" x2="14.653259375" y2="3.64286875" layer="94"/>
<rectangle x1="15.62861875" y1="3.62458125" x2="16.3703" y2="3.64286875" layer="94"/>
<rectangle x1="17.45488125" y1="3.62458125" x2="18.196559375" y2="3.64286875" layer="94"/>
<rectangle x1="23.114" y1="3.62458125" x2="23.99791875" y2="3.64286875" layer="94"/>
<rectangle x1="0.010159375" y1="3.64286875" x2="0.89408125" y2="3.660903125" layer="94"/>
<rectangle x1="2.161540625" y1="3.64286875" x2="3.063240625" y2="3.660903125" layer="94"/>
<rectangle x1="4.52881875" y1="3.64286875" x2="5.25271875" y2="3.660903125" layer="94"/>
<rectangle x1="5.28828125" y1="3.64286875" x2="5.97408125" y2="3.660903125" layer="94"/>
<rectangle x1="7.51078125" y1="3.64286875" x2="8.252459375" y2="3.660903125" layer="94"/>
<rectangle x1="9.55548125" y1="3.64286875" x2="10.29461875" y2="3.660903125" layer="94"/>
<rectangle x1="11.922759375" y1="3.64286875" x2="12.71778125" y2="3.660903125" layer="94"/>
<rectangle x1="13.858240625" y1="3.64286875" x2="14.653259375" y2="3.660903125" layer="94"/>
<rectangle x1="15.62861875" y1="3.64286875" x2="16.3703" y2="3.660903125" layer="94"/>
<rectangle x1="17.45488125" y1="3.64286875" x2="18.196559375" y2="3.660903125" layer="94"/>
<rectangle x1="23.114" y1="3.64286875" x2="23.99791875" y2="3.660903125" layer="94"/>
<rectangle x1="-0.010159375" y1="3.660903125" x2="0.89408125" y2="3.6789375" layer="94"/>
<rectangle x1="2.17931875" y1="3.660903125" x2="3.083559375" y2="3.6789375" layer="94"/>
<rectangle x1="4.52881875" y1="3.660903125" x2="5.25271875" y2="3.6789375" layer="94"/>
<rectangle x1="5.28828125" y1="3.660903125" x2="5.991859375" y2="3.6789375" layer="94"/>
<rectangle x1="7.528559375" y1="3.660903125" x2="8.270240625" y2="3.6789375" layer="94"/>
<rectangle x1="9.573259375" y1="3.660903125" x2="10.3124" y2="3.6789375" layer="94"/>
<rectangle x1="11.922759375" y1="3.660903125" x2="12.71778125" y2="3.6789375" layer="94"/>
<rectangle x1="13.87601875" y1="3.660903125" x2="14.671040625" y2="3.6789375" layer="94"/>
<rectangle x1="15.6464" y1="3.660903125" x2="16.38808125" y2="3.6789375" layer="94"/>
<rectangle x1="17.472659375" y1="3.660903125" x2="18.214340625" y2="3.6789375" layer="94"/>
<rectangle x1="23.114" y1="3.660903125" x2="23.99791875" y2="3.6789375" layer="94"/>
<rectangle x1="-0.010159375" y1="3.6789375" x2="0.8763" y2="3.69696875" layer="94"/>
<rectangle x1="2.1971" y1="3.6789375" x2="3.083559375" y2="3.69696875" layer="94"/>
<rectangle x1="4.52881875" y1="3.6789375" x2="5.25271875" y2="3.69696875" layer="94"/>
<rectangle x1="5.306059375" y1="3.6789375" x2="6.01218125" y2="3.69696875" layer="94"/>
<rectangle x1="7.528559375" y1="3.6789375" x2="8.270240625" y2="3.69696875" layer="94"/>
<rectangle x1="9.573259375" y1="3.6789375" x2="10.3124" y2="3.69696875" layer="94"/>
<rectangle x1="11.922759375" y1="3.6789375" x2="12.71778125" y2="3.69696875" layer="94"/>
<rectangle x1="13.8938" y1="3.6789375" x2="14.68881875" y2="3.69696875" layer="94"/>
<rectangle x1="15.6464" y1="3.6789375" x2="16.38808125" y2="3.69696875" layer="94"/>
<rectangle x1="17.472659375" y1="3.6789375" x2="18.214340625" y2="3.69696875" layer="94"/>
<rectangle x1="23.114" y1="3.6789375" x2="23.99791875" y2="3.69696875" layer="94"/>
<rectangle x1="-0.010159375" y1="3.69696875" x2="0.8763" y2="3.715003125" layer="94"/>
<rectangle x1="2.21488125" y1="3.69696875" x2="3.101340625" y2="3.715003125" layer="94"/>
<rectangle x1="4.52881875" y1="3.69696875" x2="5.25271875" y2="3.715003125" layer="94"/>
<rectangle x1="5.323840625" y1="3.69696875" x2="6.01218125" y2="3.715003125" layer="94"/>
<rectangle x1="7.528559375" y1="3.69696875" x2="8.270240625" y2="3.715003125" layer="94"/>
<rectangle x1="9.591040625" y1="3.69696875" x2="10.33271875" y2="3.715003125" layer="94"/>
<rectangle x1="11.922759375" y1="3.69696875" x2="12.71778125" y2="3.715003125" layer="94"/>
<rectangle x1="13.91158125" y1="3.69696875" x2="14.68881875" y2="3.715003125" layer="94"/>
<rectangle x1="15.66418125" y1="3.69696875" x2="16.405859375" y2="3.715003125" layer="94"/>
<rectangle x1="17.490440625" y1="3.69696875" x2="18.23211875" y2="3.715003125" layer="94"/>
<rectangle x1="23.114" y1="3.69696875" x2="23.99791875" y2="3.715003125" layer="94"/>
<rectangle x1="-0.010159375" y1="3.715003125" x2="0.85851875" y2="3.733290625" layer="94"/>
<rectangle x1="2.232659375" y1="3.715003125" x2="3.11911875" y2="3.733290625" layer="94"/>
<rectangle x1="4.52881875" y1="3.715003125" x2="5.25271875" y2="3.733290625" layer="94"/>
<rectangle x1="5.323840625" y1="3.715003125" x2="6.029959375" y2="3.733290625" layer="94"/>
<rectangle x1="7.54888125" y1="3.715003125" x2="8.28801875" y2="3.733290625" layer="94"/>
<rectangle x1="9.591040625" y1="3.715003125" x2="10.33271875" y2="3.733290625" layer="94"/>
<rectangle x1="11.922759375" y1="3.715003125" x2="12.71778125" y2="3.733290625" layer="94"/>
<rectangle x1="13.929359375" y1="3.715003125" x2="14.7066" y2="3.733290625" layer="94"/>
<rectangle x1="15.66418125" y1="3.715003125" x2="16.405859375" y2="3.733290625" layer="94"/>
<rectangle x1="17.490440625" y1="3.715003125" x2="18.23211875" y2="3.733290625" layer="94"/>
<rectangle x1="23.09621875" y1="3.715003125" x2="23.99791875" y2="3.733290625" layer="94"/>
<rectangle x1="-0.010159375" y1="3.733290625" x2="0.85851875" y2="3.751325" layer="94"/>
<rectangle x1="2.250440625" y1="3.733290625" x2="3.11911875" y2="3.751325" layer="94"/>
<rectangle x1="4.52881875" y1="3.733290625" x2="5.25271875" y2="3.751325" layer="94"/>
<rectangle x1="5.34161875" y1="3.733290625" x2="6.047740625" y2="3.751325" layer="94"/>
<rectangle x1="7.54888125" y1="3.733290625" x2="8.28801875" y2="3.751325" layer="94"/>
<rectangle x1="9.591040625" y1="3.733290625" x2="10.33271875" y2="3.751325" layer="94"/>
<rectangle x1="11.940540625" y1="3.733290625" x2="12.71778125" y2="3.751325" layer="94"/>
<rectangle x1="13.929359375" y1="3.733290625" x2="14.72438125" y2="3.751325" layer="94"/>
<rectangle x1="15.66418125" y1="3.733290625" x2="16.405859375" y2="3.751325" layer="94"/>
<rectangle x1="17.490440625" y1="3.733290625" x2="18.23211875" y2="3.751325" layer="94"/>
<rectangle x1="23.09621875" y1="3.733290625" x2="23.99791875" y2="3.751325" layer="94"/>
<rectangle x1="-0.010159375" y1="3.751325" x2="0.85851875" y2="3.769359375" layer="94"/>
<rectangle x1="2.26821875" y1="3.751325" x2="3.1369" y2="3.769359375" layer="94"/>
<rectangle x1="4.52881875" y1="3.751325" x2="5.25271875" y2="3.769359375" layer="94"/>
<rectangle x1="5.3594" y1="3.751325" x2="6.047740625" y2="3.769359375" layer="94"/>
<rectangle x1="7.54888125" y1="3.751325" x2="8.3058" y2="3.769359375" layer="94"/>
<rectangle x1="9.60881875" y1="3.751325" x2="10.3505" y2="3.769359375" layer="94"/>
<rectangle x1="11.940540625" y1="3.751325" x2="12.71778125" y2="3.769359375" layer="94"/>
<rectangle x1="13.947140625" y1="3.751325" x2="14.72438125" y2="3.769359375" layer="94"/>
<rectangle x1="15.681959375" y1="3.751325" x2="16.423640625" y2="3.769359375" layer="94"/>
<rectangle x1="17.50821875" y1="3.751325" x2="18.2499" y2="3.769359375" layer="94"/>
<rectangle x1="23.09621875" y1="3.751325" x2="23.99791875" y2="3.769359375" layer="94"/>
<rectangle x1="-0.010159375" y1="3.769359375" x2="0.85851875" y2="3.78739375" layer="94"/>
<rectangle x1="2.286" y1="3.769359375" x2="3.15468125" y2="3.78739375" layer="94"/>
<rectangle x1="4.5466" y1="3.769359375" x2="5.25271875" y2="3.78739375" layer="94"/>
<rectangle x1="5.3594" y1="3.769359375" x2="6.06551875" y2="3.78739375" layer="94"/>
<rectangle x1="7.566659375" y1="3.769359375" x2="8.3058" y2="3.78739375" layer="94"/>
<rectangle x1="9.60881875" y1="3.769359375" x2="10.3505" y2="3.78739375" layer="94"/>
<rectangle x1="11.940540625" y1="3.769359375" x2="12.71778125" y2="3.78739375" layer="94"/>
<rectangle x1="13.96491875" y1="3.769359375" x2="14.742159375" y2="3.78739375" layer="94"/>
<rectangle x1="15.681959375" y1="3.769359375" x2="16.423640625" y2="3.78739375" layer="94"/>
<rectangle x1="17.50821875" y1="3.769359375" x2="18.2499" y2="3.78739375" layer="94"/>
<rectangle x1="23.09621875" y1="3.769359375" x2="23.99791875" y2="3.78739375" layer="94"/>
<rectangle x1="-0.010159375" y1="3.78739375" x2="0.840740625" y2="3.805428125" layer="94"/>
<rectangle x1="2.30378125" y1="3.78739375" x2="3.172459375" y2="3.805428125" layer="94"/>
<rectangle x1="4.5466" y1="3.78739375" x2="5.25271875" y2="3.805428125" layer="94"/>
<rectangle x1="5.37718125" y1="3.78739375" x2="6.0833" y2="3.805428125" layer="94"/>
<rectangle x1="7.566659375" y1="3.78739375" x2="8.3058" y2="3.805428125" layer="94"/>
<rectangle x1="9.60881875" y1="3.78739375" x2="10.3505" y2="3.805428125" layer="94"/>
<rectangle x1="11.940540625" y1="3.78739375" x2="12.71778125" y2="3.805428125" layer="94"/>
<rectangle x1="13.9827" y1="3.78739375" x2="14.759940625" y2="3.805428125" layer="94"/>
<rectangle x1="15.681959375" y1="3.78739375" x2="16.423640625" y2="3.805428125" layer="94"/>
<rectangle x1="17.50821875" y1="3.78739375" x2="18.2499" y2="3.805428125" layer="94"/>
<rectangle x1="23.09621875" y1="3.78739375" x2="23.99791875" y2="3.805428125" layer="94"/>
<rectangle x1="-0.010159375" y1="3.805428125" x2="0.840740625" y2="3.8234625" layer="94"/>
<rectangle x1="2.3241" y1="3.805428125" x2="3.172459375" y2="3.8234625" layer="94"/>
<rectangle x1="4.5466" y1="3.805428125" x2="5.25271875" y2="3.8234625" layer="94"/>
<rectangle x1="5.3975" y1="3.805428125" x2="6.0833" y2="3.8234625" layer="94"/>
<rectangle x1="7.566659375" y1="3.805428125" x2="8.32611875" y2="3.8234625" layer="94"/>
<rectangle x1="9.6266" y1="3.805428125" x2="10.36828125" y2="3.8234625" layer="94"/>
<rectangle x1="11.940540625" y1="3.805428125" x2="12.71778125" y2="3.8234625" layer="94"/>
<rectangle x1="13.9827" y1="3.805428125" x2="14.759940625" y2="3.8234625" layer="94"/>
<rectangle x1="15.699740625" y1="3.805428125" x2="16.44141875" y2="3.8234625" layer="94"/>
<rectangle x1="17.526" y1="3.805428125" x2="18.26768125" y2="3.8234625" layer="94"/>
<rectangle x1="23.0759" y1="3.805428125" x2="23.99791875" y2="3.8234625" layer="94"/>
<rectangle x1="-0.010159375" y1="3.8234625" x2="0.840740625" y2="3.84175" layer="94"/>
<rectangle x1="2.34188125" y1="3.8234625" x2="3.190240625" y2="3.84175" layer="94"/>
<rectangle x1="4.5466" y1="3.8234625" x2="5.25271875" y2="3.84175" layer="94"/>
<rectangle x1="5.3975" y1="3.8234625" x2="6.10108125" y2="3.84175" layer="94"/>
<rectangle x1="7.584440625" y1="3.8234625" x2="8.32611875" y2="3.84175" layer="94"/>
<rectangle x1="9.6266" y1="3.8234625" x2="10.36828125" y2="3.84175" layer="94"/>
<rectangle x1="11.940540625" y1="3.8234625" x2="12.71778125" y2="3.84175" layer="94"/>
<rectangle x1="14.00048125" y1="3.8234625" x2="14.780259375" y2="3.84175" layer="94"/>
<rectangle x1="15.699740625" y1="3.8234625" x2="16.44141875" y2="3.84175" layer="94"/>
<rectangle x1="17.526" y1="3.8234625" x2="18.26768125" y2="3.84175" layer="94"/>
<rectangle x1="23.0759" y1="3.8234625" x2="23.99791875" y2="3.84175" layer="94"/>
<rectangle x1="-0.010159375" y1="3.84175" x2="0.840740625" y2="3.859784375" layer="94"/>
<rectangle x1="2.359659375" y1="3.84175" x2="3.20801875" y2="3.859784375" layer="94"/>
<rectangle x1="4.5466" y1="3.84175" x2="5.25271875" y2="3.859784375" layer="94"/>
<rectangle x1="5.41528125" y1="3.84175" x2="6.118859375" y2="3.859784375" layer="94"/>
<rectangle x1="7.584440625" y1="3.84175" x2="8.32611875" y2="3.859784375" layer="94"/>
<rectangle x1="9.6266" y1="3.84175" x2="10.386059375" y2="3.859784375" layer="94"/>
<rectangle x1="11.95831875" y1="3.84175" x2="12.71778125" y2="3.859784375" layer="94"/>
<rectangle x1="14.0208" y1="3.84175" x2="14.780259375" y2="3.859784375" layer="94"/>
<rectangle x1="15.699740625" y1="3.84175" x2="16.44141875" y2="3.859784375" layer="94"/>
<rectangle x1="17.526" y1="3.84175" x2="18.26768125" y2="3.859784375" layer="94"/>
<rectangle x1="23.0759" y1="3.84175" x2="23.99791875" y2="3.859784375" layer="94"/>
<rectangle x1="-0.010159375" y1="3.859784375" x2="0.840740625" y2="3.87781875" layer="94"/>
<rectangle x1="2.377440625" y1="3.859784375" x2="3.20801875" y2="3.87781875" layer="94"/>
<rectangle x1="4.5466" y1="3.859784375" x2="5.25271875" y2="3.87781875" layer="94"/>
<rectangle x1="5.433059375" y1="3.859784375" x2="6.136640625" y2="3.87781875" layer="94"/>
<rectangle x1="7.584440625" y1="3.859784375" x2="8.3439" y2="3.87781875" layer="94"/>
<rectangle x1="9.64438125" y1="3.859784375" x2="10.386059375" y2="3.87781875" layer="94"/>
<rectangle x1="11.95831875" y1="3.859784375" x2="12.71778125" y2="3.87781875" layer="94"/>
<rectangle x1="14.0208" y1="3.859784375" x2="14.798040625" y2="3.87781875" layer="94"/>
<rectangle x1="15.720059375" y1="3.859784375" x2="16.4592" y2="3.87781875" layer="94"/>
<rectangle x1="17.54378125" y1="3.859784375" x2="18.285459375" y2="3.87781875" layer="94"/>
<rectangle x1="23.0759" y1="3.859784375" x2="23.980140625" y2="3.87781875" layer="94"/>
<rectangle x1="-0.010159375" y1="3.87781875" x2="0.840740625" y2="3.895853125" layer="94"/>
<rectangle x1="2.377440625" y1="3.87781875" x2="3.2258" y2="3.895853125" layer="94"/>
<rectangle x1="4.5466" y1="3.87781875" x2="5.25271875" y2="3.895853125" layer="94"/>
<rectangle x1="5.433059375" y1="3.87781875" x2="6.136640625" y2="3.895853125" layer="94"/>
<rectangle x1="7.60221875" y1="3.87781875" x2="8.3439" y2="3.895853125" layer="94"/>
<rectangle x1="9.64438125" y1="3.87781875" x2="10.386059375" y2="3.895853125" layer="94"/>
<rectangle x1="11.95831875" y1="3.87781875" x2="12.71778125" y2="3.895853125" layer="94"/>
<rectangle x1="14.03858125" y1="3.87781875" x2="14.798040625" y2="3.895853125" layer="94"/>
<rectangle x1="15.720059375" y1="3.87781875" x2="16.4592" y2="3.895853125" layer="94"/>
<rectangle x1="17.54378125" y1="3.87781875" x2="18.285459375" y2="3.895853125" layer="94"/>
<rectangle x1="23.05811875" y1="3.87781875" x2="23.980140625" y2="3.895853125" layer="94"/>
<rectangle x1="-0.010159375" y1="3.895853125" x2="0.822959375" y2="3.9138875" layer="94"/>
<rectangle x1="2.39521875" y1="3.895853125" x2="3.24611875" y2="3.9138875" layer="94"/>
<rectangle x1="4.5466" y1="3.895853125" x2="5.25271875" y2="3.9138875" layer="94"/>
<rectangle x1="5.450840625" y1="3.895853125" x2="6.15441875" y2="3.9138875" layer="94"/>
<rectangle x1="7.60221875" y1="3.895853125" x2="8.3439" y2="3.9138875" layer="94"/>
<rectangle x1="9.662159375" y1="3.895853125" x2="10.403840625" y2="3.9138875" layer="94"/>
<rectangle x1="11.95831875" y1="3.895853125" x2="12.735559375" y2="3.9138875" layer="94"/>
<rectangle x1="14.03858125" y1="3.895853125" x2="14.81581875" y2="3.9138875" layer="94"/>
<rectangle x1="15.737840625" y1="3.895853125" x2="16.47951875" y2="3.9138875" layer="94"/>
<rectangle x1="17.5641" y1="3.895853125" x2="18.303240625" y2="3.9138875" layer="94"/>
<rectangle x1="23.05811875" y1="3.895853125" x2="23.980140625" y2="3.9138875" layer="94"/>
<rectangle x1="-0.010159375" y1="3.9138875" x2="0.822959375" y2="3.93191875" layer="94"/>
<rectangle x1="2.413" y1="3.9138875" x2="3.24611875" y2="3.93191875" layer="94"/>
<rectangle x1="4.5466" y1="3.9138875" x2="5.25271875" y2="3.93191875" layer="94"/>
<rectangle x1="5.46861875" y1="3.9138875" x2="6.174740625" y2="3.93191875" layer="94"/>
<rectangle x1="7.62" y1="3.9138875" x2="8.36168125" y2="3.93191875" layer="94"/>
<rectangle x1="9.662159375" y1="3.9138875" x2="10.403840625" y2="3.93191875" layer="94"/>
<rectangle x1="11.95831875" y1="3.9138875" x2="12.735559375" y2="3.93191875" layer="94"/>
<rectangle x1="14.056359375" y1="3.9138875" x2="14.81581875" y2="3.93191875" layer="94"/>
<rectangle x1="15.737840625" y1="3.9138875" x2="16.47951875" y2="3.93191875" layer="94"/>
<rectangle x1="17.5641" y1="3.9138875" x2="18.303240625" y2="3.93191875" layer="94"/>
<rectangle x1="23.05811875" y1="3.9138875" x2="23.980140625" y2="3.93191875" layer="94"/>
<rectangle x1="-0.010159375" y1="3.93191875" x2="0.822959375" y2="3.950209375" layer="94"/>
<rectangle x1="2.43078125" y1="3.93191875" x2="3.2639" y2="3.950209375" layer="94"/>
<rectangle x1="4.5466" y1="3.93191875" x2="5.25271875" y2="3.950209375" layer="94"/>
<rectangle x1="5.46861875" y1="3.93191875" x2="6.174740625" y2="3.950209375" layer="94"/>
<rectangle x1="7.62" y1="3.93191875" x2="8.36168125" y2="3.950209375" layer="94"/>
<rectangle x1="9.662159375" y1="3.93191875" x2="10.403840625" y2="3.950209375" layer="94"/>
<rectangle x1="11.9761" y1="3.93191875" x2="12.735559375" y2="3.950209375" layer="94"/>
<rectangle x1="14.074140625" y1="3.93191875" x2="14.8336" y2="3.950209375" layer="94"/>
<rectangle x1="15.737840625" y1="3.93191875" x2="16.47951875" y2="3.950209375" layer="94"/>
<rectangle x1="17.5641" y1="3.93191875" x2="18.303240625" y2="3.950209375" layer="94"/>
<rectangle x1="23.05811875" y1="3.93191875" x2="23.980140625" y2="3.950209375" layer="94"/>
<rectangle x1="0.010159375" y1="3.950209375" x2="0.822959375" y2="3.968240625" layer="94"/>
<rectangle x1="2.43078125" y1="3.950209375" x2="3.28168125" y2="3.968240625" layer="94"/>
<rectangle x1="4.5466" y1="3.950209375" x2="5.25271875" y2="3.968240625" layer="94"/>
<rectangle x1="5.4864" y1="3.950209375" x2="6.19251875" y2="3.968240625" layer="94"/>
<rectangle x1="7.62" y1="3.950209375" x2="8.36168125" y2="3.968240625" layer="94"/>
<rectangle x1="9.679940625" y1="3.950209375" x2="10.42161875" y2="3.968240625" layer="94"/>
<rectangle x1="11.9761" y1="3.950209375" x2="12.735559375" y2="3.968240625" layer="94"/>
<rectangle x1="14.074140625" y1="3.950209375" x2="14.8336" y2="3.968240625" layer="94"/>
<rectangle x1="15.75561875" y1="3.950209375" x2="16.4973" y2="3.968240625" layer="94"/>
<rectangle x1="17.58188125" y1="3.950209375" x2="18.323559375" y2="3.968240625" layer="94"/>
<rectangle x1="23.040340625" y1="3.950209375" x2="23.962359375" y2="3.968240625" layer="94"/>
<rectangle x1="0.010159375" y1="3.968240625" x2="0.822959375" y2="3.986275" layer="94"/>
<rectangle x1="2.448559375" y1="3.968240625" x2="3.28168125" y2="3.986275" layer="94"/>
<rectangle x1="4.5466" y1="3.968240625" x2="5.2705" y2="3.986275" layer="94"/>
<rectangle x1="5.50418125" y1="3.968240625" x2="6.2103" y2="3.986275" layer="94"/>
<rectangle x1="7.63778125" y1="3.968240625" x2="8.379459375" y2="3.986275" layer="94"/>
<rectangle x1="9.679940625" y1="3.968240625" x2="10.42161875" y2="3.986275" layer="94"/>
<rectangle x1="11.9761" y1="3.968240625" x2="12.735559375" y2="3.986275" layer="94"/>
<rectangle x1="14.09191875" y1="3.968240625" x2="14.85138125" y2="3.986275" layer="94"/>
<rectangle x1="15.75561875" y1="3.968240625" x2="16.4973" y2="3.986275" layer="94"/>
<rectangle x1="17.58188125" y1="3.968240625" x2="18.323559375" y2="3.986275" layer="94"/>
<rectangle x1="23.040340625" y1="3.968240625" x2="23.962359375" y2="3.986275" layer="94"/>
<rectangle x1="0.010159375" y1="3.986275" x2="0.822959375" y2="4.004309375" layer="94"/>
<rectangle x1="2.46888125" y1="3.986275" x2="3.299459375" y2="4.004309375" layer="94"/>
<rectangle x1="4.5466" y1="3.986275" x2="5.2705" y2="4.004309375" layer="94"/>
<rectangle x1="5.50418125" y1="3.986275" x2="6.2103" y2="4.004309375" layer="94"/>
<rectangle x1="7.63778125" y1="3.986275" x2="8.379459375" y2="4.004309375" layer="94"/>
<rectangle x1="9.679940625" y1="3.986275" x2="10.4394" y2="4.004309375" layer="94"/>
<rectangle x1="11.9761" y1="3.986275" x2="12.735559375" y2="4.004309375" layer="94"/>
<rectangle x1="14.09191875" y1="3.986275" x2="14.85138125" y2="4.004309375" layer="94"/>
<rectangle x1="15.75561875" y1="3.986275" x2="16.4973" y2="4.004309375" layer="94"/>
<rectangle x1="17.58188125" y1="3.986275" x2="18.323559375" y2="4.004309375" layer="94"/>
<rectangle x1="23.022559375" y1="3.986275" x2="23.962359375" y2="4.004309375" layer="94"/>
<rectangle x1="0.010159375" y1="4.004309375" x2="0.822959375" y2="4.02234375" layer="94"/>
<rectangle x1="2.486659375" y1="4.004309375" x2="3.2639" y2="4.02234375" layer="94"/>
<rectangle x1="4.5466" y1="4.004309375" x2="5.2705" y2="4.02234375" layer="94"/>
<rectangle x1="5.521959375" y1="4.004309375" x2="6.22808125" y2="4.02234375" layer="94"/>
<rectangle x1="7.63778125" y1="4.004309375" x2="8.397240625" y2="4.02234375" layer="94"/>
<rectangle x1="9.700259375" y1="4.004309375" x2="10.4394" y2="4.02234375" layer="94"/>
<rectangle x1="11.99388125" y1="4.004309375" x2="12.753340625" y2="4.02234375" layer="94"/>
<rectangle x1="14.1097" y1="4.004309375" x2="14.869159375" y2="4.02234375" layer="94"/>
<rectangle x1="15.7734" y1="4.004309375" x2="16.51508125" y2="4.02234375" layer="94"/>
<rectangle x1="17.599659375" y1="4.004309375" x2="18.341340625" y2="4.02234375" layer="94"/>
<rectangle x1="23.022559375" y1="4.004309375" x2="23.962359375" y2="4.02234375" layer="94"/>
<rectangle x1="0.010159375" y1="4.02234375" x2="0.822959375" y2="4.040378125" layer="94"/>
<rectangle x1="2.486659375" y1="4.02234375" x2="3.20801875" y2="4.040378125" layer="94"/>
<rectangle x1="4.5466" y1="4.02234375" x2="5.2705" y2="4.040378125" layer="94"/>
<rectangle x1="5.539740625" y1="4.02234375" x2="6.245859375" y2="4.040378125" layer="94"/>
<rectangle x1="7.655559375" y1="4.02234375" x2="8.397240625" y2="4.040378125" layer="94"/>
<rectangle x1="9.700259375" y1="4.02234375" x2="10.4394" y2="4.040378125" layer="94"/>
<rectangle x1="11.99388125" y1="4.02234375" x2="12.753340625" y2="4.040378125" layer="94"/>
<rectangle x1="14.1097" y1="4.02234375" x2="14.869159375" y2="4.040378125" layer="94"/>
<rectangle x1="15.7734" y1="4.02234375" x2="16.51508125" y2="4.040378125" layer="94"/>
<rectangle x1="17.599659375" y1="4.02234375" x2="18.341340625" y2="4.040378125" layer="94"/>
<rectangle x1="23.022559375" y1="4.02234375" x2="23.94458125" y2="4.040378125" layer="94"/>
<rectangle x1="0.010159375" y1="4.040378125" x2="0.822959375" y2="4.058665625" layer="94"/>
<rectangle x1="2.504440625" y1="4.040378125" x2="3.172459375" y2="4.058665625" layer="94"/>
<rectangle x1="4.5466" y1="4.040378125" x2="5.2705" y2="4.058665625" layer="94"/>
<rectangle x1="5.539740625" y1="4.040378125" x2="6.263640625" y2="4.058665625" layer="94"/>
<rectangle x1="7.655559375" y1="4.040378125" x2="8.397240625" y2="4.058665625" layer="94"/>
<rectangle x1="9.700259375" y1="4.040378125" x2="10.45718125" y2="4.058665625" layer="94"/>
<rectangle x1="11.99388125" y1="4.040378125" x2="12.753340625" y2="4.058665625" layer="94"/>
<rectangle x1="14.12748125" y1="4.040378125" x2="14.886940625" y2="4.058665625" layer="94"/>
<rectangle x1="15.7734" y1="4.040378125" x2="16.51508125" y2="4.058665625" layer="94"/>
<rectangle x1="17.599659375" y1="4.040378125" x2="18.341340625" y2="4.058665625" layer="94"/>
<rectangle x1="23.00478125" y1="4.040378125" x2="23.94458125" y2="4.058665625" layer="94"/>
<rectangle x1="0.010159375" y1="4.058665625" x2="0.822959375" y2="4.0767" layer="94"/>
<rectangle x1="2.52221875" y1="4.058665625" x2="3.11911875" y2="4.0767" layer="94"/>
<rectangle x1="4.5466" y1="4.058665625" x2="5.2705" y2="4.0767" layer="94"/>
<rectangle x1="5.560059375" y1="4.058665625" x2="6.263640625" y2="4.0767" layer="94"/>
<rectangle x1="7.655559375" y1="4.058665625" x2="8.41501875" y2="4.0767" layer="94"/>
<rectangle x1="9.718040625" y1="4.058665625" x2="10.45718125" y2="4.0767" layer="94"/>
<rectangle x1="11.99388125" y1="4.058665625" x2="12.753340625" y2="4.0767" layer="94"/>
<rectangle x1="14.12748125" y1="4.058665625" x2="14.886940625" y2="4.0767" layer="94"/>
<rectangle x1="15.79118125" y1="4.058665625" x2="16.532859375" y2="4.0767" layer="94"/>
<rectangle x1="17.617440625" y1="4.058665625" x2="18.35911875" y2="4.0767" layer="94"/>
<rectangle x1="23.00478125" y1="4.058665625" x2="23.94458125" y2="4.0767" layer="94"/>
<rectangle x1="0.010159375" y1="4.0767" x2="0.840740625" y2="4.094734375" layer="94"/>
<rectangle x1="2.52221875" y1="4.0767" x2="3.083559375" y2="4.094734375" layer="94"/>
<rectangle x1="4.5466" y1="4.0767" x2="5.2705" y2="4.094734375" layer="94"/>
<rectangle x1="5.577840625" y1="4.0767" x2="6.28141875" y2="4.094734375" layer="94"/>
<rectangle x1="7.673340625" y1="4.0767" x2="8.41501875" y2="4.094734375" layer="94"/>
<rectangle x1="9.718040625" y1="4.0767" x2="10.4775" y2="4.094734375" layer="94"/>
<rectangle x1="12.0142" y1="4.0767" x2="12.77111875" y2="4.094734375" layer="94"/>
<rectangle x1="14.145259375" y1="4.0767" x2="14.90471875" y2="4.094734375" layer="94"/>
<rectangle x1="15.79118125" y1="4.0767" x2="16.532859375" y2="4.094734375" layer="94"/>
<rectangle x1="17.617440625" y1="4.0767" x2="18.35911875" y2="4.094734375" layer="94"/>
<rectangle x1="22.987" y1="4.0767" x2="23.9268" y2="4.094734375" layer="94"/>
<rectangle x1="0.010159375" y1="4.094734375" x2="0.840740625" y2="4.11276875" layer="94"/>
<rectangle x1="2.54" y1="4.094734375" x2="3.02768125" y2="4.11276875" layer="94"/>
<rectangle x1="4.5466" y1="4.094734375" x2="5.2705" y2="4.11276875" layer="94"/>
<rectangle x1="5.577840625" y1="4.094734375" x2="6.2992" y2="4.11276875" layer="94"/>
<rectangle x1="7.673340625" y1="4.094734375" x2="8.41501875" y2="4.11276875" layer="94"/>
<rectangle x1="9.73581875" y1="4.094734375" x2="10.4775" y2="4.11276875" layer="94"/>
<rectangle x1="12.0142" y1="4.094734375" x2="12.77111875" y2="4.11276875" layer="94"/>
<rectangle x1="14.145259375" y1="4.094734375" x2="14.90471875" y2="4.11276875" layer="94"/>
<rectangle x1="15.808959375" y1="4.094734375" x2="16.550640625" y2="4.11276875" layer="94"/>
<rectangle x1="17.63521875" y1="4.094734375" x2="18.3769" y2="4.11276875" layer="94"/>
<rectangle x1="22.987" y1="4.094734375" x2="23.9268" y2="4.11276875" layer="94"/>
<rectangle x1="0.027940625" y1="4.11276875" x2="0.840740625" y2="4.130803125" layer="94"/>
<rectangle x1="2.55778125" y1="4.11276875" x2="2.974340625" y2="4.130803125" layer="94"/>
<rectangle x1="4.5466" y1="4.11276875" x2="5.2705" y2="4.130803125" layer="94"/>
<rectangle x1="5.59561875" y1="4.11276875" x2="6.2992" y2="4.130803125" layer="94"/>
<rectangle x1="7.69111875" y1="4.11276875" x2="8.4328" y2="4.130803125" layer="94"/>
<rectangle x1="9.73581875" y1="4.11276875" x2="10.4775" y2="4.130803125" layer="94"/>
<rectangle x1="12.0142" y1="4.11276875" x2="12.77111875" y2="4.130803125" layer="94"/>
<rectangle x1="14.145259375" y1="4.11276875" x2="14.9225" y2="4.130803125" layer="94"/>
<rectangle x1="15.808959375" y1="4.11276875" x2="16.550640625" y2="4.130803125" layer="94"/>
<rectangle x1="17.63521875" y1="4.11276875" x2="18.3769" y2="4.130803125" layer="94"/>
<rectangle x1="22.987" y1="4.11276875" x2="23.9268" y2="4.130803125" layer="94"/>
<rectangle x1="0.027940625" y1="4.130803125" x2="0.840740625" y2="4.149090625" layer="94"/>
<rectangle x1="2.55778125" y1="4.130803125" x2="2.93878125" y2="4.149090625" layer="94"/>
<rectangle x1="4.5466" y1="4.130803125" x2="5.2705" y2="4.149090625" layer="94"/>
<rectangle x1="5.6134" y1="4.130803125" x2="6.31951875" y2="4.149090625" layer="94"/>
<rectangle x1="7.69111875" y1="4.130803125" x2="8.4328" y2="4.149090625" layer="94"/>
<rectangle x1="9.73581875" y1="4.130803125" x2="10.49528125" y2="4.149090625" layer="94"/>
<rectangle x1="12.03198125" y1="4.130803125" x2="12.791440625" y2="4.149090625" layer="94"/>
<rectangle x1="14.16558125" y1="4.130803125" x2="14.9225" y2="4.149090625" layer="94"/>
<rectangle x1="15.808959375" y1="4.130803125" x2="16.550640625" y2="4.149090625" layer="94"/>
<rectangle x1="17.63521875" y1="4.130803125" x2="18.3769" y2="4.149090625" layer="94"/>
<rectangle x1="22.96921875" y1="4.130803125" x2="23.90901875" y2="4.149090625" layer="94"/>
<rectangle x1="0.027940625" y1="4.149090625" x2="0.840740625" y2="4.167125" layer="94"/>
<rectangle x1="2.575559375" y1="4.149090625" x2="2.8829" y2="4.167125" layer="94"/>
<rectangle x1="4.5466" y1="4.149090625" x2="5.2705" y2="4.167125" layer="94"/>
<rectangle x1="5.6134" y1="4.149090625" x2="6.3373" y2="4.167125" layer="94"/>
<rectangle x1="7.69111875" y1="4.149090625" x2="8.4328" y2="4.167125" layer="94"/>
<rectangle x1="9.7536" y1="4.149090625" x2="10.49528125" y2="4.167125" layer="94"/>
<rectangle x1="12.03198125" y1="4.149090625" x2="12.791440625" y2="4.167125" layer="94"/>
<rectangle x1="14.16558125" y1="4.149090625" x2="14.9225" y2="4.167125" layer="94"/>
<rectangle x1="15.826740625" y1="4.149090625" x2="16.56841875" y2="4.167125" layer="94"/>
<rectangle x1="17.653" y1="4.149090625" x2="18.39468125" y2="4.167125" layer="94"/>
<rectangle x1="22.96921875" y1="4.149090625" x2="23.90901875" y2="4.167125" layer="94"/>
<rectangle x1="0.027940625" y1="4.167125" x2="0.840740625" y2="4.185159375" layer="94"/>
<rectangle x1="2.593340625" y1="4.167125" x2="2.847340625" y2="4.185159375" layer="94"/>
<rectangle x1="4.5466" y1="4.167125" x2="5.2705" y2="4.185159375" layer="94"/>
<rectangle x1="5.63118125" y1="4.167125" x2="6.3373" y2="4.185159375" layer="94"/>
<rectangle x1="7.711440625" y1="4.167125" x2="8.45058125" y2="4.185159375" layer="94"/>
<rectangle x1="9.7536" y1="4.167125" x2="10.513059375" y2="4.185159375" layer="94"/>
<rectangle x1="12.03198125" y1="4.167125" x2="12.791440625" y2="4.185159375" layer="94"/>
<rectangle x1="14.183359375" y1="4.167125" x2="14.94281875" y2="4.185159375" layer="94"/>
<rectangle x1="15.826740625" y1="4.167125" x2="16.56841875" y2="4.185159375" layer="94"/>
<rectangle x1="17.653" y1="4.167125" x2="18.39468125" y2="4.185159375" layer="94"/>
<rectangle x1="22.951440625" y1="4.167125" x2="23.90901875" y2="4.185159375" layer="94"/>
<rectangle x1="0.027940625" y1="4.185159375" x2="0.840740625" y2="4.203190625" layer="94"/>
<rectangle x1="2.593340625" y1="4.185159375" x2="2.794" y2="4.203190625" layer="94"/>
<rectangle x1="4.5466" y1="4.185159375" x2="5.2705" y2="4.203190625" layer="94"/>
<rectangle x1="5.648959375" y1="4.185159375" x2="6.35508125" y2="4.203190625" layer="94"/>
<rectangle x1="7.711440625" y1="4.185159375" x2="8.45058125" y2="4.203190625" layer="94"/>
<rectangle x1="9.7536" y1="4.185159375" x2="10.513059375" y2="4.203190625" layer="94"/>
<rectangle x1="12.049759375" y1="4.185159375" x2="12.80921875" y2="4.203190625" layer="94"/>
<rectangle x1="14.183359375" y1="4.185159375" x2="14.94281875" y2="4.203190625" layer="94"/>
<rectangle x1="15.826740625" y1="4.185159375" x2="16.56841875" y2="4.203190625" layer="94"/>
<rectangle x1="17.653" y1="4.185159375" x2="18.39468125" y2="4.203190625" layer="94"/>
<rectangle x1="22.951440625" y1="4.185159375" x2="23.891240625" y2="4.203190625" layer="94"/>
<rectangle x1="0.027940625" y1="4.203190625" x2="0.840740625" y2="4.221225" layer="94"/>
<rectangle x1="2.61111875" y1="4.203190625" x2="2.7559" y2="4.221225" layer="94"/>
<rectangle x1="4.5466" y1="4.203190625" x2="5.2705" y2="4.221225" layer="94"/>
<rectangle x1="5.648959375" y1="4.203190625" x2="6.372859375" y2="4.221225" layer="94"/>
<rectangle x1="7.711440625" y1="4.203190625" x2="8.4709" y2="4.221225" layer="94"/>
<rectangle x1="9.77138125" y1="4.203190625" x2="10.513059375" y2="4.221225" layer="94"/>
<rectangle x1="12.049759375" y1="4.203190625" x2="12.80921875" y2="4.221225" layer="94"/>
<rectangle x1="14.183359375" y1="4.203190625" x2="14.9606" y2="4.221225" layer="94"/>
<rectangle x1="15.84451875" y1="4.203190625" x2="16.5862" y2="4.221225" layer="94"/>
<rectangle x1="17.67078125" y1="4.203190625" x2="18.412459375" y2="4.221225" layer="94"/>
<rectangle x1="22.93111875" y1="4.203190625" x2="23.891240625" y2="4.221225" layer="94"/>
<rectangle x1="0.04571875" y1="4.221225" x2="0.85851875" y2="4.239259375" layer="94"/>
<rectangle x1="2.61111875" y1="4.221225" x2="2.702559375" y2="4.239259375" layer="94"/>
<rectangle x1="4.5466" y1="4.221225" x2="5.2705" y2="4.239259375" layer="94"/>
<rectangle x1="5.666740625" y1="4.221225" x2="6.390640625" y2="4.239259375" layer="94"/>
<rectangle x1="7.72921875" y1="4.221225" x2="8.4709" y2="4.239259375" layer="94"/>
<rectangle x1="9.77138125" y1="4.221225" x2="10.530840625" y2="4.239259375" layer="94"/>
<rectangle x1="12.049759375" y1="4.221225" x2="12.80921875" y2="4.239259375" layer="94"/>
<rectangle x1="14.201140625" y1="4.221225" x2="14.9606" y2="4.239259375" layer="94"/>
<rectangle x1="15.84451875" y1="4.221225" x2="16.5862" y2="4.239259375" layer="94"/>
<rectangle x1="17.67078125" y1="4.221225" x2="18.412459375" y2="4.239259375" layer="94"/>
<rectangle x1="22.913340625" y1="4.221225" x2="23.873459375" y2="4.239259375" layer="94"/>
<rectangle x1="0.04571875" y1="4.239259375" x2="0.85851875" y2="4.257546875" layer="94"/>
<rectangle x1="2.631440625" y1="4.239259375" x2="2.667" y2="4.257546875" layer="94"/>
<rectangle x1="4.5466" y1="4.239259375" x2="5.2705" y2="4.257546875" layer="94"/>
<rectangle x1="5.68451875" y1="4.239259375" x2="6.390640625" y2="4.257546875" layer="94"/>
<rectangle x1="7.72921875" y1="4.239259375" x2="8.4709" y2="4.257546875" layer="94"/>
<rectangle x1="9.77138125" y1="4.239259375" x2="10.530840625" y2="4.257546875" layer="94"/>
<rectangle x1="12.067540625" y1="4.239259375" x2="12.827" y2="4.257546875" layer="94"/>
<rectangle x1="14.201140625" y1="4.239259375" x2="14.9606" y2="4.257546875" layer="94"/>
<rectangle x1="15.84451875" y1="4.239259375" x2="16.60398125" y2="4.257546875" layer="94"/>
<rectangle x1="17.67078125" y1="4.239259375" x2="18.412459375" y2="4.257546875" layer="94"/>
<rectangle x1="22.913340625" y1="4.239259375" x2="23.873459375" y2="4.257546875" layer="94"/>
<rectangle x1="0.04571875" y1="4.257546875" x2="0.85851875" y2="4.27558125" layer="94"/>
<rectangle x1="4.5466" y1="4.257546875" x2="5.2705" y2="4.27558125" layer="94"/>
<rectangle x1="5.68451875" y1="4.257546875" x2="6.40841875" y2="4.27558125" layer="94"/>
<rectangle x1="7.72921875" y1="4.257546875" x2="8.48868125" y2="4.27558125" layer="94"/>
<rectangle x1="9.789159375" y1="4.257546875" x2="10.54861875" y2="4.27558125" layer="94"/>
<rectangle x1="12.067540625" y1="4.257546875" x2="12.827" y2="4.27558125" layer="94"/>
<rectangle x1="14.21891875" y1="4.257546875" x2="14.97838125" y2="4.27558125" layer="94"/>
<rectangle x1="15.864840625" y1="4.257546875" x2="16.60398125" y2="4.27558125" layer="94"/>
<rectangle x1="17.688559375" y1="4.257546875" x2="18.430240625" y2="4.27558125" layer="94"/>
<rectangle x1="22.895559375" y1="4.257546875" x2="23.873459375" y2="4.27558125" layer="94"/>
<rectangle x1="0.04571875" y1="4.27558125" x2="0.85851875" y2="4.293615625" layer="94"/>
<rectangle x1="4.5466" y1="4.27558125" x2="5.2705" y2="4.293615625" layer="94"/>
<rectangle x1="5.704840625" y1="4.27558125" x2="6.4262" y2="4.293615625" layer="94"/>
<rectangle x1="7.747" y1="4.27558125" x2="8.48868125" y2="4.293615625" layer="94"/>
<rectangle x1="9.789159375" y1="4.27558125" x2="10.54861875" y2="4.293615625" layer="94"/>
<rectangle x1="12.067540625" y1="4.27558125" x2="12.827" y2="4.293615625" layer="94"/>
<rectangle x1="14.21891875" y1="4.27558125" x2="14.97838125" y2="4.293615625" layer="94"/>
<rectangle x1="15.864840625" y1="4.27558125" x2="16.60398125" y2="4.293615625" layer="94"/>
<rectangle x1="17.688559375" y1="4.27558125" x2="18.430240625" y2="4.293615625" layer="94"/>
<rectangle x1="22.895559375" y1="4.27558125" x2="23.853140625" y2="4.293615625" layer="94"/>
<rectangle x1="0.0635" y1="4.293615625" x2="0.85851875" y2="4.31165" layer="94"/>
<rectangle x1="4.5466" y1="4.293615625" x2="5.2705" y2="4.31165" layer="94"/>
<rectangle x1="5.72261875" y1="4.293615625" x2="6.4262" y2="4.31165" layer="94"/>
<rectangle x1="7.747" y1="4.293615625" x2="8.48868125" y2="4.31165" layer="94"/>
<rectangle x1="9.806940625" y1="4.293615625" x2="10.54861875" y2="4.31165" layer="94"/>
<rectangle x1="12.08531875" y1="4.293615625" x2="12.84478125" y2="4.31165" layer="94"/>
<rectangle x1="14.21891875" y1="4.293615625" x2="14.97838125" y2="4.31165" layer="94"/>
<rectangle x1="15.88261875" y1="4.293615625" x2="16.621759375" y2="4.31165" layer="94"/>
<rectangle x1="17.70888125" y1="4.293615625" x2="18.44801875" y2="4.31165" layer="94"/>
<rectangle x1="22.87778125" y1="4.293615625" x2="23.853140625" y2="4.31165" layer="94"/>
<rectangle x1="0.0635" y1="4.31165" x2="0.8763" y2="4.329684375" layer="94"/>
<rectangle x1="4.5466" y1="4.31165" x2="5.28828125" y2="4.329684375" layer="94"/>
<rectangle x1="5.72261875" y1="4.31165" x2="6.44398125" y2="4.329684375" layer="94"/>
<rectangle x1="7.76478125" y1="4.31165" x2="8.506459375" y2="4.329684375" layer="94"/>
<rectangle x1="9.806940625" y1="4.31165" x2="10.5664" y2="4.329684375" layer="94"/>
<rectangle x1="12.08531875" y1="4.31165" x2="12.84478125" y2="4.329684375" layer="94"/>
<rectangle x1="14.2367" y1="4.31165" x2="14.996159375" y2="4.329684375" layer="94"/>
<rectangle x1="15.88261875" y1="4.31165" x2="16.621759375" y2="4.329684375" layer="94"/>
<rectangle x1="17.70888125" y1="4.31165" x2="18.44801875" y2="4.329684375" layer="94"/>
<rectangle x1="22.86" y1="4.31165" x2="23.835359375" y2="4.329684375" layer="94"/>
<rectangle x1="0.0635" y1="4.329684375" x2="0.8763" y2="4.34771875" layer="94"/>
<rectangle x1="4.5466" y1="4.329684375" x2="5.28828125" y2="4.34771875" layer="94"/>
<rectangle x1="5.7404" y1="4.329684375" x2="6.461759375" y2="4.34771875" layer="94"/>
<rectangle x1="7.76478125" y1="4.329684375" x2="8.506459375" y2="4.34771875" layer="94"/>
<rectangle x1="9.806940625" y1="4.329684375" x2="10.5664" y2="4.34771875" layer="94"/>
<rectangle x1="12.08531875" y1="4.329684375" x2="12.862559375" y2="4.34771875" layer="94"/>
<rectangle x1="14.2367" y1="4.329684375" x2="14.996159375" y2="4.34771875" layer="94"/>
<rectangle x1="15.88261875" y1="4.329684375" x2="16.64208125" y2="4.34771875" layer="94"/>
<rectangle x1="17.70888125" y1="4.329684375" x2="18.44801875" y2="4.34771875" layer="94"/>
<rectangle x1="22.86" y1="4.329684375" x2="23.835359375" y2="4.34771875" layer="94"/>
<rectangle x1="0.0635" y1="4.34771875" x2="0.8763" y2="4.36600625" layer="94"/>
<rectangle x1="4.5466" y1="4.34771875" x2="5.28828125" y2="4.36600625" layer="94"/>
<rectangle x1="5.75818125" y1="4.34771875" x2="6.461759375" y2="4.36600625" layer="94"/>
<rectangle x1="7.76478125" y1="4.34771875" x2="8.506459375" y2="4.36600625" layer="94"/>
<rectangle x1="9.82471875" y1="4.34771875" x2="10.58418125" y2="4.36600625" layer="94"/>
<rectangle x1="12.1031" y1="4.34771875" x2="12.862559375" y2="4.36600625" layer="94"/>
<rectangle x1="14.2367" y1="4.34771875" x2="14.996159375" y2="4.36600625" layer="94"/>
<rectangle x1="15.9004" y1="4.34771875" x2="16.64208125" y2="4.36600625" layer="94"/>
<rectangle x1="17.726659375" y1="4.34771875" x2="18.4658" y2="4.36600625" layer="94"/>
<rectangle x1="22.84221875" y1="4.34771875" x2="23.81758125" y2="4.36600625" layer="94"/>
<rectangle x1="0.0635" y1="4.36600625" x2="0.8763" y2="4.384040625" layer="94"/>
<rectangle x1="4.5466" y1="4.36600625" x2="5.28828125" y2="4.384040625" layer="94"/>
<rectangle x1="5.75818125" y1="4.36600625" x2="6.48208125" y2="4.384040625" layer="94"/>
<rectangle x1="7.782559375" y1="4.36600625" x2="8.524240625" y2="4.384040625" layer="94"/>
<rectangle x1="9.82471875" y1="4.36600625" x2="10.58418125" y2="4.384040625" layer="94"/>
<rectangle x1="12.1031" y1="4.36600625" x2="12.862559375" y2="4.384040625" layer="94"/>
<rectangle x1="14.2367" y1="4.36600625" x2="15.013940625" y2="4.384040625" layer="94"/>
<rectangle x1="15.9004" y1="4.36600625" x2="16.64208125" y2="4.384040625" layer="94"/>
<rectangle x1="17.726659375" y1="4.36600625" x2="18.4658" y2="4.384040625" layer="94"/>
<rectangle x1="22.824440625" y1="4.36600625" x2="23.81758125" y2="4.384040625" layer="94"/>
<rectangle x1="0.08128125" y1="4.384040625" x2="0.89408125" y2="4.402075" layer="94"/>
<rectangle x1="4.5466" y1="4.384040625" x2="5.28828125" y2="4.402075" layer="94"/>
<rectangle x1="5.775959375" y1="4.384040625" x2="6.499859375" y2="4.402075" layer="94"/>
<rectangle x1="7.782559375" y1="4.384040625" x2="8.524240625" y2="4.402075" layer="94"/>
<rectangle x1="9.82471875" y1="4.384040625" x2="10.601959375" y2="4.402075" layer="94"/>
<rectangle x1="12.12088125" y1="4.384040625" x2="12.880340625" y2="4.402075" layer="94"/>
<rectangle x1="14.25448125" y1="4.384040625" x2="15.013940625" y2="4.402075" layer="94"/>
<rectangle x1="15.9004" y1="4.384040625" x2="16.659859375" y2="4.402075" layer="94"/>
<rectangle x1="17.726659375" y1="4.384040625" x2="18.4658" y2="4.402075" layer="94"/>
<rectangle x1="22.824440625" y1="4.384040625" x2="23.7998" y2="4.402075" layer="94"/>
<rectangle x1="0.08128125" y1="4.402075" x2="0.89408125" y2="4.420109375" layer="94"/>
<rectangle x1="4.5466" y1="4.402075" x2="5.28828125" y2="4.420109375" layer="94"/>
<rectangle x1="5.793740625" y1="4.402075" x2="6.517640625" y2="4.420109375" layer="94"/>
<rectangle x1="7.782559375" y1="4.402075" x2="8.54201875" y2="4.420109375" layer="94"/>
<rectangle x1="9.8425" y1="4.402075" x2="10.601959375" y2="4.420109375" layer="94"/>
<rectangle x1="12.12088125" y1="4.402075" x2="12.880340625" y2="4.420109375" layer="94"/>
<rectangle x1="14.25448125" y1="4.402075" x2="15.013940625" y2="4.420109375" layer="94"/>
<rectangle x1="15.91818125" y1="4.402075" x2="16.659859375" y2="4.420109375" layer="94"/>
<rectangle x1="17.744440625" y1="4.402075" x2="18.48611875" y2="4.420109375" layer="94"/>
<rectangle x1="22.806659375" y1="4.402075" x2="23.7998" y2="4.420109375" layer="94"/>
<rectangle x1="0.08128125" y1="4.420109375" x2="0.89408125" y2="4.438140625" layer="94"/>
<rectangle x1="4.5466" y1="4.420109375" x2="5.28828125" y2="4.438140625" layer="94"/>
<rectangle x1="5.793740625" y1="4.420109375" x2="6.517640625" y2="4.438140625" layer="94"/>
<rectangle x1="7.800340625" y1="4.420109375" x2="8.54201875" y2="4.438140625" layer="94"/>
<rectangle x1="9.8425" y1="4.420109375" x2="10.619740625" y2="4.438140625" layer="94"/>
<rectangle x1="12.138659375" y1="4.420109375" x2="12.89811875" y2="4.438140625" layer="94"/>
<rectangle x1="14.25448125" y1="4.420109375" x2="15.03171875" y2="4.438140625" layer="94"/>
<rectangle x1="15.91818125" y1="4.420109375" x2="16.677640625" y2="4.438140625" layer="94"/>
<rectangle x1="17.744440625" y1="4.420109375" x2="18.48611875" y2="4.438140625" layer="94"/>
<rectangle x1="22.78888125" y1="4.420109375" x2="23.78201875" y2="4.438140625" layer="94"/>
<rectangle x1="0.08128125" y1="4.438140625" x2="0.89408125" y2="4.456175" layer="94"/>
<rectangle x1="4.5466" y1="4.438140625" x2="5.28828125" y2="4.456175" layer="94"/>
<rectangle x1="5.81151875" y1="4.438140625" x2="6.53541875" y2="4.456175" layer="94"/>
<rectangle x1="7.800340625" y1="4.438140625" x2="8.54201875" y2="4.456175" layer="94"/>
<rectangle x1="9.8425" y1="4.438140625" x2="10.619740625" y2="4.456175" layer="94"/>
<rectangle x1="12.138659375" y1="4.438140625" x2="12.89811875" y2="4.456175" layer="94"/>
<rectangle x1="14.272259375" y1="4.438140625" x2="15.03171875" y2="4.456175" layer="94"/>
<rectangle x1="15.91818125" y1="4.438140625" x2="16.677640625" y2="4.456175" layer="94"/>
<rectangle x1="17.744440625" y1="4.438140625" x2="18.48611875" y2="4.456175" layer="94"/>
<rectangle x1="22.78888125" y1="4.438140625" x2="23.78201875" y2="4.456175" layer="94"/>
<rectangle x1="0.099059375" y1="4.456175" x2="0.911859375" y2="4.4744625" layer="94"/>
<rectangle x1="4.5466" y1="4.456175" x2="5.28828125" y2="4.4744625" layer="94"/>
<rectangle x1="5.8293" y1="4.456175" x2="6.5532" y2="4.4744625" layer="94"/>
<rectangle x1="7.81811875" y1="4.456175" x2="8.5598" y2="4.4744625" layer="94"/>
<rectangle x1="9.86281875" y1="4.456175" x2="10.640059375" y2="4.4744625" layer="94"/>
<rectangle x1="12.138659375" y1="4.456175" x2="12.9159" y2="4.4744625" layer="94"/>
<rectangle x1="14.272259375" y1="4.456175" x2="15.03171875" y2="4.4744625" layer="94"/>
<rectangle x1="15.935959375" y1="4.456175" x2="16.677640625" y2="4.4744625" layer="94"/>
<rectangle x1="17.76221875" y1="4.456175" x2="18.5039" y2="4.4744625" layer="94"/>
<rectangle x1="22.768559375" y1="4.456175" x2="23.764240625" y2="4.4744625" layer="94"/>
<rectangle x1="0.099059375" y1="4.4744625" x2="0.911859375" y2="4.492496875" layer="94"/>
<rectangle x1="4.5466" y1="4.4744625" x2="5.28828125" y2="4.492496875" layer="94"/>
<rectangle x1="5.8293" y1="4.4744625" x2="6.5532" y2="4.492496875" layer="94"/>
<rectangle x1="7.81811875" y1="4.4744625" x2="8.5598" y2="4.492496875" layer="94"/>
<rectangle x1="9.86281875" y1="4.4744625" x2="10.640059375" y2="4.492496875" layer="94"/>
<rectangle x1="12.156440625" y1="4.4744625" x2="12.9159" y2="4.492496875" layer="94"/>
<rectangle x1="14.272259375" y1="4.4744625" x2="15.03171875" y2="4.492496875" layer="94"/>
<rectangle x1="15.935959375" y1="4.4744625" x2="16.69541875" y2="4.492496875" layer="94"/>
<rectangle x1="17.76221875" y1="4.4744625" x2="18.5039" y2="4.492496875" layer="94"/>
<rectangle x1="22.75078125" y1="4.4744625" x2="23.746459375" y2="4.492496875" layer="94"/>
<rectangle x1="0.099059375" y1="4.492496875" x2="0.911859375" y2="4.51053125" layer="94"/>
<rectangle x1="4.5466" y1="4.492496875" x2="5.28828125" y2="4.51053125" layer="94"/>
<rectangle x1="5.84708125" y1="4.492496875" x2="6.57098125" y2="4.51053125" layer="94"/>
<rectangle x1="7.81811875" y1="4.492496875" x2="8.5598" y2="4.51053125" layer="94"/>
<rectangle x1="9.8806" y1="4.492496875" x2="10.640059375" y2="4.51053125" layer="94"/>
<rectangle x1="12.156440625" y1="4.492496875" x2="12.93621875" y2="4.51053125" layer="94"/>
<rectangle x1="14.272259375" y1="4.492496875" x2="15.0495" y2="4.51053125" layer="94"/>
<rectangle x1="15.953740625" y1="4.492496875" x2="16.69541875" y2="4.51053125" layer="94"/>
<rectangle x1="17.76221875" y1="4.492496875" x2="18.52168125" y2="4.51053125" layer="94"/>
<rectangle x1="22.733" y1="4.492496875" x2="23.746459375" y2="4.51053125" layer="94"/>
<rectangle x1="0.116840625" y1="4.51053125" x2="0.911859375" y2="4.528565625" layer="94"/>
<rectangle x1="4.5466" y1="4.51053125" x2="5.28828125" y2="4.528565625" layer="94"/>
<rectangle x1="5.8674" y1="4.51053125" x2="6.588759375" y2="4.528565625" layer="94"/>
<rectangle x1="7.8359" y1="4.51053125" x2="8.57758125" y2="4.528565625" layer="94"/>
<rectangle x1="9.8806" y1="4.51053125" x2="10.657840625" y2="4.528565625" layer="94"/>
<rectangle x1="12.176759375" y1="4.51053125" x2="12.93621875" y2="4.528565625" layer="94"/>
<rectangle x1="14.272259375" y1="4.51053125" x2="15.0495" y2="4.528565625" layer="94"/>
<rectangle x1="15.953740625" y1="4.51053125" x2="16.7132" y2="4.528565625" layer="94"/>
<rectangle x1="17.78" y1="4.51053125" x2="18.52168125" y2="4.528565625" layer="94"/>
<rectangle x1="22.71521875" y1="4.51053125" x2="23.72868125" y2="4.528565625" layer="94"/>
<rectangle x1="0.116840625" y1="4.528565625" x2="0.93218125" y2="4.5466" layer="94"/>
<rectangle x1="4.5466" y1="4.528565625" x2="5.28828125" y2="4.5466" layer="94"/>
<rectangle x1="5.8674" y1="4.528565625" x2="6.606540625" y2="4.5466" layer="94"/>
<rectangle x1="7.8359" y1="4.528565625" x2="8.57758125" y2="4.5466" layer="94"/>
<rectangle x1="9.8806" y1="4.528565625" x2="10.657840625" y2="4.5466" layer="94"/>
<rectangle x1="12.176759375" y1="4.528565625" x2="12.954" y2="4.5466" layer="94"/>
<rectangle x1="14.290040625" y1="4.528565625" x2="15.0495" y2="4.5466" layer="94"/>
<rectangle x1="15.953740625" y1="4.528565625" x2="16.7132" y2="4.5466" layer="94"/>
<rectangle x1="17.78" y1="4.528565625" x2="18.52168125" y2="4.5466" layer="94"/>
<rectangle x1="22.71521875" y1="4.528565625" x2="23.72868125" y2="4.5466" layer="94"/>
<rectangle x1="0.116840625" y1="4.5466" x2="0.93218125" y2="4.5648875" layer="94"/>
<rectangle x1="4.5466" y1="4.5466" x2="5.28828125" y2="4.5648875" layer="94"/>
<rectangle x1="5.88518125" y1="4.5466" x2="6.606540625" y2="4.5648875" layer="94"/>
<rectangle x1="7.8359" y1="4.5466" x2="8.595359375" y2="4.5648875" layer="94"/>
<rectangle x1="9.89838125" y1="4.5466" x2="10.67561875" y2="4.5648875" layer="94"/>
<rectangle x1="12.194540625" y1="4.5466" x2="12.954" y2="4.5648875" layer="94"/>
<rectangle x1="14.290040625" y1="4.5466" x2="15.0495" y2="4.5648875" layer="94"/>
<rectangle x1="15.97151875" y1="4.5466" x2="16.73098125" y2="4.5648875" layer="94"/>
<rectangle x1="17.78" y1="4.5466" x2="18.539459375" y2="4.5648875" layer="94"/>
<rectangle x1="22.697440625" y1="4.5466" x2="23.7109" y2="4.5648875" layer="94"/>
<rectangle x1="0.13461875" y1="4.5648875" x2="0.93218125" y2="4.582921875" layer="94"/>
<rectangle x1="4.5466" y1="4.5648875" x2="5.28828125" y2="4.582921875" layer="94"/>
<rectangle x1="5.902959375" y1="4.5648875" x2="6.626859375" y2="4.582921875" layer="94"/>
<rectangle x1="7.85621875" y1="4.5648875" x2="8.595359375" y2="4.582921875" layer="94"/>
<rectangle x1="9.89838125" y1="4.5648875" x2="10.67561875" y2="4.582921875" layer="94"/>
<rectangle x1="12.194540625" y1="4.5648875" x2="12.97178125" y2="4.582921875" layer="94"/>
<rectangle x1="14.290040625" y1="4.5648875" x2="15.06728125" y2="4.582921875" layer="94"/>
<rectangle x1="15.97151875" y1="4.5648875" x2="16.73098125" y2="4.582921875" layer="94"/>
<rectangle x1="17.79778125" y1="4.5648875" x2="18.539459375" y2="4.582921875" layer="94"/>
<rectangle x1="22.679659375" y1="4.5648875" x2="23.69058125" y2="4.582921875" layer="94"/>
<rectangle x1="0.13461875" y1="4.582921875" x2="0.93218125" y2="4.60095625" layer="94"/>
<rectangle x1="4.5466" y1="4.582921875" x2="5.28828125" y2="4.60095625" layer="94"/>
<rectangle x1="5.902959375" y1="4.582921875" x2="6.644640625" y2="4.60095625" layer="94"/>
<rectangle x1="7.85621875" y1="4.582921875" x2="8.595359375" y2="4.60095625" layer="94"/>
<rectangle x1="9.89838125" y1="4.582921875" x2="10.6934" y2="4.60095625" layer="94"/>
<rectangle x1="12.21231875" y1="4.582921875" x2="12.989559375" y2="4.60095625" layer="94"/>
<rectangle x1="14.290040625" y1="4.582921875" x2="15.06728125" y2="4.60095625" layer="94"/>
<rectangle x1="15.97151875" y1="4.582921875" x2="16.748759375" y2="4.60095625" layer="94"/>
<rectangle x1="17.79778125" y1="4.582921875" x2="18.539459375" y2="4.60095625" layer="94"/>
<rectangle x1="22.66188125" y1="4.582921875" x2="23.69058125" y2="4.60095625" layer="94"/>
<rectangle x1="0.13461875" y1="4.60095625" x2="0.949959375" y2="4.618990625" layer="94"/>
<rectangle x1="4.5466" y1="4.60095625" x2="5.28828125" y2="4.618990625" layer="94"/>
<rectangle x1="5.920740625" y1="4.60095625" x2="6.644640625" y2="4.618990625" layer="94"/>
<rectangle x1="7.85621875" y1="4.60095625" x2="8.613140625" y2="4.618990625" layer="94"/>
<rectangle x1="9.916159375" y1="4.60095625" x2="10.71118125" y2="4.618990625" layer="94"/>
<rectangle x1="12.2301" y1="4.60095625" x2="12.989559375" y2="4.618990625" layer="94"/>
<rectangle x1="14.290040625" y1="4.60095625" x2="15.06728125" y2="4.618990625" layer="94"/>
<rectangle x1="15.9893" y1="4.60095625" x2="16.748759375" y2="4.618990625" layer="94"/>
<rectangle x1="17.79778125" y1="4.60095625" x2="18.557240625" y2="4.618990625" layer="94"/>
<rectangle x1="22.6441" y1="4.60095625" x2="23.6728" y2="4.618990625" layer="94"/>
<rectangle x1="0.13461875" y1="4.618990625" x2="0.949959375" y2="4.637025" layer="94"/>
<rectangle x1="4.5466" y1="4.618990625" x2="5.28828125" y2="4.637025" layer="94"/>
<rectangle x1="5.93851875" y1="4.618990625" x2="6.66241875" y2="4.637025" layer="94"/>
<rectangle x1="7.874" y1="4.618990625" x2="8.613140625" y2="4.637025" layer="94"/>
<rectangle x1="9.916159375" y1="4.618990625" x2="10.71118125" y2="4.637025" layer="94"/>
<rectangle x1="12.2301" y1="4.618990625" x2="13.007340625" y2="4.637025" layer="94"/>
<rectangle x1="14.290040625" y1="4.618990625" x2="15.06728125" y2="4.637025" layer="94"/>
<rectangle x1="15.9893" y1="4.618990625" x2="16.766540625" y2="4.637025" layer="94"/>
<rectangle x1="17.815559375" y1="4.618990625" x2="18.557240625" y2="4.637025" layer="94"/>
<rectangle x1="22.62378125" y1="4.618990625" x2="23.65501875" y2="4.637025" layer="94"/>
<rectangle x1="0.1524" y1="4.637025" x2="0.949959375" y2="4.655059375" layer="94"/>
<rectangle x1="4.5466" y1="4.637025" x2="5.306059375" y2="4.655059375" layer="94"/>
<rectangle x1="5.93851875" y1="4.637025" x2="6.6802" y2="4.655059375" layer="94"/>
<rectangle x1="7.874" y1="4.637025" x2="8.613140625" y2="4.655059375" layer="94"/>
<rectangle x1="9.916159375" y1="4.637025" x2="10.728959375" y2="4.655059375" layer="94"/>
<rectangle x1="12.24788125" y1="4.637025" x2="13.007340625" y2="4.655059375" layer="94"/>
<rectangle x1="14.290040625" y1="4.637025" x2="15.06728125" y2="4.655059375" layer="94"/>
<rectangle x1="15.9893" y1="4.637025" x2="16.766540625" y2="4.655059375" layer="94"/>
<rectangle x1="17.815559375" y1="4.637025" x2="18.557240625" y2="4.655059375" layer="94"/>
<rectangle x1="22.606" y1="4.637025" x2="23.65501875" y2="4.655059375" layer="94"/>
<rectangle x1="0.1524" y1="4.655059375" x2="0.967740625" y2="4.673346875" layer="94"/>
<rectangle x1="4.5466" y1="4.655059375" x2="5.306059375" y2="4.673346875" layer="94"/>
<rectangle x1="5.9563" y1="4.655059375" x2="6.6802" y2="4.673346875" layer="94"/>
<rectangle x1="7.89178125" y1="4.655059375" x2="8.633459375" y2="4.673346875" layer="94"/>
<rectangle x1="9.933940625" y1="4.655059375" x2="10.728959375" y2="4.673346875" layer="94"/>
<rectangle x1="12.24788125" y1="4.655059375" x2="13.02511875" y2="4.673346875" layer="94"/>
<rectangle x1="14.290040625" y1="4.655059375" x2="15.06728125" y2="4.673346875" layer="94"/>
<rectangle x1="16.00708125" y1="4.655059375" x2="16.786859375" y2="4.673346875" layer="94"/>
<rectangle x1="17.815559375" y1="4.655059375" x2="18.57501875" y2="4.673346875" layer="94"/>
<rectangle x1="22.58821875" y1="4.655059375" x2="23.637240625" y2="4.673346875" layer="94"/>
<rectangle x1="0.1524" y1="4.673346875" x2="0.967740625" y2="4.69138125" layer="94"/>
<rectangle x1="4.5466" y1="4.673346875" x2="5.306059375" y2="4.69138125" layer="94"/>
<rectangle x1="5.97408125" y1="4.673346875" x2="6.69798125" y2="4.69138125" layer="94"/>
<rectangle x1="7.89178125" y1="4.673346875" x2="8.633459375" y2="4.69138125" layer="94"/>
<rectangle x1="9.933940625" y1="4.673346875" x2="10.746740625" y2="4.69138125" layer="94"/>
<rectangle x1="12.265659375" y1="4.673346875" x2="13.0429" y2="4.69138125" layer="94"/>
<rectangle x1="14.290040625" y1="4.673346875" x2="15.0876" y2="4.69138125" layer="94"/>
<rectangle x1="16.00708125" y1="4.673346875" x2="16.786859375" y2="4.69138125" layer="94"/>
<rectangle x1="17.833340625" y1="4.673346875" x2="18.57501875" y2="4.69138125" layer="94"/>
<rectangle x1="22.58821875" y1="4.673346875" x2="23.619459375" y2="4.69138125" layer="94"/>
<rectangle x1="0.17271875" y1="4.69138125" x2="0.967740625" y2="4.7094125" layer="94"/>
<rectangle x1="4.5466" y1="4.69138125" x2="5.306059375" y2="4.7094125" layer="94"/>
<rectangle x1="5.97408125" y1="4.69138125" x2="6.715759375" y2="4.7094125" layer="94"/>
<rectangle x1="7.89178125" y1="4.69138125" x2="8.633459375" y2="4.7094125" layer="94"/>
<rectangle x1="9.95171875" y1="4.69138125" x2="10.746740625" y2="4.7094125" layer="94"/>
<rectangle x1="12.283440625" y1="4.69138125" x2="13.06068125" y2="4.7094125" layer="94"/>
<rectangle x1="14.30781875" y1="4.69138125" x2="15.0876" y2="4.7094125" layer="94"/>
<rectangle x1="16.0274" y1="4.69138125" x2="16.804640625" y2="4.7094125" layer="94"/>
<rectangle x1="17.833340625" y1="4.69138125" x2="18.5928" y2="4.7094125" layer="94"/>
<rectangle x1="22.570440625" y1="4.69138125" x2="23.619459375" y2="4.7094125" layer="94"/>
<rectangle x1="0.17271875" y1="4.7094125" x2="0.98551875" y2="4.727446875" layer="94"/>
<rectangle x1="4.5466" y1="4.7094125" x2="5.306059375" y2="4.727446875" layer="94"/>
<rectangle x1="5.991859375" y1="4.7094125" x2="6.733540625" y2="4.727446875" layer="94"/>
<rectangle x1="7.909559375" y1="4.7094125" x2="8.651240625" y2="4.727446875" layer="94"/>
<rectangle x1="9.95171875" y1="4.7094125" x2="10.76451875" y2="4.727446875" layer="94"/>
<rectangle x1="12.283440625" y1="4.7094125" x2="13.06068125" y2="4.727446875" layer="94"/>
<rectangle x1="14.30781875" y1="4.7094125" x2="15.0876" y2="4.727446875" layer="94"/>
<rectangle x1="16.0274" y1="4.7094125" x2="16.804640625" y2="4.727446875" layer="94"/>
<rectangle x1="17.833340625" y1="4.7094125" x2="18.5928" y2="4.727446875" layer="94"/>
<rectangle x1="22.552659375" y1="4.7094125" x2="23.60168125" y2="4.727446875" layer="94"/>
<rectangle x1="0.17271875" y1="4.727446875" x2="0.98551875" y2="4.74548125" layer="94"/>
<rectangle x1="4.5466" y1="4.727446875" x2="5.306059375" y2="4.74548125" layer="94"/>
<rectangle x1="6.01218125" y1="4.727446875" x2="6.733540625" y2="4.74548125" layer="94"/>
<rectangle x1="7.909559375" y1="4.727446875" x2="8.651240625" y2="4.74548125" layer="94"/>
<rectangle x1="9.95171875" y1="4.727446875" x2="10.784840625" y2="4.74548125" layer="94"/>
<rectangle x1="12.30121875" y1="4.727446875" x2="13.078459375" y2="4.74548125" layer="94"/>
<rectangle x1="14.290040625" y1="4.727446875" x2="15.0876" y2="4.74548125" layer="94"/>
<rectangle x1="16.0274" y1="4.727446875" x2="16.82241875" y2="4.74548125" layer="94"/>
<rectangle x1="17.833340625" y1="4.727446875" x2="18.5928" y2="4.74548125" layer="94"/>
<rectangle x1="22.53488125" y1="4.727446875" x2="23.5839" y2="4.74548125" layer="94"/>
<rectangle x1="0.1905" y1="4.74548125" x2="0.98551875" y2="4.763515625" layer="94"/>
<rectangle x1="4.5466" y1="4.74548125" x2="5.306059375" y2="4.763515625" layer="94"/>
<rectangle x1="6.01218125" y1="4.74548125" x2="6.75131875" y2="4.763515625" layer="94"/>
<rectangle x1="7.909559375" y1="4.74548125" x2="8.66901875" y2="4.763515625" layer="94"/>
<rectangle x1="9.9695" y1="4.74548125" x2="10.784840625" y2="4.763515625" layer="94"/>
<rectangle x1="12.30121875" y1="4.74548125" x2="13.09878125" y2="4.763515625" layer="94"/>
<rectangle x1="14.290040625" y1="4.74548125" x2="15.0876" y2="4.763515625" layer="94"/>
<rectangle x1="16.04518125" y1="4.74548125" x2="16.82241875" y2="4.763515625" layer="94"/>
<rectangle x1="17.85111875" y1="4.74548125" x2="18.61058125" y2="4.763515625" layer="94"/>
<rectangle x1="22.5171" y1="4.74548125" x2="23.56611875" y2="4.763515625" layer="94"/>
<rectangle x1="0.1905" y1="4.763515625" x2="1.0033" y2="4.781803125" layer="94"/>
<rectangle x1="4.5466" y1="4.763515625" x2="5.306059375" y2="4.781803125" layer="94"/>
<rectangle x1="6.029959375" y1="4.763515625" x2="6.7691" y2="4.781803125" layer="94"/>
<rectangle x1="7.927340625" y1="4.763515625" x2="8.66901875" y2="4.781803125" layer="94"/>
<rectangle x1="9.9695" y1="4.763515625" x2="10.80261875" y2="4.781803125" layer="94"/>
<rectangle x1="12.321540625" y1="4.763515625" x2="13.116559375" y2="4.781803125" layer="94"/>
<rectangle x1="14.290040625" y1="4.763515625" x2="15.0876" y2="4.781803125" layer="94"/>
<rectangle x1="16.04518125" y1="4.763515625" x2="16.8402" y2="4.781803125" layer="94"/>
<rectangle x1="17.85111875" y1="4.763515625" x2="18.61058125" y2="4.781803125" layer="94"/>
<rectangle x1="22.49931875" y1="4.763515625" x2="23.56611875" y2="4.781803125" layer="94"/>
<rectangle x1="0.20828125" y1="4.781803125" x2="1.0033" y2="4.7998375" layer="94"/>
<rectangle x1="4.5466" y1="4.781803125" x2="5.306059375" y2="4.7998375" layer="94"/>
<rectangle x1="6.047740625" y1="4.781803125" x2="6.7691" y2="4.7998375" layer="94"/>
<rectangle x1="7.927340625" y1="4.781803125" x2="8.66901875" y2="4.7998375" layer="94"/>
<rectangle x1="9.9695" y1="4.781803125" x2="10.8204" y2="4.7998375" layer="94"/>
<rectangle x1="12.33931875" y1="4.781803125" x2="13.116559375" y2="4.7998375" layer="94"/>
<rectangle x1="14.290040625" y1="4.781803125" x2="15.0876" y2="4.7998375" layer="94"/>
<rectangle x1="16.04518125" y1="4.781803125" x2="16.8402" y2="4.7998375" layer="94"/>
<rectangle x1="17.85111875" y1="4.781803125" x2="18.61058125" y2="4.7998375" layer="94"/>
<rectangle x1="22.481540625" y1="4.781803125" x2="23.5458" y2="4.7998375" layer="94"/>
<rectangle x1="0.20828125" y1="4.7998375" x2="1.02108125" y2="4.817871875" layer="94"/>
<rectangle x1="4.5466" y1="4.7998375" x2="5.306059375" y2="4.817871875" layer="94"/>
<rectangle x1="6.047740625" y1="4.7998375" x2="6.78941875" y2="4.817871875" layer="94"/>
<rectangle x1="7.927340625" y1="4.7998375" x2="8.6868" y2="4.817871875" layer="94"/>
<rectangle x1="9.98728125" y1="4.7998375" x2="10.8204" y2="4.817871875" layer="94"/>
<rectangle x1="12.33931875" y1="4.7998375" x2="13.134340625" y2="4.817871875" layer="94"/>
<rectangle x1="14.290040625" y1="4.7998375" x2="15.0876" y2="4.817871875" layer="94"/>
<rectangle x1="16.062959375" y1="4.7998375" x2="16.85798125" y2="4.817871875" layer="94"/>
<rectangle x1="17.85111875" y1="4.7998375" x2="18.6309" y2="4.817871875" layer="94"/>
<rectangle x1="22.46121875" y1="4.7998375" x2="23.52801875" y2="4.817871875" layer="94"/>
<rectangle x1="0.20828125" y1="4.817871875" x2="1.02108125" y2="4.83590625" layer="94"/>
<rectangle x1="4.5466" y1="4.817871875" x2="5.306059375" y2="4.83590625" layer="94"/>
<rectangle x1="6.06551875" y1="4.817871875" x2="6.8072" y2="4.83590625" layer="94"/>
<rectangle x1="7.94511875" y1="4.817871875" x2="8.6868" y2="4.83590625" layer="94"/>
<rectangle x1="9.98728125" y1="4.817871875" x2="10.83818125" y2="4.83590625" layer="94"/>
<rectangle x1="12.3571" y1="4.817871875" x2="13.15211875" y2="4.83590625" layer="94"/>
<rectangle x1="14.290040625" y1="4.817871875" x2="15.0876" y2="4.83590625" layer="94"/>
<rectangle x1="16.062959375" y1="4.817871875" x2="16.875759375" y2="4.83590625" layer="94"/>
<rectangle x1="17.871440625" y1="4.817871875" x2="18.6309" y2="4.83590625" layer="94"/>
<rectangle x1="22.443440625" y1="4.817871875" x2="23.510240625" y2="4.83590625" layer="94"/>
<rectangle x1="0.226059375" y1="4.83590625" x2="1.02108125" y2="4.853940625" layer="94"/>
<rectangle x1="4.5466" y1="4.83590625" x2="5.306059375" y2="4.853940625" layer="94"/>
<rectangle x1="6.0833" y1="4.83590625" x2="6.8072" y2="4.853940625" layer="94"/>
<rectangle x1="7.94511875" y1="4.83590625" x2="8.6868" y2="4.853940625" layer="94"/>
<rectangle x1="9.98728125" y1="4.83590625" x2="10.855959375" y2="4.853940625" layer="94"/>
<rectangle x1="12.37488125" y1="4.83590625" x2="13.1699" y2="4.853940625" layer="94"/>
<rectangle x1="14.290040625" y1="4.83590625" x2="15.0876" y2="4.853940625" layer="94"/>
<rectangle x1="16.062959375" y1="4.83590625" x2="16.893540625" y2="4.853940625" layer="94"/>
<rectangle x1="17.871440625" y1="4.83590625" x2="18.6309" y2="4.853940625" layer="94"/>
<rectangle x1="22.40788125" y1="4.83590625" x2="23.510240625" y2="4.853940625" layer="94"/>
<rectangle x1="0.226059375" y1="4.853940625" x2="1.038859375" y2="4.871975" layer="94"/>
<rectangle x1="4.5466" y1="4.853940625" x2="5.306059375" y2="4.871975" layer="94"/>
<rectangle x1="6.0833" y1="4.853940625" x2="6.82498125" y2="4.871975" layer="94"/>
<rectangle x1="7.9629" y1="4.853940625" x2="8.70458125" y2="4.871975" layer="94"/>
<rectangle x1="10.0076" y1="4.853940625" x2="10.873740625" y2="4.871975" layer="94"/>
<rectangle x1="12.37488125" y1="4.853940625" x2="13.18768125" y2="4.871975" layer="94"/>
<rectangle x1="14.290040625" y1="4.853940625" x2="15.0876" y2="4.871975" layer="94"/>
<rectangle x1="16.080740625" y1="4.853940625" x2="16.893540625" y2="4.871975" layer="94"/>
<rectangle x1="17.871440625" y1="4.853940625" x2="18.6309" y2="4.871975" layer="94"/>
<rectangle x1="22.3901" y1="4.853940625" x2="23.492459375" y2="4.871975" layer="94"/>
<rectangle x1="0.243840625" y1="4.871975" x2="1.038859375" y2="4.8902625" layer="94"/>
<rectangle x1="4.5466" y1="4.871975" x2="5.306059375" y2="4.8902625" layer="94"/>
<rectangle x1="6.10108125" y1="4.871975" x2="6.842759375" y2="4.8902625" layer="94"/>
<rectangle x1="7.9629" y1="4.871975" x2="8.70458125" y2="4.8902625" layer="94"/>
<rectangle x1="10.0076" y1="4.871975" x2="10.89151875" y2="4.8902625" layer="94"/>
<rectangle x1="12.392659375" y1="4.871975" x2="13.205459375" y2="4.8902625" layer="94"/>
<rectangle x1="14.272259375" y1="4.871975" x2="15.0876" y2="4.8902625" layer="94"/>
<rectangle x1="16.080740625" y1="4.871975" x2="16.91131875" y2="4.8902625" layer="94"/>
<rectangle x1="17.871440625" y1="4.871975" x2="18.64868125" y2="4.8902625" layer="94"/>
<rectangle x1="22.37231875" y1="4.871975" x2="23.47468125" y2="4.8902625" layer="94"/>
<rectangle x1="0.243840625" y1="4.8902625" x2="1.038859375" y2="4.908296875" layer="94"/>
<rectangle x1="4.5466" y1="4.8902625" x2="5.306059375" y2="4.908296875" layer="94"/>
<rectangle x1="6.118859375" y1="4.8902625" x2="6.860540625" y2="4.908296875" layer="94"/>
<rectangle x1="7.9629" y1="4.8902625" x2="8.70458125" y2="4.908296875" layer="94"/>
<rectangle x1="10.02538125" y1="4.8902625" x2="10.9093" y2="4.908296875" layer="94"/>
<rectangle x1="12.410440625" y1="4.8902625" x2="13.223240625" y2="4.908296875" layer="94"/>
<rectangle x1="14.272259375" y1="4.8902625" x2="15.0876" y2="4.908296875" layer="94"/>
<rectangle x1="16.09851875" y1="4.8902625" x2="16.9291" y2="4.908296875" layer="94"/>
<rectangle x1="17.871440625" y1="4.8902625" x2="18.64868125" y2="4.908296875" layer="94"/>
<rectangle x1="22.354540625" y1="4.8902625" x2="23.4569" y2="4.908296875" layer="94"/>
<rectangle x1="0.243840625" y1="4.908296875" x2="1.056640625" y2="4.92633125" layer="94"/>
<rectangle x1="4.56438125" y1="4.908296875" x2="5.306059375" y2="4.92633125" layer="94"/>
<rectangle x1="6.118859375" y1="4.908296875" x2="6.860540625" y2="4.92633125" layer="94"/>
<rectangle x1="7.98068125" y1="4.908296875" x2="8.722359375" y2="4.92633125" layer="94"/>
<rectangle x1="10.02538125" y1="4.908296875" x2="10.9093" y2="4.92633125" layer="94"/>
<rectangle x1="12.42821875" y1="4.908296875" x2="13.243559375" y2="4.92633125" layer="94"/>
<rectangle x1="14.272259375" y1="4.908296875" x2="15.0876" y2="4.92633125" layer="94"/>
<rectangle x1="16.09851875" y1="4.908296875" x2="16.94941875" y2="4.92633125" layer="94"/>
<rectangle x1="17.871440625" y1="4.908296875" x2="18.64868125" y2="4.92633125" layer="94"/>
<rectangle x1="22.336759375" y1="4.908296875" x2="23.43911875" y2="4.92633125" layer="94"/>
<rectangle x1="0.26161875" y1="4.92633125" x2="1.056640625" y2="4.9443625" layer="94"/>
<rectangle x1="4.56438125" y1="4.92633125" x2="5.306059375" y2="4.9443625" layer="94"/>
<rectangle x1="6.136640625" y1="4.92633125" x2="6.87831875" y2="4.9443625" layer="94"/>
<rectangle x1="7.98068125" y1="4.92633125" x2="8.722359375" y2="4.9443625" layer="94"/>
<rectangle x1="10.02538125" y1="4.92633125" x2="10.92708125" y2="4.9443625" layer="94"/>
<rectangle x1="12.42821875" y1="4.92633125" x2="13.261340625" y2="4.9443625" layer="94"/>
<rectangle x1="14.25448125" y1="4.92633125" x2="15.0876" y2="4.9443625" layer="94"/>
<rectangle x1="16.09851875" y1="4.92633125" x2="16.9672" y2="4.9443625" layer="94"/>
<rectangle x1="17.871440625" y1="4.92633125" x2="18.666459375" y2="4.9443625" layer="94"/>
<rectangle x1="22.316440625" y1="4.92633125" x2="23.43911875" y2="4.9443625" layer="94"/>
<rectangle x1="25.372059375" y1="4.92633125" x2="25.389840625" y2="4.9443625" layer="94"/>
<rectangle x1="0.26161875" y1="4.9443625" x2="1.056640625" y2="4.962396875" layer="94"/>
<rectangle x1="4.56438125" y1="4.9443625" x2="5.306059375" y2="4.962396875" layer="94"/>
<rectangle x1="6.15441875" y1="4.9443625" x2="6.8961" y2="4.962396875" layer="94"/>
<rectangle x1="7.98068125" y1="4.9443625" x2="8.740140625" y2="4.962396875" layer="94"/>
<rectangle x1="10.043159375" y1="4.9443625" x2="10.9474" y2="4.962396875" layer="94"/>
<rectangle x1="12.446" y1="4.9443625" x2="13.2969" y2="4.962396875" layer="94"/>
<rectangle x1="14.25448125" y1="4.9443625" x2="15.0876" y2="4.962396875" layer="94"/>
<rectangle x1="16.1163" y1="4.9443625" x2="16.98498125" y2="4.962396875" layer="94"/>
<rectangle x1="17.871440625" y1="4.9443625" x2="18.666459375" y2="4.962396875" layer="94"/>
<rectangle x1="22.298659375" y1="4.9443625" x2="23.421340625" y2="4.962396875" layer="94"/>
<rectangle x1="25.3365" y1="4.9443625" x2="25.35428125" y2="4.962396875" layer="94"/>
<rectangle x1="0.26161875" y1="4.962396875" x2="1.07441875" y2="4.9806875" layer="94"/>
<rectangle x1="4.56438125" y1="4.962396875" x2="5.323840625" y2="4.9806875" layer="94"/>
<rectangle x1="6.15441875" y1="4.962396875" x2="6.8961" y2="4.9806875" layer="94"/>
<rectangle x1="7.998459375" y1="4.962396875" x2="8.740140625" y2="4.9806875" layer="94"/>
<rectangle x1="10.043159375" y1="4.962396875" x2="10.96518125" y2="4.9806875" layer="94"/>
<rectangle x1="12.46378125" y1="4.962396875" x2="13.31468125" y2="4.9806875" layer="94"/>
<rectangle x1="14.2367" y1="4.962396875" x2="15.0876" y2="4.9806875" layer="94"/>
<rectangle x1="16.1163" y1="4.962396875" x2="17.002759375" y2="4.9806875" layer="94"/>
<rectangle x1="17.871440625" y1="4.962396875" x2="18.666459375" y2="4.9806875" layer="94"/>
<rectangle x1="22.28088125" y1="4.962396875" x2="23.403559375" y2="4.9806875" layer="94"/>
<rectangle x1="25.300940625" y1="4.962396875" x2="25.3365" y2="4.9806875" layer="94"/>
<rectangle x1="0.2794" y1="4.9806875" x2="1.07441875" y2="4.99871875" layer="94"/>
<rectangle x1="4.56438125" y1="4.9806875" x2="5.323840625" y2="4.99871875" layer="94"/>
<rectangle x1="6.174740625" y1="4.9806875" x2="6.91388125" y2="4.99871875" layer="94"/>
<rectangle x1="7.998459375" y1="4.9806875" x2="8.740140625" y2="4.99871875" layer="94"/>
<rectangle x1="10.043159375" y1="4.9806875" x2="11.000740625" y2="4.99871875" layer="94"/>
<rectangle x1="12.4841" y1="4.9806875" x2="13.332459375" y2="4.99871875" layer="94"/>
<rectangle x1="14.2367" y1="4.9806875" x2="15.0876" y2="4.99871875" layer="94"/>
<rectangle x1="16.1163" y1="4.9806875" x2="17.020540625" y2="4.99871875" layer="94"/>
<rectangle x1="17.871440625" y1="4.9806875" x2="18.666459375" y2="4.99871875" layer="94"/>
<rectangle x1="22.2631" y1="4.9806875" x2="23.383240625" y2="4.99871875" layer="94"/>
<rectangle x1="25.283159375" y1="4.9806875" x2="25.31871875" y2="4.99871875" layer="94"/>
<rectangle x1="0.2794" y1="4.99871875" x2="1.094740625" y2="5.016753125" layer="94"/>
<rectangle x1="4.56438125" y1="4.99871875" x2="5.323840625" y2="5.016753125" layer="94"/>
<rectangle x1="6.19251875" y1="4.99871875" x2="6.9342" y2="5.016753125" layer="94"/>
<rectangle x1="7.998459375" y1="4.99871875" x2="8.75791875" y2="5.016753125" layer="94"/>
<rectangle x1="10.060940625" y1="4.99871875" x2="11.01851875" y2="5.016753125" layer="94"/>
<rectangle x1="11.61541875" y1="4.99871875" x2="11.65098125" y2="5.016753125" layer="94"/>
<rectangle x1="12.50188125" y1="4.99871875" x2="13.350240625" y2="5.016753125" layer="94"/>
<rectangle x1="14.21891875" y1="4.99871875" x2="15.0876" y2="5.016753125" layer="94"/>
<rectangle x1="16.13408125" y1="4.99871875" x2="17.03831875" y2="5.016753125" layer="94"/>
<rectangle x1="17.871440625" y1="4.99871875" x2="18.684240625" y2="5.016753125" layer="94"/>
<rectangle x1="22.227540625" y1="4.99871875" x2="23.365459375" y2="5.016753125" layer="94"/>
<rectangle x1="25.2476" y1="4.99871875" x2="25.283159375" y2="5.016753125" layer="94"/>
<rectangle x1="0.29718125" y1="5.016753125" x2="1.094740625" y2="5.0347875" layer="94"/>
<rectangle x1="4.56438125" y1="5.016753125" x2="5.323840625" y2="5.0347875" layer="94"/>
<rectangle x1="6.19251875" y1="5.016753125" x2="6.9342" y2="5.0347875" layer="94"/>
<rectangle x1="8.01878125" y1="5.016753125" x2="8.75791875" y2="5.0347875" layer="94"/>
<rectangle x1="10.060940625" y1="5.016753125" x2="11.0363" y2="5.0347875" layer="94"/>
<rectangle x1="11.597640625" y1="5.016753125" x2="11.668759375" y2="5.0347875" layer="94"/>
<rectangle x1="12.50188125" y1="5.016753125" x2="13.3858" y2="5.0347875" layer="94"/>
<rectangle x1="14.201140625" y1="5.016753125" x2="15.0876" y2="5.0347875" layer="94"/>
<rectangle x1="16.13408125" y1="5.016753125" x2="17.0561" y2="5.0347875" layer="94"/>
<rectangle x1="17.871440625" y1="5.016753125" x2="18.684240625" y2="5.0347875" layer="94"/>
<rectangle x1="22.209759375" y1="5.016753125" x2="23.34768125" y2="5.0347875" layer="94"/>
<rectangle x1="25.2095" y1="5.016753125" x2="25.26538125" y2="5.0347875" layer="94"/>
<rectangle x1="0.29718125" y1="5.0347875" x2="1.094740625" y2="5.052821875" layer="94"/>
<rectangle x1="4.56438125" y1="5.0347875" x2="5.323840625" y2="5.052821875" layer="94"/>
<rectangle x1="6.2103" y1="5.0347875" x2="6.95198125" y2="5.052821875" layer="94"/>
<rectangle x1="8.01878125" y1="5.0347875" x2="8.75791875" y2="5.052821875" layer="94"/>
<rectangle x1="10.060940625" y1="5.0347875" x2="11.071859375" y2="5.052821875" layer="94"/>
<rectangle x1="11.56208125" y1="5.0347875" x2="11.686540625" y2="5.052821875" layer="94"/>
<rectangle x1="12.519659375" y1="5.0347875" x2="13.40611875" y2="5.052821875" layer="94"/>
<rectangle x1="14.183359375" y1="5.0347875" x2="15.0876" y2="5.052821875" layer="94"/>
<rectangle x1="16.13408125" y1="5.0347875" x2="17.07388125" y2="5.052821875" layer="94"/>
<rectangle x1="17.85111875" y1="5.0347875" x2="18.684240625" y2="5.052821875" layer="94"/>
<rectangle x1="22.19198125" y1="5.0347875" x2="23.3299" y2="5.052821875" layer="94"/>
<rectangle x1="25.173940625" y1="5.0347875" x2="25.2476" y2="5.052821875" layer="94"/>
<rectangle x1="0.3175" y1="5.052821875" x2="1.11251875" y2="5.07085625" layer="94"/>
<rectangle x1="4.56438125" y1="5.052821875" x2="5.323840625" y2="5.07085625" layer="94"/>
<rectangle x1="6.22808125" y1="5.052821875" x2="6.969759375" y2="5.07085625" layer="94"/>
<rectangle x1="8.036559375" y1="5.052821875" x2="8.778240625" y2="5.07085625" layer="94"/>
<rectangle x1="10.07871875" y1="5.052821875" x2="11.109959375" y2="5.07085625" layer="94"/>
<rectangle x1="11.541759375" y1="5.052821875" x2="11.686540625" y2="5.07085625" layer="94"/>
<rectangle x1="12.537440625" y1="5.052821875" x2="13.44168125" y2="5.07085625" layer="94"/>
<rectangle x1="14.183359375" y1="5.052821875" x2="15.0876" y2="5.07085625" layer="94"/>
<rectangle x1="16.151859375" y1="5.052821875" x2="17.11198125" y2="5.07085625" layer="94"/>
<rectangle x1="17.85111875" y1="5.052821875" x2="18.70201875" y2="5.07085625" layer="94"/>
<rectangle x1="22.1742" y1="5.052821875" x2="23.31211875" y2="5.07085625" layer="94"/>
<rectangle x1="25.156159375" y1="5.052821875" x2="25.2095" y2="5.07085625" layer="94"/>
<rectangle x1="0.3175" y1="5.07085625" x2="1.11251875" y2="5.08914375" layer="94"/>
<rectangle x1="4.56438125" y1="5.07085625" x2="5.323840625" y2="5.08914375" layer="94"/>
<rectangle x1="6.22808125" y1="5.07085625" x2="6.987540625" y2="5.08914375" layer="94"/>
<rectangle x1="8.036559375" y1="5.07085625" x2="8.778240625" y2="5.08914375" layer="94"/>
<rectangle x1="10.07871875" y1="5.07085625" x2="11.14551875" y2="5.08914375" layer="94"/>
<rectangle x1="11.5062" y1="5.07085625" x2="11.706859375" y2="5.08914375" layer="94"/>
<rectangle x1="12.55521875" y1="5.07085625" x2="13.477240625" y2="5.08914375" layer="94"/>
<rectangle x1="14.16558125" y1="5.07085625" x2="15.06728125" y2="5.08914375" layer="94"/>
<rectangle x1="16.151859375" y1="5.07085625" x2="17.129759375" y2="5.08914375" layer="94"/>
<rectangle x1="17.833340625" y1="5.07085625" x2="18.70201875" y2="5.08914375" layer="94"/>
<rectangle x1="22.1361" y1="5.07085625" x2="23.31211875" y2="5.08914375" layer="94"/>
<rectangle x1="25.1206" y1="5.07085625" x2="25.19171875" y2="5.08914375" layer="94"/>
<rectangle x1="0.3175" y1="5.08914375" x2="1.1303" y2="5.107178125" layer="94"/>
<rectangle x1="4.56438125" y1="5.08914375" x2="5.323840625" y2="5.107178125" layer="94"/>
<rectangle x1="6.245859375" y1="5.08914375" x2="6.987540625" y2="5.107178125" layer="94"/>
<rectangle x1="8.036559375" y1="5.08914375" x2="8.778240625" y2="5.107178125" layer="94"/>
<rectangle x1="10.0965" y1="5.08914375" x2="11.198859375" y2="5.107178125" layer="94"/>
<rectangle x1="11.452859375" y1="5.08914375" x2="11.724640625" y2="5.107178125" layer="94"/>
<rectangle x1="12.573" y1="5.08914375" x2="13.5128" y2="5.107178125" layer="94"/>
<rectangle x1="14.12748125" y1="5.08914375" x2="15.06728125" y2="5.107178125" layer="94"/>
<rectangle x1="16.17218125" y1="5.08914375" x2="17.16531875" y2="5.107178125" layer="94"/>
<rectangle x1="17.833340625" y1="5.08914375" x2="18.70201875" y2="5.107178125" layer="94"/>
<rectangle x1="22.11831875" y1="5.08914375" x2="23.294340625" y2="5.107178125" layer="94"/>
<rectangle x1="25.0825" y1="5.08914375" x2="25.173940625" y2="5.107178125" layer="94"/>
<rectangle x1="0.33528125" y1="5.107178125" x2="1.1303" y2="5.1252125" layer="94"/>
<rectangle x1="4.56438125" y1="5.107178125" x2="5.323840625" y2="5.1252125" layer="94"/>
<rectangle x1="6.263640625" y1="5.107178125" x2="7.00531875" y2="5.1252125" layer="94"/>
<rectangle x1="8.054340625" y1="5.107178125" x2="8.79601875" y2="5.1252125" layer="94"/>
<rectangle x1="10.0965" y1="5.107178125" x2="11.325859375" y2="5.1252125" layer="94"/>
<rectangle x1="11.343640625" y1="5.107178125" x2="11.724640625" y2="5.1252125" layer="94"/>
<rectangle x1="12.59078125" y1="5.107178125" x2="13.5509" y2="5.1252125" layer="94"/>
<rectangle x1="14.1097" y1="5.107178125" x2="15.06728125" y2="5.1252125" layer="94"/>
<rectangle x1="16.17218125" y1="5.107178125" x2="17.20088125" y2="5.1252125" layer="94"/>
<rectangle x1="17.815559375" y1="5.107178125" x2="18.70201875" y2="5.1252125" layer="94"/>
<rectangle x1="22.100540625" y1="5.107178125" x2="23.25878125" y2="5.1252125" layer="94"/>
<rectangle x1="25.046940625" y1="5.107178125" x2="25.13838125" y2="5.1252125" layer="94"/>
<rectangle x1="0.33528125" y1="5.1252125" x2="1.14808125" y2="5.143246875" layer="94"/>
<rectangle x1="4.56438125" y1="5.1252125" x2="5.323840625" y2="5.143246875" layer="94"/>
<rectangle x1="6.263640625" y1="5.1252125" x2="7.0231" y2="5.143246875" layer="94"/>
<rectangle x1="7.711440625" y1="5.1252125" x2="9.30148125" y2="5.143246875" layer="94"/>
<rectangle x1="10.0965" y1="5.1252125" x2="11.74241875" y2="5.143246875" layer="94"/>
<rectangle x1="12.608559375" y1="5.1252125" x2="13.586459375" y2="5.143246875" layer="94"/>
<rectangle x1="14.074140625" y1="5.1252125" x2="15.06728125" y2="5.143246875" layer="94"/>
<rectangle x1="16.17218125" y1="5.1252125" x2="17.218659375" y2="5.143246875" layer="94"/>
<rectangle x1="17.79778125" y1="5.1252125" x2="18.70201875" y2="5.143246875" layer="94"/>
<rectangle x1="22.06498125" y1="5.1252125" x2="23.25878125" y2="5.143246875" layer="94"/>
<rectangle x1="25.029159375" y1="5.1252125" x2="25.1206" y2="5.143246875" layer="94"/>
<rectangle x1="0.353059375" y1="5.143246875" x2="1.14808125" y2="5.16128125" layer="94"/>
<rectangle x1="4.56438125" y1="5.143246875" x2="5.323840625" y2="5.16128125" layer="94"/>
<rectangle x1="6.28141875" y1="5.143246875" x2="7.0231" y2="5.16128125" layer="94"/>
<rectangle x1="7.711440625" y1="5.143246875" x2="9.319259375" y2="5.16128125" layer="94"/>
<rectangle x1="10.11428125" y1="5.143246875" x2="11.7602" y2="5.16128125" layer="94"/>
<rectangle x1="12.608559375" y1="5.143246875" x2="13.6398" y2="5.16128125" layer="94"/>
<rectangle x1="14.03858125" y1="5.143246875" x2="15.06728125" y2="5.16128125" layer="94"/>
<rectangle x1="16.189959375" y1="5.143246875" x2="17.256759375" y2="5.16128125" layer="94"/>
<rectangle x1="17.78" y1="5.143246875" x2="18.70201875" y2="5.16128125" layer="94"/>
<rectangle x1="22.0472" y1="5.143246875" x2="23.238459375" y2="5.16128125" layer="94"/>
<rectangle x1="24.9936" y1="5.143246875" x2="25.0825" y2="5.16128125" layer="94"/>
<rectangle x1="0.353059375" y1="5.16128125" x2="1.165859375" y2="5.1793125" layer="94"/>
<rectangle x1="4.56438125" y1="5.16128125" x2="5.323840625" y2="5.1793125" layer="94"/>
<rectangle x1="6.2992" y1="5.16128125" x2="7.04088125" y2="5.1793125" layer="94"/>
<rectangle x1="7.72921875" y1="5.16128125" x2="9.319259375" y2="5.1793125" layer="94"/>
<rectangle x1="10.11428125" y1="5.16128125" x2="11.77798125" y2="5.1793125" layer="94"/>
<rectangle x1="12.62888125" y1="5.16128125" x2="13.713459375" y2="5.1793125" layer="94"/>
<rectangle x1="13.9827" y1="5.16128125" x2="15.0495" y2="5.1793125" layer="94"/>
<rectangle x1="16.189959375" y1="5.16128125" x2="17.3101" y2="5.1793125" layer="94"/>
<rectangle x1="17.76221875" y1="5.16128125" x2="18.7198" y2="5.1793125" layer="94"/>
<rectangle x1="22.02941875" y1="5.16128125" x2="23.22068125" y2="5.1793125" layer="94"/>
<rectangle x1="24.958040625" y1="5.16128125" x2="25.06471875" y2="5.1793125" layer="94"/>
<rectangle x1="0.370840625" y1="5.1793125" x2="1.165859375" y2="5.197603125" layer="94"/>
<rectangle x1="4.56438125" y1="5.1793125" x2="5.323840625" y2="5.197603125" layer="94"/>
<rectangle x1="6.2992" y1="5.1793125" x2="7.058659375" y2="5.197603125" layer="94"/>
<rectangle x1="7.72921875" y1="5.1793125" x2="9.337040625" y2="5.197603125" layer="94"/>
<rectangle x1="10.11428125" y1="5.1793125" x2="11.77798125" y2="5.197603125" layer="94"/>
<rectangle x1="12.646659375" y1="5.1793125" x2="15.0495" y2="5.197603125" layer="94"/>
<rectangle x1="16.189959375" y1="5.1793125" x2="17.363440625" y2="5.197603125" layer="94"/>
<rectangle x1="17.726659375" y1="5.1793125" x2="18.7198" y2="5.197603125" layer="94"/>
<rectangle x1="21.99131875" y1="5.1793125" x2="23.18511875" y2="5.197603125" layer="94"/>
<rectangle x1="24.919940625" y1="5.1793125" x2="25.029159375" y2="5.197603125" layer="94"/>
<rectangle x1="0.370840625" y1="5.197603125" x2="1.165859375" y2="5.2156375" layer="94"/>
<rectangle x1="4.56438125" y1="5.197603125" x2="5.323840625" y2="5.2156375" layer="94"/>
<rectangle x1="6.31951875" y1="5.197603125" x2="7.058659375" y2="5.2156375" layer="94"/>
<rectangle x1="7.72921875" y1="5.197603125" x2="9.337040625" y2="5.2156375" layer="94"/>
<rectangle x1="10.132059375" y1="5.197603125" x2="11.795759375" y2="5.2156375" layer="94"/>
<rectangle x1="12.664440625" y1="5.197603125" x2="15.0495" y2="5.2156375" layer="94"/>
<rectangle x1="16.207740625" y1="5.197603125" x2="17.41931875" y2="5.2156375" layer="94"/>
<rectangle x1="17.688559375" y1="5.197603125" x2="18.7198" y2="5.2156375" layer="94"/>
<rectangle x1="21.973540625" y1="5.197603125" x2="23.167340625" y2="5.2156375" layer="94"/>
<rectangle x1="24.88438125" y1="5.197603125" x2="25.01138125" y2="5.2156375" layer="94"/>
<rectangle x1="0.38861875" y1="5.2156375" x2="1.183640625" y2="5.23366875" layer="94"/>
<rectangle x1="4.56438125" y1="5.2156375" x2="5.323840625" y2="5.23366875" layer="94"/>
<rectangle x1="6.3373" y1="5.2156375" x2="7.076440625" y2="5.23366875" layer="94"/>
<rectangle x1="7.747" y1="5.2156375" x2="9.337040625" y2="5.23366875" layer="94"/>
<rectangle x1="10.132059375" y1="5.2156375" x2="11.813540625" y2="5.23366875" layer="94"/>
<rectangle x1="12.68221875" y1="5.2156375" x2="15.0495" y2="5.23366875" layer="94"/>
<rectangle x1="16.207740625" y1="5.2156375" x2="18.7198" y2="5.23366875" layer="94"/>
<rectangle x1="21.955759375" y1="5.2156375" x2="23.167340625" y2="5.23366875" layer="94"/>
<rectangle x1="24.8666" y1="5.2156375" x2="24.9936" y2="5.23366875" layer="94"/>
<rectangle x1="0.38861875" y1="5.23366875" x2="1.183640625" y2="5.251703125" layer="94"/>
<rectangle x1="4.56438125" y1="5.23366875" x2="5.323840625" y2="5.251703125" layer="94"/>
<rectangle x1="6.3373" y1="5.23366875" x2="7.096759375" y2="5.251703125" layer="94"/>
<rectangle x1="7.747" y1="5.23366875" x2="9.35481875" y2="5.251703125" layer="94"/>
<rectangle x1="10.132059375" y1="5.23366875" x2="11.813540625" y2="5.251703125" layer="94"/>
<rectangle x1="12.7" y1="5.23366875" x2="15.03171875" y2="5.251703125" layer="94"/>
<rectangle x1="16.207740625" y1="5.23366875" x2="18.7198" y2="5.251703125" layer="94"/>
<rectangle x1="21.93798125" y1="5.23366875" x2="23.13178125" y2="5.251703125" layer="94"/>
<rectangle x1="24.831040625" y1="5.23366875" x2="24.958040625" y2="5.251703125" layer="94"/>
<rectangle x1="0.4064" y1="5.251703125" x2="1.20141875" y2="5.2697375" layer="94"/>
<rectangle x1="4.56438125" y1="5.251703125" x2="5.323840625" y2="5.2697375" layer="94"/>
<rectangle x1="6.35508125" y1="5.251703125" x2="7.114540625" y2="5.2697375" layer="94"/>
<rectangle x1="7.76478125" y1="5.251703125" x2="9.35481875" y2="5.2697375" layer="94"/>
<rectangle x1="10.149840625" y1="5.251703125" x2="11.83131875" y2="5.2697375" layer="94"/>
<rectangle x1="12.71778125" y1="5.251703125" x2="15.03171875" y2="5.2697375" layer="94"/>
<rectangle x1="16.22551875" y1="5.251703125" x2="18.7198" y2="5.2697375" layer="94"/>
<rectangle x1="21.90241875" y1="5.251703125" x2="23.114" y2="5.2697375" layer="94"/>
<rectangle x1="24.79548125" y1="5.251703125" x2="24.940259375" y2="5.2697375" layer="94"/>
<rectangle x1="0.4064" y1="5.2697375" x2="1.20141875" y2="5.288025" layer="94"/>
<rectangle x1="4.56438125" y1="5.2697375" x2="5.323840625" y2="5.288025" layer="94"/>
<rectangle x1="6.372859375" y1="5.2697375" x2="7.114540625" y2="5.288025" layer="94"/>
<rectangle x1="7.76478125" y1="5.2697375" x2="9.35481875" y2="5.288025" layer="94"/>
<rectangle x1="10.149840625" y1="5.2697375" x2="11.8491" y2="5.288025" layer="94"/>
<rectangle x1="12.735559375" y1="5.2697375" x2="15.03171875" y2="5.288025" layer="94"/>
<rectangle x1="16.22551875" y1="5.2697375" x2="18.7198" y2="5.288025" layer="94"/>
<rectangle x1="21.884640625" y1="5.2697375" x2="23.114" y2="5.288025" layer="94"/>
<rectangle x1="24.75738125" y1="5.2697375" x2="24.902159375" y2="5.288025" layer="94"/>
<rectangle x1="0.42418125" y1="5.288025" x2="1.2192" y2="5.306059375" layer="94"/>
<rectangle x1="4.56438125" y1="5.288025" x2="5.323840625" y2="5.306059375" layer="94"/>
<rectangle x1="6.372859375" y1="5.288025" x2="7.13231875" y2="5.306059375" layer="94"/>
<rectangle x1="7.76478125" y1="5.288025" x2="9.3726" y2="5.306059375" layer="94"/>
<rectangle x1="10.170159375" y1="5.288025" x2="11.8491" y2="5.306059375" layer="94"/>
<rectangle x1="12.77111875" y1="5.288025" x2="15.013940625" y2="5.306059375" layer="94"/>
<rectangle x1="16.2433" y1="5.288025" x2="18.7198" y2="5.306059375" layer="94"/>
<rectangle x1="21.846540625" y1="5.288025" x2="23.0759" y2="5.306059375" layer="94"/>
<rectangle x1="24.72181875" y1="5.288025" x2="24.88438125" y2="5.306059375" layer="94"/>
<rectangle x1="0.42418125" y1="5.306059375" x2="1.2192" y2="5.32409375" layer="94"/>
<rectangle x1="4.56438125" y1="5.306059375" x2="5.34161875" y2="5.32409375" layer="94"/>
<rectangle x1="6.390640625" y1="5.306059375" x2="7.1501" y2="5.32409375" layer="94"/>
<rectangle x1="7.782559375" y1="5.306059375" x2="9.3726" y2="5.32409375" layer="94"/>
<rectangle x1="10.170159375" y1="5.306059375" x2="11.86941875" y2="5.32409375" layer="94"/>
<rectangle x1="12.791440625" y1="5.306059375" x2="15.013940625" y2="5.32409375" layer="94"/>
<rectangle x1="16.2433" y1="5.306059375" x2="18.7198" y2="5.32409375" layer="94"/>
<rectangle x1="21.828759375" y1="5.306059375" x2="23.05811875" y2="5.32409375" layer="94"/>
<rectangle x1="24.686259375" y1="5.306059375" x2="24.84881875" y2="5.32409375" layer="94"/>
<rectangle x1="0.441959375" y1="5.32409375" x2="1.23951875" y2="5.342128125" layer="94"/>
<rectangle x1="4.56438125" y1="5.32409375" x2="5.34161875" y2="5.342128125" layer="94"/>
<rectangle x1="6.40841875" y1="5.32409375" x2="7.1501" y2="5.342128125" layer="94"/>
<rectangle x1="7.782559375" y1="5.32409375" x2="9.3726" y2="5.342128125" layer="94"/>
<rectangle x1="10.170159375" y1="5.32409375" x2="11.8872" y2="5.342128125" layer="94"/>
<rectangle x1="12.80921875" y1="5.32409375" x2="14.996159375" y2="5.342128125" layer="94"/>
<rectangle x1="16.2433" y1="5.32409375" x2="18.7198" y2="5.342128125" layer="94"/>
<rectangle x1="21.7932" y1="5.32409375" x2="23.040340625" y2="5.342128125" layer="94"/>
<rectangle x1="24.6507" y1="5.32409375" x2="24.813259375" y2="5.342128125" layer="94"/>
<rectangle x1="0.441959375" y1="5.342128125" x2="1.23951875" y2="5.3601625" layer="94"/>
<rectangle x1="4.56438125" y1="5.342128125" x2="5.34161875" y2="5.3601625" layer="94"/>
<rectangle x1="6.40841875" y1="5.342128125" x2="7.16788125" y2="5.3601625" layer="94"/>
<rectangle x1="7.782559375" y1="5.342128125" x2="9.39291875" y2="5.3601625" layer="94"/>
<rectangle x1="10.187940625" y1="5.342128125" x2="10.873740625" y2="5.3601625" layer="94"/>
<rectangle x1="10.89151875" y1="5.342128125" x2="11.8872" y2="5.3601625" layer="94"/>
<rectangle x1="12.827" y1="5.342128125" x2="14.996159375" y2="5.3601625" layer="94"/>
<rectangle x1="16.26108125" y1="5.342128125" x2="16.94941875" y2="5.3601625" layer="94"/>
<rectangle x1="16.9672" y1="5.342128125" x2="18.7198" y2="5.3601625" layer="94"/>
<rectangle x1="21.77541875" y1="5.342128125" x2="23.022559375" y2="5.3601625" layer="94"/>
<rectangle x1="24.6126" y1="5.342128125" x2="24.79548125" y2="5.3601625" layer="94"/>
<rectangle x1="0.459740625" y1="5.3601625" x2="1.2573" y2="5.378196875" layer="94"/>
<rectangle x1="4.56438125" y1="5.3601625" x2="5.34161875" y2="5.378196875" layer="94"/>
<rectangle x1="6.4262" y1="5.3601625" x2="7.185659375" y2="5.378196875" layer="94"/>
<rectangle x1="7.800340625" y1="5.3601625" x2="9.39291875" y2="5.378196875" layer="94"/>
<rectangle x1="10.187940625" y1="5.3601625" x2="10.873740625" y2="5.378196875" layer="94"/>
<rectangle x1="10.9093" y1="5.3601625" x2="11.90498125" y2="5.378196875" layer="94"/>
<rectangle x1="12.84478125" y1="5.3601625" x2="14.97838125" y2="5.378196875" layer="94"/>
<rectangle x1="16.26108125" y1="5.3601625" x2="16.94941875" y2="5.378196875" layer="94"/>
<rectangle x1="16.98498125" y1="5.3601625" x2="18.7198" y2="5.378196875" layer="94"/>
<rectangle x1="21.739859375" y1="5.3601625" x2="23.00478125" y2="5.378196875" layer="94"/>
<rectangle x1="24.577040625" y1="5.3601625" x2="24.75738125" y2="5.378196875" layer="94"/>
<rectangle x1="0.459740625" y1="5.378196875" x2="1.27508125" y2="5.396484375" layer="94"/>
<rectangle x1="4.56438125" y1="5.378196875" x2="5.34161875" y2="5.396484375" layer="94"/>
<rectangle x1="6.44398125" y1="5.378196875" x2="7.185659375" y2="5.396484375" layer="94"/>
<rectangle x1="7.800340625" y1="5.378196875" x2="9.4107" y2="5.396484375" layer="94"/>
<rectangle x1="10.187940625" y1="5.378196875" x2="10.89151875" y2="5.396484375" layer="94"/>
<rectangle x1="10.92708125" y1="5.378196875" x2="11.922759375" y2="5.396484375" layer="94"/>
<rectangle x1="12.862559375" y1="5.378196875" x2="14.97838125" y2="5.396484375" layer="94"/>
<rectangle x1="16.26108125" y1="5.378196875" x2="16.94941875" y2="5.396484375" layer="94"/>
<rectangle x1="17.020540625" y1="5.378196875" x2="18.7198" y2="5.396484375" layer="94"/>
<rectangle x1="21.72208125" y1="5.378196875" x2="22.987" y2="5.396484375" layer="94"/>
<rectangle x1="24.54148125" y1="5.378196875" x2="24.7396" y2="5.396484375" layer="94"/>
<rectangle x1="0.480059375" y1="5.396484375" x2="1.27508125" y2="5.41451875" layer="94"/>
<rectangle x1="4.56438125" y1="5.396484375" x2="5.34161875" y2="5.41451875" layer="94"/>
<rectangle x1="6.44398125" y1="5.396484375" x2="7.203440625" y2="5.41451875" layer="94"/>
<rectangle x1="7.800340625" y1="5.396484375" x2="9.4107" y2="5.41451875" layer="94"/>
<rectangle x1="10.20571875" y1="5.396484375" x2="10.89151875" y2="5.41451875" layer="94"/>
<rectangle x1="10.92708125" y1="5.396484375" x2="11.940540625" y2="5.41451875" layer="94"/>
<rectangle x1="12.89811875" y1="5.396484375" x2="14.9606" y2="5.41451875" layer="94"/>
<rectangle x1="16.278859375" y1="5.396484375" x2="16.9672" y2="5.41451875" layer="94"/>
<rectangle x1="17.03831875" y1="5.396484375" x2="18.7198" y2="5.41451875" layer="94"/>
<rectangle x1="21.68398125" y1="5.396484375" x2="22.96921875" y2="5.41451875" layer="94"/>
<rectangle x1="24.50591875" y1="5.396484375" x2="24.704040625" y2="5.41451875" layer="94"/>
<rectangle x1="0.480059375" y1="5.41451875" x2="1.292859375" y2="5.432553125" layer="94"/>
<rectangle x1="4.56438125" y1="5.41451875" x2="5.34161875" y2="5.432553125" layer="94"/>
<rectangle x1="6.461759375" y1="5.41451875" x2="7.22121875" y2="5.432553125" layer="94"/>
<rectangle x1="7.81811875" y1="5.41451875" x2="9.4107" y2="5.432553125" layer="94"/>
<rectangle x1="10.20571875" y1="5.41451875" x2="10.89151875" y2="5.432553125" layer="94"/>
<rectangle x1="10.9474" y1="5.41451875" x2="11.940540625" y2="5.432553125" layer="94"/>
<rectangle x1="12.9159" y1="5.41451875" x2="14.9606" y2="5.432553125" layer="94"/>
<rectangle x1="16.278859375" y1="5.41451875" x2="16.9672" y2="5.432553125" layer="94"/>
<rectangle x1="17.0561" y1="5.41451875" x2="18.70201875" y2="5.432553125" layer="94"/>
<rectangle x1="21.6662" y1="5.41451875" x2="22.951440625" y2="5.432553125" layer="94"/>
<rectangle x1="24.46781875" y1="5.41451875" x2="24.66848125" y2="5.432553125" layer="94"/>
<rectangle x1="0.497840625" y1="5.432553125" x2="1.292859375" y2="5.4505875" layer="94"/>
<rectangle x1="4.56438125" y1="5.432553125" x2="5.34161875" y2="5.4505875" layer="94"/>
<rectangle x1="6.48208125" y1="5.432553125" x2="7.241540625" y2="5.4505875" layer="94"/>
<rectangle x1="7.81811875" y1="5.432553125" x2="9.42848125" y2="5.4505875" layer="94"/>
<rectangle x1="10.2235" y1="5.432553125" x2="10.9093" y2="5.4505875" layer="94"/>
<rectangle x1="10.96518125" y1="5.432553125" x2="11.95831875" y2="5.4505875" layer="94"/>
<rectangle x1="12.93621875" y1="5.432553125" x2="14.94281875" y2="5.4505875" layer="94"/>
<rectangle x1="16.296640625" y1="5.432553125" x2="16.98498125" y2="5.4505875" layer="94"/>
<rectangle x1="17.07388125" y1="5.432553125" x2="18.70201875" y2="5.4505875" layer="94"/>
<rectangle x1="21.630640625" y1="5.432553125" x2="22.93111875" y2="5.4505875" layer="94"/>
<rectangle x1="24.432259375" y1="5.432553125" x2="24.6507" y2="5.4505875" layer="94"/>
<rectangle x1="0.497840625" y1="5.4505875" x2="1.310640625" y2="5.46861875" layer="94"/>
<rectangle x1="4.56438125" y1="5.4505875" x2="5.34161875" y2="5.46861875" layer="94"/>
<rectangle x1="6.48208125" y1="5.4505875" x2="7.241540625" y2="5.46861875" layer="94"/>
<rectangle x1="7.8359" y1="5.4505875" x2="9.42848125" y2="5.46861875" layer="94"/>
<rectangle x1="10.2235" y1="5.4505875" x2="10.9093" y2="5.46861875" layer="94"/>
<rectangle x1="10.982959375" y1="5.4505875" x2="11.9761" y2="5.46861875" layer="94"/>
<rectangle x1="12.97178125" y1="5.4505875" x2="14.9225" y2="5.46861875" layer="94"/>
<rectangle x1="16.296640625" y1="5.4505875" x2="16.98498125" y2="5.46861875" layer="94"/>
<rectangle x1="17.0942" y1="5.4505875" x2="18.70201875" y2="5.46861875" layer="94"/>
<rectangle x1="21.612859375" y1="5.4505875" x2="22.913340625" y2="5.46861875" layer="94"/>
<rectangle x1="24.3967" y1="5.4505875" x2="24.6126" y2="5.46861875" layer="94"/>
<rectangle x1="0.51561875" y1="5.46861875" x2="1.310640625" y2="5.486653125" layer="94"/>
<rectangle x1="4.56438125" y1="5.46861875" x2="5.34161875" y2="5.486653125" layer="94"/>
<rectangle x1="6.499859375" y1="5.46861875" x2="7.25931875" y2="5.486653125" layer="94"/>
<rectangle x1="7.8359" y1="5.46861875" x2="9.42848125" y2="5.486653125" layer="94"/>
<rectangle x1="10.2235" y1="5.46861875" x2="10.9093" y2="5.486653125" layer="94"/>
<rectangle x1="11.000740625" y1="5.46861875" x2="11.9761" y2="5.486653125" layer="94"/>
<rectangle x1="12.989559375" y1="5.46861875" x2="14.9225" y2="5.486653125" layer="94"/>
<rectangle x1="16.296640625" y1="5.46861875" x2="16.98498125" y2="5.486653125" layer="94"/>
<rectangle x1="17.129759375" y1="5.46861875" x2="18.70201875" y2="5.486653125" layer="94"/>
<rectangle x1="21.5773" y1="5.46861875" x2="22.87778125" y2="5.486653125" layer="94"/>
<rectangle x1="24.361140625" y1="5.46861875" x2="24.59481875" y2="5.486653125" layer="94"/>
<rectangle x1="0.51561875" y1="5.486653125" x2="1.32841875" y2="5.504940625" layer="94"/>
<rectangle x1="4.56438125" y1="5.486653125" x2="5.34161875" y2="5.504940625" layer="94"/>
<rectangle x1="6.517640625" y1="5.486653125" x2="7.2771" y2="5.504940625" layer="94"/>
<rectangle x1="7.8359" y1="5.486653125" x2="9.446259375" y2="5.504940625" layer="94"/>
<rectangle x1="10.24128125" y1="5.486653125" x2="10.92708125" y2="5.504940625" layer="94"/>
<rectangle x1="11.01851875" y1="5.486653125" x2="11.99388125" y2="5.504940625" layer="94"/>
<rectangle x1="13.02511875" y1="5.486653125" x2="14.90471875" y2="5.504940625" layer="94"/>
<rectangle x1="16.31441875" y1="5.486653125" x2="17.002759375" y2="5.504940625" layer="94"/>
<rectangle x1="17.147540625" y1="5.486653125" x2="18.684240625" y2="5.504940625" layer="94"/>
<rectangle x1="21.5392" y1="5.486653125" x2="22.86" y2="5.504940625" layer="94"/>
<rectangle x1="24.305259375" y1="5.486653125" x2="24.559259375" y2="5.504940625" layer="94"/>
<rectangle x1="0.5334" y1="5.504940625" x2="1.3462" y2="5.522975" layer="94"/>
<rectangle x1="4.56438125" y1="5.504940625" x2="5.34161875" y2="5.522975" layer="94"/>
<rectangle x1="6.517640625" y1="5.504940625" x2="7.2771" y2="5.522975" layer="94"/>
<rectangle x1="7.85621875" y1="5.504940625" x2="9.446259375" y2="5.522975" layer="94"/>
<rectangle x1="10.24128125" y1="5.504940625" x2="10.92708125" y2="5.522975" layer="94"/>
<rectangle x1="11.05408125" y1="5.504940625" x2="12.0142" y2="5.522975" layer="94"/>
<rectangle x1="13.0429" y1="5.504940625" x2="14.886940625" y2="5.522975" layer="94"/>
<rectangle x1="16.31441875" y1="5.504940625" x2="17.002759375" y2="5.522975" layer="94"/>
<rectangle x1="17.16531875" y1="5.504940625" x2="18.684240625" y2="5.522975" layer="94"/>
<rectangle x1="21.52141875" y1="5.504940625" x2="22.84221875" y2="5.522975" layer="94"/>
<rectangle x1="24.2697" y1="5.504940625" x2="24.5237" y2="5.522975" layer="94"/>
<rectangle x1="0.55118125" y1="5.522975" x2="1.3462" y2="5.541009375" layer="94"/>
<rectangle x1="4.56438125" y1="5.522975" x2="5.34161875" y2="5.541009375" layer="94"/>
<rectangle x1="6.53541875" y1="5.522975" x2="7.29488125" y2="5.541009375" layer="94"/>
<rectangle x1="7.85621875" y1="5.522975" x2="9.446259375" y2="5.541009375" layer="94"/>
<rectangle x1="10.24128125" y1="5.522975" x2="10.92708125" y2="5.541009375" layer="94"/>
<rectangle x1="11.071859375" y1="5.522975" x2="12.0142" y2="5.541009375" layer="94"/>
<rectangle x1="13.078459375" y1="5.522975" x2="14.886940625" y2="5.541009375" layer="94"/>
<rectangle x1="16.31441875" y1="5.522975" x2="17.002759375" y2="5.541009375" layer="94"/>
<rectangle x1="17.20088125" y1="5.522975" x2="18.666459375" y2="5.541009375" layer="94"/>
<rectangle x1="21.485859375" y1="5.522975" x2="22.824440625" y2="5.541009375" layer="94"/>
<rectangle x1="24.234140625" y1="5.522975" x2="24.50591875" y2="5.541009375" layer="94"/>
<rectangle x1="0.55118125" y1="5.541009375" x2="1.36398125" y2="5.55904375" layer="94"/>
<rectangle x1="4.56438125" y1="5.541009375" x2="5.34161875" y2="5.55904375" layer="94"/>
<rectangle x1="6.5532" y1="5.541009375" x2="7.312659375" y2="5.55904375" layer="94"/>
<rectangle x1="7.85621875" y1="5.541009375" x2="9.464040625" y2="5.55904375" layer="94"/>
<rectangle x1="10.259059375" y1="5.541009375" x2="10.9474" y2="5.55904375" layer="94"/>
<rectangle x1="11.09218125" y1="5.541009375" x2="12.03198125" y2="5.55904375" layer="94"/>
<rectangle x1="13.09878125" y1="5.541009375" x2="14.869159375" y2="5.55904375" layer="94"/>
<rectangle x1="16.334740625" y1="5.541009375" x2="17.020540625" y2="5.55904375" layer="94"/>
<rectangle x1="17.218659375" y1="5.541009375" x2="18.666459375" y2="5.55904375" layer="94"/>
<rectangle x1="21.4503" y1="5.541009375" x2="22.806659375" y2="5.55904375" layer="94"/>
<rectangle x1="24.19858125" y1="5.541009375" x2="24.46781875" y2="5.55904375" layer="94"/>
<rectangle x1="0.568959375" y1="5.55904375" x2="1.381759375" y2="5.577078125" layer="94"/>
<rectangle x1="4.56438125" y1="5.55904375" x2="5.34161875" y2="5.577078125" layer="94"/>
<rectangle x1="6.5532" y1="5.55904375" x2="7.330440625" y2="5.577078125" layer="94"/>
<rectangle x1="7.874" y1="5.55904375" x2="9.464040625" y2="5.577078125" layer="94"/>
<rectangle x1="10.259059375" y1="5.55904375" x2="10.9474" y2="5.577078125" layer="94"/>
<rectangle x1="11.109959375" y1="5.55904375" x2="12.049759375" y2="5.577078125" layer="94"/>
<rectangle x1="13.134340625" y1="5.55904375" x2="14.85138125" y2="5.577078125" layer="94"/>
<rectangle x1="16.334740625" y1="5.55904375" x2="17.020540625" y2="5.577078125" layer="94"/>
<rectangle x1="17.256759375" y1="5.55904375" x2="18.666459375" y2="5.577078125" layer="94"/>
<rectangle x1="21.43251875" y1="5.55904375" x2="22.768559375" y2="5.577078125" layer="94"/>
<rectangle x1="24.16048125" y1="5.55904375" x2="24.432259375" y2="5.577078125" layer="94"/>
<rectangle x1="0.586740625" y1="5.577078125" x2="1.381759375" y2="5.5951125" layer="94"/>
<rectangle x1="3.083559375" y1="5.577078125" x2="3.1369" y2="5.5951125" layer="94"/>
<rectangle x1="4.56438125" y1="5.577078125" x2="5.34161875" y2="5.5951125" layer="94"/>
<rectangle x1="6.57098125" y1="5.577078125" x2="7.330440625" y2="5.5951125" layer="94"/>
<rectangle x1="7.874" y1="5.577078125" x2="9.48181875" y2="5.5951125" layer="94"/>
<rectangle x1="10.259059375" y1="5.577078125" x2="10.96518125" y2="5.5951125" layer="94"/>
<rectangle x1="11.127740625" y1="5.577078125" x2="12.049759375" y2="5.5951125" layer="94"/>
<rectangle x1="13.1699" y1="5.577078125" x2="14.8336" y2="5.5951125" layer="94"/>
<rectangle x1="16.334740625" y1="5.577078125" x2="17.020540625" y2="5.5951125" layer="94"/>
<rectangle x1="17.274540625" y1="5.577078125" x2="18.64868125" y2="5.5951125" layer="94"/>
<rectangle x1="21.39441875" y1="5.577078125" x2="22.75078125" y2="5.5951125" layer="94"/>
<rectangle x1="24.12491875" y1="5.577078125" x2="24.3967" y2="5.5951125" layer="94"/>
<rectangle x1="0.586740625" y1="5.5951125" x2="1.40208125" y2="5.6134" layer="94"/>
<rectangle x1="3.101340625" y1="5.5951125" x2="3.20801875" y2="5.6134" layer="94"/>
<rectangle x1="4.56438125" y1="5.5951125" x2="5.34161875" y2="5.6134" layer="94"/>
<rectangle x1="6.588759375" y1="5.5951125" x2="7.34821875" y2="5.6134" layer="94"/>
<rectangle x1="7.89178125" y1="5.5951125" x2="9.48181875" y2="5.6134" layer="94"/>
<rectangle x1="10.276840625" y1="5.5951125" x2="10.96518125" y2="5.6134" layer="94"/>
<rectangle x1="11.14551875" y1="5.5951125" x2="12.067540625" y2="5.6134" layer="94"/>
<rectangle x1="13.205459375" y1="5.5951125" x2="14.81581875" y2="5.6134" layer="94"/>
<rectangle x1="16.35251875" y1="5.5951125" x2="17.03831875" y2="5.6134" layer="94"/>
<rectangle x1="17.3101" y1="5.5951125" x2="18.6309" y2="5.6134" layer="94"/>
<rectangle x1="21.358859375" y1="5.5951125" x2="22.733" y2="5.6134" layer="94"/>
<rectangle x1="24.07158125" y1="5.5951125" x2="24.361140625" y2="5.6134" layer="94"/>
<rectangle x1="0.60451875" y1="5.6134" x2="1.419859375" y2="5.631434375" layer="94"/>
<rectangle x1="3.101340625" y1="5.6134" x2="3.28168125" y2="5.631434375" layer="94"/>
<rectangle x1="4.56438125" y1="5.6134" x2="5.34161875" y2="5.631434375" layer="94"/>
<rectangle x1="6.588759375" y1="5.6134" x2="7.366" y2="5.631434375" layer="94"/>
<rectangle x1="7.89178125" y1="5.6134" x2="9.48181875" y2="5.631434375" layer="94"/>
<rectangle x1="10.276840625" y1="5.6134" x2="10.96518125" y2="5.631434375" layer="94"/>
<rectangle x1="11.1633" y1="5.6134" x2="12.08531875" y2="5.631434375" layer="94"/>
<rectangle x1="13.223240625" y1="5.6134" x2="14.798040625" y2="5.631434375" layer="94"/>
<rectangle x1="16.35251875" y1="5.6134" x2="17.03831875" y2="5.631434375" layer="94"/>
<rectangle x1="17.32788125" y1="5.6134" x2="18.61058125" y2="5.631434375" layer="94"/>
<rectangle x1="21.34108125" y1="5.6134" x2="22.71521875" y2="5.631434375" layer="94"/>
<rectangle x1="24.03601875" y1="5.6134" x2="24.343359375" y2="5.631434375" layer="94"/>
<rectangle x1="0.60451875" y1="5.631434375" x2="1.419859375" y2="5.64946875" layer="94"/>
<rectangle x1="3.101340625" y1="5.631434375" x2="3.37058125" y2="5.64946875" layer="94"/>
<rectangle x1="4.56438125" y1="5.631434375" x2="5.3594" y2="5.64946875" layer="94"/>
<rectangle x1="6.606540625" y1="5.631434375" x2="7.366" y2="5.64946875" layer="94"/>
<rectangle x1="7.89178125" y1="5.631434375" x2="9.4996" y2="5.64946875" layer="94"/>
<rectangle x1="10.29461875" y1="5.631434375" x2="10.982959375" y2="5.64946875" layer="94"/>
<rectangle x1="11.198859375" y1="5.631434375" x2="12.1031" y2="5.64946875" layer="94"/>
<rectangle x1="13.261340625" y1="5.631434375" x2="14.759940625" y2="5.64946875" layer="94"/>
<rectangle x1="16.3703" y1="5.631434375" x2="17.0561" y2="5.64946875" layer="94"/>
<rectangle x1="17.363440625" y1="5.631434375" x2="18.61058125" y2="5.64946875" layer="94"/>
<rectangle x1="21.30551875" y1="5.631434375" x2="22.679659375" y2="5.64946875" layer="94"/>
<rectangle x1="23.99791875" y1="5.631434375" x2="24.305259375" y2="5.64946875" layer="94"/>
<rectangle x1="0.624840625" y1="5.64946875" x2="1.437640625" y2="5.667503125" layer="94"/>
<rectangle x1="3.101340625" y1="5.64946875" x2="3.444240625" y2="5.667503125" layer="94"/>
<rectangle x1="4.56438125" y1="5.64946875" x2="5.3594" y2="5.667503125" layer="94"/>
<rectangle x1="6.626859375" y1="5.64946875" x2="7.38378125" y2="5.667503125" layer="94"/>
<rectangle x1="7.909559375" y1="5.64946875" x2="9.4996" y2="5.667503125" layer="94"/>
<rectangle x1="10.29461875" y1="5.64946875" x2="10.982959375" y2="5.667503125" layer="94"/>
<rectangle x1="11.216640625" y1="5.64946875" x2="12.1031" y2="5.667503125" layer="94"/>
<rectangle x1="13.31468125" y1="5.64946875" x2="14.742159375" y2="5.667503125" layer="94"/>
<rectangle x1="16.3703" y1="5.64946875" x2="17.0561" y2="5.667503125" layer="94"/>
<rectangle x1="17.401540625" y1="5.64946875" x2="18.5928" y2="5.667503125" layer="94"/>
<rectangle x1="21.269959375" y1="5.64946875" x2="22.66188125" y2="5.667503125" layer="94"/>
<rectangle x1="23.94458125" y1="5.64946875" x2="24.2697" y2="5.667503125" layer="94"/>
<rectangle x1="0.64261875" y1="5.667503125" x2="1.45541875" y2="5.6855375" layer="94"/>
<rectangle x1="3.101340625" y1="5.667503125" x2="3.515359375" y2="5.6855375" layer="94"/>
<rectangle x1="4.56438125" y1="5.667503125" x2="5.3594" y2="5.6855375" layer="94"/>
<rectangle x1="6.626859375" y1="5.667503125" x2="7.4041" y2="5.6855375" layer="94"/>
<rectangle x1="7.909559375" y1="5.667503125" x2="9.4996" y2="5.6855375" layer="94"/>
<rectangle x1="10.29461875" y1="5.667503125" x2="10.982959375" y2="5.6855375" layer="94"/>
<rectangle x1="11.23441875" y1="5.667503125" x2="12.12088125" y2="5.6855375" layer="94"/>
<rectangle x1="13.350240625" y1="5.667503125" x2="14.72438125" y2="5.6855375" layer="94"/>
<rectangle x1="16.3703" y1="5.667503125" x2="17.0561" y2="5.6855375" layer="94"/>
<rectangle x1="17.4371" y1="5.667503125" x2="18.557240625" y2="5.6855375" layer="94"/>
<rectangle x1="21.231859375" y1="5.667503125" x2="22.6441" y2="5.6855375" layer="94"/>
<rectangle x1="23.90901875" y1="5.667503125" x2="24.234140625" y2="5.6855375" layer="94"/>
<rectangle x1="0.64261875" y1="5.6855375" x2="1.45541875" y2="5.703825" layer="94"/>
<rectangle x1="3.101340625" y1="5.6855375" x2="3.58901875" y2="5.703825" layer="94"/>
<rectangle x1="4.56438125" y1="5.6855375" x2="5.3594" y2="5.703825" layer="94"/>
<rectangle x1="6.644640625" y1="5.6855375" x2="7.4041" y2="5.703825" layer="94"/>
<rectangle x1="7.909559375" y1="5.6855375" x2="9.51738125" y2="5.703825" layer="94"/>
<rectangle x1="10.3124" y1="5.6855375" x2="11.000740625" y2="5.703825" layer="94"/>
<rectangle x1="11.27251875" y1="5.6855375" x2="12.12088125" y2="5.703825" layer="94"/>
<rectangle x1="13.3858" y1="5.6855375" x2="14.68881875" y2="5.703825" layer="94"/>
<rectangle x1="16.38808125" y1="5.6855375" x2="17.07388125" y2="5.703825" layer="94"/>
<rectangle x1="17.472659375" y1="5.6855375" x2="18.539459375" y2="5.703825" layer="94"/>
<rectangle x1="21.1963" y1="5.6855375" x2="22.62378125" y2="5.703825" layer="94"/>
<rectangle x1="23.873459375" y1="5.6855375" x2="24.19858125" y2="5.703825" layer="94"/>
<rectangle x1="0.6604" y1="5.703825" x2="1.4732" y2="5.721859375" layer="94"/>
<rectangle x1="3.101340625" y1="5.703825" x2="3.660140625" y2="5.721859375" layer="94"/>
<rectangle x1="4.56438125" y1="5.703825" x2="5.3594" y2="5.721859375" layer="94"/>
<rectangle x1="6.66241875" y1="5.703825" x2="7.42188125" y2="5.721859375" layer="94"/>
<rectangle x1="7.927340625" y1="5.703825" x2="9.51738125" y2="5.721859375" layer="94"/>
<rectangle x1="10.3124" y1="5.703825" x2="11.000740625" y2="5.721859375" layer="94"/>
<rectangle x1="11.2903" y1="5.703825" x2="12.1031" y2="5.721859375" layer="94"/>
<rectangle x1="13.44168125" y1="5.703825" x2="14.653259375" y2="5.721859375" layer="94"/>
<rectangle x1="16.38808125" y1="5.703825" x2="17.07388125" y2="5.721859375" layer="94"/>
<rectangle x1="17.50821875" y1="5.703825" x2="18.52168125" y2="5.721859375" layer="94"/>
<rectangle x1="21.17851875" y1="5.703825" x2="22.58821875" y2="5.721859375" layer="94"/>
<rectangle x1="23.81758125" y1="5.703825" x2="24.1808" y2="5.721859375" layer="94"/>
<rectangle x1="0.67818125" y1="5.721859375" x2="1.49098125" y2="5.739890625" layer="94"/>
<rectangle x1="3.101340625" y1="5.721859375" x2="3.75158125" y2="5.739890625" layer="94"/>
<rectangle x1="4.56438125" y1="5.721859375" x2="5.3594" y2="5.739890625" layer="94"/>
<rectangle x1="6.66241875" y1="5.721859375" x2="7.439659375" y2="5.739890625" layer="94"/>
<rectangle x1="7.927340625" y1="5.721859375" x2="9.51738125" y2="5.739890625" layer="94"/>
<rectangle x1="10.3124" y1="5.721859375" x2="11.000740625" y2="5.739890625" layer="94"/>
<rectangle x1="11.325859375" y1="5.721859375" x2="12.067540625" y2="5.739890625" layer="94"/>
<rectangle x1="13.477240625" y1="5.721859375" x2="14.63548125" y2="5.739890625" layer="94"/>
<rectangle x1="16.38808125" y1="5.721859375" x2="17.07388125" y2="5.739890625" layer="94"/>
<rectangle x1="17.54378125" y1="5.721859375" x2="18.48611875" y2="5.739890625" layer="94"/>
<rectangle x1="21.142959375" y1="5.721859375" x2="22.570440625" y2="5.739890625" layer="94"/>
<rectangle x1="23.78201875" y1="5.721859375" x2="24.12491875" y2="5.739890625" layer="94"/>
<rectangle x1="0.67818125" y1="5.739890625" x2="1.508759375" y2="5.757925" layer="94"/>
<rectangle x1="3.101340625" y1="5.739890625" x2="3.8227" y2="5.757925" layer="94"/>
<rectangle x1="4.56438125" y1="5.739890625" x2="5.3594" y2="5.757925" layer="94"/>
<rectangle x1="6.6802" y1="5.739890625" x2="7.457440625" y2="5.757925" layer="94"/>
<rectangle x1="7.927340625" y1="5.739890625" x2="9.535159375" y2="5.757925" layer="94"/>
<rectangle x1="10.33271875" y1="5.739890625" x2="11.01851875" y2="5.757925" layer="94"/>
<rectangle x1="11.343640625" y1="5.739890625" x2="12.049759375" y2="5.757925" layer="94"/>
<rectangle x1="13.53058125" y1="5.739890625" x2="14.5796" y2="5.757925" layer="94"/>
<rectangle x1="16.405859375" y1="5.739890625" x2="17.0942" y2="5.757925" layer="94"/>
<rectangle x1="17.58188125" y1="5.739890625" x2="18.4658" y2="5.757925" layer="94"/>
<rectangle x1="21.1074" y1="5.739890625" x2="22.53488125" y2="5.757925" layer="94"/>
<rectangle x1="23.72868125" y1="5.739890625" x2="24.089359375" y2="5.757925" layer="94"/>
<rectangle x1="0.695959375" y1="5.757925" x2="1.526540625" y2="5.775959375" layer="94"/>
<rectangle x1="3.101340625" y1="5.757925" x2="3.896359375" y2="5.775959375" layer="94"/>
<rectangle x1="4.56438125" y1="5.757925" x2="5.3594" y2="5.775959375" layer="94"/>
<rectangle x1="6.69798125" y1="5.757925" x2="7.457440625" y2="5.775959375" layer="94"/>
<rectangle x1="7.94511875" y1="5.757925" x2="9.535159375" y2="5.775959375" layer="94"/>
<rectangle x1="10.33271875" y1="5.757925" x2="11.01851875" y2="5.775959375" layer="94"/>
<rectangle x1="11.3792" y1="5.757925" x2="12.0142" y2="5.775959375" layer="94"/>
<rectangle x1="13.586459375" y1="5.757925" x2="14.544040625" y2="5.775959375" layer="94"/>
<rectangle x1="16.405859375" y1="5.757925" x2="17.0942" y2="5.775959375" layer="94"/>
<rectangle x1="17.63521875" y1="5.757925" x2="18.430240625" y2="5.775959375" layer="94"/>
<rectangle x1="21.0693" y1="5.757925" x2="22.5171" y2="5.775959375" layer="94"/>
<rectangle x1="23.6728" y1="5.757925" x2="24.07158125" y2="5.775959375" layer="94"/>
<rectangle x1="0.713740625" y1="5.775959375" x2="1.546859375" y2="5.79399375" layer="94"/>
<rectangle x1="3.101340625" y1="5.775959375" x2="3.93191875" y2="5.79399375" layer="94"/>
<rectangle x1="8.28801875" y1="5.775959375" x2="9.0297" y2="5.79399375" layer="94"/>
<rectangle x1="11.4173" y1="5.775959375" x2="11.9761" y2="5.79399375" layer="94"/>
<rectangle x1="13.65758125" y1="5.775959375" x2="14.4907" y2="5.79399375" layer="94"/>
<rectangle x1="17.688559375" y1="5.775959375" x2="18.39468125" y2="5.79399375" layer="94"/>
<rectangle x1="21.033740625" y1="5.775959375" x2="22.481540625" y2="5.79399375" layer="94"/>
<rectangle x1="23.637240625" y1="5.775959375" x2="24.03601875" y2="5.79399375" layer="94"/>
<rectangle x1="0.713740625" y1="5.79399375" x2="1.546859375" y2="5.81228125" layer="94"/>
<rectangle x1="3.101340625" y1="5.79399375" x2="3.93191875" y2="5.81228125" layer="94"/>
<rectangle x1="8.28801875" y1="5.79399375" x2="9.04748125" y2="5.81228125" layer="94"/>
<rectangle x1="11.470640625" y1="5.79399375" x2="11.922759375" y2="5.81228125" layer="94"/>
<rectangle x1="13.731240625" y1="5.79399375" x2="14.417040625" y2="5.81228125" layer="94"/>
<rectangle x1="17.76221875" y1="5.79399375" x2="18.323559375" y2="5.81228125" layer="94"/>
<rectangle x1="20.99818125" y1="5.79399375" x2="22.46121875" y2="5.81228125" layer="94"/>
<rectangle x1="23.5839" y1="5.79399375" x2="23.99791875" y2="5.81228125" layer="94"/>
<rectangle x1="0.73151875" y1="5.81228125" x2="1.564640625" y2="5.830315625" layer="94"/>
<rectangle x1="3.101340625" y1="5.81228125" x2="3.93191875" y2="5.830315625" layer="94"/>
<rectangle x1="8.3058" y1="5.81228125" x2="9.04748125" y2="5.830315625" layer="94"/>
<rectangle x1="11.541759375" y1="5.81228125" x2="11.86941875" y2="5.830315625" layer="94"/>
<rectangle x1="13.83791875" y1="5.81228125" x2="14.328140625" y2="5.830315625" layer="94"/>
<rectangle x1="17.85111875" y1="5.81228125" x2="18.2499" y2="5.830315625" layer="94"/>
<rectangle x1="20.96261875" y1="5.81228125" x2="22.443440625" y2="5.830315625" layer="94"/>
<rectangle x1="23.52801875" y1="5.81228125" x2="23.962359375" y2="5.830315625" layer="94"/>
<rectangle x1="0.7493" y1="5.830315625" x2="1.58241875" y2="5.84835" layer="94"/>
<rectangle x1="3.101340625" y1="5.830315625" x2="3.93191875" y2="5.84835" layer="94"/>
<rectangle x1="8.3058" y1="5.830315625" x2="9.04748125" y2="5.84835" layer="94"/>
<rectangle x1="11.668759375" y1="5.830315625" x2="11.74241875" y2="5.84835" layer="94"/>
<rectangle x1="14.03858125" y1="5.830315625" x2="14.12748125" y2="5.84835" layer="94"/>
<rectangle x1="18.034" y1="5.830315625" x2="18.087340625" y2="5.84835" layer="94"/>
<rectangle x1="20.92451875" y1="5.830315625" x2="22.40788125" y2="5.84835" layer="94"/>
<rectangle x1="23.492459375" y1="5.830315625" x2="23.9268" y2="5.84835" layer="94"/>
<rectangle x1="0.76708125" y1="5.84835" x2="1.6002" y2="5.866384375" layer="94"/>
<rectangle x1="3.083559375" y1="5.84835" x2="3.9497" y2="5.866384375" layer="94"/>
<rectangle x1="8.32611875" y1="5.84835" x2="9.065259375" y2="5.866384375" layer="94"/>
<rectangle x1="20.888959375" y1="5.84835" x2="22.3901" y2="5.866384375" layer="94"/>
<rectangle x1="23.43911875" y1="5.84835" x2="23.891240625" y2="5.866384375" layer="94"/>
<rectangle x1="0.76708125" y1="5.866384375" x2="1.61798125" y2="5.88441875" layer="94"/>
<rectangle x1="3.083559375" y1="5.866384375" x2="3.9497" y2="5.88441875" layer="94"/>
<rectangle x1="8.32611875" y1="5.866384375" x2="9.065259375" y2="5.88441875" layer="94"/>
<rectangle x1="20.8534" y1="5.866384375" x2="22.354540625" y2="5.88441875" layer="94"/>
<rectangle x1="23.383240625" y1="5.866384375" x2="23.853140625" y2="5.88441875" layer="94"/>
<rectangle x1="0.7874" y1="5.88441875" x2="1.635759375" y2="5.902453125" layer="94"/>
<rectangle x1="3.083559375" y1="5.88441875" x2="3.9497" y2="5.902453125" layer="94"/>
<rectangle x1="8.32611875" y1="5.88441875" x2="9.065259375" y2="5.902453125" layer="94"/>
<rectangle x1="20.817840625" y1="5.88441875" x2="22.336759375" y2="5.902453125" layer="94"/>
<rectangle x1="23.3299" y1="5.88441875" x2="23.81758125" y2="5.902453125" layer="94"/>
<rectangle x1="0.80518125" y1="5.902453125" x2="1.653540625" y2="5.920740625" layer="94"/>
<rectangle x1="3.083559375" y1="5.902453125" x2="3.9497" y2="5.920740625" layer="94"/>
<rectangle x1="8.3439" y1="5.902453125" x2="9.08558125" y2="5.920740625" layer="94"/>
<rectangle x1="20.779740625" y1="5.902453125" x2="22.298659375" y2="5.920740625" layer="94"/>
<rectangle x1="23.276559375" y1="5.902453125" x2="23.764240625" y2="5.920740625" layer="94"/>
<rectangle x1="0.822959375" y1="5.920740625" x2="1.67131875" y2="5.938775" layer="94"/>
<rectangle x1="3.083559375" y1="5.920740625" x2="3.9497" y2="5.938775" layer="94"/>
<rectangle x1="8.3439" y1="5.920740625" x2="9.08558125" y2="5.938775" layer="94"/>
<rectangle x1="20.74418125" y1="5.920740625" x2="22.28088125" y2="5.938775" layer="94"/>
<rectangle x1="23.22068125" y1="5.920740625" x2="23.72868125" y2="5.938775" layer="94"/>
<rectangle x1="0.822959375" y1="5.938775" x2="1.6891" y2="5.956809375" layer="94"/>
<rectangle x1="3.063240625" y1="5.938775" x2="3.9497" y2="5.956809375" layer="94"/>
<rectangle x1="8.3439" y1="5.938775" x2="9.103359375" y2="5.956809375" layer="94"/>
<rectangle x1="20.70861875" y1="5.938775" x2="22.24531875" y2="5.956809375" layer="94"/>
<rectangle x1="23.18511875" y1="5.938775" x2="23.69058125" y2="5.956809375" layer="94"/>
<rectangle x1="0.840740625" y1="5.956809375" x2="1.7272" y2="5.974840625" layer="94"/>
<rectangle x1="3.063240625" y1="5.956809375" x2="3.9497" y2="5.974840625" layer="94"/>
<rectangle x1="8.36168125" y1="5.956809375" x2="9.103359375" y2="5.974840625" layer="94"/>
<rectangle x1="20.673059375" y1="5.956809375" x2="22.227540625" y2="5.974840625" layer="94"/>
<rectangle x1="23.13178125" y1="5.956809375" x2="23.65501875" y2="5.974840625" layer="94"/>
<rectangle x1="0.85851875" y1="5.974840625" x2="1.74498125" y2="5.992875" layer="94"/>
<rectangle x1="3.045459375" y1="5.974840625" x2="3.9497" y2="5.992875" layer="94"/>
<rectangle x1="8.36168125" y1="5.974840625" x2="9.103359375" y2="5.992875" layer="94"/>
<rectangle x1="20.6375" y1="5.974840625" x2="22.19198125" y2="5.992875" layer="94"/>
<rectangle x1="23.05811875" y1="5.974840625" x2="23.619459375" y2="5.992875" layer="94"/>
<rectangle x1="0.8763" y1="5.992875" x2="1.762759375" y2="6.010909375" layer="94"/>
<rectangle x1="3.045459375" y1="5.992875" x2="3.9497" y2="6.010909375" layer="94"/>
<rectangle x1="8.36168125" y1="5.992875" x2="9.121140625" y2="6.010909375" layer="94"/>
<rectangle x1="20.5994" y1="5.992875" x2="22.1742" y2="6.010909375" layer="94"/>
<rectangle x1="23.00478125" y1="5.992875" x2="23.5839" y2="6.010909375" layer="94"/>
<rectangle x1="0.89408125" y1="6.010909375" x2="1.780540625" y2="6.029196875" layer="94"/>
<rectangle x1="3.02768125" y1="6.010909375" x2="3.9497" y2="6.029196875" layer="94"/>
<rectangle x1="8.379459375" y1="6.010909375" x2="9.121140625" y2="6.029196875" layer="94"/>
<rectangle x1="20.563840625" y1="6.010909375" x2="22.1361" y2="6.029196875" layer="94"/>
<rectangle x1="22.951440625" y1="6.010909375" x2="23.52801875" y2="6.029196875" layer="94"/>
<rectangle x1="0.911859375" y1="6.029196875" x2="1.8161" y2="6.04723125" layer="94"/>
<rectangle x1="3.02768125" y1="6.029196875" x2="3.9497" y2="6.04723125" layer="94"/>
<rectangle x1="8.379459375" y1="6.029196875" x2="9.121140625" y2="6.04723125" layer="94"/>
<rectangle x1="20.5105" y1="6.029196875" x2="22.100540625" y2="6.04723125" layer="94"/>
<rectangle x1="22.87778125" y1="6.029196875" x2="23.492459375" y2="6.04723125" layer="94"/>
<rectangle x1="0.911859375" y1="6.04723125" x2="1.83388125" y2="6.065265625" layer="94"/>
<rectangle x1="3.0099" y1="6.04723125" x2="3.9497" y2="6.065265625" layer="94"/>
<rectangle x1="8.397240625" y1="6.04723125" x2="9.13891875" y2="6.065265625" layer="94"/>
<rectangle x1="20.4724" y1="6.04723125" x2="22.082759375" y2="6.065265625" layer="94"/>
<rectangle x1="22.824440625" y1="6.04723125" x2="23.4569" y2="6.065265625" layer="94"/>
<rectangle x1="0.93218125" y1="6.065265625" x2="1.87198125" y2="6.0833" layer="94"/>
<rectangle x1="3.0099" y1="6.065265625" x2="3.9497" y2="6.0833" layer="94"/>
<rectangle x1="8.397240625" y1="6.065265625" x2="9.13891875" y2="6.0833" layer="94"/>
<rectangle x1="20.436840625" y1="6.065265625" x2="22.0472" y2="6.0833" layer="94"/>
<rectangle x1="22.75078125" y1="6.065265625" x2="23.421340625" y2="6.0833" layer="94"/>
<rectangle x1="0.949959375" y1="6.0833" x2="1.889759375" y2="6.101334375" layer="94"/>
<rectangle x1="2.99211875" y1="6.0833" x2="3.9497" y2="6.101334375" layer="94"/>
<rectangle x1="8.397240625" y1="6.0833" x2="9.13891875" y2="6.101334375" layer="94"/>
<rectangle x1="20.40128125" y1="6.0833" x2="22.0091" y2="6.101334375" layer="94"/>
<rectangle x1="22.679659375" y1="6.0833" x2="23.365459375" y2="6.101334375" layer="94"/>
<rectangle x1="0.967740625" y1="6.101334375" x2="1.92531875" y2="6.119621875" layer="94"/>
<rectangle x1="2.974340625" y1="6.101334375" x2="3.9497" y2="6.119621875" layer="94"/>
<rectangle x1="8.41501875" y1="6.101334375" x2="9.1567" y2="6.119621875" layer="94"/>
<rectangle x1="20.347940625" y1="6.101334375" x2="21.973540625" y2="6.119621875" layer="94"/>
<rectangle x1="22.62378125" y1="6.101334375" x2="23.3299" y2="6.119621875" layer="94"/>
<rectangle x1="0.98551875" y1="6.119621875" x2="1.96088125" y2="6.13765625" layer="94"/>
<rectangle x1="2.956559375" y1="6.119621875" x2="3.9497" y2="6.13765625" layer="94"/>
<rectangle x1="8.41501875" y1="6.119621875" x2="9.1567" y2="6.13765625" layer="94"/>
<rectangle x1="20.309840625" y1="6.119621875" x2="21.955759375" y2="6.13765625" layer="94"/>
<rectangle x1="22.53488125" y1="6.119621875" x2="23.276559375" y2="6.13765625" layer="94"/>
<rectangle x1="1.0033" y1="6.13765625" x2="1.978659375" y2="6.155690625" layer="94"/>
<rectangle x1="2.93878125" y1="6.13765625" x2="3.93191875" y2="6.155690625" layer="94"/>
<rectangle x1="8.41501875" y1="6.13765625" x2="9.17448125" y2="6.155690625" layer="94"/>
<rectangle x1="20.27428125" y1="6.13765625" x2="21.9202" y2="6.155690625" layer="94"/>
<rectangle x1="22.46121875" y1="6.13765625" x2="23.238459375" y2="6.155690625" layer="94"/>
<rectangle x1="1.02108125" y1="6.155690625" x2="2.016759375" y2="6.173725" layer="94"/>
<rectangle x1="2.918459375" y1="6.155690625" x2="3.93191875" y2="6.173725" layer="94"/>
<rectangle x1="8.4328" y1="6.155690625" x2="9.17448125" y2="6.173725" layer="94"/>
<rectangle x1="20.220940625" y1="6.155690625" x2="21.884640625" y2="6.173725" layer="94"/>
<rectangle x1="22.3901" y1="6.155690625" x2="23.2029" y2="6.173725" layer="94"/>
<rectangle x1="1.038859375" y1="6.173725" x2="2.0701" y2="6.191759375" layer="94"/>
<rectangle x1="2.90068125" y1="6.173725" x2="3.93191875" y2="6.191759375" layer="94"/>
<rectangle x1="8.4328" y1="6.173725" x2="9.17448125" y2="6.191759375" layer="94"/>
<rectangle x1="20.18538125" y1="6.173725" x2="21.846540625" y2="6.191759375" layer="94"/>
<rectangle x1="22.298659375" y1="6.173725" x2="23.149559375" y2="6.191759375" layer="94"/>
<rectangle x1="1.056640625" y1="6.191759375" x2="2.105659375" y2="6.209790625" layer="94"/>
<rectangle x1="2.86511875" y1="6.191759375" x2="3.93191875" y2="6.209790625" layer="94"/>
<rectangle x1="8.45058125" y1="6.191759375" x2="9.192259375" y2="6.209790625" layer="94"/>
<rectangle x1="20.14728125" y1="6.191759375" x2="21.81098125" y2="6.209790625" layer="94"/>
<rectangle x1="22.209759375" y1="6.191759375" x2="23.114" y2="6.209790625" layer="94"/>
<rectangle x1="1.07441875" y1="6.209790625" x2="2.161540625" y2="6.22808125" layer="94"/>
<rectangle x1="2.829559375" y1="6.209790625" x2="3.93191875" y2="6.22808125" layer="94"/>
<rectangle x1="8.45058125" y1="6.209790625" x2="9.192259375" y2="6.22808125" layer="94"/>
<rectangle x1="20.093940625" y1="6.209790625" x2="21.77541875" y2="6.22808125" layer="94"/>
<rectangle x1="22.100540625" y1="6.209790625" x2="23.05811875" y2="6.22808125" layer="94"/>
<rectangle x1="1.094740625" y1="6.22808125" x2="2.1971" y2="6.2461125" layer="94"/>
<rectangle x1="2.794" y1="6.22808125" x2="3.93191875" y2="6.2461125" layer="94"/>
<rectangle x1="8.45058125" y1="6.22808125" x2="9.192259375" y2="6.2461125" layer="94"/>
<rectangle x1="20.05838125" y1="6.22808125" x2="21.739859375" y2="6.2461125" layer="94"/>
<rectangle x1="21.973540625" y1="6.22808125" x2="23.022559375" y2="6.2461125" layer="94"/>
<rectangle x1="1.11251875" y1="6.2461125" x2="2.26821875" y2="6.264146875" layer="94"/>
<rectangle x1="2.73811875" y1="6.2461125" x2="3.93191875" y2="6.264146875" layer="94"/>
<rectangle x1="8.4709" y1="6.2461125" x2="9.210040625" y2="6.264146875" layer="94"/>
<rectangle x1="20.0025" y1="6.2461125" x2="21.701759375" y2="6.264146875" layer="94"/>
<rectangle x1="21.7932" y1="6.2461125" x2="22.96921875" y2="6.264146875" layer="94"/>
<rectangle x1="1.1303" y1="6.264146875" x2="2.377440625" y2="6.28218125" layer="94"/>
<rectangle x1="2.667" y1="6.264146875" x2="3.93191875" y2="6.28218125" layer="94"/>
<rectangle x1="8.4709" y1="6.264146875" x2="9.210040625" y2="6.28218125" layer="94"/>
<rectangle x1="19.966940625" y1="6.264146875" x2="22.93111875" y2="6.28218125" layer="94"/>
<rectangle x1="1.14808125" y1="6.28218125" x2="3.914140625" y2="6.300215625" layer="94"/>
<rectangle x1="8.4709" y1="6.28218125" x2="9.22781875" y2="6.300215625" layer="94"/>
<rectangle x1="19.9136" y1="6.28218125" x2="22.87778125" y2="6.300215625" layer="94"/>
<rectangle x1="1.165859375" y1="6.300215625" x2="3.914140625" y2="6.31825" layer="94"/>
<rectangle x1="8.48868125" y1="6.300215625" x2="9.22781875" y2="6.31825" layer="94"/>
<rectangle x1="19.878040625" y1="6.300215625" x2="22.84221875" y2="6.31825" layer="94"/>
<rectangle x1="1.183640625" y1="6.31825" x2="3.914140625" y2="6.3365375" layer="94"/>
<rectangle x1="8.48868125" y1="6.31825" x2="9.22781875" y2="6.3365375" layer="94"/>
<rectangle x1="19.822159375" y1="6.31825" x2="22.78888125" y2="6.3365375" layer="94"/>
<rectangle x1="1.20141875" y1="6.3365375" x2="3.914140625" y2="6.354571875" layer="94"/>
<rectangle x1="8.48868125" y1="6.3365375" x2="9.248140625" y2="6.354571875" layer="94"/>
<rectangle x1="19.76881875" y1="6.3365375" x2="22.733" y2="6.354571875" layer="94"/>
<rectangle x1="1.2192" y1="6.354571875" x2="3.896359375" y2="6.37260625" layer="94"/>
<rectangle x1="8.506459375" y1="6.354571875" x2="9.248140625" y2="6.37260625" layer="94"/>
<rectangle x1="19.71548125" y1="6.354571875" x2="22.679659375" y2="6.37260625" layer="94"/>
<rectangle x1="1.23951875" y1="6.37260625" x2="3.896359375" y2="6.390640625" layer="94"/>
<rectangle x1="8.506459375" y1="6.37260625" x2="9.248140625" y2="6.390640625" layer="94"/>
<rectangle x1="19.67738125" y1="6.37260625" x2="22.62378125" y2="6.390640625" layer="94"/>
<rectangle x1="1.2573" y1="6.390640625" x2="3.896359375" y2="6.408675" layer="94"/>
<rectangle x1="8.54201875" y1="6.390640625" x2="9.26591875" y2="6.408675" layer="94"/>
<rectangle x1="19.624040625" y1="6.390640625" x2="22.58821875" y2="6.408675" layer="94"/>
<rectangle x1="1.292859375" y1="6.408675" x2="3.87858125" y2="6.426709375" layer="94"/>
<rectangle x1="8.57758125" y1="6.408675" x2="9.26591875" y2="6.426709375" layer="94"/>
<rectangle x1="19.5707" y1="6.408675" x2="22.53488125" y2="6.426709375" layer="94"/>
<rectangle x1="1.310640625" y1="6.426709375" x2="3.87858125" y2="6.444996875" layer="94"/>
<rectangle x1="8.613140625" y1="6.426709375" x2="9.26591875" y2="6.444996875" layer="94"/>
<rectangle x1="19.51481875" y1="6.426709375" x2="22.481540625" y2="6.444996875" layer="94"/>
<rectangle x1="1.32841875" y1="6.444996875" x2="3.87858125" y2="6.46303125" layer="94"/>
<rectangle x1="8.651240625" y1="6.444996875" x2="9.2837" y2="6.46303125" layer="94"/>
<rectangle x1="19.46148125" y1="6.444996875" x2="22.425659375" y2="6.46303125" layer="94"/>
<rectangle x1="1.3462" y1="6.46303125" x2="3.8608" y2="6.4810625" layer="94"/>
<rectangle x1="8.6868" y1="6.46303125" x2="9.2837" y2="6.4810625" layer="94"/>
<rectangle x1="19.408140625" y1="6.46303125" x2="22.37231875" y2="6.4810625" layer="94"/>
<rectangle x1="1.381759375" y1="6.4810625" x2="3.8608" y2="6.499096875" layer="94"/>
<rectangle x1="8.722359375" y1="6.4810625" x2="9.30148125" y2="6.499096875" layer="94"/>
<rectangle x1="19.352259375" y1="6.4810625" x2="22.316440625" y2="6.499096875" layer="94"/>
<rectangle x1="1.40208125" y1="6.499096875" x2="3.84048125" y2="6.51713125" layer="94"/>
<rectangle x1="8.75791875" y1="6.499096875" x2="9.30148125" y2="6.51713125" layer="94"/>
<rectangle x1="19.29891875" y1="6.499096875" x2="22.2631" y2="6.51713125" layer="94"/>
<rectangle x1="1.419859375" y1="6.51713125" x2="3.84048125" y2="6.53541875" layer="94"/>
<rectangle x1="8.79601875" y1="6.51713125" x2="9.30148125" y2="6.53541875" layer="94"/>
<rectangle x1="19.24558125" y1="6.51713125" x2="22.227540625" y2="6.53541875" layer="94"/>
<rectangle x1="1.45541875" y1="6.53541875" x2="3.8227" y2="6.553453125" layer="94"/>
<rectangle x1="8.83158125" y1="6.53541875" x2="9.319259375" y2="6.553453125" layer="94"/>
<rectangle x1="19.1897" y1="6.53541875" x2="22.1742" y2="6.553453125" layer="94"/>
<rectangle x1="1.4732" y1="6.553453125" x2="3.8227" y2="6.5714875" layer="94"/>
<rectangle x1="8.867140625" y1="6.553453125" x2="9.319259375" y2="6.5714875" layer="94"/>
<rectangle x1="19.11858125" y1="6.553453125" x2="22.11831875" y2="6.5714875" layer="94"/>
<rectangle x1="1.508759375" y1="6.5714875" x2="3.80491875" y2="6.589521875" layer="94"/>
<rectangle x1="8.88491875" y1="6.5714875" x2="9.319259375" y2="6.589521875" layer="94"/>
<rectangle x1="19.0627" y1="6.5714875" x2="22.0472" y2="6.589521875" layer="94"/>
<rectangle x1="1.526540625" y1="6.589521875" x2="3.787140625" y2="6.60755625" layer="94"/>
<rectangle x1="8.92048125" y1="6.589521875" x2="9.337040625" y2="6.60755625" layer="94"/>
<rectangle x1="18.99158125" y1="6.589521875" x2="21.99131875" y2="6.60755625" layer="94"/>
<rectangle x1="1.564640625" y1="6.60755625" x2="3.787140625" y2="6.625590625" layer="94"/>
<rectangle x1="8.95858125" y1="6.60755625" x2="9.337040625" y2="6.625590625" layer="94"/>
<rectangle x1="18.91791875" y1="6.60755625" x2="21.93798125" y2="6.625590625" layer="94"/>
<rectangle x1="1.58241875" y1="6.625590625" x2="3.769359375" y2="6.643878125" layer="94"/>
<rectangle x1="8.994140625" y1="6.625590625" x2="9.337040625" y2="6.643878125" layer="94"/>
<rectangle x1="18.86458125" y1="6.625590625" x2="21.866859375" y2="6.643878125" layer="94"/>
<rectangle x1="1.61798125" y1="6.643878125" x2="3.75158125" y2="6.6619125" layer="94"/>
<rectangle x1="9.0297" y1="6.643878125" x2="9.35481875" y2="6.6619125" layer="94"/>
<rectangle x1="18.793459375" y1="6.643878125" x2="21.81098125" y2="6.6619125" layer="94"/>
<rectangle x1="1.635759375" y1="6.6619125" x2="3.7338" y2="6.679946875" layer="94"/>
<rectangle x1="9.065259375" y1="6.6619125" x2="9.35481875" y2="6.679946875" layer="94"/>
<rectangle x1="18.7198" y1="6.6619125" x2="21.757640625" y2="6.679946875" layer="94"/>
<rectangle x1="1.67131875" y1="6.679946875" x2="3.71601875" y2="6.69798125" layer="94"/>
<rectangle x1="9.103359375" y1="6.679946875" x2="9.3726" y2="6.69798125" layer="94"/>
<rectangle x1="18.64868125" y1="6.679946875" x2="21.68398125" y2="6.69798125" layer="94"/>
<rectangle x1="1.70941875" y1="6.69798125" x2="3.698240625" y2="6.7160125" layer="94"/>
<rectangle x1="9.13891875" y1="6.69798125" x2="9.3726" y2="6.7160125" layer="94"/>
<rectangle x1="18.57501875" y1="6.69798125" x2="21.630640625" y2="6.7160125" layer="94"/>
<rectangle x1="1.74498125" y1="6.7160125" x2="3.67791875" y2="6.734046875" layer="94"/>
<rectangle x1="9.17448125" y1="6.7160125" x2="9.3726" y2="6.734046875" layer="94"/>
<rectangle x1="18.5039" y1="6.7160125" x2="21.5773" y2="6.734046875" layer="94"/>
<rectangle x1="1.780540625" y1="6.734046875" x2="3.660140625" y2="6.7523375" layer="94"/>
<rectangle x1="9.210040625" y1="6.734046875" x2="9.39291875" y2="6.7523375" layer="94"/>
<rectangle x1="18.430240625" y1="6.734046875" x2="21.503640625" y2="6.7523375" layer="94"/>
<rectangle x1="1.79831875" y1="6.7523375" x2="3.642359375" y2="6.77036875" layer="94"/>
<rectangle x1="9.248140625" y1="6.7523375" x2="9.39291875" y2="6.77036875" layer="94"/>
<rectangle x1="18.35911875" y1="6.7523375" x2="21.4503" y2="6.77036875" layer="94"/>
<rectangle x1="1.83388125" y1="6.77036875" x2="3.62458125" y2="6.788403125" layer="94"/>
<rectangle x1="9.2837" y1="6.77036875" x2="9.39291875" y2="6.788403125" layer="94"/>
<rectangle x1="18.285459375" y1="6.77036875" x2="21.376640625" y2="6.788403125" layer="94"/>
<rectangle x1="1.889759375" y1="6.788403125" x2="3.6068" y2="6.8064375" layer="94"/>
<rectangle x1="9.319259375" y1="6.788403125" x2="9.4107" y2="6.8064375" layer="94"/>
<rectangle x1="18.196559375" y1="6.788403125" x2="21.30551875" y2="6.8064375" layer="94"/>
<rectangle x1="1.92531875" y1="6.8064375" x2="3.571240625" y2="6.824471875" layer="94"/>
<rectangle x1="9.35481875" y1="6.8064375" x2="9.4107" y2="6.824471875" layer="94"/>
<rectangle x1="18.10511875" y1="6.8064375" x2="21.25218125" y2="6.824471875" layer="94"/>
<rectangle x1="1.96088125" y1="6.824471875" x2="3.553459375" y2="6.842759375" layer="94"/>
<rectangle x1="9.39291875" y1="6.824471875" x2="9.4107" y2="6.842759375" layer="94"/>
<rectangle x1="18.034" y1="6.824471875" x2="21.17851875" y2="6.842759375" layer="94"/>
<rectangle x1="1.996440625" y1="6.842759375" x2="3.515359375" y2="6.86079375" layer="94"/>
<rectangle x1="17.942559375" y1="6.842759375" x2="21.1074" y2="6.86079375" layer="94"/>
<rectangle x1="2.05231875" y1="6.86079375" x2="3.4798" y2="6.878828125" layer="94"/>
<rectangle x1="17.85111875" y1="6.86079375" x2="21.033740625" y2="6.878828125" layer="94"/>
<rectangle x1="2.105659375" y1="6.878828125" x2="3.46201875" y2="6.8968625" layer="94"/>
<rectangle x1="17.76221875" y1="6.878828125" x2="20.96261875" y2="6.8968625" layer="94"/>
<rectangle x1="2.161540625" y1="6.8968625" x2="3.40868125" y2="6.914896875" layer="94"/>
<rectangle x1="17.67078125" y1="6.8968625" x2="20.888959375" y2="6.914896875" layer="94"/>
<rectangle x1="2.21488125" y1="6.914896875" x2="3.37058125" y2="6.93293125" layer="94"/>
<rectangle x1="17.58188125" y1="6.914896875" x2="20.817840625" y2="6.93293125" layer="94"/>
<rectangle x1="2.286" y1="6.93293125" x2="3.317240625" y2="6.95121875" layer="94"/>
<rectangle x1="17.472659375" y1="6.93293125" x2="20.74418125" y2="6.95121875" layer="94"/>
<rectangle x1="2.359659375" y1="6.95121875" x2="3.24611875" y2="6.969253125" layer="94"/>
<rectangle x1="17.38121875" y1="6.95121875" x2="20.673059375" y2="6.969253125" layer="94"/>
<rectangle x1="2.448559375" y1="6.969253125" x2="3.172459375" y2="6.9872875" layer="94"/>
<rectangle x1="17.274540625" y1="6.969253125" x2="20.58161875" y2="6.9872875" layer="94"/>
<rectangle x1="2.575559375" y1="6.9872875" x2="3.063240625" y2="7.00531875" layer="94"/>
<rectangle x1="17.16531875" y1="6.9872875" x2="20.5105" y2="7.00531875" layer="94"/>
<rectangle x1="17.0561" y1="7.00531875" x2="20.436840625" y2="7.023353125" layer="94"/>
<rectangle x1="16.9291" y1="7.023353125" x2="20.347940625" y2="7.0413875" layer="94"/>
<rectangle x1="16.82241875" y1="7.0413875" x2="20.27428125" y2="7.059675" layer="94"/>
<rectangle x1="16.69541875" y1="7.059675" x2="20.18538125" y2="7.077709375" layer="94"/>
<rectangle x1="16.56841875" y1="7.077709375" x2="20.093940625" y2="7.09574375" layer="94"/>
<rectangle x1="16.44141875" y1="7.09574375" x2="20.0025" y2="7.113778125" layer="94"/>
<rectangle x1="16.296640625" y1="7.113778125" x2="19.9136" y2="7.1318125" layer="94"/>
<rectangle x1="16.151859375" y1="7.1318125" x2="19.822159375" y2="7.149846875" layer="94"/>
<rectangle x1="15.9893" y1="7.149846875" x2="19.733259375" y2="7.168134375" layer="94"/>
<rectangle x1="15.826740625" y1="7.168134375" x2="19.624040625" y2="7.18616875" layer="94"/>
<rectangle x1="15.66418125" y1="7.18616875" x2="19.5326" y2="7.204203125" layer="94"/>
<rectangle x1="15.483840625" y1="7.204203125" x2="19.4437" y2="7.2222375" layer="94"/>
<rectangle x1="15.28571875" y1="7.2222375" x2="19.33448125" y2="7.24026875" layer="94"/>
<rectangle x1="15.06728125" y1="7.24026875" x2="19.225259375" y2="7.258559375" layer="94"/>
<rectangle x1="14.85138125" y1="7.258559375" x2="19.11858125" y2="7.276590625" layer="94"/>
<rectangle x1="14.59738125" y1="7.276590625" x2="19.009359375" y2="7.294625" layer="94"/>
<rectangle x1="9.933940625" y1="7.294625" x2="10.02538125" y2="7.312659375" layer="94"/>
<rectangle x1="14.30781875" y1="7.294625" x2="18.900140625" y2="7.312659375" layer="94"/>
<rectangle x1="10.0076" y1="7.312659375" x2="10.3505" y2="7.33069375" layer="94"/>
<rectangle x1="13.96491875" y1="7.312659375" x2="18.773140625" y2="7.33069375" layer="94"/>
<rectangle x1="10.11428125" y1="7.33069375" x2="10.784840625" y2="7.348728125" layer="94"/>
<rectangle x1="13.53058125" y1="7.33069375" x2="18.64868125" y2="7.348728125" layer="94"/>
<rectangle x1="10.2235" y1="7.348728125" x2="11.43508125" y2="7.367015625" layer="94"/>
<rectangle x1="12.862559375" y1="7.348728125" x2="18.52168125" y2="7.367015625" layer="94"/>
<rectangle x1="10.3505" y1="7.367015625" x2="18.39468125" y2="7.38505" layer="94"/>
<rectangle x1="10.45718125" y1="7.38505" x2="18.26768125" y2="7.403084375" layer="94"/>
<rectangle x1="10.58418125" y1="7.403084375" x2="18.1229" y2="7.42111875" layer="94"/>
<rectangle x1="10.728959375" y1="7.42111875" x2="17.97811875" y2="7.439153125" layer="94"/>
<rectangle x1="10.855959375" y1="7.439153125" x2="17.815559375" y2="7.4571875" layer="94"/>
<rectangle x1="11.000740625" y1="7.4571875" x2="17.67078125" y2="7.475475" layer="94"/>
<rectangle x1="11.1633" y1="7.475475" x2="17.50821875" y2="7.493509375" layer="94"/>
<rectangle x1="11.325859375" y1="7.493509375" x2="17.3101" y2="7.511540625" layer="94"/>
<rectangle x1="11.5062" y1="7.511540625" x2="17.129759375" y2="7.529575" layer="94"/>
<rectangle x1="11.686540625" y1="7.529575" x2="16.9291" y2="7.547609375" layer="94"/>
<rectangle x1="11.90498125" y1="7.547609375" x2="16.7132" y2="7.56564375" layer="94"/>
<rectangle x1="12.12088125" y1="7.56564375" x2="16.4592" y2="7.58393125" layer="94"/>
<rectangle x1="12.392659375" y1="7.58393125" x2="16.189959375" y2="7.601965625" layer="94"/>
<rectangle x1="12.7" y1="7.601965625" x2="15.864840625" y2="7.62" layer="94"/>
<rectangle x1="13.09878125" y1="7.62" x2="15.44828125" y2="7.638034375" layer="94"/>
<rectangle x1="13.7668" y1="7.638034375" x2="14.780259375" y2="7.65606875" layer="94"/>
</symbol>
<symbol name="TESTPOINT">
<pin name="P$1" x="0" y="0" visible="off" length="point"/>
<circle x="0" y="0" radius="0.635" width="0.254" layer="94"/>
<text x="0" y="1.27" size="0.8128" layer="95" font="vector" ratio="15" align="center">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="FRAME-A4" prefix="FRAME">
<description>Standard A4 frame</description>
<gates>
<gate name="G$1" symbol="BORDER-A4" x="0" y="0"/>
<gate name="G$2" symbol="DATA-FIELD" x="177.8" y="0" addlevel="must"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CYTRON-LOGO" prefix="LOGO">
<description>Cytron Logo</description>
<gates>
<gate name="G$1" symbol="CYTRON-LOGO" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TESTPOINT" prefix="TP">
<description>Test Point&lt;br&gt;
&lt;br&gt;
Created by: Wai Weng</description>
<gates>
<gate name="G$1" symbol="TESTPOINT" x="0" y="0"/>
</gates>
<devices>
<device name="_(1.5MM)" package="TP-1.5MM">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="IC &amp; Transistor">
<description>ICs and Transistors.</description>
<packages>
<package name="SOT-23-6">
<description>Small Outline Transistor - 6 Pins (SOT-23-6).&lt;br&gt;
Size: 2.9 x 1.6mm&lt;br&gt;
&lt;br&gt;
Modified by: Wai Weng</description>
<smd name="4" x="1.35" y="-0.95" dx="0.6" dy="1" layer="1" roundness="50" rot="R90"/>
<smd name="6" x="1.35" y="0.95" dx="0.6" dy="1" layer="1" roundness="50" rot="R90"/>
<smd name="1" x="-1.35" y="0.95" dx="0.6" dy="1" layer="1" roundness="50" rot="R90"/>
<wire x1="-0.55" y1="-1.2" x2="-0.55" y2="0.7" width="0.2032" layer="21"/>
<wire x1="-0.55" y1="0.7" x2="-0.55" y2="1.2" width="0.2032" layer="21"/>
<wire x1="-0.55" y1="1.2" x2="0.55" y2="1.2" width="0.2032" layer="21"/>
<wire x1="-0.55" y1="-1.2" x2="0.55" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="0.55" y1="-1.2" x2="0.55" y2="0.7" width="0.2032" layer="21"/>
<text x="0" y="1.7" size="0.762" layer="25" font="vector" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.7" size="0.762" layer="27" font="vector" ratio="15" align="top-center">&gt;MPN</text>
<wire x1="0.55" y1="0.7" x2="0.55" y2="1.2" width="0.2032" layer="21"/>
<wire x1="-0.8" y1="-1.45" x2="0.8" y2="-1.45" width="0.2032" layer="51"/>
<wire x1="0.8" y1="-1.45" x2="0.8" y2="1.45" width="0.2032" layer="51"/>
<wire x1="0.8" y1="1.45" x2="-0.8" y2="1.45" width="0.2032" layer="51"/>
<wire x1="-0.8" y1="1.45" x2="-0.8" y2="-1.45" width="0.2032" layer="51"/>
<smd name="5" x="1.35" y="0" dx="0.6" dy="1" layer="1" roundness="50" rot="R90"/>
<smd name="3" x="-1.35" y="-0.95" dx="0.6" dy="1" layer="1" roundness="50" rot="R90"/>
<smd name="2" x="-1.35" y="0" dx="0.6" dy="1" layer="1" roundness="50" rot="R90"/>
<wire x1="-0.55" y1="0.7" x2="0.55" y2="0.7" width="0.2032" layer="21"/>
<rectangle x1="0.8" y1="-0.2" x2="1.4" y2="0.2" layer="51"/>
<rectangle x1="-1.4" y1="-0.2" x2="-0.8" y2="0.2" layer="51"/>
<rectangle x1="0.8" y1="0.75" x2="1.4" y2="1.15" layer="51"/>
<rectangle x1="0.8" y1="-1.15" x2="1.4" y2="-0.75" layer="51"/>
<rectangle x1="-1.4" y1="-1.15" x2="-0.8" y2="-0.75" layer="51"/>
<rectangle x1="-1.4" y1="0.75" x2="-0.8" y2="1.15" layer="51"/>
</package>
<package name="SOT-23">
<description>Small Outline Transistor (SOT-23)</description>
<smd name="1" x="1.2" y="-0.95" dx="0.7" dy="0.8" layer="1" roundness="50" rot="R90"/>
<smd name="2" x="1.2" y="0.95" dx="0.7" dy="0.8" layer="1" roundness="50" rot="R90"/>
<smd name="3" x="-1.2" y="0" dx="0.7" dy="0.8" layer="1" roundness="50" rot="R90"/>
<wire x1="-0.35" y1="-1.2" x2="-0.35" y2="1.2" width="0.2032" layer="21"/>
<wire x1="-0.35" y1="1.2" x2="0.35" y2="1.2" width="0.2032" layer="21"/>
<wire x1="-0.35" y1="-1.2" x2="0.35" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="0.35" y1="-1.2" x2="0.35" y2="1.2" width="0.2032" layer="21"/>
<text x="0" y="1.7" size="0.762" layer="25" font="vector" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.7" size="0.762" layer="27" font="vector" ratio="15" align="top-center">&gt;MPN</text>
<wire x1="-0.65" y1="-1.45" x2="0.65" y2="-1.45" width="0.2032" layer="51"/>
<wire x1="0.65" y1="-1.45" x2="0.65" y2="1.45" width="0.2032" layer="51"/>
<wire x1="0.65" y1="1.45" x2="-0.65" y2="1.45" width="0.2032" layer="51"/>
<wire x1="-0.65" y1="1.45" x2="-0.65" y2="-1.45" width="0.2032" layer="51"/>
<rectangle x1="-1.125" y1="-0.275" x2="-0.725" y2="0.275" layer="51" rot="R90"/>
<rectangle x1="0.725" y1="0.675" x2="1.125" y2="1.225" layer="51" rot="R90"/>
<rectangle x1="0.725" y1="-1.225" x2="1.125" y2="-0.675" layer="51" rot="R90"/>
<polygon width="0.2032" layer="200">
<vertex x="-0.65" y="1.45"/>
<vertex x="0.65" y="1.45"/>
<vertex x="0.65" y="-1.45"/>
<vertex x="-0.65" y="-1.45"/>
</polygon>
</package>
<package name="TO-92">
<description>TO-92 (Through Hole)&lt;br&gt;
&lt;br&gt;
Created by: Wai Weng</description>
<pad name="1" x="-2.54" y="0" drill="0.8"/>
<pad name="2" x="0" y="0" drill="0.8"/>
<pad name="3" x="2.54" y="0" drill="0.8"/>
<wire x1="1.7" y1="1.6" x2="-1.7" y2="1.6" width="0.2032" layer="51" curve="-266.528591"/>
<wire x1="-1.7" y1="1.6" x2="1.7" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="1.6" x2="2.2" y2="0.8" width="0.2032" layer="21" curve="-23.281189"/>
<wire x1="-2.2" y1="0.8" x2="-1.7" y2="1.6" width="0.2032" layer="21" curve="-23.281189"/>
<wire x1="-1.7" y1="1.6" x2="1.7" y2="1.6" width="0.2032" layer="21"/>
<wire x1="2.2" y1="-0.8" x2="-2.2" y2="-0.8" width="0.2032" layer="21" curve="-140.033787"/>
<text x="0" y="3" size="0.762" layer="25" font="vector" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="0" y="2" size="0.762" layer="27" font="vector" ratio="15" align="bottom-center">&gt;MPN</text>
</package>
<package name="DPAK">
<description>DPAK (TO-252)&lt;br&gt;
&lt;br&gt;
&lt;b&gt;&lt;i&gt;* Need to verify the Pick &amp; Place origin.&lt;/i&gt;&lt;/b&gt;</description>
<smd name="1" x="-3.425" y="2.285" dx="2" dy="1.3" layer="1" roundness="30"/>
<smd name="3" x="-3.425" y="-2.285" dx="2" dy="1.3" layer="1" roundness="30"/>
<smd name="4" x="3.6" y="0" dx="6.5" dy="5.7" layer="1" roundness="10"/>
<text x="-1.2" y="-3.6" size="0.762" layer="27" font="vector" ratio="15" align="top-left">&gt;MPN</text>
<text x="-1.2" y="3.6" size="0.762" layer="25" font="vector" ratio="15">&gt;Name</text>
<rectangle x1="-4" y1="1.785" x2="-1.2" y2="2.785" layer="51"/>
<rectangle x1="-4" y1="-2.785" x2="-1.2" y2="-1.785" layer="51"/>
<rectangle x1="-2.6" y1="-0.5" x2="-1.2" y2="0.5" layer="51"/>
<wire x1="-1.2" y1="3.3" x2="-1.2" y2="-3.3" width="0.2032" layer="51"/>
<wire x1="-1.2" y1="-3.3" x2="4.8" y2="-3.3" width="0.2032" layer="51"/>
<wire x1="4.8" y1="-3.3" x2="4.8" y2="3.3" width="0.2032" layer="51"/>
<wire x1="4.8" y1="3.3" x2="-1.2" y2="3.3" width="0.2032" layer="51"/>
<rectangle x1="0.7" y1="-2.65" x2="6" y2="2.65" layer="51"/>
<wire x1="4.8" y1="3.1" x2="4.8" y2="3.3" width="0.2032" layer="21"/>
<wire x1="4.8" y1="3.3" x2="-1.2" y2="3.3" width="0.2032" layer="21"/>
<wire x1="-1.2" y1="3.3" x2="-1.2" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="-1.2" y1="-3.3" x2="4.8" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="4.8" y1="-3.3" x2="4.8" y2="-3.1" width="0.2032" layer="21"/>
<polygon width="0.2032" layer="200">
<vertex x="-1.2" y="3.3"/>
<vertex x="4.8" y="3.3"/>
<vertex x="4.8" y="-3.3"/>
<vertex x="-1.2" y="-3.3"/>
</polygon>
</package>
<package name="POWERPAK-1212-8-SINGLE">
<smd name="1" x="-1.65" y="0.975" dx="1.2" dy="0.42" layer="1" roundness="50"/>
<smd name="2" x="-1.65" y="0.325" dx="1.2" dy="0.42" layer="1" roundness="50"/>
<smd name="3" x="-1.65" y="-0.325" dx="1.2" dy="0.42" layer="1" roundness="50"/>
<smd name="4" x="-1.65" y="-0.975" dx="1.2" dy="0.42" layer="1" roundness="50"/>
<smd name="8" x="1.65" y="0.975" dx="1.2" dy="0.42" layer="1" roundness="50"/>
<smd name="7" x="1.65" y="0.325" dx="1.2" dy="0.42" layer="1" roundness="50"/>
<smd name="6" x="1.65" y="-0.325" dx="1.2" dy="0.42" layer="1" roundness="50"/>
<smd name="5" x="1.65" y="-0.975" dx="1.2" dy="0.42" layer="1" roundness="50"/>
<smd name="9" x="0.575" y="0" dx="1.95" dy="3" layer="1" roundness="50"/>
<wire x1="-1.65" y1="1.65" x2="1.65" y2="1.65" width="0.2" layer="51"/>
<wire x1="-1.65" y1="-1.65" x2="1.65" y2="-1.65" width="0.2" layer="51"/>
<wire x1="-1.65" y1="-1.65" x2="-1.65" y2="1.65" width="0.2" layer="51"/>
<wire x1="1.65" y1="-1.65" x2="1.65" y2="1.65" width="0.2" layer="51"/>
<circle x="-1.35" y="1.375" radius="0.05" width="0.1" layer="51"/>
<wire x1="-1.65" y1="1.7145" x2="1.65" y2="1.7145" width="0.2" layer="21"/>
<wire x1="-1.65" y1="-1.7145" x2="1.65" y2="-1.7145" width="0.2" layer="21"/>
<wire x1="1.65" y1="1.7145" x2="1.65" y2="1.4351" width="0.2" layer="21"/>
<wire x1="-1.65" y1="1.4351" x2="-1.65" y2="1.7145" width="0.2" layer="21"/>
<wire x1="1.65" y1="-1.4351" x2="1.65" y2="-1.7145" width="0.2" layer="21"/>
<wire x1="-1.65" y1="-1.7145" x2="-1.65" y2="-1.4351" width="0.2" layer="21"/>
<circle x="-1.35" y="1.4224" radius="0.05" width="0.1" layer="21"/>
</package>
<package name="TO-263-2">
<description>TO-263 Package&lt;br&gt;
&lt;br&gt;&lt;br&gt;
Created by: Wai Weng&lt;br&gt;</description>
<rectangle x1="0.825" y1="-5.3" x2="7.175" y2="5.3" layer="51"/>
<wire x1="4" y1="5.205" x2="-2.985" y2="5.205" width="0.2032" layer="51"/>
<wire x1="-2.985" y1="5.205" x2="-2.985" y2="-5.205" width="0.2032" layer="51"/>
<wire x1="-2.985" y1="-5.205" x2="4" y2="-5.205" width="0.2032" layer="51"/>
<rectangle x1="-5.34" y1="-0.5" x2="-3" y2="0.5" layer="51"/>
<rectangle x1="-8.48" y1="2.04" x2="-3" y2="3.04" layer="51"/>
<rectangle x1="-8.48" y1="-3.04" x2="-3" y2="-2.04" layer="51"/>
<smd name="4" x="2.3" y="0" dx="11" dy="10.16" layer="1" roundness="10"/>
<smd name="1" x="-7.2" y="2.54" dx="4" dy="1.524" layer="1" roundness="30"/>
<smd name="3" x="-7.2" y="-2.54" dx="4" dy="1.524" layer="1" roundness="30"/>
<wire x1="-4.8" y1="2.54" x2="-3.5" y2="2.54" width="0.2032" layer="21"/>
<wire x1="-4.8" y1="0" x2="-3.5" y2="0" width="0.2032" layer="21"/>
<wire x1="-4.8" y1="-2.54" x2="-3.5" y2="-2.54" width="0.2032" layer="21"/>
<text x="-3" y="6" size="0.762" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-3" y="-6.8" size="0.762" layer="27" font="vector" ratio="15">&gt;MPN</text>
</package>
<package name="SO-8">
<description>SO-8</description>
<wire x1="2.4" y1="1.773" x2="2.4" y2="-1.527" width="0.2032" layer="51"/>
<wire x1="2.4" y1="-1.527" x2="2.4" y2="-2.027" width="0.2032" layer="51"/>
<wire x1="2.4" y1="-2.027" x2="-2.4" y2="-2.027" width="0.2032" layer="51"/>
<wire x1="-2.4" y1="-2.027" x2="-2.4" y2="-1.527" width="0.2032" layer="51"/>
<wire x1="-2.4" y1="-1.527" x2="-2.4" y2="1.773" width="0.2032" layer="51"/>
<wire x1="-2.4" y1="1.773" x2="2.4" y2="1.773" width="0.2032" layer="51"/>
<wire x1="2.4" y1="-1.527" x2="-2.4" y2="-1.527" width="0.2032" layer="51"/>
<smd name="6" x="-0.635" y="-2.473" dx="0.7112" dy="1.778" layer="1"/>
<smd name="3" x="-0.635" y="2.219" dx="0.7112" dy="1.778" layer="1"/>
<smd name="5" x="-1.905" y="-2.473" dx="0.7112" dy="1.778" layer="1"/>
<smd name="7" x="0.635" y="-2.473" dx="0.7112" dy="1.778" layer="1"/>
<smd name="8" x="1.905" y="-2.473" dx="0.7112" dy="1.778" layer="1"/>
<smd name="4" x="-1.905" y="2.219" dx="0.7112" dy="1.778" layer="1"/>
<smd name="2" x="0.635" y="2.219" dx="0.7112" dy="1.778" layer="1"/>
<smd name="1" x="1.905" y="2.219" dx="0.7112" dy="1.778" layer="1"/>
<text x="0" y="3.683" size="0.762" layer="25" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="0" y="-4.699" size="0.762" layer="27" ratio="15" align="bottom-center">&gt;VALUE</text>
<rectangle x1="-2.1501" y1="-3.2271" x2="-1.6599" y2="-2.127" layer="51"/>
<rectangle x1="-0.8801" y1="-3.2271" x2="-0.3899" y2="-2.127" layer="51"/>
<rectangle x1="0.3899" y1="-3.2271" x2="0.8801" y2="-2.127" layer="51"/>
<rectangle x1="1.6599" y1="-3.2271" x2="2.1501" y2="-2.127" layer="51"/>
<rectangle x1="1.6599" y1="1.873" x2="2.1501" y2="2.9731" layer="51"/>
<rectangle x1="0.3899" y1="1.873" x2="0.8801" y2="2.9731" layer="51"/>
<rectangle x1="-0.8801" y1="1.873" x2="-0.3899" y2="2.9731" layer="51"/>
<rectangle x1="-2.1501" y1="1.873" x2="-1.6599" y2="2.9731" layer="51"/>
<wire x1="-2.54" y1="1.778" x2="-2.54" y2="-2.032" width="0.2032" layer="21"/>
<wire x1="2.54" y1="1.778" x2="2.54" y2="-2.032" width="0.2032" layer="21"/>
<circle x="3.175" y="2.286" radius="0.359209375" width="0" layer="21"/>
<wire x1="-2.032" y1="1.016" x2="-2.032" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-2.032" y1="-1.27" x2="1.524" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.524" y1="-1.27" x2="2.032" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="2.032" y1="-1.27" x2="2.032" y2="1.016" width="0.2032" layer="21"/>
<wire x1="2.032" y1="1.016" x2="1.524" y2="1.016" width="0.2032" layer="21"/>
<wire x1="1.524" y1="1.016" x2="-2.032" y2="1.016" width="0.2032" layer="21"/>
<wire x1="1.524" y1="1.016" x2="1.524" y2="-1.27" width="0.2032" layer="21"/>
<circle x="3.175" y="2.286" radius="0.359209375" width="0" layer="51"/>
</package>
<package name="SOIC-16N">
<description>Small Outline Integrated Circuit (SOIC)&lt;br&gt;
Narrow (JEDEC) SOIC Package&lt;br&gt;
&lt;br&gt;
Size: 9.90 x 3.90mm / 10.55 x 4.4mm&lt;br&gt;
Lead Pitch: 1.27mm&lt;br&gt;
&lt;br&gt;
Created by: OoiBP&lt;br&gt;</description>
<wire x1="-5.275" y1="2.2" x2="-5.275" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="-5.275" y1="-2.2" x2="5.275" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="5.275" y1="-2.2" x2="5.275" y2="2.2" width="0.2032" layer="51"/>
<wire x1="5.275" y1="2.2" x2="-5.275" y2="2.2" width="0.2032" layer="51"/>
<circle x="-4" y="-1" radius="0.5099" width="0.2032" layer="51"/>
<wire x1="-4.7" y1="1.7" x2="-4.7" y2="-1.7" width="0.2032" layer="21"/>
<wire x1="-4.7" y1="-1.7" x2="4.7" y2="-1.7" width="0.2032" layer="21"/>
<wire x1="4.7" y1="-1.7" x2="4.7" y2="1.7" width="0.2032" layer="21"/>
<wire x1="4.7" y1="1.7" x2="-4.7" y2="1.7" width="0.2032" layer="21"/>
<smd name="1" x="-4.445" y="-3" dx="0.6" dy="1.7" layer="1" roundness="50"/>
<smd name="2" x="-3.175" y="-3" dx="0.6" dy="1.7" layer="1" roundness="50"/>
<smd name="3" x="-1.905" y="-3" dx="0.6" dy="1.7" layer="1" roundness="50"/>
<smd name="4" x="-0.635" y="-3" dx="0.6" dy="1.7" layer="1" roundness="50"/>
<smd name="5" x="0.635" y="-3" dx="0.6" dy="1.7" layer="1" roundness="50"/>
<smd name="6" x="1.905" y="-3" dx="0.6" dy="1.7" layer="1" roundness="50"/>
<smd name="7" x="3.175" y="-3" dx="0.6" dy="1.7" layer="1" roundness="50"/>
<smd name="8" x="4.445" y="-3" dx="0.6" dy="1.7" layer="1" roundness="50"/>
<smd name="9" x="4.445" y="3" dx="0.6" dy="1.7" layer="1" roundness="50"/>
<smd name="10" x="3.175" y="3" dx="0.6" dy="1.7" layer="1" roundness="50"/>
<smd name="11" x="1.905" y="3" dx="0.6" dy="1.7" layer="1" roundness="50"/>
<smd name="12" x="0.635" y="3" dx="0.6" dy="1.7" layer="1" roundness="50"/>
<smd name="13" x="-0.635" y="3" dx="0.6" dy="1.7" layer="1" roundness="50"/>
<smd name="14" x="-1.905" y="3" dx="0.6" dy="1.7" layer="1" roundness="50"/>
<smd name="15" x="-3.175" y="3" dx="0.6" dy="1.7" layer="1" roundness="50"/>
<smd name="16" x="-4.445" y="3" dx="0.6" dy="1.7" layer="1" roundness="50"/>
<rectangle x1="-4.65" y1="2.2" x2="-4.24" y2="3.5" layer="51"/>
<rectangle x1="-3.38" y1="2.2" x2="-2.97" y2="3.5" layer="51"/>
<rectangle x1="-2.11" y1="2.2" x2="-1.7" y2="3.5" layer="51"/>
<rectangle x1="-0.84" y1="2.2" x2="-0.43" y2="3.5" layer="51"/>
<rectangle x1="0.43" y1="2.2" x2="0.84" y2="3.5" layer="51"/>
<rectangle x1="1.7" y1="2.2" x2="2.11" y2="3.5" layer="51"/>
<rectangle x1="2.97" y1="2.2" x2="3.38" y2="3.5" layer="51"/>
<rectangle x1="4.24" y1="2.2" x2="4.65" y2="3.5" layer="51"/>
<rectangle x1="-4.65" y1="-3.5" x2="-4.24" y2="-2.2" layer="51"/>
<rectangle x1="-3.38" y1="-3.5" x2="-2.97" y2="-2.2" layer="51"/>
<rectangle x1="-2.11" y1="-3.5" x2="-1.7" y2="-2.2" layer="51"/>
<rectangle x1="-0.84" y1="-3.5" x2="-0.43" y2="-2.2" layer="51"/>
<rectangle x1="0.43" y1="-3.5" x2="0.84" y2="-2.2" layer="51"/>
<rectangle x1="1.7" y1="-3.5" x2="2.11" y2="-2.2" layer="51"/>
<rectangle x1="2.97" y1="-3.5" x2="3.38" y2="-2.2" layer="51"/>
<rectangle x1="4.24" y1="-3.5" x2="4.65" y2="-2.2" layer="51"/>
<text x="-6.4" y="-2" size="0.8128" layer="25" ratio="15" rot="R90" align="top-left">&gt;NAME</text>
<text x="6.2" y="-2" size="0.8128" layer="27" ratio="15" rot="R90">&gt;MPN</text>
<circle x="-4" y="-1" radius="0.14141875" width="0.3048" layer="21"/>
</package>
<package name="TO-220">
<description>TO-220 (Through Hole)</description>
<pad name="1" x="-2.54" y="0" drill="1.143" diameter="1.651" shape="long" rot="R90"/>
<pad name="2" x="0" y="0" drill="1.143" diameter="1.651" shape="long" rot="R90"/>
<pad name="3" x="2.54" y="0" drill="1.143" diameter="1.651" shape="long" rot="R90"/>
<wire x1="-5.3" y1="3.2" x2="5.3" y2="3.2" width="0.2032" layer="51"/>
<wire x1="5.3" y1="3.2" x2="5.3" y2="2" width="0.2032" layer="51"/>
<wire x1="5.3" y1="2" x2="5" y2="-2" width="0.2032" layer="51"/>
<wire x1="5" y1="-2" x2="-5" y2="-2" width="0.2032" layer="51"/>
<wire x1="-5" y1="-2" x2="-5.3" y2="2" width="0.2032" layer="51"/>
<wire x1="-5.3" y1="2" x2="-5.3" y2="3.2" width="0.2032" layer="51"/>
<wire x1="-5.3" y1="2" x2="5.3" y2="2" width="0.2032" layer="51"/>
<wire x1="-5.3" y1="3.2" x2="5.3" y2="3.2" width="0.2032" layer="21"/>
<wire x1="5.3" y1="3.2" x2="5.3" y2="2" width="0.2032" layer="21"/>
<wire x1="5.3" y1="2" x2="5" y2="-2" width="0.2032" layer="21"/>
<wire x1="5" y1="-2" x2="-5" y2="-2" width="0.2032" layer="21"/>
<wire x1="-5" y1="-2" x2="-5.3" y2="2" width="0.2032" layer="21"/>
<wire x1="-5.3" y1="2" x2="-5.3" y2="3.2" width="0.2032" layer="21"/>
<wire x1="-5.3" y1="2" x2="5.3" y2="2" width="0.2032" layer="21"/>
<text x="-5.3" y="3.6" size="0.762" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-5.3" y="-3.1" size="0.762" layer="27" font="vector" ratio="15">&gt;VALUE</text>
</package>
<package name="D2PAK-7">
<description>D2PAK-7 Package&lt;br&gt;
&lt;br&gt;
Created by: Idris</description>
<rectangle x1="0.825" y1="-5.3" x2="7.175" y2="5.3" layer="51"/>
<wire x1="4" y1="5.205" x2="-2.985" y2="5.205" width="0.2032" layer="51"/>
<wire x1="-2.985" y1="5.205" x2="-2.985" y2="-5.205" width="0.2032" layer="51"/>
<wire x1="-2.985" y1="-5.205" x2="4" y2="-5.205" width="0.2032" layer="51"/>
<rectangle x1="-5.34" y1="-0.5" x2="-3" y2="0.5" layer="51"/>
<rectangle x1="-8.48" y1="2.04" x2="-3" y2="3.04" layer="51"/>
<rectangle x1="-8.48" y1="-3.04" x2="-3" y2="-2.04" layer="51"/>
<smd name="D" x="2.3" y="0" dx="11" dy="10.16" layer="1" roundness="10"/>
<smd name="S1" x="-7.2" y="2.54" dx="4" dy="1.016" layer="1" roundness="30"/>
<smd name="S4" x="-7.2" y="-2.54" dx="4" dy="1.016" layer="1" roundness="30"/>
<wire x1="-4.8" y1="2.54" x2="-3.5" y2="2.54" width="0.2032" layer="21"/>
<wire x1="-4.8" y1="0" x2="-3.5" y2="0" width="0.2032" layer="21"/>
<wire x1="-4.8" y1="-2.54" x2="-3.5" y2="-2.54" width="0.2032" layer="21"/>
<text x="-3" y="6" size="0.762" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-3" y="-6.8" size="0.762" layer="27" font="vector" ratio="15">&gt;MPN</text>
<rectangle x1="-8.48" y1="0.77" x2="-3" y2="1.77" layer="51"/>
<rectangle x1="-8.48" y1="-1.77" x2="-3" y2="-0.77" layer="51"/>
<rectangle x1="-8.48" y1="-4.31" x2="-3" y2="-3.31" layer="51"/>
<rectangle x1="-8.48" y1="3.31" x2="-3" y2="4.31" layer="51"/>
<wire x1="-4.8" y1="1.27" x2="-3.5" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-4.8" y1="3.81" x2="-3.5" y2="3.81" width="0.2032" layer="21"/>
<wire x1="-4.8" y1="-1.27" x2="-3.5" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-4.8" y1="-3.81" x2="-3.5" y2="-3.81" width="0.2032" layer="21"/>
<smd name="G" x="-7.2" y="3.81" dx="4" dy="1.016" layer="1" roundness="30"/>
<smd name="S2" x="-7.2" y="1.27" dx="4" dy="1.016" layer="1" roundness="30"/>
<smd name="S3" x="-7.2" y="-1.27" dx="4" dy="1.016" layer="1" roundness="30"/>
<smd name="S5" x="-7.2" y="-3.81" dx="4" dy="1.016" layer="1" roundness="30"/>
</package>
<package name="SOT-23-5">
<description>Small Outline Transistor - 5 Pins (SOT-23-5)</description>
<smd name="1" x="-1.35" y="0.95" dx="0.6" dy="1" layer="1" roundness="50" rot="R270"/>
<smd name="3" x="-1.35" y="-0.95" dx="0.6" dy="1" layer="1" roundness="50" rot="R270"/>
<smd name="4" x="1.35" y="-0.95" dx="0.6" dy="1" layer="1" roundness="50" rot="R270"/>
<text x="0" y="1.7" size="0.762" layer="25" font="vector" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.7" size="0.762" layer="27" font="vector" ratio="15" align="top-center">&gt;MPN</text>
<smd name="2" x="-1.35" y="0" dx="0.6" dy="1" layer="1" roundness="50" rot="R270"/>
<smd name="5" x="1.35" y="0.95" dx="0.6" dy="1" layer="1" roundness="50" rot="R270"/>
<wire x1="-0.55" y1="-1.2" x2="-0.55" y2="0.7" width="0.2032" layer="21"/>
<wire x1="-0.55" y1="0.7" x2="-0.55" y2="1.2" width="0.2032" layer="21"/>
<wire x1="-0.55" y1="1.2" x2="0.55" y2="1.2" width="0.2032" layer="21"/>
<wire x1="-0.55" y1="-1.2" x2="0.55" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="0.55" y1="-1.2" x2="0.55" y2="0.7" width="0.2032" layer="21"/>
<wire x1="0.55" y1="0.7" x2="0.55" y2="1.2" width="0.2032" layer="21"/>
<wire x1="-0.8" y1="-1.45" x2="0.8" y2="-1.45" width="0.2032" layer="51"/>
<wire x1="0.8" y1="-1.45" x2="0.8" y2="1.45" width="0.2032" layer="51"/>
<wire x1="0.8" y1="1.45" x2="-0.8" y2="1.45" width="0.2032" layer="51"/>
<wire x1="-0.8" y1="1.45" x2="-0.8" y2="-1.45" width="0.2032" layer="51"/>
<wire x1="-0.55" y1="0.7" x2="0.55" y2="0.7" width="0.2032" layer="21"/>
<rectangle x1="-1.4" y1="-0.2" x2="-0.8" y2="0.2" layer="51"/>
<rectangle x1="0.8" y1="0.75" x2="1.4" y2="1.15" layer="51"/>
<rectangle x1="0.8" y1="-1.15" x2="1.4" y2="-0.75" layer="51"/>
<rectangle x1="-1.4" y1="-1.15" x2="-0.8" y2="-0.75" layer="51"/>
<rectangle x1="-1.4" y1="0.75" x2="-0.8" y2="1.15" layer="51"/>
</package>
<package name="TQFP-44-08">
<description>Thin Plasic Quad Flat Package (TQFP)&lt;br&gt;
Pin Pitch: 0.8mm</description>
<wire x1="-4.6" y1="4.3" x2="-4.3" y2="4.6" width="0.2032" layer="21"/>
<wire x1="-4.3" y1="4.6" x2="4.3" y2="4.6" width="0.2032" layer="21"/>
<wire x1="4.3" y1="4.6" x2="4.6" y2="4.3" width="0.2032" layer="21"/>
<wire x1="4.6" y1="4.3" x2="4.6" y2="-4.3" width="0.2032" layer="21"/>
<wire x1="4.6" y1="-4.3" x2="4.3" y2="-4.6" width="0.2032" layer="21"/>
<wire x1="4.3" y1="-4.6" x2="-4.3" y2="-4.6" width="0.2032" layer="21"/>
<wire x1="-4.3" y1="-4.6" x2="-4.6" y2="-4.3" width="0.2032" layer="21"/>
<wire x1="-4.6" y1="-4.3" x2="-4.6" y2="4.3" width="0.2032" layer="21"/>
<circle x="-4" y="4" radius="0.2827" width="0.254" layer="51"/>
<smd name="1" x="-5.8" y="4" dx="1.5" dy="0.5" layer="1" roundness="50"/>
<smd name="2" x="-5.8" y="3.2" dx="1.5" dy="0.5" layer="1" roundness="50"/>
<smd name="3" x="-5.8" y="2.4" dx="1.5" dy="0.5" layer="1" roundness="50"/>
<smd name="4" x="-5.8" y="1.6" dx="1.5" dy="0.5" layer="1" roundness="50"/>
<smd name="5" x="-5.8" y="0.8" dx="1.5" dy="0.5" layer="1" roundness="50"/>
<smd name="6" x="-5.8" y="0" dx="1.5" dy="0.5" layer="1" roundness="50"/>
<smd name="7" x="-5.8" y="-0.8" dx="1.5" dy="0.5" layer="1" roundness="50"/>
<smd name="8" x="-5.8" y="-1.6" dx="1.5" dy="0.5" layer="1" roundness="50"/>
<smd name="9" x="-5.8" y="-2.4" dx="1.5" dy="0.5" layer="1" roundness="50"/>
<smd name="10" x="-5.8" y="-3.2" dx="1.5" dy="0.5" layer="1" roundness="50"/>
<smd name="11" x="-5.8" y="-4" dx="1.5" dy="0.5" layer="1" roundness="50"/>
<smd name="12" x="-4" y="-5.8" dx="0.5" dy="1.5" layer="1" roundness="50"/>
<smd name="13" x="-3.2" y="-5.8" dx="0.5" dy="1.5" layer="1" roundness="50"/>
<smd name="14" x="-2.4" y="-5.8" dx="0.5" dy="1.5" layer="1" roundness="50"/>
<smd name="15" x="-1.6" y="-5.8" dx="0.5" dy="1.5" layer="1" roundness="50"/>
<smd name="16" x="-0.8" y="-5.8" dx="0.5" dy="1.5" layer="1" roundness="50"/>
<smd name="17" x="0" y="-5.8" dx="0.5" dy="1.5" layer="1" roundness="50"/>
<smd name="18" x="0.8" y="-5.8" dx="0.5" dy="1.5" layer="1" roundness="50"/>
<smd name="19" x="1.6" y="-5.8" dx="0.5" dy="1.5" layer="1" roundness="50"/>
<smd name="20" x="2.4" y="-5.8" dx="0.5" dy="1.5" layer="1" roundness="50"/>
<smd name="21" x="3.2" y="-5.8" dx="0.5" dy="1.5" layer="1" roundness="50"/>
<smd name="22" x="4" y="-5.8" dx="0.5" dy="1.5" layer="1" roundness="50"/>
<smd name="23" x="5.8" y="-4" dx="1.5" dy="0.5" layer="1" roundness="50"/>
<smd name="24" x="5.8" y="-3.2" dx="1.5" dy="0.5" layer="1" roundness="50"/>
<smd name="25" x="5.8" y="-2.4" dx="1.5" dy="0.5" layer="1" roundness="50"/>
<smd name="26" x="5.8" y="-1.6" dx="1.5" dy="0.5" layer="1" roundness="50"/>
<smd name="27" x="5.8" y="-0.8" dx="1.5" dy="0.5" layer="1" roundness="50"/>
<smd name="28" x="5.8" y="0" dx="1.5" dy="0.5" layer="1" roundness="50"/>
<smd name="29" x="5.8" y="0.8" dx="1.5" dy="0.5" layer="1" roundness="50"/>
<smd name="30" x="5.8" y="1.6" dx="1.5" dy="0.5" layer="1" roundness="50"/>
<smd name="31" x="5.8" y="2.4" dx="1.5" dy="0.5" layer="1" roundness="50"/>
<smd name="32" x="5.8" y="3.2" dx="1.5" dy="0.5" layer="1" roundness="50"/>
<smd name="33" x="5.8" y="4" dx="1.5" dy="0.5" layer="1" roundness="50"/>
<smd name="34" x="4" y="5.8" dx="0.5" dy="1.5" layer="1" roundness="50"/>
<smd name="35" x="3.2" y="5.8" dx="0.5" dy="1.5" layer="1" roundness="50"/>
<smd name="36" x="2.4" y="5.8" dx="0.5" dy="1.5" layer="1" roundness="50"/>
<smd name="37" x="1.6" y="5.8" dx="0.5" dy="1.5" layer="1" roundness="50"/>
<smd name="38" x="0.8" y="5.8" dx="0.5" dy="1.5" layer="1" roundness="50"/>
<smd name="39" x="0" y="5.8" dx="0.5" dy="1.5" layer="1" roundness="50"/>
<smd name="40" x="-0.8" y="5.8" dx="0.5" dy="1.5" layer="1" roundness="50"/>
<smd name="41" x="-1.6" y="5.8" dx="0.5" dy="1.5" layer="1" roundness="50"/>
<smd name="42" x="-2.4" y="5.8" dx="0.5" dy="1.5" layer="1" roundness="50"/>
<smd name="43" x="-3.2" y="5.8" dx="0.5" dy="1.5" layer="1" roundness="50"/>
<smd name="44" x="-4" y="5.8" dx="0.5" dy="1.5" layer="1" roundness="50"/>
<text x="-4" y="-3" size="0.762" layer="25" ratio="15">&gt;NAME</text>
<text x="-4" y="-4" size="0.762" layer="27" ratio="15">&gt;MPN</text>
<rectangle x1="-6.1001" y1="3.8001" x2="-4.95" y2="4.1999" layer="51"/>
<rectangle x1="-6.1001" y1="3" x2="-4.95" y2="3.4" layer="51"/>
<rectangle x1="-6.1001" y1="2.1999" x2="-4.95" y2="2.5999" layer="51"/>
<rectangle x1="-6.1001" y1="1.4" x2="-4.95" y2="1.8001" layer="51"/>
<rectangle x1="-6.1001" y1="0.5999" x2="-4.95" y2="1" layer="51"/>
<rectangle x1="-6.1001" y1="-0.1999" x2="-4.95" y2="0.1999" layer="51"/>
<rectangle x1="-6.1001" y1="-1" x2="-4.95" y2="-0.5999" layer="51"/>
<rectangle x1="-6.1001" y1="-1.8001" x2="-4.95" y2="-1.4" layer="51"/>
<rectangle x1="-6.1001" y1="-2.5999" x2="-4.95" y2="-2.1999" layer="51"/>
<rectangle x1="-6.1001" y1="-3.4" x2="-4.95" y2="-3" layer="51"/>
<rectangle x1="-6.1001" y1="-4.1999" x2="-4.95" y2="-3.8001" layer="51"/>
<rectangle x1="-4.1999" y1="-6.1001" x2="-3.8001" y2="-4.95" layer="51"/>
<rectangle x1="-3.4" y1="-6.1001" x2="-3" y2="-4.95" layer="51"/>
<rectangle x1="-2.5999" y1="-6.1001" x2="-2.1999" y2="-4.95" layer="51"/>
<rectangle x1="-1.8001" y1="-6.1001" x2="-1.4" y2="-4.95" layer="51"/>
<rectangle x1="-1" y1="-6.1001" x2="-0.5999" y2="-4.95" layer="51"/>
<rectangle x1="-0.1999" y1="-6.1001" x2="0.1999" y2="-4.95" layer="51"/>
<rectangle x1="0.5999" y1="-6.1001" x2="1" y2="-4.95" layer="51"/>
<rectangle x1="1.4" y1="-6.1001" x2="1.8001" y2="-4.95" layer="51"/>
<rectangle x1="2.1999" y1="-6.1001" x2="2.5999" y2="-4.95" layer="51"/>
<rectangle x1="3" y1="-6.1001" x2="3.4" y2="-4.95" layer="51"/>
<rectangle x1="3.8001" y1="-6.1001" x2="4.1999" y2="-4.95" layer="51"/>
<rectangle x1="4.95" y1="-4.1999" x2="6.1001" y2="-3.8001" layer="51"/>
<rectangle x1="4.95" y1="-3.4" x2="6.1001" y2="-3" layer="51"/>
<rectangle x1="4.95" y1="-2.5999" x2="6.1001" y2="-2.1999" layer="51"/>
<rectangle x1="4.95" y1="-1.8001" x2="6.1001" y2="-1.4" layer="51"/>
<rectangle x1="4.95" y1="-1" x2="6.1001" y2="-0.5999" layer="51"/>
<rectangle x1="4.95" y1="-0.1999" x2="6.1001" y2="0.1999" layer="51"/>
<rectangle x1="4.95" y1="0.5999" x2="6.1001" y2="1" layer="51"/>
<rectangle x1="4.95" y1="1.4" x2="6.1001" y2="1.8001" layer="51"/>
<rectangle x1="4.95" y1="2.1999" x2="6.1001" y2="2.5999" layer="51"/>
<rectangle x1="4.95" y1="3" x2="6.1001" y2="3.4" layer="51"/>
<rectangle x1="4.95" y1="3.8001" x2="6.1001" y2="4.1999" layer="51"/>
<rectangle x1="3.8001" y1="4.95" x2="4.1999" y2="6.1001" layer="51"/>
<rectangle x1="3" y1="4.95" x2="3.4" y2="6.1001" layer="51"/>
<rectangle x1="2.1999" y1="4.95" x2="2.5999" y2="6.1001" layer="51"/>
<rectangle x1="1.4" y1="4.95" x2="1.8001" y2="6.1001" layer="51"/>
<rectangle x1="0.5999" y1="4.95" x2="1" y2="6.1001" layer="51"/>
<rectangle x1="-0.1999" y1="4.95" x2="0.1999" y2="6.1001" layer="51"/>
<rectangle x1="-1" y1="4.95" x2="-0.5999" y2="6.1001" layer="51"/>
<rectangle x1="-1.8001" y1="4.95" x2="-1.4" y2="6.1001" layer="51"/>
<rectangle x1="-2.5999" y1="4.95" x2="-2.1999" y2="6.1001" layer="51"/>
<rectangle x1="-3.4" y1="4.95" x2="-3" y2="6.1001" layer="51"/>
<rectangle x1="-4.1999" y1="4.95" x2="-3.8001" y2="6.1001" layer="51"/>
<wire x1="5" y1="-4.4901" x2="5" y2="4.5" width="0.2032" layer="51"/>
<wire x1="5" y1="4.5" x2="4.5" y2="5" width="0.2032" layer="51"/>
<wire x1="4.5" y1="5" x2="-4.4901" y2="5" width="0.2032" layer="51"/>
<wire x1="-4.4901" y1="5" x2="-5" y2="4.4901" width="0.2032" layer="51"/>
<wire x1="-5" y1="4.4901" x2="-5" y2="-4.5" width="0.2032" layer="51"/>
<wire x1="-5" y1="-4.5" x2="-4.5" y2="-5" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="-5" x2="4.4901" y2="-5" width="0.2032" layer="51"/>
<wire x1="4.4901" y1="-5" x2="5" y2="-4.4901" width="0.2032" layer="51"/>
<circle x="-3.8" y="3.8" radius="0.2827" width="0.254" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="MT3608">
<description>High Efficiency 1.2MHz 2A Step Up Converter&lt;br&gt;
&lt;br&gt;
Created by: Wai Weng</description>
<pin name="VIN" x="-10.16" y="2.54" length="short"/>
<pin name="EN" x="-10.16" y="0" length="short"/>
<pin name="GND" x="-10.16" y="-2.54" length="short"/>
<pin name="FB" x="10.16" y="-2.54" length="short" rot="R180"/>
<pin name="SW" x="10.16" y="2.54" length="short" rot="R180"/>
<wire x1="-7.62" y1="5.08" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<text x="-7.62" y="7.62" size="1.778" layer="95" align="top-left">&gt;NAME</text>
<text x="-7.62" y="-7.62" size="1.778" layer="96">&gt;MPN</text>
</symbol>
<symbol name="TRANSISTOR-NPN">
<wire x1="0" y1="2.54" x2="-2.032" y2="1.524" width="0.1524" layer="94"/>
<wire x1="-0.762" y1="-1.524" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-0.762" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="-1" y1="-2.04" x2="-2.232" y2="-1.424" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="-2.413" x2="-0.254" y2="-2.413" width="0.254" layer="94"/>
<wire x1="-0.254" y1="-2.413" x2="-0.762" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-0.762" y1="-1.778" x2="-1.016" y2="-2.286" width="0.254" layer="94"/>
<wire x1="-1.016" y1="-2.286" x2="-0.635" y2="-2.286" width="0.254" layer="94"/>
<wire x1="-0.635" y1="-2.286" x2="-0.762" y2="-2.032" width="0.254" layer="94"/>
<text x="2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="0" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.794" y1="-2.54" x2="-2.032" y2="2.54" layer="94"/>
<pin name="B" x="-5.08" y="0" visible="off" length="short" direction="in"/>
<pin name="E" x="0" y="-5.08" visible="off" length="short" rot="R90"/>
<pin name="C" x="0" y="5.08" visible="off" length="short" rot="R270"/>
<text x="2.54" y="-2.54" size="1.778" layer="97">&gt;MPN</text>
</symbol>
<symbol name="MOSFET-P">
<wire x1="-3.6576" y1="2.413" x2="-3.6576" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="1.905" x2="-2.0066" y2="1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="1.905" x2="2.54" y2="1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.905" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-0.762" x2="2.54" y2="1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-0.762" x2="3.175" y2="0.635" width="0.1524" layer="94"/>
<wire x1="3.175" y1="0.635" x2="1.905" y2="0.635" width="0.1524" layer="94"/>
<wire x1="1.905" y1="0.635" x2="2.54" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="3.175" y1="-0.762" x2="2.54" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-0.762" x2="1.905" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="1.905" y1="-0.762" x2="1.651" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="3.175" y1="-0.762" x2="3.429" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0.508" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0.508" x2="-1.27" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-0.508" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.143" y1="0" x2="-2.032" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.143" y1="-0.254" x2="-0.254" y2="0" width="0.3048" layer="94"/>
<wire x1="-0.254" y1="0" x2="-1.143" y2="0.254" width="0.3048" layer="94"/>
<wire x1="-1.143" y1="0.254" x2="-1.143" y2="0" width="0.3048" layer="94"/>
<wire x1="-1.143" y1="0" x2="-0.889" y2="0" width="0.3048" layer="94"/>
<wire x1="-3.81" y1="0" x2="-5.08" y2="0" width="0.1524" layer="94"/>
<circle x="0" y="-1.905" radius="0.127" width="0.4064" layer="94"/>
<circle x="0" y="1.905" radius="0.127" width="0.4064" layer="94"/>
<text x="5.08" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="5.08" y="0" size="1.778" layer="96">&gt;VALUE</text>
<text x="-1.27" y="2.54" size="0.8128" layer="93">D</text>
<text x="-1.27" y="-3.556" size="0.8128" layer="93">S</text>
<text x="-5.08" y="-1.27" size="0.8128" layer="93">G</text>
<rectangle x1="-2.794" y1="-2.54" x2="-2.032" y2="-1.27" layer="94"/>
<rectangle x1="-2.794" y1="1.27" x2="-2.032" y2="2.54" layer="94"/>
<rectangle x1="-2.794" y1="-0.889" x2="-2.032" y2="0.889" layer="94"/>
<pin name="G" x="-7.62" y="0" visible="off" length="short" direction="in"/>
<pin name="D" x="0" y="5.08" visible="off" length="short" rot="R270"/>
<pin name="S" x="0" y="-5.08" visible="off" length="short" rot="R90"/>
<text x="5.08" y="-2.54" size="1.778" layer="97">&gt;MPN</text>
</symbol>
<symbol name="MX1508">
<description>MX1508 Dual Channel DC Motor Driver&lt;br&gt;
&lt;br&gt;
Created by: Wai Weng</description>
<pin name="VDD1" x="-12.7" y="15.24" length="short"/>
<pin name="VDD2" x="-12.7" y="12.7" length="short"/>
<pin name="VCC1" x="-12.7" y="7.62" length="short"/>
<pin name="VCC2" x="-12.7" y="5.08" length="short"/>
<pin name="OUT2B" x="12.7" y="-17.78" length="short" rot="R180"/>
<pin name="OUT2A" x="12.7" y="-5.08" length="short" rot="R180"/>
<pin name="OUT1B" x="12.7" y="2.54" length="short" rot="R180"/>
<pin name="OUT1A" x="12.7" y="15.24" length="short" rot="R180"/>
<pin name="IN1A" x="-12.7" y="-7.62" length="short"/>
<pin name="IN1B" x="-12.7" y="-10.16" length="short"/>
<pin name="IN2A" x="-12.7" y="-15.24" length="short"/>
<pin name="IN2B" x="-12.7" y="-17.78" length="short"/>
<pin name="GND" x="-12.7" y="0" length="short"/>
<wire x1="-10.16" y1="17.78" x2="10.16" y2="17.78" width="0.254" layer="94"/>
<wire x1="10.16" y1="17.78" x2="10.16" y2="-20.32" width="0.254" layer="94"/>
<wire x1="10.16" y1="-20.32" x2="-10.16" y2="-20.32" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-20.32" x2="-10.16" y2="17.78" width="0.254" layer="94"/>
<text x="-10.16" y="20.32" size="1.778" layer="95" align="top-left">&gt;NAME</text>
<text x="-10.16" y="-22.86" size="1.778" layer="96">&gt;MPN</text>
</symbol>
<symbol name="MOSFET-N">
<wire x1="-3.6576" y1="2.413" x2="-3.6576" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="1.905" x2="-2.0066" y2="1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="1.905" x2="2.54" y2="1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.905" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="0" x2="-5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="0" x2="-0.762" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="-0.762" y1="-0.508" x2="-0.762" y2="0.508" width="0.1524" layer="94"/>
<wire x1="-0.762" y1="0.508" x2="-2.032" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="0.254" x2="-1.778" y2="0" width="0.3048" layer="94"/>
<wire x1="-1.778" y1="0" x2="-0.889" y2="-0.254" width="0.3048" layer="94"/>
<wire x1="-0.889" y1="-0.254" x2="-0.889" y2="0" width="0.3048" layer="94"/>
<wire x1="-0.889" y1="0" x2="-1.143" y2="0" width="0.3048" layer="94"/>
<wire x1="2.54" y1="1.905" x2="2.54" y2="0.762" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0.762" x2="1.905" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="1.905" y1="-0.635" x2="3.175" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="3.175" y1="-0.635" x2="2.54" y2="0.762" width="0.1524" layer="94"/>
<wire x1="1.905" y1="0.762" x2="2.54" y2="0.762" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0.762" x2="3.175" y2="0.762" width="0.1524" layer="94"/>
<wire x1="3.175" y1="0.762" x2="3.429" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.905" y1="0.762" x2="1.651" y2="0.508" width="0.1524" layer="94"/>
<circle x="0" y="-1.905" radius="0.127" width="0.4064" layer="94"/>
<circle x="0" y="1.905" radius="0.127" width="0.4064" layer="94"/>
<text x="5.08" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="5.08" y="0" size="1.778" layer="96">&gt;VALUE</text>
<text x="-1.27" y="2.54" size="0.8128" layer="93">D</text>
<text x="-1.27" y="-3.556" size="0.8128" layer="93">S</text>
<text x="-5.08" y="-1.27" size="0.8128" layer="93">G</text>
<rectangle x1="-2.794" y1="-2.54" x2="-2.032" y2="-1.27" layer="94"/>
<rectangle x1="-2.794" y1="1.27" x2="-2.032" y2="2.54" layer="94"/>
<rectangle x1="-2.794" y1="-0.889" x2="-2.032" y2="0.889" layer="94"/>
<pin name="G" x="-7.62" y="0" visible="off" length="short" direction="in"/>
<pin name="D" x="0" y="5.08" visible="off" length="short" rot="R270"/>
<pin name="S" x="0" y="-5.08" visible="off" length="short" rot="R90"/>
<text x="5.08" y="-2.54" size="1.778" layer="97">&gt;MPN</text>
</symbol>
<symbol name="MCP6001U">
<pin name="IN+" x="10.16" y="-2.54" length="short" rot="R180"/>
<pin name="V-" x="10.16" y="0" length="short" rot="R180"/>
<pin name="IN-" x="10.16" y="2.54" length="short" rot="R180"/>
<pin name="OUT" x="-10.16" y="2.54" length="short"/>
<pin name="V+" x="-10.16" y="-2.54" length="short"/>
<wire x1="-7.62" y1="5.08" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<text x="-7.62" y="5.842" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-7.62" size="1.778" layer="96">&gt;MPN</text>
</symbol>
<symbol name="PIC16F1937">
<wire x1="-48.26" y1="-22.86" x2="53.34" y2="-22.86" width="0.254" layer="94"/>
<wire x1="53.34" y1="-22.86" x2="53.34" y2="30.48" width="0.254" layer="94"/>
<wire x1="53.34" y1="30.48" x2="-48.26" y2="30.48" width="0.254" layer="94"/>
<wire x1="-48.26" y1="30.48" x2="-48.26" y2="-22.86" width="0.254" layer="94"/>
<text x="-48.26" y="31.75" size="1.778" layer="95">&gt;NAME</text>
<text x="-48.26" y="-25.4" size="1.778" layer="97">&gt;MPN</text>
<pin name="SEG8/DT/RX/RC7" x="-50.8" y="27.94" length="short"/>
<pin name="SEG17/P2D/CPS12/RD4" x="-50.8" y="25.4" length="short"/>
<pin name="SEG18/P1B/CPS13/RD5" x="-50.8" y="22.86" length="short"/>
<pin name="SEG19/P1C/CPS14/RD6" x="-50.8" y="20.32" length="short"/>
<pin name="SEG20/P1D/CPS15/RD7" x="-50.8" y="17.78" length="short"/>
<pin name="VSS@6" x="-50.8" y="15.24" length="short"/>
<pin name="VDD@7" x="-50.8" y="12.7" length="short"/>
<pin name="SEG0/INT/SRI/CPS0/AN12/RB0" x="-50.8" y="10.16" length="short"/>
<pin name="VLCD1/CPS1/C12IN3-/AN10/RB1" x="-50.8" y="7.62" length="short"/>
<pin name="VLCD2/CPS2/AN8/RB2" x="-50.8" y="5.08" length="short"/>
<pin name="VLCD3/P2A/CCP2/CPS3/C12IN2-/AN9/RB3" x="-50.8" y="2.54" length="short"/>
<pin name="COM0/CPS4/AN11/RB4" x="-50.8" y="0" length="short"/>
<pin name="COM1/T1G/P3A/CCP3/CPS5/AN13/RB5" x="-50.8" y="-2.54" length="short"/>
<pin name="SEG14/ICDCLK/ICSPCLK/RB6" x="-50.8" y="-5.08" length="short"/>
<pin name="SEG13/ICDDAT/ICSPDAT/RB7" x="-50.8" y="-7.62" length="short"/>
<pin name="VPP/!MCLR!/RE3" x="-50.8" y="-10.16" length="short"/>
<pin name="SEG12/VCAP/SS/SRNQ/C2OUT/C12IN0-/AN0/RA0" x="-50.8" y="-12.7" length="short"/>
<pin name="SEG7/C12IN1-/AN1/RA1" x="-50.8" y="-15.24" length="short"/>
<pin name="COM2/DACOUT/VREF-/C2IN+/AN2/RA2" x="-50.8" y="-17.78" length="short"/>
<pin name="SEG15/VREF+/C1IN+/AN3/RA3" x="-50.8" y="-20.32" length="short"/>
<pin name="RA4/C1OUT/CPS6/T0CKI/SRQ/SEG4" x="55.88" y="-20.32" length="short" rot="R180"/>
<pin name="RA5/AN4/C2OUT/CPS7/SRNQ/!SS!/VCAP/SEG5" x="55.88" y="-17.78" length="short" rot="R180"/>
<pin name="RE0/AN5/CCP3/P3A/SEG21" x="55.88" y="-15.24" length="short" rot="R180"/>
<pin name="RE1/AN6/P3B/SEG22" x="55.88" y="-12.7" length="short" rot="R180"/>
<pin name="RE2/AN7/CCP5/SEG23" x="55.88" y="-10.16" length="short" rot="R180"/>
<pin name="VDD@28" x="55.88" y="-7.62" length="short" rot="R180"/>
<pin name="VSS@29" x="55.88" y="-5.08" length="short" rot="R180"/>
<pin name="RA7/OSC1/CLKIN/SEG2" x="55.88" y="-2.54" length="short" rot="R180"/>
<pin name="RA6/OSC2/CLKOUT/VCAP/SEG1" x="55.88" y="0" length="short" rot="R180"/>
<pin name="RC0/T1OSO/T1CKI/P2B" x="55.88" y="2.54" length="short" rot="R180"/>
<pin name="RC1/T1OSI/CCP2/P2A" x="55.88" y="5.08" length="short" rot="R180"/>
<pin name="RC2/CCP1/P1A/SEG3" x="55.88" y="7.62" length="short" rot="R180"/>
<pin name="RC3/SCL/SCK/SEG6" x="55.88" y="10.16" length="short" rot="R180"/>
<pin name="RD0/CPS8/COM3" x="55.88" y="12.7" length="short" rot="R180"/>
<pin name="RD1/CPS9/CCP4" x="55.88" y="15.24" length="short" rot="R180"/>
<pin name="RD2/CPS10/P2B" x="55.88" y="17.78" length="short" rot="R180"/>
<pin name="RD3/CPS11/P2C/SEG16" x="55.88" y="20.32" length="short" rot="R180"/>
<pin name="RC4/SDI/SDA/T1G/SEG11" x="55.88" y="22.86" length="short" rot="R180"/>
<pin name="RC5/SDO/SEG10" x="55.88" y="25.4" length="short" rot="R180"/>
<pin name="RC6/TX/CK/SEG9" x="55.88" y="27.94" length="short" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MT3608" prefix="U">
<description>High Efficiency 1.2MHz 2A Step Up Converter&lt;br&gt;
&lt;br&gt;
Created by: Wai Weng</description>
<gates>
<gate name="G$1" symbol="MT3608" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT-23-6">
<connects>
<connect gate="G$1" pin="EN" pad="4"/>
<connect gate="G$1" pin="FB" pad="3"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="SW" pad="1"/>
<connect gate="G$1" pin="VIN" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="S-IC-MT3608-SOT236" constant="no"/>
<attribute name="DESC" value="S Boost Converter MT3608 SOT23-6" constant="no"/>
<attribute name="MF" value="Aerosemi" constant="no"/>
<attribute name="MPN" value="MT3608" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="24V 2A" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TRANSISTOR-NPN" prefix="Q">
<description>NPN Transistor</description>
<gates>
<gate name="G$1" symbol="TRANSISTOR-NPN" x="0" y="0"/>
</gates>
<devices>
<device name="_(SOT-23-BEC)" package="SOT-23">
<connects>
<connect gate="G$1" pin="B" pad="1"/>
<connect gate="G$1" pin="C" pad="3"/>
<connect gate="G$1" pin="E" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="SMD-TR-?" constant="no"/>
<attribute name="DESC" value="SMD Transistor - NPN" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
<technology name="_BC817">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="S NPN Transistor BC817 SOT-23" constant="no"/>
<attribute name="MF" value="NXP" constant="no"/>
<attribute name="MPN" value="BC817" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="45V 500mA" constant="no"/>
</technology>
<technology name="_MMBT2222">
<attribute name="CYTRON-PC" value="S-Q-2222-SOT23" constant="no"/>
<attribute name="DESC" value="S NPN Transistor MMBT2222A SOT-23" constant="no"/>
<attribute name="MF" value="CHANGJIANG" constant="no"/>
<attribute name="MPN" value="MMBT2222A" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="40V 600mA" constant="no"/>
</technology>
<technology name="_MMBT3904">
<attribute name="CYTRON-PC" value="S-Q-3904-SOT23" constant="no"/>
<attribute name="DESC" value="S NPN Transistor 3904 SOT23" constant="no"/>
<attribute name="MF" value="FAIRCHILD" constant="no"/>
<attribute name="MPN" value="MMBT3904" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="40V 200mA" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(TO-92-TH-BCE)" package="TO-92">
<connects>
<connect gate="G$1" pin="B" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
<connect gate="G$1" pin="E" pad="3"/>
</connects>
<technologies>
<technology name="_3DG3020A1">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="NPN Transistor 3DG3020A1 TO-92" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="3DG3020A1" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="450V 1.5A" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MOSFET-P" prefix="Q" uservalue="yes">
<description>P-Channel MOSFET</description>
<gates>
<gate name="G$1" symbol="MOSFET-P" x="0" y="0"/>
</gates>
<devices>
<device name="_(DPAK)" package="DPAK">
<connects>
<connect gate="G$1" pin="D" pad="4"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
<technology name="_NCE40P70K">
<attribute name="CYTRON-PC" value="S-Q-NCE40P70K-DPAK" constant="no"/>
<attribute name="DESC" value="S Mosfet NCE40P70K P Channel DPAK" constant="no"/>
<attribute name="MF" value="NCEPOWER" constant="no"/>
<attribute name="MPN" value="NCE40P70K" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="-40V -70A" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(POWERPAK)" package="POWERPAK-1212-8-SINGLE">
<connects>
<connect gate="G$1" pin="D" pad="5 6 7 8 9"/>
<connect gate="G$1" pin="G" pad="4"/>
<connect gate="G$1" pin="S" pad="1 2 3"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
<technology name="_SI7617DN">
<attribute name="CYTRON-PC" value="S-Q-SI7617DN-T1" constant="no"/>
<attribute name="DESC" value="S P Mosfet SI7617DN-T1-GE3 PowerPAK 1212" constant="no"/>
<attribute name="MF" value="VISHAY_SILICONIX" constant="no"/>
<attribute name="MPN" value="Si7617DN" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="-30V -35A" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(TO-263)" package="TO-263-2">
<connects>
<connect gate="G$1" pin="D" pad="4"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="3"/>
</connects>
<technologies>
<technology name="_IRF4905SPBF">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="S Mosfet IRF4905SPBF P Channel DPAK" constant="no"/>
<attribute name="MF" value="International Rectifier" constant="no"/>
<attribute name="MPN" value="IRF4905SPBF" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="-55V -70A" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(SO-8)" package="SO-8">
<connects>
<connect gate="G$1" pin="D" pad="5 6 7 8"/>
<connect gate="G$1" pin="G" pad="4"/>
<connect gate="G$1" pin="S" pad="1 2 3"/>
</connects>
<technologies>
<technology name="_IRF7410">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="P Channel Mosfet 12V 16A" constant="no"/>
<attribute name="MF" value="IR" constant="no"/>
<attribute name="MPN" value="IRF7410" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="12V 16A" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(SOT)" package="SOT-23">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name="_IRLML6402">
<attribute name="CYTRON-PC" value="S-Q-IRLML6402-SOT23" constant="no"/>
<attribute name="DESC" value="S Mosfet IRLML6402PBF SOT23" constant="no"/>
<attribute name="MF" value="IR" constant="no"/>
<attribute name="MPN" value="IRLML6402" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="-20V3.7A" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MX1508" prefix="U">
<description>MX1508 Dual Channel DC Motor Driver&lt;br&gt;
&lt;br&gt;
Created by: Wai Weng</description>
<gates>
<gate name="G$1" symbol="MX1508" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOIC-16N">
<connects>
<connect gate="G$1" pin="GND" pad="10 11 14 15"/>
<connect gate="G$1" pin="IN1A" pad="2"/>
<connect gate="G$1" pin="IN1B" pad="3"/>
<connect gate="G$1" pin="IN2A" pad="6"/>
<connect gate="G$1" pin="IN2B" pad="7"/>
<connect gate="G$1" pin="OUT1A" pad="16"/>
<connect gate="G$1" pin="OUT1B" pad="13"/>
<connect gate="G$1" pin="OUT2A" pad="12"/>
<connect gate="G$1" pin="OUT2B" pad="9"/>
<connect gate="G$1" pin="VCC1" pad="1"/>
<connect gate="G$1" pin="VCC2" pad="5"/>
<connect gate="G$1" pin="VDD1" pad="4"/>
<connect gate="G$1" pin="VDD2" pad="8"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="S-IC-MX1508-SO16" constant="no"/>
<attribute name="DESC" value="S Dual Motor Driver MX1508 SO16" constant="no"/>
<attribute name="MF" value="Sinotech Mixic Electronics" constant="no"/>
<attribute name="MPN" value="MX1508" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="2 - 9.6V" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MOSFET-N" prefix="Q">
<description>N-Channel MOSFET&lt;br&gt;
&lt;br&gt;&lt;br&gt;
Created by: Wai Weng&lt;br&gt;</description>
<gates>
<gate name="G$1" symbol="MOSFET-N" x="0" y="0"/>
</gates>
<devices>
<device name="_(DPAK)" package="DPAK">
<connects>
<connect gate="G$1" pin="D" pad="4"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="3"/>
</connects>
<technologies>
<technology name="_NCE3080K">
<attribute name="CYTRON-PC" value="S-Q-NCE3080K-DPAK" constant="no"/>
<attribute name="DESC" value="S Mosfet NCE3080K DPAK" constant="no"/>
<attribute name="MF" value="NCE Power" constant="no"/>
<attribute name="MPN" value="NCE3080K" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="30V 80A" constant="no"/>
</technology>
<technology name="_NCE30H12K">
<attribute name="CYTRON-PC" value="S-Q-NCE30H12K-DPAK" constant="no"/>
<attribute name="DESC" value="S Mosfet NCE30H12K DPAK" constant="no"/>
<attribute name="MF" value="NCE Power" constant="no"/>
<attribute name="MPN" value="NCE30H12K" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="30V 120A" constant="no"/>
</technology>
<technology name="_NCE4080K">
<attribute name="CYTRON-PC" value="S-Q-NCE4080K-DPAK" constant="no"/>
<attribute name="DESC" value="S Mosfet NCE4080K DPAK" constant="no"/>
<attribute name="MF" value="NCE Power" constant="no"/>
<attribute name="MPN" value="NCE4080K" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="40V 80A" constant="no"/>
</technology>
<technology name="_NCE40H12K">
<attribute name="CYTRON-PC" value="S-Q-NCE40H12K-DPAK" constant="no"/>
<attribute name="DESC" value="S Mosfet NCE40H12K DPAK" constant="no"/>
<attribute name="MF" value="NCE Power" constant="no"/>
<attribute name="MPN" value="NCE40H12K" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="40V 120A" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(TO-220-TH)" package="TO-220">
<connects>
<connect gate="G$1" pin="D" pad="2"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="3"/>
</connects>
<technologies>
<technology name="_NCE40H20">
<attribute name="CYTRON-PC" value="TR-NCE-40H20" constant="no"/>
<attribute name="DESC" value="MOSFET - N Channel" constant="no"/>
<attribute name="MF" value="NCE Power" constant="no"/>
<attribute name="MPN" value="NCE40H20" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="40V 200A" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(SOT-23)" package="SOT-23">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name="_BSS138">
<attribute name="CYTRON-PC" value="S-Q-BSS138-SOT23" constant="no"/>
<attribute name="DESC" value="SMD MOSFET - N Channel" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="BSS138" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="50V 220mA" constant="no"/>
</technology>
<technology name="_NCE2302">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="SMD MOSFET - N Channel" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="NCE2302" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="20V 2.9A" constant="no"/>
</technology>
<technology name="_SI2312BDS">
<attribute name="CYTRON-PC" value="S-Q-SI2312BD-SOT23" constant="no"/>
<attribute name="DESC" value="S Mosfet SI2312BDS SOT23" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="20V 3.9A" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(TO-263)" package="TO-263-2">
<connects>
<connect gate="G$1" pin="D" pad="4"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="3"/>
</connects>
<technologies>
<technology name="_NCE75H21D">
<attribute name="CYTRON-PC" value="S-Q-NCE75H21D-TO263" constant="no"/>
<attribute name="DESC" value="S Mosfet NCE75H21D TO263" constant="no"/>
<attribute name="MF" value="NCE Power" constant="no"/>
<attribute name="MPN" value="NCE75H21D" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="75V 210A" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(D2PAK-7)" package="D2PAK-7">
<connects>
<connect gate="G$1" pin="D" pad="D"/>
<connect gate="G$1" pin="G" pad="G"/>
<connect gate="G$1" pin="S" pad="S1 S2 S3 S4 S5"/>
</connects>
<technologies>
<technology name="_IRFS7430">
<attribute name="CYTRON-PC" value="S-Q-IRFS7430-D2PAK7" constant="no"/>
<attribute name="DESC" value="" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="PNP-LIB" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MCP6001U" prefix="U">
<gates>
<gate name="G$1" symbol="MCP6001U" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT-23-5">
<connects>
<connect gate="G$1" pin="IN+" pad="1"/>
<connect gate="G$1" pin="IN-" pad="3"/>
<connect gate="G$1" pin="OUT" pad="4"/>
<connect gate="G$1" pin="V+" pad="5"/>
<connect gate="G$1" pin="V-" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="S-IC-MCP6001-SOT235" constant="no"/>
<attribute name="DESC" value="S Op Amp MCP6001 SOT23-5" constant="no"/>
<attribute name="MF" value="MICROCHIP" constant="no"/>
<attribute name="MPN" value="MCP6001UT-I/OT" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="5 Pins" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PIC16F1937" prefix="U">
<description>PIC16F1937</description>
<gates>
<gate name="G$1" symbol="PIC16F1937" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TQFP-44-08">
<connects>
<connect gate="G$1" pin="COM0/CPS4/AN11/RB4" pad="14"/>
<connect gate="G$1" pin="COM1/T1G/P3A/CCP3/CPS5/AN13/RB5" pad="15"/>
<connect gate="G$1" pin="COM2/DACOUT/VREF-/C2IN+/AN2/RA2" pad="21"/>
<connect gate="G$1" pin="RA4/C1OUT/CPS6/T0CKI/SRQ/SEG4" pad="23"/>
<connect gate="G$1" pin="RA5/AN4/C2OUT/CPS7/SRNQ/!SS!/VCAP/SEG5" pad="24"/>
<connect gate="G$1" pin="RA6/OSC2/CLKOUT/VCAP/SEG1" pad="31"/>
<connect gate="G$1" pin="RA7/OSC1/CLKIN/SEG2" pad="30"/>
<connect gate="G$1" pin="RC0/T1OSO/T1CKI/P2B" pad="32"/>
<connect gate="G$1" pin="RC1/T1OSI/CCP2/P2A" pad="35"/>
<connect gate="G$1" pin="RC2/CCP1/P1A/SEG3" pad="36"/>
<connect gate="G$1" pin="RC3/SCL/SCK/SEG6" pad="37"/>
<connect gate="G$1" pin="RC4/SDI/SDA/T1G/SEG11" pad="42"/>
<connect gate="G$1" pin="RC5/SDO/SEG10" pad="43"/>
<connect gate="G$1" pin="RC6/TX/CK/SEG9" pad="44"/>
<connect gate="G$1" pin="RD0/CPS8/COM3" pad="38"/>
<connect gate="G$1" pin="RD1/CPS9/CCP4" pad="39"/>
<connect gate="G$1" pin="RD2/CPS10/P2B" pad="40"/>
<connect gate="G$1" pin="RD3/CPS11/P2C/SEG16" pad="41"/>
<connect gate="G$1" pin="RE0/AN5/CCP3/P3A/SEG21" pad="25"/>
<connect gate="G$1" pin="RE1/AN6/P3B/SEG22" pad="26"/>
<connect gate="G$1" pin="RE2/AN7/CCP5/SEG23" pad="27"/>
<connect gate="G$1" pin="SEG0/INT/SRI/CPS0/AN12/RB0" pad="8"/>
<connect gate="G$1" pin="SEG12/VCAP/SS/SRNQ/C2OUT/C12IN0-/AN0/RA0" pad="19"/>
<connect gate="G$1" pin="SEG13/ICDDAT/ICSPDAT/RB7" pad="17"/>
<connect gate="G$1" pin="SEG14/ICDCLK/ICSPCLK/RB6" pad="16"/>
<connect gate="G$1" pin="SEG15/VREF+/C1IN+/AN3/RA3" pad="22"/>
<connect gate="G$1" pin="SEG17/P2D/CPS12/RD4" pad="2"/>
<connect gate="G$1" pin="SEG18/P1B/CPS13/RD5" pad="3"/>
<connect gate="G$1" pin="SEG19/P1C/CPS14/RD6" pad="4"/>
<connect gate="G$1" pin="SEG20/P1D/CPS15/RD7" pad="5"/>
<connect gate="G$1" pin="SEG7/C12IN1-/AN1/RA1" pad="20"/>
<connect gate="G$1" pin="SEG8/DT/RX/RC7" pad="1"/>
<connect gate="G$1" pin="VDD@28" pad="28"/>
<connect gate="G$1" pin="VDD@7" pad="7"/>
<connect gate="G$1" pin="VLCD1/CPS1/C12IN3-/AN10/RB1" pad="9"/>
<connect gate="G$1" pin="VLCD2/CPS2/AN8/RB2" pad="10"/>
<connect gate="G$1" pin="VLCD3/P2A/CCP2/CPS3/C12IN2-/AN9/RB3" pad="11"/>
<connect gate="G$1" pin="VPP/!MCLR!/RE3" pad="18"/>
<connect gate="G$1" pin="VSS@29" pad="29"/>
<connect gate="G$1" pin="VSS@6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="S-IC-16F1937-TQFP44" constant="no"/>
<attribute name="DESC" value="S PIC16F1937 TQFP44" constant="no"/>
<attribute name="MF" value="Microchip Technology" constant="no"/>
<attribute name="MPN" value="PIC16F1937-I/PT" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Others">
<description>Other general components such as resistor, capacitor, inductor, diode, LED, switch, crystal, etc.</description>
<packages>
<package name="2012">
<description>SMD resistor, capacitor and other devices.&lt;br&gt;
Size: 2.0 x 1.2mm&lt;br&gt;
&lt;br&gt;
Same with size 0805 in inches.</description>
<smd name="1" x="-1" y="0" dx="1.2" dy="1.6" layer="1" roundness="75"/>
<smd name="2" x="1" y="0" dx="1.2" dy="1.6" layer="1" roundness="75"/>
<text x="0" y="0.95" size="0.762" layer="25" font="vector" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.95" size="0.762" layer="27" font="vector" ratio="15" align="top-center">&gt;VALUE</text>
<wire x1="-1" y1="0.6" x2="-1" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="-1" y1="-0.6" x2="1" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.6" x2="1" y2="0.6" width="0.2032" layer="51"/>
<wire x1="1" y1="0.6" x2="-1" y2="0.6" width="0.2032" layer="51"/>
<rectangle x1="-1" y1="-0.6" x2="-0.5" y2="0.6" layer="51"/>
<rectangle x1="0.5" y1="-0.6" x2="1" y2="0.6" layer="51"/>
<wire x1="0" y1="0.2" x2="0" y2="-0.2" width="0.55" layer="21"/>
<polygon width="0.2032" layer="200">
<vertex x="-0.6" y="0.6"/>
<vertex x="0.6" y="0.6"/>
<vertex x="0.6" y="-0.6"/>
<vertex x="-0.6" y="-0.6"/>
</polygon>
</package>
<package name="AXIAL-0.3-TH">
<description>1/4W Through Hole Resistor&lt;br&gt;
Length: 300mil
&lt;br&gt;&lt;br&gt;
&lt;b&gt;&lt;i&gt;* Need to measure the actual size of the component.&lt;/i&gt;&lt;/b&gt;</description>
<pad name="P$1" x="-3.81" y="0" drill="0.762" diameter="1.524"/>
<pad name="P$2" x="3.81" y="0" drill="0.762" diameter="1.524"/>
<text x="0" y="0.127" size="0.762" layer="25" font="vector" ratio="15" align="bottom-center">&gt;Name</text>
<text x="0" y="-0.127" size="0.762" layer="27" font="vector" ratio="15" align="top-center">&gt;Value</text>
<wire x1="-2.54" y1="1.1938" x2="2.54" y2="1.1938" width="0.2032" layer="21"/>
<wire x1="2.54" y1="1.1938" x2="2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-1.1938" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.1938" x2="-2.54" y2="-1.1938" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-1.1938" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="1.1938" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="1.1938" x2="2.54" y2="1.1938" width="0.2032" layer="51"/>
<wire x1="2.54" y1="1.1938" x2="2.54" y2="0" width="0.2032" layer="51"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-1.1938" width="0.2032" layer="51"/>
<wire x1="2.54" y1="-1.1938" x2="-2.54" y2="-1.1938" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-1.1938" x2="-2.54" y2="0" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="1.1938" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.2032" layer="51"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.2032" layer="51"/>
</package>
<package name="AXIAL-0.4-TH">
<description>1/4W Through Hole Resistor&lt;br&gt;
Length: 400mil
&lt;br&gt;&lt;br&gt;
&lt;b&gt;&lt;i&gt;* Need to measure the actual size of the component.&lt;/i&gt;&lt;/b&gt;</description>
<pad name="P$1" x="-5.08" y="0" drill="0.762" diameter="1.524"/>
<pad name="P$2" x="5.08" y="0" drill="0.762" diameter="1.524"/>
<text x="0" y="0.127" size="0.762" layer="25" font="vector" ratio="15" align="bottom-center">&gt;Name</text>
<text x="0" y="-0.127" size="0.762" layer="27" font="vector" ratio="15" align="top-center">&gt;Value</text>
<wire x1="-3.175" y1="1.1938" x2="3.175" y2="1.1938" width="0.2032" layer="51"/>
<wire x1="3.175" y1="1.1938" x2="3.175" y2="0" width="0.2032" layer="51"/>
<wire x1="3.175" y1="0" x2="3.175" y2="-1.1938" width="0.2032" layer="51"/>
<wire x1="3.175" y1="-1.1938" x2="-3.175" y2="-1.1938" width="0.2032" layer="51"/>
<wire x1="-3.175" y1="-1.1938" x2="-3.175" y2="0" width="0.2032" layer="51"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="1.1938" width="0.2032" layer="51"/>
<wire x1="-3.175" y1="0" x2="-4.064" y2="0" width="0.2032" layer="51"/>
<wire x1="3.175" y1="0" x2="4.064" y2="0" width="0.2032" layer="51"/>
<wire x1="-3.175" y1="1.1938" x2="3.175" y2="1.1938" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.1938" x2="3.175" y2="0" width="0.2032" layer="21"/>
<wire x1="3.175" y1="0" x2="3.175" y2="-1.1938" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.1938" x2="-3.175" y2="-1.1938" width="0.2032" layer="21"/>
<wire x1="-3.175" y1="-1.1938" x2="-3.175" y2="0" width="0.2032" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="1.1938" width="0.2032" layer="21"/>
<wire x1="-3.175" y1="0" x2="-4.064" y2="0" width="0.2032" layer="21"/>
<wire x1="3.175" y1="0" x2="4.064" y2="0" width="0.2032" layer="21"/>
</package>
<package name="6865">
<description>SMD resistor&lt;br&gt;
Size: 6.81mm x 6.45mm&lt;br&gt;
&lt;br&gt;
Same with size 2725 in inches.</description>
<smd name="1" x="-2.5" y="0" dx="2.9" dy="6.86" layer="1" roundness="10"/>
<smd name="2" x="2.5" y="0" dx="2.9" dy="6.86" layer="1" roundness="10"/>
<text x="0" y="3.99" size="0.762" layer="25" font="vector" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.99" size="0.762" layer="27" font="vector" ratio="15" align="top-center">&gt;VALUE</text>
<wire x1="-3.405" y1="3.225" x2="-3.405" y2="-3.225" width="0.2032" layer="51"/>
<wire x1="-3.405" y1="-3.225" x2="3.405" y2="-3.225" width="0.2032" layer="51"/>
<wire x1="3.405" y1="-3.225" x2="3.405" y2="3.225" width="0.2032" layer="51"/>
<wire x1="3.405" y1="3.225" x2="-3.405" y2="3.225" width="0.2032" layer="51"/>
<rectangle x1="-3.405" y1="-3.225" x2="-1.245" y2="3.225" layer="51"/>
<rectangle x1="1.245" y1="-3.225" x2="3.405" y2="3.225" layer="51"/>
<wire x1="0" y1="2.5" x2="0" y2="-2.5" width="0.55" layer="21"/>
</package>
<package name="1608">
<description>SMD resistor, capacitor and other devices.&lt;br&gt;
Size: 1.6 x 0.8mm&lt;br&gt;
&lt;br&gt;
Same with size 0603 in inches.</description>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1" roundness="75"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1" roundness="75"/>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<text x="0" y="0.762" size="0.762" layer="25" font="vector" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.762" size="0.762" layer="27" font="vector" ratio="15" align="top-center">&gt;VALUE</text>
<polygon width="0.2032" layer="21">
<vertex x="-0.127" y="0.254"/>
<vertex x="0.127" y="0.254"/>
<vertex x="0.127" y="-0.254"/>
<vertex x="-0.127" y="-0.254"/>
</polygon>
</package>
<package name="2512">
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="1.473" x2="1.498" y2="1.473" width="0.1524" layer="21"/>
<wire x1="-1.473" y1="-1.473" x2="1.498" y2="-1.473" width="0.1524" layer="21"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.667" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.667" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
</package>
<package name="6432">
<description>SMD resistor, capacitor and other devices.&lt;br&gt;
Size: 6.4 x 3.2mm&lt;br&gt;
&lt;br&gt;
Same with size 2512 in inches.&lt;br&gt;
&lt;br&gt;
Created by: Wai Weng</description>
<smd name="1" x="-2.8" y="0" dx="2.6" dy="3.6" layer="1" roundness="20"/>
<smd name="2" x="2.8" y="0" dx="2.6" dy="3.6" layer="1" roundness="20"/>
<text x="0" y="2.093" size="0.762" layer="25" font="vector" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.093" size="0.762" layer="27" font="vector" ratio="15" align="top-center">&gt;VALUE</text>
<wire x1="-3.2" y1="1.6" x2="-3.2" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-3.2" y1="-1.6" x2="3.2" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="3.2" y1="-1.6" x2="3.2" y2="1.6" width="0.2032" layer="51"/>
<wire x1="3.2" y1="1.6" x2="-3.2" y2="1.6" width="0.2032" layer="51"/>
<rectangle x1="-3.2" y1="-1.6" x2="-1.65" y2="1.6" layer="51"/>
<rectangle x1="1.65" y1="-1.6" x2="3.2" y2="1.6" layer="51"/>
<wire x1="-1.1" y1="1.2" x2="1.1" y2="1.2" width="0.2032" layer="21"/>
<wire x1="1.1" y1="1.2" x2="1.1" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="1.1" y1="-1.2" x2="-1.1" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="-1.1" y1="-1.2" x2="-1.1" y2="1.2" width="0.2032" layer="21"/>
</package>
<package name="5930">
<description>SMD resistor&lt;br&gt;
Size: 15 x 7.6mm&lt;br&gt;
&lt;br&gt;
Reference: &lt;a href="https://world.taobao.com/item/44707812451.htm?spm=a312a.7700714.0.0.bpWZ39#detail"&gt;Taobao 2mR 5W&lt;/a&gt;&lt;br&gt;
&lt;br&gt;
Created by: Idris</description>
<smd name="1" x="-5.5" y="0" dx="6" dy="7.6" layer="1" roundness="20"/>
<smd name="2" x="5.5" y="0" dx="6" dy="7.6" layer="1" roundness="20"/>
<text x="0" y="2.093" size="0.762" layer="25" font="vector" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.093" size="0.762" layer="27" font="vector" ratio="15" align="top-center">&gt;VALUE</text>
<wire x1="-7.5" y1="3.8" x2="-7.5" y2="-3.8" width="0.2032" layer="51"/>
<wire x1="-7.5" y1="-3.8" x2="7.5" y2="-3.8" width="0.2032" layer="51"/>
<wire x1="7.5" y1="-3.8" x2="7.5" y2="3.8" width="0.2032" layer="51"/>
<wire x1="7.5" y1="3.8" x2="-7.5" y2="3.8" width="0.2032" layer="51"/>
<rectangle x1="-7.5" y1="-3.8" x2="-2.5" y2="3.8" layer="51"/>
<rectangle x1="2.5" y1="-3.8" x2="7.5" y2="3.8" layer="51"/>
<wire x1="-2" y1="3.2" x2="2" y2="3.2" width="0.2032" layer="21"/>
<wire x1="2" y1="3.2" x2="2" y2="-3.2" width="0.2032" layer="21"/>
<wire x1="2" y1="-3.2" x2="-2" y2="-3.2" width="0.2032" layer="21"/>
<wire x1="-2" y1="-3.2" x2="-2" y2="3.2" width="0.2032" layer="21"/>
</package>
<package name="AXIAL-0.6-TH">
<description>1W Through Hole Resistor&lt;br&gt;
Length: 600mil
&lt;br&gt;&lt;br&gt;
Created by: Wai Weng</description>
<pad name="P$1" x="-7.62" y="0" drill="0.8" diameter="1.524"/>
<pad name="P$2" x="7.62" y="0" drill="0.8" diameter="1.524"/>
<text x="0" y="0.127" size="0.762" layer="25" font="vector" ratio="15" align="bottom-center">&gt;Name</text>
<text x="0" y="-0.127" size="0.762" layer="27" font="vector" ratio="15" align="top-center">&gt;Value</text>
<wire x1="-5.5" y1="2.25" x2="5.5" y2="2.25" width="0.2032" layer="51"/>
<wire x1="5.5" y1="2.25" x2="5.5" y2="0" width="0.2032" layer="51"/>
<wire x1="5.5" y1="0" x2="5.5" y2="-2.25" width="0.2032" layer="51"/>
<wire x1="5.5" y1="-2.25" x2="-5.5" y2="-2.25" width="0.2032" layer="51"/>
<wire x1="-5.5" y1="-2.25" x2="-5.5" y2="0" width="0.2032" layer="51"/>
<wire x1="-5.5" y1="0" x2="-5.5" y2="2.25" width="0.2032" layer="51"/>
<wire x1="-5.5" y1="0" x2="-6.5" y2="0" width="0.2032" layer="51"/>
<wire x1="5.5" y1="0" x2="6.5" y2="0" width="0.2032" layer="51"/>
<wire x1="-5.5" y1="2.25" x2="5.5" y2="2.25" width="0.2032" layer="21"/>
<wire x1="5.5" y1="2.25" x2="5.5" y2="0" width="0.2032" layer="21"/>
<wire x1="5.5" y1="0" x2="5.5" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="5.5" y1="-2.25" x2="-5.5" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-5.5" y1="-2.25" x2="-5.5" y2="0" width="0.2032" layer="21"/>
<wire x1="-5.5" y1="0" x2="-5.5" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-5.5" y1="0" x2="-6.5" y2="0" width="0.2032" layer="21"/>
<wire x1="5.5" y1="0" x2="6.5" y2="0" width="0.2032" layer="21"/>
</package>
<package name="3216">
<description>SMD resistor, capacitor and other devices.&lt;br&gt;
Size: 3.2 x 1.6mm&lt;br&gt;
&lt;br&gt;
Same with size 1206 in inches.&lt;br&gt;
&lt;br&gt;
Created by: Wai Weng</description>
<smd name="1" x="-1.6" y="0" dx="1.3" dy="2" layer="1" roundness="30"/>
<smd name="2" x="1.6" y="0" dx="1.3" dy="2" layer="1" roundness="30"/>
<text x="0" y="1.331" size="0.762" layer="25" font="vector" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.331" size="0.762" layer="27" font="vector" ratio="15" align="top-center">&gt;VALUE</text>
<wire x1="-1.6" y1="0.8" x2="-1.6" y2="-0.8" width="0.2032" layer="51"/>
<wire x1="-1.6" y1="-0.8" x2="1.6" y2="-0.8" width="0.2032" layer="51"/>
<wire x1="1.6" y1="-0.8" x2="1.6" y2="0.8" width="0.2032" layer="51"/>
<wire x1="1.6" y1="0.8" x2="-1.6" y2="0.8" width="0.2032" layer="51"/>
<rectangle x1="-1.6" y1="-0.8" x2="-1.1" y2="0.8" layer="51"/>
<rectangle x1="1.1" y1="-0.8" x2="1.6" y2="0.8" layer="51"/>
<wire x1="0" y1="0.3" x2="0" y2="-0.3" width="0.55" layer="21"/>
</package>
<package name="R-CEMENT-220100">
<description>5W Through Hole Resistor&lt;br&gt;
Size: 22mm x 10mm&lt;br&gt;
&lt;br&gt;
Created by: Wai Weng</description>
<wire x1="-11" y1="5" x2="11" y2="5" width="0.2032" layer="51"/>
<wire x1="11" y1="5" x2="11" y2="0" width="0.2032" layer="51"/>
<wire x1="11" y1="0" x2="11" y2="-5" width="0.2032" layer="51"/>
<wire x1="11" y1="-5" x2="-11" y2="-5" width="0.2032" layer="51"/>
<wire x1="-11" y1="-5" x2="-11" y2="0" width="0.2032" layer="51"/>
<pad name="1" x="-13.97" y="0" drill="1" diameter="1.778"/>
<pad name="2" x="13.97" y="0" drill="1" diameter="1.778"/>
<wire x1="-11" y1="0" x2="-11" y2="5" width="0.2032" layer="51"/>
<wire x1="-11" y1="0" x2="-12.7" y2="0" width="0.2032" layer="51"/>
<wire x1="11" y1="0" x2="12.7" y2="0" width="0.2032" layer="51"/>
<text x="0" y="0.127" size="0.762" layer="25" font="vector" ratio="15" align="bottom-center">&gt;Name</text>
<text x="0" y="-0.127" size="0.762" layer="27" font="vector" ratio="15" align="top-center">&gt;Value</text>
</package>
<package name="LED-3216">
<description>SMD LED&lt;br&gt;
Size: 3.2 x 1.6mm&lt;br&gt;
&lt;br&gt;
Same with size 1206 in inches.</description>
<wire x1="-0.3" y1="-0.5" x2="-0.3" y2="0" width="0.2032" layer="51"/>
<wire x1="-0.3" y1="0" x2="-0.3" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-0.3" y1="0" x2="0.3" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="0.3" y1="-0.5" x2="0.3" y2="0.5" width="0.2032" layer="51"/>
<wire x1="0.3" y1="0.5" x2="-0.3" y2="0" width="0.2032" layer="51"/>
<smd name="A" x="1.5" y="0" dx="1.3" dy="1.4" layer="1" roundness="25" rot="R180"/>
<smd name="C" x="-1.5" y="0" dx="1.3" dy="1.4" layer="1" roundness="25" rot="R180"/>
<text x="-2.1" y="2.2" size="0.762" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="0" y="-1.2" size="0.762" layer="21" font="vector" ratio="15" align="top-center">&gt;LABEL</text>
<text x="-2.1" y="1.2" size="0.762" layer="27" font="vector" ratio="15">&gt;VALUE</text>
<wire x1="-1.6" y1="0.8" x2="-1.6" y2="-0.8" width="0.2032" layer="51"/>
<wire x1="-1.6" y1="-0.8" x2="1.6" y2="-0.8" width="0.2032" layer="51"/>
<wire x1="1.6" y1="-0.8" x2="1.6" y2="0.8" width="0.2032" layer="51"/>
<wire x1="1.6" y1="0.8" x2="-1.6" y2="0.8" width="0.2032" layer="51"/>
<wire x1="-0.3" y1="-0.5" x2="-0.3" y2="0" width="0.2032" layer="21"/>
<wire x1="-0.3" y1="0" x2="-0.3" y2="0.5" width="0.2032" layer="21"/>
<wire x1="-0.3" y1="0" x2="0.3" y2="-0.5" width="0.2032" layer="21"/>
<wire x1="0.3" y1="-0.5" x2="0.3" y2="0.5" width="0.2032" layer="21"/>
<wire x1="0.3" y1="0.5" x2="-0.3" y2="0" width="0.2032" layer="21"/>
<rectangle x1="-1.6" y1="-0.7" x2="-1" y2="0.7" layer="51"/>
<rectangle x1="1" y1="-0.7" x2="1.6" y2="0.7" layer="51"/>
<text x="0" y="-1.2" size="0.762" layer="51" font="vector" ratio="15" align="top-center">&gt;LABEL</text>
<polygon width="0.2032" layer="200">
<vertex x="-1.1" y="0.8"/>
<vertex x="1.1" y="0.8"/>
<vertex x="1.1" y="-0.8"/>
<vertex x="-1.1" y="-0.8"/>
</polygon>
</package>
<package name="LED-1608">
<description>SMD LED&lt;br&gt;
Size: 1.6 x 0.8mm&lt;br&gt;
&lt;br&gt;
Same with size 0603 in inches.&lt;br&gt;
&lt;br&gt;
Created by: OoiBP</description>
<wire x1="-0.8" y1="0.4" x2="-0.8" y2="-0.4" width="0.2032" layer="51"/>
<wire x1="-0.8" y1="-0.4" x2="0.8" y2="-0.4" width="0.2032" layer="51"/>
<wire x1="0.8" y1="-0.4" x2="0.8" y2="0.4" width="0.2032" layer="51"/>
<wire x1="0.8" y1="0.4" x2="-0.8" y2="0.4" width="0.2032" layer="51"/>
<smd name="A" x="-0.9" y="0" dx="0.8" dy="1" layer="1" roundness="50"/>
<smd name="C" x="0.9" y="0" dx="0.8" dy="1" layer="1" roundness="50"/>
<rectangle x1="-0.8" y1="-0.4" x2="-0.6" y2="0.4" layer="51"/>
<rectangle x1="0.6" y1="-0.4" x2="0.8" y2="0.4" layer="51"/>
<text x="0" y="1.778" size="0.762" layer="25" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="0" y="0.762" size="0.762" layer="27" ratio="15" rot="R180" align="top-center">&gt;VALUE</text>
<wire x1="-0.3" y1="0.2" x2="-0.3" y2="-0.2" width="0.2032" layer="21"/>
<wire x1="-0.3" y1="-0.2" x2="0.2" y2="0" width="0.2032" layer="21"/>
<wire x1="0.2" y1="0" x2="-0.3" y2="0.2" width="0.2032" layer="21"/>
<wire x1="0.3" y1="0.2" x2="0.3" y2="-0.2" width="0.2032" layer="21"/>
<text x="0" y="-0.889" size="0.8128" layer="21" font="vector" ratio="15" align="top-center">&gt;LABEL</text>
</package>
<package name="LED-5MM-LONGPAD">
<description>Through Hole LED (Long Pad)&lt;br&gt;
Size: 5mm x 5mm&lt;br&gt;
Height: 9mm</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<pad name="A" x="-1.27" y="0" drill="0.762" diameter="1.651" shape="long" rot="R90"/>
<pad name="K" x="1.27" y="0" drill="0.762" diameter="1.651" shape="long" rot="R90"/>
<text x="-3.5814" y="-1.651" size="0.762" layer="25" font="vector" ratio="15" rot="R90">&gt;NAME</text>
<text x="3.7084" y="-2.0066" size="0.762" layer="21" font="vector" ratio="15" rot="R90">&gt;LABEL</text>
<text x="3.7084" y="-2.0066" size="0.762" layer="51" font="vector" ratio="15" rot="R90">&gt;LABEL</text>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="51"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="51" curve="-286.260205" cap="flat"/>
</package>
<package name="LED-2012">
<description>SMD LED&lt;br&gt;
Size: 2.0 x 1.25mm&lt;br&gt;
&lt;br&gt;
Same with size 0805 in inches.&lt;br&gt;
&lt;br&gt;
Created by: SC Lim</description>
<smd name="A" x="-1" y="0" dx="1.2" dy="1.6" layer="1" roundness="75"/>
<smd name="C" x="1" y="0" dx="1.2" dy="1.6" layer="1" roundness="75"/>
<text x="0" y="1.95" size="0.762" layer="25" font="vector" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="0" y="0.95" size="0.762" layer="27" font="vector" ratio="15" rot="R180" align="top-center">&gt;VALUE</text>
<wire x1="-1" y1="0.6" x2="-1" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="-1" y1="-0.6" x2="1" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.6" x2="1" y2="0.6" width="0.2032" layer="51"/>
<wire x1="1" y1="0.6" x2="-1" y2="0.6" width="0.2032" layer="51"/>
<rectangle x1="-1" y1="-0.6" x2="-0.5" y2="0.6" layer="51"/>
<rectangle x1="0.5" y1="-0.6" x2="1" y2="0.6" layer="51"/>
<wire x1="-0.2" y1="-0.3" x2="-0.2" y2="0.3" width="0.127" layer="51"/>
<wire x1="-0.2" y1="0.3" x2="0.2" y2="0" width="0.127" layer="51"/>
<wire x1="0.2" y1="0" x2="-0.2" y2="-0.3" width="0.127" layer="51"/>
<wire x1="0.19" y1="0.3" x2="0.19" y2="-0.3" width="0.127" layer="51"/>
<text x="0" y="-0.95" size="0.762" layer="51" font="vector" ratio="15" align="top-center">&gt;LABEL</text>
</package>
<package name="3225">
<description>SMD resistor, capacitor and other devices.&lt;br&gt;
Size: 3.2 x 2.5mm&lt;br&gt;
&lt;br&gt;
Same with size 1210 in inches.&lt;br&gt;
&lt;br&gt;
Created by: Wai Weng</description>
<smd name="1" x="-1.6" y="0" dx="1.3" dy="2.7" layer="1" roundness="30"/>
<smd name="2" x="1.6" y="0" dx="1.3" dy="2.7" layer="1" roundness="30"/>
<text x="0" y="2.032" size="0.762" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<text x="0" y="-2.032" size="0.762" layer="27" font="vector" ratio="15" align="center">&gt;VALUE</text>
<wire x1="-1.6" y1="1.25" x2="-1.6" y2="-1.25" width="0.2032" layer="51"/>
<wire x1="-1.6" y1="-1.25" x2="1.6" y2="-1.25" width="0.2032" layer="51"/>
<wire x1="1.6" y1="-1.25" x2="1.6" y2="1.25" width="0.2032" layer="51"/>
<wire x1="1.6" y1="1.25" x2="-1.6" y2="1.25" width="0.2032" layer="51"/>
<rectangle x1="-1.6" y1="-1.25" x2="-1.1" y2="1.25" layer="51"/>
<rectangle x1="1.1" y1="-1.25" x2="1.6" y2="1.25" layer="51"/>
<wire x1="0" y1="0.3" x2="0" y2="-0.3" width="0.55" layer="21"/>
</package>
<package name="CD54">
<description>SMD Inductor&lt;br&gt;
Diameter: 5.8mm</description>
<smd name="1" x="-1.95" y="0" dx="5.5" dy="2.2" layer="1" roundness="30" rot="R90"/>
<smd name="2" x="1.95" y="0" dx="5.5" dy="2.2" layer="1" roundness="30" rot="R90"/>
<wire x1="-0.6" y1="2.3" x2="0.6" y2="2.3" width="0.2032" layer="21"/>
<wire x1="-0.6" y1="-2.3" x2="0.6" y2="-2.3" width="0.2032" layer="21"/>
<text x="0" y="3" size="0.762" layer="25" font="vector" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3" size="0.762" layer="27" font="vector" ratio="15" align="top-center">&gt;VALUE</text>
<wire x1="-1.25" y1="2.6" x2="1.25" y2="2.6" width="0.2032" layer="51"/>
<wire x1="-1.25" y1="-2.6" x2="1.25" y2="-2.6" width="0.2032" layer="51"/>
<wire x1="1.25" y1="2.6" x2="1.25" y2="-2.6" width="0.2032" layer="51" curve="-130"/>
<wire x1="-1.25" y1="-2.6" x2="-1.25" y2="2.6" width="0.2032" layer="51" curve="-130"/>
</package>
<package name="L-JDO5022">
<description>JDO5022 SMD Inductor&lt;br&gt;
Size: 18.5mm x 4.5mm</description>
<smd name="1" x="-8" y="0" dx="4" dy="4" layer="1" roundness="20"/>
<smd name="2" x="8" y="0" dx="4" dy="4" layer="1" roundness="20"/>
<wire x1="-9.25" y1="2.25" x2="-9.25" y2="-2.25" width="0.2032" layer="51"/>
<wire x1="9.25" y1="2.25" x2="9.25" y2="-2.25" width="0.2032" layer="51"/>
<wire x1="-9.25" y1="2.25" x2="-3.85" y2="5.85" width="0.2032" layer="21"/>
<wire x1="-3.85" y1="5.85" x2="3.85" y2="5.85" width="0.2032" layer="21" curve="-67.380135"/>
<wire x1="3.85" y1="5.85" x2="9.25" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-9.25" y1="-2.25" x2="-3.85" y2="-5.85" width="0.2032" layer="21"/>
<wire x1="-3.85" y1="-5.85" x2="3.85" y2="-5.85" width="0.2032" layer="21" curve="67.380135"/>
<wire x1="3.85" y1="-5.85" x2="9.25" y2="-2.25" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="6.4" width="0.2032" layer="51"/>
<wire x1="-9.25" y1="2.25" x2="-3.85" y2="5.85" width="0.2032" layer="51"/>
<wire x1="-3.85" y1="5.85" x2="3.85" y2="5.85" width="0.2032" layer="51" curve="-67.380135"/>
<wire x1="3.85" y1="5.85" x2="9.25" y2="2.25" width="0.2032" layer="51"/>
<wire x1="-9.25" y1="-2.25" x2="-3.85" y2="-5.85" width="0.2032" layer="51"/>
<wire x1="-3.85" y1="-5.85" x2="3.85" y2="-5.85" width="0.2032" layer="51" curve="67.380135"/>
<wire x1="3.85" y1="-5.85" x2="9.25" y2="-2.25" width="0.2032" layer="51"/>
<wire x1="-5.9" y1="2.5" x2="5.9" y2="2.5" width="0.2032" layer="21" curve="-134"/>
<wire x1="5.9" y1="-2.5" x2="-5.9" y2="-2.5" width="0.2032" layer="21" curve="-134"/>
<text x="0" y="0.3" size="0.762" layer="25" font="vector" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.1" size="0.762" layer="27" font="vector" ratio="15" align="bottom-center">&gt;VALUE</text>
</package>
<package name="L-JDO3340">
<description>JDO3340 SMD Inductor&lt;br&gt;
Size: 12.95mm x 9.40mm</description>
<smd name="1" x="-5.4" y="0" dx="3.7" dy="3" layer="1" roundness="20"/>
<smd name="2" x="5.4" y="0" dx="3.7" dy="3" layer="1" roundness="20"/>
<wire x1="-5.675" y1="1.87" x2="-2" y2="4.7" width="0.2032" layer="21"/>
<wire x1="2" y1="4.7" x2="5.675" y2="1.87" width="0.2032" layer="21"/>
<wire x1="-5.675" y1="-1.87" x2="-2" y2="-4.7" width="0.2032" layer="21"/>
<wire x1="2" y1="-4.7" x2="5.675" y2="-1.87" width="0.2032" layer="21"/>
<text x="0" y="0.3" size="0.762" layer="25" font="vector" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.1" size="0.762" layer="27" font="vector" ratio="15" align="bottom-center">&gt;VALUE</text>
<wire x1="-2" y1="4.7" x2="2" y2="4.7" width="0.2032" layer="21"/>
<wire x1="-2" y1="-4.7" x2="2" y2="-4.7" width="0.2032" layer="21"/>
<wire x1="-6.475" y1="1.27" x2="-6.475" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="6.475" y1="1.27" x2="6.475" y2="-1.27" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="4.2" width="0.2032" layer="51"/>
<wire x1="-6.475" y1="1.27" x2="-2" y2="4.7" width="0.2032" layer="51"/>
<wire x1="2" y1="4.7" x2="6.475" y2="1.27" width="0.2032" layer="51"/>
<wire x1="-6.475" y1="-1.27" x2="-2" y2="-4.7" width="0.2032" layer="51"/>
<wire x1="2" y1="-4.7" x2="6.475" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="-2" y1="4.7" x2="2" y2="4.7" width="0.2032" layer="51"/>
<wire x1="-2" y1="-4.7" x2="2" y2="-4.7" width="0.2032" layer="51"/>
<rectangle x1="-6.475" y1="-1.27" x2="-4.025" y2="1.27" layer="51"/>
<rectangle x1="4.025" y1="-1.27" x2="6.475" y2="1.27" layer="51"/>
<wire x1="3.7" y1="2" x2="-3.7" y2="2" width="0.2032" layer="21" curve="123.213961"/>
<wire x1="-3.7" y1="-2" x2="3.7" y2="-2" width="0.2032" layer="21" curve="123.213961"/>
</package>
<package name="L-SDRH8D43">
<description>SMD Inductor&lt;br&gt;
Diameter: 8.3mm x 8.3mm</description>
<smd name="P$1" x="-4.05" y="0" dx="2" dy="2.8" layer="1" roundness="50"/>
<smd name="P$2" x="4.05" y="0" dx="2" dy="2.8" layer="1" roundness="50"/>
<circle x="0" y="0" radius="4.15" width="0.2032" layer="51"/>
<text x="-2.032" y="0.254" size="0.762" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-2.032" y="-1.016" size="0.762" layer="27" font="vector" ratio="15">&gt;VALUE</text>
<wire x1="-4.15" y1="-1.778" x2="-4.15" y2="1.778" width="0.2032" layer="51"/>
<wire x1="-4.15" y1="1.778" x2="-1.778" y2="4.15" width="0.2032" layer="51"/>
<wire x1="-1.778" y1="4.15" x2="1.778" y2="4.15" width="0.2032" layer="51"/>
<wire x1="1.778" y1="4.15" x2="4.15" y2="1.778" width="0.2032" layer="51"/>
<wire x1="4.15" y1="1.778" x2="4.15" y2="-1.778" width="0.2032" layer="51"/>
<wire x1="4.15" y1="-1.778" x2="1.778" y2="-4.15" width="0.2032" layer="51"/>
<wire x1="1.778" y1="-4.15" x2="-1.778" y2="-4.15" width="0.2032" layer="51"/>
<wire x1="-1.778" y1="-4.15" x2="-4.15" y2="-1.778" width="0.2032" layer="51"/>
<wire x1="-4.15" y1="1.778" x2="-1.778" y2="4.15" width="0.2032" layer="21"/>
<wire x1="-1.778" y1="4.15" x2="1.778" y2="4.15" width="0.2032" layer="21"/>
<wire x1="1.778" y1="4.15" x2="4.15" y2="1.778" width="0.2032" layer="21"/>
<wire x1="4.15" y1="-1.778" x2="1.778" y2="-4.15" width="0.2032" layer="21"/>
<wire x1="1.778" y1="-4.15" x2="-1.778" y2="-4.15" width="0.2032" layer="21"/>
<wire x1="-1.778" y1="-4.15" x2="-4.15" y2="-1.778" width="0.2032" layer="21"/>
</package>
<package name="L-SDRH5D28">
<description>SMD Inductor&lt;br&gt;
Size: 6mm x 6mm&lt;br&gt;
&lt;br&gt;
Created by: Wai Weng</description>
<smd name="1" x="-2.3" y="0" dx="6.5" dy="3" layer="1" roundness="30" rot="R90"/>
<smd name="2" x="2.3" y="0" dx="6.5" dy="3" layer="1" roundness="30" rot="R90"/>
<text x="0" y="3.5" size="0.762" layer="25" font="vector" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.5" size="0.762" layer="27" font="vector" ratio="15" align="top-center">&gt;VALUE</text>
<wire x1="-3" y1="3" x2="3" y2="3" width="0.2032" layer="51"/>
<wire x1="3" y1="3" x2="3" y2="-3" width="0.2032" layer="51"/>
<wire x1="3" y1="-3" x2="-3" y2="-3" width="0.2032" layer="51"/>
<wire x1="-3" y1="-3" x2="-3" y2="3" width="0.2032" layer="51"/>
<rectangle x1="-3" y1="-3" x2="-1" y2="3" layer="51"/>
<rectangle x1="1" y1="-3" x2="3" y2="3" layer="51"/>
</package>
<package name="4532">
<description>Fuse.&lt;br&gt;
Size: 4.5 x 3.2mm&lt;br&gt;
&lt;br&gt;
Same with size 1812 in inches.</description>
<smd name="1" x="-2.1" y="0" dx="1.5" dy="3.2" layer="1" roundness="50"/>
<smd name="2" x="2.1" y="0" dx="1.5" dy="3.2" layer="1" roundness="50"/>
<wire x1="-1.016" y1="1.524" x2="1.016" y2="1.524" width="0.2032" layer="51"/>
<wire x1="1.016" y1="1.524" x2="1.016" y2="-1.524" width="0.2032" layer="51"/>
<wire x1="1.016" y1="-1.524" x2="-1.016" y2="-1.524" width="0.2032" layer="51"/>
<wire x1="-1.016" y1="-1.524" x2="-1.016" y2="1.524" width="0.2032" layer="51"/>
<wire x1="-1.016" y1="1.524" x2="1.016" y2="1.524" width="0.2032" layer="21"/>
<wire x1="1.016" y1="1.524" x2="1.016" y2="-1.524" width="0.2032" layer="21"/>
<wire x1="1.016" y1="-1.524" x2="-1.016" y2="-1.524" width="0.2032" layer="21"/>
<wire x1="-1.016" y1="-1.524" x2="-1.016" y2="1.524" width="0.2032" layer="21"/>
<text x="-0.0508" y="1.778" size="0.762" layer="25" font="vector" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="0.0762" y="-1.778" size="0.762" layer="27" font="vector" ratio="15" align="top-center">&gt;VALUE</text>
</package>
<package name="L-120120-SHIELDED">
<description>SMD Inductor (Shielded)&lt;br&gt;
Size: 12mm x 12mm&lt;br&gt;
&lt;br&gt;
Created by: Wai Weng</description>
<wire x1="-6" y1="-5" x2="-6" y2="5" width="0.2032" layer="51"/>
<wire x1="-6" y1="5" x2="-5" y2="6" width="0.2032" layer="51" curve="-90"/>
<wire x1="-5" y1="6" x2="5" y2="6" width="0.2032" layer="51"/>
<wire x1="5" y1="6" x2="6" y2="5" width="0.2032" layer="51" curve="-90"/>
<wire x1="6" y1="5" x2="6" y2="-5" width="0.2032" layer="51"/>
<wire x1="6" y1="-5" x2="5" y2="-6" width="0.2032" layer="51" curve="-90"/>
<wire x1="5" y1="-6" x2="-5" y2="-6" width="0.2032" layer="51"/>
<wire x1="-5" y1="-6" x2="-6" y2="-5" width="0.2032" layer="51" curve="-90"/>
<smd name="1" x="0" y="5.5" dx="5.4" dy="4" layer="1" roundness="20"/>
<smd name="2" x="0" y="-5.5" dx="5.4" dy="4" layer="1" roundness="20"/>
<text x="0" y="1" size="0.762" layer="25" font="vector" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1" size="0.762" layer="27" font="vector" ratio="15" align="top-center">&gt;VALUE</text>
<wire x1="3.1" y1="5.6" x2="4.878890625" y2="5.6" width="0.2032" layer="21"/>
<wire x1="4.878890625" y1="5.6" x2="5.6" y2="4.878890625" width="0.2032" layer="21" curve="-90"/>
<wire x1="5.6" y1="4.878890625" x2="5.6" y2="-4.878890625" width="0.2032" layer="21"/>
<wire x1="5.6" y1="-4.878890625" x2="4.878890625" y2="-5.6" width="0.2032" layer="21" curve="-90"/>
<wire x1="4.878890625" y1="-5.6" x2="3.1" y2="-5.6" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-5.6" x2="-4.751471875" y2="-5.6" width="0.2032" layer="21"/>
<wire x1="-4.751471875" y1="-5.6" x2="-5.6" y2="-4.751471875" width="0.2032" layer="21" curve="-90"/>
<wire x1="-5.6" y1="-4.751471875" x2="-5.6" y2="4.878890625" width="0.2032" layer="21"/>
<wire x1="-5.6" y1="4.878890625" x2="-4.878890625" y2="5.6" width="0.2032" layer="21" curve="-90"/>
<wire x1="-4.878890625" y1="5.6" x2="-3.1" y2="5.6" width="0.2032" layer="21"/>
</package>
<package name="D-SMA">
<description>SMD Diode&lt;br&gt;
Size: 5.0 x 2.7mm</description>
<wire x1="1.8" y1="-1.05" x2="-0.8" y2="-1.05" width="0.2032" layer="21"/>
<wire x1="-0.8" y1="-1.05" x2="-1.8" y2="-1.05" width="0.2032" layer="21"/>
<wire x1="-1.8" y1="1.05" x2="-0.8" y2="1.05" width="0.2032" layer="21"/>
<wire x1="-0.8" y1="1.05" x2="1.8" y2="1.05" width="0.2032" layer="21"/>
<wire x1="-0.8" y1="-1.05" x2="-0.8" y2="1.05" width="0.2032" layer="21"/>
<smd name="A" x="2" y="0" dx="1.9" dy="1.6" layer="1" roundness="40"/>
<smd name="C" x="-2" y="0" dx="1.9" dy="1.6" layer="1" roundness="40" rot="R180"/>
<text x="0" y="1.7" size="0.762" layer="25" font="vector" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.7" size="0.762" layer="27" font="vector" ratio="15" align="top-center">&gt;MPN</text>
<wire x1="-2.15" y1="1.35" x2="-2.15" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-1.35" x2="-1.1" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="-1.35" x2="2.15" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="2.15" y1="-1.35" x2="2.15" y2="1.35" width="0.2032" layer="51"/>
<wire x1="2.15" y1="1.35" x2="-1.1" y2="1.35" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.35" x2="-2.15" y2="1.35" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="-1.35" x2="-1.1" y2="1.35" width="0.2032" layer="51"/>
<rectangle x1="-2.5" y1="-0.75" x2="-1.4" y2="0.75" layer="51"/>
<rectangle x1="1.4" y1="-0.75" x2="2.5" y2="0.75" layer="51"/>
</package>
<package name="D-SMB">
<description>SMD Diode&lt;br&gt;
Size: 5.5 x 3.6mm</description>
<wire x1="1.8" y1="-1.5" x2="-0.8" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="-0.8" y1="-1.5" x2="-1.8" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="-1.8" y1="1.5" x2="-0.8" y2="1.5" width="0.2032" layer="21"/>
<wire x1="-0.8" y1="1.5" x2="1.8" y2="1.5" width="0.2032" layer="21"/>
<wire x1="-0.8" y1="-1.5" x2="-0.8" y2="1.5" width="0.2032" layer="21"/>
<smd name="A" x="2.2" y="0" dx="2" dy="2.5" layer="1" roundness="30"/>
<smd name="C" x="-2.2" y="0" dx="2" dy="2.5" layer="1" roundness="30" rot="R180"/>
<text x="0" y="2.1" size="0.762" layer="25" font="vector" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.1" size="0.762" layer="27" font="vector" ratio="15" align="top-center">&gt;MPN</text>
<wire x1="-2.15" y1="1.8" x2="-2.15" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-1.8" x2="-1.1" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="-1.8" x2="2.15" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="2.15" y1="-1.8" x2="2.15" y2="1.8" width="0.2032" layer="51"/>
<wire x1="2.15" y1="1.8" x2="-1.1" y2="1.8" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.8" x2="-2.15" y2="1.8" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="-1.8" x2="-1.1" y2="1.8" width="0.2032" layer="51"/>
<rectangle x1="-2.75" y1="-1.1" x2="-1.6" y2="1.1" layer="51"/>
<rectangle x1="1.6" y1="-1.1" x2="2.75" y2="1.1" layer="51"/>
</package>
<package name="D-SMC">
<description>SMD Diode&lt;br&gt;
Size: 6.88 x 5.9mm</description>
<wire x1="2.8" y1="-2.5" x2="-1.8" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="-1.8" y1="-2.5" x2="-2.8" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="-2.8" y1="2.5" x2="-1.8" y2="2.5" width="0.2032" layer="21"/>
<wire x1="-1.8" y1="2.5" x2="2.8" y2="2.5" width="0.2032" layer="21"/>
<wire x1="-1.8" y1="-2.5" x2="-1.8" y2="2.5" width="0.2032" layer="21"/>
<smd name="A" x="3.5" y="0" dx="2" dy="3.2" layer="1" roundness="30"/>
<smd name="C" x="-3.5" y="0" dx="2" dy="3.2" layer="1" roundness="30" rot="R180"/>
<text x="0" y="3.3" size="0.762" layer="25" font="vector" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.3" size="0.762" layer="27" font="vector" ratio="15" align="top-center">&gt;MPN</text>
<wire x1="-3.44" y1="2.95" x2="-3.44" y2="-2.95" width="0.2032" layer="51"/>
<wire x1="-3.44" y1="-2.95" x2="-2.1" y2="-2.95" width="0.2032" layer="51"/>
<wire x1="-2.1" y1="-2.95" x2="3.44" y2="-2.95" width="0.2032" layer="51"/>
<wire x1="3.44" y1="-2.95" x2="3.44" y2="2.95" width="0.2032" layer="51"/>
<wire x1="3.44" y1="2.95" x2="-2.1" y2="2.95" width="0.2032" layer="51"/>
<wire x1="-2.1" y1="2.95" x2="-3.44" y2="2.95" width="0.2032" layer="51"/>
<wire x1="-2.1" y1="-2.95" x2="-2.1" y2="2.95" width="0.2032" layer="51"/>
<rectangle x1="-3.98" y1="-1.5" x2="-2.85" y2="1.5" layer="51"/>
<rectangle x1="2.85" y1="-1.5" x2="3.98" y2="1.5" layer="51"/>
</package>
<package name="SW-SLIDE-DPDT-AYZ0202AGRLC-SMD">
<description>SMD DPDT Slide Switch - AYZ0202AGRLC&lt;br&gt;
&lt;br&gt;
Modification:&lt;br&gt;
1. SC - Renamed to follow common standard.</description>
<smd name="1" x="-2.5" y="2.825" dx="1" dy="1.15" layer="1" roundness="50" rot="R180"/>
<smd name="2" x="0" y="2.825" dx="1" dy="1.15" layer="1" roundness="50"/>
<smd name="3" x="2.5" y="2.825" dx="1" dy="1.15" layer="1" roundness="50" rot="R180"/>
<smd name="4" x="-2.5" y="-2.825" dx="1" dy="1.15" layer="1" roundness="50" rot="R180"/>
<smd name="5" x="0" y="-2.825" dx="1" dy="1.15" layer="1" roundness="50" rot="R180"/>
<smd name="6" x="2.5" y="-2.825" dx="1" dy="1.15" layer="1" roundness="50" rot="R180"/>
<hole x="-1.5" y="0" drill="0.85"/>
<hole x="1.5" y="0" drill="0.85"/>
<wire x1="-3.6" y1="1.75" x2="-3.6" y2="-1.75" width="0.2032" layer="51"/>
<wire x1="-3.6" y1="-1.75" x2="3.6" y2="-1.75" width="0.2032" layer="51"/>
<wire x1="3.6" y1="-1.75" x2="3.6" y2="1.75" width="0.2032" layer="51"/>
<wire x1="3.6" y1="1.75" x2="-3.6" y2="1.75" width="0.2032" layer="51"/>
<rectangle x1="-2.75" y1="1.7" x2="-2.25" y2="3" layer="51"/>
<rectangle x1="-0.25" y1="1.7" x2="0.25" y2="3" layer="51"/>
<rectangle x1="2.25" y1="1.7" x2="2.75" y2="3" layer="51"/>
<rectangle x1="-2.75" y1="-3" x2="-2.25" y2="-1.7" layer="51" rot="R180"/>
<rectangle x1="-0.25" y1="-3" x2="0.25" y2="-1.7" layer="51" rot="R180"/>
<rectangle x1="2.25" y1="-3" x2="2.75" y2="-1.7" layer="51" rot="R180"/>
<wire x1="-1.4" y1="-1.7" x2="-1.4" y2="-0.4" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-0.4" x2="1.4" y2="-0.4" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-0.4" x2="1.4" y2="-1.7" width="0.2032" layer="51"/>
<rectangle x1="-0.2" y1="-1.5" x2="1.4" y2="-0.4" layer="51"/>
<wire x1="-3.5" y1="1.7" x2="3.5" y2="1.7" width="0.2032" layer="21"/>
<wire x1="3.5" y1="1.7" x2="3.5" y2="-1.7" width="0.2032" layer="21"/>
<wire x1="3.5" y1="-1.7" x2="-3.5" y2="-1.7" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="-1.7" x2="-3.5" y2="1.7" width="0.2032" layer="21"/>
</package>
<package name="SW-SLIDE-DPDT-MSS22D18-SMD">
<description>SMD DPDT Slide Switch - MSS22D18&lt;br&gt;
&lt;br&gt;
Created by: SC Lim</description>
<wire x1="-4.6" y1="1.9" x2="-4.6" y2="-1.9" width="0.2032" layer="51"/>
<wire x1="-4.6" y1="-1.9" x2="4.6" y2="-1.9" width="0.2032" layer="51"/>
<wire x1="4.6" y1="-1.9" x2="4.6" y2="1.9" width="0.2032" layer="51"/>
<wire x1="4.6" y1="1.9" x2="-4.6" y2="1.9" width="0.2032" layer="51"/>
<wire x1="-2.1" y1="1" x2="2.1" y2="1" width="0.2032" layer="51"/>
<wire x1="2.1" y1="1" x2="2.1" y2="-1" width="0.2032" layer="51"/>
<wire x1="2.1" y1="-1" x2="-2.1" y2="-1" width="0.2032" layer="51"/>
<wire x1="-2.1" y1="-1" x2="-2.1" y2="1" width="0.2032" layer="51"/>
<rectangle x1="-1.8" y1="-0.7" x2="-0.4" y2="0.7" layer="51"/>
<rectangle x1="-0.33" y1="1.9" x2="0.33" y2="3.8" layer="51"/>
<rectangle x1="2.17" y1="1.9" x2="2.83" y2="3.8" layer="51"/>
<rectangle x1="-2.83" y1="1.9" x2="-2.17" y2="3.8" layer="51"/>
<rectangle x1="-0.33" y1="-3.8" x2="0.33" y2="-1.9" layer="51" rot="R180"/>
<rectangle x1="2.17" y1="-3.8" x2="2.83" y2="-1.9" layer="51" rot="R180"/>
<rectangle x1="-2.83" y1="-3.8" x2="-2.17" y2="-1.9" layer="51" rot="R180"/>
<smd name="1" x="-2.5" y="3" dx="2" dy="1.2" layer="1" roundness="30" rot="R90"/>
<smd name="2" x="0" y="3" dx="2" dy="1.2" layer="1" roundness="30" rot="R90"/>
<smd name="3" x="2.5" y="3" dx="2" dy="1.2" layer="1" roundness="30" rot="R90"/>
<smd name="4" x="-2.5" y="-3" dx="2" dy="1.2" layer="1" roundness="30" rot="R90"/>
<smd name="5" x="0" y="-3" dx="2" dy="1.2" layer="1" roundness="30" rot="R90"/>
<smd name="6" x="2.5" y="-3" dx="2" dy="1.2" layer="1" roundness="30" rot="R90"/>
<wire x1="-4.4" y1="1.7" x2="-4.4" y2="-1.7" width="0.2032" layer="21"/>
<wire x1="-4.4" y1="-1.7" x2="4.4" y2="-1.7" width="0.2032" layer="21"/>
<wire x1="4.4" y1="-1.7" x2="4.4" y2="1.7" width="0.2032" layer="21"/>
<wire x1="4.4" y1="1.7" x2="-4.4" y2="1.7" width="0.2032" layer="21"/>
<wire x1="-2.1" y1="1" x2="2.1" y2="1" width="0.2032" layer="21"/>
<wire x1="2.1" y1="1" x2="2.1" y2="-1" width="0.2032" layer="21"/>
<wire x1="2.1" y1="-1" x2="-2.1" y2="-1" width="0.2032" layer="21"/>
<wire x1="-2.1" y1="-1" x2="-2.1" y2="1" width="0.2032" layer="21"/>
<rectangle x1="-1.8" y1="-0.7" x2="-0.4" y2="0.7" layer="21"/>
<text x="-2.54" y="4.318" size="0.762" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-2.54" y="-5.08" size="0.762" layer="51" font="vector" ratio="15">&gt;Label</text>
</package>
<package name="ECAP-CHEMICON-F">
<description>SMD Aluminium Electrolytic Capacitor&lt;br&gt;
Size: 6.6 x 6.6mm
&lt;br&gt;&lt;br&gt;
Related Cap Value:&lt;br&gt;
16V: 100, 150, 220uF&lt;br&gt;
25V: 47, 56, 100uF&lt;br&gt;
35V: 33, 100uF&lt;br&gt;
50V: 22, 33, 47uF&lt;br&gt;</description>
<smd name="+" x="2.4" y="0" dx="3.2" dy="1.3" layer="1" roundness="50"/>
<smd name="-" x="-2.4" y="0" dx="3.2" dy="1.3" layer="1" roundness="50"/>
<wire x1="3.2" y1="-0.9" x2="-3.2" y2="-0.9" width="0.2032" layer="21" curve="-147"/>
<wire x1="-3.3" y1="-0.9" x2="-3.3" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="-3.3" y1="-3.3" x2="2.2" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="2.2" y1="-3.3" x2="3.3" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="3.3" y1="-2.2" x2="3.3" y2="-0.9" width="0.2032" layer="21"/>
<wire x1="3.3" y1="0.9" x2="3.3" y2="2.2" width="0.2032" layer="21"/>
<wire x1="3.3" y1="2.2" x2="2.2" y2="3.3" width="0.2032" layer="21"/>
<wire x1="2.2" y1="3.3" x2="-3.3" y2="3.3" width="0.2032" layer="21"/>
<wire x1="-3.3" y1="3.3" x2="-3.3" y2="0.9" width="0.2032" layer="21"/>
<text x="0" y="1" size="0.762" layer="25" font="vector" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1" size="0.762" layer="27" font="vector" ratio="15" align="top-center">&gt;VALUE</text>
<wire x1="-3.2" y1="0.9" x2="3.2" y2="0.9" width="0.2032" layer="21" curve="-147"/>
<wire x1="-3.3" y1="3.3" x2="-3.3" y2="-3.3" width="0.2032" layer="51"/>
<wire x1="-3.3" y1="-3.3" x2="2.2" y2="-3.3" width="0.2032" layer="51"/>
<wire x1="2.2" y1="-3.3" x2="3.3" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="3.3" y1="-2.2" x2="3.3" y2="2.2" width="0.2032" layer="51"/>
<wire x1="3.3" y1="2.2" x2="2.2" y2="3.3" width="0.2032" layer="51"/>
<wire x1="2.2" y1="3.3" x2="-3.3" y2="3.3" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="3.3" width="0.2032" layer="51"/>
<rectangle x1="-3.7" y1="-0.4" x2="-0.95" y2="0.4" layer="51"/>
<rectangle x1="0.95" y1="-0.4" x2="3.7" y2="0.4" layer="51"/>
</package>
<package name="ECAP-CHEMICON-J">
<description>SMD Aluminium Electrolytic Capacitor&lt;br&gt;
Size: 10.3 x 10.3mm
&lt;br&gt;&lt;br&gt;
Related Cap Value:&lt;br&gt;
16V: 680uF&lt;br&gt;
25V: 470uF&lt;br&gt;
35V: 330uF&lt;br&gt;
50V: 220uF&lt;br&gt;</description>
<wire x1="-5.15" y1="1" x2="-5.15" y2="5.15" width="0.2032" layer="21"/>
<wire x1="-5.15" y1="5.15" x2="2.8" y2="5.15" width="0.2032" layer="21"/>
<wire x1="2.8" y1="5.15" x2="5.15" y2="2.8" width="0.2032" layer="21"/>
<wire x1="5.15" y1="2.8" x2="5.15" y2="1" width="0.2032" layer="21"/>
<wire x1="5.15" y1="-1" x2="5.15" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="5.15" y1="-2.8" x2="2.8" y2="-5.15" width="0.2032" layer="21"/>
<wire x1="2.8" y1="-5.15" x2="-5.15" y2="-5.15" width="0.2032" layer="21"/>
<wire x1="-5.15" y1="-5.15" x2="-5.15" y2="-1" width="0.2032" layer="21"/>
<wire x1="-5.1" y1="-1" x2="5.1" y2="-1" width="0.2032" layer="21" curve="156.699401"/>
<wire x1="-5.1" y1="1" x2="5.1" y2="1" width="0.2032" layer="21" curve="-156.699401"/>
<smd name="-" x="-4" y="0" dx="4.2" dy="1.5" layer="1" roundness="50"/>
<smd name="+" x="4" y="0" dx="4.2" dy="1.5" layer="1" roundness="50"/>
<text x="0" y="1.3" size="0.762" layer="25" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.3" size="0.762" layer="27" ratio="15" align="top-center">&gt;VALUE</text>
<wire x1="-5.15" y1="5.15" x2="-5.15" y2="-5.15" width="0.2032" layer="51"/>
<wire x1="-5.15" y1="-5.15" x2="2.8" y2="-5.15" width="0.2032" layer="51"/>
<wire x1="2.8" y1="-5.15" x2="5.15" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="5.15" y1="-2.8" x2="5.15" y2="2.8" width="0.2032" layer="51"/>
<wire x1="5.15" y1="2.8" x2="2.8" y2="5.15" width="0.2032" layer="51"/>
<wire x1="2.8" y1="5.15" x2="-5.15" y2="5.15" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="5.15" width="0.2032" layer="51"/>
<rectangle x1="-5.6" y1="-0.55" x2="-2.25" y2="0.55" layer="51"/>
<rectangle x1="2.25" y1="-0.55" x2="5.6" y2="0.55" layer="51"/>
</package>
<package name="ECAP-16MM-TH">
<description>Aluminium Electrolytic Capacitor&lt;br&gt;
Diameter: 16mm&lt;br&gt;
Lead Pitch: 7.5mm
&lt;br&gt;&lt;br&gt;
Related Cap Value:&lt;br&gt;
50V: 2200uF&lt;br&gt;</description>
<pad name="+" x="-3.75" y="0" drill="1.143" diameter="2.286"/>
<pad name="-" x="3.75" y="0" drill="1.143" diameter="2.286"/>
<circle x="0" y="0" radius="8" width="0.2032" layer="21"/>
<wire x1="3" y1="7.4" x2="3" y2="1.3" width="0.2032" layer="21"/>
<wire x1="3" y1="-1.3" x2="3" y2="-7.4" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="8" width="0.2032" layer="51"/>
<wire x1="3" y1="7.4" x2="3" y2="-7.4" width="0.2032" layer="51"/>
<text x="0" y="0.2" size="0.762" layer="25" font="vector" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1" size="0.762" layer="27" font="vector" ratio="15" align="bottom-center">&gt;VALUE</text>
</package>
<package name="ECAP-18MM-TH">
<description>Aluminium Electrolytic Capacitor&lt;br&gt;
Diameter: 18mm&lt;br&gt;
Lead Pitch: 7.5mm
&lt;br&gt;&lt;br&gt;
Related Cap Value:&lt;br&gt;
50V: 3300uF&lt;br&gt;</description>
<pad name="+" x="-3.75" y="0" drill="1.143" diameter="3.048"/>
<pad name="-" x="3.75" y="0" drill="1.143" diameter="3.048"/>
<circle x="0" y="0" radius="9" width="0.2032" layer="21"/>
<wire x1="3" y1="8.4" x2="3" y2="2.3" width="0.2032" layer="21"/>
<wire x1="3" y1="-2.3" x2="3" y2="-8.4" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="9" width="0.2032" layer="51"/>
<wire x1="3" y1="8.4" x2="3" y2="-8.4" width="0.2032" layer="51"/>
<text x="0" y="0.2" size="0.762" layer="25" font="vector" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1" size="0.762" layer="27" font="vector" ratio="15" align="bottom-center">&gt;VALUE</text>
</package>
<package name="ECAP-08MM-TH">
<description>Aluminium Electrolytic Capacitor&lt;br&gt;
Diameter: 8mm&lt;br&gt;
Lead Pitch: 3.5mm&lt;br&gt;
&lt;br&gt;
Created by: Wai Weng&lt;br&gt;</description>
<pad name="+" x="-1.75" y="0" drill="1" diameter="1.8"/>
<pad name="-" x="1.75" y="0" drill="1" diameter="1.8"/>
<circle x="0" y="0" radius="4" width="0.2032" layer="21"/>
<wire x1="1.75" y1="3.58" x2="1.75" y2="1.3" width="0.2032" layer="21"/>
<wire x1="1.75" y1="-1.3" x2="1.75" y2="-3.58" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="4" width="0.2032" layer="51"/>
<wire x1="1.75" y1="3.58" x2="1.75" y2="-3.58" width="0.2032" layer="51"/>
<text x="0" y="-5.1" size="0.762" layer="25" font="vector" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="0" y="-6.2" size="0.762" layer="27" font="vector" ratio="15" align="bottom-center">&gt;VALUE</text>
</package>
<package name="ECAP-13MM-TH">
<description>Aluminium Electrolytic Capacitor&lt;br&gt;
Diameter: 13mm&lt;br&gt;
Lead Pitch: 5.08mm&lt;br&gt;
&lt;br&gt;
Related Cap Value:&lt;br&gt;
50V 1000uF&lt;br&gt;</description>
<pad name="+" x="-2.54" y="0" drill="1.143" diameter="2.286"/>
<pad name="-" x="2.54" y="0" drill="1.143" diameter="2.286"/>
<circle x="0" y="0" radius="6.477" width="0.2032" layer="21"/>
<wire x1="2.54" y1="5.9436" x2="2.54" y2="1.397" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.397" x2="2.54" y2="-5.9436" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="6.477" width="0.2032" layer="51"/>
<wire x1="2.54" y1="5.9436" x2="2.54" y2="-5.9436" width="0.2032" layer="51"/>
<text x="0" y="2.74" size="0.762" layer="25" font="vector" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="0" y="1.54" size="0.762" layer="27" font="vector" ratio="15" align="bottom-center">&gt;VALUE</text>
</package>
<package name="ECAP-CHEMICON-K">
<description>SMD Aluminium Electrolytic Capacitor&lt;br&gt;
Size: 13 x 13mm
&lt;br&gt;&lt;br&gt;
Related Cap Value:&lt;br&gt;
50V: 330uF&lt;br&gt;</description>
<wire x1="-6.5" y1="1" x2="-6.5" y2="6.5" width="0.2032" layer="21"/>
<wire x1="-6.5" y1="6.5" x2="4" y2="6.5" width="0.2032" layer="21"/>
<wire x1="4" y1="6.5" x2="6.5" y2="4" width="0.2032" layer="21"/>
<wire x1="6.5" y1="4" x2="6.5" y2="1" width="0.2032" layer="21"/>
<wire x1="6.5" y1="-1" x2="6.5" y2="-4" width="0.2032" layer="21"/>
<wire x1="6.5" y1="-4" x2="4" y2="-6.5" width="0.2032" layer="21"/>
<wire x1="4" y1="-6.5" x2="-6.5" y2="-6.5" width="0.2032" layer="21"/>
<wire x1="-6.5" y1="-6.5" x2="-6.5" y2="-1" width="0.2032" layer="21"/>
<wire x1="-6.5" y1="-1" x2="6.5" y2="-1" width="0.2032" layer="21" curve="160"/>
<wire x1="-6.5" y1="1" x2="6.5" y2="1" width="0.2032" layer="21" curve="-160"/>
<smd name="-" x="-4.8" y="0" dx="7.2" dy="1.5" layer="1" roundness="50"/>
<smd name="+" x="4.8" y="0" dx="7.2" dy="1.5" layer="1" roundness="50"/>
<text x="0" y="1.3" size="0.762" layer="25" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.3" size="0.762" layer="27" ratio="15" align="top-center">&gt;VALUE</text>
<wire x1="-6.5" y1="6.5" x2="-6.5" y2="-6.5" width="0.2032" layer="51"/>
<wire x1="-6.5" y1="-6.5" x2="4" y2="-6.5" width="0.2032" layer="51"/>
<wire x1="4" y1="-6.5" x2="6.5" y2="-4" width="0.2032" layer="51"/>
<wire x1="6.5" y1="-4" x2="6.5" y2="4" width="0.2032" layer="51"/>
<wire x1="6.5" y1="4" x2="4" y2="6.5" width="0.2032" layer="51"/>
<wire x1="4" y1="6.5" x2="-6.5" y2="6.5" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="6.5" width="0.2032" layer="51"/>
<rectangle x1="-7.35" y1="-0.55" x2="-2.1" y2="0.55" layer="51"/>
<rectangle x1="2.1" y1="-0.55" x2="7.35" y2="0.55" layer="51"/>
</package>
<package name="ECAP-10MM-TH">
<description>Aluminium Electrolytic Capacitor&lt;br&gt;
Diameter: 10mm&lt;br&gt;
Lead Pitch: 5.0mm&lt;br&gt;
&lt;br&gt;
Created by: Wai Weng</description>
<pad name="+" x="-2.5" y="0" drill="1" diameter="2.286"/>
<pad name="-" x="2.5" y="0" drill="1" diameter="2.286"/>
<circle x="0" y="0" radius="5" width="0.2032" layer="21"/>
<wire x1="2.54" y1="4.3" x2="2.54" y2="1.397" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.397" x2="2.54" y2="-4.3" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="5" width="0.2032" layer="51"/>
<wire x1="2.54" y1="4.3" x2="2.54" y2="-4.3" width="0.2032" layer="51"/>
<text x="0" y="2.74" size="0.762" layer="25" font="vector" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="0" y="1.54" size="0.762" layer="27" font="vector" ratio="15" align="bottom-center">&gt;VALUE</text>
</package>
<package name="SW-PB-6036-2P">
<description>Momentary Push Button&lt;br&gt;
Size: 6.0 x 3.6mm</description>
<wire x1="-2.75" y1="1.8" x2="-3" y2="1.55" width="0.127" layer="51" curve="90"/>
<wire x1="-2.75" y1="1.8" x2="2.75" y2="1.8" width="0.127" layer="51"/>
<wire x1="2.75" y1="1.8" x2="3" y2="1.55" width="0.127" layer="51" curve="-90"/>
<wire x1="3" y1="-1.55" x2="2.65" y2="-1.8" width="0.127" layer="51" curve="-90"/>
<wire x1="2.65" y1="-1.8" x2="-2.75" y2="-1.8" width="0.127" layer="51"/>
<wire x1="-3" y1="-1.55" x2="-2.75" y2="-1.8" width="0.127" layer="51" curve="90"/>
<wire x1="-3" y1="-1.55" x2="-3" y2="1.55" width="0.127" layer="51"/>
<wire x1="3" y1="-1.55" x2="3" y2="1.55" width="0.127" layer="51"/>
<wire x1="-1.5" y1="0.75" x2="1.5" y2="0.75" width="0.127" layer="51"/>
<wire x1="1.5" y1="-0.75" x2="-1.5" y2="-0.75" width="0.127" layer="51"/>
<wire x1="-1.5" y1="-0.75" x2="-1.5" y2="0.75" width="0.127" layer="51"/>
<wire x1="1.5" y1="-0.75" x2="1.5" y2="0.75" width="0.127" layer="51"/>
<smd name="1" x="-3.95" y="0" dx="1.5" dy="1.4" layer="1" roundness="50" rot="R180"/>
<smd name="2" x="3.95" y="0" dx="1.5" dy="1.4" layer="1" roundness="50" rot="R180"/>
<text x="0" y="0.9" size="0.762" layer="25" font="vector" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.2" size="0.762" layer="21" font="vector" ratio="15" align="top-center">&gt;LABEL</text>
<text x="0" y="-2.2" size="0.762" layer="51" font="vector" ratio="15" align="top-center">&gt;LABEL</text>
<wire x1="-2.75" y1="1.8" x2="-3" y2="1.55" width="0.127" layer="21" curve="90"/>
<wire x1="-2.75" y1="1.8" x2="2.75" y2="1.8" width="0.127" layer="21"/>
<wire x1="2.75" y1="1.8" x2="3" y2="1.55" width="0.127" layer="21" curve="-90"/>
<wire x1="3" y1="-1.55" x2="2.65" y2="-1.8" width="0.127" layer="21" curve="-90"/>
<wire x1="2.65" y1="-1.8" x2="-2.75" y2="-1.8" width="0.127" layer="21"/>
<wire x1="-3" y1="-1.55" x2="-2.75" y2="-1.8" width="0.127" layer="21" curve="90"/>
<wire x1="-3" y1="-1.55" x2="-3" y2="1.55" width="0.127" layer="21"/>
<wire x1="3" y1="-1.55" x2="3" y2="1.55" width="0.127" layer="21"/>
<wire x1="-1.5" y1="0.75" x2="1.5" y2="0.75" width="0.127" layer="21"/>
<wire x1="1.5" y1="-0.75" x2="-1.5" y2="-0.75" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-0.75" x2="-1.5" y2="0.75" width="0.127" layer="21"/>
<wire x1="1.5" y1="-0.75" x2="1.5" y2="0.75" width="0.127" layer="21"/>
<rectangle x1="-4" y1="-0.35" x2="-3" y2="0.35" layer="51"/>
<rectangle x1="3" y1="-0.35" x2="4" y2="0.35" layer="51"/>
</package>
<package name="SW-PB-6060-4P">
<description>Momentary Push Button - 4 pin&lt;br/&gt;
Size: 6.0 x 6.0mm</description>
<wire x1="-3" y1="-3" x2="3" y2="-3" width="0.2032" layer="51"/>
<wire x1="-3" y1="-3" x2="-3" y2="3" width="0.2032" layer="21"/>
<wire x1="-3" y1="-3" x2="-3" y2="3" width="0.2032" layer="51"/>
<wire x1="-3" y1="3" x2="3" y2="3" width="0.2032" layer="21"/>
<wire x1="-3" y1="3" x2="3" y2="3" width="0.2032" layer="51"/>
<wire x1="3" y1="3" x2="3" y2="-3" width="0.2032" layer="21"/>
<wire x1="3" y1="3" x2="3" y2="-3" width="0.2032" layer="51"/>
<wire x1="3" y1="-3" x2="-3" y2="-3" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="1.905" y2="0.445" width="0.127" layer="51"/>
<wire x1="1.905" y1="0.445" x2="2.16" y2="-0.01" width="0.127" layer="51"/>
<wire x1="1.905" y1="-0.23" x2="1.905" y2="-1.115" width="0.127" layer="51"/>
<circle x="0" y="0" radius="1.27" width="0.2032" layer="21"/>
<smd name="1" x="-4" y="2.25" dx="1.3" dy="1.8" layer="1" roundness="50" rot="R90"/>
<smd name="2" x="4" y="2.25" dx="1.3" dy="1.8" layer="1" roundness="50" rot="R90"/>
<smd name="3" x="-4" y="-2.25" dx="1.3" dy="1.8" layer="1" roundness="50" rot="R90"/>
<smd name="4" x="4" y="-2.25" dx="1.3" dy="1.8" layer="1" roundness="50" rot="R90"/>
<text x="0" y="1.8" size="0.762" layer="25" font="vector" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="0" y="-4.191" size="0.762" layer="21" font="vector" ratio="15" align="bottom-center">&gt;LABEL</text>
<text x="0" y="-4.191" size="0.762" layer="51" font="vector" ratio="15" align="bottom-center">&gt;LABEL</text>
</package>
<package name="SW-PB-6060-4P-TH">
<description>Momentary Push Button - 4 pin (Through Hole)&lt;br&gt;
Size: 6.0 x 6.0mm</description>
<wire x1="3.048" y1="-2.54" x2="3.048" y2="2.54" width="0.2032" layer="51"/>
<wire x1="3.048" y1="2.54" x2="2.54" y2="3.048" width="0.2032" layer="51"/>
<wire x1="2.54" y1="-3.048" x2="3.048" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="3.048" x2="-3.048" y2="2.54" width="0.2032" layer="51"/>
<wire x1="-3.048" y1="2.54" x2="-3.048" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-3.048" x2="-3.048" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="2.54" y1="-3.048" x2="-2.54" y2="-3.048" width="0.2032" layer="51"/>
<wire x1="2.54" y1="3.048" x2="-2.54" y2="3.048" width="0.2032" layer="51"/>
<wire x1="2.159" y1="3.048" x2="-2.159" y2="3.048" width="0.2032" layer="21"/>
<wire x1="-2.159" y1="-3.048" x2="2.159" y2="-3.048" width="0.2032" layer="21"/>
<wire x1="3.048" y1="0.998" x2="3.048" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="-3.048" y1="1.028" x2="-3.048" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="0.508" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-0.508" x2="-2.54" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="0.508" x2="-2.159" y2="-0.381" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.778" width="0.2032" layer="21"/>
<pad name="1" x="-3.2512" y="2.2606" drill="1.016" diameter="1.8796"/>
<pad name="2" x="3.2512" y="2.2606" drill="1.016" diameter="1.8796"/>
<pad name="3" x="-3.2512" y="-2.2606" drill="1.016" diameter="1.8796"/>
<pad name="4" x="3.2512" y="-2.2606" drill="1.016" diameter="1.8796"/>
<text x="0" y="2.032" size="0.762" layer="25" font="vector" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="0" y="-4.191" size="0.762" layer="21" font="vector" ratio="15" align="bottom-center">&gt;LABEL</text>
<text x="0" y="-4.191" size="0.762" layer="51" font="vector" ratio="15" align="bottom-center">&gt;LABEL</text>
</package>
<package name="S-SW-PBM-KAN0444">
<description>Momentary Push Button&lt;br&gt;
Size: 4.0 x 3.0 mm</description>
<wire x1="-1.7272" y1="-1.2192" x2="1.7272" y2="-1.2192" width="0.2032" layer="21"/>
<wire x1="1.7272" y1="1.2192" x2="-1.7272" y2="1.2192" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="0.889" width="0.2032" layer="21"/>
<smd name="P$1" x="-2.286" y="0" dx="1.2" dy="1.5" layer="1" roundness="50"/>
<smd name="P$2" x="2.286" y="0" dx="1.2" dy="1.5" layer="1" roundness="50"/>
<wire x1="2.032" y1="1.524" x2="-2.032" y2="1.524" width="0.2032" layer="51"/>
<wire x1="-2.032" y1="-1.524" x2="2.032" y2="-1.524" width="0.2032" layer="51"/>
<wire x1="2.032" y1="-1.524" x2="2.032" y2="1.524" width="0.2032" layer="51"/>
<wire x1="-2.032" y1="1.524" x2="-2.032" y2="-1.524" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="0.889" width="0.2032" layer="51"/>
<rectangle x1="-2.4638" y1="-0.635" x2="-1.9558" y2="0.635" layer="51"/>
<rectangle x1="1.9558" y1="-0.635" x2="2.4638" y2="0.635" layer="51"/>
<text x="-1.651" y="1.778" size="0.762" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-2.032" y="-2.54" size="0.762" layer="51" font="vector" ratio="15">&gt;LABEL</text>
<text x="-2.032" y="-2.54" size="0.762" layer="21" font="vector" ratio="15">&gt;LABEL</text>
<wire x1="-1.7272" y1="-1.016" x2="-1.7272" y2="-1.2192" width="0.2032" layer="21"/>
<wire x1="1.7272" y1="1.2192" x2="1.7272" y2="1.016" width="0.2032" layer="21"/>
<wire x1="-1.7272" y1="1.2192" x2="-1.7272" y2="1.016" width="0.2032" layer="21"/>
<wire x1="1.7272" y1="-1.016" x2="1.7272" y2="-1.2192" width="0.2032" layer="21"/>
</package>
<package name="S-SP-2N-KAN0441">
<description>SMD Push Button KAN0441.&lt;br&gt;
Size: 6.0 x 3.75mm&lt;br&gt;
&lt;br&gt;
Created by: OoiBP</description>
<wire x1="-3" y1="1.875" x2="-3" y2="-1.875" width="0.2032" layer="51"/>
<wire x1="-3" y1="-1.875" x2="3" y2="-1.875" width="0.2032" layer="51"/>
<wire x1="3" y1="-1.875" x2="3" y2="1.875" width="0.2032" layer="51"/>
<wire x1="3" y1="1.875" x2="-3" y2="1.875" width="0.2032" layer="51"/>
<rectangle x1="-4" y1="-0.4" x2="-3" y2="0.4" layer="51"/>
<rectangle x1="3" y1="-0.4" x2="4" y2="0.4" layer="51"/>
<wire x1="-2.5" y1="1" x2="-2.5" y2="1.5" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="1.5" x2="2.5" y2="1.5" width="0.2032" layer="21"/>
<wire x1="2.5" y1="1.5" x2="2.5" y2="1" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-1" x2="-2.5" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-1.5" x2="2.5" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="2.5" y1="-1.5" x2="2.5" y2="-1" width="0.2032" layer="21"/>
<smd name="1" x="-3.7" y="0" dx="1.7" dy="1.3" layer="1" roundness="50"/>
<smd name="2" x="3.7" y="0" dx="1.7" dy="1.3" layer="1" roundness="50"/>
<wire x1="-1.5" y1="0.75" x2="-1.5" y2="-0.75" width="0.2032" layer="51"/>
<wire x1="-1.5" y1="-0.75" x2="1.5" y2="-0.75" width="0.2032" layer="51"/>
<wire x1="1.5" y1="-0.75" x2="1.5" y2="0.75" width="0.2032" layer="51"/>
<wire x1="1.5" y1="0.75" x2="-1.5" y2="0.75" width="0.2032" layer="51"/>
<text x="-3" y="3" size="0.8128" layer="25" ratio="15" align="top-left">&gt;NAME</text>
<text x="-3" y="-3" size="0.8128" layer="27" ratio="15">&gt;MPN</text>
</package>
<package name="S-SP-4N-KAN0641">
<description>SMD Push Button KAN0641.&lt;br&gt;
Size: 6.0 x 6.0mm.&lt;br&gt;
&lt;br&gt;
Created by: OoiBP</description>
<wire x1="-3" y1="3" x2="-3" y2="-3" width="0.2032" layer="51"/>
<wire x1="-3" y1="-3" x2="3" y2="-3" width="0.2032" layer="51"/>
<wire x1="3" y1="-3" x2="3" y2="3" width="0.2032" layer="51"/>
<wire x1="3" y1="3" x2="-3" y2="3" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.75" width="0.2032" layer="51"/>
<rectangle x1="-5" y1="1.9" x2="-3" y2="2.6" layer="51"/>
<rectangle x1="-5" y1="-2.6" x2="-3" y2="-1.9" layer="51"/>
<rectangle x1="3" y1="1.9" x2="5" y2="2.6" layer="51"/>
<rectangle x1="3" y1="-2.6" x2="5" y2="-1.9" layer="51"/>
<smd name="4" x="-4.55" y="2.25" dx="2.1" dy="1.4" layer="1" roundness="50"/>
<smd name="3" x="4.55" y="2.25" dx="2.1" dy="1.4" layer="1" roundness="50"/>
<smd name="2" x="4.55" y="-2.25" dx="2.1" dy="1.4" layer="1" roundness="50"/>
<smd name="1" x="-4.55" y="-2.25" dx="2.1" dy="1.4" layer="1" roundness="50"/>
<wire x1="-2.5" y1="2" x2="-2.5" y2="2.5" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="2.5" x2="2.5" y2="2.5" width="0.2032" layer="21"/>
<wire x1="2.5" y1="2.5" x2="2.5" y2="2" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-2" x2="-2.5" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-2.5" x2="2.5" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="2.5" y1="-2.5" x2="2.5" y2="-2" width="0.2032" layer="21"/>
<text x="-3" y="4" size="0.8128" layer="25" ratio="15" align="top-left">&gt;NAME</text>
<text x="-3" y="-4" size="0.8128" layer="27" ratio="15">&gt;MPN</text>
</package>
<package name="SW-PB-120120-4P">
<description>Momentary Push Button - 4 pin&lt;br&gt;
Size: 12.0 x 12.0mm&lt;br&gt;
&lt;br&gt;
Created by: Wai Weng</description>
<wire x1="-6" y1="-6" x2="6" y2="-6" width="0.2032" layer="51"/>
<wire x1="-5.6" y1="-5.6" x2="-5.6" y2="5.6" width="0.2032" layer="21"/>
<wire x1="-6" y1="-6" x2="-6" y2="6" width="0.2032" layer="51"/>
<wire x1="-5.6" y1="5.6" x2="5.6" y2="5.6" width="0.2032" layer="21"/>
<wire x1="-6" y1="6" x2="6" y2="6" width="0.2032" layer="51"/>
<wire x1="5.6" y1="5.6" x2="5.6" y2="-5.6" width="0.2032" layer="21"/>
<wire x1="6" y1="6" x2="6" y2="-6" width="0.2032" layer="51"/>
<wire x1="5.6" y1="-5.6" x2="-5.6" y2="-5.6" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="3.4" width="0.2032" layer="21"/>
<text x="0" y="0.8" size="0.762" layer="25" font="vector" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="0" y="-7.191" size="0.762" layer="21" font="vector" ratio="15" align="bottom-center">&gt;LABEL</text>
<rectangle x1="-8" y1="2" x2="-6" y2="3" layer="51"/>
<rectangle x1="-8" y1="-3" x2="-6" y2="-2" layer="51"/>
<rectangle x1="6" y1="-3" x2="8" y2="-2" layer="51"/>
<rectangle x1="6" y1="2" x2="8" y2="3" layer="51"/>
<smd name="1" x="-7.6" y="2.5" dx="2.7" dy="1.2" layer="1" roundness="30"/>
<smd name="2" x="7.6" y="2.5" dx="2.7" dy="1.2" layer="1" roundness="30"/>
<smd name="3" x="-7.6" y="-2.5" dx="2.7" dy="1.2" layer="1" roundness="30"/>
<smd name="4" x="7.6" y="-2.5" dx="2.7" dy="1.2" layer="1" roundness="30"/>
</package>
<package name="SW-PB-6060-RA-TH">
<description>Momentary Push Button - Right Angle (Through Hole)&lt;br&gt;
&lt;br&gt;
Created by: Wai Weng</description>
<pad name="P$1" x="-2.25" y="0" drill="0.9" diameter="1.5"/>
<pad name="P$2" x="2.25" y="0" drill="0.9" diameter="1.5"/>
<pad name="P$3" x="-3.5" y="-2.3" drill="1.2" diameter="2"/>
<pad name="P$4" x="3.5" y="-2.3" drill="1.2" diameter="2"/>
<rectangle x1="-3.7" y1="-3.6" x2="-3.3" y2="1.7" layer="51"/>
<rectangle x1="3.3" y1="-3.6" x2="3.7" y2="1.7" layer="51"/>
<wire x1="-3.7" y1="2.5" x2="3.7" y2="2.5" width="0.2032" layer="51"/>
<wire x1="3.7" y1="2.5" x2="3.7" y2="-1.5" width="0.2032" layer="51"/>
<wire x1="3.7" y1="-1.5" x2="-3.7" y2="-1.5" width="0.2032" layer="51"/>
<wire x1="-3.7" y1="-1.5" x2="-3.7" y2="2.5" width="0.2032" layer="51"/>
<rectangle x1="-1.7" y1="2.5" x2="1.7" y2="12.5" layer="51"/>
<wire x1="-3.4" y1="2.2" x2="3.4" y2="2.2" width="0.2032" layer="21"/>
<wire x1="3.4" y1="2.2" x2="3.4" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="3.4" y1="-1.1" x2="-3.4" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="-1.1" x2="-3.4" y2="2.2" width="0.2032" layer="21"/>
</package>
<package name="SW-PB-KAN3246">
<description>Momentary Push Button&lt;br&gt;
Size: 4.2 x 3.3mm&lt;br&gt;
&lt;br&gt;&lt;br&gt;
Created by: Idris&lt;br&gt;</description>
<smd name="1" x="-2.2" y="1.075" dx="1.2" dy="0.85" layer="1" roundness="50"/>
<smd name="2" x="2.2" y="1.075" dx="1.2" dy="0.85" layer="1" roundness="50"/>
<smd name="3" x="-2.2" y="-1.075" dx="1.2" dy="0.85" layer="1" roundness="50"/>
<smd name="4" x="2.2" y="-1.075" dx="1.2" dy="0.85" layer="1" roundness="50"/>
<wire x1="-2.1" y1="1.65" x2="2.1" y2="1.65" width="0.2032" layer="51"/>
<wire x1="2.1" y1="1.65" x2="2.1" y2="-1.65" width="0.2032" layer="51"/>
<wire x1="2.1" y1="-1.65" x2="-2.1" y2="-1.65" width="0.2032" layer="51"/>
<wire x1="-2.1" y1="-1.65" x2="-2.1" y2="1.65" width="0.2032" layer="51"/>
<wire x1="-0.4" y1="1.075" x2="0.4" y2="1.075" width="0.2032" layer="51"/>
<wire x1="0.4" y1="1.075" x2="1.475" y2="0" width="0.2032" layer="51" curve="-90"/>
<wire x1="1.475" y1="0" x2="0.4" y2="-1.075" width="0.2032" layer="51" curve="-90"/>
<wire x1="0.4" y1="-1.075" x2="-0.4" y2="-1.075" width="0.2032" layer="51"/>
<wire x1="-0.4" y1="-1.075" x2="-1.475" y2="0" width="0.2032" layer="51" curve="-90"/>
<wire x1="-1.475" y1="0" x2="-0.4" y2="1.075" width="0.2032" layer="51" curve="-90"/>
<text x="0" y="1.905" size="0.762" layer="25" font="vector" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.905" size="0.762" layer="21" font="vector" ratio="15" align="top-center">&gt;LABEL</text>
<text x="0" y="-1.905" size="0.762" layer="51" font="vector" ratio="15" align="top-center">&gt;LABEL</text>
<wire x1="-1.27" y1="1.397" x2="0" y2="1.397" width="0.2032" layer="21"/>
<wire x1="0" y1="1.397" x2="1.27" y2="1.397" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.397" x2="0" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="0" y1="-1.397" x2="1.27" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="0" y1="1.397" x2="0" y2="0.635" width="0.2032" layer="21"/>
<wire x1="0" y1="-1.397" x2="0" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.508" y2="0.381" width="0.2032" layer="21"/>
</package>
<package name="LED-5MM">
<description>Through Hole LED&lt;br&gt;
Size: 5mm x 5mm&lt;br&gt;
Height: 9mm</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<pad name="A" x="-1.27" y="0" drill="0.75" diameter="1.524"/>
<pad name="K" x="1.27" y="0" drill="0.75" diameter="1.524"/>
<text x="-3.5814" y="-1.651" size="0.762" layer="25" font="vector" ratio="15" rot="R90">&gt;NAME</text>
<text x="3.7084" y="-2.0066" size="0.762" layer="21" font="vector" ratio="15" rot="R90">&gt;LABEL</text>
<text x="3.7084" y="-2.0066" size="0.762" layer="51" font="vector" ratio="15" rot="R90">&gt;LABEL</text>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="51"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="51" curve="-286.260205" cap="flat"/>
</package>
<package name="LED-3216-REVERSE">
<description>Reverse Mount SMD LED&lt;br&gt;
Size: 3.2 x 1.6mm&lt;br&gt;
&lt;br&gt;
Same with size 1206 in inches.&lt;br&gt;
&lt;br&gt;
Created by: Wai Weng</description>
<wire x1="-0.3" y1="-0.5" x2="-0.3" y2="0" width="0.2032" layer="51"/>
<wire x1="-0.3" y1="0" x2="-0.3" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-0.3" y1="0" x2="0.3" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="0.3" y1="-0.5" x2="0.3" y2="0.5" width="0.2032" layer="51"/>
<wire x1="0.3" y1="0.5" x2="-0.3" y2="0" width="0.2032" layer="51"/>
<smd name="A" x="1.65" y="0" dx="1" dy="1.5" layer="1" roundness="25" rot="R180"/>
<smd name="C" x="-1.65" y="0" dx="1" dy="1.5" layer="1" roundness="25" rot="R180"/>
<text x="0" y="2.54" size="0.762" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<text x="0" y="-1.397" size="0.762" layer="21" font="vector" ratio="15" align="top-center">&gt;LABEL</text>
<text x="0" y="1.651" size="0.762" layer="27" font="vector" ratio="15" align="center">&gt;VALUE</text>
<wire x1="-1.6" y1="0.8" x2="-1.6" y2="-0.8" width="0.2032" layer="51"/>
<wire x1="-1.6" y1="-0.8" x2="-0.7" y2="-0.8" width="0.2032" layer="51"/>
<wire x1="-0.7" y1="-0.8" x2="0.7" y2="-0.8" width="0.2032" layer="51"/>
<wire x1="0.7" y1="-0.8" x2="1.6" y2="-0.8" width="0.2032" layer="51"/>
<wire x1="1.6" y1="-0.8" x2="1.6" y2="0.8" width="0.2032" layer="51"/>
<wire x1="1.6" y1="0.8" x2="0.7" y2="0.8" width="0.2032" layer="51"/>
<wire x1="0.7" y1="0.8" x2="-0.7" y2="0.8" width="0.2032" layer="51"/>
<wire x1="-0.7" y1="0.8" x2="-1.6" y2="0.8" width="0.2032" layer="51"/>
<rectangle x1="-1.6" y1="-0.7" x2="-1" y2="0.7" layer="51"/>
<rectangle x1="1" y1="-0.7" x2="1.6" y2="0.7" layer="51"/>
<hole x="0" y="0" drill="2.25"/>
<wire x1="-0.7" y1="0.8" x2="-0.7" y2="-0.8" width="0.2032" layer="51"/>
<wire x1="0.7" y1="0.8" x2="0.7" y2="-0.8" width="0.2032" layer="51"/>
<circle x="-2.8" y="0" radius="0.14141875" width="0.3048" layer="21"/>
<text x="0" y="0" size="0.508" layer="46" font="vector" ratio="15" align="center">NPTH</text>
</package>
<package name="LED-5MM-HOLDER-RA">
<description>Through Hole LED 5mm with Right Angle Holder.</description>
<pad name="1" x="-1.27" y="0" drill="0.75" diameter="1.524"/>
<pad name="2" x="1.27" y="0" drill="0.75" diameter="1.524"/>
<text x="-1.143" y="1.524" size="1.27" layer="21" font="vector" ratio="18" align="center">+</text>
<text x="1.397" y="1.524" size="1.27" layer="21" font="vector" ratio="18" align="center">-</text>
<wire x1="-3.175" y1="3.81" x2="3.175" y2="3.81" width="0.2032" layer="21"/>
<wire x1="3.175" y1="3.81" x2="3.175" y2="-5.334" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-5.334" x2="-3.175" y2="-5.334" width="0.2032" layer="21"/>
<wire x1="-3.175" y1="-5.334" x2="-3.175" y2="3.81" width="0.2032" layer="21"/>
<wire x1="-2.667" y1="-5.334" x2="-2.667" y2="-7.623021875" width="0.2032" layer="51"/>
<wire x1="-2.667" y1="-7.623021875" x2="-0.003021875" y2="-10.287" width="0.2032" layer="51" curve="90"/>
<wire x1="-0.003021875" y1="-10.287" x2="2.667" y2="-7.616978125" width="0.2032" layer="51" curve="90"/>
<wire x1="2.667" y1="-7.616978125" x2="2.667" y2="-5.334" width="0.2032" layer="51"/>
<text x="0" y="-3.429" size="0.762" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<text x="0" y="4.699" size="0.762" layer="21" font="vector" ratio="15" align="center">&gt;LABEL</text>
<text x="0" y="-4.572" size="0.762" layer="27" font="vector" ratio="15" align="center">&gt;MPN</text>
</package>
<package name="LED-5MM-RA">
<description>Through Hole 5mm LED - Right Angle.</description>
<pad name="1" x="-1.27" y="0" drill="0.75" diameter="1.524"/>
<pad name="2" x="1.27" y="0" drill="0.75" diameter="1.524"/>
<text x="-1.143" y="-1.905" size="1.27" layer="21" font="vector" ratio="18" align="center">+</text>
<text x="1.397" y="-1.905" size="1.27" layer="21" font="vector" ratio="18" align="center">-</text>
<wire x1="2.9" y1="-1" x2="-2.9" y2="-1" width="0.2032" layer="21"/>
<wire x1="2.5" y1="-2" x2="2.5" y2="-7.5" width="0.2032" layer="21"/>
<wire x1="2.5" y1="-7.5" x2="0" y2="-10" width="0.2032" layer="21" curve="-90"/>
<wire x1="0" y1="-10" x2="-2.5" y2="-7.5" width="0.2032" layer="21" curve="-90"/>
<wire x1="-2.5" y1="-7.5" x2="-2.5" y2="-2" width="0.2032" layer="21"/>
<wire x1="2.9" y1="-1" x2="2.9" y2="-2" width="0.2032" layer="21"/>
<wire x1="2.9" y1="-2" x2="2.5" y2="-2" width="0.2032" layer="21"/>
<wire x1="-2.9" y1="-1" x2="-2.9" y2="-2" width="0.2032" layer="21"/>
<wire x1="-2.9" y1="-2" x2="-2.5" y2="-2" width="0.2032" layer="21"/>
<text x="0" y="-6.604" size="0.762" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<text x="0" y="1.524" size="0.762" layer="21" font="vector" ratio="15" align="center">&gt;LABEL</text>
<text x="0" y="-7.747" size="0.762" layer="27" font="vector" ratio="15" align="center">&gt;MPN</text>
</package>
<package name="SW-PB-KAN3543-RA">
<description>Momentary Push Button (Right Angled)&lt;br&gt;
Size: 7.0 x 2.5mm&lt;br&gt;
&lt;br&gt;&lt;br&gt;
Created by: Wai Weng&lt;br&gt;
&lt;br&gt;
Modified for the Through Hole Holding Pin.</description>
<wire x1="-3.5" y1="1.25" x2="-1.5" y2="1.25" width="0.2032" layer="51"/>
<wire x1="-1.5" y1="1.25" x2="1.5" y2="1.25" width="0.2032" layer="51"/>
<wire x1="1.5" y1="1.25" x2="3.5" y2="1.25" width="0.2032" layer="51"/>
<wire x1="3.5" y1="1.25" x2="3.5" y2="-1.25" width="0.2032" layer="51"/>
<wire x1="3.5" y1="-1.25" x2="-3.5" y2="-1.25" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="-1.25" x2="-3.5" y2="1.25" width="0.2032" layer="51"/>
<wire x1="-1.5" y1="1.25" x2="-1.5" y2="2.25" width="0.2032" layer="51"/>
<wire x1="-1.5" y1="2.25" x2="1.5" y2="2.25" width="0.2032" layer="51"/>
<wire x1="1.5" y1="2.25" x2="1.5" y2="1.25" width="0.2032" layer="51"/>
<hole x="-1.15" y="0.25" drill="0.9"/>
<hole x="1.15" y="0.25" drill="0.9"/>
<rectangle x1="-3.9" y1="0.25" x2="-3.05" y2="1.25" layer="51"/>
<rectangle x1="3.05" y1="0.25" x2="3.9" y2="1.25" layer="51"/>
<rectangle x1="1.8" y1="-1.7" x2="2.35" y2="-0.75" layer="51"/>
<rectangle x1="-2.35" y1="-1.7" x2="-1.8" y2="-0.75" layer="51"/>
<smd name="1" x="-2.075" y="-1.225" dx="0.95" dy="1.35" layer="1" roundness="30"/>
<smd name="2" x="2.075" y="-1.225" dx="0.95" dy="1.35" layer="1" roundness="30"/>
<smd name="3" x="3.55" y="0.7" dx="1.7" dy="1.7" layer="1" roundness="20"/>
<smd name="4" x="-3.55" y="0.7" dx="1.7" dy="1.7" layer="1" roundness="20"/>
<wire x1="-2.3" y1="1" x2="2.3" y2="1" width="0.2032" layer="21"/>
<wire x1="-1.2" y1="-1" x2="1.2" y2="-1" width="0.2032" layer="21"/>
<wire x1="3.2" y1="-0.5" x2="3.2" y2="-1" width="0.2032" layer="21"/>
<wire x1="3.2" y1="-1" x2="3" y2="-1" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-0.5" x2="-3.2" y2="-1" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-1" x2="-3" y2="-1" width="0.2032" layer="21"/>
<text x="0" y="2.5" size="0.762" layer="25" font="vector" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.1" size="0.762" layer="21" font="vector" ratio="15" align="top-center">&gt;LABEL</text>
<text x="0" y="-2.1" size="0.762" layer="51" font="vector" ratio="15" align="top-center">&gt;LABEL</text>
</package>
<package name="BUZZER-90-CYL">
<description>SMD Piezo Buzzer (Cylinder)&lt;br&gt;
Size: 9.0mm&lt;br&gt;
&lt;br&gt;
Created by: Wai Weng</description>
<circle x="0" y="0" radius="4.5" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1" width="0.2032" layer="51"/>
<text x="1.927" y="2.802" size="1.27" layer="21" font="vector" ratio="15" align="center">+</text>
<smd name="+" x="0" y="3.9" dx="2" dy="6" layer="1"/>
<rectangle x1="-0.8" y1="1.25" x2="0.8" y2="6.15" layer="51"/>
<smd name="-" x="0" y="-3.9" dx="2" dy="6" layer="1"/>
<rectangle x1="-0.8" y1="-6.15" x2="0.8" y2="-1.25" layer="51"/>
<wire x1="-1.4" y1="4" x2="-1.4" y2="-4" width="0.2032" layer="21" curve="141.419908"/>
<wire x1="1.4" y1="-4" x2="1.4" y2="4" width="0.2032" layer="21" curve="141.419908"/>
<wire x1="-1.4" y1="1.2" x2="-1.4" y2="-1.2" width="0.2032" layer="21" curve="81.202589"/>
<wire x1="1.4" y1="-1.2" x2="1.4" y2="1.2" width="0.2032" layer="21" curve="81.202589"/>
</package>
<package name="S-BUZZER-HXD-8530A-03627">
<description>SMD Piezo Buzzer (Square)&lt;br&gt;
Size: 8.5mm x 8.5mm&lt;br&gt;
&lt;br&gt;
Created by: Idris</description>
<wire x1="-4.25" y1="-4.25" x2="-4.25" y2="4.25" width="0.2032" layer="51"/>
<wire x1="-4.25" y1="4.25" x2="4.25" y2="4.25" width="0.2032" layer="51"/>
<wire x1="4.25" y1="4.25" x2="4.25" y2="-4.25" width="0.2032" layer="51"/>
<wire x1="4.25" y1="-4.25" x2="-4.25" y2="-4.25" width="0.2032" layer="51"/>
<smd name="1" x="-3.4" y="3.4" dx="2.4" dy="1.5" layer="1" roundness="50" rot="R225"/>
<smd name="+" x="-3.6" y="-3.6" dx="2" dy="2" layer="1" roundness="50" rot="R90"/>
<smd name="-" x="3.6" y="-3.6" dx="2" dy="2" layer="1" roundness="50" rot="R90"/>
<smd name="2" x="3.6" y="3.6" dx="2" dy="2" layer="1" roundness="50" rot="R90"/>
<wire x1="-2.667" y1="-1.651" x2="-3.429" y2="-1.651" width="0.2032" layer="21"/>
<wire x1="-3.048" y1="-1.27" x2="-3.048" y2="-2.032" width="0.2032" layer="21"/>
<wire x1="-3.996" y1="-2.218" x2="-3.996" y2="1.71" width="0.2032" layer="21"/>
<wire x1="-1.71" y1="3.996" x2="2.218" y2="3.996" width="0.2032" layer="21"/>
<wire x1="3.996" y1="2.218" x2="3.996" y2="-2.218" width="0.2032" layer="21"/>
<wire x1="2.218" y1="-3.996" x2="-2.218" y2="-3.996" width="0.2032" layer="21"/>
<text x="-2.54" y="1.27" size="0.762" layer="25" font="vector" ratio="15">&gt;NAME</text>
<wire x1="0" y1="3.302" x2="-0.127" y2="3.175" width="0.2032" layer="21"/>
<wire x1="-0.127" y1="3.175" x2="-0.508" y2="2.794" width="0.2032" layer="21"/>
<wire x1="-0.508" y1="2.794" x2="0.508" y2="2.794" width="0.2032" layer="21"/>
<wire x1="0.508" y1="2.794" x2="0.127" y2="3.175" width="0.2032" layer="21"/>
<wire x1="0.127" y1="3.175" x2="0" y2="3.302" width="0.2032" layer="21"/>
<wire x1="-0.254" y1="2.921" x2="0.254" y2="2.921" width="0.2032" layer="21"/>
<wire x1="-0.127" y1="3.048" x2="0.127" y2="3.048" width="0.2032" layer="21"/>
<wire x1="-0.127" y1="3.175" x2="0.127" y2="3.175" width="0.2032" layer="21"/>
</package>
<package name="S-BUZZER-MLT-9650">
<description>SMD  Buzzer (Square)&lt;br&gt;
Size: 9.6mm x 9.6mm&lt;br&gt;
&lt;br&gt;
Created by: Idris</description>
<wire x1="-4.8" y1="4.8" x2="4.8" y2="4.8" width="0.2032" layer="21"/>
<wire x1="4.8" y1="4.8" x2="4.8" y2="-4.8" width="0.2032" layer="21"/>
<wire x1="4.8" y1="-4.8" x2="-3" y2="-4.8" width="0.2032" layer="21"/>
<smd name="P$1" x="0" y="-4.3" dx="1.5" dy="2" layer="1" roundness="50"/>
<smd name="P$2" x="0" y="4.3" dx="1.5" dy="2" layer="1" roundness="50"/>
<wire x1="-4.8" y1="-3" x2="-4.8" y2="4.8" width="0.2032" layer="21"/>
<wire x1="-4.8" y1="-3" x2="-3" y2="-4.8" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="0.7" width="0.2032" layer="21"/>
<wire x1="-4.8" y1="-3" x2="-4.8" y2="4.8" width="0.2032" layer="51"/>
<wire x1="-4.8" y1="4.8" x2="4.8" y2="4.8" width="0.2032" layer="51"/>
<wire x1="4.8" y1="4.8" x2="4.8" y2="-4.8" width="0.2032" layer="51"/>
<wire x1="4.8" y1="-4.8" x2="-3" y2="-4.8" width="0.2032" layer="51"/>
<wire x1="-4.8" y1="-3" x2="-3" y2="-4.8" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="0.7" width="0.2032" layer="51"/>
<text x="-2.54" y="1.778" size="0.762" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="0.762" layer="27" font="vector" ratio="15">&gt;MPN</text>
</package>
<package name="SW-SLIDE-SPDT-SMD">
<smd name="1" x="-2.25" y="2.25" dx="1.5" dy="0.8" layer="1" roundness="50" rot="R90"/>
<smd name="2" x="0.75" y="2.25" dx="1.5" dy="0.8" layer="1" roundness="50" rot="R90"/>
<smd name="3" x="2.25" y="2.25" dx="1.5" dy="0.8" layer="1" roundness="50" rot="R90"/>
<smd name="P$4" x="-3.75" y="1.1" dx="1.2" dy="0.8" layer="1" roundness="50"/>
<smd name="P$5" x="-3.75" y="-1.1" dx="1.2" dy="0.8" layer="1" roundness="50"/>
<smd name="P$6" x="3.75" y="-1.1" dx="1.2" dy="0.8" layer="1" roundness="50"/>
<smd name="P$7" x="3.75" y="1.1" dx="1.2" dy="0.8" layer="1" roundness="50"/>
<hole x="-1.5" y="0" drill="0.9"/>
<hole x="1.5" y="0" drill="0.9"/>
<wire x1="-3.4" y1="1.35" x2="3.4" y2="1.35" width="0.2032" layer="51"/>
<wire x1="3.4" y1="1.35" x2="3.4" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="3.4" y1="-1.35" x2="-3.4" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-1.35" x2="-3.4" y2="1.35" width="0.2032" layer="51"/>
<wire x1="-1.425" y1="-1.425" x2="-1.425" y2="-2.825" width="0.2032" layer="51"/>
<wire x1="-1.425" y1="-2.825" x2="1.375" y2="-2.825" width="0.2032" layer="51"/>
<wire x1="1.375" y1="-2.825" x2="1.375" y2="-1.425" width="0.2032" layer="51"/>
<wire x1="-2.9" y1="1" x2="2.9" y2="1" width="0.2032" layer="21"/>
<wire x1="2.9" y1="1" x2="2.9" y2="-1" width="0.2032" layer="21"/>
<wire x1="2.9" y1="-1" x2="-2.9" y2="-1" width="0.2032" layer="21"/>
<wire x1="-2.9" y1="-1" x2="-2.9" y2="1" width="0.2032" layer="21"/>
<rectangle x1="-2.5" y1="1.4" x2="-2" y2="2.2" layer="51"/>
<rectangle x1="0.5" y1="1.4" x2="1" y2="2.2" layer="51"/>
<rectangle x1="2" y1="1.4" x2="2.5" y2="2.2" layer="51"/>
<rectangle x1="3.55" y1="0.75" x2="4.05" y2="1.55" layer="51" rot="R90"/>
<rectangle x1="3.55" y1="-1.55" x2="4.05" y2="-0.75" layer="51" rot="R90"/>
<rectangle x1="-4.05" y1="0.75" x2="-3.55" y2="1.55" layer="51" rot="R90"/>
<rectangle x1="-4.05" y1="-1.55" x2="-3.55" y2="-0.75" layer="51" rot="R90"/>
<text x="-8" y="2" size="0.762" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-8" y="-3" size="0.762" layer="27" font="vector" ratio="15">&gt;LABEL</text>
</package>
<package name="SW-SLIDE-SPDT-9040-TH">
<description>Slide Switch - SPDT (Through Hole)&lt;br&gt;
Size: 90x40mm
&lt;br&gt;&lt;br&gt;
Created by: Wai Weng</description>
<pad name="P$1" x="-2.54" y="0" drill="0.8" diameter="1.524"/>
<pad name="P$2" x="0" y="0" drill="0.8" diameter="1.524"/>
<pad name="P$3" x="2.54" y="0" drill="0.8" diameter="1.524"/>
<wire x1="-4.5" y1="2" x2="4.5" y2="2" width="0.2032" layer="51"/>
<wire x1="4.5" y1="2" x2="4.5" y2="-2" width="0.2032" layer="51"/>
<wire x1="4.5" y1="-2" x2="-4.5" y2="-2" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="-2" x2="-4.5" y2="2" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="2" x2="4.5" y2="2" width="0.2032" layer="21"/>
<wire x1="4.5" y1="2" x2="4.5" y2="-2" width="0.2032" layer="21"/>
<wire x1="4.5" y1="-2" x2="-4.5" y2="-2" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="-2" x2="-4.5" y2="2" width="0.2032" layer="21"/>
<text x="-4.572" y="3.81" size="0.762" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-4.572" y="2.54" size="0.762" layer="21" font="vector" ratio="15">&gt;LABEL</text>
</package>
<package name="D-SOD-123">
<description>SMD Diode&lt;br&gt;
Size: 3.7 x 1.6mm</description>
<smd name="C" x="-1.9" y="0" dx="0.9" dy="1.1" layer="1" roundness="75"/>
<smd name="A" x="1.9" y="0" dx="0.9" dy="1.1" layer="1" roundness="75"/>
<wire x1="-1" y1="0.5" x2="-0.5" y2="0.5" width="0.2032" layer="21"/>
<wire x1="-0.5" y1="0.5" x2="1" y2="0.5" width="0.2032" layer="21"/>
<wire x1="-1" y1="-0.5" x2="-0.5" y2="-0.5" width="0.2032" layer="21"/>
<wire x1="-0.5" y1="-0.5" x2="1" y2="-0.5" width="0.2032" layer="21"/>
<wire x1="-0.5" y1="0.5" x2="-0.5" y2="-0.5" width="0.2032" layer="21"/>
<text x="0" y="1.2" size="0.762" layer="25" font="vector" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.2" size="0.762" layer="27" font="vector" ratio="15" align="top-center">&gt;MPN</text>
<rectangle x1="-1.9" y1="-0.3" x2="-1.35" y2="0.3" layer="51"/>
<rectangle x1="1.35" y1="-0.3" x2="1.9" y2="0.3" layer="51"/>
<wire x1="-1.35" y1="0.8" x2="-1.35" y2="-0.8" width="0.2032" layer="51"/>
<wire x1="-1.35" y1="-0.8" x2="-0.85" y2="-0.8" width="0.2032" layer="51"/>
<wire x1="-0.85" y1="-0.8" x2="1.35" y2="-0.8" width="0.2032" layer="51"/>
<wire x1="1.35" y1="-0.8" x2="1.35" y2="0.8" width="0.2032" layer="51"/>
<wire x1="1.35" y1="0.8" x2="-0.85" y2="0.8" width="0.2032" layer="51"/>
<wire x1="-0.85" y1="0.8" x2="-1.35" y2="0.8" width="0.2032" layer="51"/>
<wire x1="-0.85" y1="0.8" x2="-0.85" y2="-0.8" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.5" width="0.2032" layer="21"/>
<wire x1="1" y1="0.5" x2="1" y2="-0.5" width="0.2032" layer="21"/>
<polygon width="0.2032" layer="200">
<vertex x="-1.4" y="0.8"/>
<vertex x="1.4" y="0.8"/>
<vertex x="1.4" y="-0.8"/>
<vertex x="-1.4" y="-0.8"/>
</polygon>
</package>
<package name="D-SOD-323">
<wire x1="0.85" y1="0.625" x2="0.85" y2="-0.625" width="0.127" layer="51"/>
<wire x1="0.85" y1="-0.625" x2="-0.85" y2="-0.625" width="0.127" layer="21"/>
<wire x1="-0.85" y1="-0.625" x2="-0.85" y2="0.625" width="0.127" layer="51"/>
<wire x1="-0.85" y1="0.625" x2="0.85" y2="0.625" width="0.127" layer="21"/>
<smd name="A" x="-1.2" y="0" dx="1" dy="0.8" layer="1" roundness="25"/>
<smd name="C" x="1.2" y="0" dx="1" dy="0.8" layer="1" roundness="25"/>
<wire x1="-0.85" y1="0.6" x2="-0.85" y2="0.5" width="0.127" layer="21"/>
<wire x1="0.85" y1="0.6" x2="0.85" y2="0.5" width="0.127" layer="21"/>
<wire x1="-0.85" y1="-0.6" x2="-0.85" y2="-0.5" width="0.127" layer="21"/>
<wire x1="0.85" y1="-0.6" x2="0.85" y2="-0.5" width="0.127" layer="21"/>
<text x="0" y="1.2" size="0.762" layer="25" font="vector" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.2" size="0.762" layer="27" font="vector" ratio="15" align="top-center">&gt;MPN</text>
<wire x1="0.391" y1="-0.596" x2="0.391" y2="0.596" width="0.127" layer="21"/>
</package>
<package name="LED-5050-WS2812B">
<description>SMD RGB LED (Addressable)&lt;br&gt;
Size: 5.0x 5.0mm &lt;br&gt;&lt;br&gt;
&lt;b&gt;&lt;i&gt;* Need to check the orientation.&lt;/i&gt;&lt;/b&gt;</description>
<wire x1="1.4" y1="1.8" x2="-1.4" y2="1.8" width="0.2032" layer="21"/>
<wire x1="-1.4" y1="-1.8" x2="0.8" y2="-1.8" width="0.2032" layer="21"/>
<smd name="1" x="-2.35" y="1.6" dx="1.3" dy="1" layer="1"/>
<smd name="2" x="-2.35" y="-1.6" dx="1.3" dy="1" layer="1"/>
<smd name="4" x="2.35" y="1.6" dx="1.3" dy="1" layer="1"/>
<smd name="3" x="2.35" y="-1.6" dx="1.3" dy="1" layer="1"/>
<text x="-3.429" y="0.635" size="0.762" layer="21" font="vector" ratio="15">.</text>
<text x="-2.921" y="0.635" size="0.762" layer="51" font="vector" ratio="15">.</text>
<wire x1="-2.5" y1="0.85" x2="-2.5" y2="-0.85" width="0.2032" layer="51"/>
<wire x1="-2.5" y1="-2.5" x2="2" y2="-2.5" width="0.2032" layer="51"/>
<wire x1="2.5" y1="0.85" x2="2.5" y2="-0.85" width="0.2032" layer="51"/>
<wire x1="-2.5" y1="2.5" x2="2.5" y2="2.5" width="0.2032" layer="51"/>
<wire x1="2" y1="-2.5" x2="2.39" y2="-2.3" width="0.2032" layer="51"/>
<text x="-1.778" y="2.794" size="0.762" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-1.905" y="-3.556" size="0.762" layer="27" font="vector" ratio="15">&gt;VALUE</text>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-0.635" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-0.635" y2="-0.508" width="0.2032" layer="21"/>
<wire x1="-1.8" y1="0.8" x2="-1.8" y2="-0.8" width="0.2032" layer="21"/>
<wire x1="1.8" y1="0.8" x2="1.8" y2="-0.6" width="0.2032" layer="21"/>
<wire x1="1.8" y1="-0.6" x2="0.8" y2="-1.7" width="0.2032" layer="21"/>
</package>
<package name="SMD-RS-VR">
<description>Panasonic EMV3E SMD Potentiometer.</description>
<smd name="2" x="0" y="1.65" dx="1.6" dy="1.5" layer="1" roundness="30"/>
<smd name="3" x="1" y="-1.6" dx="1.2" dy="1.2" layer="1" roundness="30" rot="R90"/>
<smd name="1" x="-1" y="-1.6" dx="1.2" dy="1.2" layer="1" roundness="30" rot="R90"/>
<wire x1="-1.55" y1="1.55" x2="1.55" y2="1.55" width="0.2032" layer="51"/>
<wire x1="1.55" y1="1.55" x2="1.55" y2="-1.7" width="0.2032" layer="51"/>
<wire x1="1.55" y1="-1.7" x2="-1.55" y2="-1.7" width="0.2032" layer="51"/>
<wire x1="-1.55" y1="-1.7" x2="-1.55" y2="1.55" width="0.2032" layer="51"/>
<rectangle x1="-0.6" y1="1.55" x2="0.6" y2="1.85" layer="51"/>
<rectangle x1="-1.4" y1="-1.95" x2="-0.45" y2="-1.3" layer="51"/>
<rectangle x1="0.45" y1="-1.95" x2="1.4" y2="-1.3" layer="51"/>
<wire x1="-1.1" y1="1.3" x2="-1.3" y2="1.3" width="0.2032" layer="21"/>
<wire x1="-1.3" y1="1.3" x2="-1.3" y2="-0.7" width="0.2032" layer="21"/>
<wire x1="1.1" y1="1.3" x2="1.3" y2="1.3" width="0.2032" layer="21"/>
<wire x1="1.3" y1="1.3" x2="1.3" y2="-0.7" width="0.2032" layer="21"/>
<wire x1="-0.1" y1="-1.5" x2="0.1" y2="-1.5" width="0.2032" layer="21"/>
<text x="0" y="3.048" size="0.762" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<text x="0" y="-2.794" size="0.762" layer="27" font="vector" ratio="15" align="center">&gt;VALUE</text>
</package>
<package name="VR-3386-TH">
<description>Finger Adjustable Potentiometer (Through Hole)&lt;br&gt;
&lt;br&gt;
Modified by: Wai Weng</description>
<circle x="0" y="0" radius="4.25" width="0.127" layer="51"/>
<wire x1="-4.78" y1="-5.16" x2="-4" y2="-5.16" width="0.2032" layer="21"/>
<wire x1="4" y1="-5.16" x2="4.78" y2="-5.16" width="0.2032" layer="21"/>
<wire x1="4.78" y1="-5.16" x2="4.78" y2="4.4" width="0.2032" layer="21"/>
<wire x1="4.78" y1="4.4" x2="-4.78" y2="4.4" width="0.2032" layer="21"/>
<wire x1="-4.78" y1="4.4" x2="-4.78" y2="-5.16" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="2.2" x2="-0.5" y2="2.2" width="0.127" layer="51"/>
<wire x1="-0.5" y1="2.2" x2="-0.5" y2="-1.8" width="0.127" layer="51"/>
<wire x1="-0.5" y1="-1.8" x2="0.5" y2="-1.8" width="0.127" layer="51"/>
<wire x1="0.5" y1="-1.8" x2="0.5" y2="2.2" width="0.127" layer="51"/>
<wire x1="0.5" y1="2.2" x2="1.5" y2="2.2" width="0.127" layer="51"/>
<wire x1="0" y1="3.7" x2="-1.5" y2="2.2" width="0.127" layer="51"/>
<wire x1="1.5" y1="2.2" x2="0" y2="3.7" width="0.127" layer="51"/>
<wire x1="-4" y1="-5.16" x2="-4" y2="-4.8" width="0.2032" layer="21"/>
<wire x1="-4" y1="-4.8" x2="4" y2="-4.8" width="0.2032" layer="21"/>
<wire x1="4" y1="-4.8" x2="4" y2="-5.16" width="0.2032" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="0.7" diameter="1.524"/>
<pad name="2" x="0" y="-2.54" drill="0.7" diameter="1.524"/>
<pad name="3" x="2.54" y="0" drill="0.7" diameter="1.524"/>
<text x="0" y="5.334" size="0.762" layer="25" font="vector" ratio="16" align="center">&gt;Name</text>
<text x="0" y="-5.842" size="0.762" layer="27" font="vector" ratio="16" align="center">&gt;Value</text>
</package>
<package name="VR-3314J">
<smd name="3" x="-1.15" y="-2" dx="1.3" dy="2" layer="1" roundness="30"/>
<smd name="1" x="1.15" y="-2" dx="1.3" dy="2" layer="1" roundness="30"/>
<smd name="2" x="0" y="2" dx="2" dy="2" layer="1" roundness="30"/>
<wire x1="-2.25" y1="2.25" x2="2.25" y2="2.25" width="0.2032" layer="51"/>
<wire x1="2.25" y1="2.25" x2="2.25" y2="-2.25" width="0.2032" layer="51"/>
<wire x1="2.25" y1="-2.25" x2="-2.25" y2="-2.25" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-2.25" x2="-2.25" y2="2.25" width="0.2032" layer="51"/>
<wire x1="1.3" y1="2.25" x2="2.25" y2="2.25" width="0.2032" layer="21"/>
<wire x1="2.25" y1="2.25" x2="2.25" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-1.3" y1="2.25" x2="-2.25" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-2.25" y1="2.25" x2="-2.25" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-0.2" y1="-2.25" x2="0.2" y2="-2.25" width="0.2032" layer="21"/>
<text x="0" y="3.683" size="0.762" layer="25" font="vector" ratio="16" align="center">&gt;Name</text>
<text x="0" y="-3.683" size="0.762" layer="27" font="vector" ratio="16" align="center">&gt;Value</text>
</package>
<package name="MIC-6MM">
<circle x="0" y="0" radius="3" width="0.2032" layer="51"/>
<pad name="1" x="-1" y="-1" drill="0.8" shape="square" rot="R180"/>
<pad name="2" x="1" y="-1" drill="0.8" rot="R180"/>
<text x="0.802" y="-0.198" size="0.762" layer="51" font="vector" ratio="15">-</text>
<text x="-1.309" y="-0.198" size="0.762" layer="51" font="vector" ratio="15">+</text>
<circle x="0" y="0" radius="2.794" width="0.4064" layer="21"/>
<text x="0.802" y="-0.198" size="0.762" layer="21" font="vector" ratio="15">-</text>
<text x="-1.309" y="-0.198" size="0.762" layer="21" font="vector" ratio="15">+</text>
<circle x="0" y="1.4224" radius="0.635" width="0.2032" layer="21"/>
<wire x1="0.635" y1="2.0828" x2="-0.635" y2="2.0828" width="0.2032" layer="21"/>
<wire x1="0.381" y1="0.9144" x2="0.381" y2="0.2286" width="0.2032" layer="21"/>
<wire x1="-0.381" y1="0.9144" x2="-0.381" y2="0.2286" width="0.2032" layer="21"/>
<text x="-1.397" y="3.302" size="0.762" layer="25" font="vector" ratio="15">&gt;NAME</text>
</package>
<package name="ITR20001">
<wire x1="3.2" y1="-2.5" x2="3.2" y2="2.5" width="0.2032" layer="51"/>
<wire x1="3.2" y1="2.5" x2="-3.2" y2="2.5" width="0.2032" layer="51"/>
<wire x1="-3.2" y1="2.5" x2="-3.2" y2="-1.7" width="0.2032" layer="51"/>
<circle x="1.4" y="0" radius="1.1" width="0.2032" layer="51"/>
<circle x="-1.4" y="0" radius="1.1" width="0.2032" layer="51"/>
<wire x1="-2.4" y1="-2.5" x2="3.2" y2="-2.5" width="0.2032" layer="51"/>
<wire x1="-2.4" y1="-2.5" x2="-3.2" y2="-1.7" width="0.2032" layer="51"/>
<wire x1="3" y1="-2.3" x2="3" y2="2.3" width="0.2032" layer="21"/>
<wire x1="3" y1="2.3" x2="-3" y2="2.3" width="0.2032" layer="21"/>
<wire x1="-3" y1="2.3" x2="-3" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-3" y1="-1.6" x2="-2.3" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-2.3" x2="3" y2="-2.3" width="0.2032" layer="21"/>
<pad name="CATHODE" x="1.4" y="-1.27" drill="0.762" diameter="1.6764" rot="R270"/>
<pad name="ANODE" x="1.4" y="1.27" drill="0.762" diameter="1.6764" rot="R270"/>
<pad name="COLLECTOR" x="-1.4" y="1.27" drill="0.762" diameter="1.6764" rot="R270"/>
<pad name="EMITTER" x="-1.4" y="-1.27" drill="0.762" diameter="1.6764" rot="R270"/>
<wire x1="1.9" y1="-0.4" x2="1.4" y2="-0.4" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-0.4" x2="0.9" y2="-0.4" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-0.3" x2="1.9" y2="0.4" width="0.2032" layer="51"/>
<wire x1="1.9" y1="0.4" x2="1.4" y2="0.4" width="0.2032" layer="51"/>
<wire x1="1.4" y1="0.4" x2="0.9" y2="0.4" width="0.2032" layer="51"/>
<wire x1="0.9" y1="0.4" x2="1.4" y2="-0.3" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="0.5" x2="-1.1" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="-1.8" y1="-0.4" x2="-1.2" y2="0" width="0.2032" layer="51"/>
<wire x1="-1.2" y1="0" x2="-1.8" y2="0.3" width="0.2032" layer="51"/>
<wire x1="1.4" y1="0.4" x2="1.4" y2="1.1" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-0.4" x2="1.4" y2="-1.1" width="0.2032" layer="51"/>
<wire x1="-1.8" y1="0.3" x2="-1.8" y2="1" width="0.2032" layer="51"/>
<wire x1="-1.8" y1="-0.4" x2="-1.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="-1.5" y1="-0.4" x2="-1.8" y2="-0.4" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="-0.1" x2="-1.8" y2="-0.4" width="0.2032" layer="51"/>
<text x="-2.2" y="-1.1" size="0.4064" layer="51" font="vector" ratio="15" rot="R180">E</text>
<text x="-2.2" y="1.5" size="0.4064" layer="51" font="vector" ratio="15" rot="R180">C</text>
<text x="2.5" y="1.5" size="0.4064" layer="51" font="vector" ratio="15" rot="R180">A</text>
<text x="2.5" y="-1.1" size="0.4064" layer="51" font="vector" ratio="15" rot="R180">C</text>
<text x="-3.048" y="2.921" size="0.762" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-3.048" y="-3.683" size="0.762" layer="51" font="vector" ratio="15">&gt;MPN</text>
</package>
<package name="ITR1502">
<description>Package for ITR1502 IR sensor.&lt;br&gt;
&lt;br&gt;
Created by: Wai Weng</description>
<wire x1="-2" y1="1.5" x2="2" y2="1.5" width="0.2032" layer="51"/>
<wire x1="2" y1="1.5" x2="2" y2="-1.5" width="0.2032" layer="51"/>
<wire x1="2" y1="-1.5" x2="-2" y2="-1.5" width="0.2032" layer="51"/>
<wire x1="-2" y1="-1.5" x2="-2" y2="1.5" width="0.2032" layer="51"/>
<wire x1="-1.35" y1="1.15" x2="-0.25" y2="1.15" width="0.2032" layer="51"/>
<wire x1="-0.25" y1="1.15" x2="-0.25" y2="-1.15" width="0.2032" layer="51"/>
<wire x1="-0.25" y1="-1.15" x2="-1.35" y2="-1.15" width="0.2032" layer="51"/>
<wire x1="-1.35" y1="-1.15" x2="-1.35" y2="1.15" width="0.2032" layer="51"/>
<wire x1="0.25" y1="1.15" x2="1.35" y2="1.15" width="0.2032" layer="51"/>
<wire x1="1.35" y1="1.15" x2="1.35" y2="-1.15" width="0.2032" layer="51"/>
<wire x1="1.35" y1="-1.15" x2="0.25" y2="-1.15" width="0.2032" layer="51"/>
<wire x1="0.25" y1="-1.15" x2="0.25" y2="1.15" width="0.2032" layer="51"/>
<smd name="1" x="-1.65" y="1.2" dx="1.2" dy="1.1" layer="1" roundness="20"/>
<smd name="2" x="-1.65" y="-1.2" dx="1.2" dy="1.1" layer="1" roundness="20"/>
<smd name="3" x="1.65" y="-1.2" dx="1.2" dy="1.1" layer="1" roundness="20"/>
<smd name="4" x="1.65" y="1.2" dx="1.2" dy="1.1" layer="1" roundness="20"/>
<wire x1="-0.8" y1="1.3" x2="0.8" y2="1.3" width="0.2032" layer="21"/>
<wire x1="1.8" y1="0.4" x2="1.8" y2="-0.4" width="0.2032" layer="21"/>
<wire x1="0.8" y1="-1.3" x2="-0.8" y2="-1.3" width="0.2032" layer="21"/>
<wire x1="-1.8" y1="0.4" x2="-1.8" y2="-0.4" width="0.2032" layer="21"/>
<text x="0" y="2.286" size="0.762" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<text x="0" y="-2.286" size="0.762" layer="27" font="vector" ratio="15" align="center">&gt;MPN</text>
<wire x1="-0.381" y1="0.508" x2="-0.381" y2="-0.508" width="0.2032" layer="21"/>
<wire x1="-0.381" y1="-0.508" x2="-0.127" y2="-0.381" width="0.2032" layer="21"/>
<wire x1="-0.127" y1="-0.381" x2="0.127" y2="-0.254" width="0.2032" layer="21"/>
<wire x1="0.127" y1="-0.254" x2="0.635" y2="0" width="0.2032" layer="21"/>
<wire x1="0.635" y1="0" x2="0.127" y2="0.254" width="0.2032" layer="21"/>
<wire x1="0.127" y1="0.254" x2="-0.127" y2="0.381" width="0.2032" layer="21"/>
<wire x1="-0.127" y1="0.381" x2="-0.381" y2="0.508" width="0.2032" layer="21"/>
<wire x1="-0.254" y1="0.381" x2="-0.254" y2="-0.381" width="0.2032" layer="21"/>
<wire x1="-0.127" y1="0.381" x2="-0.127" y2="-0.381" width="0.2032" layer="21"/>
<wire x1="0" y1="0.254" x2="0" y2="-0.254" width="0.2032" layer="21"/>
<wire x1="0.127" y1="0.254" x2="0.127" y2="0" width="0.2032" layer="21"/>
<wire x1="0.127" y1="0" x2="0.127" y2="-0.254" width="0.2032" layer="21"/>
<wire x1="0.127" y1="0" x2="0.381" y2="0" width="0.2032" layer="21"/>
</package>
<package name="VR-RV0932-TH">
<description>0932 Potentiometer (Through Hole)&lt;br&gt;
&lt;br&gt;
Created by: Wai Weng</description>
<pad name="1" x="-2.5" y="-7" drill="1" diameter="1.778"/>
<pad name="2" x="0" y="-7" drill="1" diameter="1.778"/>
<pad name="3" x="2.5" y="-7" drill="1" diameter="1.778"/>
<pad name="4" x="-4.9" y="0" drill="1" diameter="1.9304" shape="long" rot="R90"/>
<wire x1="-4.4" y1="1" x2="-4.4" y2="-1" width="0" layer="46"/>
<wire x1="-4.4" y1="-1" x2="-4.9" y2="-1.5" width="0" layer="46" curve="-90"/>
<wire x1="-4.9" y1="-1.5" x2="-5.4" y2="-1" width="0" layer="46" curve="-90"/>
<wire x1="-5.4" y1="-1" x2="-5.4" y2="1" width="0" layer="46"/>
<wire x1="-5.4" y1="1" x2="-4.9" y2="1.5" width="0" layer="46" curve="-90"/>
<wire x1="-4.9" y1="1.5" x2="-4.4" y2="1" width="0" layer="46" curve="-90"/>
<pad name="5" x="4.9" y="0" drill="1" diameter="1.9304" shape="long" rot="R90"/>
<wire x1="5.4" y1="1" x2="5.4" y2="-1" width="0" layer="46"/>
<wire x1="5.4" y1="-1" x2="4.9" y2="-1.5" width="0" layer="46" curve="-90"/>
<wire x1="4.9" y1="-1.5" x2="4.4" y2="-1" width="0" layer="46" curve="-90"/>
<wire x1="4.4" y1="-1" x2="4.4" y2="1" width="0" layer="46"/>
<wire x1="4.4" y1="1" x2="4.9" y2="1.5" width="0" layer="46" curve="-90"/>
<wire x1="4.9" y1="1.5" x2="5.4" y2="1" width="0" layer="46" curve="-90"/>
<wire x1="-4.9" y1="5.5" x2="4.9" y2="5.5" width="0.2032" layer="21"/>
<wire x1="4.9" y1="5.5" x2="4.9" y2="2.3" width="0.2032" layer="21"/>
<wire x1="4.9" y1="-2.3" x2="4.9" y2="-5.5" width="0.2032" layer="21"/>
<wire x1="4.9" y1="-5.5" x2="-4.9" y2="-5.5" width="0.2032" layer="21"/>
<wire x1="-4.9" y1="-5.5" x2="-4.9" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-4.9" y1="2.3" x2="-4.9" y2="5.5" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="3" width="0.2032" layer="21"/>
<text x="0" y="0.508" size="0.762" layer="25" font="vector" ratio="16" align="center">&gt;Name</text>
<text x="0" y="-0.508" size="0.762" layer="27" font="vector" ratio="16" align="center">&gt;Value</text>
</package>
<package name="LED-10MM">
<description>10mm Through Hole LED&lt;br&gt;
&lt;br&gt;
Created by: Wai Weng</description>
<pad name="A" x="-1.27" y="0" drill="0.75" diameter="1.524"/>
<pad name="K" x="1.27" y="0" drill="0.75" diameter="1.524"/>
<text x="0" y="-1.905" size="0.762" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<text x="5.842" y="0" size="0.762" layer="21" font="vector" ratio="15" rot="R90" align="top-center">&gt;LABEL</text>
<wire x1="5" y1="2.3" x2="5" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="5" y1="-2.3" x2="5" y2="2.3" width="0.2032" layer="21" curve="-310.59514"/>
<text x="0" y="-3.005" size="0.762" layer="27" font="vector" ratio="15" align="center">&gt;VALUE</text>
</package>
<package name="SOT-23">
<description>Small Outline Transistor (SOT-23)&lt;br&gt;
&lt;br&gt;
Created by: Wai Weng</description>
<smd name="1" x="1.2" y="-0.95" dx="0.7" dy="0.8" layer="1" roundness="50" rot="R90"/>
<smd name="2" x="1.2" y="0.95" dx="0.7" dy="0.8" layer="1" roundness="50" rot="R90"/>
<smd name="3" x="-1.2" y="0" dx="0.7" dy="0.8" layer="1" roundness="50" rot="R90"/>
<wire x1="-0.35" y1="-1.2" x2="-0.35" y2="1.2" width="0.2032" layer="21"/>
<wire x1="-0.35" y1="1.2" x2="0.35" y2="1.2" width="0.2032" layer="21"/>
<wire x1="-0.35" y1="-1.2" x2="0.35" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="0.35" y1="-1.2" x2="0.35" y2="1.2" width="0.2032" layer="21"/>
<text x="0" y="2.159" size="0.762" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<text x="0" y="-2.159" size="0.762" layer="27" font="vector" ratio="15" align="center">&gt;MPN</text>
<wire x1="-0.65" y1="-1.45" x2="0.65" y2="-1.45" width="0.2032" layer="51"/>
<wire x1="0.65" y1="-1.45" x2="0.65" y2="1.45" width="0.2032" layer="51"/>
<wire x1="0.65" y1="1.45" x2="-0.65" y2="1.45" width="0.2032" layer="51"/>
<wire x1="-0.65" y1="1.45" x2="-0.65" y2="-1.45" width="0.2032" layer="51"/>
<rectangle x1="-1.125" y1="-0.275" x2="-0.725" y2="0.275" layer="51" rot="R90"/>
<rectangle x1="0.725" y1="0.675" x2="1.125" y2="1.225" layer="51" rot="R90"/>
<rectangle x1="0.725" y1="-1.225" x2="1.125" y2="-0.675" layer="51" rot="R90"/>
</package>
<package name="MIC-9.7MM">
<description>Microphone - 9.7mm&lt;br&gt;
&lt;br&gt;
Created by: Wai Weng</description>
<circle x="0" y="0" radius="4.85" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="4.445" width="0.2032" layer="21"/>
<pad name="+" x="-1.3" y="-2" drill="0.8" diameter="1.7" shape="square"/>
<pad name="-" x="1.3" y="-2" drill="0.8" diameter="1.7"/>
<text x="-1.76" y="-0.94" size="1.27" layer="21" font="vector" ratio="15">+</text>
<text x="0" y="2.286" size="0.762" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<text x="0" y="1.27" size="0.762" layer="27" font="vector" ratio="15" align="center">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="-2.54" y="1.524" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-1.524" size="1.778" layer="96" align="top-left">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="LED">
<wire x1="0" y1="-1.27" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="0.762" y1="2.032" x2="2.159" y2="3.429" width="0.1524" layer="94"/>
<wire x1="1.905" y1="1.905" x2="3.302" y2="3.302" width="0.1524" layer="94"/>
<text x="1.27" y="3.81" size="1.778" layer="95" align="bottom-center">&gt;NAME</text>
<text x="1.27" y="-2.032" size="1.778" layer="97" align="top-center">&gt;LABEL</text>
<pin name="C" x="5.08" y="0" visible="off" length="short" rot="R180"/>
<pin name="A" x="-2.54" y="0" visible="off" length="short"/>
<polygon width="0.1524" layer="94">
<vertex x="2.159" y="3.429"/>
<vertex x="1.27" y="3.048"/>
<vertex x="1.778" y="2.54"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="3.302" y="3.302"/>
<vertex x="2.413" y="2.921"/>
<vertex x="2.921" y="2.413"/>
</polygon>
<text x="1.27" y="-4.318" size="1.778" layer="96" align="top-center">&gt;VALUE</text>
</symbol>
<symbol name="CAP">
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="-2.54" y="-1.27" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="4.318" y="-1.27" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="INDUCTOR">
<wire x1="-2.54" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="0" y1="0" x2="1.27" y2="1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="2.54" y1="0" x2="1.27" y2="1.27" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="2.54" y1="0" x2="3.81" y2="1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="5.08" y1="0" x2="3.81" y2="1.27" width="0.254" layer="94" curve="90" cap="flat"/>
<text x="-2.54" y="2.032" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-0.762" size="1.778" layer="96" align="top-left">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="7.62" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="DIODE-SCHOTTKY">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.397" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.397" width="0.254" layer="94"/>
<text x="-2.54" y="2.286" size="1.778" layer="95">&gt;NAME</text>
<pin name="A" x="-5.08" y="0" visible="off" length="short"/>
<pin name="C" x="5.08" y="0" visible="off" length="short" rot="R180"/>
<wire x1="1.27" y1="1.397" x2="1.778" y2="1.397" width="0.254" layer="94"/>
<wire x1="1.778" y1="1.397" x2="1.778" y2="1.016" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.397" x2="0.762" y2="-1.397" width="0.254" layer="94"/>
<wire x1="0.762" y1="-1.397" x2="0.762" y2="-1.016" width="0.254" layer="94"/>
<text x="-2.54" y="-2.286" size="1.778" layer="96" align="top-left">&gt;VALUE</text>
<text x="-2.54" y="-4.826" size="1.778" layer="97" align="top-left">&gt;MPN</text>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="1.27" y2="0" width="0.1524" layer="94"/>
</symbol>
<symbol name="SW-DPDT">
<description>DPDT Slide Switch

Modification:
1. SC - corrected the pins connection point's location.</description>
<pin name="2" x="-7.62" y="5.08" visible="pin" length="middle"/>
<pin name="5" x="-7.62" y="-5.08" visible="pin" length="middle"/>
<pin name="1" x="7.62" y="7.62" visible="pin" length="middle" rot="R180"/>
<pin name="3" x="7.62" y="2.54" visible="pin" length="middle" rot="R180"/>
<pin name="4" x="7.62" y="-2.54" visible="pin" length="middle" rot="R180"/>
<pin name="6" x="7.62" y="-7.62" visible="pin" length="middle" rot="R180"/>
<circle x="-2.159" y="5.08" radius="0.3302" width="0.1524" layer="94"/>
<circle x="-2.159" y="-5.08" radius="0.3302" width="0.1524" layer="94"/>
<circle x="2.159" y="7.62" radius="0.3302" width="0.1524" layer="94"/>
<circle x="2.159" y="2.54" radius="0.3302" width="0.1524" layer="94"/>
<circle x="2.159" y="-2.54" radius="0.3302" width="0.1524" layer="94"/>
<circle x="2.159" y="-7.62" radius="0.3302" width="0.1524" layer="94"/>
<wire x1="-1.778" y1="5.08" x2="1.778" y2="7.62" width="0.1524" layer="94"/>
<wire x1="-1.778" y1="-5.08" x2="1.778" y2="-2.54" width="0.1524" layer="94"/>
<text x="0" y="12.7" size="1.778" layer="95" align="top-right">&gt;NAME</text>
<text x="0" y="-10.16" size="1.778" layer="97" align="top-right">&gt;LABEL</text>
</symbol>
<symbol name="CAP-POL">
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="-1.016" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1" x2="2.4892" y2="-1.8542" width="0.254" layer="94" curve="-37.878202" cap="flat"/>
<wire x1="-2.4669" y1="-1.8504" x2="0" y2="-1.0161" width="0.254" layer="94" curve="-37.376341" cap="flat"/>
<text x="-3.302" y="-4.064" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.08" y="-4.064" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.253" y1="0.668" x2="-1.364" y2="0.795" layer="94"/>
<rectangle x1="-1.872" y1="0.287" x2="-1.745" y2="1.176" layer="94"/>
<pin name="+" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="-" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
</symbol>
<symbol name="SW-PB">
<text x="0" y="2.54" size="1.778" layer="95" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.016" size="1.778" layer="97" align="top-center">&gt;LABEL</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<polygon width="0.254" layer="94">
<vertex x="-2.54" y="1.016"/>
<vertex x="2.54" y="1.016"/>
<vertex x="2.54" y="1.27"/>
<vertex x="0.762" y="1.27"/>
<vertex x="0.762" y="1.778"/>
<vertex x="-0.762" y="1.778"/>
<vertex x="-0.762" y="1.27"/>
<vertex x="-2.54" y="1.27"/>
</polygon>
<circle x="-2.2098" y="0" radius="0.3302" width="0.1524" layer="94"/>
<circle x="2.2098" y="0" radius="0.3302" width="0.1524" layer="94"/>
</symbol>
<symbol name="BUZZER">
<wire x1="2.54" y1="-5.08" x2="2.54" y2="5.08" width="0.1524" layer="94" curve="-180"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="-5.08" width="0.1524" layer="94"/>
<pin name="+" x="-5.08" y="2.54" visible="off" length="short"/>
<pin name="-" x="-5.08" y="-2.54" visible="off" length="short"/>
<wire x1="-2.54" y1="2.54" x2="-1.905" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-1.905" y2="-2.54" width="0.1524" layer="94"/>
<text x="-2.286" y="3.81" size="1.778" layer="94" align="center">+</text>
<text x="-5.08" y="5.588" size="1.778" layer="95">&gt;NAME</text>
</symbol>
<symbol name="SW-SPDT">
<pin name="2" x="-5.08" y="0" visible="pin" length="short"/>
<pin name="3" x="5.08" y="2.54" visible="pin" length="short" rot="R180"/>
<pin name="1" x="5.08" y="-2.54" visible="pin" length="short" rot="R180"/>
<wire x1="-1.778" y1="0" x2="1.778" y2="2.54" width="0.1524" layer="94"/>
<text x="-5.08" y="5.08" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-7.62" size="1.778" layer="97">&gt;LABEL</text>
<circle x="-2.159" y="0" radius="0.3302" width="0.1524" layer="94"/>
<circle x="2.159" y="2.54" radius="0.3302" width="0.1524" layer="94"/>
<circle x="2.159" y="-2.54" radius="0.3302" width="0.1524" layer="94"/>
</symbol>
<symbol name="DIODE-ZENER">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.778" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.27" x2="0.762" y2="-1.524" width="0.254" layer="94"/>
<text x="-2.54" y="2.286" size="1.778" layer="95">&gt;NAME</text>
<pin name="A" x="-5.08" y="0" visible="off" length="short"/>
<pin name="C" x="5.08" y="0" visible="off" length="short" rot="R180"/>
<text x="-2.54" y="-2.286" size="1.778" layer="96" align="top-left">&gt;VALUE</text>
<text x="-2.54" y="-4.826" size="1.778" layer="97" align="top-left">&gt;MPN</text>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="1.27" y2="0" width="0.1524" layer="94"/>
</symbol>
<symbol name="LED-RGB-ADD">
<wire x1="-7.62" y1="5.08" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="-2.54" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<pin name="VDD" x="-10.16" y="2.54" length="short"/>
<pin name="DOUT" x="-10.16" y="-2.54" length="short"/>
<pin name="DIN" x="10.16" y="2.54" length="short" rot="R180"/>
<pin name="GND" x="10.16" y="-2.54" length="short" rot="R180"/>
<wire x1="7.62" y1="-2.54" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<text x="-4.826" y="5.842" size="1.778" layer="95">&gt;Name</text>
<text x="-2.54" y="-7.62" size="1.778" layer="97">&gt;MPN</text>
</symbol>
<symbol name="VARIABLE-RESISTOR">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="0" y="-2.54" size="1.778" layer="95" align="top-center">&gt;NAME</text>
<text x="0" y="-5.08" size="1.778" layer="96" align="top-center">&gt;VALUE</text>
<pin name="3" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<polygon width="0.254" layer="94">
<vertex x="-1.27" y="2.54"/>
<vertex x="0" y="1.27"/>
<vertex x="1.27" y="2.54"/>
</polygon>
</symbol>
<symbol name="MIC">
<circle x="0" y="0" radius="5.588" width="0.254" layer="94"/>
<wire x1="5.842" y1="5.08" x2="5.842" y2="-5.08" width="0.508" layer="94"/>
<pin name="+" x="-10.16" y="2.54" length="middle"/>
<pin name="-" x="-10.16" y="-2.54" length="middle"/>
<text x="-4.318" y="6.096" size="1.778" layer="94">&gt;NAME</text>
</symbol>
<symbol name="OPTO-REFLECTIVE-SENSOR">
<wire x1="4.318" y1="-1.524" x2="2.54" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.762" y1="-1.524" x2="2.54" y2="-1.524" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.524" x2="4.318" y2="1.524" width="0.254" layer="94"/>
<wire x1="4.318" y1="1.524" x2="2.54" y2="1.524" width="0.254" layer="94"/>
<wire x1="2.54" y1="1.524" x2="0.762" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.762" y1="1.524" x2="2.54" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="0" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<polygon width="0.254" layer="94">
<vertex x="-5.02" y="-2.48"/>
<vertex x="-3.7" y="-2.1"/>
<vertex x="-4.6" y="-1.2"/>
</polygon>
<pin name="CATHODE" x="2.54" y="-7.62" visible="off" length="middle" rot="R90"/>
<pin name="EMITTER" x="-5.08" y="-7.62" visible="off" length="middle" rot="R90"/>
<pin name="ANODE" x="2.54" y="7.62" visible="off" length="middle" rot="R270"/>
<pin name="COLLECTOR" x="-5.08" y="7.62" visible="off" length="middle" rot="R270"/>
<wire x1="6.35" y1="5.08" x2="6.35" y2="-5.08" width="0.254" layer="94"/>
<wire x1="6.35" y1="-5.08" x2="-6.35" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-6.35" y1="-5.08" x2="-7.62" y2="-3.81" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-3.81" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="5.08" x2="6.35" y2="5.08" width="0.254" layer="94"/>
<text x="3.81" y="3.937" size="0.8128" layer="94" rot="R180">A</text>
<text x="3.81" y="-3.048" size="0.8128" layer="94" rot="R180">C</text>
<text x="-5.842" y="3.937" size="0.8128" layer="94" rot="R180">C</text>
<text x="-5.842" y="-3.048" size="0.8128" layer="94" rot="R180">E</text>
<text x="-8.382" y="-5.334" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="9.144" y="-5.334" size="1.778" layer="97" rot="R90">&gt;MPN</text>
<wire x1="2.54" y1="2.54" x2="2.54" y2="1.524" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="-1.524" width="0.254" layer="94"/>
</symbol>
<symbol name="DIODES-SCHOTTKY-AC">
<description>Schottky Diodes Pair&lt;br&gt;
Common Anode-Cathode&lt;br&gt;
&lt;br&gt;
Created by: Wai Weng</description>
<wire x1="1.27" y1="-3.81" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="-3.81" width="0.254" layer="94"/>
<wire x1="-1.397" y1="-1.27" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-3.81" x2="0" y2="-3.81" width="0.254" layer="94"/>
<wire x1="0" y1="-3.81" x2="1.27" y2="-3.81" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="1.397" y2="-1.27" width="0.254" layer="94"/>
<text x="2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<pin name="ANODE" x="0" y="-7.62" visible="off" length="short" rot="R90"/>
<pin name="CATHODE" x="0" y="7.62" visible="off" length="short" rot="R270"/>
<wire x1="-1.397" y1="-1.27" x2="-1.397" y2="-0.762" width="0.254" layer="94"/>
<wire x1="-1.397" y1="-0.762" x2="-1.016" y2="-0.762" width="0.254" layer="94"/>
<wire x1="1.397" y1="-1.27" x2="1.397" y2="-1.778" width="0.254" layer="94"/>
<wire x1="1.397" y1="-1.778" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<text x="2.54" y="2.286" size="1.778" layer="97" align="top-left">&gt;MPN</text>
<wire x1="0" y1="-5.08" x2="0" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="1.27" y1="1.27" x2="0" y2="3.81" width="0.254" layer="94"/>
<wire x1="0" y1="3.81" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.397" y1="3.81" x2="0" y2="3.81" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="3.81" x2="1.397" y2="3.81" width="0.254" layer="94"/>
<wire x1="-1.397" y1="3.81" x2="-1.397" y2="4.318" width="0.254" layer="94"/>
<wire x1="-1.397" y1="4.318" x2="-1.016" y2="4.318" width="0.254" layer="94"/>
<wire x1="1.397" y1="3.81" x2="1.397" y2="3.302" width="0.254" layer="94"/>
<wire x1="1.397" y1="3.302" x2="1.016" y2="3.302" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="3.81" width="0.1524" layer="94"/>
<pin name="COMMON" x="0" y="0" visible="off" length="point"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="RESISTOR" prefix="R">
<description>Resistor</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="_(2012)" package="2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="SMD Resistor" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
<technology name="_0R">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="S Resistor 0R 0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="0R" constant="no"/>
</technology>
<technology name="_100K">
<attribute name="CYTRON-PC" value="S-R-100K-85" constant="no"/>
<attribute name="DESC" value="S Resistor 100K,1%,1/8W,0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="100K" constant="no"/>
</technology>
<technology name="_100R">
<attribute name="CYTRON-PC" value="S-R-100-85" constant="no"/>
<attribute name="DESC" value="S Resistor 100R,1%,1/8W,0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="100R" constant="no"/>
</technology>
<technology name="_10K">
<attribute name="CYTRON-PC" value="S-R-10K-85-1P" constant="no"/>
<attribute name="DESC" value="S Resistor 10K,1%,1/8W,0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="10K" constant="no"/>
</technology>
<technology name="_10R">
<attribute name="CYTRON-PC" value="S-R-10-85" constant="no"/>
<attribute name="DESC" value="S Resistor 10R,1%,1/8W,0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="10R" constant="no"/>
</technology>
<technology name="_120R">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="S Resistor 120R 0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="120R" constant="no"/>
</technology>
<technology name="_12K4_1P">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="S Resistor 12K4 0805 1%" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="12K4 1%" constant="no"/>
</technology>
<technology name="_15K">
<attribute name="CYTRON-PC" value="S-R-15K-85" constant="no"/>
<attribute name="DESC" value="S Resistor 15K,1%,1/8W,0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="15K" constant="no"/>
</technology>
<technology name="_18K">
<attribute name="CYTRON-PC" value="S-R-18K-85" constant="no"/>
<attribute name="DESC" value="S Resistor 18K,1%,1/8W,0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="18K" constant="no"/>
</technology>
<technology name="_1K">
<attribute name="CYTRON-PC" value="S-R-1K-85" constant="no"/>
<attribute name="DESC" value="S Resistor 1K,1%,1/8W,0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="1K" constant="no"/>
</technology>
<technology name="_1K5">
<attribute name="CYTRON-PC" value="S-R-1K5-85" constant="no"/>
<attribute name="DESC" value="S Resistor 1.5K,1%,1/8W,0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="1K5" constant="no"/>
</technology>
<technology name="_1M">
<attribute name="CYTRON-PC" value="S-R-1M-85" constant="no"/>
<attribute name="DESC" value="S Resistor 1M,1%,1/8W,0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="1M" constant="no"/>
</technology>
<technology name="_1M5">
<attribute name="CYTRON-PC" value="S-R-1M5-85" constant="no"/>
<attribute name="DESC" value="S Resistor 1.5M,1%,1/8W,0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="1M5" constant="no"/>
</technology>
<technology name="_220K">
<attribute name="CYTRON-PC" value="S-R-220K-85" constant="no"/>
<attribute name="DESC" value="S Resistor 220K,1%,1/8W,0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="220K" constant="no"/>
</technology>
<technology name="_22K">
<attribute name="CYTRON-PC" value="S-R-22K-85" constant="no"/>
<attribute name="DESC" value="S Resistor 22K,5%,1/8W,0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="22K" constant="no"/>
</technology>
<technology name="_22K_1P">
<attribute name="CYTRON-PC" value="S-R-22K-85-1P" constant="no"/>
<attribute name="DESC" value="S Resistor 22K,1%,1/8W,0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="22K 1%" constant="no"/>
</technology>
<technology name="_24K">
<attribute name="CYTRON-PC" value="S-R-24K-85" constant="no"/>
<attribute name="DESC" value="S Resistor 24K,1%,1/8W,0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="24K" constant="no"/>
</technology>
<technology name="_27R">
<attribute name="CYTRON-PC" value="S-R-27-85" constant="no"/>
<attribute name="DESC" value="S Resistor 27R,1%,1/8W,0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="27R" constant="no"/>
</technology>
<technology name="_2K">
<attribute name="CYTRON-PC" value="S-R-2K-85" constant="no"/>
<attribute name="DESC" value="S Resistor 2K,1%,1/8W,0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="2K" constant="no"/>
</technology>
<technology name="_2K2">
<attribute name="CYTRON-PC" value="S-R-2K2-85" constant="no"/>
<attribute name="DESC" value="S Resistor 2.2K,1%,1/8W,0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="2K2" constant="no"/>
</technology>
<technology name="_2K7">
<attribute name="CYTRON-PC" value="S-R-2K7-85" constant="no"/>
<attribute name="DESC" value="S Resistor 2.7K,1%,1/8W,0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="2K7" constant="no"/>
</technology>
<technology name="_330R">
<attribute name="CYTRON-PC" value="S-R-330-85" constant="no"/>
<attribute name="DESC" value="S Resistor 330R,1%,1/8W,0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="330R" constant="no"/>
</technology>
<technology name="_33K">
<attribute name="CYTRON-PC" value="S-R-33K-85" constant="no"/>
<attribute name="DESC" value="S Resistor 33K,1%,1/8W,0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="33K" constant="no"/>
</technology>
<technology name="_33R">
<attribute name="CYTRON-PC" value="S-R-33-85" constant="no"/>
<attribute name="DESC" value="S Resistor 33R,1%,1/8W,0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="33R" constant="no"/>
</technology>
<technology name="_390R">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="S Resistor 390R 0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="390R" constant="no"/>
</technology>
<technology name="_39K">
<attribute name="CYTRON-PC" value="S-R-39K-85" constant="no"/>
<attribute name="DESC" value="S Resistor 39K,1%,1/8W,0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="39K" constant="no"/>
</technology>
<technology name="_44K2">
<attribute name="CYTRON-PC" value="S-R-44K2-85" constant="no"/>
<attribute name="DESC" value="S Resistor 44K2,1%,1/8W,0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="44K2" constant="no"/>
</technology>
<technology name="_470K">
<attribute name="CYTRON-PC" value="S-R-470K-85" constant="no"/>
<attribute name="DESC" value="S Resistor 470K,1%,1/8W,0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="470K" constant="no"/>
</technology>
<technology name="_470R">
<attribute name="CYTRON-PC" value="S-R-470-85" constant="no"/>
<attribute name="DESC" value="S Resistor 470R,1%,1/8W,0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="470R" constant="no"/>
</technology>
<technology name="_47K">
<attribute name="CYTRON-PC" value="S-R-47K-85" constant="no"/>
<attribute name="DESC" value="S Resistor 47K,1%,1/8W,0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="47K" constant="no"/>
</technology>
<technology name="_47R">
<attribute name="CYTRON-PC" value="S-R-47R-85" constant="no"/>
<attribute name="DESC" value="S Resistor 47R,1%,1/8W,0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="47R" constant="no"/>
</technology>
<technology name="_49R9_1P">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="S Resistor 49R9 0805 1%" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="49R9 1%" constant="no"/>
</technology>
<technology name="_4K7">
<attribute name="CYTRON-PC" value="S-R-4K7-85" constant="no"/>
<attribute name="DESC" value="S Resistor 4.7K,1%,1/8W,0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="4K7" constant="no"/>
</technology>
<technology name="_4R7">
<attribute name="CYTRON-PC" value="S-R-4R7-85" constant="no"/>
<attribute name="DESC" value="S Resistor 4.7R,1%,1/8W,0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="4R7" constant="no"/>
</technology>
<technology name="_53K6">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="S Resistor 53K6 0805 1%" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="53K6" constant="no"/>
</technology>
<technology name="_54K9">
<attribute name="CYTRON-PC" value="S-R-54K9-85" constant="no"/>
<attribute name="DESC" value="S Resistor 54K9,1%,1/8W,0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="54K9" constant="no"/>
</technology>
<technology name="_560K">
<attribute name="CYTRON-PC" value="S-R-560K-85" constant="no"/>
<attribute name="DESC" value="S Resistor 560K,1%,1/8W,0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="560K" constant="no"/>
</technology>
<technology name="_5R1">
<attribute name="CYTRON-PC" value="S-R-5R1-85" constant="no"/>
<attribute name="DESC" value="S Resistor 5.1R,1%,1/8W,0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="5R1" constant="no"/>
</technology>
<technology name="_680R">
<attribute name="CYTRON-PC" value="S-R-680-85" constant="no"/>
<attribute name="DESC" value="S Resistor 680R,1%,1/8W,0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="680R" constant="no"/>
</technology>
<technology name="_68K">
<attribute name="CYTRON-PC" value="S-R-68K-85" constant="no"/>
<attribute name="DESC" value="S Resistor 68K,1%,1/8W,0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="68K" constant="no"/>
</technology>
<technology name="_68R">
<attribute name="CYTRON-PC" value="S-R-68-85" constant="no"/>
<attribute name="DESC" value="S Resistor 68R,1%,1/8W,0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="68R" constant="no"/>
</technology>
<technology name="_75K">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="S Resistor 75K 0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="75K" constant="no"/>
</technology>
<technology name="_7K8">
<attribute name="CYTRON-PC" value="S-R-7K8-85" constant="no"/>
<attribute name="DESC" value="S Resistor 7.8K 0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="7K8" constant="no"/>
</technology>
<technology name="_820R">
<attribute name="CYTRON-PC" value="S-R-820-85" constant="no"/>
<attribute name="DESC" value="S Resistor 820R,1%,1/8W,0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="820R" constant="no"/>
</technology>
<technology name="_82K_1P">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="S Resistor 82K 0805 1%" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="82K 1%" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(TH-0.3)" package="AXIAL-0.3-TH">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="RS-025W-?" constant="no"/>
<attribute name="DESC" value="Resistor" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(TH-0.4)" package="AXIAL-0.4-TH">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="RS-025W-?" constant="no"/>
<attribute name="DESC" value="Resistor" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(6865)" package="6865">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="_0R00025">
<attribute name="CYTRON-PC" value="S-R-4W-0.25MR-2725" constant="no"/>
<attribute name="DESC" value="S Resistor 4W 0.25mR 2725" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="0.25mR 4W" constant="no"/>
</technology>
<technology name="_0R002_4W">
<attribute name="CYTRON-PC" value="S-R-4W-2MR-2725" constant="no"/>
<attribute name="DESC" value="S Resistor 4W 2mR 2725" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="2mR 4W" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(1608)" package="1608">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="SMD Resistor" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
<technology name="_100K">
<attribute name="CYTRON-PC" value="S-R-100K-63" constant="no"/>
<attribute name="DESC" value="S Resistor 100K ,1%,1/10W,0603" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="100K" constant="no"/>
</technology>
<technology name="_100R">
<attribute name="CYTRON-PC" value="S-R-100R-63" constant="no"/>
<attribute name="DESC" value="S Resistor 100R,1%,1/10W,0603" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="100R" constant="no"/>
</technology>
<technology name="_10K">
<attribute name="CYTRON-PC" value="S-R-10K-63" constant="no"/>
<attribute name="DESC" value="S Resistor 10K,1%,1/10W,0603" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="10K" constant="no"/>
</technology>
<technology name="_10R">
<attribute name="CYTRON-PC" value="S-R-10R-63" constant="no"/>
<attribute name="DESC" value="S Resistor 10R,1%,1/10W,0603" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="10R" constant="no"/>
</technology>
<technology name="_11K">
<attribute name="CYTRON-PC" value="S-R-11K-63" constant="no"/>
<attribute name="DESC" value="S Resistor 11K,1%,1/10W,0603" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="11K" constant="no"/>
</technology>
<technology name="_120K">
<attribute name="CYTRON-PC" value="S-R-120K-63" constant="no"/>
<attribute name="DESC" value="S Resistor 120K,1%,1/10W,0603" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="120K" constant="no"/>
</technology>
<technology name="_160">
<attribute name="CYTRON-PC" value="S-R-160R-63" constant="no"/>
<attribute name="DESC" value="S Resistor 160R,1%,1/10W,0603" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="160R" constant="no"/>
</technology>
<technology name="_1K">
<attribute name="CYTRON-PC" value="S-R-1K-63" constant="no"/>
<attribute name="DESC" value="S Resistor 1K,1%,1/10W,0603" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="1K" constant="no"/>
</technology>
<technology name="_1K5">
<attribute name="CYTRON-PC" value="S-R-1K5-63" constant="no"/>
<attribute name="DESC" value="S Resistor 1K5,1%,1/10W,0603" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="1K5" constant="no"/>
</technology>
<technology name="_1M">
<attribute name="CYTRON-PC" value="S-R-1M-63" constant="no"/>
<attribute name="DESC" value="S Resistor 1M,1%,1/10W,0603" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="1M" constant="no"/>
</technology>
<technology name="_200K">
<attribute name="CYTRON-PC" value="S-R-200K-63" constant="no"/>
<attribute name="DESC" value="S Resistor 200K 0603" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="200K" constant="no"/>
</technology>
<technology name="_220R">
<attribute name="CYTRON-PC" value="S-R-220R-63" constant="no"/>
<attribute name="DESC" value="S Resistor 220R,1%,1/10W,0603" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="220R" constant="no"/>
</technology>
<technology name="_22K">
<attribute name="CYTRON-PC" value="S-R-22K-63" constant="no"/>
<attribute name="DESC" value="S Resistor 22K,1%,1/10W,0603" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="22K" constant="no"/>
</technology>
<technology name="_2K">
<attribute name="CYTRON-PC" value="S-R-2K-63" constant="no"/>
<attribute name="DESC" value="S Resistor 2K,1%,1/10W,0603" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="2K" constant="no"/>
</technology>
<technology name="_2K2">
<attribute name="CYTRON-PC" value="S-R-2K2-63" constant="no"/>
<attribute name="DESC" value="S Resistor 2.2K,1%,1/10W,0603" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="2K2" constant="no"/>
</technology>
<technology name="_2K7">
<attribute name="CYTRON-PC" value="S-R-2K7-63" constant="no"/>
<attribute name="DESC" value="S Resistor 2.7K,1%,1/10W,0603" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="2K7" constant="no"/>
</technology>
<technology name="_2M2">
<attribute name="CYTRON-PC" value="S-R-2M2-63" constant="no"/>
<attribute name="DESC" value="S Resistor 2.2M 0603" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="2M2" constant="no"/>
</technology>
<technology name="_330R">
<attribute name="CYTRON-PC" value="S-R-330-63" constant="no"/>
<attribute name="DESC" value="S Resistor 330R,1%,1/10W,0603" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="330R" constant="no"/>
</technology>
<technology name="_3K9">
<attribute name="CYTRON-PC" value="S-R-3K9-63" constant="no"/>
<attribute name="DESC" value="S Resistor 3.9K,1%,1/10W,0603" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="3K9" constant="no"/>
</technology>
<technology name="_3M3">
<attribute name="CYTRON-PC" value="S-R-3M3-63" constant="no"/>
<attribute name="DESC" value="S Resistor 3.3M,1%,1/10W,0603" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="3M3" constant="no"/>
</technology>
<technology name="_47K">
<attribute name="CYTRON-PC" value="S-R-47K-63" constant="no"/>
<attribute name="DESC" value="S Resistor 47K,1%,1/10W,0603" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="47K" constant="no"/>
</technology>
<technology name="_47R">
<attribute name="CYTRON-PC" value="S-R-47R-63" constant="no"/>
<attribute name="DESC" value="S Resistor 47R,1%,1/10W,0603" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="47R" constant="no"/>
</technology>
<technology name="_4K7">
<attribute name="CYTRON-PC" value="S-R-4K7-63" constant="no"/>
<attribute name="DESC" value="S Resistor 4.7K,1%,1/10W,0603" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="4K7" constant="no"/>
</technology>
<technology name="_56K">
<attribute name="CYTRON-PC" value="S-R-56K-63" constant="no"/>
<attribute name="DESC" value="S Resistor 56K,1%,1/10W,0603" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="56K" constant="no"/>
</technology>
<technology name="_5K6">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="S Resistor 5.6K,1%,1/10W,0603" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="5K6" constant="no"/>
</technology>
<technology name="_5M1">
<attribute name="CYTRON-PC" value="S-R-5M1-63" constant="no"/>
<attribute name="DESC" value="S Resistor 5.1M,1%,1/10W,0603" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="5M1" constant="no"/>
</technology>
<technology name="_68K">
<attribute name="CYTRON-PC" value="S-R-68K-63" constant="no"/>
<attribute name="DESC" value="S Resistor 68K,1%,1/10W,0603" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="68K" constant="no"/>
</technology>
<technology name="_6K8">
<attribute name="CYTRON-PC" value="S-R-6K8-63" constant="no"/>
<attribute name="DESC" value="S Resistor 6.8K,1%,1/10W,0603" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="6K8" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(2512)" package="2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
<technology name="_R33">
<attribute name="CYTRON-PC" value="S-R-R33-2512-5-1" constant="no"/>
<attribute name="DESC" value="S Resistor  0.33R,1W,1%,2512" constant="no"/>
<attribute name="MF" value="TE CONNECTIVITY" constant="no"/>
<attribute name="MPN" value="RL73K3AR33JTDF" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="0R33" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(6432)" package="6432">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="_0R002_1W_1P">
<attribute name="CYTRON-PC" value="S-R-1W-2MR-6432-1P" constant="no"/>
<attribute name="DESC" value="S Resistor 2mR ,1W,1%,6432" constant="no"/>
<attribute name="MF" value="TE Connectivity" constant="no"/>
<attribute name="MPN" value="TLR3A10DR002FTDG" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="2mR 1W 1%" constant="no"/>
</technology>
<technology name="_0R015_1W_1P">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="S Resistor 1W 15mR 6432 1%" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="15mR 1W 1%" constant="no"/>
</technology>
<technology name="_100R_1W_1P">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="S Resistor 1W 100R 6432 1%" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="100R 1W 1%" constant="no"/>
</technology>
<technology name="_1K_1W_1P">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="S Resistor 1W 1K 6432 1%" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="1K 1W 1%" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(5930)" package="5930">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="_0R0002">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="SMD Resistor" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="2mR 5W" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(TH-0.6)" package="AXIAL-0.6-TH">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="_F_10R_1W">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="Fusible Resistor" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="10R 1W" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(3216)" package="3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="_0R">
<attribute name="CYTRON-PC" value="S-R-0R-1206" constant="no"/>
<attribute name="DESC" value="S Resistor 0 Ohm,1%,1/2W,1206" constant="no"/>
<attribute name="MF" value="UniOhm" constant="no"/>
<attribute name="MPN" value="1206 J0000T5E" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="0R" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(CEMENT-220100)" package="R-CEMENT-220100">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="_5R1_5W">
<attribute name="CYTRON-PC" value="RS-5W-5R1" constant="no"/>
<attribute name="DESC" value="Resistor 5W 5% 5R1" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="5R1 5W" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED" prefix="DS" uservalue="yes">
<description>LED</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="_(3216)" package="LED-3216">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="S-DS-?-1206" constant="no"/>
<attribute name="DESC" value="S LED 1206" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
<technology name="_BLUE">
<attribute name="CYTRON-PC" value="S-DS-B-1206" constant="no"/>
<attribute name="DESC" value="S LED Blue 1206" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="Blue" constant="no"/>
</technology>
<technology name="_GREEN">
<attribute name="CYTRON-PC" value="S-DS-G-1206" constant="no"/>
<attribute name="DESC" value="S LED Green 1206" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="Green" constant="no"/>
</technology>
<technology name="_ORANGE">
<attribute name="CYTRON-PC" value="S-DS-O-1206" constant="no"/>
<attribute name="DESC" value="S LED Orange 1206" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="Orange" constant="no"/>
</technology>
<technology name="_RED">
<attribute name="CYTRON-PC" value="S-DS-R-1206" constant="no"/>
<attribute name="DESC" value="S LED Red 1206" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="Red" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(TH-5MM)" package="LED-5MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
<technology name="_RED">
<attribute name="CYTRON-PC" value="DS-LED-5NR" constant="no"/>
<attribute name="DESC" value="LED" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="Red" constant="no"/>
</technology>
<technology name="_TSAL6200">
<attribute name="CYTRON-PC" value="SN-IR-T" constant="no"/>
<attribute name="DESC" value="IR Transmitter" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="IRTX" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(1608)" package="LED-1608">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="_BLUE">
<attribute name="CYTRON-PC" value="S-DS-B-63" constant="no"/>
<attribute name="DESC" value="S LED Blue 0603" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="Blue" constant="no"/>
</technology>
<technology name="_GREEN">
<attribute name="CYTRON-PC" value="S-DS-G-63" constant="no"/>
<attribute name="DESC" value="S LED Green 0603" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="Green" constant="no"/>
</technology>
<technology name="_RED">
<attribute name="CYTRON-PC" value="S-DS-R-63" constant="no"/>
<attribute name="DESC" value="S LED Red 0603" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="Red" constant="no"/>
</technology>
<technology name="_YELLOW">
<attribute name="CYTRON-PC" value="S-DS-Y-63" constant="no"/>
<attribute name="DESC" value="S LED Yellow 0603" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="Yellow" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(TH-5MM-LONGPAD)" package="LED-5MM-LONGPAD">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
<technology name="_RED">
<attribute name="CYTRON-PC" value="DS-LED-5NR" constant="no"/>
<attribute name="DESC" value="LED" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="Red" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(2012)" package="LED-2012">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="S-DS-?-0805" constant="no"/>
<attribute name="DESC" value="S LED 0805" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
<technology name="_BLUE">
<attribute name="CYTRON-PC" value="S-DS-B-0805" constant="no"/>
<attribute name="DESC" value="S LED Blue 0805" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="MF" value="EVERBRIGHT" constant="no"/>
<attribute name="MPN" value="EC04-0805QBD/E" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="Blue" constant="no"/>
</technology>
<technology name="_GREEN">
<attribute name="CYTRON-PC" value="S-DS-G-0805" constant="no"/>
<attribute name="DESC" value="S LED Green 0805" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="MF" value="EVERBRIGHT" constant="no"/>
<attribute name="MPN" value="EC04-0805QGD/E" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="Green" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(3216-REVERSE)" package="LED-3216-REVERSE">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="_GREEN">
<attribute name="CYTRON-PC" value="S-DS-G-1206-RM" constant="no"/>
<attribute name="DESC" value="S Reversed LED Green 1206" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="Green" constant="no"/>
</technology>
<technology name="_RED">
<attribute name="CYTRON-PC" value="S-DS-R-1206-RM" constant="no"/>
<attribute name="DESC" value="S Reversed LED Red 1206" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="Red" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(TH-5MM-HOLDER-RA)" package="LED-5MM-HOLDER-RA">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name="_TSAL6200">
<attribute name="CYTRON-PC" value="SN-IR-T" constant="no"/>
<attribute name="DESC" value="IR Tansmitter" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="Holder = DS-LED-HOLDER-RA" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(TH-5MM-RA)" package="LED-5MM-RA">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name="_TSAL6200">
<attribute name="CYTRON-PC" value="SN-IR-T" constant="no"/>
<attribute name="DESC" value="IR Tansmitter" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(10MM-TH)" package="LED-10MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name="_GREEN">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="VALUE" value="Green" constant="no"/>
</technology>
<technology name="_RED">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="VALUE" value="Red" constant="no"/>
</technology>
<technology name="_YELLOW">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="VALUE" value="Yellow" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAP-C" prefix="C" uservalue="yes">
<description>Ceramic and Multilayer Capacitor</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="_(2012)" package="2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="S Ceramic Capacitor" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
<technology name="_0.01UF">
<attribute name="CYTRON-PC" value="S-CC-10N-85" constant="no"/>
<attribute name="DESC" value="S Ceramic Cap 10nF 10% 50V X7R 0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="0.01uF" constant="no"/>
</technology>
<technology name="_0.1UF">
<attribute name="CYTRON-PC" value="S-CC-100N-85" constant="no"/>
<attribute name="DESC" value="S Ceramic Cap 0.1UF 10% 50V X7R 0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="0.1uF" constant="no"/>
</technology>
<technology name="_100V_0.1UF">
<attribute name="CYTRON-PC" value="S-CC-100-100N-85" constant="no"/>
<attribute name="DESC" value="S Ceramic Cap 0.1uF 10% 100V X7R 0805" constant="no"/>
<attribute name="MF" value="Samsung" constant="no"/>
<attribute name="MPN" value="CL21B104KCFNNNE" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="100V0.1uF" constant="no"/>
</technology>
<technology name="_100V_10NF">
<attribute name="CYTRON-PC" value="S-CC-100-10N-85" constant="no"/>
<attribute name="DESC" value="S Ceramic Cap 10nF 10% 100V X7R 0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="100V10nF" constant="no"/>
</technology>
<technology name="_100V_22NF">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="S Ceramic Capacitor 100V 22nF 0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="100V22nF" constant="no"/>
</technology>
<technology name="_100V_47NF">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="S Ceramic Capacitor 100V 47nF 0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="100V47nF" constant="no"/>
</technology>
<technology name="_10UF">
<attribute name="CYTRON-PC" value="S-CC-10U-85" constant="no"/>
<attribute name="DESC" value="S Ceramic Cap 10UF 10% 10V X5R 0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="10uF" constant="no"/>
</technology>
<technology name="_150PF">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="S Ceramic Capacitor 150pF 0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="150pF" constant="no"/>
</technology>
<technology name="_15PF">
<attribute name="CYTRON-PC" value="S-CC-15P-85" constant="no"/>
<attribute name="DESC" value="S Ceramic Capacitor 15pF 0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="15pF" constant="no"/>
</technology>
<technology name="_1UF">
<attribute name="CYTRON-PC" value="S-CC-1U-85" constant="no"/>
<attribute name="DESC" value="S C Cap 1uF -20%+80% 16V Y5V 0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="1uF" constant="no"/>
</technology>
<technology name="_2.2NF">
<attribute name="CYTRON-PC" value="S-CC-0.0022U-85" constant="no"/>
<attribute name="DESC" value="SMD Cap 0.0022UF" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="2.2nF" constant="no"/>
</technology>
<technology name="_2.2UF">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="S Ceramic Capacitor 2.2uF 0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="16V2.2uF" constant="no"/>
</technology>
<technology name="_22NF">
<attribute name="CYTRON-PC" value="S-CC-22N-85" constant="no"/>
<attribute name="DESC" value="S Ceramic Cap 22n 50V X7R 0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="22nF" constant="no"/>
</technology>
<technology name="_22PF">
<attribute name="CYTRON-PC" value="S-CC-22P-85" constant="no"/>
<attribute name="DESC" value="S Ceramic Cap 22pF 5% 50V C0G/NP0 0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="22pF" constant="no"/>
</technology>
<technology name="_22UF">
<attribute name="CYTRON-PC" value="S-CC-22U-85" constant="no"/>
<attribute name="DESC" value="S Ceramic Cap 22UF 10V X5R 0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="22uF" constant="no"/>
</technology>
<technology name="_270PF">
<attribute name="CYTRON-PC" value="S-CC-270P-85" constant="no"/>
<attribute name="DESC" value="S Ceramic Cap 270pF 10% 50V X7R 0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="270pF" constant="no"/>
</technology>
<technology name="_30PF">
<attribute name="CYTRON-PC" value="S-CC-30P-85" constant="no"/>
<attribute name="DESC" value="S Ceramic Cap 30pF 5% 50V C0G/NP0 0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="30pF" constant="no"/>
</technology>
<technology name="_330PF">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="S Ceramic Capacitor 330pF 0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="330pF" constant="no"/>
</technology>
<technology name="_4.7NF">
<attribute name="CYTRON-PC" value="S-CC-4.7N-85" constant="no"/>
<attribute name="DESC" value="S Ceramic Cap 4.7nF 10% 50V X7R 0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="4.7nF" constant="no"/>
</technology>
<technology name="_4.7UF">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="S Ceramic Capacitor 4.7uF 0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="4.7uF" constant="no"/>
</technology>
<technology name="_470NF">
<attribute name="CYTRON-PC" value="S-CC-470N-85" constant="no"/>
<attribute name="DESC" value="S C Cap 470nF -20%, +80% 50V Y5V 0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="470nF" constant="no"/>
</technology>
<technology name="_470PF">
<attribute name="CYTRON-PC" value="S-CC-470P-85" constant="no"/>
<attribute name="DESC" value="S Ceramic Cap 470pF 10% 50V X7R 0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="470pF" constant="no"/>
</technology>
<technology name="_47PF">
<attribute name="CYTRON-PC" value="S-CC-47P-85" constant="no"/>
<attribute name="DESC" value="S Ceramic Cap 47pF 5% 50V C0G/NP0 0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="47pF" constant="no"/>
</technology>
<technology name="_560PF">
<attribute name="CYTRON-PC" value="S-CC-560P-85" constant="no"/>
<attribute name="DESC" value="S Ceramic Cap 560pF 10% 50V X7R 0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="560pF" constant="no"/>
</technology>
<technology name="_6.8NF">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="S Ceramic Capacitor 6.8nF 0805" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="6.8nF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(1608)" package="1608">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="S Ceramic Capacitor 0603" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
<technology name="_0.1UF">
<attribute name="CYTRON-PC" value="S-CC-100N-63" constant="no"/>
<attribute name="DESC" value="S C Cap 0.1uF -20%+80% 50V Y5V 0603" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="0.1uF" constant="no"/>
</technology>
<technology name="_10NF">
<attribute name="CYTRON-PC" value="S-CC-10N-63" constant="no"/>
<attribute name="DESC" value="S Ceramic Cap 10nF 10% 50V X7R 0603" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="10nF" constant="no"/>
</technology>
<technology name="_12PF">
<attribute name="CYTRON-PC" value="S-CC-12P-63" constant="no"/>
<attribute name="DESC" value="S Ceramic Cap 12PF +-5% 50V C0G/NP0 0603" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="12pF" constant="no"/>
</technology>
<technology name="_1NF">
<attribute name="CYTRON-PC" value="S-CC-1N-63" constant="no"/>
<attribute name="DESC" value="S Ceramic Cap 1nF 5% 50V X7R 0603" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="1nF" constant="no"/>
</technology>
<technology name="_1UF">
<attribute name="CYTRON-PC" value="S-CC-1U-63" constant="no"/>
<attribute name="DESC" value="S C Cap 1uF -20%+80% 10V Y5V 0603" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="1uF" constant="no"/>
</technology>
<technology name="_30PF">
<attribute name="CYTRON-PC" value="S-CC-30P-63" constant="no"/>
<attribute name="DESC" value="S Ceramic Cap 30pF 5%  50V C0G/NP0 0603" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="30pF" constant="no"/>
</technology>
<technology name="_560PF">
<attribute name="CYTRON-PC" value="S-CC-560P-63" constant="no"/>
<attribute name="DESC" value="S Ceramic Cap 560pF 10% 50V X7R 0603" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="560pF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(3216)" package="3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="SMD Ceramic Capacitor 1206" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
<technology name="_10UF">
<attribute name="CYTRON-PC" value="S-CC-50-10U-1206" constant="no"/>
<attribute name="DESC" value="S Ceramic Cap 10uF 10% 50V X5R 1206" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="MF" value="SAMSUNG" constant="no"/>
<attribute name="MPN" value="CL31A106KBHNNNE" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="50V10uF" constant="no"/>
</technology>
<technology name="_470NF">
<attribute name="CYTRON-PC" value="S-CC-470N-1206" constant="no"/>
<attribute name="DESC" value="S Ceramic Cap 470nF 10% 50V X7R 1206" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="MF" value="SAMSUNG" constant="no"/>
<attribute name="MPN" value="CL31B474KBHNNNE" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="50V470nF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(3225)" package="3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="S Ceramic Capacitor 3225" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
<technology name="_100V_4.7UF">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="S Ceramic Capacitor 100V 4.7uF 3225" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="100V4.7uF" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="INDUCTOR" prefix="L">
<description>Inductor</description>
<gates>
<gate name="G$1" symbol="INDUCTOR" x="0" y="0"/>
</gates>
<devices>
<device name="_(CD54)" package="CD54">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="SMD Inductor" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
<technology name="_250UH">
<attribute name="CYTRON-PC" value="S-L-250UH-0.3A" constant="no"/>
<attribute name="DESC" value="S Inductor 250uH 0.3A CD54" constant="no"/>
<attribute name="MF" value="JinXiangHai" constant="no"/>
<attribute name="MPN" value="SP54-251-T" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="250uH" constant="no"/>
</technology>
<technology name="_33UH">
<attribute name="CYTRON-PC" value="S-L-33UH-0.75A" constant="no"/>
<attribute name="DESC" value="S Inductor 33uH 0.75A SDRH5D28" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="33uH" constant="no"/>
</technology>
<technology name="_4.7UH">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="SMD Inductor" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="4.7uH" constant="no"/>
</technology>
<technology name="_6.8UH">
<attribute name="CYTRON-PC" value="S-L-6.8UH-2.5A" constant="no"/>
<attribute name="DESC" value="S Inductor 6.8uH 2.5A CD54" constant="no"/>
<attribute name="MF" value="JinXiangHai" constant="no"/>
<attribute name="MPN" value="SP54-6R8K-T" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="6.8uH" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(JDO5022)" package="L-JDO5022">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="_100UH">
<attribute name="CYTRON-PC" value="S-L-100UH-3A" constant="no"/>
<attribute name="DESC" value="S Inductor 100uH 3A JDO5022" constant="no"/>
<attribute name="MF" value="JinXiangHai" constant="no"/>
<attribute name="MPN" value="JDO5022-101M" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="100uH" constant="no"/>
</technology>
<technology name="_33UH">
<attribute name="CYTRON-PC" value="S-L-33UH-5.5A" constant="no"/>
<attribute name="DESC" value="S Inductor 33uH  5.5A JDO5022" constant="no"/>
<attribute name="MF" value="JinXiangHai" constant="no"/>
<attribute name="MPN" value="JDO5022-330M" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="33uH" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(JDO3340)" package="L-JDO3340">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="_68UH">
<attribute name="CYTRON-PC" value="S-L-68UH-1.68A" constant="no"/>
<attribute name="DESC" value="S Inductor 68uH 1.68A JDO3340" constant="no"/>
<attribute name="MF" value="JinXiangHai" constant="no"/>
<attribute name="MPN" value="JDO3340-680M" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="68uH" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(L-SDRH8D43)" package="L-SDRH8D43">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="_47UH">
<attribute name="CYTRON-PC" value="S-L-47UH-1.8A" constant="no"/>
<attribute name="DESC" value="S Inductor 47uH 1.8A SDRH8D43" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="47uH" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(L-SDRH5D28)" package="L-SDRH5D28">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="_2.2UH">
<attribute name="CYTRON-PC" value="S-L-2.2UH-2.6A" constant="no"/>
<attribute name="DESC" value="S Inductor 2.2uH 2.6A SDRH5D28-2R2M" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="SDRH5D28-2R2M" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="2.2uH" constant="no"/>
</technology>
<technology name="_330UH">
<attribute name="CYTRON-PC" value="S-L-330UH-0.25A" constant="no"/>
<attribute name="DESC" value="S Inductor 330uH 0.25A SDRH5D28" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="SDRH5D28-331M" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="330uH" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(4532)" package="4532">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
<technology name="_680UH">
<attribute name="CYTRON-PC" value="S-L-680UH-0.05A" constant="no"/>
<attribute name="DESC" value="S Inductor 680uH 50mA 1812" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="MF" value="TDK" constant="no"/>
<attribute name="MPN" value="NL453232T-681J-PF" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="680uH 50mA" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(2012)" package="2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="_10UH">
<attribute name="CYTRON-PC" value="S-L-10UH-?A" constant="no"/>
<attribute name="DESC" value="" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="10uH" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(L-120120-SHIELDED)" package="L-120120-SHIELDED">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="_22UH">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="S Inductor 22uH 5.3A 120120-SHIELDED" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="22uH" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODE-SCHOTTKY" prefix="D">
<description>Schottky Diode&lt;br&gt;
&lt;br&gt;&lt;br&gt;
Created by: Wai Weng&lt;br&gt;</description>
<gates>
<gate name="G$1" symbol="DIODE-SCHOTTKY" x="0" y="0"/>
</gates>
<devices>
<device name="_(SMA)" package="D-SMA">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="S-D-?-SMA" constant="no"/>
<attribute name="DESC" value="SMD Schottky Diode" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
<technology name="_15MQ040N">
<attribute name="CYTRON-PC" value="S-D-15MQ040N-SMA" constant="no"/>
<attribute name="DESC" value="S Schottky Diode 40V 3A 15MQ040N SMA" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="15MQ040N" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="40V 3A" constant="no"/>
</technology>
<technology name="_MBRA140T">
<attribute name="CYTRON-PC" value="S-D-MBRA140T-SMA" constant="no"/>
<attribute name="DESC" value="S Schottky Diode 40V 1A MBRA140T3G SMA" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="MBRA140T" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="40V 1A" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(SMB)" package="D-SMB">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="_MBRS360BT3G">
<attribute name="CYTRON-PC" value="S-D-MBRS360BT3G-SMB" constant="no"/>
<attribute name="DESC" value="S Schottky Diode 60V 3A SMB" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="60V 3A" constant="no"/>
</technology>
<technology name="_SSB44">
<attribute name="CYTRON-PC" value="S-D-SSB44-SMB" constant="no"/>
<attribute name="DESC" value="SMD Schottky Diode" constant="no"/>
<attribute name="MF" value="Vishay" constant="no"/>
<attribute name="MPN" value="SSB44-E3/52T" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="40V 4A" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(SMC)" package="D-SMC">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="_SS54">
<attribute name="CYTRON-PC" value="S-D-SS54-SMC" constant="no"/>
<attribute name="DESC" value="S Schottky Rectifier 40V 5.0A" constant="no"/>
<attribute name="MF" value="MULTICOMP" constant="no"/>
<attribute name="MPN" value="SS54" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="40V 5A" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SW-SLIDE-DPDT" prefix="S">
<description>DPDT Slide Switch</description>
<gates>
<gate name="G$1" symbol="SW-DPDT" x="0" y="0"/>
</gates>
<devices>
<device name="_(AYZ0202AGRLC-SMD)" package="SW-SLIDE-DPDT-AYZ0202AGRLC-SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="SMD DPDT Slide Switch - AYZ0202AGRLC" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="MF" value="CK Components" constant="no"/>
<attribute name="MPN" value="AYZ0202AGRLC" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(MSS22D18-SMD)" package="SW-SLIDE-DPDT-MSS22D18-SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="S-SS-MSS22D18" constant="no"/>
<attribute name="DESC" value="SMD DPDT Slide Switch - MSS22D18" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="MSS22D18" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAP-E-ALUMINIUM" prefix="C" uservalue="yes">
<description>Aluminium Electrolytic Capacitor&lt;br&gt;
&lt;br&gt;&lt;br&gt;
Created by: Wai Weng&lt;br&gt;</description>
<gates>
<gate name="G$1" symbol="CAP-POL" x="0" y="0"/>
</gates>
<devices>
<device name="_(CHEMICON-F55)" package="ECAP-CHEMICON-F">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="S-EC-?-?" constant="no"/>
<attribute name="DESC" value="S Electrolytic Cap" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
<technology name="_16V100UF">
<attribute name="CYTRON-PC" value="S-EC-16-100U-F55" constant="no"/>
<attribute name="DESC" value="S Electrolytic Cap 16V 100uF F55" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="16V100uF" constant="no"/>
</technology>
<technology name="_16V220UF">
<attribute name="CYTRON-PC" value="S-EC-16-220U-F55" constant="no"/>
<attribute name="DESC" value="S Electrolytic Cap 16V 220uF F55" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="16V220uF" constant="no"/>
</technology>
<technology name="_25V47UF">
<attribute name="CYTRON-PC" value="S-EC-25-47U-F55" constant="no"/>
<attribute name="DESC" value="S Electrolytic Cap 25V 47uF F55" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="25V47uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(CHEMICON-JA0)" package="ECAP-CHEMICON-J">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="SMD-EC-?-?" constant="no"/>
<attribute name="DESC" value="SMD Aluminium Electrolytic Cap" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
<technology name="_16V680UF">
<attribute name="CYTRON-PC" value="S-EC-16-680U-JA0" constant="no"/>
<attribute name="DESC" value="S Electrolytic Cap 16V 680uF JA0" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="16V680uF" constant="no"/>
</technology>
<technology name="_35V330UF">
<attribute name="CYTRON-PC" value="S-EC-35-330U-JA0" constant="no"/>
<attribute name="DESC" value="S Electrolytic Cap 35V 330uF JA0" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="35V330uF" constant="no"/>
</technology>
<technology name="_50V330UF">
<attribute name="CYTRON-PC" value="S-EC-50-330U-JA0" constant="no"/>
<attribute name="DESC" value="S Electrolytic Cap 50V 330uF JA0" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="50V330uF" constant="no"/>
</technology>
<technology name="_63V100UF">
<attribute name="CYTRON-PC" value="S-EC-63-100U-JA0" constant="no"/>
<attribute name="DESC" value="S Electrolytic Cap 63V 100uF JA0" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="63V100uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(16MM-TH)" package="ECAP-16MM-TH">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="_50V2200UF">
<attribute name="CYTRON-PC" value="CP-EC-50-2200" constant="no"/>
<attribute name="DESC" value="Aluminium Electrolytic Cap" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="50V 2200uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(18MM-TH)" package="ECAP-18MM-TH">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="_50V3300UF">
<attribute name="CYTRON-PC" value="CP-EC-50-3300" constant="no"/>
<attribute name="DESC" value="Electrolytic Capacitor 50V 3300uF" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="50V 3300uF" constant="no"/>
</technology>
<technology name="_63V2200UF">
<attribute name="CYTRON-PC" value="CP-EC-63-2200" constant="no"/>
<attribute name="DESC" value="Electrolytic Capacitor 63V 2200uF" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="63V 2200uF" constant="no"/>
</technology>
<technology name="_80V1000UF">
<attribute name="CYTRON-PC" value="CP-EC-100-1000" constant="no"/>
<attribute name="DESC" value="Electrolytic Capacitor 100V 1000uF" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="100V 1000uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(08MM-TH)" package="ECAP-08MM-TH">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="_400V4.7UF">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="Aluminium Electrolytic Cap" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="400V 4.7uF" constant="no"/>
</technology>
<technology name="_63V100UF">
<attribute name="CYTRON-PC" value="CP-EC-63-100" constant="no"/>
<attribute name="DESC" value="Aluminium Electrolytic Cap" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="63V 100uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(13MM-TH)" package="ECAP-13MM-TH">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="_50V1000UF">
<attribute name="CYTRON-PC" value="CP-EC-50-1000" constant="no"/>
<attribute name="DESC" value="Aluminium Electrolytic Cap" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="50V1000uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(CHEMICON-KE0)" package="ECAP-CHEMICON-K">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="_100V100UF">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="Aluminium Electrolytic Cap" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="100V100uF" constant="no"/>
</technology>
<technology name="_50V330UF">
<attribute name="CYTRON-PC" value="CP-EC-50-330" constant="no"/>
<attribute name="DESC" value="Aluminium Electrolytic Cap" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="50V330uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(10MM-TH)" package="ECAP-10MM-TH">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="_100V100UF">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="Aluminium Electrolytic Cap" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="100V 100uF" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SW-PB" prefix="S">
<description>Momentary Push Button</description>
<gates>
<gate name="G$1" symbol="SW-PB" x="0" y="0"/>
</gates>
<devices>
<device name="_(6036-2P)" package="SW-PB-6036-2P">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="SMD-SW-PBM-0603" constant="no"/>
<attribute name="DESC" value="SMD Push Button - 2 Pins (6.0 x 3.6mm)" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(6060-4P)" package="SW-PB-6060-4P">
<connects>
<connect gate="G$1" pin="1" pad="1 2"/>
<connect gate="G$1" pin="2" pad="3 4"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="SMD-SW-PBM-6060"/>
<attribute name="DESC" value="SMD Push Button - 4 Pins (6.0 x 6.0mm)"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(6060-4P-TH)" package="SW-PB-6060-4P-TH">
<connects>
<connect gate="G$1" pin="1" pad="1 2"/>
<connect gate="G$1" pin="2" pad="3 4"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="SW-PBM-6060"/>
<attribute name="DESC" value="Push Button - 4 Pins (6.0 x 6.0mm)"/>
<attribute name="LABEL" value=""/>
<attribute name="MF" value=""/>
<attribute name="MPN" value=""/>
<attribute name="REMARK" value=""/>
<attribute name="VALUE" value=""/>
</technology>
</technologies>
</device>
<device name="_(KAN0444)" package="S-SW-PBM-KAN0444">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="S-SP-2N-KAN0444" constant="no"/>
<attribute name="DESC" value="S Push Button KAN0444 3.9x2.9mm" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(KAN3543-RA)" package="SW-PB-KAN3543-RA">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="S-SP-2N-RA-KAN3543" constant="no"/>
<attribute name="DESC" value="S Push Button R/A KAN3543 7x3.5mm White" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(KAN0441)" package="S-SP-2N-KAN0441">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="S-SP-2N-KAN0441" constant="no"/>
<attribute name="DESC" value="S Push Button KAN0441 6x3.75 (White)" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="MF" value="GANGYUAN" constant="no"/>
<attribute name="MPN" value="KAN0441B-0252B" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(KAN0641)" package="S-SP-4N-KAN0641">
<connects>
<connect gate="G$1" pin="1" pad="1 2"/>
<connect gate="G$1" pin="2" pad="3 4"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="S-SP-4N-KAN0641 " constant="no"/>
<attribute name="DESC" value="S Push Button KAN0641 6x6mm (Black)" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="MF" value="GANGYUAN" constant="no"/>
<attribute name="MPN" value="KAN0641-0501B1" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(120120-4P)" package="SW-PB-120120-4P">
<connects>
<connect gate="G$1" pin="1" pad="1 2"/>
<connect gate="G$1" pin="2" pad="3 4"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value=""/>
<attribute name="DESC" value="Push Button - 4 Pins (12.0 x 12.0mm)"/>
<attribute name="LABEL" value=""/>
<attribute name="MF" value=""/>
<attribute name="MPN" value=""/>
<attribute name="REMARK" value=""/>
<attribute name="VALUE" value=""/>
</technology>
</technologies>
</device>
<device name="_(6060-RA-TH)" package="SW-PB-6060-RA-TH">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="SW-PBM-2N-060610RA"/>
<attribute name="DESC" value="6x6x10 Push Button 2 Pins (Right Angle)"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="MF" value=""/>
<attribute name="MPN" value=""/>
<attribute name="REMARK" value=""/>
<attribute name="VALUE" value=""/>
</technology>
</technologies>
</device>
<device name="_(KAN3246)" package="SW-PB-KAN3246">
<connects>
<connect gate="G$1" pin="1" pad="1 2"/>
<connect gate="G$1" pin="2" pad="3 4"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="S-SP-4N-KAN3246" constant="no"/>
<attribute name="DESC" value="S Push Button KAN3246 4.2x3.3mm" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BUZZER" prefix="LS">
<description>Piezo Buzzer&lt;br&gt;
&lt;br&gt;
Created by: Wai Weng</description>
<gates>
<gate name="G$1" symbol="BUZZER" x="0" y="0"/>
</gates>
<devices>
<device name="" package="BUZZER-90-CYL">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="S Buzzer Ext Drive" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(8530)" package="S-BUZZER-HXD-8530A-03627">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="S-BUZZ-HXD8530A" constant="no"/>
<attribute name="DESC" value="SMD Buzzer HXD8530A" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(9650)" package="S-BUZZER-MLT-9650">
<connects>
<connect gate="G$1" pin="+" pad="P$2"/>
<connect gate="G$1" pin="-" pad="P$1"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="S-BUZZ-MLT-9650" constant="no"/>
<attribute name="DESC" value="SMD Buzzer MLT9650" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="MLT-9650" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SW-SLIDE-SPDT" prefix="S">
<description>SMD Slide Switch</description>
<gates>
<gate name="G$1" symbol="SW-SPDT" x="0" y="0"/>
</gates>
<devices>
<device name="_(SMD)" package="SW-SLIDE-SPDT-SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="S-SS-2C-0307" constant="no"/>
<attribute name="DESC" value="Slide Switch 2pin SMD" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(9040-TH)" package="SW-SLIDE-SPDT-9040-TH">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
<connect gate="G$1" pin="3" pad="P$3"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="SW-SL-2SC-040904" constant="no"/>
<attribute name="DESC" value="Mini Slide Switch (PCB)" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODE-ZENER/TVS-UNI" prefix="D" uservalue="yes">
<description>Zener Diode / Unidirectional TVS Diode</description>
<gates>
<gate name="G$1" symbol="DIODE-ZENER" x="0" y="0"/>
</gates>
<devices>
<device name="_(SOD-123)" package="D-SOD-123">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="SMD Zener Diode" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
<technology name="_0.5W12V">
<attribute name="CYTRON-PC" value="S-D-ZD05W12V-SOD123" constant="no"/>
<attribute name="DESC" value="S Zener Diode 0.5W 12V SOD123" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="12V" constant="no"/>
</technology>
<technology name="_0.5W15V">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="S Zener Diode 0.5W 15V SOD123" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="15V" constant="no"/>
</technology>
<technology name="_0.5W3.3V">
<attribute name="CYTRON-PC" value="S-D-ZD05W3.3V-SOD123" constant="no"/>
<attribute name="DESC" value="S Zener Diode 0.5W 3.3V SOD123" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="3V3" constant="no"/>
</technology>
<technology name="_0.5W3.6V">
<attribute name="CYTRON-PC" value="S-D-ZD05W3.6V-SOD123" constant="no"/>
<attribute name="DESC" value="S Zener Diode 0.5W 3.6V SOD123" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="3V6" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(SOD-323)" package="D-SOD-323">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
<technology name="_0.3W4.3V">
<attribute name="CYTRON-PC" value="S-D-ZD03W4.3V-SOD323" constant="no"/>
<attribute name="DESC" value="SMD Zener Diode" constant="no"/>
<attribute name="MF" value="NXP" constant="no"/>
<attribute name="MPN" value="BZX384-C4V3" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="4v3" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(SMB)" package="D-SMB">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="_SM6T36A">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="S TVS Diode 36V 62A SM6T36A SMB" constant="no"/>
<attribute name="MF" value="VISHAY" constant="no"/>
<attribute name="MPN" value="SM6T36A" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="36V 62A" constant="no"/>
</technology>
<technology name="_SM6T68A">
<attribute name="CYTRON-PC" value="S-D-SM6T68A-SMB" constant="no"/>
<attribute name="DESC" value="S TVS Diode 68V 33A SM6T68A SMB" constant="no"/>
<attribute name="MF" value="VISHAY" constant="no"/>
<attribute name="MPN" value="SM6T68A" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="68V 33A" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(SMC)" package="D-SMC">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="_SM15T33A">
<attribute name="CYTRON-PC" value="S-D-SM15T33A-SMC" constant="no"/>
<attribute name="DESC" value="S TVS Diode 33V 169A SMC" constant="no"/>
<attribute name="MF" value="VISHAY" constant="no"/>
<attribute name="MPN" value="SM15T33A" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="33V 169A" constant="no"/>
</technology>
<technology name="_SM15T36A">
<attribute name="CYTRON-PC" value="S-D-SM15T36A-SMC" constant="no"/>
<attribute name="DESC" value="S TVS Diode 36V 156A SMC" constant="no"/>
<attribute name="MF" value="VISHAY" constant="no"/>
<attribute name="MPN" value="SM15T36A" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="36V 156A" constant="no"/>
</technology>
<technology name="_SM15T56A">
<attribute name="CYTRON-PC" value="S-D-SM15T56A-SMC" constant="no"/>
<attribute name="DESC" value="S TVS Diode 56V 100A SMC" constant="no"/>
<attribute name="MF" value="VISHAY" constant="no"/>
<attribute name="MPN" value="SM15T56A" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="56V 100A" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED-RGB-ADD">
<description>SMD RGB Addressable LED&lt;br&gt;
Size: 5.0 x 5.0mm</description>
<gates>
<gate name="G$1" symbol="LED-RGB-ADD" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LED-5050-WS2812B">
<connects>
<connect gate="G$1" pin="DIN" pad="4"/>
<connect gate="G$1" pin="DOUT" pad="2"/>
<connect gate="G$1" pin="GND" pad="3"/>
<connect gate="G$1" pin="VDD" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="S-DS-WS2812B-5050" constant="no"/>
<attribute name="DESC" value="SMD RGB Addressable LED" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="MPN" value="WS2812B" constant="no"/>
<attribute name="REMARK" value="WS2812B, B means newer version, 4-pin" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VARIABLE-RESISTOR" prefix="VR">
<description>Potentiometer</description>
<gates>
<gate name="G$1" symbol="VARIABLE-RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="_(3.2MM)" package="SMD-RS-VR">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="S-VR-5K" constant="no"/>
<attribute name="DESC" value="Trimmer Potentialmeter" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="5K" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(3386-TH)" package="VR-3386-TH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="_10K">
<attribute name="CYTRON-PC" value="RS-MP3386-10K" constant="no"/>
<attribute name="DESC" value="Finger Adjust Preset 10K" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="10K" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(3314J)" package="VR-3314J">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="_5K">
<attribute name="CYTRON-PC" value="S-R-VR-3314J-5K" constant="no"/>
<attribute name="DESC" value="S Variable Resistor 5K 3314J" constant="no"/>
<attribute name="MPN" value="3314J-1-502E" constant="no"/>
<attribute name="VALUE" value="5K" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(RV0932-TH)" package="VR-RV0932-TH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="_10K">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="VALUE" value="10K" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MIC" prefix="MK">
<description>Microphone&lt;br&gt;
Diameter: 6mm&lt;br&gt;
Height: 2.2mm&lt;br&gt;
&lt;br&gt;
Created by SC Lim</description>
<gates>
<gate name="G$1" symbol="MIC" x="0" y="0"/>
</gates>
<devices>
<device name="_(6MM)" package="MIC-6MM">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name="_6022P">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="Microphone 6mm" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(9.7MM)" package="MIC-9.7MM">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="_9767P">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="Microphone 9.7x6.7mm" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="OPTO-REFLECTIVE-SENSOR" prefix="U">
<description>This opto switch consist of an infrared emitting diode (clear) and an NPN silicon phototransistor (black).&lt;br&gt;
&lt;a href="http://www.everlight.com/file/ProductFile/ITR20001-T.pdf"&gt;ITR20001 Datasheet&lt;/a&gt;&lt;br&gt;
&lt;br&gt;
Created by SC Lim</description>
<gates>
<gate name="G$1" symbol="OPTO-REFLECTIVE-SENSOR" x="0" y="0"/>
</gates>
<devices>
<device name="_ITR20001" package="ITR20001">
<connects>
<connect gate="G$1" pin="ANODE" pad="ANODE"/>
<connect gate="G$1" pin="CATHODE" pad="CATHODE"/>
<connect gate="G$1" pin="COLLECTOR" pad="COLLECTOR"/>
<connect gate="G$1" pin="EMITTER" pad="EMITTER"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="SN-IRS-ITR20001T" constant="no"/>
<attribute name="DESC" value="Opto Reflective Sensor ITR20001" constant="no"/>
<attribute name="MF" value="Everlight" constant="no"/>
<attribute name="MPN" value="ITR20001/T" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_ITR1502" package="ITR1502">
<connects>
<connect gate="G$1" pin="ANODE" pad="4"/>
<connect gate="G$1" pin="CATHODE" pad="3"/>
<connect gate="G$1" pin="COLLECTOR" pad="2"/>
<connect gate="G$1" pin="EMITTER" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="S-SN-IRS-ITR1502" constant="no"/>
<attribute name="DESC" value="Opto Reflective Sensor ITR1502" constant="no"/>
<attribute name="MF" value="Everlight" constant="no"/>
<attribute name="MPN" value="ITR1502SR40A/TR8" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODES-SCHOTTKY-AC" prefix="D">
<description>Schottky Diodes Pair&lt;br&gt;
Common Anode-Cathode&lt;br&gt;
&lt;br&gt;
Created by: Wai Weng</description>
<gates>
<gate name="G$1" symbol="DIODES-SCHOTTKY-AC" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT-23">
<connects>
<connect gate="G$1" pin="ANODE" pad="1"/>
<connect gate="G$1" pin="CATHODE" pad="2"/>
<connect gate="G$1" pin="COMMON" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="S-D-BAT754S-SOT23" constant="no"/>
<attribute name="DESC" value="S Schottky Diode Pair BAT754S SOT23" constant="no"/>
<attribute name="MF" value="NXP" constant="no"/>
<attribute name="MPN" value="BAT754S" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="30V200mA" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FERRITE-BEAD" prefix="L">
<description>SMD Ferrite Bead</description>
<gates>
<gate name="G$1" symbol="INDUCTOR" x="0" y="0"/>
</gates>
<devices>
<device name="_(1608)" package="1608">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="S-FB-600R-63" constant="no"/>
<attribute name="DESC" value="S Ferrite Bead 600R 0603" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="MF" value="MURATA" constant="no"/>
<attribute name="MPN" value="BLM18KG601SN1D" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="600R 1300mA" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(2012)" package="2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="S-FB-0.035R-85" constant="no"/>
<attribute name="DESC" value="S Ferrite Bead 0.035R 0805" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="HCB-2012G-330A40T" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="0.035R 4000mA" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Supply">
<description>Supply symbols such as VCC, VIN, GND...</description>
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
<text x="0" y="-2.54" size="1.778" layer="96" align="bottom-center">&gt;VALUE</text>
</symbol>
<symbol name="+3V3">
<wire x1="0.762" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.762" y2="1.27" width="0.254" layer="94"/>
<text x="0" y="3.302" size="1.778" layer="96" rot="R90" align="center-left">&gt;VALUE</text>
<pin name="+3V3" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="VM">
<wire x1="0.762" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.762" y2="1.27" width="0.254" layer="94"/>
<text x="0" y="3.302" size="1.778" layer="96" rot="R90" align="center-left">&gt;VALUE</text>
<pin name="VM" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="VIN">
<wire x1="0.762" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.762" y2="1.27" width="0.254" layer="94"/>
<text x="0" y="3.302" size="1.778" layer="96" rot="R90" align="center-left">&gt;VALUE</text>
<pin name="VIN" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>Ground</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+3V3">
<description>+3V3</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VM" prefix="VM">
<description>V Motor</description>
<gates>
<gate name="G$1" symbol="VM" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VIN" prefix="VIN">
<description>V In</description>
<gates>
<gate name="G$1" symbol="VIN" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Header and Connector">
<description>Headers and Connectors.</description>
<packages>
<package name="TERMINAL-KAR301-02">
<description>Screw Terminal Block KAR301 - 2 Ways&lt;br&gt;
Size:&lt;br&gt;
&lt;br&gt;
&lt;b&gt;&lt;i&gt;* Need to measure the actual dimension. Make sure the pin spacing is correct when joining 2 terminal together.&lt;/i&gt;&lt;/b&gt;</description>
<pad name="2" x="2.5" y="0" drill="1.2" diameter="2.5" rot="R180"/>
<pad name="1" x="-2.5" y="0" drill="1.2" diameter="2.5" rot="R180"/>
<wire x1="5" y1="-3.429" x2="3.81" y2="-3.429" width="0.2032" layer="51"/>
<wire x1="3.81" y1="-3.429" x2="1.27" y2="-3.429" width="0.2032" layer="51"/>
<wire x1="1.27" y1="-3.429" x2="-1.27" y2="-3.429" width="0.2032" layer="51"/>
<wire x1="-1.27" y1="-3.429" x2="-3.81" y2="-3.429" width="0.2032" layer="51"/>
<wire x1="-3.81" y1="-3.429" x2="-5" y2="-3.429" width="0.2032" layer="51"/>
<wire x1="-5" y1="-3.429" x2="-5" y2="4.445" width="0.2032" layer="51"/>
<wire x1="-5" y1="4.445" x2="5" y2="4.445" width="0.2032" layer="51"/>
<wire x1="5" y1="4.445" x2="5" y2="-3.429" width="0.2032" layer="51"/>
<wire x1="3.81" y1="-3.429" x2="3.81" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="3.81" y1="-2.54" x2="1.27" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="-3.429" width="0.2032" layer="51"/>
<wire x1="-1.27" y1="-3.429" x2="-1.27" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="-1.27" y1="-2.54" x2="-3.81" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="-3.81" y1="-2.54" x2="-3.81" y2="-3.429" width="0.2032" layer="51"/>
<text x="0" y="2.54" size="0.762" layer="25" font="vector" ratio="15" rot="R180" align="bottom-center">&gt;NAME</text>
<text x="0" y="-4.572" size="0.762" layer="51" font="vector" ratio="15" align="bottom-center">&gt;LABEL</text>
<wire x1="5" y1="-3.429" x2="3.81" y2="-3.429" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-3.429" x2="1.27" y2="-3.429" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-3.429" x2="-1.27" y2="-3.429" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-3.429" x2="-3.81" y2="-3.429" width="0.2032" layer="21"/>
<wire x1="-3.81" y1="-3.429" x2="-5" y2="-3.429" width="0.2032" layer="21"/>
<wire x1="-5" y1="-3.429" x2="-5" y2="4.445" width="0.2032" layer="21"/>
<wire x1="-5" y1="4.445" x2="5" y2="4.445" width="0.2032" layer="21"/>
<wire x1="5" y1="4.445" x2="5" y2="-3.429" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-3.429" x2="3.81" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-2.54" x2="1.27" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="-3.429" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-3.429" x2="-1.27" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="-3.81" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="-3.81" y1="-2.54" x2="-3.81" y2="-3.429" width="0.2032" layer="21"/>
<text x="0" y="-4.572" size="0.762" layer="21" font="vector" ratio="15" align="bottom-center">&gt;LABEL</text>
</package>
<package name="TERMINAL-KF127-2P">
<description>Screw Terminal Block - 2 Pins</description>
<pad name="1" x="-2.54" y="0" drill="1.1" diameter="2.5"/>
<pad name="2" x="2.54" y="0" drill="1.1" diameter="2.5"/>
<wire x1="-5" y1="4" x2="5" y2="4" width="0.2032" layer="21"/>
<wire x1="5" y1="4" x2="5" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="5" y1="-4.2" x2="3.81" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-4.2" x2="1.27" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-4.2" x2="-3.81" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="-3.81" y1="-4.2" x2="-5" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="-3.81" y1="-4.2" x2="-3.81" y2="-3.302" width="0.2032" layer="21"/>
<wire x1="-3.81" y1="-3.302" x2="-1.27" y2="-3.302" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-3.302" x2="-1.27" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-4.2" x2="1.27" y2="-3.302" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-3.302" x2="3.81" y2="-3.302" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-3.302" x2="3.81" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="-5" y1="-4.2" x2="-5" y2="4" width="0.2032" layer="21"/>
<wire x1="-5" y1="-4.2" x2="-5" y2="4" width="0.2032" layer="51"/>
<wire x1="-5" y1="4" x2="5" y2="4" width="0.2032" layer="51"/>
<wire x1="5" y1="4" x2="5" y2="-4.2" width="0.2032" layer="51"/>
<wire x1="5" y1="-4.2" x2="3.81" y2="-4.2" width="0.2032" layer="51"/>
<wire x1="3.81" y1="-4.2" x2="1.27" y2="-4.2" width="0.2032" layer="51"/>
<wire x1="-1.27" y1="-4.2" x2="-3.81" y2="-4.2" width="0.2032" layer="51"/>
<wire x1="-3.81" y1="-4.2" x2="-5" y2="-4.2" width="0.2032" layer="51"/>
<wire x1="-3.81" y1="-4.2" x2="-3.81" y2="-3.302" width="0.2032" layer="51"/>
<wire x1="-3.81" y1="-3.302" x2="-1.27" y2="-3.302" width="0.2032" layer="51"/>
<wire x1="-1.27" y1="-3.302" x2="-1.27" y2="-4.2" width="0.2032" layer="51"/>
<wire x1="1.27" y1="-4.2" x2="1.27" y2="-3.302" width="0.2032" layer="51"/>
<wire x1="1.27" y1="-3.302" x2="3.81" y2="-3.302" width="0.2032" layer="51"/>
<wire x1="3.81" y1="-3.302" x2="3.81" y2="-4.2" width="0.2032" layer="51"/>
<wire x1="1.27" y1="-4.2" x2="-1.27" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-4.2" x2="-1.27" y2="-4.2" width="0.2032" layer="51"/>
<text x="0" y="2.54" size="0.762" layer="25" font="vector" ratio="15" align="top-center">&gt;NAME</text>
<text x="0" y="-5.334" size="0.762" layer="51" font="vector" ratio="15" align="bottom-center">&gt;LABEL</text>
<text x="0" y="-5.334" size="0.762" layer="21" font="vector" ratio="15" align="bottom-center">&gt;LABEL</text>
</package>
<package name="TERMINAL-HB9500-02">
<description>Screw Terminal Block HB9500 - 2 Ways
&lt;br&gt;&lt;br&gt;
Owner: Wai Weng</description>
<pad name="1" x="-4.75" y="0" drill="1.7" diameter="3.048"/>
<pad name="2" x="4.75" y="0" drill="1.7" diameter="3.048"/>
<wire x1="-10.4" y1="4" x2="0" y2="4" width="0.2032" layer="51"/>
<wire x1="0" y1="4" x2="10.4" y2="4" width="0.2032" layer="51"/>
<wire x1="10.4" y1="4" x2="10.4" y2="-12.2" width="0.2032" layer="51"/>
<wire x1="10.4" y1="-12.2" x2="8" y2="-12.2" width="0.2032" layer="51"/>
<wire x1="8" y1="-12.2" x2="2" y2="-12.2" width="0.2032" layer="51"/>
<wire x1="2" y1="-12.2" x2="0" y2="-12.2" width="0.2032" layer="51"/>
<wire x1="0" y1="-12.2" x2="-2" y2="-12.2" width="0.2032" layer="51"/>
<wire x1="-2" y1="-12.2" x2="-8" y2="-12.2" width="0.2032" layer="51"/>
<wire x1="-8" y1="-12.2" x2="-10.4" y2="-12.2" width="0.2032" layer="51"/>
<wire x1="-10.4" y1="-12.2" x2="-10.4" y2="4" width="0.2032" layer="51"/>
<wire x1="0" y1="4" x2="0" y2="-12.2" width="0.2032" layer="51"/>
<wire x1="-8" y1="-12.2" x2="-8" y2="-10" width="0.2032" layer="51"/>
<wire x1="-8" y1="-10" x2="-2" y2="-10" width="0.2032" layer="51"/>
<wire x1="-2" y1="-10" x2="-2" y2="-12.2" width="0.2032" layer="51"/>
<wire x1="2" y1="-12.2" x2="2" y2="-10" width="0.2032" layer="51"/>
<wire x1="2" y1="-10" x2="8" y2="-10" width="0.2032" layer="51"/>
<wire x1="8" y1="-10" x2="8" y2="-12.2" width="0.2032" layer="51"/>
<wire x1="-10.4" y1="4" x2="0" y2="4" width="0.2032" layer="21"/>
<wire x1="0" y1="4" x2="10.4" y2="4" width="0.2032" layer="21"/>
<wire x1="10.4" y1="4" x2="10.4" y2="-12.2" width="0.2032" layer="21"/>
<wire x1="10.4" y1="-12.2" x2="8" y2="-12.2" width="0.2032" layer="21"/>
<wire x1="8" y1="-12.2" x2="2" y2="-12.2" width="0.2032" layer="21"/>
<wire x1="2" y1="-12.2" x2="0" y2="-12.2" width="0.2032" layer="21"/>
<wire x1="0" y1="-12.2" x2="-2" y2="-12.2" width="0.2032" layer="21"/>
<wire x1="-2" y1="-12.2" x2="-8" y2="-12.2" width="0.2032" layer="21"/>
<wire x1="-8" y1="-12.2" x2="-10.4" y2="-12.2" width="0.2032" layer="21"/>
<wire x1="-10.4" y1="-12.2" x2="-10.4" y2="4" width="0.2032" layer="21"/>
<wire x1="0" y1="4" x2="0" y2="-12.2" width="0.2032" layer="21"/>
<wire x1="-8" y1="-12.2" x2="-8" y2="-10" width="0.2032" layer="21"/>
<wire x1="-8" y1="-10" x2="-2" y2="-10" width="0.2032" layer="21"/>
<wire x1="-2" y1="-10" x2="-2" y2="-12.2" width="0.2032" layer="21"/>
<wire x1="2" y1="-12.2" x2="2" y2="-10" width="0.2032" layer="21"/>
<wire x1="2" y1="-10" x2="8" y2="-10" width="0.2032" layer="21"/>
<wire x1="8" y1="-10" x2="8" y2="-12.2" width="0.2032" layer="21"/>
<text x="0" y="5" size="0.762" layer="25" font="vector" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="0" y="-14" size="0.762" layer="21" font="vector" ratio="15" align="bottom-center">&gt;LABEL</text>
<text x="0" y="-14" size="0.762" layer="51" font="vector" ratio="15" align="bottom-center">&gt;LABEL</text>
</package>
<package name="KF139-19.0">
<description>PCB Screw Terminal Block (600V/100A)&lt;br&gt;
Size: 38 x 19.5 mm&lt;br&gt;&lt;br&gt;
Created by: Idris</description>
<wire x1="-19" y1="-9.75" x2="-14.5" y2="-9.75" width="0.2032" layer="21"/>
<wire x1="-14.5" y1="-9.75" x2="-4.5" y2="-9.75" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="-9.75" x2="0" y2="-9.75" width="0.2032" layer="21"/>
<wire x1="0" y1="-9.75" x2="4.5" y2="-9.75" width="0.2032" layer="21"/>
<wire x1="4.5" y1="-9.75" x2="14.5" y2="-9.75" width="0.2032" layer="21"/>
<wire x1="14.5" y1="-9.75" x2="19" y2="-9.75" width="0.2032" layer="21"/>
<wire x1="19" y1="9.75" x2="19" y2="-9.75" width="0.2032" layer="21"/>
<wire x1="-19" y1="9.75" x2="-19" y2="-9.75" width="0.2032" layer="21"/>
<wire x1="-19" y1="9.75" x2="0" y2="9.75" width="0.2032" layer="21"/>
<pad name="2C" x="7.6" y="6.65" drill="1.6" diameter="2.8"/>
<pad name="2D" x="11.4" y="6.65" drill="1.6" diameter="2.8"/>
<pad name="2A" x="7.6" y="-5.85" drill="1.6" diameter="2.8"/>
<pad name="2B" x="11.4" y="-5.85" drill="1.6" diameter="2.8"/>
<wire x1="0" y1="9.75" x2="19" y2="9.75" width="0.2032" layer="21"/>
<wire x1="4.5" y1="-9.75" x2="4.5" y2="-8.5" width="0.2032" layer="21"/>
<wire x1="4.5" y1="-8.5" x2="14.5" y2="-8.5" width="0.2032" layer="21"/>
<wire x1="14.5" y1="-8.5" x2="14.5" y2="-9.75" width="0.2032" layer="21"/>
<pad name="1C" x="-11.4" y="6.65" drill="1.6" diameter="2.8"/>
<pad name="1D" x="-7.6" y="6.65" drill="1.6" diameter="2.8"/>
<pad name="1A" x="-11.4" y="-5.85" drill="1.6" diameter="2.8"/>
<pad name="1B" x="-7.6" y="-5.85" drill="1.6" diameter="2.8"/>
<wire x1="-14.5" y1="-9.75" x2="-14.5" y2="-8.5" width="0.2032" layer="21"/>
<wire x1="-14.5" y1="-8.5" x2="-4.5" y2="-8.5" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="-8.5" x2="-4.5" y2="-9.75" width="0.2032" layer="21"/>
<wire x1="0" y1="9.75" x2="0" y2="-9.75" width="0.2032" layer="21"/>
<wire x1="-19" y1="9.75" x2="-19" y2="-9.75" width="0.2032" layer="51"/>
<wire x1="-19" y1="9.75" x2="19" y2="9.75" width="0.2032" layer="51"/>
<wire x1="19" y1="9.75" x2="19" y2="-9.75" width="0.2032" layer="51"/>
<wire x1="-19" y1="-9.75" x2="-14.5" y2="-9.75" width="0.2032" layer="51"/>
<wire x1="-14.5" y1="-9.75" x2="-4.5" y2="-9.75" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="-9.75" x2="0" y2="-9.75" width="0.2032" layer="51"/>
<wire x1="0" y1="-9.75" x2="4.5" y2="-9.75" width="0.2032" layer="51"/>
<wire x1="4.5" y1="-9.75" x2="14.5" y2="-9.75" width="0.2032" layer="51"/>
<wire x1="14.5" y1="-9.75" x2="19" y2="-9.75" width="0.2032" layer="51"/>
<wire x1="0" y1="9.75" x2="0" y2="-9.75" width="0.2032" layer="51"/>
<wire x1="-14.5" y1="-9.75" x2="-14.5" y2="-8.5" width="0.2032" layer="51"/>
<wire x1="-14.5" y1="-8.5" x2="-4.5" y2="-8.5" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="-8.5" x2="-4.5" y2="-9.75" width="0.2032" layer="51"/>
<wire x1="4.5" y1="-9.75" x2="4.5" y2="-8.5" width="0.2032" layer="51"/>
<wire x1="4.5" y1="-8.5" x2="14.5" y2="-8.5" width="0.2032" layer="51"/>
<wire x1="14.5" y1="-8.5" x2="14.5" y2="-9.75" width="0.2032" layer="51"/>
<text x="-9.5" y="0.5" size="0.762" layer="25" font="vector" ratio="15" align="center">&gt;LABEL</text>
<text x="9.5" y="0.5" size="0.762" layer="27" font="vector" ratio="15" align="center">&gt;VALUE</text>
</package>
<package name="KF136HT-10.16">
<description>PCB Screw Terminal Block (1000V/65A)&lt;br&gt;
Size: 20.32 x 22 mm&lt;br&gt;&lt;br&gt;
Created by: Idris</description>
<wire x1="-10.16" y1="-11" x2="-8.66" y2="-11" width="0.2032" layer="21"/>
<wire x1="-8.66" y1="-11" x2="-1.5" y2="-11" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="-11" x2="0" y2="-11" width="0.2032" layer="21"/>
<wire x1="0" y1="-11" x2="1.5" y2="-11" width="0.2032" layer="21"/>
<wire x1="1.5" y1="-11" x2="8.66" y2="-11" width="0.2032" layer="21"/>
<wire x1="8.66" y1="-11" x2="10.16" y2="-11" width="0.2032" layer="21"/>
<wire x1="10.16" y1="-11" x2="10.16" y2="11" width="0.2032" layer="21"/>
<wire x1="-10.16" y1="-11" x2="-10.16" y2="11" width="0.2032" layer="21"/>
<wire x1="-10.16" y1="11" x2="0" y2="11" width="0.2032" layer="21"/>
<pad name="2B" x="5.08" y="7.8" drill="1.6" diameter="2.8"/>
<wire x1="0" y1="11" x2="10.16" y2="11" width="0.2032" layer="21"/>
<wire x1="0" y1="-11" x2="0" y2="11" width="0.2032" layer="21"/>
<pad name="2A" x="5.08" y="-2.36" drill="1.6" diameter="2.8"/>
<pad name="1B" x="-5.08" y="7.8" drill="1.6" diameter="2.8"/>
<pad name="1A" x="-5.08" y="-2.36" drill="1.6" diameter="2.8"/>
<wire x1="1.5" y1="-11" x2="1.5" y2="-6" width="0.2032" layer="21"/>
<wire x1="1.5" y1="-6" x2="8.66" y2="-6" width="0.2032" layer="21"/>
<wire x1="8.66" y1="-6" x2="8.66" y2="-11" width="0.2032" layer="21"/>
<wire x1="-8.66" y1="-11" x2="-8.66" y2="-6" width="0.2032" layer="21"/>
<wire x1="-8.66" y1="-6" x2="-1.5" y2="-6" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="-6" x2="-1.5" y2="-11" width="0.2032" layer="21"/>
<wire x1="-10.16" y1="-11" x2="-10.16" y2="11" width="0.2032" layer="51"/>
<wire x1="0" y1="-11" x2="0" y2="11" width="0.2032" layer="51"/>
<wire x1="-10.16" y1="11" x2="10.16" y2="11" width="0.2032" layer="51"/>
<wire x1="-10.16" y1="-11" x2="-8.66" y2="-11" width="0.2032" layer="51"/>
<wire x1="-8.66" y1="-11" x2="-1.5" y2="-11" width="0.2032" layer="51"/>
<wire x1="-1.5" y1="-11" x2="0" y2="-11" width="0.2032" layer="51"/>
<wire x1="0" y1="-11" x2="1.5" y2="-11" width="0.2032" layer="51"/>
<wire x1="1.5" y1="-11" x2="8.66" y2="-11" width="0.2032" layer="51"/>
<wire x1="8.66" y1="-11" x2="10.16" y2="-11" width="0.2032" layer="51"/>
<wire x1="-8.66" y1="-11" x2="-8.66" y2="-6" width="0.2032" layer="51"/>
<wire x1="-8.66" y1="-6" x2="-1.5" y2="-6" width="0.2032" layer="51"/>
<wire x1="-1.5" y1="-6" x2="-1.5" y2="-11" width="0.2032" layer="51"/>
<wire x1="1.5" y1="-11" x2="1.5" y2="-6" width="0.2032" layer="51"/>
<wire x1="1.5" y1="-6" x2="8.66" y2="-6" width="0.2032" layer="51"/>
<wire x1="8.66" y1="-6" x2="8.66" y2="-11" width="0.2032" layer="51"/>
<wire x1="10.16" y1="-11" x2="10.16" y2="11" width="0.2032" layer="51"/>
<text x="-5" y="2.5" size="0.762" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<text x="5" y="2.5" size="0.762" layer="27" font="vector" ratio="15" align="center">&gt;VALUE</text>
</package>
<package name="TERMINAL-KF127-2P-MD">
<description>Screw Terminal Block - 2 Pins (For Motor Driver)</description>
<pad name="1" x="-2.54" y="0.508" drill="1.1" diameter="2.5"/>
<pad name="2" x="2.54" y="0.508" drill="1.1" diameter="2.5"/>
<wire x1="-5" y1="4.508" x2="5" y2="4.508" width="0.2032" layer="21"/>
<wire x1="5" y1="4.508" x2="5" y2="-1.992" width="0.2032" layer="21"/>
<wire x1="-5" y1="-1.992" x2="-5" y2="4.508" width="0.2032" layer="21"/>
<wire x1="-5" y1="-3.692" x2="-5" y2="4.508" width="0.2032" layer="51"/>
<wire x1="-5" y1="4.508" x2="5" y2="4.508" width="0.2032" layer="51"/>
<wire x1="5" y1="4.508" x2="5" y2="-3.692" width="0.2032" layer="51"/>
<wire x1="5" y1="-3.692" x2="3.81" y2="-3.692" width="0.2032" layer="51"/>
<wire x1="3.81" y1="-3.692" x2="1.27" y2="-3.692" width="0.2032" layer="51"/>
<wire x1="-1.27" y1="-3.692" x2="-3.81" y2="-3.692" width="0.2032" layer="51"/>
<wire x1="-3.81" y1="-3.692" x2="-5" y2="-3.692" width="0.2032" layer="51"/>
<wire x1="-3.81" y1="-3.692" x2="-3.81" y2="-2.794" width="0.2032" layer="51"/>
<wire x1="-3.81" y1="-2.794" x2="-1.27" y2="-2.794" width="0.2032" layer="51"/>
<wire x1="-1.27" y1="-2.794" x2="-1.27" y2="-3.692" width="0.2032" layer="51"/>
<wire x1="1.27" y1="-3.692" x2="1.27" y2="-2.794" width="0.2032" layer="51"/>
<wire x1="1.27" y1="-2.794" x2="3.81" y2="-2.794" width="0.2032" layer="51"/>
<wire x1="3.81" y1="-2.794" x2="3.81" y2="-3.692" width="0.2032" layer="51"/>
<wire x1="1.27" y1="-3.692" x2="-1.27" y2="-3.692" width="0.2032" layer="51"/>
<text x="0" y="3.048" size="0.762" layer="25" font="vector" ratio="15" align="top-center">&gt;NAME</text>
<text x="0" y="4.826" size="0.762" layer="51" font="vector" ratio="15" align="bottom-center">&gt;LABEL</text>
<text x="0" y="4.826" size="0.762" layer="21" font="vector" ratio="15" align="bottom-center">&gt;LABEL</text>
<pad name="1-1" x="-3.81" y="-2.54" drill="1.1" diameter="1.9"/>
<pad name="1-2" x="-1.27" y="-2.54" drill="1.1" diameter="1.9"/>
<pad name="2-1" x="1.27" y="-2.54" drill="1.1" diameter="1.9"/>
<pad name="2-2" x="3.81" y="-2.54" drill="1.1" diameter="1.9"/>
<polygon width="0.254" layer="1">
<vertex x="-0.381" y="-4.826"/>
<vertex x="-4.699" y="-4.826"/>
<vertex x="-4.699" y="-2.54" curve="-90"/>
<vertex x="-3.683" y="-1.651"/>
<vertex x="-3.683" y="0.508" curve="-90"/>
<vertex x="-2.54" y="1.651" curve="-90"/>
<vertex x="-1.397" y="0.508"/>
<vertex x="-1.397" y="-1.651" curve="-90"/>
<vertex x="-0.381" y="-2.54"/>
</polygon>
<polygon width="0.254" layer="1">
<vertex x="4.699" y="-4.826"/>
<vertex x="0.381" y="-4.826"/>
<vertex x="0.381" y="-2.54" curve="-90"/>
<vertex x="1.397" y="-1.651"/>
<vertex x="1.397" y="0.508" curve="-90"/>
<vertex x="2.54" y="1.651" curve="-90"/>
<vertex x="3.683" y="0.508"/>
<vertex x="3.683" y="-1.651" curve="-90"/>
<vertex x="4.699" y="-2.54"/>
</polygon>
<polygon width="0.254" layer="16">
<vertex x="-0.381" y="-4.826"/>
<vertex x="-4.699" y="-4.826"/>
<vertex x="-4.699" y="-2.54" curve="-90"/>
<vertex x="-3.683" y="-1.651"/>
<vertex x="-3.683" y="0.508" curve="-90"/>
<vertex x="-2.54" y="1.651" curve="-90"/>
<vertex x="-1.397" y="0.508"/>
<vertex x="-1.397" y="-1.651" curve="-90"/>
<vertex x="-0.381" y="-2.54"/>
</polygon>
<polygon width="0.254" layer="16">
<vertex x="4.699" y="-4.826"/>
<vertex x="0.381" y="-4.826"/>
<vertex x="0.381" y="-2.54" curve="-90"/>
<vertex x="1.397" y="-1.651"/>
<vertex x="1.397" y="0.508" curve="-90"/>
<vertex x="2.54" y="1.651" curve="-90"/>
<vertex x="3.683" y="0.508"/>
<vertex x="3.683" y="-1.651" curve="-90"/>
<vertex x="4.699" y="-2.54"/>
</polygon>
<polygon width="0.3302" layer="29">
<vertex x="-0.381" y="-4.826"/>
<vertex x="-4.699" y="-4.826"/>
<vertex x="-4.699" y="-2.54" curve="-90"/>
<vertex x="-3.683" y="-1.651"/>
<vertex x="-3.683" y="0.508" curve="-90"/>
<vertex x="-2.54" y="1.651" curve="-90"/>
<vertex x="-1.397" y="0.508"/>
<vertex x="-1.397" y="-1.651" curve="-90"/>
<vertex x="-0.381" y="-2.54"/>
</polygon>
<polygon width="0.3302" layer="29">
<vertex x="4.699" y="-4.826"/>
<vertex x="0.381" y="-4.826"/>
<vertex x="0.381" y="-2.54" curve="-90"/>
<vertex x="1.397" y="-1.651"/>
<vertex x="1.397" y="0.508" curve="-90"/>
<vertex x="2.54" y="1.651" curve="-90"/>
<vertex x="3.683" y="0.508"/>
<vertex x="3.683" y="-1.651" curve="-90"/>
<vertex x="4.699" y="-2.54"/>
</polygon>
<polygon width="0.3302" layer="30">
<vertex x="4.699" y="-4.826"/>
<vertex x="0.381" y="-4.826"/>
<vertex x="0.381" y="-2.54" curve="-90"/>
<vertex x="1.397" y="-1.651"/>
<vertex x="1.397" y="0.508" curve="-90"/>
<vertex x="2.54" y="1.651" curve="-90"/>
<vertex x="3.683" y="0.508"/>
<vertex x="3.683" y="-1.651" curve="-90"/>
<vertex x="4.699" y="-2.54"/>
</polygon>
<polygon width="0.3302" layer="30">
<vertex x="-0.381" y="-4.826"/>
<vertex x="-4.699" y="-4.826"/>
<vertex x="-4.699" y="-2.54" curve="-90"/>
<vertex x="-3.683" y="-1.651"/>
<vertex x="-3.683" y="0.508" curve="-90"/>
<vertex x="-2.54" y="1.651" curve="-90"/>
<vertex x="-1.397" y="0.508"/>
<vertex x="-1.397" y="-1.651" curve="-90"/>
<vertex x="-0.381" y="-2.54"/>
</polygon>
</package>
<package name="TERMINAL-KF127-2P-MD-HALFHOLE">
<description>Screw Terminal Block - 2 Pins (For Motor Driver, With plated half hole)</description>
<pad name="1" x="-2.54" y="0.508" drill="1.1" diameter="2.5"/>
<pad name="2" x="2.54" y="0.508" drill="1.1" diameter="2.5"/>
<wire x1="-5" y1="4.508" x2="5" y2="4.508" width="0.2032" layer="21"/>
<wire x1="5" y1="4.508" x2="5" y2="-1.992" width="0.2032" layer="21"/>
<wire x1="-5" y1="-1.992" x2="-5" y2="4.508" width="0.2032" layer="21"/>
<wire x1="-5" y1="-3.692" x2="-5" y2="4.508" width="0.2032" layer="51"/>
<wire x1="-5" y1="4.508" x2="5" y2="4.508" width="0.2032" layer="51"/>
<wire x1="5" y1="4.508" x2="5" y2="-3.692" width="0.2032" layer="51"/>
<wire x1="5" y1="-3.692" x2="3.81" y2="-3.692" width="0.2032" layer="51"/>
<wire x1="3.81" y1="-3.692" x2="1.27" y2="-3.692" width="0.2032" layer="51"/>
<wire x1="-1.27" y1="-3.692" x2="-3.81" y2="-3.692" width="0.2032" layer="51"/>
<wire x1="-3.81" y1="-3.692" x2="-5" y2="-3.692" width="0.2032" layer="51"/>
<wire x1="-3.81" y1="-3.692" x2="-3.81" y2="-2.794" width="0.2032" layer="51"/>
<wire x1="-3.81" y1="-2.794" x2="-1.27" y2="-2.794" width="0.2032" layer="51"/>
<wire x1="-1.27" y1="-2.794" x2="-1.27" y2="-3.692" width="0.2032" layer="51"/>
<wire x1="1.27" y1="-3.692" x2="1.27" y2="-2.794" width="0.2032" layer="51"/>
<wire x1="1.27" y1="-2.794" x2="3.81" y2="-2.794" width="0.2032" layer="51"/>
<wire x1="3.81" y1="-2.794" x2="3.81" y2="-3.692" width="0.2032" layer="51"/>
<wire x1="1.27" y1="-3.692" x2="-1.27" y2="-3.692" width="0.2032" layer="51"/>
<text x="0" y="3.048" size="0.762" layer="25" font="vector" ratio="15" align="top-center">&gt;NAME</text>
<text x="0" y="4.826" size="0.762" layer="51" font="vector" ratio="15" align="bottom-center">&gt;LABEL</text>
<text x="0" y="4.826" size="0.762" layer="21" font="vector" ratio="15" align="bottom-center">&gt;LABEL</text>
<pad name="1-1" x="-3.81" y="-2.54" drill="1.1" diameter="1.9"/>
<pad name="1-2" x="-1.27" y="-2.54" drill="1.1" diameter="1.9"/>
<pad name="1-3" x="-3.81" y="-5.08" drill="1.4" diameter="2" shape="square"/>
<pad name="1-4" x="-1.27" y="-5.08" drill="1.4" diameter="2" shape="square"/>
<pad name="2-1" x="1.27" y="-2.54" drill="1.1" diameter="1.9"/>
<pad name="2-2" x="3.81" y="-2.54" drill="1.1" diameter="1.9"/>
<pad name="2-3" x="1.27" y="-5.08" drill="1.4" diameter="2" shape="square"/>
<pad name="2-4" x="3.81" y="-5.08" drill="1.4" diameter="2" shape="square"/>
<polygon width="0.254" layer="1">
<vertex x="-0.381" y="-5.969"/>
<vertex x="-4.699" y="-5.969"/>
<vertex x="-4.699" y="-2.54" curve="-90"/>
<vertex x="-3.683" y="-1.651"/>
<vertex x="-3.683" y="0.508" curve="-90"/>
<vertex x="-2.54" y="1.651" curve="-90"/>
<vertex x="-1.397" y="0.508"/>
<vertex x="-1.397" y="-1.651" curve="-90"/>
<vertex x="-0.381" y="-2.54"/>
</polygon>
<polygon width="0.254" layer="1">
<vertex x="4.699" y="-5.969"/>
<vertex x="0.381" y="-5.969"/>
<vertex x="0.381" y="-2.54" curve="-90"/>
<vertex x="1.397" y="-1.651"/>
<vertex x="1.397" y="0.508" curve="-90"/>
<vertex x="2.54" y="1.651" curve="-90"/>
<vertex x="3.683" y="0.508"/>
<vertex x="3.683" y="-1.651" curve="-90"/>
<vertex x="4.699" y="-2.54"/>
</polygon>
<polygon width="0.254" layer="16">
<vertex x="-0.381" y="-5.969"/>
<vertex x="-4.699" y="-5.969"/>
<vertex x="-4.699" y="-2.54" curve="-90"/>
<vertex x="-3.683" y="-1.651"/>
<vertex x="-3.683" y="0.508" curve="-90"/>
<vertex x="-2.54" y="1.651" curve="-90"/>
<vertex x="-1.397" y="0.508"/>
<vertex x="-1.397" y="-1.651" curve="-90"/>
<vertex x="-0.381" y="-2.54"/>
</polygon>
<polygon width="0.254" layer="16">
<vertex x="4.699" y="-5.969"/>
<vertex x="0.381" y="-5.969"/>
<vertex x="0.381" y="-2.54" curve="-90"/>
<vertex x="1.397" y="-1.651"/>
<vertex x="1.397" y="0.508" curve="-90"/>
<vertex x="2.54" y="1.651" curve="-90"/>
<vertex x="3.683" y="0.508"/>
<vertex x="3.683" y="-1.651" curve="-90"/>
<vertex x="4.699" y="-2.54"/>
</polygon>
<polygon width="0.3302" layer="29">
<vertex x="-0.381" y="-5.969"/>
<vertex x="-4.699" y="-5.969"/>
<vertex x="-4.699" y="-2.54" curve="-90"/>
<vertex x="-3.683" y="-1.651"/>
<vertex x="-3.683" y="0.508" curve="-90"/>
<vertex x="-2.54" y="1.651" curve="-90"/>
<vertex x="-1.397" y="0.508"/>
<vertex x="-1.397" y="-1.651" curve="-90"/>
<vertex x="-0.381" y="-2.54"/>
</polygon>
<polygon width="0.3302" layer="29">
<vertex x="4.699" y="-5.969"/>
<vertex x="0.381" y="-5.969"/>
<vertex x="0.381" y="-2.54" curve="-90"/>
<vertex x="1.397" y="-1.651"/>
<vertex x="1.397" y="0.508" curve="-90"/>
<vertex x="2.54" y="1.651" curve="-90"/>
<vertex x="3.683" y="0.508"/>
<vertex x="3.683" y="-1.651" curve="-90"/>
<vertex x="4.699" y="-2.54"/>
</polygon>
<polygon width="0.3302" layer="30">
<vertex x="4.699" y="-5.969"/>
<vertex x="0.381" y="-5.969"/>
<vertex x="0.381" y="-2.54" curve="-90"/>
<vertex x="1.397" y="-1.651"/>
<vertex x="1.397" y="0.508" curve="-90"/>
<vertex x="2.54" y="1.651" curve="-90"/>
<vertex x="3.683" y="0.508"/>
<vertex x="3.683" y="-1.651" curve="-90"/>
<vertex x="4.699" y="-2.54"/>
</polygon>
<polygon width="0.3302" layer="30">
<vertex x="-0.381" y="-5.969"/>
<vertex x="-4.699" y="-5.969"/>
<vertex x="-4.699" y="-2.54" curve="-90"/>
<vertex x="-3.683" y="-1.651"/>
<vertex x="-3.683" y="0.508" curve="-90"/>
<vertex x="-2.54" y="1.651" curve="-90"/>
<vertex x="-1.397" y="0.508"/>
<vertex x="-1.397" y="-1.651" curve="-90"/>
<vertex x="-0.381" y="-2.54"/>
</polygon>
</package>
<package name="SPTAF_1/2-5.0">
<description>Phoenix Contact SPTAF Terminal Block - 2 Ways</description>
<wire x1="-5" y1="-5.5" x2="-3.5" y2="-5.5" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="-5.5" x2="-1.5" y2="-5.5" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="-5.5" x2="1.5" y2="-5.5" width="0.2032" layer="21"/>
<wire x1="1.5" y1="-5.5" x2="3.5" y2="-5.5" width="0.2032" layer="21"/>
<wire x1="3.5" y1="-5.5" x2="5" y2="-5.5" width="0.2032" layer="21"/>
<wire x1="5" y1="5.5" x2="-5" y2="5.5" width="0.2032" layer="21"/>
<wire x1="-5" y1="5.5" x2="-5" y2="-5.5" width="0.2032" layer="21"/>
<pad name="2" x="2.5" y="2.5" drill="0.8" diameter="1.778"/>
<pad name="1" x="-2.5" y="2.5" drill="0.8" diameter="1.778"/>
<pad name="3" x="2.5" y="-2.5" drill="0.8" diameter="1.778"/>
<pad name="4" x="-2.5" y="-2.5" drill="0.8" diameter="1.778"/>
<wire x1="-5" y1="5.5" x2="-5" y2="-5.5" width="0.2032" layer="51"/>
<wire x1="-5" y1="-5.5" x2="-3.5" y2="-5.5" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="-5.5" x2="-1.5" y2="-5.5" width="0.2032" layer="51"/>
<wire x1="-1.5" y1="-5.5" x2="1.5" y2="-5.5" width="0.2032" layer="51"/>
<wire x1="1.5" y1="-5.5" x2="3.5" y2="-5.5" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="-5.5" x2="-3.5" y2="-4.5" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="-4.5" x2="-1.5" y2="-4.5" width="0.2032" layer="51"/>
<wire x1="-1.5" y1="-4.5" x2="-1.5" y2="-5.5" width="0.2032" layer="51"/>
<wire x1="1.5" y1="-5.5" x2="1.5" y2="-4.5" width="0.2032" layer="51"/>
<wire x1="1.5" y1="-4.5" x2="3.5" y2="-4.5" width="0.2032" layer="51"/>
<wire x1="3.5" y1="-4.5" x2="3.5" y2="-5.5" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="-5.5" x2="-3.5" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-3.5" y1="-4.5" x2="-1.5" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="-4.5" x2="-1.5" y2="-5.5" width="0.2032" layer="21"/>
<wire x1="1.5" y1="-5.5" x2="1.5" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="1.5" y1="-4.5" x2="3.5" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="3.5" y1="-4.5" x2="3.5" y2="-5.5" width="0.2032" layer="21"/>
<wire x1="5" y1="5.5" x2="5" y2="-5.5" width="0.2032" layer="21"/>
<wire x1="-5" y1="5.5" x2="5" y2="5.5" width="0.2032" layer="51"/>
<wire x1="5" y1="5.5" x2="5" y2="-5.5" width="0.2032" layer="51"/>
<wire x1="5" y1="-5.5" x2="3.5" y2="-5.5" width="0.2032" layer="51"/>
<text x="0" y="0" size="0.762" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<text x="0" y="-6.35" size="0.762" layer="21" font="vector" ratio="15" align="center">&gt;LABEL</text>
</package>
<package name="TERMINAL-2060-2P">
<description>SMD Spring Terminal Block 2060 - 2 Ways&lt;br&gt;
&lt;br&gt;
Created by: Wai Weng</description>
<smd name="F2" x="-4" y="-2" dx="6" dy="2" layer="1" roundness="30"/>
<smd name="R2" x="5.25" y="-2" dx="3.5" dy="2" layer="1" roundness="30"/>
<smd name="F1" x="-4" y="2" dx="6" dy="2" layer="1" roundness="30"/>
<smd name="R1" x="5.25" y="2" dx="3.5" dy="2" layer="1" roundness="30"/>
<wire x1="-6.5" y1="3.95" x2="6.2" y2="3.95" width="0.2032" layer="51"/>
<wire x1="6.2" y1="3.95" x2="6.2" y2="-3.95" width="0.2032" layer="51"/>
<wire x1="6.2" y1="-3.95" x2="-6.5" y2="-3.95" width="0.2032" layer="51"/>
<wire x1="-6.5" y1="-3.95" x2="-6.5" y2="3.95" width="0.2032" layer="51"/>
<rectangle x1="-6.5" y1="1.4" x2="-2.5" y2="2.6" layer="51"/>
<rectangle x1="-6.5" y1="-2.6" x2="-2.5" y2="-1.4" layer="51"/>
<rectangle x1="4.5" y1="1.4" x2="6.6" y2="2.6" layer="51"/>
<rectangle x1="4.5" y1="-2.6" x2="6.6" y2="-1.4" layer="51"/>
<wire x1="-6.1" y1="3.3" x2="-6.1" y2="3.6" width="0.2032" layer="21"/>
<wire x1="-6.1" y1="3.6" x2="4.6" y2="3.6" width="0.2032" layer="21"/>
<wire x1="4.6" y1="3.6" x2="4.6" y2="3.3" width="0.2032" layer="21"/>
<wire x1="-6.1" y1="-3.3" x2="-6.1" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-6.1" y1="-3.6" x2="4.6" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="4.6" y1="-3.6" x2="4.6" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="-6.1" y1="0.7" x2="-6.1" y2="0.4" width="0.2032" layer="21"/>
<wire x1="-6.1" y1="0.4" x2="-6.1" y2="-0.4" width="0.2032" layer="21"/>
<wire x1="-6.1" y1="-0.4" x2="-6.1" y2="-0.7" width="0.2032" layer="21"/>
<wire x1="4.6" y1="0.7" x2="4.6" y2="-0.7" width="0.2032" layer="21"/>
<wire x1="-6.1" y1="0.4" x2="-5.3" y2="0.4" width="0.2032" layer="21"/>
<wire x1="-5.3" y1="0.4" x2="-5.3" y2="-0.4" width="0.2032" layer="21"/>
<wire x1="-5.3" y1="-0.4" x2="-6.1" y2="-0.4" width="0.2032" layer="21"/>
<text x="0" y="0" size="0.762" layer="25" font="vector" ratio="15" rot="R90" align="center">&gt;NAME</text>
<text x="-8.0518" y="0" size="0.762" layer="21" font="vector" ratio="15" rot="SR270" align="center">&gt;LABEL</text>
</package>
<package name="TERMINAL-DG308-2P">
<pad name="1" x="-1.27" y="0" drill="0.9" diameter="1.778"/>
<pad name="2" x="1.27" y="0" drill="0.9" diameter="1.778"/>
<wire x1="-2.8" y1="2.6" x2="2.8" y2="2.6" width="0.2032" layer="21"/>
<wire x1="2.8" y1="2.6" x2="2.8" y2="-3.8" width="0.2032" layer="21"/>
<wire x1="2.8" y1="-3.8" x2="2.11" y2="-3.8" width="0.2032" layer="21"/>
<wire x1="2.11" y1="-3.8" x2="0.47" y2="-3.8" width="0.2032" layer="21"/>
<wire x1="-0.47" y1="-3.8" x2="-2.11" y2="-3.8" width="0.2032" layer="21"/>
<wire x1="-2.11" y1="-3.8" x2="-2.8" y2="-3.8" width="0.2032" layer="21"/>
<wire x1="-2.11" y1="-3.8" x2="-2.11" y2="-2.902" width="0.2032" layer="21"/>
<wire x1="-2.11" y1="-2.902" x2="-0.47" y2="-2.902" width="0.2032" layer="21"/>
<wire x1="-0.47" y1="-2.902" x2="-0.47" y2="-3.8" width="0.2032" layer="21"/>
<wire x1="0.47" y1="-3.8" x2="0.47" y2="-2.902" width="0.2032" layer="21"/>
<wire x1="0.47" y1="-2.902" x2="2.11" y2="-2.902" width="0.2032" layer="21"/>
<wire x1="2.11" y1="-2.902" x2="2.11" y2="-3.8" width="0.2032" layer="21"/>
<wire x1="-2.8" y1="-3.8" x2="-2.8" y2="2.6" width="0.2032" layer="21"/>
<wire x1="-2.8" y1="-3.8" x2="-2.8" y2="2.6" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="2.6" x2="2.8" y2="2.6" width="0.2032" layer="51"/>
<wire x1="2.8" y1="2.6" x2="2.8" y2="-3.8" width="0.2032" layer="51"/>
<wire x1="2.8" y1="-3.8" x2="2.11" y2="-3.8" width="0.2032" layer="51"/>
<wire x1="2.11" y1="-3.8" x2="0.47" y2="-3.8" width="0.2032" layer="51"/>
<wire x1="-0.47" y1="-3.8" x2="-2.11" y2="-3.8" width="0.2032" layer="51"/>
<wire x1="-2.11" y1="-3.8" x2="-2.8" y2="-3.8" width="0.2032" layer="51"/>
<wire x1="-2.11" y1="-3.8" x2="-2.11" y2="-2.902" width="0.2032" layer="51"/>
<wire x1="-2.11" y1="-2.902" x2="-0.47" y2="-2.902" width="0.2032" layer="51"/>
<wire x1="-0.47" y1="-2.902" x2="-0.47" y2="-3.8" width="0.2032" layer="51"/>
<wire x1="0.47" y1="-3.8" x2="0.47" y2="-2.902" width="0.2032" layer="51"/>
<wire x1="0.47" y1="-2.902" x2="2.11" y2="-2.902" width="0.2032" layer="51"/>
<wire x1="2.11" y1="-2.902" x2="2.11" y2="-3.8" width="0.2032" layer="51"/>
<wire x1="0.47" y1="-3.8" x2="-0.47" y2="-3.8" width="0.2032" layer="21"/>
<wire x1="0.47" y1="-3.8" x2="-0.47" y2="-3.8" width="0.2032" layer="51"/>
<text x="0" y="2.032" size="0.762" layer="25" font="vector" ratio="15" align="top-center">&gt;NAME</text>
<text x="0" y="-5.08" size="0.762" layer="21" font="vector" ratio="15" align="bottom-center">&gt;LABEL</text>
</package>
<package name="CN-CE16R-80">
<description>Microbit connector&lt;br&gt;
CE16R-80&lt;br&gt;
&lt;br&gt;
Created by: Idris</description>
<pad name="1" x="-24.765" y="0" drill="0.889"/>
<pad name="3" x="-22.225" y="0" drill="0.889"/>
<pad name="5" x="-19.685" y="0" drill="0.889"/>
<pad name="7" x="-17.145" y="0" drill="0.889"/>
<pad name="9" x="-14.605" y="0" drill="0.889"/>
<pad name="11" x="-12.065" y="0" drill="0.889"/>
<pad name="13" x="-9.525" y="0" drill="0.889"/>
<pad name="15" x="-6.985" y="0" drill="0.889"/>
<pad name="17" x="-4.445" y="0" drill="0.889"/>
<pad name="19" x="-1.905" y="0" drill="0.889"/>
<pad name="21" x="0.635" y="0" drill="0.889"/>
<pad name="23" x="3.175" y="0" drill="0.889"/>
<pad name="25" x="5.715" y="0" drill="0.889"/>
<pad name="27" x="8.255" y="0" drill="0.889"/>
<pad name="29" x="10.795" y="0" drill="0.889"/>
<pad name="31" x="13.335" y="0" drill="0.889"/>
<pad name="33" x="15.875" y="0" drill="0.889"/>
<pad name="35" x="18.415" y="0" drill="0.889"/>
<pad name="37" x="20.955" y="0" drill="0.889"/>
<pad name="39" x="23.495" y="0" drill="0.889"/>
<pad name="2" x="-23.495" y="-1.9" drill="0.889"/>
<pad name="4" x="-20.955" y="-1.9" drill="0.889"/>
<pad name="6" x="-18.415" y="-1.9" drill="0.889"/>
<pad name="8" x="-15.875" y="-1.9" drill="0.889"/>
<pad name="10" x="-13.335" y="-1.9" drill="0.889"/>
<pad name="12" x="-10.795" y="-1.9" drill="0.889"/>
<pad name="14" x="-8.255" y="-1.9" drill="0.889"/>
<pad name="16" x="-5.715" y="-1.9" drill="0.889"/>
<pad name="18" x="-3.175" y="-1.9" drill="0.889"/>
<pad name="20" x="-0.635" y="-1.9" drill="0.889"/>
<pad name="22" x="1.905" y="-1.9" drill="0.889"/>
<pad name="24" x="4.445" y="-1.9" drill="0.889"/>
<pad name="26" x="6.985" y="-1.9" drill="0.889"/>
<pad name="28" x="9.525" y="-1.9" drill="0.889"/>
<pad name="30" x="12.065" y="-1.9" drill="0.889"/>
<pad name="32" x="14.605" y="-1.9" drill="0.889"/>
<pad name="34" x="17.145" y="-1.9" drill="0.889"/>
<pad name="36" x="19.685" y="-1.9" drill="0.889"/>
<pad name="38" x="22.225" y="-1.9" drill="0.889"/>
<pad name="40" x="24.765" y="-1.9" drill="0.889"/>
<pad name="P$41" x="-24.765" y="-3.8" drill="0.889"/>
<pad name="P$42" x="-22.225" y="-3.8" drill="0.889"/>
<pad name="P$43" x="-19.685" y="-3.8" drill="0.889"/>
<pad name="P$44" x="-17.145" y="-3.8" drill="0.889"/>
<pad name="P$45" x="-14.605" y="-3.8" drill="0.889"/>
<pad name="P$46" x="-12.065" y="-3.8" drill="0.889"/>
<pad name="P$47" x="-9.525" y="-3.8" drill="0.889"/>
<pad name="P$48" x="-6.985" y="-3.8" drill="0.889"/>
<pad name="P$49" x="-4.445" y="-3.8" drill="0.889"/>
<pad name="P$50" x="-1.905" y="-3.8" drill="0.889"/>
<pad name="P$51" x="0.635" y="-3.8" drill="0.889"/>
<pad name="P$52" x="3.175" y="-3.8" drill="0.889"/>
<pad name="P$53" x="5.715" y="-3.8" drill="0.889"/>
<pad name="P$54" x="8.255" y="-3.8" drill="0.889"/>
<pad name="P$55" x="10.795" y="-3.8" drill="0.889"/>
<pad name="P$56" x="13.335" y="-3.8" drill="0.889"/>
<pad name="P$57" x="15.875" y="-3.8" drill="0.889"/>
<pad name="P$58" x="18.415" y="-3.8" drill="0.889"/>
<pad name="P$59" x="20.955" y="-3.8" drill="0.889"/>
<pad name="P$60" x="23.495" y="-3.8" drill="0.889"/>
<pad name="P$61" x="-23.495" y="-5.7" drill="0.889"/>
<pad name="P$62" x="-20.955" y="-5.7" drill="0.889"/>
<pad name="P$63" x="-18.415" y="-5.7" drill="0.889"/>
<pad name="P$64" x="-15.875" y="-5.7" drill="0.889"/>
<pad name="P$65" x="-13.335" y="-5.7" drill="0.889"/>
<pad name="P$66" x="-10.795" y="-5.7" drill="0.889"/>
<pad name="P$67" x="-8.255" y="-5.7" drill="0.889"/>
<pad name="P$68" x="-5.715" y="-5.7" drill="0.889"/>
<pad name="P$69" x="-3.175" y="-5.7" drill="0.889"/>
<pad name="P$70" x="-0.635" y="-5.7" drill="0.889"/>
<pad name="P$71" x="1.905" y="-5.7" drill="0.889"/>
<pad name="P$72" x="4.445" y="-5.7" drill="0.889"/>
<pad name="P$73" x="6.985" y="-5.7" drill="0.889"/>
<pad name="P$74" x="9.525" y="-5.7" drill="0.889"/>
<pad name="P$75" x="12.065" y="-5.7" drill="0.889"/>
<pad name="P$76" x="14.605" y="-5.7" drill="0.889"/>
<pad name="P$77" x="17.145" y="-5.7" drill="0.889"/>
<pad name="P$78" x="19.685" y="-5.7" drill="0.889"/>
<pad name="P$79" x="22.225" y="-5.7" drill="0.889"/>
<pad name="P$80" x="24.765" y="-5.7" drill="0.889"/>
<wire x1="-28.45" y1="0" x2="-28.45" y2="14" width="0.2032" layer="51"/>
<wire x1="-28.45" y1="0" x2="28.45" y2="0" width="0.2032" layer="51"/>
<wire x1="28.45" y1="0" x2="28.45" y2="14" width="0.2032" layer="51"/>
<wire x1="-28.45" y1="14" x2="28.45" y2="14" width="0.2032" layer="51"/>
<wire x1="-28.45" y1="-7.37" x2="-28.45" y2="0" width="0.2032" layer="51"/>
<wire x1="28.45" y1="-7.37" x2="28.45" y2="0" width="0.2032" layer="51"/>
<wire x1="-28.45" y1="-7.37" x2="28.45" y2="-7.37" width="0.2032" layer="51"/>
<text x="-1.27" y="6.35" size="0.762" layer="25" font="vector" ratio="15">&gt;NAME</text>
</package>
<package name="S-CN-MICROBIT-RA">
<description>Microbit SMD connector&lt;br&gt;
&lt;br&gt;
Part No: PCIRSM127-40SG-LK&lt;br&gt;
Dimension: 56.6 mm x 11.5 mm&lt;br&gt;
&lt;br&gt;
Created by: Idris</description>
<hole x="-12.5" y="0" drill="1.8"/>
<hole x="12.5" y="0" drill="1.8"/>
<smd name="20" x="-0.635" y="-6.15" dx="0.9" dy="3" layer="1" roundness="50"/>
<smd name="21" x="0.635" y="-6.15" dx="0.9" dy="3" layer="1" roundness="50"/>
<smd name="22" x="1.905" y="-6.15" dx="0.9" dy="3" layer="1" roundness="50"/>
<smd name="23" x="3.175" y="-6.15" dx="0.9" dy="3" layer="1" roundness="50"/>
<smd name="24" x="4.445" y="-6.15" dx="0.9" dy="3" layer="1" roundness="50"/>
<smd name="25" x="5.715" y="-6.15" dx="0.9" dy="3" layer="1" roundness="50"/>
<smd name="26" x="6.985" y="-6.15" dx="0.9" dy="3" layer="1" roundness="50"/>
<smd name="27" x="8.255" y="-6.15" dx="0.9" dy="3" layer="1" roundness="50"/>
<smd name="28" x="9.525" y="-6.15" dx="0.9" dy="3" layer="1" roundness="50"/>
<smd name="29" x="10.795" y="-6.15" dx="0.9" dy="3" layer="1" roundness="50"/>
<smd name="30" x="12.065" y="-6.15" dx="0.9" dy="3" layer="1" roundness="50"/>
<smd name="31" x="13.335" y="-6.15" dx="0.9" dy="3" layer="1" roundness="50"/>
<smd name="32" x="14.605" y="-6.15" dx="0.9" dy="3" layer="1" roundness="50"/>
<smd name="33" x="15.875" y="-6.15" dx="0.9" dy="3" layer="1" roundness="50"/>
<smd name="34" x="17.145" y="-6.15" dx="0.9" dy="3" layer="1" roundness="50"/>
<smd name="35" x="18.415" y="-6.15" dx="0.9" dy="3" layer="1" roundness="50"/>
<smd name="36" x="19.685" y="-6.15" dx="0.9" dy="3" layer="1" roundness="50"/>
<smd name="37" x="20.955" y="-6.15" dx="0.9" dy="3" layer="1" roundness="50"/>
<smd name="38" x="22.225" y="-6.15" dx="0.9" dy="3" layer="1" roundness="50"/>
<smd name="39" x="23.495" y="-6.15" dx="0.9" dy="3" layer="1" roundness="50"/>
<smd name="40" x="24.765" y="-6.15" dx="0.9" dy="3" layer="1" roundness="50"/>
<smd name="19" x="-1.905" y="-6.15" dx="0.9" dy="3" layer="1" roundness="50"/>
<smd name="18" x="-3.175" y="-6.15" dx="0.9" dy="3" layer="1" roundness="50"/>
<smd name="17" x="-4.445" y="-6.15" dx="0.9" dy="3" layer="1" roundness="50"/>
<smd name="16" x="-5.715" y="-6.15" dx="0.9" dy="3" layer="1" roundness="50"/>
<smd name="15" x="-6.985" y="-6.15" dx="0.9" dy="3" layer="1" roundness="50"/>
<smd name="14" x="-8.255" y="-6.15" dx="0.9" dy="3" layer="1" roundness="50"/>
<smd name="13" x="-9.525" y="-6.15" dx="0.9" dy="3" layer="1" roundness="50"/>
<smd name="12" x="-10.795" y="-6.15" dx="0.9" dy="3" layer="1" roundness="50"/>
<smd name="11" x="-12.065" y="-6.15" dx="0.9" dy="3" layer="1" roundness="50"/>
<smd name="10" x="-13.335" y="-6.15" dx="0.9" dy="3" layer="1" roundness="50"/>
<smd name="9" x="-14.605" y="-6.15" dx="0.9" dy="3" layer="1" roundness="50"/>
<smd name="8" x="-15.875" y="-6.15" dx="0.9" dy="3" layer="1" roundness="50"/>
<smd name="7" x="-17.145" y="-6.15" dx="0.9" dy="3" layer="1" roundness="50"/>
<smd name="6" x="-18.415" y="-6.15" dx="0.9" dy="3" layer="1" roundness="50"/>
<smd name="5" x="-19.685" y="-6.15" dx="0.9" dy="3" layer="1" roundness="50"/>
<smd name="4" x="-20.955" y="-6.15" dx="0.9" dy="3" layer="1" roundness="50"/>
<smd name="3" x="-22.225" y="-6.15" dx="0.9" dy="3" layer="1" roundness="50"/>
<smd name="2" x="-23.495" y="-6.15" dx="0.9" dy="3" layer="1" roundness="50"/>
<smd name="1" x="-24.765" y="-6.15" dx="0.9" dy="3" layer="1" roundness="50"/>
<wire x1="27.3" y1="4.25" x2="25.8" y2="4.25" width="0.2032" layer="51"/>
<wire x1="25.8" y1="4.25" x2="-25.8" y2="4.25" width="0.2032" layer="51"/>
<wire x1="-25.8" y1="4.25" x2="-27.3" y2="4.25" width="0.2032" layer="51"/>
<wire x1="27.3" y1="4.25" x2="27.3" y2="-7.25" width="0.2032" layer="51"/>
<wire x1="-27.3" y1="4.25" x2="-27.3" y2="-7.25" width="0.2032" layer="51"/>
<wire x1="27.3" y1="-7.25" x2="26" y2="-7.25" width="0.2032" layer="51"/>
<wire x1="-26" y1="-7.25" x2="-27.3" y2="-7.25" width="0.2032" layer="51"/>
<wire x1="26.924" y1="-6.985" x2="26.035" y2="-6.985" width="0.2032" layer="21"/>
<wire x1="-26.924" y1="-6.985" x2="-26.035" y2="-6.985" width="0.2032" layer="21"/>
<text x="-2.54" y="0" size="0.762" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-2.54" y="-1.27" size="0.762" layer="51" font="vector" ratio="15">&gt;LABEL</text>
<text x="-2.54" y="-1.27" size="0.762" layer="21" font="vector" ratio="15">&gt;LABEL</text>
<pad name="P$1" x="-26.15" y="2.9" drill="1.4" shape="long" rot="R90"/>
<pad name="P$2" x="26.15" y="2.9" drill="1.4" shape="long" rot="R90"/>
<wire x1="-26.85" y1="2.3" x2="-26.85" y2="3.5" width="0" layer="46"/>
<wire x1="-26.85" y1="3.5" x2="-26.14" y2="4.307" width="0" layer="46" curve="-90"/>
<wire x1="-26.14" y1="4.307" x2="-25.45" y2="3.5" width="0" layer="46" curve="-90"/>
<wire x1="-25.45" y1="3.5" x2="-25.45" y2="2.3" width="0" layer="46"/>
<wire x1="-25.45" y1="2.3" x2="-26.15" y2="1.5" width="0" layer="46" curve="-90"/>
<wire x1="-26.15" y1="1.5" x2="-26.85" y2="2.3" width="0" layer="46" curve="-90"/>
<wire x1="25.45" y1="2.3" x2="25.45" y2="3.5" width="0" layer="46"/>
<wire x1="25.45" y1="3.5" x2="26.15" y2="4.3" width="0" layer="46" curve="-90"/>
<wire x1="26.15" y1="4.3" x2="26.85" y2="3.5" width="0" layer="46" curve="-90"/>
<wire x1="26.85" y1="3.5" x2="26.85" y2="2.3" width="0" layer="46"/>
<wire x1="26.85" y1="2.3" x2="26.15" y2="1.5" width="0" layer="46" curve="-90"/>
<wire x1="26.15" y1="1.5" x2="25.45" y2="2.3" width="0" layer="46" curve="-90"/>
<wire x1="-26.924" y1="0.635" x2="-26.924" y2="-6.985" width="0.2032" layer="21"/>
<wire x1="24.765" y1="3.937" x2="-24.765" y2="3.937" width="0.2032" layer="21"/>
<wire x1="26.924" y1="0.635" x2="26.924" y2="-6.985" width="0.2032" layer="21"/>
<wire x1="25.8" y1="4.25" x2="25.8" y2="35.35" width="0.2032" layer="51"/>
<wire x1="25.8" y1="35.35" x2="22.8" y2="38.35" width="0.2032" layer="51" curve="90"/>
<wire x1="22.8" y1="38.35" x2="-22.8" y2="38.35" width="0.2032" layer="51"/>
<wire x1="-22.8" y1="38.35" x2="-25.8" y2="35.35" width="0.2032" layer="51" curve="90"/>
<wire x1="-25.8" y1="35.35" x2="-25.8" y2="4.25" width="0.2032" layer="51"/>
<text x="-12.7" y="46.99" size="2.54" layer="51" font="vector" ratio="15">BBC micro:bit</text>
<wire x1="-1.27" y1="45.72" x2="1.27" y2="45.72" width="0.2032" layer="51"/>
<wire x1="1.27" y1="45.72" x2="1.27" y2="41.91" width="0.2032" layer="51"/>
<wire x1="1.27" y1="41.91" x2="2.54" y2="41.91" width="0.2032" layer="51"/>
<wire x1="2.54" y1="41.91" x2="0" y2="39.37" width="0.2032" layer="51"/>
<wire x1="0" y1="39.37" x2="-2.54" y2="41.91" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="41.91" x2="-1.27" y2="41.91" width="0.2032" layer="51"/>
<wire x1="-1.27" y1="41.91" x2="-1.27" y2="45.72" width="0.2032" layer="51"/>
<wire x1="-3.282" y1="62.53" x2="-1.758" y2="62.53" width="0.2032" layer="51"/>
<wire x1="-1.758" y1="62.53" x2="-0.996" y2="62.53" width="0.2032" layer="51"/>
<wire x1="-0.996" y1="62.53" x2="5.708" y2="62.53" width="0.2032" layer="51"/>
<wire x1="-6.43" y1="61.768" x2="-6.43" y2="53.132" width="0.2032" layer="51"/>
<wire x1="-6.43" y1="53.132" x2="-5.668" y2="52.37" width="0.2032" layer="51" curve="90"/>
<wire x1="-5.668" y1="52.37" x2="5.708" y2="52.37" width="0.2032" layer="51"/>
<wire x1="5.708" y1="52.37" x2="6.47" y2="53.132" width="0.2032" layer="51" curve="90"/>
<wire x1="6.47" y1="53.132" x2="6.47" y2="61.768" width="0.2032" layer="51"/>
<wire x1="6.47" y1="61.768" x2="5.708" y2="62.53" width="0.2032" layer="51" curve="90"/>
<wire x1="-5.668" y1="62.53" x2="-6.43" y2="61.768" width="0.2032" layer="51" curve="90"/>
<wire x1="-0.615" y1="61.387" x2="0.655" y2="61.387" width="0.254" layer="51"/>
<wire x1="0.655" y1="61.387" x2="0.655" y2="60.117" width="0.254" layer="51" curve="-180"/>
<wire x1="0.655" y1="60.117" x2="-0.615" y2="60.117" width="0.254" layer="51"/>
<wire x1="-0.615" y1="60.117" x2="-0.615" y2="61.387" width="0.254" layer="51" curve="-180"/>
<circle x="-0.49" y="60.754" radius="0.09219375" width="0.2032" layer="51"/>
<circle x="0.53" y="60.754" radius="0.09219375" width="0.2032" layer="51"/>
<wire x1="-1.758" y1="61.6664" x2="-1.758" y2="62.53" width="0.2032" layer="51"/>
<wire x1="-1.758" y1="62.53" x2="-3.282" y2="60.8028" width="0.2032" layer="51"/>
<wire x1="-3.282" y1="60.8028" x2="-3.282" y2="62.53" width="0.2032" layer="51"/>
<polygon width="0.2032" layer="51">
<vertex x="-0.996" y="62.53"/>
<vertex x="-1.758" y="61.6664"/>
<vertex x="-1.758" y="62.53"/>
</polygon>
<polygon width="0.2032" layer="51">
<vertex x="-1.758" y="62.53"/>
<vertex x="-3.282" y="60.8028"/>
<vertex x="-3.282" y="62.53"/>
</polygon>
<polygon width="0.2032" layer="51">
<vertex x="-3.282" y="62.53"/>
<vertex x="-6.43" y="59.101"/>
<vertex x="-6.43" y="61.768" curve="-90"/>
<vertex x="-5.668" y="62.53"/>
</polygon>
<wire x1="-1.758" y1="61.6664" x2="-0.996" y2="62.53" width="0.2032" layer="51"/>
<wire x1="-5.6" y1="58.21" x2="-4.3" y2="58.21" width="0.254" layer="51"/>
<wire x1="-4.3" y1="56.91" x2="-4.3" y2="58.21" width="0.254" layer="51"/>
<wire x1="-5.6" y1="56.91" x2="-5.6" y2="58.21" width="0.254" layer="51"/>
<wire x1="-4.3" y1="56.91" x2="-5.6" y2="56.91" width="0.254" layer="51"/>
<circle x="-4.95" y="57.56" radius="0.5280125" width="0.254" layer="51"/>
<circle x="-4.95" y="57.56" radius="0.64" width="0.254" layer="51"/>
<wire x1="5.7" y1="56.91" x2="4.4" y2="56.91" width="0.254" layer="51"/>
<wire x1="4.4" y1="58.21" x2="4.4" y2="56.91" width="0.254" layer="51"/>
<wire x1="5.7" y1="58.21" x2="5.7" y2="56.91" width="0.254" layer="51"/>
<wire x1="4.4" y1="58.21" x2="5.7" y2="58.21" width="0.254" layer="51"/>
<circle x="5.05" y="57.56" radius="0.5280125" width="0.254" layer="51"/>
<circle x="5.05" y="57.56" radius="0.64" width="0.254" layer="51"/>
<rectangle x1="-0.1" y1="57.11" x2="0.1" y2="57.51" layer="51"/>
<rectangle x1="-0.1" y1="57.91" x2="0.1" y2="58.31" layer="51"/>
<rectangle x1="-0.1" y1="58.71" x2="0.1" y2="59.11" layer="51"/>
<rectangle x1="-0.1" y1="56.31" x2="0.1" y2="56.71" layer="51"/>
<rectangle x1="-0.1" y1="55.51" x2="0.1" y2="55.91" layer="51"/>
<rectangle x1="0.9" y1="57.11" x2="1.1" y2="57.51" layer="51"/>
<rectangle x1="0.9" y1="57.91" x2="1.1" y2="58.31" layer="51"/>
<rectangle x1="0.9" y1="58.71" x2="1.1" y2="59.11" layer="51"/>
<rectangle x1="0.9" y1="56.31" x2="1.1" y2="56.71" layer="51"/>
<rectangle x1="0.9" y1="55.51" x2="1.1" y2="55.91" layer="51"/>
<rectangle x1="1.9" y1="57.11" x2="2.1" y2="57.51" layer="51"/>
<rectangle x1="1.9" y1="57.91" x2="2.1" y2="58.31" layer="51"/>
<rectangle x1="1.9" y1="58.71" x2="2.1" y2="59.11" layer="51"/>
<rectangle x1="1.9" y1="56.31" x2="2.1" y2="56.71" layer="51"/>
<rectangle x1="1.9" y1="55.51" x2="2.1" y2="55.91" layer="51"/>
<rectangle x1="-1.1" y1="57.11" x2="-0.9" y2="57.51" layer="51"/>
<rectangle x1="-1.1" y1="57.91" x2="-0.9" y2="58.31" layer="51"/>
<rectangle x1="-1.1" y1="58.71" x2="-0.9" y2="59.11" layer="51"/>
<rectangle x1="-1.1" y1="56.31" x2="-0.9" y2="56.71" layer="51"/>
<rectangle x1="-1.1" y1="55.51" x2="-0.9" y2="55.91" layer="51"/>
<rectangle x1="-2.1" y1="57.11" x2="-1.9" y2="57.51" layer="51"/>
<rectangle x1="-2.1" y1="57.91" x2="-1.9" y2="58.31" layer="51"/>
<rectangle x1="-2.1" y1="58.71" x2="-1.9" y2="59.11" layer="51"/>
<rectangle x1="-2.1" y1="56.31" x2="-1.9" y2="56.71" layer="51"/>
<rectangle x1="-2.1" y1="55.51" x2="-1.9" y2="55.91" layer="51"/>
<rectangle x1="0.7" y1="52.31" x2="0.9" y2="53.61" layer="51"/>
<rectangle x1="1" y1="52.31" x2="1.2" y2="53.61" layer="51"/>
<rectangle x1="1.3" y1="52.31" x2="1.5" y2="53.61" layer="51"/>
<rectangle x1="1.6" y1="52.31" x2="1.8" y2="53.61" layer="51"/>
<rectangle x1="1.9" y1="52.31" x2="2.1" y2="53.61" layer="51"/>
<rectangle x1="3.8" y1="52.31" x2="4" y2="53.61" layer="51"/>
<rectangle x1="4.1" y1="52.31" x2="4.3" y2="53.61" layer="51"/>
<rectangle x1="4.4" y1="52.31" x2="4.6" y2="53.61" layer="51"/>
<rectangle x1="3.5" y1="52.31" x2="3.7" y2="53.61" layer="51"/>
<circle x="0" y="54.21" radius="0.48" width="0.24" layer="51"/>
<rectangle x1="-0.6" y1="52.31" x2="-0.4" y2="54.21" layer="51"/>
<rectangle x1="0.4" y1="52.31" x2="0.6" y2="54.21" layer="51"/>
<rectangle x1="-0.5" y1="52.31" x2="0.5" y2="53.81" layer="51"/>
<circle x="2.8" y="54.21" radius="0.48" width="0.24" layer="51"/>
<rectangle x1="2.2" y1="52.31" x2="2.4" y2="54.21" layer="51"/>
<rectangle x1="3.2" y1="52.31" x2="3.4" y2="54.21" layer="51"/>
<rectangle x1="2.3" y1="52.31" x2="3.3" y2="53.81" layer="51"/>
<circle x="5.3" y="54.21" radius="0.48" width="0.24" layer="51"/>
<rectangle x1="4.7" y1="52.31" x2="4.9" y2="54.21" layer="51"/>
<rectangle x1="5.7" y1="52.31" x2="5.9" y2="54.21" layer="51"/>
<rectangle x1="4.8" y1="52.31" x2="5.8" y2="53.81" layer="51"/>
<rectangle x1="6" y1="52.51" x2="6.2" y2="53.61" layer="51"/>
<rectangle x1="-2.1" y1="52.31" x2="-1.9" y2="53.61" layer="51"/>
<rectangle x1="-1.8" y1="52.31" x2="-1.6" y2="53.61" layer="51"/>
<rectangle x1="-1.5" y1="52.31" x2="-1.3" y2="53.61" layer="51"/>
<rectangle x1="-1.2" y1="52.31" x2="-1" y2="53.61" layer="51"/>
<rectangle x1="-0.9" y1="52.31" x2="-0.7" y2="53.61" layer="51"/>
<circle x="-2.8" y="54.21" radius="0.48" width="0.24" layer="51"/>
<rectangle x1="-3.4" y1="52.31" x2="-3.2" y2="54.21" layer="51"/>
<rectangle x1="-2.4" y1="52.31" x2="-2.2" y2="54.21" layer="51"/>
<rectangle x1="-3.3" y1="52.31" x2="-2.3" y2="53.81" layer="51"/>
<rectangle x1="-4.6" y1="52.31" x2="-4.4" y2="53.61" layer="51"/>
<rectangle x1="-4.3" y1="52.31" x2="-4.1" y2="53.61" layer="51"/>
<rectangle x1="-4" y1="52.31" x2="-3.8" y2="53.61" layer="51"/>
<rectangle x1="-3.7" y1="52.31" x2="-3.5" y2="53.61" layer="51"/>
<circle x="-5.3" y="54.21" radius="0.48" width="0.24" layer="51"/>
<rectangle x1="-5.9" y1="52.31" x2="-5.7" y2="54.21" layer="51"/>
<rectangle x1="-4.9" y1="52.31" x2="-4.7" y2="54.21" layer="51"/>
<rectangle x1="-5.8" y1="52.31" x2="-4.8" y2="53.81" layer="51"/>
<rectangle x1="-6.2" y1="52.51" x2="-6" y2="53.61" layer="51"/>
<polygon width="0.2" layer="31">
<vertex x="-26.15" y="0.9" curve="-90"/>
<vertex x="-27.1" y="1.85"/>
<vertex x="-27.1" y="3.950840625" curve="-90"/>
<vertex x="-26.150840625" y="4.9" curve="-90"/>
<vertex x="-25.2" y="3.949159375"/>
<vertex x="-25.2" y="1.85" curve="-90"/>
</polygon>
<polygon width="0.2" layer="31">
<vertex x="26.15" y="0.9" curve="-90"/>
<vertex x="25.2" y="1.85"/>
<vertex x="25.2" y="3.950840625" curve="-90"/>
<vertex x="26.149159375" y="4.9" curve="-90"/>
<vertex x="27.1" y="3.949159375"/>
<vertex x="27.1" y="1.85" curve="-90"/>
</polygon>
</package>
<package name="GROVE-04-RA">
<description>SMD Grove Connector (Right Angle) - 4 Ways&lt;br&gt;
Standard connector for Seeed Studio Grove System.&lt;br&gt;
&lt;br&gt;
Created by: Wai Weng</description>
<wire x1="-5.95" y1="3.8" x2="-3.5" y2="3.8" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="3.8" x2="-3.5" y2="2.8" width="0.2032" layer="51"/>
<wire x1="-3.5" y1="2.8" x2="2.5" y2="2.8" width="0.2032" layer="51"/>
<wire x1="2.5" y1="2.8" x2="3.3" y2="2.8" width="0.2032" layer="51"/>
<wire x1="3.3" y1="2.8" x2="3.5" y2="2.8" width="0.2032" layer="51"/>
<wire x1="3.5" y1="2.8" x2="3.5" y2="3.8" width="0.2032" layer="51"/>
<wire x1="3.5" y1="3.8" x2="5.95" y2="3.8" width="0.2032" layer="51"/>
<wire x1="5.95" y1="3.8" x2="5.95" y2="-5.3" width="0.2032" layer="51"/>
<wire x1="5.95" y1="-5.3" x2="5.25" y2="-5.3" width="0.2032" layer="51"/>
<wire x1="5.25" y1="-5.3" x2="5.25" y2="-3.8" width="0.2032" layer="51"/>
<rectangle x1="-1.25" y1="-6.3" x2="-0.75" y2="-2.2" layer="51"/>
<rectangle x1="0.75" y1="-6.3" x2="1.25" y2="-2.2" layer="51"/>
<smd name="P$2" x="5.55" y="2.25" dx="3.4" dy="1.6" layer="1" roundness="30" rot="R90"/>
<wire x1="5.25" y1="-3.8" x2="-5.25" y2="-3.8" width="0.2032" layer="51"/>
<wire x1="-5.25" y1="-3.8" x2="-5.25" y2="-5.3" width="0.2032" layer="51"/>
<wire x1="-5.25" y1="-5.3" x2="-5.95" y2="-5.3" width="0.2032" layer="51"/>
<wire x1="-5.95" y1="-5.3" x2="-5.95" y2="3.8" width="0.2032" layer="51"/>
<smd name="1" x="3" y="-4.5" dx="4.6" dy="1" layer="1" roundness="50" rot="R90"/>
<rectangle x1="2.75" y1="-6.3" x2="3.25" y2="-2.2" layer="51"/>
<rectangle x1="-3.25" y1="-6.3" x2="-2.75" y2="-2.2" layer="51"/>
<rectangle x1="4.95" y1="0.8" x2="5.95" y2="3.8" layer="51"/>
<smd name="P$1" x="-5.55" y="2.25" dx="3.4" dy="1.6" layer="1" roundness="30" rot="R90"/>
<rectangle x1="-5.95" y1="0.8" x2="-4.95" y2="3.8" layer="51"/>
<smd name="2" x="1" y="-4.5" dx="4.6" dy="1" layer="1" roundness="50" rot="R90"/>
<smd name="3" x="-1" y="-4.5" dx="4.6" dy="1" layer="1" roundness="50" rot="R90"/>
<smd name="4" x="-3" y="-4.5" dx="4.6" dy="1" layer="1" roundness="50" rot="R90"/>
<wire x1="-4.5" y1="3.4" x2="-3.9" y2="3.4" width="0.2032" layer="21"/>
<wire x1="-3.9" y1="3.4" x2="-3.9" y2="2.4" width="0.2032" layer="21"/>
<wire x1="-3.9" y1="2.4" x2="2.5" y2="2.4" width="0.2032" layer="21"/>
<wire x1="2.5" y1="2.4" x2="3.3" y2="2.4" width="0.2032" layer="21"/>
<wire x1="3.3" y1="2.4" x2="3.9" y2="2.4" width="0.2032" layer="21"/>
<wire x1="3.9" y1="2.4" x2="3.9" y2="3.4" width="0.2032" layer="21"/>
<wire x1="3.9" y1="3.4" x2="4.5" y2="3.4" width="0.2032" layer="21"/>
<wire x1="-5.5" y1="0.3" x2="-5.5" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="-5.5" y1="-1.8" x2="5.5" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="5.5" y1="-1.8" x2="5.5" y2="0.3" width="0.2032" layer="21"/>
<wire x1="3.3" y1="2.4" x2="3.3" y2="1.4" width="0.2032" layer="21"/>
<wire x1="3.3" y1="1.4" x2="2.5" y2="1.4" width="0.2032" layer="21"/>
<wire x1="2.5" y1="1.4" x2="2.5" y2="2.4" width="0.2032" layer="21"/>
<wire x1="3.3" y1="2.8" x2="3.3" y2="1.4" width="0.2032" layer="51"/>
<wire x1="3.3" y1="1.4" x2="2.5" y2="1.4" width="0.2032" layer="51"/>
<wire x1="2.5" y1="1.4" x2="2.5" y2="2.8" width="0.2032" layer="51"/>
<text x="0" y="0" size="0.762" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<text x="7.1" y="-5.4" size="0.762" layer="21" font="vector" ratio="15" rot="R90">&gt;LABEL</text>
</package>
<package name="AUDIO_JACK">
<wire x1="-7" y1="3" x2="-4.5" y2="3" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="3" x2="7" y2="3" width="0.2032" layer="51"/>
<wire x1="7" y1="3" x2="7" y2="-3" width="0.2032" layer="51"/>
<wire x1="7" y1="-3" x2="-4.5" y2="-3" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="-3" x2="-7" y2="-3" width="0.2032" layer="51"/>
<wire x1="-7" y1="-3" x2="-7" y2="3" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="3" x2="-4.5" y2="-3" width="0.2032" layer="51"/>
<smd name="4" x="-2.7" y="3.25" dx="1.875" dy="1.875" layer="1" rot="R90"/>
<smd name="5" x="0.5" y="3.25" dx="1.875" dy="1.875" layer="1" rot="R90"/>
<smd name="6" x="4" y="3.25" dx="1.875" dy="1.875" layer="1" rot="R90"/>
<smd name="1" x="-2.7" y="-3.25" dx="1.875" dy="1.875" layer="1" rot="R90"/>
<smd name="2" x="0.5" y="-3.25" dx="1.875" dy="1.875" layer="1" rot="R90"/>
<smd name="3" x="4" y="-3.25" dx="1.875" dy="1.875" layer="1" rot="R90"/>
<wire x1="6.5" y1="2" x2="6.5" y2="-2" width="0.2032" layer="21"/>
<wire x1="6.5" y1="-2" x2="-4.5" y2="-2" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="-2" x2="-6.5" y2="-2" width="0.2032" layer="51"/>
<wire x1="-6.5" y1="-2" x2="-6.5" y2="2" width="0.2032" layer="51"/>
<wire x1="-6.5" y1="2" x2="-4.5" y2="2" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="2" x2="6.5" y2="2" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="2" x2="-4.5" y2="-2" width="0.2032" layer="21"/>
<hole x="-2" y="0" drill="1.5"/>
<hole x="4" y="0" drill="1.5"/>
</package>
<package name="AUDIO-JACK-MJ-074N">
<description>Audio Jack - MJ-074N&lt;br&gt;
&lt;br&gt;
Created by: Wai Weng</description>
<circle x="0" y="0" radius="4" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="4" width="0.2032" layer="51"/>
<pad name="2" x="2.6" y="0" drill="1.2" shape="long" rot="R90"/>
<pad name="3" x="-0.4" y="2.55" drill="0.9" shape="long" rot="R180"/>
<wire x1="-1.1" y1="3" x2="1.1" y2="3" width="0" layer="46"/>
<wire x1="1.1" y1="2.1" x2="-1.1" y2="2.1" width="0" layer="46"/>
<wire x1="1.1" y1="3" x2="1.1" y2="2.1" width="0" layer="46" curve="-180"/>
<wire x1="-1.1" y1="2.1" x2="-1.1" y2="3" width="0" layer="46" curve="-180"/>
<pad name="4" x="0.4" y="2.55" drill="0.9" shape="long" rot="R180"/>
<wire x1="3" y1="1" x2="3" y2="-1" width="0" layer="46"/>
<wire x1="2.2" y1="-1" x2="2.2" y2="1" width="0" layer="46"/>
<wire x1="3" y1="-1" x2="2.2" y2="-1" width="0" layer="46" curve="-180"/>
<wire x1="2.2" y1="1" x2="3" y2="1" width="0" layer="46" curve="-180"/>
<pad name="1" x="-2.6" y="0" drill="1.2" shape="long" rot="R90"/>
<wire x1="-2.2" y1="1" x2="-2.2" y2="-1" width="0" layer="46"/>
<wire x1="-3" y1="-1" x2="-3" y2="1" width="0" layer="46"/>
<wire x1="-2.2" y1="-1" x2="-3" y2="-1" width="0" layer="46" curve="-180"/>
<wire x1="-3" y1="1" x2="-2.2" y2="1" width="0" layer="46" curve="-180"/>
<text x="0" y="0" size="0.762" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<text x="0" y="-5.08" size="0.762" layer="21" font="vector" ratio="15" align="center">&gt;LABEL</text>
</package>
<package name="ICSP-5PIN">
<description>Cytron Internal ICSP Header - 1X05</description>
<pad name="1" x="0" y="0" drill="1" diameter="1.6256" shape="square" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1" diameter="1.6256" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1" diameter="1.6256" rot="R90"/>
<text x="-1.27" y="2.54" size="0.762" layer="25" font="vector" ratio="15" align="top-left">&gt;NAME</text>
<wire x1="-1.27" y1="1.27" x2="11.43" y2="1.27" width="0.2032" layer="51"/>
<wire x1="11.43" y1="1.27" x2="11.43" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="11.43" y1="-1.27" x2="-1.27" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="1.27" width="0.2032" layer="51"/>
<pad name="4" x="7.62" y="0" drill="1" diameter="1.6256" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1" diameter="1.6256" rot="R90"/>
</package>
<package name="HDR-F1X03">
<description>Female Header - 1X03</description>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.778" rot="R90"/>
<text x="-1.27" y="1.651" size="0.762" layer="25" font="vector" ratio="15">&gt;NAME</text>
<wire x1="-1.27" y1="1.27" x2="6.35" y2="1.27" width="0.2032" layer="51"/>
<wire x1="6.35" y1="1.27" x2="6.35" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="6.35" y1="-1.27" x2="-1.27" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="1.27" width="0.2032" layer="51"/>
<text x="-1.27" y="-1.651" size="0.762" layer="21" font="vector" ratio="15" align="top-left">&gt;LABEL</text>
<text x="-1.27" y="-1.651" size="0.762" layer="51" font="vector" ratio="15" align="top-left">&gt;LABEL</text>
<wire x1="-1.27" y1="1.27" x2="6.35" y2="1.27" width="0.2032" layer="21"/>
<wire x1="6.35" y1="1.27" x2="6.35" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-1.27" x2="-1.27" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="1.27" width="0.2032" layer="21"/>
</package>
<package name="HDR-M1X03">
<description>Male Header - 1X03</description>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.778" rot="R90"/>
<text x="-1.27" y="1.651" size="0.762" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-1.27" y="-1.651" size="0.762" layer="21" font="vector" ratio="15" align="top-left">&gt;LABEL</text>
<text x="-1.27" y="-1.651" size="0.762" layer="51" font="vector" ratio="15" align="top-left">&gt;LABEL</text>
<wire x1="-1.27" y1="0.762" x2="-0.762" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-0.762" y1="1.27" x2="0.762" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.762" y1="1.27" x2="1.27" y2="0.762" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.762" x2="1.778" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.778" y1="1.27" x2="3.302" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.302" y1="1.27" x2="3.81" y2="0.762" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.762" x2="3.302" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.302" y1="-1.27" x2="1.778" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.778" y1="-1.27" x2="1.27" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.762" x2="0.762" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.762" y1="-1.27" x2="-0.762" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.762" y1="-1.27" x2="-1.27" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.762" x2="-1.27" y2="0.762" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.762" x2="4.318" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.318" y1="1.27" x2="5.842" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.842" y1="1.27" x2="6.35" y2="0.762" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.762" x2="5.842" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.842" y1="-1.27" x2="4.318" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="3.81" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.762" x2="6.35" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.762" x2="1.27" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.762" x2="3.81" y2="-0.762" width="0.2032" layer="21"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.778" rot="R90"/>
<rectangle x1="-0.32" y1="-0.32" x2="0.32" y2="0.32" layer="51"/>
<rectangle x1="2.22" y1="-0.32" x2="2.86" y2="0.32" layer="51"/>
<rectangle x1="4.76" y1="-0.32" x2="5.4" y2="0.32" layer="51"/>
<wire x1="-1.27" y1="0.762" x2="-0.762" y2="1.27" width="0.2032" layer="51"/>
<wire x1="-0.762" y1="1.27" x2="0.762" y2="1.27" width="0.2032" layer="51"/>
<wire x1="0.762" y1="1.27" x2="1.27" y2="0.762" width="0.2032" layer="51"/>
<wire x1="1.27" y1="0.762" x2="1.778" y2="1.27" width="0.2032" layer="51"/>
<wire x1="1.778" y1="1.27" x2="3.302" y2="1.27" width="0.2032" layer="51"/>
<wire x1="3.302" y1="1.27" x2="3.81" y2="0.762" width="0.2032" layer="51"/>
<wire x1="3.81" y1="-0.762" x2="3.302" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="3.302" y1="-1.27" x2="1.778" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="1.778" y1="-1.27" x2="1.27" y2="-0.762" width="0.2032" layer="51"/>
<wire x1="1.27" y1="-0.762" x2="0.762" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="0.762" y1="-1.27" x2="-0.762" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="-0.762" y1="-1.27" x2="-1.27" y2="-0.762" width="0.2032" layer="51"/>
<wire x1="-1.27" y1="-0.762" x2="-1.27" y2="0.762" width="0.2032" layer="51"/>
<wire x1="3.81" y1="0.762" x2="4.318" y2="1.27" width="0.2032" layer="51"/>
<wire x1="4.318" y1="1.27" x2="5.842" y2="1.27" width="0.2032" layer="51"/>
<wire x1="5.842" y1="1.27" x2="6.35" y2="0.762" width="0.2032" layer="51"/>
<wire x1="6.35" y1="-0.762" x2="5.842" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="5.842" y1="-1.27" x2="4.318" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="4.318" y1="-1.27" x2="3.81" y2="-0.762" width="0.2032" layer="51"/>
<wire x1="6.35" y1="0.762" x2="6.35" y2="-0.762" width="0.2032" layer="51"/>
<wire x1="1.27" y1="0.762" x2="1.27" y2="-0.762" width="0.2032" layer="51"/>
<wire x1="3.81" y1="0.762" x2="3.81" y2="-0.762" width="0.2032" layer="51"/>
</package>
<package name="HDR-M1X03-LOCK">
<description>Male Header - 1X03 (Pad shifted 5mil to lock the header during soldering)</description>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.778" rot="R90"/>
<text x="-1.27" y="1.651" size="0.762" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-1.27" y="-1.651" size="0.762" layer="21" font="vector" ratio="15" align="top-left">&gt;LABEL</text>
<text x="-1.27" y="-1.651" size="0.762" layer="51" font="vector" ratio="15" align="top-left">&gt;LABEL</text>
<wire x1="-1.27" y1="0.762" x2="-0.762" y2="1.27" width="0.2032" layer="51"/>
<wire x1="-0.762" y1="1.27" x2="0.762" y2="1.27" width="0.2032" layer="51"/>
<wire x1="0.762" y1="1.27" x2="1.27" y2="0.762" width="0.2032" layer="51"/>
<wire x1="1.27" y1="0.762" x2="1.778" y2="1.27" width="0.2032" layer="51"/>
<wire x1="1.778" y1="1.27" x2="3.302" y2="1.27" width="0.2032" layer="51"/>
<wire x1="3.302" y1="1.27" x2="3.81" y2="0.762" width="0.2032" layer="51"/>
<wire x1="3.81" y1="-0.762" x2="3.302" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="3.302" y1="-1.27" x2="1.778" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="1.778" y1="-1.27" x2="1.27" y2="-0.762" width="0.2032" layer="51"/>
<wire x1="1.27" y1="-0.762" x2="0.762" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="0.762" y1="-1.27" x2="-0.762" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="-0.762" y1="-1.27" x2="-1.27" y2="-0.762" width="0.2032" layer="51"/>
<wire x1="-1.27" y1="-0.762" x2="-1.27" y2="0.762" width="0.2032" layer="51"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.778" rot="R90"/>
<wire x1="3.81" y1="0.762" x2="4.318" y2="1.27" width="0.2032" layer="51"/>
<wire x1="4.318" y1="1.27" x2="5.842" y2="1.27" width="0.2032" layer="51"/>
<wire x1="5.842" y1="1.27" x2="6.35" y2="0.762" width="0.2032" layer="51"/>
<wire x1="6.35" y1="-0.762" x2="5.842" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="5.842" y1="-1.27" x2="4.318" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="4.318" y1="-1.27" x2="3.81" y2="-0.762" width="0.2032" layer="51"/>
<wire x1="6.35" y1="0.762" x2="6.35" y2="-0.762" width="0.2032" layer="51"/>
<rectangle x1="-0.32" y1="-0.32" x2="0.32" y2="0.32" layer="51"/>
<rectangle x1="2.22" y1="-0.32" x2="2.86" y2="0.32" layer="51"/>
<rectangle x1="4.76" y1="-0.32" x2="5.4" y2="0.32" layer="51"/>
<wire x1="1.27" y1="0.762" x2="1.27" y2="-0.762" width="0.2032" layer="51"/>
<wire x1="3.81" y1="0.762" x2="3.81" y2="-0.762" width="0.2032" layer="51"/>
<wire x1="-1.27" y1="0.762" x2="-0.762" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-0.762" y1="1.27" x2="0.762" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.762" y1="1.27" x2="1.27" y2="0.762" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.762" x2="1.778" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.778" y1="1.27" x2="3.302" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.302" y1="1.27" x2="3.81" y2="0.762" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.762" x2="3.302" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.302" y1="-1.27" x2="1.778" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.778" y1="-1.27" x2="1.27" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.762" x2="0.762" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.762" y1="-1.27" x2="-0.762" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.762" y1="-1.27" x2="-1.27" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.762" x2="-1.27" y2="0.762" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.762" x2="4.318" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.318" y1="1.27" x2="5.842" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.842" y1="1.27" x2="6.35" y2="0.762" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.762" x2="5.842" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.842" y1="-1.27" x2="4.318" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="3.81" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.762" x2="6.35" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.762" x2="1.27" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.762" x2="3.81" y2="-0.762" width="0.2032" layer="21"/>
</package>
<package name="HDR-1X03-NOSILK">
<description>1X03 pad without overlay.&lt;br&gt;
Do not use this footprint for component.</description>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.778" rot="R90"/>
<text x="-0.889" y="1.27" size="0.762" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-0.889" y="-1.27" size="0.762" layer="21" font="vector" ratio="15" align="top-left">&gt;LABEL</text>
<text x="-0.889" y="-1.27" size="0.762" layer="51" font="vector" ratio="15" align="top-left">&gt;LABEL</text>
</package>
<package name="HDR-1X03-SQ-NOSILK">
<description>1X03 square pad without overlay.&lt;br&gt;
&lt;br&gt;
Created by: Wai Weng</description>
<pad name="1" x="-2.54" y="0" drill="1.4" diameter="2" shape="square" rot="R90"/>
<pad name="2" x="0" y="0" drill="1.4" diameter="2" shape="square" rot="R90"/>
<pad name="3" x="2.54" y="0" drill="1.4" diameter="2" shape="square" rot="R90"/>
<text x="-3.429" y="1.27" size="0.762" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-3.429" y="-1.27" size="0.762" layer="21" font="vector" ratio="15" align="top-left">&gt;LABEL</text>
<text x="-3.429" y="-1.27" size="0.762" layer="51" font="vector" ratio="15" align="top-left">&gt;LABEL</text>
</package>
<package name="JST-S3B-PH-SM4-TB">
<description>SMD 3 Ways Connector (Right Angle)&lt;br&gt;
S3B-PH-SM4-TB&lt;br&gt;</description>
<smd name="2" x="0" y="-3.6" dx="2.9" dy="1" layer="1" roundness="50" rot="R90"/>
<smd name="1" x="2" y="-3.6" dx="2.9" dy="1" layer="1" roundness="50" rot="R90"/>
<wire x1="-4.777" y1="-3.529" x2="-4.777" y2="0.49" width="0.2032" layer="21"/>
<wire x1="-4.777" y1="-3.529" x2="-4.231" y2="-3.529" width="0.2032" layer="21"/>
<wire x1="-4.231" y1="-3.529" x2="-4.231" y2="-1.956" width="0.2032" layer="21"/>
<wire x1="-4.231" y1="-1.956" x2="-2.8" y2="-1.956" width="0.2032" layer="21"/>
<wire x1="4.777" y1="-3.529" x2="4.231" y2="-3.529" width="0.2032" layer="21"/>
<wire x1="4.777" y1="0.49" x2="4.777" y2="-3.529" width="0.2032" layer="21"/>
<wire x1="4.231" y1="-3.529" x2="4.231" y2="-1.956" width="0.2032" layer="21"/>
<wire x1="4.231" y1="-1.956" x2="2.8" y2="-1.956" width="0.2032" layer="21"/>
<wire x1="-3.531" y1="3.409" x2="2.358" y2="3.409" width="0.2032" layer="21"/>
<wire x1="-5.031" y1="3.79" x2="5.031" y2="3.79" width="0.2032" layer="51"/>
<wire x1="5.031" y1="3.79" x2="5.031" y2="-3.91" width="0.2032" layer="51"/>
<wire x1="5.031" y1="-3.91" x2="4.231" y2="-3.91" width="0.2032" layer="51"/>
<wire x1="4.231" y1="-3.91" x2="4.231" y2="-2.21" width="0.2032" layer="51"/>
<wire x1="4.231" y1="-2.21" x2="3.431" y2="-2.21" width="0.2032" layer="51"/>
<wire x1="3.431" y1="-2.21" x2="2.231" y2="-2.21" width="0.2032" layer="51"/>
<wire x1="2.231" y1="-2.21" x2="-2.231" y2="-2.21" width="0.2032" layer="51"/>
<wire x1="-2.231" y1="-2.21" x2="-3.431" y2="-2.21" width="0.2032" layer="51"/>
<wire x1="-3.431" y1="-2.21" x2="-4.231" y2="-2.21" width="0.2032" layer="51"/>
<wire x1="-4.231" y1="-2.21" x2="-4.231" y2="-3.91" width="0.2032" layer="51"/>
<wire x1="-4.231" y1="-3.91" x2="-5.031" y2="-3.91" width="0.2032" layer="51"/>
<wire x1="-5.031" y1="-3.91" x2="-5.031" y2="3.79" width="0.2032" layer="51"/>
<rectangle x1="-0.4" y1="-4.91" x2="0.4" y2="-2.31" layer="51"/>
<wire x1="3.431" y1="-2.21" x2="3.431" y2="1.79" width="0.2032" layer="51"/>
<wire x1="3.431" y1="1.79" x2="2.231" y2="1.79" width="0.2032" layer="51"/>
<wire x1="2.231" y1="1.79" x2="2.231" y2="-2.21" width="0.2032" layer="51"/>
<wire x1="-3.431" y1="-2.21" x2="-3.431" y2="1.79" width="0.2032" layer="51"/>
<wire x1="-3.431" y1="1.79" x2="-2.231" y2="1.79" width="0.2032" layer="51"/>
<wire x1="-2.231" y1="1.79" x2="-2.231" y2="-2.21" width="0.2032" layer="51"/>
<text x="0" y="0" size="0.762" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<text x="0" y="4.572" size="0.762" layer="21" font="vector" ratio="15" align="center">&gt;LABEL</text>
<wire x1="2.358" y1="3.409" x2="3.177" y2="3.409" width="0.2032" layer="21"/>
<wire x1="3.177" y1="3.409" x2="3.531" y2="3.409" width="0.2032" layer="21"/>
<wire x1="2.358" y1="3.409" x2="2.358" y2="2.663" width="0.2032" layer="21"/>
<wire x1="2.358" y1="2.663" x2="3.177" y2="2.663" width="0.2032" layer="21"/>
<wire x1="3.177" y1="2.663" x2="3.177" y2="3.409" width="0.2032" layer="21"/>
<smd name="P$1" x="-4.43" y="2.29" dx="3" dy="1.2" layer="1" roundness="30" rot="R90"/>
<smd name="P$2" x="4.43" y="2.29" dx="3" dy="1.2" layer="1" roundness="30" rot="R90"/>
<smd name="3" x="-2" y="-3.6" dx="2.9" dy="1" layer="1" roundness="50" rot="R90"/>
<rectangle x1="-2.4" y1="-4.91" x2="-1.6" y2="-2.31" layer="51"/>
<rectangle x1="1.6" y1="-4.91" x2="2.4" y2="-2.31" layer="51"/>
</package>
<package name="S-HDR-M1X03-RA">
<description>SMD Right Angle Male Header - 1X03
&lt;br&gt;
&lt;br&gt;
Created by: Wai Weng</description>
<wire x1="-3.81" y1="1.27" x2="3.81" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="1.27" x2="3.81" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-1.27" x2="-3.81" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-3.81" y1="-1.27" x2="-3.81" y2="1.27" width="0.2032" layer="21"/>
<rectangle x1="2.22" y1="1.27" x2="2.86" y2="7.27" layer="51"/>
<rectangle x1="-0.32" y1="1.27" x2="0.32" y2="7.27" layer="51"/>
<rectangle x1="-2.86" y1="1.27" x2="-2.22" y2="7.27" layer="51"/>
<rectangle x1="2.22" y1="-5.97" x2="2.86" y2="-4.17" layer="51"/>
<rectangle x1="-0.32" y1="-5.97" x2="0.32" y2="-4.17" layer="51"/>
<rectangle x1="-2.86" y1="-5.97" x2="-2.22" y2="-4.17" layer="51"/>
<smd name="1" x="-2.54" y="-5.08" dx="2.286" dy="0.762" layer="1" roundness="30" rot="R90"/>
<smd name="2" x="0" y="-5.08" dx="2.286" dy="0.762" layer="1" roundness="30" rot="R90"/>
<smd name="3" x="2.54" y="-5.08" dx="2.286" dy="0.762" layer="1" roundness="30" rot="R90"/>
<text x="0" y="0" size="0.8128" layer="25" ratio="15" align="center">&gt;NAME</text>
<text x="0" y="-2.54" size="0.8128" layer="21" ratio="15" align="center">&gt;LABEL</text>
</package>
<package name="S-HDR-M1X03">
<description>SMD Straight Male Header - 1X03
&lt;br&gt;
&lt;br&gt;
Created by: Wai Weng</description>
<text x="-1.27" y="2.921" size="0.762" layer="25" font="vector" ratio="15">&gt;NAME</text>
<smd name="1" x="0" y="-1.7" dx="2" dy="1" layer="1" roundness="50" rot="R90"/>
<smd name="2" x="2.54" y="1.7" dx="2" dy="1" layer="1" roundness="50" rot="R90"/>
<smd name="3" x="5.08" y="-1.7" dx="2" dy="1" layer="1" roundness="50" rot="R90"/>
<text x="-1.27" y="-3.81" size="0.762" layer="51" font="vector" ratio="15">&gt;LABEL</text>
<text x="-1.27" y="-3.81" size="0.762" layer="21" font="vector" ratio="15">&gt;LABEL</text>
<wire x1="-1.27" y1="-0.762" x2="-1.27" y2="0.762" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.762" x2="1.27" y2="0.762" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.762" x2="3.81" y2="0.762" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.762" x2="6.35" y2="0.762" width="0.2032" layer="21"/>
<wire x1="0.762" y1="1.27" x2="-0.762" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.842" y1="1.27" x2="4.318" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.302" y1="-1.27" x2="1.778" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.762" x2="-0.762" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-0.762" y1="-1.27" x2="-1.27" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="0.762" y1="-1.27" x2="1.27" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.762" x2="0.762" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.762" x2="1.778" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.762" x2="4.318" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.302" y1="-1.27" x2="3.81" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="5.842" y1="-1.27" x2="6.35" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="1.778" y1="-1.27" x2="1.27" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="3.81" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.762" x2="3.302" y2="1.27" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.762" x2="5.842" y2="1.27" width="0.2032" layer="21"/>
<rectangle x1="-0.32" y1="-0.32" x2="0.32" y2="0.32" layer="51"/>
<rectangle x1="2.22" y1="-0.32" x2="2.86" y2="0.32" layer="51"/>
<rectangle x1="4.76" y1="-0.32" x2="5.4" y2="0.32" layer="51"/>
<wire x1="-1.27" y1="-0.762" x2="-1.27" y2="0.762" width="0.2032" layer="51"/>
<wire x1="1.27" y1="-0.762" x2="1.27" y2="0.762" width="0.2032" layer="51"/>
<wire x1="3.81" y1="-0.762" x2="3.81" y2="0.762" width="0.2032" layer="51"/>
<wire x1="6.35" y1="-0.762" x2="6.35" y2="0.762" width="0.2032" layer="51"/>
<wire x1="0.762" y1="1.27" x2="-0.762" y2="1.27" width="0.2032" layer="51"/>
<wire x1="3.302" y1="1.27" x2="1.778" y2="1.27" width="0.2032" layer="51"/>
<wire x1="5.842" y1="1.27" x2="4.318" y2="1.27" width="0.2032" layer="51"/>
<wire x1="5.842" y1="-1.27" x2="4.318" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="3.302" y1="-1.27" x2="1.778" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="0.762" y1="-1.27" x2="-0.762" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="-1.27" y1="0.762" x2="-0.762" y2="1.27" width="0.2032" layer="51"/>
<wire x1="-0.762" y1="-1.27" x2="-1.27" y2="-0.762" width="0.2032" layer="51"/>
<wire x1="0.762" y1="-1.27" x2="1.27" y2="-0.762" width="0.2032" layer="51"/>
<wire x1="1.27" y1="0.762" x2="0.762" y2="1.27" width="0.2032" layer="51"/>
<wire x1="1.27" y1="0.762" x2="1.778" y2="1.27" width="0.2032" layer="51"/>
<wire x1="3.81" y1="0.762" x2="4.318" y2="1.27" width="0.2032" layer="51"/>
<wire x1="3.302" y1="-1.27" x2="3.81" y2="-0.762" width="0.2032" layer="51"/>
<wire x1="5.842" y1="-1.27" x2="6.35" y2="-0.762" width="0.2032" layer="51"/>
<wire x1="1.778" y1="-1.27" x2="1.27" y2="-0.762" width="0.2032" layer="51"/>
<wire x1="4.318" y1="-1.27" x2="3.81" y2="-0.762" width="0.2032" layer="51"/>
<wire x1="3.81" y1="0.762" x2="3.302" y2="1.27" width="0.2032" layer="51"/>
<wire x1="6.35" y1="0.762" x2="5.842" y2="1.27" width="0.2032" layer="51"/>
</package>
<package name="GROVE-04">
<description>SMD Grove Connector - 4 Ways&lt;br&gt;
Standard connector for Seeed Studio Grove System.&lt;br&gt;
&lt;br&gt;
Created by: Wai Weng</description>
<wire x1="6" y1="2.2" x2="6" y2="-2.9" width="0.2032" layer="51"/>
<wire x1="6" y1="-2.9" x2="3.85" y2="-2.9" width="0.2032" layer="51"/>
<wire x1="3.85" y1="-2.9" x2="3.85" y2="-2.2" width="0.2032" layer="51"/>
<rectangle x1="-1.25" y1="-4.1" x2="-0.75" y2="0" layer="51"/>
<rectangle x1="0.75" y1="-4.1" x2="1.25" y2="0" layer="51"/>
<smd name="P$2" x="5.5" y="0" dx="3" dy="1.6" layer="1" roundness="30" rot="R90"/>
<wire x1="3.85" y1="-2.2" x2="-3.85" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="-3.85" y1="-2.2" x2="-3.85" y2="-2.9" width="0.2032" layer="51"/>
<wire x1="-3.85" y1="-2.9" x2="-6" y2="-2.9" width="0.2032" layer="51"/>
<wire x1="-6" y1="-2.9" x2="-6" y2="2.2" width="0.2032" layer="51"/>
<smd name="1" x="3" y="-2.2" dx="4.6" dy="1" layer="1" roundness="50" rot="R90"/>
<rectangle x1="2.75" y1="-4.1" x2="3.25" y2="0" layer="51"/>
<rectangle x1="-3.25" y1="-4.1" x2="-2.75" y2="0" layer="51"/>
<smd name="P$1" x="-5.5" y="0" dx="3" dy="1.6" layer="1" roundness="30" rot="R90"/>
<rectangle x1="-6" y1="-1.1" x2="-5" y2="1.1" layer="51"/>
<smd name="2" x="1" y="-2.2" dx="4.6" dy="1" layer="1" roundness="50" rot="R90"/>
<smd name="3" x="-1" y="-2.2" dx="4.6" dy="1" layer="1" roundness="50" rot="R90"/>
<smd name="4" x="-3" y="-2.2" dx="4.6" dy="1" layer="1" roundness="50" rot="R90"/>
<text x="0" y="0.762" size="0.762" layer="25" font="vector" ratio="15" align="center">&gt;NAME</text>
<text x="7.366" y="-3.048" size="0.762" layer="21" font="vector" ratio="15" rot="R90">&gt;LABEL</text>
<wire x1="-6" y1="2.2" x2="6" y2="2.2" width="0.2032" layer="51"/>
<rectangle x1="5" y1="-1.1" x2="6" y2="1.1" layer="51"/>
<wire x1="-5.6" y1="1.9" x2="5.6" y2="1.9" width="0.2032" layer="21"/>
<wire x1="-5.6" y1="-1.8" x2="-5.6" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="-5.6" y1="-2.5" x2="-4.2" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="-4.2" y1="-2.5" x2="-4.2" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="-4.2" y1="-1.8" x2="-3.8" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="5.6" y1="-1.8" x2="5.6" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="5.6" y1="-2.5" x2="4.2" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="4.2" y1="-2.5" x2="4.2" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="4.2" y1="-1.8" x2="3.8" y2="-1.8" width="0.2032" layer="21"/>
</package>
<package name="DC_POWER_JACK">
<wire x1="-7.4" y1="4.5" x2="7.4" y2="4.5" width="0.2032" layer="51"/>
<wire x1="7.4" y1="4.5" x2="7.4" y2="-4.5" width="0.2032" layer="51"/>
<wire x1="7.4" y1="-4.5" x2="-7.4" y2="-4.5" width="0.2032" layer="51"/>
<wire x1="-7.4" y1="-4.5" x2="-7.4" y2="4.5" width="0.2032" layer="51"/>
<smd name="3" x="-2.4" y="6.0625" dx="3.125" dy="2.875" layer="1" rot="R90"/>
<smd name="4" x="3.7" y="6.0625" dx="3.125" dy="2.875" layer="1" rot="R90"/>
<smd name="2" x="3.7" y="-6.0625" dx="3.125" dy="2.875" layer="1" rot="R90"/>
<smd name="1" x="-2.4" y="-6.0625" dx="3.125" dy="2.875" layer="1" rot="R90"/>
<hole x="-2.4" y="0" drill="1.6"/>
<hole x="2.1" y="0" drill="1.8"/>
<wire x1="7" y1="4" x2="7" y2="-4" width="0.2032" layer="21"/>
<wire x1="7" y1="-4" x2="-4.46" y2="-4" width="0.2032" layer="21"/>
<wire x1="-4.46" y1="4" x2="7" y2="4" width="0.2032" layer="21"/>
<circle x="-2.4" y="0" radius="1" width="0.2032" layer="21"/>
<circle x="2.1" y="0" radius="1.1" width="0.2032" layer="21"/>
<wire x1="-3.81" y1="3.81" x2="6.858" y2="3.81" width="0.2032" layer="21"/>
<wire x1="6.858" y1="3.81" x2="6.858" y2="3.683" width="0.2032" layer="21"/>
<wire x1="6.858" y1="3.683" x2="6.858" y2="1.651" width="0.2032" layer="21"/>
<wire x1="-3.81" y1="3.683" x2="6.731" y2="3.683" width="0.2032" layer="21"/>
<wire x1="6.731" y1="3.683" x2="6.858" y2="3.683" width="0.2032" layer="21"/>
<wire x1="6.731" y1="3.683" x2="6.731" y2="1.651" width="0.2032" layer="21"/>
<wire x1="-7" y1="4" x2="-4.43" y2="4" width="0.2032" layer="51"/>
<wire x1="-7" y1="-4" x2="-7" y2="4" width="0.2032" layer="51"/>
<wire x1="-7" y1="-4.001" x2="-4.43" y2="-4.001" width="0.2032" layer="51"/>
</package>
<package name="DC_PLUG_PTH_T_B">
<wire x1="-7.493" y1="-4.445" x2="-7.493" y2="4.445" width="0.2032" layer="51"/>
<wire x1="-7.493" y1="4.445" x2="7.112" y2="4.445" width="0.2032" layer="51"/>
<pad name="1" x="0" y="0" drill="1" diameter="2" shape="long" rot="R90"/>
<pad name="3" x="3.048" y="-4.953" drill="1" diameter="2" shape="long" rot="R180"/>
<pad name="2" x="6.1976" y="0" drill="1" diameter="2" shape="long" rot="R90"/>
<wire x1="-7.493" y1="-4.445" x2="0.762" y2="-4.445" width="0.2032" layer="51"/>
<wire x1="7.112" y1="-4.445" x2="5.334" y2="-4.445" width="0.2032" layer="51"/>
<wire x1="7.112" y1="-4.445" x2="7.112" y2="-2.032" width="0.2032" layer="51"/>
<wire x1="7.112" y1="4.445" x2="7.112" y2="2.032" width="0.2032" layer="51"/>
<wire x1="-0.5" y1="1" x2="-0.5" y2="-1" width="0" layer="46"/>
<wire x1="0.5" y1="1" x2="0.5" y2="-1" width="0" layer="46"/>
<wire x1="-0.5" y1="-1" x2="0" y2="-1.5" width="0" layer="46" curve="90"/>
<wire x1="0" y1="-1.5" x2="0.5" y2="-1" width="0" layer="46" curve="90"/>
<wire x1="0.5" y1="1" x2="0" y2="1.5" width="0" layer="46" curve="90"/>
<wire x1="0" y1="1.5" x2="-0.5" y2="1" width="0" layer="46" curve="90"/>
<wire x1="5.6976" y1="1" x2="5.6976" y2="-1" width="0" layer="46"/>
<wire x1="6.6976" y1="1" x2="6.6976" y2="-1" width="0" layer="46"/>
<wire x1="5.6976" y1="-1" x2="6.1976" y2="-1.5" width="0" layer="46" curve="90"/>
<wire x1="6.1976" y1="-1.5" x2="6.6976" y2="-1" width="0" layer="46" curve="90"/>
<wire x1="6.6976" y1="1" x2="6.1976" y2="1.5" width="0" layer="46" curve="90"/>
<wire x1="6.1976" y1="1.5" x2="5.6976" y2="1" width="0" layer="46" curve="90"/>
<wire x1="4.048" y1="-5.453" x2="2.048" y2="-5.453" width="0" layer="46"/>
<wire x1="4.048" y1="-4.453" x2="2.048" y2="-4.453" width="0" layer="46"/>
<wire x1="2.048" y1="-5.453" x2="1.548" y2="-4.953" width="0" layer="46" curve="-90"/>
<wire x1="1.548" y1="-4.953" x2="2.048" y2="-4.453" width="0" layer="46" curve="-90"/>
<wire x1="4.048" y1="-4.453" x2="4.548" y2="-4.953" width="0" layer="46" curve="-90"/>
<wire x1="4.548" y1="-4.953" x2="4.048" y2="-5.453" width="0" layer="46" curve="-90"/>
<text x="-6.35" y="2.54" size="0.762" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-6.35" y="1.27" size="0.762" layer="51" font="vector" ratio="15">&gt;LABEL</text>
<text x="-6.35" y="1.27" size="0.762" layer="21" font="vector" ratio="15">&gt;LABEL</text>
<pad name="4" x="3.048" y="4.953" drill="1" diameter="2" shape="long"/>
<wire x1="4.048" y1="4.497" x2="2.048" y2="4.497" width="0" layer="46"/>
<wire x1="4.048" y1="5.497" x2="2.048" y2="5.497" width="0" layer="46"/>
<wire x1="2.048" y1="4.497" x2="1.548" y2="4.997" width="0" layer="46" curve="-90"/>
<wire x1="1.548" y1="4.997" x2="2.048" y2="5.497" width="0" layer="46" curve="-90"/>
<wire x1="4.048" y1="5.497" x2="4.548" y2="4.997" width="0" layer="46" curve="-90"/>
<wire x1="4.548" y1="4.997" x2="4.048" y2="4.497" width="0" layer="46" curve="-90"/>
</package>
<package name="DC_PLUG_PTH">
<wire x1="-10.033" y1="-4.445" x2="-10.033" y2="4.445" width="0.2032" layer="51"/>
<wire x1="-10.033" y1="4.445" x2="4.572" y2="4.445" width="0.2032" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1" diameter="2" shape="long" rot="R90"/>
<pad name="3" x="0.508" y="-4.953" drill="1" diameter="2" shape="long" rot="R180"/>
<pad name="2" x="3.6576" y="0" drill="1" diameter="2" shape="long" rot="R90"/>
<wire x1="-10.033" y1="-4.445" x2="-1.778" y2="-4.445" width="0.2032" layer="51"/>
<wire x1="4.572" y1="-4.445" x2="2.794" y2="-4.445" width="0.2032" layer="51"/>
<wire x1="4.572" y1="-4.445" x2="4.572" y2="-2.032" width="0.2032" layer="51"/>
<wire x1="4.572" y1="4.445" x2="4.572" y2="2.032" width="0.2032" layer="51"/>
<wire x1="-3.04" y1="1" x2="-3.04" y2="-1" width="0" layer="46"/>
<wire x1="-2.04" y1="1" x2="-2.04" y2="-1" width="0" layer="46"/>
<wire x1="-3.04" y1="-1" x2="-2.54" y2="-1.5" width="0" layer="46" curve="90"/>
<wire x1="-2.54" y1="-1.5" x2="-2.04" y2="-1" width="0" layer="46" curve="90"/>
<wire x1="-2.04" y1="1" x2="-2.54" y2="1.5" width="0" layer="46" curve="90"/>
<wire x1="-2.54" y1="1.5" x2="-3.04" y2="1" width="0" layer="46" curve="90"/>
<wire x1="3.1576" y1="1" x2="3.1576" y2="-1" width="0" layer="46"/>
<wire x1="4.1576" y1="1" x2="4.1576" y2="-1" width="0" layer="46"/>
<wire x1="3.1576" y1="-1" x2="3.6576" y2="-1.5" width="0" layer="46" curve="90"/>
<wire x1="3.6576" y1="-1.5" x2="4.1576" y2="-1" width="0" layer="46" curve="90"/>
<wire x1="4.1576" y1="1" x2="3.6576" y2="1.5" width="0" layer="46" curve="90"/>
<wire x1="3.6576" y1="1.5" x2="3.1576" y2="1" width="0" layer="46" curve="90"/>
<wire x1="1.508" y1="-5.453" x2="-0.492" y2="-5.453" width="0" layer="46"/>
<wire x1="1.508" y1="-4.453" x2="-0.492" y2="-4.453" width="0" layer="46"/>
<wire x1="-0.492" y1="-5.453" x2="-0.992" y2="-4.953" width="0" layer="46" curve="-90"/>
<wire x1="-0.992" y1="-4.953" x2="-0.492" y2="-4.453" width="0" layer="46" curve="-90"/>
<wire x1="1.508" y1="-4.453" x2="2.008" y2="-4.953" width="0" layer="46" curve="-90"/>
<wire x1="2.008" y1="-4.953" x2="1.508" y2="-5.453" width="0" layer="46" curve="-90"/>
<text x="-8.89" y="2.54" size="0.762" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-8.89" y="1.27" size="0.762" layer="21" font="vector" ratio="15">&gt;LABEL</text>
</package>
<package name="HDR-F2X04">
<description>Female Header - 2X04</description>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="2" x="0" y="2.54" drill="1.016" diameter="1.778" rot="R90"/>
<text x="-1.27" y="4.191" size="0.762" layer="25" font="vector" ratio="15">&gt;NAME</text>
<wire x1="-1.27" y1="3.81" x2="8.89" y2="3.81" width="0.2032" layer="21"/>
<wire x1="8.89" y1="3.81" x2="8.89" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-1.27" x2="-1.27" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="3.81" width="0.2032" layer="21"/>
<text x="-1.27" y="-1.651" size="0.762" layer="21" font="vector" ratio="15" align="top-left">&gt;LABEL</text>
<text x="-1.27" y="-1.651" size="0.762" layer="51" font="vector" ratio="15" align="top-left">&gt;LABEL</text>
<pad name="3" x="2.54" y="0" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="4" x="2.54" y="2.54" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="5" x="5.08" y="0" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="6" x="5.08" y="2.54" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="7" x="7.62" y="0" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="8" x="7.62" y="2.54" drill="1.016" diameter="1.778" rot="R90"/>
<wire x1="-1.27" y1="3.81" x2="8.89" y2="3.81" width="0.2032" layer="51"/>
<wire x1="8.89" y1="3.81" x2="8.89" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="8.89" y1="-1.27" x2="-1.27" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="3.81" width="0.2032" layer="51"/>
</package>
<package name="HDR-M2X04">
<description>Male Header - 2X04</description>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="2" x="0" y="2.54" drill="1.016" diameter="1.778" rot="R90"/>
<text x="-1.27" y="4.191" size="0.762" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-1.27" y="-1.651" size="0.762" layer="21" font="vector" ratio="15" align="top-left">&gt;LABEL</text>
<text x="-1.27" y="-1.651" size="0.762" layer="51" font="vector" ratio="15" align="top-left">&gt;LABEL</text>
<pad name="3" x="2.54" y="0" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="4" x="2.54" y="2.54" drill="1.016" diameter="1.778" rot="R90"/>
<wire x1="-0.762" y1="3.81" x2="0.762" y2="3.81" width="0.2032" layer="51"/>
<wire x1="0.762" y1="3.81" x2="1.27" y2="3.302" width="0.2032" layer="51"/>
<wire x1="1.27" y1="3.302" x2="1.778" y2="3.81" width="0.2032" layer="51"/>
<wire x1="1.778" y1="3.81" x2="3.302" y2="3.81" width="0.2032" layer="51"/>
<wire x1="3.302" y1="3.81" x2="3.81" y2="3.302" width="0.2032" layer="51"/>
<wire x1="3.81" y1="-0.762" x2="3.302" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="3.302" y1="-1.27" x2="1.778" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="1.778" y1="-1.27" x2="1.27" y2="-0.762" width="0.2032" layer="51"/>
<wire x1="1.27" y1="-0.762" x2="0.762" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="0.762" y1="-1.27" x2="-0.762" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="-0.762" y1="-1.27" x2="-1.27" y2="-0.762" width="0.2032" layer="51"/>
<wire x1="-1.27" y1="-0.762" x2="-1.27" y2="3.302" width="0.2032" layer="51"/>
<wire x1="-1.27" y1="3.302" x2="-0.762" y2="3.81" width="0.2032" layer="51"/>
<pad name="5" x="5.08" y="0" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="6" x="5.08" y="2.54" drill="1.016" diameter="1.778" rot="R90"/>
<wire x1="3.81" y1="3.302" x2="4.318" y2="3.81" width="0.2032" layer="51"/>
<wire x1="4.318" y1="3.81" x2="5.842" y2="3.81" width="0.2032" layer="51"/>
<wire x1="5.842" y1="3.81" x2="6.35" y2="3.302" width="0.2032" layer="51"/>
<wire x1="6.35" y1="-0.762" x2="5.842" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="5.842" y1="-1.27" x2="4.318" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="4.318" y1="-1.27" x2="3.81" y2="-0.762" width="0.2032" layer="51"/>
<pad name="7" x="7.62" y="0" drill="1.016" diameter="1.778" rot="R90"/>
<pad name="8" x="7.62" y="2.54" drill="1.016" diameter="1.778" rot="R90"/>
<wire x1="6.35" y1="3.302" x2="6.858" y2="3.81" width="0.2032" layer="51"/>
<wire x1="6.858" y1="3.81" x2="8.382" y2="3.81" width="0.2032" layer="51"/>
<wire x1="8.382" y1="3.81" x2="8.89" y2="3.302" width="0.2032" layer="51"/>
<wire x1="8.89" y1="3.302" x2="8.89" y2="-0.762" width="0.2032" layer="51"/>
<wire x1="8.89" y1="-0.762" x2="8.382" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="8.382" y1="-1.27" x2="6.858" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="6.858" y1="-1.27" x2="6.35" y2="-0.762" width="0.2032" layer="51"/>
<wire x1="1.27" y1="3.302" x2="1.27" y2="-0.762" width="0.2032" layer="51"/>
<wire x1="3.81" y1="3.302" x2="3.81" y2="-0.762" width="0.2032" layer="51"/>
<wire x1="6.35" y1="3.302" x2="6.35" y2="-0.762" width="0.2032" layer="51"/>
<rectangle x1="-0.32" y1="-0.32" x2="0.32" y2="0.32" layer="51"/>
<rectangle x1="2.22" y1="-0.32" x2="2.86" y2="0.32" layer="51"/>
<rectangle x1="4.76" y1="-0.32" x2="5.4" y2="0.32" layer="51"/>
<rectangle x1="7.3" y1="-0.32" x2="7.94" y2="0.32" layer="51"/>
<rectangle x1="7.3" y1="2.22" x2="7.94" y2="2.86" layer="51"/>
<rectangle x1="4.76" y1="2.22" x2="5.4" y2="2.86" layer="51"/>
<rectangle x1="2.22" y1="2.22" x2="2.86" y2="2.86" layer="51"/>
<rectangle x1="-0.32" y1="2.22" x2="0.32" y2="2.86" layer="51"/>
<wire x1="-0.762" y1="3.81" x2="0.762" y2="3.81" width="0.2032" layer="21"/>
<wire x1="0.762" y1="3.81" x2="1.27" y2="3.302" width="0.2032" layer="21"/>
<wire x1="1.27" y1="3.302" x2="1.778" y2="3.81" width="0.2032" layer="21"/>
<wire x1="1.778" y1="3.81" x2="3.302" y2="3.81" width="0.2032" layer="21"/>
<wire x1="3.302" y1="3.81" x2="3.81" y2="3.302" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.762" x2="3.302" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.302" y1="-1.27" x2="1.778" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.778" y1="-1.27" x2="1.27" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.762" x2="0.762" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.762" y1="-1.27" x2="-0.762" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.762" y1="-1.27" x2="-1.27" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.762" x2="-1.27" y2="3.302" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="3.302" x2="-0.762" y2="3.81" width="0.2032" layer="21"/>
<wire x1="3.81" y1="3.302" x2="4.318" y2="3.81" width="0.2032" layer="21"/>
<wire x1="4.318" y1="3.81" x2="5.842" y2="3.81" width="0.2032" layer="21"/>
<wire x1="5.842" y1="3.81" x2="6.35" y2="3.302" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.762" x2="5.842" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.842" y1="-1.27" x2="4.318" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="3.81" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="6.35" y1="3.302" x2="6.858" y2="3.81" width="0.2032" layer="21"/>
<wire x1="6.858" y1="3.81" x2="8.382" y2="3.81" width="0.2032" layer="21"/>
<wire x1="8.382" y1="3.81" x2="8.89" y2="3.302" width="0.2032" layer="21"/>
<wire x1="8.89" y1="3.302" x2="8.89" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.762" x2="8.382" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.382" y1="-1.27" x2="6.858" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="6.858" y1="-1.27" x2="6.35" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="1.27" y1="3.302" x2="1.27" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="3.81" y1="3.302" x2="3.81" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="6.35" y1="3.302" x2="6.35" y2="-0.762" width="0.2032" layer="21"/>
</package>
<package name="S-HDR-F2X04-LP">
<description>SMD Female Header (Low Profile) - 2X04&lt;br&gt;
Height: 3.5mm&lt;br&gt;
&lt;br&gt;
Created by: Wai Weng</description>
<text x="0" y="4.699" size="0.762" layer="25" ratio="15" rot="SR0" align="center">&gt;NAME</text>
<hole x="3.81" y="1.27" drill="1.05"/>
<hole x="3.81" y="-1.27" drill="1.05"/>
<hole x="1.27" y="1.27" drill="1.05"/>
<hole x="1.27" y="-1.27" drill="1.05"/>
<hole x="-1.27" y="1.27" drill="1.05"/>
<hole x="-1.27" y="-1.27" drill="1.05"/>
<hole x="-3.81" y="1.27" drill="1.05"/>
<hole x="-3.81" y="-1.27" drill="1.05"/>
<hole x="-2.54" y="0" drill="1.8"/>
<hole x="2.54" y="0" drill="1.8"/>
<smd name="1" x="-3.81" y="-3.048" dx="1.778" dy="1.016" layer="1" roundness="50" rot="R270"/>
<wire x1="-5.08" y1="-2.54" x2="-5.08" y2="2.54" width="0.2032" layer="51"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="2.54" width="0.2032" layer="51"/>
<wire x1="-5.08" y1="2.54" x2="5.08" y2="2.54" width="0.2032" layer="51"/>
<wire x1="-5.08" y1="-2.54" x2="5.08" y2="-2.54" width="0.2032" layer="51"/>
<smd name="2" x="-3.81" y="3.048" dx="1.778" dy="1.016" layer="1" roundness="50" rot="R270"/>
<smd name="3" x="-1.27" y="-3.048" dx="1.778" dy="1.016" layer="1" roundness="50" rot="R270"/>
<smd name="4" x="-1.27" y="3.048" dx="1.778" dy="1.016" layer="1" roundness="50" rot="R270"/>
<smd name="5" x="1.27" y="-3.048" dx="1.778" dy="1.016" layer="1" roundness="50" rot="R270"/>
<smd name="6" x="1.27" y="3.048" dx="1.778" dy="1.016" layer="1" roundness="50" rot="R270"/>
<smd name="7" x="3.81" y="-3.048" dx="1.778" dy="1.016" layer="1" roundness="50" rot="R270"/>
<smd name="8" x="3.81" y="3.048" dx="1.778" dy="1.016" layer="1" roundness="50" rot="R270"/>
<text x="0" y="-4.953" size="0.762" layer="21" ratio="15" rot="SR0" align="center">&gt;LABEL</text>
<text x="-3.81" y="1.27" size="0.254" layer="46" font="vector" ratio="15" align="center">NPTH</text>
<text x="-1.27" y="1.27" size="0.254" layer="46" font="vector" ratio="15" align="center">NPTH</text>
<text x="1.27" y="1.27" size="0.254" layer="46" font="vector" ratio="15" align="center">NPTH</text>
<text x="3.81" y="1.27" size="0.254" layer="46" font="vector" ratio="15" align="center">NPTH</text>
<text x="2.54" y="0" size="0.254" layer="46" font="vector" ratio="15" align="center">NPTH</text>
<text x="3.81" y="-1.27" size="0.254" layer="46" font="vector" ratio="15" align="center">NPTH</text>
<text x="1.27" y="-1.27" size="0.254" layer="46" font="vector" ratio="15" align="center">NPTH</text>
<text x="-1.27" y="-1.27" size="0.254" layer="46" font="vector" ratio="15" align="center">NPTH</text>
<text x="-3.81" y="-1.27" size="0.254" layer="46" font="vector" ratio="15" align="center">NPTH</text>
<text x="-2.54" y="0" size="0.254" layer="46" font="vector" ratio="15" align="center">NPTH</text>
<wire x1="-4.572" y1="2.54" x2="-5.08" y2="2.54" width="0.2032" layer="21"/>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="-5.08" y1="-2.54" x2="-4.572" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="4.572" y1="2.54" x2="5.08" y2="2.54" width="0.2032" layer="21"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="5.08" y1="-2.54" x2="4.572" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="-3.048" y1="2.54" x2="-2.032" y2="2.54" width="0.2032" layer="21"/>
<wire x1="-0.508" y1="2.54" x2="0.508" y2="2.54" width="0.2032" layer="21"/>
<wire x1="2.032" y1="2.54" x2="3.048" y2="2.54" width="0.2032" layer="21"/>
<wire x1="-3.048" y1="-2.54" x2="-2.032" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="-0.508" y1="-2.54" x2="0.508" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="2.032" y1="-2.54" x2="3.048" y2="-2.54" width="0.2032" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="1X02">
<wire x1="3.81" y1="-2.54" x2="-2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<text x="-2.54" y="5.842" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="1" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="2" x="7.62" y="0" visible="pad" length="middle" direction="pas" rot="R180"/>
<text x="-2.54" y="-3.302" size="1.778" layer="97" font="vector" align="top-left">&gt;LABEL</text>
</symbol>
<symbol name="CN-MICROBIT">
<pin name="P3/AN/COL1@1" x="-20.32" y="22.86" visible="pin" length="short"/>
<pin name="P0/AN@2" x="-20.32" y="20.32" visible="pin" length="short"/>
<pin name="P0/AN@3" x="-20.32" y="17.78" visible="pin" length="short"/>
<pin name="P0/AN@4" x="-20.32" y="15.24" visible="pin" length="short"/>
<pin name="P0/AN@5" x="-20.32" y="12.7" visible="pin" length="short"/>
<pin name="P4/AN/COL2@6" x="-20.32" y="10.16" visible="pin" length="short"/>
<pin name="P5/BTNA@7" x="-20.32" y="7.62" visible="pin" length="short"/>
<pin name="P6/COL9@8" x="-20.32" y="5.08" visible="pin" length="short"/>
<pin name="P7/COL8@9" x="-20.32" y="2.54" visible="pin" length="short"/>
<pin name="P1/AN@10" x="-20.32" y="0" visible="pin" length="short"/>
<pin name="P1/AN@11" x="-20.32" y="-2.54" visible="pin" length="short"/>
<pin name="P1/AN@12" x="-20.32" y="-5.08" visible="pin" length="short"/>
<pin name="P1/AN@13" x="-20.32" y="-7.62" visible="pin" length="short"/>
<pin name="P8@14" x="-20.32" y="-10.16" visible="pin" length="short"/>
<pin name="P9/COL7@15" x="-20.32" y="-12.7" visible="pin" length="short"/>
<pin name="P10/AN/COL3@16" x="-20.32" y="-15.24" visible="pin" length="short"/>
<pin name="P11/BTNB@17" x="-20.32" y="-17.78" visible="pin" length="short"/>
<pin name="P12/RESERVE@18" x="-20.32" y="-20.32" visible="pin" length="short"/>
<pin name="P2/AN@19" x="-20.32" y="-22.86" visible="pin" length="short"/>
<pin name="P2/AN@20" x="-20.32" y="-25.4" visible="pin" length="short"/>
<pin name="P2/AN@21" x="20.32" y="-25.4" visible="pin" length="short" rot="R180"/>
<pin name="P2/AN@22" x="20.32" y="-22.86" visible="pin" length="short" rot="R180"/>
<pin name="P13/SCK@23" x="20.32" y="-20.32" visible="pin" length="short" rot="R180"/>
<pin name="P14/MISO@24" x="20.32" y="-17.78" visible="pin" length="short" rot="R180"/>
<pin name="P15/MOSI@25" x="20.32" y="-15.24" visible="pin" length="short" rot="R180"/>
<pin name="P16@26" x="20.32" y="-12.7" visible="pin" length="short" rot="R180"/>
<pin name="3V3@27" x="20.32" y="-10.16" visible="pin" length="short" rot="R180"/>
<pin name="3V3@28" x="20.32" y="-7.62" visible="pin" length="short" rot="R180"/>
<pin name="3V3@29" x="20.32" y="-5.08" visible="pin" length="short" rot="R180"/>
<pin name="3V3@30" x="20.32" y="-2.54" visible="pin" length="short" rot="R180"/>
<pin name="3V3@31" x="20.32" y="0" visible="pin" length="short" rot="R180"/>
<pin name="3V3@32" x="20.32" y="2.54" visible="pin" length="short" rot="R180"/>
<pin name="P19/SCL@33" x="20.32" y="5.08" visible="pin" length="short" rot="R180"/>
<pin name="P20/SDA@34" x="20.32" y="7.62" visible="pin" length="short" rot="R180"/>
<pin name="GND@35" x="20.32" y="10.16" visible="pin" length="short" rot="R180"/>
<pin name="GND@36" x="20.32" y="12.7" visible="pin" length="short" rot="R180"/>
<pin name="GND@37" x="20.32" y="15.24" visible="pin" length="short" rot="R180"/>
<pin name="GND@38" x="20.32" y="17.78" visible="pin" length="short" rot="R180"/>
<pin name="GND@39" x="20.32" y="20.32" visible="pin" length="short" rot="R180"/>
<pin name="GND@40" x="20.32" y="22.86" visible="pin" length="short" rot="R180"/>
<wire x1="-17.78" y1="25.4" x2="17.78" y2="25.4" width="0.254" layer="94"/>
<wire x1="17.78" y1="25.4" x2="17.78" y2="-27.94" width="0.254" layer="94"/>
<wire x1="17.78" y1="-27.94" x2="-17.78" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-27.94" x2="-17.78" y2="25.4" width="0.254" layer="94"/>
<text x="-17.78" y="26.162" size="1.778" layer="95">&gt;NAME</text>
<text x="-17.78" y="-30.48" size="1.778" layer="96">&gt;LABEL</text>
</symbol>
<symbol name="1X04">
<wire x1="3.81" y1="-5.08" x2="-2.54" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="7.62" x2="-2.54" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<text x="-2.54" y="8.382" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="1" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="2" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="3" x="7.62" y="0" visible="pad" length="middle" direction="pas" rot="R180"/>
<text x="-2.54" y="-5.842" size="1.778" layer="97" font="vector" align="top-left">&gt;LABEL</text>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<pin name="4" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" rot="R180"/>
</symbol>
<symbol name="AUDIO_JACK">
<wire x1="-1.27" y1="-2.54" x2="0" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.27" x2="1.27" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-1.27" x2="-3.81" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="-5.08" y2="2.54" width="0.1524" layer="94"/>
<text x="-5.08" y="3.048" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-5.08" size="1.778" layer="96">&gt;LABEL</text>
<rectangle x1="-7.62" y1="-0.762" x2="-2.54" y2="0.762" layer="94" rot="R90"/>
<pin name="R" x="5.08" y="0" visible="off" length="short" rot="R180"/>
<pin name="T" x="5.08" y="-2.54" visible="off" length="short" rot="R180"/>
<pin name="S" x="5.08" y="2.54" visible="off" length="short" rot="R180"/>
</symbol>
<symbol name="ICSP-5PIN">
<wire x1="5.08" y1="-7.62" x2="-5.08" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="-5.08" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="5.08" y1="-7.62" x2="5.08" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="5.08" y2="7.62" width="0.4064" layer="94"/>
<text x="-5.08" y="8.382" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="VPP" x="7.62" y="5.08" length="short" direction="pas" rot="R180"/>
<text x="-5.08" y="-8.382" size="1.778" layer="97" font="vector" align="top-left">&gt;LABEL</text>
<pin name="VDD" x="7.62" y="2.54" length="short" direction="pas" rot="R180"/>
<pin name="VSS" x="7.62" y="0" length="short" direction="pas" rot="R180"/>
<pin name="PGD" x="7.62" y="-2.54" length="short" direction="pas" rot="R180"/>
<pin name="PGC" x="7.62" y="-5.08" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="1X03">
<wire x1="3.81" y1="-5.08" x2="-2.54" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<text x="-2.54" y="5.842" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="1" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="2" x="7.62" y="0" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="3" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" rot="R180"/>
<text x="-2.54" y="-5.842" size="1.778" layer="97" font="vector" align="top-left">&gt;LABEL</text>
</symbol>
<symbol name="DC_POWER_JACK">
<wire x1="-7.62" y1="-2.54" x2="-5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="0" x2="-2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<rectangle x1="-7.62" y1="1.778" x2="2.54" y2="3.302" layer="94"/>
<pin name="GNDBREAK" x="5.08" y="0" visible="off" length="short" rot="R180"/>
<pin name="GND" x="5.08" y="-2.54" visible="off" length="short" rot="R180"/>
<pin name="PWR" x="5.08" y="2.54" visible="off" length="short" rot="R180"/>
<text x="-7.62" y="3.81" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-5.08" size="1.778" layer="96">&gt;LABEL</text>
</symbol>
<symbol name="2X04">
<wire x1="3.81" y1="-5.08" x2="-3.81" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="-2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-3.81" y1="7.62" x2="-3.81" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<text x="-3.81" y="8.382" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="1" x="-7.62" y="5.08" visible="pad" length="middle" direction="pas"/>
<pin name="2" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" rot="R180"/>
<text x="-3.81" y="-5.842" size="1.778" layer="97" font="vector" align="top-left">&gt;LABEL</text>
<wire x1="-1.27" y1="2.54" x2="-2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<pin name="3" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas"/>
<pin name="4" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" rot="R180"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<pin name="5" x="-7.62" y="0" visible="pad" length="middle" direction="pas"/>
<pin name="6" x="7.62" y="0" visible="pad" length="middle" direction="pas" rot="R180"/>
<wire x1="-1.27" y1="-2.54" x2="-2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<pin name="7" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas"/>
<pin name="8" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="TERMINAL-BLOCK-02" prefix="JP">
<description>Terminal Block - 2 Ways</description>
<gates>
<gate name="G$1" symbol="1X02" x="0" y="0"/>
</gates>
<devices>
<device name="_(KAR301)" package="TERMINAL-KAR301-02">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="CN-TB-KAR-2" constant="no"/>
<attribute name="DESC" value="Terminal Block - KAR301" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="2 Ways" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(KF127)" package="TERMINAL-KF127-2P">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="_BLACK">
<attribute name="CYTRON-PC" value="CN-TB-KF127-2P-BLK" constant="no"/>
<attribute name="DESC" value="Terminal Block KF127 2pin Black" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="2 Ways Black" constant="no"/>
</technology>
<technology name="_GREEN">
<attribute name="CYTRON-PC" value="CN-TB-KF127-2P" constant="no"/>
<attribute name="DESC" value="Terminal Block KF127 2pin Green" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="2 Ways Green" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(HB9500)" package="TERMINAL-HB9500-02">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="CN-TB-9502" constant="no"/>
<attribute name="DESC" value="Screw Board Terminal Block-2 Ways" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="2 Ways" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(KF139)" package="KF139-19.0">
<connects>
<connect gate="G$1" pin="1" pad="1A 1B 1C 1D"/>
<connect gate="G$1" pin="2" pad="2A 2B 2C 2D"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_(KF136HT)" package="KF136HT-10.16">
<connects>
<connect gate="G$1" pin="1" pad="1A 1B"/>
<connect gate="G$1" pin="2" pad="2A 2B"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_(KF127-MD)" package="TERMINAL-KF127-2P-MD">
<connects>
<connect gate="G$1" pin="1" pad="1 1-1 1-2"/>
<connect gate="G$1" pin="2" pad="2 2-1 2-2"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="CN-TB-KF127-2P" constant="no"/>
<attribute name="DESC" value="Terminal Block - KF127" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="2 Ways" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(KF127-MD-HALFHOLE)" package="TERMINAL-KF127-2P-MD-HALFHOLE">
<connects>
<connect gate="G$1" pin="1" pad="1 1-1 1-2 1-3 1-4"/>
<connect gate="G$1" pin="2" pad="2 2-1 2-2 2-3 2-4"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="CN-TB-KF127-2P" constant="no"/>
<attribute name="DESC" value="Terminal Block - KF127" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="2 Ways" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(SPTAF_5.0)" package="SPTAF_1/2-5.0">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="PCB Terminal Block Push IN 2 ways 5mm pitch" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="MF" value="Phoenix Contact" constant="no"/>
<attribute name="MPN" value="SPTAF 1/2-5,0-LL" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(2060)" package="TERMINAL-2060-2P">
<connects>
<connect gate="G$1" pin="1" pad="F1 R1"/>
<connect gate="G$1" pin="2" pad="F2 R2"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="S Terminal Block 2060 2 Ways" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="2060-2P" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="2 Ways" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(DG308)" package="TERMINAL-DG308-2P">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CN-MICROBIT" prefix="CON">
<gates>
<gate name="G$1" symbol="CN-MICROBIT" x="0" y="0"/>
</gates>
<devices>
<device name="_(CE16R)" package="CN-CE16R-80">
<connects>
<connect gate="G$1" pin="3V3@27" pad="27"/>
<connect gate="G$1" pin="3V3@28" pad="28"/>
<connect gate="G$1" pin="3V3@29" pad="29"/>
<connect gate="G$1" pin="3V3@30" pad="30"/>
<connect gate="G$1" pin="3V3@31" pad="31"/>
<connect gate="G$1" pin="3V3@32" pad="32"/>
<connect gate="G$1" pin="GND@35" pad="35"/>
<connect gate="G$1" pin="GND@36" pad="36"/>
<connect gate="G$1" pin="GND@37" pad="37"/>
<connect gate="G$1" pin="GND@38" pad="38"/>
<connect gate="G$1" pin="GND@39" pad="39"/>
<connect gate="G$1" pin="GND@40" pad="40"/>
<connect gate="G$1" pin="P0/AN@2" pad="2"/>
<connect gate="G$1" pin="P0/AN@3" pad="3"/>
<connect gate="G$1" pin="P0/AN@4" pad="4"/>
<connect gate="G$1" pin="P0/AN@5" pad="5"/>
<connect gate="G$1" pin="P1/AN@10" pad="10"/>
<connect gate="G$1" pin="P1/AN@11" pad="11"/>
<connect gate="G$1" pin="P1/AN@12" pad="12"/>
<connect gate="G$1" pin="P1/AN@13" pad="13"/>
<connect gate="G$1" pin="P10/AN/COL3@16" pad="16"/>
<connect gate="G$1" pin="P11/BTNB@17" pad="17"/>
<connect gate="G$1" pin="P12/RESERVE@18" pad="18"/>
<connect gate="G$1" pin="P13/SCK@23" pad="23"/>
<connect gate="G$1" pin="P14/MISO@24" pad="24"/>
<connect gate="G$1" pin="P15/MOSI@25" pad="25"/>
<connect gate="G$1" pin="P16@26" pad="26"/>
<connect gate="G$1" pin="P19/SCL@33" pad="33"/>
<connect gate="G$1" pin="P2/AN@19" pad="19"/>
<connect gate="G$1" pin="P2/AN@20" pad="20"/>
<connect gate="G$1" pin="P2/AN@21" pad="21"/>
<connect gate="G$1" pin="P2/AN@22" pad="22"/>
<connect gate="G$1" pin="P20/SDA@34" pad="34"/>
<connect gate="G$1" pin="P3/AN/COL1@1" pad="1"/>
<connect gate="G$1" pin="P4/AN/COL2@6" pad="6"/>
<connect gate="G$1" pin="P5/BTNA@7" pad="7"/>
<connect gate="G$1" pin="P6/COL9@8" pad="8"/>
<connect gate="G$1" pin="P7/COL8@9" pad="9"/>
<connect gate="G$1" pin="P8@14" pad="14"/>
<connect gate="G$1" pin="P9/COL7@15" pad="15"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="CN-CE16R-80" constant="no"/>
<attribute name="DESC" value="Microbit Right Angle Connector" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="40 Ways" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(PCIRSM127)" package="S-CN-MICROBIT-RA">
<connects>
<connect gate="G$1" pin="3V3@27" pad="27"/>
<connect gate="G$1" pin="3V3@28" pad="28"/>
<connect gate="G$1" pin="3V3@29" pad="29"/>
<connect gate="G$1" pin="3V3@30" pad="30"/>
<connect gate="G$1" pin="3V3@31" pad="31"/>
<connect gate="G$1" pin="3V3@32" pad="32"/>
<connect gate="G$1" pin="GND@35" pad="35"/>
<connect gate="G$1" pin="GND@36" pad="36"/>
<connect gate="G$1" pin="GND@37" pad="37"/>
<connect gate="G$1" pin="GND@38" pad="38"/>
<connect gate="G$1" pin="GND@39" pad="39"/>
<connect gate="G$1" pin="GND@40" pad="40"/>
<connect gate="G$1" pin="P0/AN@2" pad="2"/>
<connect gate="G$1" pin="P0/AN@3" pad="3"/>
<connect gate="G$1" pin="P0/AN@4" pad="4"/>
<connect gate="G$1" pin="P0/AN@5" pad="5"/>
<connect gate="G$1" pin="P1/AN@10" pad="10"/>
<connect gate="G$1" pin="P1/AN@11" pad="11"/>
<connect gate="G$1" pin="P1/AN@12" pad="12"/>
<connect gate="G$1" pin="P1/AN@13" pad="13"/>
<connect gate="G$1" pin="P10/AN/COL3@16" pad="16"/>
<connect gate="G$1" pin="P11/BTNB@17" pad="17"/>
<connect gate="G$1" pin="P12/RESERVE@18" pad="18"/>
<connect gate="G$1" pin="P13/SCK@23" pad="23"/>
<connect gate="G$1" pin="P14/MISO@24" pad="24"/>
<connect gate="G$1" pin="P15/MOSI@25" pad="25"/>
<connect gate="G$1" pin="P16@26" pad="26"/>
<connect gate="G$1" pin="P19/SCL@33" pad="33"/>
<connect gate="G$1" pin="P2/AN@19" pad="19"/>
<connect gate="G$1" pin="P2/AN@20" pad="20"/>
<connect gate="G$1" pin="P2/AN@21" pad="21"/>
<connect gate="G$1" pin="P2/AN@22" pad="22"/>
<connect gate="G$1" pin="P20/SDA@34" pad="34"/>
<connect gate="G$1" pin="P3/AN/COL1@1" pad="1"/>
<connect gate="G$1" pin="P4/AN/COL2@6" pad="6"/>
<connect gate="G$1" pin="P5/BTNA@7" pad="7"/>
<connect gate="G$1" pin="P6/COL9@8" pad="8"/>
<connect gate="G$1" pin="P7/COL8@9" pad="9"/>
<connect gate="G$1" pin="P8@14" pad="14"/>
<connect gate="G$1" pin="P9/COL7@15" pad="15"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="S-CN-MICROBIT-RA" constant="no"/>
<attribute name="DESC" value="Microbit SMD Right Angle Connector" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="40 Ways" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GROVE-04" prefix="JP">
<description>Grove Connector - 4 Ways</description>
<gates>
<gate name="G$1" symbol="1X04" x="0" y="0"/>
</gates>
<devices>
<device name="_(SMD-RA)" package="GROVE-04-RA">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="S-CN-GROVE-4H" constant="no"/>
<attribute name="DESC" value="S Grove Connector 4 Ways (Horizontal) " constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="4 Ways" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(SMD)" package="GROVE-04">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="S-CN-GROVE-4V" constant="no"/>
<attribute name="DESC" value="S Grove Connector 4 Ways (Vertical) " constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="4 Ways" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="AUDIO_JACK" prefix="JP">
<description>PJ-3502</description>
<gates>
<gate name="G$1" symbol="AUDIO_JACK" x="0" y="0"/>
</gates>
<devices>
<device name="_(SMD)" package="AUDIO_JACK">
<connects>
<connect gate="G$1" pin="R" pad="2 5"/>
<connect gate="G$1" pin="S" pad="1 4"/>
<connect gate="G$1" pin="T" pad="3 6"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="S-CN-PJ-3B-6PSMT" constant="no"/>
<attribute name="DESC" value="3.5mm SMD Audio Jack" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(STRAIGHT-TH)" package="AUDIO-JACK-MJ-074N">
<connects>
<connect gate="G$1" pin="R" pad="2"/>
<connect gate="G$1" pin="S" pad="3 4"/>
<connect gate="G$1" pin="T" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="3.5mm Audio Jack MJ-074N" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="MF" value="multicomp" constant="no"/>
<attribute name="MPN" value="MJ-074N" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ICSP-5PIN" prefix="JP">
<description>ICSP Connector for Microchip MCU (without AUX pin)</description>
<gates>
<gate name="G$1" symbol="ICSP-5PIN" x="0" y="0"/>
</gates>
<devices>
<device name="_INTERNAL" package="ICSP-5PIN">
<connects>
<connect gate="G$1" pin="PGC" pad="2"/>
<connect gate="G$1" pin="PGD" pad="3"/>
<connect gate="G$1" pin="VDD" pad="5"/>
<connect gate="G$1" pin="VPP" pad="1"/>
<connect gate="G$1" pin="VSS" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="LABEL" value="ICSP" constant="no"/>
<attribute name="REMARK" value="No component is needed." constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="HEADER-1X03" prefix="JP">
<description>Header - 1X03</description>
<gates>
<gate name="G$1" symbol="1X03" x="0" y="0"/>
</gates>
<devices>
<device name="_(FEMALE)" package="HDR-F1X03">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="CN-PH-F140S" constant="no"/>
<attribute name="DESC" value="Female Header - 1x40 Ways" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="3/40 Ways" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(MALE)" package="HDR-M1X03">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="CN-PH-M140S" constant="no"/>
<attribute name="DESC" value="Male Header - 1x40 Ways" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="3/40 Ways" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(MALE-LOCK)" package="HDR-M1X03-LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="CN-PH-M140S" constant="no"/>
<attribute name="DESC" value="Male Header - 1x40 Ways" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="3/40 Ways" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(NOSILK)" package="HDR-1X03-NOSILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESC" value="SIL Pad" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="REMARK" value="Not Populated" constant="no"/>
<attribute name="VALUE" value="3 Ways" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(SQ-NOSILK)" package="HDR-1X03-SQ-NOSILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESC" value="SIL Pad (Square)" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="REMARK" value="Not Populated" constant="no"/>
<attribute name="VALUE" value="2 Ways" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(GRAVITY-RA)" package="JST-S3B-PH-SM4-TB">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="SMD Gravity Right Angle Connector" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="3 Ways" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(SMD-MALE-RA)" package="S-HDR-M1X03-RA">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="S-CN-PH-M140R" constant="no"/>
<attribute name="DESC" value="S Right Angle Male Header 1x40 Ways" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="3/40 Ways" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(SMD-MALE)" package="S-HDR-M1X03">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="S-CN-PH-M140" constant="no"/>
<attribute name="DESC" value="S Straight Male Header 1x40 Ways" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="3/40 Ways" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DC_POWER_JACK" prefix="DC">
<description>DC Power Jack (SMD)</description>
<gates>
<gate name="G$1" symbol="DC_POWER_JACK" x="0" y="0"/>
</gates>
<devices>
<device name="_(SMD)" package="DC_POWER_JACK">
<connects>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="GNDBREAK" pad="2"/>
<connect gate="G$1" pin="PWR" pad="3 4"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="S-CN-HL2527" constant="no"/>
<attribute name="DESC" value="DC Power Jack (SMD)" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="MF" value="LONGFA" constant="no"/>
<attribute name="MPN" value="DC-050" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(PTH_T_B)" package="DC_PLUG_PTH_T_B">
<connects>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="GNDBREAK" pad="3 4"/>
<connect gate="G$1" pin="PWR" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="CN-HL-2527-B" constant="no"/>
<attribute name="DESC" value="HL2527 DC Plug (2.1mm) (Black)" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(PTH)" package="DC_PLUG_PTH">
<connects>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="GNDBREAK" pad="3"/>
<connect gate="G$1" pin="PWR" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="HEADER-2X04" prefix="JP">
<description>Header - 2X04</description>
<gates>
<gate name="G$1" symbol="2X04" x="0" y="0"/>
</gates>
<devices>
<device name="_(FEMALE)" package="HDR-F2X04">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="CN-PH-F240S" constant="no"/>
<attribute name="DESC" value="Female Header - 2x40 Ways" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="4/40 Ways" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(MALE)" package="HDR-M2X04">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="CN-PH-M240S" constant="no"/>
<attribute name="DESC" value="Male Header - 2x40 Ways" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="4/40 Ways" constant="no"/>
</technology>
</technologies>
</device>
<device name="_(SMD-FEMALE-LP)" package="S-HDR-F2X04-LP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name="">
<attribute name="CYTRON-PC" value="" constant="no"/>
<attribute name="DESC" value="S Straight Female Header - 2x40 Ways" constant="no"/>
<attribute name="LABEL" value="" constant="no"/>
<attribute name="REMARK" value="" constant="no"/>
<attribute name="VALUE" value="2x40 Ways" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0.254" drill="0.4">
</class>
</classes>
<parts>
<part name="FRAME1" library="Misc" deviceset="FRAME-A4" device=""/>
<part name="LOGO1" library="Misc" deviceset="CYTRON-LOGO" device=""/>
<part name="R61" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_330R" value="330R"/>
<part name="DS33" library="Others" deviceset="LED" device="_(1608)" technology="_RED" value="Red">
<attribute name="LABEL" value="LOW"/>
</part>
<part name="GND3" library="Supply" deviceset="GND" device=""/>
<part name="U1" library="IC &amp; Transistor" deviceset="MT3608" device="" value="24V 2A"/>
<part name="GND25" library="Supply" deviceset="GND" device=""/>
<part name="C7" library="Others" deviceset="CAP-C" device="_(1608)" technology="_0.1UF" value="0.1uF"/>
<part name="L1" library="Others" deviceset="INDUCTOR" device="_(CD54)" technology="_33UH" value="33uH"/>
<part name="GND26" library="Supply" deviceset="GND" device=""/>
<part name="C11" library="Others" deviceset="CAP-C" device="_(3216)" technology="_10UF" value="50V10uF"/>
<part name="D2" library="Others" deviceset="DIODE-SCHOTTKY" device="_(SMA)" technology="_15MQ040N" value="40V 3A"/>
<part name="L2" library="Others" deviceset="INDUCTOR" device="_(CD54)" technology="_33UH" value="33uH"/>
<part name="GND27" library="Supply" deviceset="GND" device=""/>
<part name="R6" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_10K" value="10K"/>
<part name="R7" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_2K2" value="2K2"/>
<part name="GND28" library="Supply" deviceset="GND" device=""/>
<part name="GND29" library="Supply" deviceset="GND" device=""/>
<part name="U$1" library="Supply" deviceset="+3V3" device=""/>
<part name="Q2" library="IC &amp; Transistor" deviceset="MOSFET-P" device="_(SOT)" technology="_IRLML6402" value="-20V3.7A"/>
<part name="S1" library="Others" deviceset="SW-SLIDE-DPDT" device="_(MSS22D18-SMD)" value="">
<attribute name="LABEL" value="POWER"/>
</part>
<part name="Q1" library="IC &amp; Transistor" deviceset="MOSFET-P" device="_(SOT)" technology="_IRLML6402" value="-20V3.7A"/>
<part name="Q3" library="IC &amp; Transistor" deviceset="TRANSISTOR-NPN" device="_(SOT-23-BEC)" technology="_MMBT3904" value="40V 200mA"/>
<part name="R3" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_10K" value="10K"/>
<part name="GND10" library="Supply" deviceset="GND" device=""/>
<part name="R4" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_100K" value="100K"/>
<part name="R5" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_100K" value="100K"/>
<part name="R1" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_100K" value="100K"/>
<part name="C6" library="Others" deviceset="CAP-E-ALUMINIUM" device="_(CHEMICON-F55)" technology="_16V100UF" value="16V100uF"/>
<part name="GND2" library="Supply" deviceset="GND" device=""/>
<part name="JP10" library="Header and Connector" deviceset="TERMINAL-BLOCK-02" device="_(KF127)" technology="_BLACK" value="2 Ways Black">
<attribute name="LABEL" value="MOTOR2"/>
</part>
<part name="JP11" library="Header and Connector" deviceset="TERMINAL-BLOCK-02" device="_(KF127)" technology="_BLACK" value="2 Ways Black">
<attribute name="LABEL" value="MOTOR1"/>
</part>
<part name="R63" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_330R" value="330R"/>
<part name="DS34" library="Others" deviceset="LED" device="_(1608)" technology="_RED" value="Red">
<attribute name="LABEL" value="M2B"/>
</part>
<part name="DS31" library="Others" deviceset="LED" device="_(1608)" technology="_RED" value="Red">
<attribute name="LABEL" value="M2A"/>
</part>
<part name="Q16" library="IC &amp; Transistor" deviceset="TRANSISTOR-NPN" device="_(SOT-23-BEC)" technology="_MMBT3904" value="40V 200mA"/>
<part name="GND13" library="Supply" deviceset="GND" device=""/>
<part name="R49" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_10K" value="10K"/>
<part name="R53" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_10K" value="10K"/>
<part name="S2" library="Others" deviceset="SW-PB" device="_(KAN3246)" value="">
<attribute name="LABEL" value="M2B"/>
</part>
<part name="U2" library="IC &amp; Transistor" deviceset="MX1508" device="" value="2 - 9.6V"/>
<part name="C14" library="Others" deviceset="CAP-C" device="_(2012)" technology="_10UF" value="10uF"/>
<part name="GND16" library="Supply" deviceset="GND" device=""/>
<part name="GND17" library="Supply" deviceset="GND" device=""/>
<part name="R64" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_330R" value="330R"/>
<part name="DS35" library="Others" deviceset="LED" device="_(1608)" technology="_RED" value="Red">
<attribute name="LABEL" value="M1B"/>
</part>
<part name="DS32" library="Others" deviceset="LED" device="_(1608)" technology="_RED" value="Red">
<attribute name="LABEL" value="M1A"/>
</part>
<part name="Q17" library="IC &amp; Transistor" deviceset="TRANSISTOR-NPN" device="_(SOT-23-BEC)" technology="_MMBT3904" value="40V 200mA"/>
<part name="GND18" library="Supply" deviceset="GND" device=""/>
<part name="R50" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_10K" value="10K"/>
<part name="R54" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_10K" value="10K"/>
<part name="S3" library="Others" deviceset="SW-PB" device="_(KAN3246)" value="">
<attribute name="LABEL" value="M2A"/>
</part>
<part name="Q18" library="IC &amp; Transistor" deviceset="TRANSISTOR-NPN" device="_(SOT-23-BEC)" technology="_MMBT3904" value="40V 200mA"/>
<part name="GND20" library="Supply" deviceset="GND" device=""/>
<part name="R51" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_10K" value="10K"/>
<part name="R55" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_10K" value="10K"/>
<part name="S4" library="Others" deviceset="SW-PB" device="_(KAN3246)" value="">
<attribute name="LABEL" value="M1B"/>
</part>
<part name="Q19" library="IC &amp; Transistor" deviceset="TRANSISTOR-NPN" device="_(SOT-23-BEC)" technology="_MMBT3904" value="40V 200mA"/>
<part name="GND22" library="Supply" deviceset="GND" device=""/>
<part name="R52" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_10K" value="10K"/>
<part name="R56" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_10K" value="10K"/>
<part name="S5" library="Others" deviceset="SW-PB" device="_(KAN3246)" value="">
<attribute name="LABEL" value="M1A"/>
</part>
<part name="U$6" library="Supply" deviceset="+3V3" device=""/>
<part name="GND4" library="Supply" deviceset="GND" device=""/>
<part name="GND5" library="Supply" deviceset="GND" device=""/>
<part name="GND6" library="Supply" deviceset="GND" device=""/>
<part name="GND8" library="Supply" deviceset="GND" device=""/>
<part name="R66" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_1K" value="1K"/>
<part name="R67" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_1K" value="1K"/>
<part name="R68" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_1K" value="1K"/>
<part name="FRAME2" library="Misc" deviceset="FRAME-A4" device=""/>
<part name="LOGO2" library="Misc" deviceset="CYTRON-LOGO" device=""/>
<part name="CON1" library="Header and Connector" deviceset="CN-MICROBIT" device="_(PCIRSM127)" value="40 Ways">
<attribute name="LABEL" value="MICRO:BIT"/>
</part>
<part name="GND15" library="Supply" deviceset="GND" device=""/>
<part name="U$9" library="Supply" deviceset="+3V3" device=""/>
<part name="C13" library="Others" deviceset="CAP-C" device="_(2012)" technology="_10UF" value="10uF"/>
<part name="GND24" library="Supply" deviceset="GND" device=""/>
<part name="FRAME3" library="Misc" deviceset="FRAME-A4" device=""/>
<part name="LOGO3" library="Misc" deviceset="CYTRON-LOGO" device=""/>
<part name="JP6" library="Header and Connector" deviceset="GROVE-04" device="_(SMD)" value="4 Ways">
<attribute name="LABEL" value="P14:P15:P16"/>
</part>
<part name="GND30" library="Supply" deviceset="GND" device=""/>
<part name="JP1" library="Header and Connector" deviceset="GROVE-04" device="_(SMD)" value="4 Ways">
<attribute name="LABEL" value="P0"/>
</part>
<part name="GND32" library="Supply" deviceset="GND" device=""/>
<part name="U$11" library="Supply" deviceset="+3V3" device=""/>
<part name="C1" library="Others" deviceset="CAP-C" device="_(2012)" technology="_10UF" value="10uF"/>
<part name="GND33" library="Supply" deviceset="GND" device=""/>
<part name="JP2" library="Header and Connector" deviceset="GROVE-04" device="_(SMD)" value="4 Ways">
<attribute name="LABEL" value="P1"/>
</part>
<part name="GND34" library="Supply" deviceset="GND" device=""/>
<part name="U$12" library="Supply" deviceset="+3V3" device=""/>
<part name="C2" library="Others" deviceset="CAP-C" device="_(2012)" technology="_10UF" value="10uF"/>
<part name="GND35" library="Supply" deviceset="GND" device=""/>
<part name="JP3" library="Header and Connector" deviceset="GROVE-04" device="_(SMD)" value="4 Ways">
<attribute name="LABEL" value="P2"/>
</part>
<part name="GND36" library="Supply" deviceset="GND" device=""/>
<part name="U$13" library="Supply" deviceset="+3V3" device=""/>
<part name="C3" library="Others" deviceset="CAP-C" device="_(2012)" technology="_10UF" value="10uF"/>
<part name="GND37" library="Supply" deviceset="GND" device=""/>
<part name="JP4" library="Header and Connector" deviceset="GROVE-04" device="_(SMD)" value="4 Ways">
<attribute name="LABEL" value="P8/P12"/>
</part>
<part name="GND38" library="Supply" deviceset="GND" device=""/>
<part name="U$14" library="Supply" deviceset="+3V3" device=""/>
<part name="C4" library="Others" deviceset="CAP-C" device="_(2012)" technology="_10UF" value="10uF"/>
<part name="GND39" library="Supply" deviceset="GND" device=""/>
<part name="JP5" library="Header and Connector" deviceset="GROVE-04" device="_(SMD)" value="4 Ways">
<attribute name="LABEL" value="P13"/>
</part>
<part name="GND40" library="Supply" deviceset="GND" device=""/>
<part name="U$15" library="Supply" deviceset="+3V3" device=""/>
<part name="C5" library="Others" deviceset="CAP-C" device="_(2012)" technology="_10UF" value="10uF"/>
<part name="GND41" library="Supply" deviceset="GND" device=""/>
<part name="JP7" library="Header and Connector" deviceset="GROVE-04" device="_(SMD)" value="4 Ways">
<attribute name="LABEL" value="BTNA/BTNB"/>
</part>
<part name="GND42" library="Supply" deviceset="GND" device=""/>
<part name="U$16" library="Supply" deviceset="+3V3" device=""/>
<part name="C9" library="Others" deviceset="CAP-C" device="_(2012)" technology="_10UF" value="10uF"/>
<part name="GND43" library="Supply" deviceset="GND" device=""/>
<part name="LS1" library="Others" deviceset="BUZZER" device="_(8530)"/>
<part name="Q23" library="IC &amp; Transistor" deviceset="MOSFET-N" device="_(SOT-23)" technology="_BSS138" value="50V 220mA"/>
<part name="R93" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_3M3" value="3M3"/>
<part name="C31" library="Others" deviceset="CAP-C" device="_(1608)" technology="_0.1UF" value="0.1uF"/>
<part name="GND44" library="Supply" deviceset="GND" device=""/>
<part name="GND45" library="Supply" deviceset="GND" device=""/>
<part name="U$17" library="Supply" deviceset="+3V3" device=""/>
<part name="S8" library="Others" deviceset="SW-SLIDE-SPDT" device="_(SMD)" value="">
<attribute name="LABEL" value="Buzzer"/>
</part>
<part name="JP23" library="Header and Connector" deviceset="AUDIO_JACK" device="_(SMD)">
<attribute name="LABEL" value="External"/>
</part>
<part name="GND47" library="Supply" deviceset="GND" device=""/>
<part name="DS2" library="Others" deviceset="LED" device="_(1608)" technology="_BLUE" value="Blue">
<attribute name="LABEL" value="P8"/>
</part>
<part name="Q8" library="IC &amp; Transistor" deviceset="MOSFET-N" device="_(SOT-23)" technology="_BSS138" value="50V 220mA"/>
<part name="R14" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_4K7" value="4K7"/>
<part name="R12" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_3M3" value="3M3"/>
<part name="GND48" library="Supply" deviceset="GND" device=""/>
<part name="U$18" library="Supply" deviceset="+3V3" device=""/>
<part name="D3" library="Others" deviceset="DIODE-ZENER/TVS-UNI" device="_(SOD-123)" technology="_0.5W12V" value="12V"/>
<part name="GND56" library="Supply" deviceset="GND" device=""/>
<part name="GND57" library="Supply" deviceset="GND" device=""/>
<part name="DS4" library="Others" deviceset="LED" device="_(1608)" technology="_BLUE" value="Blue">
<attribute name="LABEL" value="P12"/>
</part>
<part name="Q10" library="IC &amp; Transistor" deviceset="MOSFET-N" device="_(SOT-23)" technology="_BSS138" value="50V 220mA"/>
<part name="R18" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_4K7" value="4K7"/>
<part name="R16" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_3M3" value="3M3"/>
<part name="GND49" library="Supply" deviceset="GND" device=""/>
<part name="U$19" library="Supply" deviceset="+3V3" device=""/>
<part name="D5" library="Others" deviceset="DIODE-ZENER/TVS-UNI" device="_(SOD-123)" technology="_0.5W12V" value="12V"/>
<part name="GND50" library="Supply" deviceset="GND" device=""/>
<part name="GND51" library="Supply" deviceset="GND" device=""/>
<part name="DS6" library="Others" deviceset="LED" device="_(1608)" technology="_BLUE" value="Blue">
<attribute name="LABEL" value="P13"/>
</part>
<part name="Q12" library="IC &amp; Transistor" deviceset="MOSFET-N" device="_(SOT-23)" technology="_BSS138" value="50V 220mA"/>
<part name="R22" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_4K7" value="4K7"/>
<part name="R20" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_3M3" value="3M3"/>
<part name="GND52" library="Supply" deviceset="GND" device=""/>
<part name="U$20" library="Supply" deviceset="+3V3" device=""/>
<part name="D7" library="Others" deviceset="DIODE-ZENER/TVS-UNI" device="_(SOD-123)" technology="_0.5W12V" value="12V"/>
<part name="GND53" library="Supply" deviceset="GND" device=""/>
<part name="GND54" library="Supply" deviceset="GND" device=""/>
<part name="DS8" library="Others" deviceset="LED" device="_(1608)" technology="_BLUE" value="Blue">
<attribute name="LABEL" value="P14"/>
</part>
<part name="Q14" library="IC &amp; Transistor" deviceset="MOSFET-N" device="_(SOT-23)" technology="_BSS138" value="50V 220mA"/>
<part name="R26" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_4K7" value="4K7"/>
<part name="R24" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_3M3" value="3M3"/>
<part name="GND55" library="Supply" deviceset="GND" device=""/>
<part name="U$21" library="Supply" deviceset="+3V3" device=""/>
<part name="D9" library="Others" deviceset="DIODE-ZENER/TVS-UNI" device="_(SOD-123)" technology="_0.5W12V" value="12V"/>
<part name="GND58" library="Supply" deviceset="GND" device=""/>
<part name="GND59" library="Supply" deviceset="GND" device=""/>
<part name="DS3" library="Others" deviceset="LED" device="_(1608)" technology="_BLUE" value="Blue">
<attribute name="LABEL" value="P15"/>
</part>
<part name="Q9" library="IC &amp; Transistor" deviceset="MOSFET-N" device="_(SOT-23)" technology="_BSS138" value="50V 220mA"/>
<part name="R15" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_4K7" value="4K7"/>
<part name="R13" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_3M3" value="3M3"/>
<part name="GND60" library="Supply" deviceset="GND" device=""/>
<part name="U$22" library="Supply" deviceset="+3V3" device=""/>
<part name="D4" library="Others" deviceset="DIODE-ZENER/TVS-UNI" device="_(SOD-123)" technology="_0.5W12V" value="12V"/>
<part name="GND61" library="Supply" deviceset="GND" device=""/>
<part name="GND62" library="Supply" deviceset="GND" device=""/>
<part name="DS5" library="Others" deviceset="LED" device="_(1608)" technology="_BLUE" value="Blue">
<attribute name="LABEL" value="P16"/>
</part>
<part name="Q11" library="IC &amp; Transistor" deviceset="MOSFET-N" device="_(SOT-23)" technology="_BSS138" value="50V 220mA"/>
<part name="R19" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_4K7" value="4K7"/>
<part name="R17" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_3M3" value="3M3"/>
<part name="GND63" library="Supply" deviceset="GND" device=""/>
<part name="U$23" library="Supply" deviceset="+3V3" device=""/>
<part name="D6" library="Others" deviceset="DIODE-ZENER/TVS-UNI" device="_(SOD-123)" technology="_0.5W12V" value="12V"/>
<part name="GND64" library="Supply" deviceset="GND" device=""/>
<part name="GND65" library="Supply" deviceset="GND" device=""/>
<part name="DS7" library="Others" deviceset="LED" device="_(1608)" technology="_BLUE" value="Blue">
<attribute name="LABEL" value="BTNA"/>
</part>
<part name="Q13" library="IC &amp; Transistor" deviceset="MOSFET-N" device="_(SOT-23)" technology="_BSS138" value="50V 220mA"/>
<part name="R23" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_4K7" value="4K7"/>
<part name="R21" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_3M3" value="3M3"/>
<part name="GND66" library="Supply" deviceset="GND" device=""/>
<part name="U$24" library="Supply" deviceset="+3V3" device=""/>
<part name="D8" library="Others" deviceset="DIODE-ZENER/TVS-UNI" device="_(SOD-123)" technology="_0.5W12V" value="12V"/>
<part name="GND67" library="Supply" deviceset="GND" device=""/>
<part name="GND68" library="Supply" deviceset="GND" device=""/>
<part name="DS9" library="Others" deviceset="LED" device="_(1608)" technology="_BLUE" value="Blue">
<attribute name="LABEL" value="BTNB"/>
</part>
<part name="Q15" library="IC &amp; Transistor" deviceset="MOSFET-N" device="_(SOT-23)" technology="_BSS138" value="50V 220mA"/>
<part name="R27" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_4K7" value="4K7"/>
<part name="R25" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_3M3" value="3M3"/>
<part name="GND69" library="Supply" deviceset="GND" device=""/>
<part name="U$25" library="Supply" deviceset="+3V3" device=""/>
<part name="D10" library="Others" deviceset="DIODE-ZENER/TVS-UNI" device="_(SOD-123)" technology="_0.5W12V" value="12V"/>
<part name="GND70" library="Supply" deviceset="GND" device=""/>
<part name="GND71" library="Supply" deviceset="GND" device=""/>
<part name="D12" library="Others" deviceset="DIODE-ZENER/TVS-UNI" device="_(SOD-123)" technology="_0.5W12V" value="12V"/>
<part name="GND72" library="Supply" deviceset="GND" device=""/>
<part name="GND73" library="Supply" deviceset="GND" device=""/>
<part name="FRAME4" library="Misc" deviceset="FRAME-A4" device=""/>
<part name="LOGO4" library="Misc" deviceset="CYTRON-LOGO" device=""/>
<part name="U$27" library="Supply" deviceset="+3V3" device=""/>
<part name="GND75" library="Supply" deviceset="GND" device=""/>
<part name="C19" library="Others" deviceset="CAP-C" device="_(1608)" technology="_0.1UF" value="0.1uF"/>
<part name="S6" library="Others" deviceset="SW-PB" device="_(120120-4P)" value="">
<attribute name="LABEL" value="B"/>
</part>
<part name="R78" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_100K" value="100K"/>
<part name="R76" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_1K" value="1K"/>
<part name="GND76" library="Supply" deviceset="GND" device=""/>
<part name="JP16" library="Header and Connector" deviceset="GROVE-04" device="_(SMD-RA)" value="4 Ways"/>
<part name="GND77" library="Supply" deviceset="GND" device=""/>
<part name="U$28" library="Supply" deviceset="+3V3" device=""/>
<part name="U$29" library="Supply" deviceset="+3V3" device=""/>
<part name="GND78" library="Supply" deviceset="GND" device=""/>
<part name="C20" library="Others" deviceset="CAP-C" device="_(1608)" technology="_0.1UF" value="0.1uF"/>
<part name="S7" library="Others" deviceset="SW-PB" device="_(120120-4P)" value="">
<attribute name="LABEL" value="A"/>
</part>
<part name="R79" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_100K" value="100K"/>
<part name="R77" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_1K" value="1K"/>
<part name="GND79" library="Supply" deviceset="GND" device=""/>
<part name="U5" library="Others" deviceset="LED-RGB-ADD" device=""/>
<part name="U$30" library="Supply" deviceset="+3V3" device=""/>
<part name="GND80" library="Supply" deviceset="GND" device=""/>
<part name="C25" library="Others" deviceset="CAP-C" device="_(1608)" technology="_0.1UF" value="0.1uF"/>
<part name="JP20" library="Header and Connector" deviceset="GROVE-04" device="_(SMD-RA)" value="4 Ways"/>
<part name="U$31" library="Supply" deviceset="+3V3" device=""/>
<part name="U$32" library="Supply" deviceset="+3V3" device=""/>
<part name="GND83" library="Supply" deviceset="GND" device=""/>
<part name="GND82" library="Supply" deviceset="GND" device=""/>
<part name="JP17" library="Header and Connector" deviceset="GROVE-04" device="_(SMD-RA)" value="4 Ways"/>
<part name="GND84" library="Supply" deviceset="GND" device=""/>
<part name="U$33" library="Supply" deviceset="+3V3" device=""/>
<part name="C21" library="Others" deviceset="CAP-C" device="_(1608)" technology="_0.1UF" value="0.1uF"/>
<part name="VR1" library="Others" deviceset="VARIABLE-RESISTOR" device="_(RV0932-TH)" technology="_10K" value="10K"/>
<part name="GND85" library="Supply" deviceset="GND" device=""/>
<part name="U$34" library="Supply" deviceset="+3V3" device=""/>
<part name="C18" library="Others" deviceset="CAP-C" device="_(1608)" technology="_0.1UF" value="0.1uF"/>
<part name="GND86" library="Supply" deviceset="GND" device=""/>
<part name="GND87" library="Supply" deviceset="GND" device=""/>
<part name="JP18" library="Header and Connector" deviceset="GROVE-04" device="_(SMD-RA)" value="4 Ways"/>
<part name="GND88" library="Supply" deviceset="GND" device=""/>
<part name="DS38" library="Others" deviceset="LED" device="_(10MM-TH)" technology="_YELLOW" value="Yellow"/>
<part name="DS39" library="Others" deviceset="LED" device="_(10MM-TH)" technology="_RED" value="Red"/>
<part name="R72" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_330R" value="330R"/>
<part name="R74" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_330R" value="330R"/>
<part name="R75" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_2K" value="2K"/>
<part name="GND89" library="Supply" deviceset="GND" device=""/>
<part name="U7" library="Others" deviceset="LED-RGB-ADD" device=""/>
<part name="GND90" library="Supply" deviceset="GND" device=""/>
<part name="C28" library="Others" deviceset="CAP-C" device="_(1608)" technology="_0.1UF" value="0.1uF"/>
<part name="U$36" library="Supply" deviceset="+3V3" device=""/>
<part name="GND91" library="Supply" deviceset="GND" device=""/>
<part name="U9" library="Others" deviceset="LED-RGB-ADD" device=""/>
<part name="GND92" library="Supply" deviceset="GND" device=""/>
<part name="C30" library="Others" deviceset="CAP-C" device="_(1608)" technology="_0.1UF" value="0.1uF"/>
<part name="U$37" library="Supply" deviceset="+3V3" device=""/>
<part name="GND93" library="Supply" deviceset="GND" device=""/>
<part name="JP21" library="Header and Connector" deviceset="GROVE-04" device="_(SMD-RA)" value="4 Ways"/>
<part name="GND94" library="Supply" deviceset="GND" device=""/>
<part name="U$38" library="Supply" deviceset="+3V3" device=""/>
<part name="MK1" library="Others" deviceset="MIC" device="_(9.7MM)" technology="_9767P" value=""/>
<part name="GND95" library="Supply" deviceset="GND" device=""/>
<part name="R92" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_10K" value="10K"/>
<part name="U$39" library="Supply" deviceset="+3V3" device=""/>
<part name="C29" library="Others" deviceset="CAP-C" device="_(1608)" technology="_1UF" value="1uF"/>
<part name="R90" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_10K" value="10K"/>
<part name="R88" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_1M" value="1M"/>
<part name="C27" library="Others" deviceset="CAP-C" device="_(1608)" technology="_12PF" value="12pF"/>
<part name="C26" library="Others" deviceset="CAP-C" device="_(1608)" technology="_0.1UF" value="0.1uF"/>
<part name="U$40" library="Supply" deviceset="+3V3" device=""/>
<part name="GND97" library="Supply" deviceset="GND" device=""/>
<part name="U6" library="Others" deviceset="OPTO-REFLECTIVE-SENSOR" device="_ITR20001" value=""/>
<part name="R86" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_220R" value="220R"/>
<part name="R87" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_220R" value="220R"/>
<part name="R89" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_220R" value="220R"/>
<part name="U$42" library="Supply" deviceset="+3V3" device=""/>
<part name="GND101" library="Supply" deviceset="GND" device=""/>
<part name="R85" library="Others" deviceset="RESISTOR" device="_(2012)" technology="_220K" value="220K"/>
<part name="GND102" library="Supply" deviceset="GND" device=""/>
<part name="U$43" library="Supply" deviceset="+3V3" device=""/>
<part name="U4" library="IC &amp; Transistor" deviceset="MCP6001U" device="" value="5 Pins"/>
<part name="GND103" library="Supply" deviceset="GND" device=""/>
<part name="VR2" library="Others" deviceset="VARIABLE-RESISTOR" device="_(3386-TH)" technology="_10K" value="10K"/>
<part name="GND104" library="Supply" deviceset="GND" device=""/>
<part name="U$44" library="Supply" deviceset="+3V3" device=""/>
<part name="C23" library="Others" deviceset="CAP-C" device="_(1608)" technology="_0.1UF" value="0.1uF"/>
<part name="GND105" library="Supply" deviceset="GND" device=""/>
<part name="C22" library="Others" deviceset="CAP-C" device="_(1608)" technology="_0.1UF" value="0.1uF"/>
<part name="GND106" library="Supply" deviceset="GND" device=""/>
<part name="U$45" library="Supply" deviceset="+3V3" device=""/>
<part name="JP19" library="Header and Connector" deviceset="GROVE-04" device="_(SMD-RA)" value="4 Ways"/>
<part name="GND107" library="Supply" deviceset="GND" device=""/>
<part name="U$46" library="Supply" deviceset="+3V3" device=""/>
<part name="R81" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_1K" value="1K"/>
<part name="DS40" library="Others" deviceset="LED" device="_(1608)" technology="_RED" value="Red"/>
<part name="GND108" library="Supply" deviceset="GND" device=""/>
<part name="JP8" library="Header and Connector" deviceset="GROVE-04" device="_(SMD)" value="4 Ways">
<attribute name="LABEL" value="I2C"/>
</part>
<part name="GND109" library="Supply" deviceset="GND" device=""/>
<part name="U$47" library="Supply" deviceset="+3V3" device=""/>
<part name="C10" library="Others" deviceset="CAP-C" device="_(2012)" technology="_10UF" value="10uF"/>
<part name="GND110" library="Supply" deviceset="GND" device=""/>
<part name="U$48" library="Supply" deviceset="+3V3" device=""/>
<part name="C8" library="Others" deviceset="CAP-C" device="_(1608)" technology="_0.1UF" value="0.1uF"/>
<part name="GND111" library="Supply" deviceset="GND" device=""/>
<part name="GND112" library="Supply" deviceset="GND" device=""/>
<part name="GND113" library="Supply" deviceset="GND" device=""/>
<part name="C17" library="Others" deviceset="CAP-C" device="_(1608)" technology="_1UF" value="1uF"/>
<part name="GND19" library="Supply" deviceset="GND" device=""/>
<part name="R83" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_4K7" value="4K7"/>
<part name="U3" library="IC &amp; Transistor" deviceset="PIC16F1937" device=""/>
<part name="GND21" library="Supply" deviceset="GND" device=""/>
<part name="GND23" library="Supply" deviceset="GND" device=""/>
<part name="C16" library="Others" deviceset="CAP-C" device="_(1608)" technology="_1UF" value="1uF"/>
<part name="GND74" library="Supply" deviceset="GND" device=""/>
<part name="GND1" library="Supply" deviceset="GND" device=""/>
<part name="JP15" library="Header and Connector" deviceset="ICSP-5PIN" device="_INTERNAL"/>
<part name="GND7" library="Supply" deviceset="GND" device=""/>
<part name="DS10" library="Others" deviceset="LED" device="_(1608)" technology="_BLUE" value="Blue"/>
<part name="R28" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_4K7" value="4K7"/>
<part name="DS11" library="Others" deviceset="LED" device="_(1608)" technology="_BLUE" value="Blue"/>
<part name="R29" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_4K7" value="4K7"/>
<part name="DS12" library="Others" deviceset="LED" device="_(1608)" technology="_BLUE" value="Blue"/>
<part name="R30" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_4K7" value="4K7"/>
<part name="DS13" library="Others" deviceset="LED" device="_(1608)" technology="_BLUE" value="Blue"/>
<part name="R31" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_4K7" value="4K7"/>
<part name="DS14" library="Others" deviceset="LED" device="_(1608)" technology="_BLUE" value="Blue"/>
<part name="R32" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_4K7" value="4K7"/>
<part name="DS15" library="Others" deviceset="LED" device="_(1608)" technology="_BLUE" value="Blue"/>
<part name="R33" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_4K7" value="4K7"/>
<part name="DS16" library="Others" deviceset="LED" device="_(1608)" technology="_BLUE" value="Blue"/>
<part name="R34" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_4K7" value="4K7"/>
<part name="GND11" library="Supply" deviceset="GND" device=""/>
<part name="DS24" library="Others" deviceset="LED" device="_(1608)" technology="_BLUE" value="Blue"/>
<part name="R42" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_4K7" value="4K7"/>
<part name="DS25" library="Others" deviceset="LED" device="_(1608)" technology="_BLUE" value="Blue"/>
<part name="R43" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_4K7" value="4K7"/>
<part name="DS26" library="Others" deviceset="LED" device="_(1608)" technology="_BLUE" value="Blue"/>
<part name="R44" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_4K7" value="4K7"/>
<part name="DS27" library="Others" deviceset="LED" device="_(1608)" technology="_BLUE" value="Blue"/>
<part name="R45" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_4K7" value="4K7"/>
<part name="DS28" library="Others" deviceset="LED" device="_(1608)" technology="_BLUE" value="Blue"/>
<part name="R46" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_4K7" value="4K7"/>
<part name="DS29" library="Others" deviceset="LED" device="_(1608)" technology="_BLUE" value="Blue"/>
<part name="R47" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_4K7" value="4K7"/>
<part name="DS30" library="Others" deviceset="LED" device="_(1608)" technology="_BLUE" value="Blue"/>
<part name="R48" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_4K7" value="4K7"/>
<part name="GND12" library="Supply" deviceset="GND" device=""/>
<part name="DS17" library="Others" deviceset="LED" device="_(1608)" technology="_BLUE" value="Blue"/>
<part name="R35" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_4K7" value="4K7"/>
<part name="DS18" library="Others" deviceset="LED" device="_(1608)" technology="_BLUE" value="Blue"/>
<part name="R36" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_4K7" value="4K7"/>
<part name="DS19" library="Others" deviceset="LED" device="_(1608)" technology="_BLUE" value="Blue"/>
<part name="R37" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_4K7" value="4K7"/>
<part name="DS20" library="Others" deviceset="LED" device="_(1608)" technology="_BLUE" value="Blue"/>
<part name="R38" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_4K7" value="4K7"/>
<part name="DS21" library="Others" deviceset="LED" device="_(1608)" technology="_BLUE" value="Blue"/>
<part name="R39" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_4K7" value="4K7"/>
<part name="DS22" library="Others" deviceset="LED" device="_(1608)" technology="_BLUE" value="Blue"/>
<part name="R40" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_4K7" value="4K7"/>
<part name="DS23" library="Others" deviceset="LED" device="_(1608)" technology="_BLUE" value="Blue"/>
<part name="R41" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_4K7" value="4K7"/>
<part name="GND14" library="Supply" deviceset="GND" device=""/>
<part name="TP12" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="TP1" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="TP2" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="TP3" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="TP8" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="TP5" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="TP4" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="TP6" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="TP7" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="TP9" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="TP11" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="TP10" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="R59" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_3M3" value="3M3"/>
<part name="GND114" library="Supply" deviceset="GND" device=""/>
<part name="R60" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_3M3" value="3M3"/>
<part name="GND115" library="Supply" deviceset="GND" device=""/>
<part name="R62" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_3M3" value="3M3"/>
<part name="GND116" library="Supply" deviceset="GND" device=""/>
<part name="JP22" library="Header and Connector" deviceset="GROVE-04" device="_(SMD-RA)" value="4 Ways"/>
<part name="U$65" library="Supply" deviceset="+3V3" device=""/>
<part name="GND117" library="Supply" deviceset="GND" device=""/>
<part name="U10" library="Others" deviceset="LED-RGB-ADD" device=""/>
<part name="GND81" library="Supply" deviceset="GND" device=""/>
<part name="C32" library="Others" deviceset="CAP-C" device="_(1608)" technology="_0.1UF" value="0.1uF"/>
<part name="GND118" library="Supply" deviceset="GND" device=""/>
<part name="R57" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_10K" value="10K"/>
<part name="R58" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_2K2" value="2K2"/>
<part name="GND119" library="Supply" deviceset="GND" device=""/>
<part name="C15" library="Others" deviceset="CAP-C" device="_(1608)" technology="_0.1UF" value="0.1uF"/>
<part name="GND120" library="Supply" deviceset="GND" device=""/>
<part name="JP12" library="Header and Connector" deviceset="HEADER-1X03" device="_(MALE)" value="3/40 Ways"/>
<part name="JP13" library="Header and Connector" deviceset="HEADER-1X03" device="_(MALE)" value="3/40 Ways"/>
<part name="JP14" library="Header and Connector" deviceset="HEADER-1X03" device="_(MALE)" value="3/40 Ways"/>
<part name="R10" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_330R" value="330R"/>
<part name="DS1" library="Others" deviceset="LED" device="_(1608)" technology="_GREEN" value="Green">
<attribute name="LABEL" value="ON"/>
</part>
<part name="GND9" library="Supply" deviceset="GND" device=""/>
<part name="U$35" library="Supply" deviceset="+3V3" device=""/>
<part name="U$72" library="Supply" deviceset="+3V3" device=""/>
<part name="U$73" library="Supply" deviceset="+3V3" device=""/>
<part name="U$74" library="Supply" deviceset="+3V3" device=""/>
<part name="R91" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_10K" value="10K"/>
<part name="C24" library="Others" deviceset="CAP-C" device="_(1608)" technology="_0.1UF" value="0.1uF"/>
<part name="R84" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_1M" value="1M"/>
<part name="GND121" library="Supply" deviceset="GND" device=""/>
<part name="GND122" library="Supply" deviceset="GND" device=""/>
<part name="U8" library="IC &amp; Transistor" deviceset="MCP6001U" device="" value="5 Pins"/>
<part name="GND96" library="Supply" deviceset="GND" device=""/>
<part name="D11" library="Others" deviceset="DIODES-SCHOTTKY-AC" device="" value="30V200mA"/>
<part name="DS37" library="Others" deviceset="LED" device="_(10MM-TH)" technology="_GREEN" value="Green"/>
<part name="R73" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_5K6" value="5K6"/>
<part name="DC1" library="Header and Connector" deviceset="DC_POWER_JACK" device="_(PTH)"/>
<part name="VM1" library="Supply" deviceset="VM" device=""/>
<part name="VM2" library="Supply" deviceset="VM" device=""/>
<part name="VIN1" library="Supply" deviceset="VIN" device=""/>
<part name="D1" library="Others" deviceset="DIODE-SCHOTTKY" device="_(SMA)" technology="_15MQ040N" value="40V 3A"/>
<part name="VIN2" library="Supply" deviceset="VIN" device=""/>
<part name="VM3" library="Supply" deviceset="VM" device=""/>
<part name="Q21" library="IC &amp; Transistor" deviceset="MOSFET-P" device="_(SOT)" technology="_IRLML6402" value="-20V3.7A"/>
<part name="VIN3" library="Supply" deviceset="VIN" device=""/>
<part name="R69" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_100K" value="100K"/>
<part name="VIN4" library="Supply" deviceset="VIN" device=""/>
<part name="GND46" library="Supply" deviceset="GND" device=""/>
<part name="R70" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_3M3" value="3M3"/>
<part name="Q22" library="IC &amp; Transistor" deviceset="MOSFET-N" device="_(SOT-23)" technology="_BSS138" value="50V 220mA"/>
<part name="R2" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_100K" value="100K"/>
<part name="R71" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_100K" value="100K"/>
<part name="R65" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_10K" value="10K"/>
<part name="DS36" library="Others" deviceset="LED" device="_(1608)" technology="_RED" value="Red">
<attribute name="LABEL" value="HIGH"/>
</part>
<part name="GND98" library="Supply" deviceset="GND" device=""/>
<part name="Q20" library="IC &amp; Transistor" deviceset="MOSFET-N" device="_(SOT-23)" technology="_BSS138" value="50V 220mA"/>
<part name="GND100" library="Supply" deviceset="GND" device=""/>
<part name="GND123" library="Supply" deviceset="GND" device=""/>
<part name="Q6" library="IC &amp; Transistor" deviceset="MOSFET-P" device="_(SOT)" technology="_IRLML6402" value="-20V3.7A"/>
<part name="Q7" library="IC &amp; Transistor" deviceset="TRANSISTOR-NPN" device="_(SOT-23-BEC)" technology="_MMBT3904" value="40V 200mA"/>
<part name="R9" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_100K" value="100K"/>
<part name="R11" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_100K" value="100K"/>
<part name="R82" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_10K" value="10K"/>
<part name="GND124" library="Supply" deviceset="GND" device=""/>
<part name="R80" library="Others" deviceset="RESISTOR" device="_(1608)" technology="_330R" value="330R"/>
<part name="C12" library="Others" deviceset="CAP-E-ALUMINIUM" device="_(CHEMICON-F55)" technology="_16V100UF" value="16V100uF"/>
<part name="JP9" library="Header and Connector" deviceset="HEADER-2X04" device="_(SMD-FEMALE-LP)" value="2x40 Ways">
<attribute name="LABEL" value="OLED"/>
</part>
<part name="C33" library="Others" deviceset="CAP-C" device="_(1608)" technology="_1UF" value="1uF"/>
<part name="GND31" library="Supply" deviceset="GND" device=""/>
<part name="VIN5" library="Supply" deviceset="VIN" device=""/>
<part name="VIN6" library="Supply" deviceset="VIN" device=""/>
<part name="C34" library="Others" deviceset="CAP-C" device="_(1608)" technology="_0.1UF" value="0.1uF"/>
<part name="GND99" library="Supply" deviceset="GND" device=""/>
<part name="L3" library="Others" deviceset="FERRITE-BEAD" device="_(2012)" value="0.035R 4000mA"/>
<part name="L4" library="Others" deviceset="FERRITE-BEAD" device="_(2012)" value="0.035R 4000mA"/>
<part name="TP13" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="TP14" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="TP15" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="TP16" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="TP17" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="TP18" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="TP19" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="TP20" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="TP21" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="TP22" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="TP23" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="TP24" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="TP25" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="TP26" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="TP27" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="TP28" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="TP29" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="TP30" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="TP31" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="TP32" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="TP33" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="TP34" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="TP35" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="TP36" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="TP37" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="TP38" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="TP39" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="TP40" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="TP41" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="TP42" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="TP43" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="TP44" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="TP45" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="TP46" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="TP47" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="TP48" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="TP49" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="TP50" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="TP51" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
<part name="TP52" library="Misc" deviceset="TESTPOINT" device="_(1.5MM)"/>
</parts>
<sheets>
<sheet>
<plain>
<text x="190.5" y="17.78" size="2.54" layer="94">EDU:BIT</text>
<text x="269.24" y="10.16" size="2.54" layer="94">1.0.0</text>
<text x="2.54" y="185.42" size="2.54" layer="94">POWER SUPPLY</text>
<wire x1="0" y1="132.08" x2="177.8" y2="132.08" width="0.4064" layer="94"/>
<text x="2.54" y="127" size="2.54" layer="94">GROVE &amp; OLED CONNECTORS</text>
<wire x1="177.8" y1="132.08" x2="279.4" y2="132.08" width="0.4064" layer="94"/>
<wire x1="177.8" y1="132.08" x2="177.8" y2="22.86" width="0.4064" layer="94"/>
<text x="180.34" y="127" size="2.54" layer="94">MICRO:BIT</text>
</plain>
<instances>
<instance part="FRAME1" gate="G$1" x="0" y="0"/>
<instance part="FRAME1" gate="G$2" x="177.8" y="0"/>
<instance part="LOGO1" gate="G$1" x="251.46" y="25.4"/>
<instance part="U1" gate="G$1" x="152.4" y="162.56"/>
<instance part="GND25" gate="1" x="139.7" y="147.32"/>
<instance part="C7" gate="G$1" x="134.62" y="160.02"/>
<instance part="L1" gate="G$1" x="152.4" y="172.72"/>
<instance part="GND26" gate="1" x="134.62" y="147.32"/>
<instance part="C11" gate="G$1" x="170.18" y="172.72" smashed="yes" rot="R270">
<attribute name="NAME" x="168.91" y="175.26" size="1.778" layer="95"/>
<attribute name="VALUE" x="166.37" y="168.402" size="1.778" layer="96"/>
</instance>
<instance part="D2" gate="G$1" x="200.66" y="172.72" smashed="yes">
<attribute name="NAME" x="198.12" y="175.006" size="1.778" layer="95"/>
<attribute name="MPN" x="195.58" y="170.434" size="1.778" layer="97" align="top-left"/>
</instance>
<instance part="L2" gate="G$1" x="180.34" y="162.56" rot="R90"/>
<instance part="GND27" gate="1" x="180.34" y="147.32"/>
<instance part="R6" gate="G$1" x="220.98" y="165.1" rot="R90"/>
<instance part="R7" gate="G$1" x="220.98" y="154.94" rot="R90"/>
<instance part="GND28" gate="1" x="220.98" y="147.32"/>
<instance part="GND29" gate="1" x="233.68" y="147.32"/>
<instance part="U$1" gate="G$1" x="264.16" y="175.26"/>
<instance part="Q2" gate="G$1" x="71.12" y="172.72" smashed="yes" rot="MR90">
<attribute name="NAME" x="66.04" y="180.594" size="1.778" layer="95" rot="MR180"/>
<attribute name="MPN" x="66.04" y="178.054" size="1.778" layer="97" rot="MR180"/>
</instance>
<instance part="S1" gate="G$1" x="48.26" y="147.32" smashed="yes" rot="MR0">
<attribute name="NAME" x="50.8" y="147.32" size="1.778" layer="95" rot="MR0" align="top-right"/>
<attribute name="LABEL" x="48.26" y="137.16" size="1.778" layer="97" rot="MR0" align="top-right"/>
</instance>
<instance part="Q1" gate="G$1" x="27.94" y="172.72" smashed="yes" rot="R90">
<attribute name="NAME" x="33.02" y="180.594" size="1.778" layer="95" rot="R180"/>
<attribute name="MPN" x="33.02" y="178.054" size="1.778" layer="97" rot="R180"/>
</instance>
<instance part="Q3" gate="G$1" x="76.2" y="152.4" smashed="yes" rot="MR270">
<attribute name="NAME" x="73.66" y="152.4" size="1.778" layer="95" rot="MR180"/>
<attribute name="MPN" x="73.66" y="149.86" size="1.778" layer="97" rot="MR180"/>
</instance>
<instance part="R3" gate="G$1" x="63.5" y="152.4"/>
<instance part="GND10" gate="1" x="91.44" y="149.86"/>
<instance part="R4" gate="G$1" x="76.2" y="165.1" rot="R90"/>
<instance part="R5" gate="G$1" x="83.82" y="157.48"/>
<instance part="R1" gate="G$1" x="55.88" y="167.64" rot="R90"/>
<instance part="C6" gate="G$1" x="124.46" y="162.56"/>
<instance part="GND2" gate="1" x="124.46" y="147.32"/>
<instance part="CON1" gate="G$1" x="213.36" y="81.28"/>
<instance part="GND15" gate="1" x="241.3" y="99.06"/>
<instance part="U$9" gate="G$1" x="266.7" y="83.82" rot="R270"/>
<instance part="C13" gate="G$1" x="241.3" y="78.74" smashed="yes">
<attribute name="NAME" x="238.76" y="77.47" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="245.618" y="77.47" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND24" gate="1" x="241.3" y="73.66"/>
<instance part="JP6" gate="G$1" x="78.74" y="25.4" smashed="yes">
<attribute name="NAME" x="76.2" y="33.782" size="1.778" layer="95" font="vector"/>
<attribute name="LABEL" x="68.58" y="19.558" size="1.778" layer="97" font="vector" align="top-left"/>
</instance>
<instance part="GND30" gate="1" x="88.9" y="17.78"/>
<instance part="JP1" gate="G$1" x="12.7" y="96.52"/>
<instance part="GND32" gate="1" x="22.86" y="88.9"/>
<instance part="U$11" gate="G$1" x="25.4" y="106.68"/>
<instance part="C1" gate="G$1" x="30.48" y="104.14" rot="R270"/>
<instance part="GND33" gate="1" x="40.64" y="101.6"/>
<instance part="JP2" gate="G$1" x="12.7" y="60.96"/>
<instance part="GND34" gate="1" x="22.86" y="53.34"/>
<instance part="U$12" gate="G$1" x="25.4" y="71.12"/>
<instance part="C2" gate="G$1" x="30.48" y="68.58" rot="R270"/>
<instance part="GND35" gate="1" x="40.64" y="66.04"/>
<instance part="JP3" gate="G$1" x="12.7" y="25.4"/>
<instance part="GND36" gate="1" x="22.86" y="17.78"/>
<instance part="U$13" gate="G$1" x="25.4" y="35.56"/>
<instance part="C3" gate="G$1" x="30.48" y="33.02" rot="R270"/>
<instance part="GND37" gate="1" x="40.64" y="30.48"/>
<instance part="JP4" gate="G$1" x="78.74" y="96.52"/>
<instance part="GND38" gate="1" x="88.9" y="88.9"/>
<instance part="U$14" gate="G$1" x="91.44" y="106.68"/>
<instance part="C4" gate="G$1" x="96.52" y="104.14" rot="R270"/>
<instance part="GND39" gate="1" x="106.68" y="101.6"/>
<instance part="JP5" gate="G$1" x="78.74" y="60.96"/>
<instance part="GND40" gate="1" x="88.9" y="53.34"/>
<instance part="U$15" gate="G$1" x="91.44" y="71.12"/>
<instance part="C5" gate="G$1" x="96.52" y="68.58" rot="R270"/>
<instance part="GND41" gate="1" x="106.68" y="66.04"/>
<instance part="JP7" gate="G$1" x="134.62" y="96.52" smashed="yes">
<attribute name="NAME" x="132.08" y="104.902" size="1.778" layer="95" font="vector"/>
<attribute name="LABEL" x="127" y="90.678" size="1.778" layer="97" font="vector" align="top-left"/>
</instance>
<instance part="GND42" gate="1" x="144.78" y="88.9"/>
<instance part="U$16" gate="G$1" x="147.32" y="106.68"/>
<instance part="C9" gate="G$1" x="152.4" y="104.14" rot="R270"/>
<instance part="GND43" gate="1" x="162.56" y="101.6"/>
<instance part="JP8" gate="G$1" x="134.62" y="60.96"/>
<instance part="GND109" gate="1" x="144.78" y="53.34"/>
<instance part="U$47" gate="G$1" x="147.32" y="71.12"/>
<instance part="C10" gate="G$1" x="152.4" y="68.58" rot="R270"/>
<instance part="GND110" gate="1" x="162.56" y="66.04"/>
<instance part="U$48" gate="G$1" x="144.78" y="35.56"/>
<instance part="C8" gate="G$1" x="149.86" y="33.02" rot="R270"/>
<instance part="GND111" gate="1" x="144.78" y="17.78"/>
<instance part="GND112" gate="1" x="160.02" y="30.48"/>
<instance part="GND113" gate="1" x="20.32" y="137.16"/>
<instance part="TP12" gate="G$1" x="264.16" y="172.72"/>
<instance part="TP1" gate="G$1" x="20.32" y="93.98"/>
<instance part="TP2" gate="G$1" x="20.32" y="58.42"/>
<instance part="TP3" gate="G$1" x="20.32" y="22.86"/>
<instance part="TP8" gate="G$1" x="86.36" y="25.4"/>
<instance part="TP5" gate="G$1" x="86.36" y="93.98"/>
<instance part="TP4" gate="G$1" x="86.36" y="96.52"/>
<instance part="TP6" gate="G$1" x="86.36" y="58.42"/>
<instance part="TP7" gate="G$1" x="86.36" y="27.94"/>
<instance part="TP9" gate="G$1" x="86.36" y="22.86"/>
<instance part="TP11" gate="G$1" x="142.24" y="93.98"/>
<instance part="TP10" gate="G$1" x="142.24" y="96.52"/>
<instance part="R10" gate="G$1" x="264.16" y="165.1" rot="R270"/>
<instance part="DS1" gate="G$1" x="264.16" y="157.48" smashed="yes" rot="R270">
<attribute name="NAME" x="267.97" y="156.21" size="1.778" layer="95" rot="R270" align="bottom-center"/>
<attribute name="LABEL" x="262.128" y="156.21" size="1.778" layer="97" rot="R270" align="top-center"/>
</instance>
<instance part="GND9" gate="1" x="264.16" y="147.32"/>
<instance part="DC1" gate="G$1" x="12.7" y="160.02"/>
<instance part="VIN1" gate="G$1" x="81.28" y="172.72" rot="R270"/>
<instance part="D1" gate="G$1" x="114.3" y="172.72" smashed="yes">
<attribute name="NAME" x="111.76" y="175.006" size="1.778" layer="95"/>
<attribute name="MPN" x="109.22" y="170.434" size="1.778" layer="97" align="top-left"/>
</instance>
<instance part="VIN2" gate="G$1" x="109.22" y="172.72" rot="R90"/>
<instance part="R2" gate="G$1" x="55.88" y="157.48" rot="R90"/>
<instance part="GND123" gate="1" x="261.62" y="53.34"/>
<instance part="Q6" gate="G$1" x="261.62" y="83.82" smashed="yes" rot="MR90">
<attribute name="NAME" x="256.54" y="91.44" size="1.778" layer="95" rot="MR180"/>
<attribute name="MPN" x="256.54" y="88.9" size="1.778" layer="97" rot="MR180"/>
</instance>
<instance part="Q7" gate="G$1" x="261.62" y="63.5" smashed="yes" rot="MR0">
<attribute name="NAME" x="261.62" y="66.04" size="1.778" layer="95" rot="MR270"/>
<attribute name="MPN" x="259.08" y="71.12" size="1.778" layer="97" rot="MR270"/>
</instance>
<instance part="R9" gate="G$1" x="254" y="78.74" rot="R90"/>
<instance part="R11" gate="G$1" x="269.24" y="68.58" rot="R90"/>
<instance part="C12" gate="G$1" x="233.68" y="160.02"/>
<instance part="JP9" gate="G$1" x="134.62" y="25.4"/>
<instance part="C33" gate="G$1" x="248.92" y="157.48"/>
<instance part="GND31" gate="1" x="248.92" y="147.32"/>
<instance part="VIN6" gate="G$1" x="269.24" y="73.66"/>
<instance part="C34" gate="G$1" x="269.24" y="58.42"/>
<instance part="GND99" gate="1" x="269.24" y="53.34"/>
<instance part="L3" gate="G$1" x="187.96" y="172.72" smashed="yes">
<attribute name="NAME" x="185.42" y="174.752" size="1.778" layer="95"/>
</instance>
<instance part="L4" gate="G$1" x="210.82" y="172.72" smashed="yes">
<attribute name="NAME" x="208.28" y="174.752" size="1.778" layer="95"/>
</instance>
<instance part="TP13" gate="G$1" x="142.24" y="60.96"/>
<instance part="TP14" gate="G$1" x="142.24" y="58.42"/>
<instance part="TP48" gate="G$1" x="264.16" y="160.02"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="GND"/>
<pinref part="GND25" gate="1" pin="GND"/>
<wire x1="142.24" y1="160.02" x2="139.7" y2="160.02" width="0.1524" layer="91"/>
<wire x1="139.7" y1="160.02" x2="139.7" y2="149.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND26" gate="1" pin="GND"/>
<pinref part="C7" gate="G$1" pin="2"/>
<wire x1="134.62" y1="149.86" x2="134.62" y2="157.48" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="L2" gate="G$1" pin="1"/>
<pinref part="GND27" gate="1" pin="GND"/>
<wire x1="180.34" y1="149.86" x2="180.34" y2="157.48" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R7" gate="G$1" pin="1"/>
<pinref part="GND28" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND29" gate="1" pin="GND"/>
<wire x1="233.68" y1="149.86" x2="233.68" y2="154.94" width="0.1524" layer="91"/>
<pinref part="C12" gate="G$1" pin="-"/>
</segment>
<segment>
<pinref part="S1" gate="G$1" pin="3"/>
<wire x1="40.64" y1="149.86" x2="27.94" y2="149.86" width="0.1524" layer="91"/>
<wire x1="27.94" y1="149.86" x2="27.94" y2="139.7" width="0.1524" layer="91"/>
<pinref part="S1" gate="G$1" pin="6"/>
<wire x1="40.64" y1="139.7" x2="27.94" y2="139.7" width="0.1524" layer="91"/>
<pinref part="GND113" gate="1" pin="GND"/>
<junction x="27.94" y="139.7"/>
<wire x1="27.94" y1="139.7" x2="20.32" y2="139.7" width="0.1524" layer="91"/>
<pinref part="DC1" gate="G$1" pin="GND"/>
<wire x1="17.78" y1="157.48" x2="20.32" y2="157.48" width="0.1524" layer="91"/>
<wire x1="20.32" y1="157.48" x2="20.32" y2="139.7" width="0.1524" layer="91"/>
<junction x="20.32" y="139.7"/>
</segment>
<segment>
<pinref part="Q3" gate="G$1" pin="E"/>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="81.28" y1="152.4" x2="91.44" y2="152.4" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="88.9" y1="157.48" x2="91.44" y2="157.48" width="0.1524" layer="91"/>
<wire x1="91.44" y1="157.48" x2="91.44" y2="152.4" width="0.1524" layer="91"/>
<junction x="91.44" y="152.4"/>
</segment>
<segment>
<pinref part="C6" gate="G$1" pin="-"/>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="124.46" y1="157.48" x2="124.46" y2="149.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C13" gate="G$1" pin="2"/>
<pinref part="GND24" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="CON1" gate="G$1" pin="GND@40"/>
<pinref part="GND15" gate="1" pin="GND"/>
<wire x1="233.68" y1="104.14" x2="236.22" y2="104.14" width="0.1524" layer="91"/>
<wire x1="236.22" y1="104.14" x2="241.3" y2="104.14" width="0.1524" layer="91"/>
<wire x1="241.3" y1="104.14" x2="241.3" y2="101.6" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="GND@35"/>
<wire x1="233.68" y1="91.44" x2="236.22" y2="91.44" width="0.1524" layer="91"/>
<wire x1="236.22" y1="91.44" x2="236.22" y2="93.98" width="0.1524" layer="91"/>
<junction x="236.22" y="104.14"/>
<pinref part="CON1" gate="G$1" pin="GND@39"/>
<wire x1="236.22" y1="93.98" x2="236.22" y2="96.52" width="0.1524" layer="91"/>
<wire x1="236.22" y1="96.52" x2="236.22" y2="99.06" width="0.1524" layer="91"/>
<wire x1="236.22" y1="99.06" x2="236.22" y2="101.6" width="0.1524" layer="91"/>
<wire x1="236.22" y1="101.6" x2="236.22" y2="104.14" width="0.1524" layer="91"/>
<wire x1="233.68" y1="101.6" x2="236.22" y2="101.6" width="0.1524" layer="91"/>
<junction x="236.22" y="101.6"/>
<pinref part="CON1" gate="G$1" pin="GND@38"/>
<wire x1="233.68" y1="99.06" x2="236.22" y2="99.06" width="0.1524" layer="91"/>
<junction x="236.22" y="99.06"/>
<pinref part="CON1" gate="G$1" pin="GND@37"/>
<wire x1="233.68" y1="96.52" x2="236.22" y2="96.52" width="0.1524" layer="91"/>
<junction x="236.22" y="96.52"/>
<pinref part="CON1" gate="G$1" pin="GND@36"/>
<wire x1="233.68" y1="93.98" x2="236.22" y2="93.98" width="0.1524" layer="91"/>
<junction x="236.22" y="93.98"/>
</segment>
<segment>
<pinref part="JP6" gate="G$1" pin="1"/>
<pinref part="GND30" gate="1" pin="GND"/>
<wire x1="86.36" y1="30.48" x2="88.9" y2="30.48" width="0.1524" layer="91"/>
<wire x1="88.9" y1="30.48" x2="88.9" y2="20.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP1" gate="G$1" pin="1"/>
<pinref part="GND32" gate="1" pin="GND"/>
<wire x1="20.32" y1="101.6" x2="22.86" y2="101.6" width="0.1524" layer="91"/>
<wire x1="22.86" y1="101.6" x2="22.86" y2="91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="1"/>
<pinref part="GND33" gate="1" pin="GND"/>
<wire x1="35.56" y1="104.14" x2="40.64" y2="104.14" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP2" gate="G$1" pin="1"/>
<pinref part="GND34" gate="1" pin="GND"/>
<wire x1="20.32" y1="66.04" x2="22.86" y2="66.04" width="0.1524" layer="91"/>
<wire x1="22.86" y1="66.04" x2="22.86" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C2" gate="G$1" pin="1"/>
<pinref part="GND35" gate="1" pin="GND"/>
<wire x1="35.56" y1="68.58" x2="40.64" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP3" gate="G$1" pin="1"/>
<pinref part="GND36" gate="1" pin="GND"/>
<wire x1="20.32" y1="30.48" x2="22.86" y2="30.48" width="0.1524" layer="91"/>
<wire x1="22.86" y1="30.48" x2="22.86" y2="20.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C3" gate="G$1" pin="1"/>
<pinref part="GND37" gate="1" pin="GND"/>
<wire x1="35.56" y1="33.02" x2="40.64" y2="33.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP4" gate="G$1" pin="1"/>
<pinref part="GND38" gate="1" pin="GND"/>
<wire x1="86.36" y1="101.6" x2="88.9" y2="101.6" width="0.1524" layer="91"/>
<wire x1="88.9" y1="101.6" x2="88.9" y2="91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C4" gate="G$1" pin="1"/>
<pinref part="GND39" gate="1" pin="GND"/>
<wire x1="101.6" y1="104.14" x2="106.68" y2="104.14" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP5" gate="G$1" pin="1"/>
<pinref part="GND40" gate="1" pin="GND"/>
<wire x1="86.36" y1="66.04" x2="88.9" y2="66.04" width="0.1524" layer="91"/>
<wire x1="88.9" y1="66.04" x2="88.9" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C5" gate="G$1" pin="1"/>
<pinref part="GND41" gate="1" pin="GND"/>
<wire x1="101.6" y1="68.58" x2="106.68" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP7" gate="G$1" pin="1"/>
<pinref part="GND42" gate="1" pin="GND"/>
<wire x1="142.24" y1="101.6" x2="144.78" y2="101.6" width="0.1524" layer="91"/>
<wire x1="144.78" y1="101.6" x2="144.78" y2="91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C9" gate="G$1" pin="1"/>
<pinref part="GND43" gate="1" pin="GND"/>
<wire x1="157.48" y1="104.14" x2="162.56" y2="104.14" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP8" gate="G$1" pin="1"/>
<pinref part="GND109" gate="1" pin="GND"/>
<wire x1="142.24" y1="66.04" x2="144.78" y2="66.04" width="0.1524" layer="91"/>
<wire x1="144.78" y1="66.04" x2="144.78" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C10" gate="G$1" pin="1"/>
<pinref part="GND110" gate="1" pin="GND"/>
<wire x1="157.48" y1="68.58" x2="162.56" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND111" gate="1" pin="GND"/>
<wire x1="142.24" y1="27.94" x2="144.78" y2="27.94" width="0.1524" layer="91"/>
<wire x1="144.78" y1="27.94" x2="144.78" y2="20.32" width="0.1524" layer="91"/>
<pinref part="JP9" gate="G$1" pin="4"/>
<pinref part="JP9" gate="G$1" pin="3"/>
<wire x1="127" y1="27.94" x2="142.24" y2="27.94" width="0.1524" layer="91"/>
<junction x="142.24" y="27.94"/>
</segment>
<segment>
<pinref part="C8" gate="G$1" pin="1"/>
<pinref part="GND112" gate="1" pin="GND"/>
<wire x1="154.94" y1="33.02" x2="160.02" y2="33.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="DS1" gate="G$1" pin="C"/>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="264.16" y1="152.4" x2="264.16" y2="149.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q7" gate="G$1" pin="E"/>
<pinref part="GND123" gate="1" pin="GND"/>
<wire x1="261.62" y1="55.88" x2="261.62" y2="58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C33" gate="G$1" pin="2"/>
<pinref part="GND31" gate="1" pin="GND"/>
<wire x1="248.92" y1="149.86" x2="248.92" y2="154.94" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C34" gate="G$1" pin="2"/>
<pinref part="GND99" gate="1" pin="GND"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="L1" gate="G$1" pin="2"/>
<pinref part="C11" gate="G$1" pin="2"/>
<wire x1="167.64" y1="172.72" x2="165.1" y2="172.72" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="SW"/>
<wire x1="165.1" y1="172.72" x2="160.02" y2="172.72" width="0.1524" layer="91"/>
<wire x1="162.56" y1="165.1" x2="165.1" y2="165.1" width="0.1524" layer="91"/>
<wire x1="165.1" y1="165.1" x2="165.1" y2="172.72" width="0.1524" layer="91"/>
<junction x="165.1" y="172.72"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="175.26" y1="172.72" x2="180.34" y2="172.72" width="0.1524" layer="91"/>
<pinref part="L2" gate="G$1" pin="2"/>
<wire x1="180.34" y1="170.18" x2="180.34" y2="172.72" width="0.1524" layer="91"/>
<pinref part="L3" gate="G$1" pin="1"/>
<wire x1="182.88" y1="172.72" x2="180.34" y2="172.72" width="0.1524" layer="91"/>
<junction x="180.34" y="172.72"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="R6" gate="G$1" pin="1"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="220.98" y1="160.02" x2="187.96" y2="160.02" width="0.1524" layer="91"/>
<wire x1="187.96" y1="160.02" x2="187.96" y2="154.94" width="0.1524" layer="91"/>
<junction x="220.98" y="160.02"/>
<wire x1="187.96" y1="154.94" x2="165.1" y2="154.94" width="0.1524" layer="91"/>
<wire x1="165.1" y1="154.94" x2="165.1" y2="160.02" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="FB"/>
<wire x1="165.1" y1="160.02" x2="162.56" y2="160.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="R10" gate="G$1" pin="1"/>
<pinref part="U$1" gate="G$1" pin="+3V3"/>
<wire x1="264.16" y1="172.72" x2="264.16" y2="175.26" width="0.1524" layer="91"/>
<junction x="264.16" y="172.72"/>
<pinref part="TP12" gate="G$1" pin="P$1"/>
<wire x1="264.16" y1="172.72" x2="264.16" y2="170.18" width="0.1524" layer="91"/>
<wire x1="264.16" y1="172.72" x2="248.92" y2="172.72" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="248.92" y1="172.72" x2="233.68" y2="172.72" width="0.1524" layer="91"/>
<wire x1="218.44" y1="172.72" x2="220.98" y2="172.72" width="0.1524" layer="91"/>
<wire x1="220.98" y1="172.72" x2="233.68" y2="172.72" width="0.1524" layer="91"/>
<wire x1="233.68" y1="162.56" x2="233.68" y2="172.72" width="0.1524" layer="91"/>
<junction x="233.68" y="172.72"/>
<pinref part="C12" gate="G$1" pin="+"/>
<pinref part="C33" gate="G$1" pin="1"/>
<wire x1="248.92" y1="162.56" x2="248.92" y2="172.72" width="0.1524" layer="91"/>
<junction x="248.92" y="172.72"/>
<pinref part="L4" gate="G$1" pin="2"/>
<wire x1="220.98" y1="170.18" x2="220.98" y2="172.72" width="0.1524" layer="91"/>
<junction x="220.98" y="172.72"/>
</segment>
<segment>
<pinref part="U$11" gate="G$1" pin="+3V3"/>
<wire x1="25.4" y1="106.68" x2="25.4" y2="104.14" width="0.1524" layer="91"/>
<pinref part="JP1" gate="G$1" pin="2"/>
<wire x1="25.4" y1="104.14" x2="25.4" y2="99.06" width="0.1524" layer="91"/>
<wire x1="25.4" y1="99.06" x2="20.32" y2="99.06" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="27.94" y1="104.14" x2="25.4" y2="104.14" width="0.1524" layer="91"/>
<junction x="25.4" y="104.14"/>
</segment>
<segment>
<pinref part="U$12" gate="G$1" pin="+3V3"/>
<wire x1="25.4" y1="71.12" x2="25.4" y2="68.58" width="0.1524" layer="91"/>
<pinref part="JP2" gate="G$1" pin="2"/>
<wire x1="25.4" y1="68.58" x2="25.4" y2="63.5" width="0.1524" layer="91"/>
<wire x1="25.4" y1="63.5" x2="20.32" y2="63.5" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="27.94" y1="68.58" x2="25.4" y2="68.58" width="0.1524" layer="91"/>
<junction x="25.4" y="68.58"/>
</segment>
<segment>
<pinref part="U$13" gate="G$1" pin="+3V3"/>
<wire x1="25.4" y1="35.56" x2="25.4" y2="33.02" width="0.1524" layer="91"/>
<pinref part="JP3" gate="G$1" pin="2"/>
<wire x1="25.4" y1="33.02" x2="25.4" y2="27.94" width="0.1524" layer="91"/>
<wire x1="25.4" y1="27.94" x2="20.32" y2="27.94" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="27.94" y1="33.02" x2="25.4" y2="33.02" width="0.1524" layer="91"/>
<junction x="25.4" y="33.02"/>
</segment>
<segment>
<pinref part="U$14" gate="G$1" pin="+3V3"/>
<wire x1="91.44" y1="106.68" x2="91.44" y2="104.14" width="0.1524" layer="91"/>
<pinref part="JP4" gate="G$1" pin="2"/>
<wire x1="91.44" y1="104.14" x2="91.44" y2="99.06" width="0.1524" layer="91"/>
<wire x1="91.44" y1="99.06" x2="86.36" y2="99.06" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="93.98" y1="104.14" x2="91.44" y2="104.14" width="0.1524" layer="91"/>
<junction x="91.44" y="104.14"/>
</segment>
<segment>
<pinref part="U$15" gate="G$1" pin="+3V3"/>
<wire x1="91.44" y1="71.12" x2="91.44" y2="68.58" width="0.1524" layer="91"/>
<pinref part="JP5" gate="G$1" pin="2"/>
<wire x1="91.44" y1="68.58" x2="91.44" y2="63.5" width="0.1524" layer="91"/>
<wire x1="91.44" y1="63.5" x2="86.36" y2="63.5" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="93.98" y1="68.58" x2="91.44" y2="68.58" width="0.1524" layer="91"/>
<junction x="91.44" y="68.58"/>
</segment>
<segment>
<pinref part="U$16" gate="G$1" pin="+3V3"/>
<wire x1="147.32" y1="106.68" x2="147.32" y2="104.14" width="0.1524" layer="91"/>
<pinref part="JP7" gate="G$1" pin="2"/>
<wire x1="147.32" y1="104.14" x2="147.32" y2="99.06" width="0.1524" layer="91"/>
<wire x1="147.32" y1="99.06" x2="142.24" y2="99.06" width="0.1524" layer="91"/>
<pinref part="C9" gate="G$1" pin="2"/>
<wire x1="149.86" y1="104.14" x2="147.32" y2="104.14" width="0.1524" layer="91"/>
<junction x="147.32" y="104.14"/>
</segment>
<segment>
<pinref part="U$47" gate="G$1" pin="+3V3"/>
<wire x1="147.32" y1="71.12" x2="147.32" y2="68.58" width="0.1524" layer="91"/>
<pinref part="JP8" gate="G$1" pin="2"/>
<wire x1="147.32" y1="68.58" x2="147.32" y2="63.5" width="0.1524" layer="91"/>
<wire x1="147.32" y1="63.5" x2="142.24" y2="63.5" width="0.1524" layer="91"/>
<pinref part="C10" gate="G$1" pin="2"/>
<wire x1="149.86" y1="68.58" x2="147.32" y2="68.58" width="0.1524" layer="91"/>
<junction x="147.32" y="68.58"/>
</segment>
<segment>
<pinref part="U$48" gate="G$1" pin="+3V3"/>
<wire x1="144.78" y1="35.56" x2="144.78" y2="33.02" width="0.1524" layer="91"/>
<wire x1="144.78" y1="33.02" x2="144.78" y2="30.48" width="0.1524" layer="91"/>
<wire x1="144.78" y1="30.48" x2="142.24" y2="30.48" width="0.1524" layer="91"/>
<pinref part="C8" gate="G$1" pin="2"/>
<wire x1="147.32" y1="33.02" x2="144.78" y2="33.02" width="0.1524" layer="91"/>
<junction x="144.78" y="33.02"/>
<pinref part="JP9" gate="G$1" pin="2"/>
<pinref part="JP9" gate="G$1" pin="1"/>
<wire x1="127" y1="30.48" x2="142.24" y2="30.48" width="0.1524" layer="91"/>
<junction x="142.24" y="30.48"/>
</segment>
<segment>
<pinref part="Q6" gate="G$1" pin="D"/>
<pinref part="U$9" gate="G$1" pin="+3V3"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="Q1" gate="G$1" pin="G"/>
<wire x1="27.94" y1="165.1" x2="27.94" y2="162.56" width="0.1524" layer="91"/>
<pinref part="Q2" gate="G$1" pin="G"/>
<wire x1="71.12" y1="162.56" x2="71.12" y2="165.1" width="0.1524" layer="91"/>
<wire x1="27.94" y1="162.56" x2="55.88" y2="162.56" width="0.1524" layer="91"/>
<wire x1="55.88" y1="162.56" x2="71.12" y2="162.56" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="1"/>
<junction x="55.88" y="162.56"/>
<pinref part="R2" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="Q1" gate="G$1" pin="D"/>
<wire x1="20.32" y1="172.72" x2="22.86" y2="172.72" width="0.1524" layer="91"/>
<pinref part="DC1" gate="G$1" pin="PWR"/>
<wire x1="17.78" y1="162.56" x2="20.32" y2="162.56" width="0.1524" layer="91"/>
<wire x1="20.32" y1="162.56" x2="20.32" y2="172.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="Q1" gate="G$1" pin="S"/>
<pinref part="Q2" gate="G$1" pin="S"/>
<wire x1="33.02" y1="172.72" x2="38.1" y2="172.72" width="0.1524" layer="91"/>
<wire x1="38.1" y1="172.72" x2="55.88" y2="172.72" width="0.1524" layer="91"/>
<wire x1="55.88" y1="172.72" x2="66.04" y2="172.72" width="0.1524" layer="91"/>
<wire x1="38.1" y1="154.94" x2="38.1" y2="172.72" width="0.1524" layer="91"/>
<junction x="38.1" y="172.72"/>
<pinref part="S1" gate="G$1" pin="1"/>
<wire x1="38.1" y1="154.94" x2="40.64" y2="154.94" width="0.1524" layer="91"/>
<wire x1="38.1" y1="154.94" x2="38.1" y2="144.78" width="0.1524" layer="91"/>
<junction x="38.1" y="154.94"/>
<pinref part="S1" gate="G$1" pin="4"/>
<wire x1="38.1" y1="144.78" x2="40.64" y2="144.78" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="2"/>
<junction x="55.88" y="172.72"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="2"/>
<pinref part="Q3" gate="G$1" pin="C"/>
<wire x1="68.58" y1="152.4" x2="71.12" y2="152.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="1"/>
<pinref part="Q3" gate="G$1" pin="B"/>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="76.2" y1="157.48" x2="78.74" y2="157.48" width="0.1524" layer="91"/>
<junction x="76.2" y="157.48"/>
<wire x1="76.2" y1="157.48" x2="76.2" y2="160.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<pinref part="CON1" gate="G$1" pin="P20/SDA@34"/>
<wire x1="233.68" y1="88.9" x2="243.84" y2="88.9" width="0.1524" layer="91"/>
<label x="238.76" y="88.9" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="JP8" gate="G$1" pin="3"/>
<wire x1="142.24" y1="60.96" x2="154.94" y2="60.96" width="0.1524" layer="91"/>
<label x="149.86" y="60.96" size="1.778" layer="95"/>
<pinref part="TP13" gate="G$1" pin="P$1"/>
<junction x="142.24" y="60.96"/>
</segment>
<segment>
<wire x1="142.24" y1="22.86" x2="154.94" y2="22.86" width="0.1524" layer="91"/>
<label x="149.86" y="22.86" size="1.778" layer="95"/>
<pinref part="JP9" gate="G$1" pin="8"/>
<pinref part="JP9" gate="G$1" pin="7"/>
<wire x1="127" y1="22.86" x2="142.24" y2="22.86" width="0.1524" layer="91"/>
<junction x="142.24" y="22.86"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<pinref part="CON1" gate="G$1" pin="P19/SCL@33"/>
<wire x1="233.68" y1="86.36" x2="243.84" y2="86.36" width="0.1524" layer="91"/>
<label x="238.76" y="86.36" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="JP8" gate="G$1" pin="4"/>
<wire x1="142.24" y1="58.42" x2="154.94" y2="58.42" width="0.1524" layer="91"/>
<label x="149.86" y="58.42" size="1.778" layer="95"/>
<pinref part="TP14" gate="G$1" pin="P$1"/>
<junction x="142.24" y="58.42"/>
</segment>
<segment>
<wire x1="142.24" y1="25.4" x2="154.94" y2="25.4" width="0.1524" layer="91"/>
<label x="149.86" y="25.4" size="1.778" layer="95"/>
<pinref part="JP9" gate="G$1" pin="6"/>
<pinref part="JP9" gate="G$1" pin="5"/>
<wire x1="127" y1="25.4" x2="142.24" y2="25.4" width="0.1524" layer="91"/>
<junction x="142.24" y="25.4"/>
</segment>
</net>
<net name="P2" class="0">
<segment>
<pinref part="CON1" gate="G$1" pin="P2/AN@19"/>
<wire x1="193.04" y1="58.42" x2="190.5" y2="58.42" width="0.1524" layer="91"/>
<label x="182.88" y="58.42" size="1.778" layer="95"/>
<pinref part="CON1" gate="G$1" pin="P2/AN@20"/>
<wire x1="190.5" y1="58.42" x2="182.88" y2="58.42" width="0.1524" layer="91"/>
<wire x1="193.04" y1="55.88" x2="190.5" y2="55.88" width="0.1524" layer="91"/>
<wire x1="190.5" y1="55.88" x2="190.5" y2="58.42" width="0.1524" layer="91"/>
<junction x="190.5" y="58.42"/>
<wire x1="190.5" y1="55.88" x2="190.5" y2="48.26" width="0.1524" layer="91"/>
<junction x="190.5" y="55.88"/>
<wire x1="190.5" y1="48.26" x2="236.22" y2="48.26" width="0.1524" layer="91"/>
<wire x1="236.22" y1="48.26" x2="236.22" y2="55.88" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="P2/AN@22"/>
<wire x1="236.22" y1="55.88" x2="236.22" y2="58.42" width="0.1524" layer="91"/>
<wire x1="236.22" y1="58.42" x2="233.68" y2="58.42" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="P2/AN@21"/>
<wire x1="233.68" y1="55.88" x2="236.22" y2="55.88" width="0.1524" layer="91"/>
<junction x="236.22" y="55.88"/>
</segment>
<segment>
<pinref part="JP3" gate="G$1" pin="4"/>
<wire x1="20.32" y1="22.86" x2="33.02" y2="22.86" width="0.1524" layer="91"/>
<label x="27.94" y="22.86" size="1.778" layer="95"/>
<pinref part="TP3" gate="G$1" pin="P$1"/>
<junction x="20.32" y="22.86"/>
</segment>
</net>
<net name="P0" class="0">
<segment>
<pinref part="CON1" gate="G$1" pin="P0/AN@2"/>
<wire x1="193.04" y1="101.6" x2="190.5" y2="101.6" width="0.1524" layer="91"/>
<wire x1="190.5" y1="101.6" x2="190.5" y2="99.06" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="P0/AN@5"/>
<wire x1="190.5" y1="99.06" x2="190.5" y2="96.52" width="0.1524" layer="91"/>
<wire x1="190.5" y1="96.52" x2="190.5" y2="93.98" width="0.1524" layer="91"/>
<wire x1="190.5" y1="93.98" x2="193.04" y2="93.98" width="0.1524" layer="91"/>
<wire x1="190.5" y1="101.6" x2="182.88" y2="101.6" width="0.1524" layer="91"/>
<junction x="190.5" y="101.6"/>
<label x="182.88" y="101.6" size="1.778" layer="95"/>
<pinref part="CON1" gate="G$1" pin="P0/AN@3"/>
<wire x1="193.04" y1="99.06" x2="190.5" y2="99.06" width="0.1524" layer="91"/>
<junction x="190.5" y="99.06"/>
<pinref part="CON1" gate="G$1" pin="P0/AN@4"/>
<wire x1="193.04" y1="96.52" x2="190.5" y2="96.52" width="0.1524" layer="91"/>
<junction x="190.5" y="96.52"/>
</segment>
<segment>
<pinref part="JP1" gate="G$1" pin="4"/>
<wire x1="20.32" y1="93.98" x2="33.02" y2="93.98" width="0.1524" layer="91"/>
<label x="27.94" y="93.98" size="1.778" layer="95"/>
<pinref part="TP1" gate="G$1" pin="P$1"/>
<junction x="20.32" y="93.98"/>
</segment>
</net>
<net name="P1" class="0">
<segment>
<pinref part="CON1" gate="G$1" pin="P1/AN@10"/>
<wire x1="193.04" y1="81.28" x2="190.5" y2="81.28" width="0.1524" layer="91"/>
<wire x1="190.5" y1="81.28" x2="190.5" y2="78.74" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="P1/AN@13"/>
<wire x1="190.5" y1="78.74" x2="190.5" y2="76.2" width="0.1524" layer="91"/>
<wire x1="190.5" y1="76.2" x2="190.5" y2="73.66" width="0.1524" layer="91"/>
<wire x1="190.5" y1="73.66" x2="193.04" y2="73.66" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="P1/AN@12"/>
<wire x1="193.04" y1="76.2" x2="190.5" y2="76.2" width="0.1524" layer="91"/>
<junction x="190.5" y="76.2"/>
<pinref part="CON1" gate="G$1" pin="P1/AN@11"/>
<wire x1="193.04" y1="78.74" x2="190.5" y2="78.74" width="0.1524" layer="91"/>
<junction x="190.5" y="78.74"/>
<wire x1="190.5" y1="81.28" x2="182.88" y2="81.28" width="0.1524" layer="91"/>
<junction x="190.5" y="81.28"/>
<label x="182.88" y="81.28" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="JP2" gate="G$1" pin="4"/>
<wire x1="20.32" y1="58.42" x2="33.02" y2="58.42" width="0.1524" layer="91"/>
<label x="27.94" y="58.42" size="1.778" layer="95"/>
<pinref part="TP2" gate="G$1" pin="P$1"/>
<junction x="20.32" y="58.42"/>
</segment>
</net>
<net name="P8" class="0">
<segment>
<pinref part="CON1" gate="G$1" pin="P8@14"/>
<wire x1="193.04" y1="71.12" x2="182.88" y2="71.12" width="0.1524" layer="91"/>
<label x="182.88" y="71.12" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="JP4" gate="G$1" pin="4"/>
<wire x1="86.36" y1="93.98" x2="99.06" y2="93.98" width="0.1524" layer="91"/>
<label x="93.98" y="93.98" size="1.778" layer="95"/>
<pinref part="TP5" gate="G$1" pin="P$1"/>
<junction x="86.36" y="93.98"/>
</segment>
</net>
<net name="P12" class="0">
<segment>
<pinref part="CON1" gate="G$1" pin="P12/RESERVE@18"/>
<wire x1="193.04" y1="60.96" x2="182.88" y2="60.96" width="0.1524" layer="91"/>
<label x="182.88" y="60.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="JP4" gate="G$1" pin="3"/>
<wire x1="86.36" y1="96.52" x2="99.06" y2="96.52" width="0.1524" layer="91"/>
<label x="93.98" y="96.52" size="1.778" layer="95"/>
<pinref part="TP4" gate="G$1" pin="P$1"/>
<junction x="86.36" y="96.52"/>
</segment>
</net>
<net name="P13" class="0">
<segment>
<pinref part="CON1" gate="G$1" pin="P13/SCK@23"/>
<wire x1="233.68" y1="60.96" x2="243.84" y2="60.96" width="0.1524" layer="91"/>
<label x="238.76" y="60.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="JP5" gate="G$1" pin="4"/>
<wire x1="86.36" y1="58.42" x2="99.06" y2="58.42" width="0.1524" layer="91"/>
<label x="93.98" y="58.42" size="1.778" layer="95"/>
<pinref part="TP6" gate="G$1" pin="P$1"/>
<junction x="86.36" y="58.42"/>
</segment>
</net>
<net name="P14" class="0">
<segment>
<pinref part="CON1" gate="G$1" pin="P14/MISO@24"/>
<wire x1="233.68" y1="63.5" x2="243.84" y2="63.5" width="0.1524" layer="91"/>
<label x="238.76" y="63.5" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="JP6" gate="G$1" pin="4"/>
<wire x1="86.36" y1="22.86" x2="99.06" y2="22.86" width="0.1524" layer="91"/>
<label x="93.98" y="22.86" size="1.778" layer="95"/>
<pinref part="TP9" gate="G$1" pin="P$1"/>
<junction x="86.36" y="22.86"/>
</segment>
</net>
<net name="P15" class="0">
<segment>
<pinref part="CON1" gate="G$1" pin="P15/MOSI@25"/>
<wire x1="233.68" y1="66.04" x2="243.84" y2="66.04" width="0.1524" layer="91"/>
<label x="238.76" y="66.04" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="JP6" gate="G$1" pin="3"/>
<wire x1="86.36" y1="25.4" x2="99.06" y2="25.4" width="0.1524" layer="91"/>
<label x="93.98" y="25.4" size="1.778" layer="95"/>
<pinref part="TP8" gate="G$1" pin="P$1"/>
<junction x="86.36" y="25.4"/>
</segment>
</net>
<net name="P16" class="0">
<segment>
<pinref part="CON1" gate="G$1" pin="P16@26"/>
<wire x1="233.68" y1="68.58" x2="243.84" y2="68.58" width="0.1524" layer="91"/>
<label x="238.76" y="68.58" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="JP6" gate="G$1" pin="2"/>
<wire x1="86.36" y1="27.94" x2="99.06" y2="27.94" width="0.1524" layer="91"/>
<label x="93.98" y="27.94" size="1.778" layer="95"/>
<pinref part="TP7" gate="G$1" pin="P$1"/>
<junction x="86.36" y="27.94"/>
</segment>
</net>
<net name="BTNB" class="0">
<segment>
<pinref part="JP7" gate="G$1" pin="3"/>
<wire x1="142.24" y1="96.52" x2="154.94" y2="96.52" width="0.1524" layer="91"/>
<label x="149.86" y="96.52" size="1.778" layer="95"/>
<pinref part="TP10" gate="G$1" pin="P$1"/>
<junction x="142.24" y="96.52"/>
</segment>
<segment>
<pinref part="CON1" gate="G$1" pin="P11/BTNB@17"/>
<wire x1="193.04" y1="63.5" x2="182.88" y2="63.5" width="0.1524" layer="91"/>
<label x="182.88" y="63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="BTNA" class="0">
<segment>
<pinref part="JP7" gate="G$1" pin="4"/>
<wire x1="142.24" y1="93.98" x2="154.94" y2="93.98" width="0.1524" layer="91"/>
<label x="149.86" y="93.98" size="1.778" layer="95"/>
<pinref part="TP11" gate="G$1" pin="P$1"/>
<junction x="142.24" y="93.98"/>
</segment>
<segment>
<pinref part="CON1" gate="G$1" pin="P5/BTNA@7"/>
<wire x1="193.04" y1="88.9" x2="182.88" y2="88.9" width="0.1524" layer="91"/>
<label x="182.88" y="88.9" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="DS1" gate="G$1" pin="A"/>
<pinref part="R10" gate="G$1" pin="2"/>
<pinref part="TP48" gate="G$1" pin="P$1"/>
<junction x="264.16" y="160.02"/>
<pinref part="TP48" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="N$48" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="VIN"/>
<pinref part="U1" gate="G$1" pin="EN"/>
<wire x1="139.7" y1="165.1" x2="142.24" y2="165.1" width="0.1524" layer="91"/>
<wire x1="142.24" y1="162.56" x2="139.7" y2="162.56" width="0.1524" layer="91"/>
<wire x1="139.7" y1="162.56" x2="139.7" y2="165.1" width="0.1524" layer="91"/>
<junction x="139.7" y="165.1"/>
<wire x1="139.7" y1="165.1" x2="139.7" y2="172.72" width="0.1524" layer="91"/>
<pinref part="L1" gate="G$1" pin="1"/>
<wire x1="139.7" y1="172.72" x2="147.32" y2="172.72" width="0.1524" layer="91"/>
<junction x="139.7" y="172.72"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="134.62" y1="172.72" x2="139.7" y2="172.72" width="0.1524" layer="91"/>
<wire x1="134.62" y1="165.1" x2="134.62" y2="172.72" width="0.1524" layer="91"/>
<junction x="134.62" y="172.72"/>
<pinref part="D1" gate="G$1" pin="C"/>
<wire x1="119.38" y1="172.72" x2="124.46" y2="172.72" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="+"/>
<wire x1="124.46" y1="172.72" x2="134.62" y2="172.72" width="0.1524" layer="91"/>
<wire x1="124.46" y1="165.1" x2="124.46" y2="172.72" width="0.1524" layer="91"/>
<junction x="124.46" y="172.72"/>
</segment>
</net>
<net name="VIN" class="0">
<segment>
<pinref part="Q2" gate="G$1" pin="D"/>
<wire x1="81.28" y1="172.72" x2="76.2" y2="172.72" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="76.2" y1="170.18" x2="76.2" y2="172.72" width="0.1524" layer="91"/>
<junction x="76.2" y="172.72"/>
<pinref part="VIN1" gate="G$1" pin="VIN"/>
</segment>
<segment>
<pinref part="D1" gate="G$1" pin="A"/>
<pinref part="VIN2" gate="G$1" pin="VIN"/>
</segment>
<segment>
<pinref part="R11" gate="G$1" pin="2"/>
<pinref part="VIN6" gate="G$1" pin="VIN"/>
</segment>
</net>
<net name="N$82" class="0">
<segment>
<pinref part="S1" gate="G$1" pin="5"/>
<pinref part="R3" gate="G$1" pin="1"/>
<pinref part="S1" gate="G$1" pin="2"/>
<junction x="55.88" y="152.4"/>
<wire x1="55.88" y1="152.4" x2="58.42" y2="152.4" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="55.88" y1="152.4" x2="55.88" y2="142.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="UB3V3" class="0">
<segment>
<pinref part="C13" gate="G$1" pin="1"/>
<pinref part="CON1" gate="G$1" pin="3V3@32"/>
<wire x1="241.3" y1="83.82" x2="236.22" y2="83.82" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="3V3@27"/>
<wire x1="236.22" y1="83.82" x2="233.68" y2="83.82" width="0.1524" layer="91"/>
<wire x1="233.68" y1="71.12" x2="236.22" y2="71.12" width="0.1524" layer="91"/>
<wire x1="236.22" y1="71.12" x2="236.22" y2="73.66" width="0.1524" layer="91"/>
<junction x="236.22" y="83.82"/>
<pinref part="CON1" gate="G$1" pin="3V3@31"/>
<wire x1="236.22" y1="73.66" x2="236.22" y2="76.2" width="0.1524" layer="91"/>
<wire x1="236.22" y1="76.2" x2="236.22" y2="78.74" width="0.1524" layer="91"/>
<wire x1="236.22" y1="78.74" x2="236.22" y2="81.28" width="0.1524" layer="91"/>
<wire x1="236.22" y1="81.28" x2="236.22" y2="83.82" width="0.1524" layer="91"/>
<wire x1="233.68" y1="81.28" x2="236.22" y2="81.28" width="0.1524" layer="91"/>
<junction x="236.22" y="81.28"/>
<pinref part="CON1" gate="G$1" pin="3V3@30"/>
<wire x1="233.68" y1="78.74" x2="236.22" y2="78.74" width="0.1524" layer="91"/>
<junction x="236.22" y="78.74"/>
<pinref part="CON1" gate="G$1" pin="3V3@29"/>
<wire x1="233.68" y1="76.2" x2="236.22" y2="76.2" width="0.1524" layer="91"/>
<junction x="236.22" y="76.2"/>
<pinref part="CON1" gate="G$1" pin="3V3@28"/>
<wire x1="233.68" y1="73.66" x2="236.22" y2="73.66" width="0.1524" layer="91"/>
<junction x="236.22" y="73.66"/>
<wire x1="256.54" y1="83.82" x2="254" y2="83.82" width="0.1524" layer="91"/>
<junction x="241.3" y="83.82"/>
<pinref part="Q6" gate="G$1" pin="S"/>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="254" y1="83.82" x2="241.3" y2="83.82" width="0.1524" layer="91"/>
<junction x="254" y="83.82"/>
<label x="243.84" y="83.82" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$81" class="0">
<segment>
<pinref part="Q6" gate="G$1" pin="G"/>
<pinref part="Q7" gate="G$1" pin="C"/>
<wire x1="261.62" y1="68.58" x2="261.62" y2="73.66" width="0.1524" layer="91"/>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="261.62" y1="73.66" x2="261.62" y2="76.2" width="0.1524" layer="91"/>
<wire x1="254" y1="73.66" x2="261.62" y2="73.66" width="0.1524" layer="91"/>
<junction x="261.62" y="73.66"/>
</segment>
</net>
<net name="N$84" class="0">
<segment>
<pinref part="R11" gate="G$1" pin="1"/>
<pinref part="Q7" gate="G$1" pin="B"/>
<wire x1="269.24" y1="63.5" x2="266.7" y2="63.5" width="0.1524" layer="91"/>
<pinref part="C34" gate="G$1" pin="1"/>
<junction x="269.24" y="63.5"/>
</segment>
</net>
<net name="N$57" class="0">
<segment>
<pinref part="L4" gate="G$1" pin="1"/>
<pinref part="D2" gate="G$1" pin="C"/>
</segment>
</net>
<net name="N$80" class="0">
<segment>
<pinref part="L3" gate="G$1" pin="2"/>
<pinref part="D2" gate="G$1" pin="A"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="190.5" y="17.78" size="2.54" layer="94">EDU:BIT</text>
<text x="269.24" y="10.16" size="2.54" layer="94">1.0.0</text>
<text x="2.54" y="185.42" size="2.54" layer="94">DIGITAL IO STATUS LEDS</text>
<text x="180.34" y="185.42" size="2.54" layer="94">ANALOG IO STATUS LEDS</text>
<wire x1="177.8" y1="0" x2="177.8" y2="190.5" width="0.4064" layer="94"/>
</plain>
<instances>
<instance part="FRAME3" gate="G$1" x="0" y="0"/>
<instance part="FRAME3" gate="G$2" x="177.8" y="0"/>
<instance part="LOGO3" gate="G$1" x="147.32" y="2.54"/>
<instance part="DS2" gate="G$1" x="33.02" y="154.94" smashed="yes" rot="R270">
<attribute name="NAME" x="36.83" y="153.67" size="1.778" layer="95" rot="R270" align="bottom-center"/>
<attribute name="LABEL" x="30.988" y="153.67" size="1.778" layer="97" rot="R270" align="top-center"/>
</instance>
<instance part="Q8" gate="G$1" x="33.02" y="134.62" smashed="yes">
<attribute name="NAME" x="38.1" y="129.54" size="1.778" layer="95" rot="R90"/>
<attribute name="MPN" x="40.64" y="129.54" size="1.778" layer="97" rot="R90"/>
</instance>
<instance part="R14" gate="G$1" x="33.02" y="144.78" rot="R270"/>
<instance part="R12" gate="G$1" x="25.4" y="129.54" rot="R270"/>
<instance part="GND48" gate="1" x="33.02" y="121.92"/>
<instance part="U$18" gate="G$1" x="33.02" y="157.48"/>
<instance part="D3" gate="G$1" x="15.24" y="129.54" rot="R90"/>
<instance part="GND56" gate="1" x="25.4" y="121.92"/>
<instance part="GND57" gate="1" x="15.24" y="121.92"/>
<instance part="DS4" gate="G$1" x="73.66" y="154.94" smashed="yes" rot="R270">
<attribute name="NAME" x="77.47" y="153.67" size="1.778" layer="95" rot="R270" align="bottom-center"/>
<attribute name="LABEL" x="71.628" y="153.67" size="1.778" layer="97" rot="R270" align="top-center"/>
</instance>
<instance part="Q10" gate="G$1" x="73.66" y="134.62" smashed="yes">
<attribute name="NAME" x="78.74" y="129.54" size="1.778" layer="95" rot="R90"/>
<attribute name="MPN" x="81.28" y="129.54" size="1.778" layer="97" rot="R90"/>
</instance>
<instance part="R18" gate="G$1" x="73.66" y="144.78" rot="R270"/>
<instance part="R16" gate="G$1" x="66.04" y="129.54" rot="R270"/>
<instance part="GND49" gate="1" x="73.66" y="121.92"/>
<instance part="U$19" gate="G$1" x="73.66" y="157.48"/>
<instance part="D5" gate="G$1" x="55.88" y="129.54" rot="R90"/>
<instance part="GND50" gate="1" x="66.04" y="121.92"/>
<instance part="GND51" gate="1" x="55.88" y="121.92"/>
<instance part="DS6" gate="G$1" x="114.3" y="154.94" smashed="yes" rot="R270">
<attribute name="NAME" x="118.11" y="153.67" size="1.778" layer="95" rot="R270" align="bottom-center"/>
<attribute name="LABEL" x="112.268" y="153.67" size="1.778" layer="97" rot="R270" align="top-center"/>
</instance>
<instance part="Q12" gate="G$1" x="114.3" y="134.62" smashed="yes">
<attribute name="NAME" x="119.38" y="129.54" size="1.778" layer="95" rot="R90"/>
<attribute name="MPN" x="121.92" y="129.54" size="1.778" layer="97" rot="R90"/>
</instance>
<instance part="R22" gate="G$1" x="114.3" y="144.78" rot="R270"/>
<instance part="R20" gate="G$1" x="106.68" y="129.54" rot="R270"/>
<instance part="GND52" gate="1" x="114.3" y="121.92"/>
<instance part="U$20" gate="G$1" x="114.3" y="157.48"/>
<instance part="D7" gate="G$1" x="96.52" y="129.54" rot="R90"/>
<instance part="GND53" gate="1" x="106.68" y="121.92"/>
<instance part="GND54" gate="1" x="96.52" y="121.92"/>
<instance part="DS8" gate="G$1" x="154.94" y="154.94" smashed="yes" rot="R270">
<attribute name="NAME" x="158.75" y="153.67" size="1.778" layer="95" rot="R270" align="bottom-center"/>
<attribute name="LABEL" x="152.908" y="153.67" size="1.778" layer="97" rot="R270" align="top-center"/>
</instance>
<instance part="Q14" gate="G$1" x="154.94" y="134.62" smashed="yes">
<attribute name="NAME" x="160.02" y="129.54" size="1.778" layer="95" rot="R90"/>
<attribute name="MPN" x="162.56" y="129.54" size="1.778" layer="97" rot="R90"/>
</instance>
<instance part="R26" gate="G$1" x="154.94" y="144.78" rot="R270"/>
<instance part="R24" gate="G$1" x="147.32" y="129.54" rot="R270"/>
<instance part="GND55" gate="1" x="154.94" y="121.92"/>
<instance part="U$21" gate="G$1" x="154.94" y="157.48"/>
<instance part="D9" gate="G$1" x="137.16" y="129.54" rot="R90"/>
<instance part="GND58" gate="1" x="147.32" y="121.92"/>
<instance part="GND59" gate="1" x="137.16" y="121.92"/>
<instance part="DS3" gate="G$1" x="33.02" y="91.44" smashed="yes" rot="R270">
<attribute name="NAME" x="36.83" y="90.17" size="1.778" layer="95" rot="R270" align="bottom-center"/>
<attribute name="LABEL" x="30.988" y="90.17" size="1.778" layer="97" rot="R270" align="top-center"/>
</instance>
<instance part="Q9" gate="G$1" x="33.02" y="71.12" smashed="yes">
<attribute name="NAME" x="38.1" y="66.04" size="1.778" layer="95" rot="R90"/>
<attribute name="MPN" x="40.64" y="66.04" size="1.778" layer="97" rot="R90"/>
</instance>
<instance part="R15" gate="G$1" x="33.02" y="81.28" rot="R270"/>
<instance part="R13" gate="G$1" x="25.4" y="66.04" rot="R270"/>
<instance part="GND60" gate="1" x="33.02" y="58.42"/>
<instance part="U$22" gate="G$1" x="33.02" y="93.98"/>
<instance part="D4" gate="G$1" x="15.24" y="66.04" rot="R90"/>
<instance part="GND61" gate="1" x="25.4" y="58.42"/>
<instance part="GND62" gate="1" x="15.24" y="58.42"/>
<instance part="DS5" gate="G$1" x="73.66" y="91.44" smashed="yes" rot="R270">
<attribute name="NAME" x="77.47" y="90.17" size="1.778" layer="95" rot="R270" align="bottom-center"/>
<attribute name="LABEL" x="71.628" y="90.17" size="1.778" layer="97" rot="R270" align="top-center"/>
</instance>
<instance part="Q11" gate="G$1" x="73.66" y="71.12" smashed="yes">
<attribute name="NAME" x="78.74" y="66.04" size="1.778" layer="95" rot="R90"/>
<attribute name="MPN" x="81.28" y="66.04" size="1.778" layer="97" rot="R90"/>
</instance>
<instance part="R19" gate="G$1" x="73.66" y="81.28" rot="R270"/>
<instance part="R17" gate="G$1" x="66.04" y="66.04" rot="R270"/>
<instance part="GND63" gate="1" x="73.66" y="58.42"/>
<instance part="U$23" gate="G$1" x="73.66" y="93.98"/>
<instance part="D6" gate="G$1" x="55.88" y="66.04" rot="R90"/>
<instance part="GND64" gate="1" x="66.04" y="58.42"/>
<instance part="GND65" gate="1" x="55.88" y="58.42"/>
<instance part="DS7" gate="G$1" x="114.3" y="91.44" smashed="yes" rot="R270">
<attribute name="NAME" x="118.11" y="90.17" size="1.778" layer="95" rot="R270" align="bottom-center"/>
<attribute name="LABEL" x="112.268" y="90.17" size="1.778" layer="97" rot="R270" align="top-center"/>
</instance>
<instance part="Q13" gate="G$1" x="114.3" y="71.12" smashed="yes">
<attribute name="NAME" x="119.38" y="66.04" size="1.778" layer="95" rot="R90"/>
<attribute name="MPN" x="121.92" y="66.04" size="1.778" layer="97" rot="R90"/>
</instance>
<instance part="R23" gate="G$1" x="114.3" y="81.28" rot="R270"/>
<instance part="R21" gate="G$1" x="106.68" y="66.04" rot="R270"/>
<instance part="GND66" gate="1" x="114.3" y="58.42"/>
<instance part="U$24" gate="G$1" x="114.3" y="93.98"/>
<instance part="D8" gate="G$1" x="96.52" y="66.04" rot="R90"/>
<instance part="GND67" gate="1" x="106.68" y="58.42"/>
<instance part="GND68" gate="1" x="96.52" y="58.42"/>
<instance part="DS9" gate="G$1" x="154.94" y="91.44" smashed="yes" rot="R270">
<attribute name="NAME" x="158.75" y="90.17" size="1.778" layer="95" rot="R270" align="bottom-center"/>
<attribute name="LABEL" x="152.908" y="90.17" size="1.778" layer="97" rot="R270" align="top-center"/>
</instance>
<instance part="Q15" gate="G$1" x="154.94" y="71.12" smashed="yes">
<attribute name="NAME" x="160.02" y="66.04" size="1.778" layer="95" rot="R90"/>
<attribute name="MPN" x="162.56" y="66.04" size="1.778" layer="97" rot="R90"/>
</instance>
<instance part="R27" gate="G$1" x="154.94" y="81.28" rot="R270"/>
<instance part="R25" gate="G$1" x="147.32" y="66.04" rot="R270"/>
<instance part="GND69" gate="1" x="154.94" y="58.42"/>
<instance part="U$25" gate="G$1" x="154.94" y="93.98"/>
<instance part="D10" gate="G$1" x="137.16" y="66.04" rot="R90"/>
<instance part="GND70" gate="1" x="147.32" y="58.42"/>
<instance part="GND71" gate="1" x="137.16" y="58.42"/>
<instance part="DS10" gate="G$1" x="213.36" y="162.56" smashed="yes">
<attribute name="NAME" x="214.63" y="166.37" size="1.778" layer="95" align="bottom-center"/>
<attribute name="LABEL" x="214.63" y="160.528" size="1.778" layer="97" align="top-center"/>
</instance>
<instance part="R28" gate="G$1" x="205.74" y="162.56"/>
<instance part="DS11" gate="G$1" x="213.36" y="154.94" smashed="yes">
<attribute name="NAME" x="214.63" y="158.75" size="1.778" layer="95" align="bottom-center"/>
<attribute name="LABEL" x="214.63" y="152.908" size="1.778" layer="97" align="top-center"/>
</instance>
<instance part="R29" gate="G$1" x="205.74" y="154.94"/>
<instance part="DS12" gate="G$1" x="213.36" y="147.32" smashed="yes">
<attribute name="NAME" x="214.63" y="151.13" size="1.778" layer="95" align="bottom-center"/>
<attribute name="LABEL" x="214.63" y="145.288" size="1.778" layer="97" align="top-center"/>
</instance>
<instance part="R30" gate="G$1" x="205.74" y="147.32"/>
<instance part="DS13" gate="G$1" x="213.36" y="139.7" smashed="yes">
<attribute name="NAME" x="214.63" y="143.51" size="1.778" layer="95" align="bottom-center"/>
<attribute name="LABEL" x="214.63" y="137.668" size="1.778" layer="97" align="top-center"/>
</instance>
<instance part="R31" gate="G$1" x="205.74" y="139.7"/>
<instance part="DS14" gate="G$1" x="213.36" y="132.08" smashed="yes">
<attribute name="NAME" x="214.63" y="135.89" size="1.778" layer="95" align="bottom-center"/>
<attribute name="LABEL" x="214.63" y="130.048" size="1.778" layer="97" align="top-center"/>
</instance>
<instance part="R32" gate="G$1" x="205.74" y="132.08"/>
<instance part="DS15" gate="G$1" x="213.36" y="124.46" smashed="yes">
<attribute name="NAME" x="214.63" y="128.27" size="1.778" layer="95" align="bottom-center"/>
<attribute name="LABEL" x="214.63" y="122.428" size="1.778" layer="97" align="top-center"/>
</instance>
<instance part="R33" gate="G$1" x="205.74" y="124.46"/>
<instance part="DS16" gate="G$1" x="213.36" y="116.84" smashed="yes">
<attribute name="NAME" x="214.63" y="120.65" size="1.778" layer="95" align="bottom-center"/>
<attribute name="LABEL" x="214.63" y="114.808" size="1.778" layer="97" align="top-center"/>
</instance>
<instance part="R34" gate="G$1" x="205.74" y="116.84"/>
<instance part="GND11" gate="1" x="220.98" y="111.76"/>
<instance part="DS24" gate="G$1" x="256.54" y="162.56" smashed="yes">
<attribute name="NAME" x="257.81" y="166.37" size="1.778" layer="95" align="bottom-center"/>
<attribute name="LABEL" x="257.81" y="160.528" size="1.778" layer="97" align="top-center"/>
</instance>
<instance part="R42" gate="G$1" x="248.92" y="162.56"/>
<instance part="DS25" gate="G$1" x="256.54" y="154.94" smashed="yes">
<attribute name="NAME" x="257.81" y="158.75" size="1.778" layer="95" align="bottom-center"/>
<attribute name="LABEL" x="257.81" y="152.908" size="1.778" layer="97" align="top-center"/>
</instance>
<instance part="R43" gate="G$1" x="248.92" y="154.94"/>
<instance part="DS26" gate="G$1" x="256.54" y="147.32" smashed="yes">
<attribute name="NAME" x="257.81" y="151.13" size="1.778" layer="95" align="bottom-center"/>
<attribute name="LABEL" x="257.81" y="145.288" size="1.778" layer="97" align="top-center"/>
</instance>
<instance part="R44" gate="G$1" x="248.92" y="147.32"/>
<instance part="DS27" gate="G$1" x="256.54" y="139.7" smashed="yes">
<attribute name="NAME" x="257.81" y="143.51" size="1.778" layer="95" align="bottom-center"/>
<attribute name="LABEL" x="257.81" y="137.668" size="1.778" layer="97" align="top-center"/>
</instance>
<instance part="R45" gate="G$1" x="248.92" y="139.7"/>
<instance part="DS28" gate="G$1" x="256.54" y="132.08" smashed="yes">
<attribute name="NAME" x="257.81" y="135.89" size="1.778" layer="95" align="bottom-center"/>
<attribute name="LABEL" x="257.81" y="130.048" size="1.778" layer="97" align="top-center"/>
</instance>
<instance part="R46" gate="G$1" x="248.92" y="132.08"/>
<instance part="DS29" gate="G$1" x="256.54" y="124.46" smashed="yes">
<attribute name="NAME" x="257.81" y="128.27" size="1.778" layer="95" align="bottom-center"/>
<attribute name="LABEL" x="257.81" y="122.428" size="1.778" layer="97" align="top-center"/>
</instance>
<instance part="R47" gate="G$1" x="248.92" y="124.46"/>
<instance part="DS30" gate="G$1" x="256.54" y="116.84" smashed="yes">
<attribute name="NAME" x="257.81" y="120.65" size="1.778" layer="95" align="bottom-center"/>
<attribute name="LABEL" x="257.81" y="114.808" size="1.778" layer="97" align="top-center"/>
</instance>
<instance part="R48" gate="G$1" x="248.92" y="116.84"/>
<instance part="GND12" gate="1" x="264.16" y="111.76"/>
<instance part="DS17" gate="G$1" x="213.36" y="93.98" smashed="yes">
<attribute name="NAME" x="214.63" y="97.79" size="1.778" layer="95" align="bottom-center"/>
<attribute name="LABEL" x="214.63" y="91.948" size="1.778" layer="97" align="top-center"/>
</instance>
<instance part="R35" gate="G$1" x="205.74" y="93.98"/>
<instance part="DS18" gate="G$1" x="213.36" y="86.36" smashed="yes">
<attribute name="NAME" x="214.63" y="90.17" size="1.778" layer="95" align="bottom-center"/>
<attribute name="LABEL" x="214.63" y="84.328" size="1.778" layer="97" align="top-center"/>
</instance>
<instance part="R36" gate="G$1" x="205.74" y="86.36"/>
<instance part="DS19" gate="G$1" x="213.36" y="78.74" smashed="yes">
<attribute name="NAME" x="214.63" y="82.55" size="1.778" layer="95" align="bottom-center"/>
<attribute name="LABEL" x="214.63" y="76.708" size="1.778" layer="97" align="top-center"/>
</instance>
<instance part="R37" gate="G$1" x="205.74" y="78.74"/>
<instance part="DS20" gate="G$1" x="213.36" y="71.12" smashed="yes">
<attribute name="NAME" x="214.63" y="74.93" size="1.778" layer="95" align="bottom-center"/>
<attribute name="LABEL" x="214.63" y="69.088" size="1.778" layer="97" align="top-center"/>
</instance>
<instance part="R38" gate="G$1" x="205.74" y="71.12"/>
<instance part="DS21" gate="G$1" x="213.36" y="63.5" smashed="yes">
<attribute name="NAME" x="214.63" y="67.31" size="1.778" layer="95" align="bottom-center"/>
<attribute name="LABEL" x="214.63" y="61.468" size="1.778" layer="97" align="top-center"/>
</instance>
<instance part="R39" gate="G$1" x="205.74" y="63.5"/>
<instance part="DS22" gate="G$1" x="213.36" y="55.88" smashed="yes">
<attribute name="NAME" x="214.63" y="59.69" size="1.778" layer="95" align="bottom-center"/>
<attribute name="LABEL" x="214.63" y="53.848" size="1.778" layer="97" align="top-center"/>
</instance>
<instance part="R40" gate="G$1" x="205.74" y="55.88"/>
<instance part="DS23" gate="G$1" x="213.36" y="48.26" smashed="yes">
<attribute name="NAME" x="214.63" y="52.07" size="1.778" layer="95" align="bottom-center"/>
<attribute name="LABEL" x="214.63" y="46.228" size="1.778" layer="97" align="top-center"/>
</instance>
<instance part="R41" gate="G$1" x="205.74" y="48.26"/>
<instance part="GND14" gate="1" x="220.98" y="43.18"/>
<instance part="TP15" gate="G$1" x="210.82" y="162.56"/>
<instance part="TP16" gate="G$1" x="210.82" y="154.94"/>
<instance part="TP17" gate="G$1" x="210.82" y="147.32"/>
<instance part="TP18" gate="G$1" x="210.82" y="139.7"/>
<instance part="TP19" gate="G$1" x="210.82" y="132.08"/>
<instance part="TP20" gate="G$1" x="210.82" y="124.46"/>
<instance part="TP21" gate="G$1" x="210.82" y="116.84"/>
<instance part="TP22" gate="G$1" x="254" y="162.56"/>
<instance part="TP23" gate="G$1" x="254" y="154.94"/>
<instance part="TP24" gate="G$1" x="254" y="147.32"/>
<instance part="TP25" gate="G$1" x="254" y="139.7"/>
<instance part="TP26" gate="G$1" x="254" y="132.08"/>
<instance part="TP27" gate="G$1" x="254" y="124.46"/>
<instance part="TP28" gate="G$1" x="254" y="116.84"/>
<instance part="TP29" gate="G$1" x="210.82" y="93.98"/>
<instance part="TP30" gate="G$1" x="210.82" y="86.36"/>
<instance part="TP31" gate="G$1" x="210.82" y="78.74"/>
<instance part="TP32" gate="G$1" x="210.82" y="71.12"/>
<instance part="TP33" gate="G$1" x="210.82" y="63.5"/>
<instance part="TP34" gate="G$1" x="210.82" y="55.88"/>
<instance part="TP35" gate="G$1" x="210.82" y="48.26"/>
<instance part="TP36" gate="G$1" x="33.02" y="149.86"/>
<instance part="TP37" gate="G$1" x="73.66" y="149.86"/>
<instance part="TP38" gate="G$1" x="114.3" y="149.86"/>
<instance part="TP39" gate="G$1" x="154.94" y="149.86"/>
<instance part="TP40" gate="G$1" x="33.02" y="86.36"/>
<instance part="TP41" gate="G$1" x="73.66" y="86.36"/>
<instance part="TP42" gate="G$1" x="114.3" y="86.36"/>
<instance part="TP43" gate="G$1" x="154.94" y="86.36"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="Q8" gate="G$1" pin="S"/>
<wire x1="33.02" y1="129.54" x2="33.02" y2="124.46" width="0.1524" layer="91"/>
<pinref part="GND48" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R12" gate="G$1" pin="2"/>
<pinref part="GND56" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="D3" gate="G$1" pin="A"/>
<pinref part="GND57" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="Q10" gate="G$1" pin="S"/>
<wire x1="73.66" y1="129.54" x2="73.66" y2="124.46" width="0.1524" layer="91"/>
<pinref part="GND49" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R16" gate="G$1" pin="2"/>
<pinref part="GND50" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="D5" gate="G$1" pin="A"/>
<pinref part="GND51" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="Q12" gate="G$1" pin="S"/>
<wire x1="114.3" y1="129.54" x2="114.3" y2="124.46" width="0.1524" layer="91"/>
<pinref part="GND52" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R20" gate="G$1" pin="2"/>
<pinref part="GND53" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="D7" gate="G$1" pin="A"/>
<pinref part="GND54" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="Q14" gate="G$1" pin="S"/>
<wire x1="154.94" y1="129.54" x2="154.94" y2="124.46" width="0.1524" layer="91"/>
<pinref part="GND55" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R24" gate="G$1" pin="2"/>
<pinref part="GND58" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="D9" gate="G$1" pin="A"/>
<pinref part="GND59" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="Q9" gate="G$1" pin="S"/>
<wire x1="33.02" y1="66.04" x2="33.02" y2="60.96" width="0.1524" layer="91"/>
<pinref part="GND60" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R13" gate="G$1" pin="2"/>
<pinref part="GND61" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="D4" gate="G$1" pin="A"/>
<pinref part="GND62" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="Q11" gate="G$1" pin="S"/>
<wire x1="73.66" y1="66.04" x2="73.66" y2="60.96" width="0.1524" layer="91"/>
<pinref part="GND63" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R17" gate="G$1" pin="2"/>
<pinref part="GND64" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="D6" gate="G$1" pin="A"/>
<pinref part="GND65" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="Q13" gate="G$1" pin="S"/>
<wire x1="114.3" y1="66.04" x2="114.3" y2="60.96" width="0.1524" layer="91"/>
<pinref part="GND66" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R21" gate="G$1" pin="2"/>
<pinref part="GND67" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="D8" gate="G$1" pin="A"/>
<pinref part="GND68" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="Q15" gate="G$1" pin="S"/>
<wire x1="154.94" y1="66.04" x2="154.94" y2="60.96" width="0.1524" layer="91"/>
<pinref part="GND69" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R25" gate="G$1" pin="2"/>
<pinref part="GND70" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="D10" gate="G$1" pin="A"/>
<pinref part="GND71" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="DS10" gate="G$1" pin="C"/>
<pinref part="GND11" gate="1" pin="GND"/>
<wire x1="218.44" y1="162.56" x2="220.98" y2="162.56" width="0.1524" layer="91"/>
<wire x1="220.98" y1="162.56" x2="220.98" y2="154.94" width="0.1524" layer="91"/>
<pinref part="DS11" gate="G$1" pin="C"/>
<wire x1="220.98" y1="154.94" x2="220.98" y2="147.32" width="0.1524" layer="91"/>
<wire x1="220.98" y1="147.32" x2="220.98" y2="139.7" width="0.1524" layer="91"/>
<wire x1="220.98" y1="139.7" x2="220.98" y2="132.08" width="0.1524" layer="91"/>
<wire x1="220.98" y1="132.08" x2="220.98" y2="124.46" width="0.1524" layer="91"/>
<wire x1="220.98" y1="124.46" x2="220.98" y2="116.84" width="0.1524" layer="91"/>
<wire x1="220.98" y1="116.84" x2="220.98" y2="114.3" width="0.1524" layer="91"/>
<wire x1="218.44" y1="154.94" x2="220.98" y2="154.94" width="0.1524" layer="91"/>
<junction x="220.98" y="154.94"/>
<pinref part="DS12" gate="G$1" pin="C"/>
<wire x1="218.44" y1="147.32" x2="220.98" y2="147.32" width="0.1524" layer="91"/>
<junction x="220.98" y="147.32"/>
<pinref part="DS13" gate="G$1" pin="C"/>
<wire x1="218.44" y1="139.7" x2="220.98" y2="139.7" width="0.1524" layer="91"/>
<junction x="220.98" y="139.7"/>
<pinref part="DS14" gate="G$1" pin="C"/>
<wire x1="218.44" y1="132.08" x2="220.98" y2="132.08" width="0.1524" layer="91"/>
<junction x="220.98" y="132.08"/>
<pinref part="DS15" gate="G$1" pin="C"/>
<wire x1="218.44" y1="124.46" x2="220.98" y2="124.46" width="0.1524" layer="91"/>
<junction x="220.98" y="124.46"/>
<pinref part="DS16" gate="G$1" pin="C"/>
<wire x1="218.44" y1="116.84" x2="220.98" y2="116.84" width="0.1524" layer="91"/>
<junction x="220.98" y="116.84"/>
</segment>
<segment>
<pinref part="DS24" gate="G$1" pin="C"/>
<pinref part="GND12" gate="1" pin="GND"/>
<wire x1="261.62" y1="162.56" x2="264.16" y2="162.56" width="0.1524" layer="91"/>
<wire x1="264.16" y1="162.56" x2="264.16" y2="154.94" width="0.1524" layer="91"/>
<pinref part="DS25" gate="G$1" pin="C"/>
<wire x1="264.16" y1="154.94" x2="264.16" y2="147.32" width="0.1524" layer="91"/>
<wire x1="264.16" y1="147.32" x2="264.16" y2="139.7" width="0.1524" layer="91"/>
<wire x1="264.16" y1="139.7" x2="264.16" y2="132.08" width="0.1524" layer="91"/>
<wire x1="264.16" y1="132.08" x2="264.16" y2="124.46" width="0.1524" layer="91"/>
<wire x1="264.16" y1="124.46" x2="264.16" y2="116.84" width="0.1524" layer="91"/>
<wire x1="264.16" y1="116.84" x2="264.16" y2="114.3" width="0.1524" layer="91"/>
<wire x1="261.62" y1="154.94" x2="264.16" y2="154.94" width="0.1524" layer="91"/>
<junction x="264.16" y="154.94"/>
<pinref part="DS26" gate="G$1" pin="C"/>
<wire x1="261.62" y1="147.32" x2="264.16" y2="147.32" width="0.1524" layer="91"/>
<junction x="264.16" y="147.32"/>
<pinref part="DS27" gate="G$1" pin="C"/>
<wire x1="261.62" y1="139.7" x2="264.16" y2="139.7" width="0.1524" layer="91"/>
<junction x="264.16" y="139.7"/>
<pinref part="DS28" gate="G$1" pin="C"/>
<wire x1="261.62" y1="132.08" x2="264.16" y2="132.08" width="0.1524" layer="91"/>
<junction x="264.16" y="132.08"/>
<pinref part="DS29" gate="G$1" pin="C"/>
<wire x1="261.62" y1="124.46" x2="264.16" y2="124.46" width="0.1524" layer="91"/>
<junction x="264.16" y="124.46"/>
<pinref part="DS30" gate="G$1" pin="C"/>
<wire x1="261.62" y1="116.84" x2="264.16" y2="116.84" width="0.1524" layer="91"/>
<junction x="264.16" y="116.84"/>
</segment>
<segment>
<pinref part="DS17" gate="G$1" pin="C"/>
<pinref part="GND14" gate="1" pin="GND"/>
<wire x1="218.44" y1="93.98" x2="220.98" y2="93.98" width="0.1524" layer="91"/>
<wire x1="220.98" y1="93.98" x2="220.98" y2="86.36" width="0.1524" layer="91"/>
<pinref part="DS18" gate="G$1" pin="C"/>
<wire x1="220.98" y1="86.36" x2="220.98" y2="78.74" width="0.1524" layer="91"/>
<wire x1="220.98" y1="78.74" x2="220.98" y2="71.12" width="0.1524" layer="91"/>
<wire x1="220.98" y1="71.12" x2="220.98" y2="63.5" width="0.1524" layer="91"/>
<wire x1="220.98" y1="63.5" x2="220.98" y2="55.88" width="0.1524" layer="91"/>
<wire x1="220.98" y1="55.88" x2="220.98" y2="48.26" width="0.1524" layer="91"/>
<wire x1="220.98" y1="48.26" x2="220.98" y2="45.72" width="0.1524" layer="91"/>
<wire x1="218.44" y1="86.36" x2="220.98" y2="86.36" width="0.1524" layer="91"/>
<junction x="220.98" y="86.36"/>
<pinref part="DS19" gate="G$1" pin="C"/>
<wire x1="218.44" y1="78.74" x2="220.98" y2="78.74" width="0.1524" layer="91"/>
<junction x="220.98" y="78.74"/>
<pinref part="DS20" gate="G$1" pin="C"/>
<wire x1="218.44" y1="71.12" x2="220.98" y2="71.12" width="0.1524" layer="91"/>
<junction x="220.98" y="71.12"/>
<pinref part="DS21" gate="G$1" pin="C"/>
<wire x1="218.44" y1="63.5" x2="220.98" y2="63.5" width="0.1524" layer="91"/>
<junction x="220.98" y="63.5"/>
<pinref part="DS22" gate="G$1" pin="C"/>
<wire x1="218.44" y1="55.88" x2="220.98" y2="55.88" width="0.1524" layer="91"/>
<junction x="220.98" y="55.88"/>
<pinref part="DS23" gate="G$1" pin="C"/>
<wire x1="218.44" y1="48.26" x2="220.98" y2="48.26" width="0.1524" layer="91"/>
<junction x="220.98" y="48.26"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="DS2" gate="G$1" pin="A"/>
<pinref part="U$18" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="DS4" gate="G$1" pin="A"/>
<pinref part="U$19" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="DS6" gate="G$1" pin="A"/>
<pinref part="U$20" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="DS8" gate="G$1" pin="A"/>
<pinref part="U$21" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="DS3" gate="G$1" pin="A"/>
<pinref part="U$22" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="DS5" gate="G$1" pin="A"/>
<pinref part="U$23" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="DS7" gate="G$1" pin="A"/>
<pinref part="U$24" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="DS9" gate="G$1" pin="A"/>
<pinref part="U$25" gate="G$1" pin="+3V3"/>
</segment>
</net>
<net name="P8" class="0">
<segment>
<pinref part="R12" gate="G$1" pin="1"/>
<pinref part="Q8" gate="G$1" pin="G"/>
<wire x1="25.4" y1="134.62" x2="15.24" y2="134.62" width="0.1524" layer="91"/>
<label x="7.62" y="134.62" size="1.778" layer="95"/>
<junction x="25.4" y="134.62"/>
<pinref part="D3" gate="G$1" pin="C"/>
<wire x1="15.24" y1="134.62" x2="7.62" y2="134.62" width="0.1524" layer="91"/>
<junction x="15.24" y="134.62"/>
</segment>
</net>
<net name="L2" class="0">
<segment>
<pinref part="Q8" gate="G$1" pin="D"/>
<pinref part="R14" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$58" class="0">
<segment>
<pinref part="R14" gate="G$1" pin="1"/>
<pinref part="DS2" gate="G$1" pin="C"/>
<pinref part="TP36" gate="G$1" pin="P$1"/>
<junction x="33.02" y="149.86"/>
<pinref part="TP36" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="L1" class="0">
<segment>
<pinref part="Q10" gate="G$1" pin="D"/>
<pinref part="R18" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="R18" gate="G$1" pin="1"/>
<pinref part="DS4" gate="G$1" pin="C"/>
<pinref part="TP37" gate="G$1" pin="P$1"/>
<junction x="73.66" y="149.86"/>
<pinref part="TP37" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="L3" class="0">
<segment>
<pinref part="Q12" gate="G$1" pin="D"/>
<pinref part="R22" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="R22" gate="G$1" pin="1"/>
<pinref part="DS6" gate="G$1" pin="C"/>
<pinref part="TP38" gate="G$1" pin="P$1"/>
<junction x="114.3" y="149.86"/>
<pinref part="TP38" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="L4" class="0">
<segment>
<pinref part="Q14" gate="G$1" pin="D"/>
<pinref part="R26" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="R26" gate="G$1" pin="1"/>
<pinref part="DS8" gate="G$1" pin="C"/>
<pinref part="TP39" gate="G$1" pin="P$1"/>
<junction x="154.94" y="149.86"/>
<pinref part="TP39" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="L5" class="0">
<segment>
<pinref part="Q9" gate="G$1" pin="D"/>
<pinref part="R15" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="R15" gate="G$1" pin="1"/>
<pinref part="DS3" gate="G$1" pin="C"/>
<pinref part="TP40" gate="G$1" pin="P$1"/>
<junction x="33.02" y="86.36"/>
<pinref part="TP40" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="L6" class="0">
<segment>
<pinref part="Q11" gate="G$1" pin="D"/>
<pinref part="R19" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="R19" gate="G$1" pin="1"/>
<pinref part="DS5" gate="G$1" pin="C"/>
<pinref part="TP41" gate="G$1" pin="P$1"/>
<junction x="73.66" y="86.36"/>
<pinref part="TP41" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="L7" class="0">
<segment>
<pinref part="Q13" gate="G$1" pin="D"/>
<pinref part="R23" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="R23" gate="G$1" pin="1"/>
<pinref part="DS7" gate="G$1" pin="C"/>
<pinref part="TP42" gate="G$1" pin="P$1"/>
<junction x="114.3" y="86.36"/>
<pinref part="TP42" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="L8" class="0">
<segment>
<pinref part="Q15" gate="G$1" pin="D"/>
<pinref part="R27" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="R27" gate="G$1" pin="1"/>
<pinref part="DS9" gate="G$1" pin="C"/>
<pinref part="TP43" gate="G$1" pin="P$1"/>
<junction x="154.94" y="86.36"/>
<pinref part="TP43" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="P12" class="0">
<segment>
<pinref part="R16" gate="G$1" pin="1"/>
<pinref part="Q10" gate="G$1" pin="G"/>
<wire x1="66.04" y1="134.62" x2="55.88" y2="134.62" width="0.1524" layer="91"/>
<label x="48.26" y="134.62" size="1.778" layer="95"/>
<junction x="66.04" y="134.62"/>
<pinref part="D5" gate="G$1" pin="C"/>
<wire x1="55.88" y1="134.62" x2="48.26" y2="134.62" width="0.1524" layer="91"/>
<junction x="55.88" y="134.62"/>
</segment>
</net>
<net name="P13" class="0">
<segment>
<pinref part="R20" gate="G$1" pin="1"/>
<pinref part="Q12" gate="G$1" pin="G"/>
<wire x1="106.68" y1="134.62" x2="96.52" y2="134.62" width="0.1524" layer="91"/>
<label x="88.9" y="134.62" size="1.778" layer="95"/>
<junction x="106.68" y="134.62"/>
<pinref part="D7" gate="G$1" pin="C"/>
<wire x1="96.52" y1="134.62" x2="88.9" y2="134.62" width="0.1524" layer="91"/>
<junction x="96.52" y="134.62"/>
</segment>
</net>
<net name="P14" class="0">
<segment>
<pinref part="R24" gate="G$1" pin="1"/>
<pinref part="Q14" gate="G$1" pin="G"/>
<wire x1="147.32" y1="134.62" x2="137.16" y2="134.62" width="0.1524" layer="91"/>
<label x="129.54" y="134.62" size="1.778" layer="95"/>
<junction x="147.32" y="134.62"/>
<pinref part="D9" gate="G$1" pin="C"/>
<wire x1="137.16" y1="134.62" x2="129.54" y2="134.62" width="0.1524" layer="91"/>
<junction x="137.16" y="134.62"/>
</segment>
</net>
<net name="P15" class="0">
<segment>
<pinref part="R13" gate="G$1" pin="1"/>
<pinref part="Q9" gate="G$1" pin="G"/>
<wire x1="25.4" y1="71.12" x2="15.24" y2="71.12" width="0.1524" layer="91"/>
<label x="7.62" y="71.12" size="1.778" layer="95"/>
<junction x="25.4" y="71.12"/>
<pinref part="D4" gate="G$1" pin="C"/>
<wire x1="15.24" y1="71.12" x2="7.62" y2="71.12" width="0.1524" layer="91"/>
<junction x="15.24" y="71.12"/>
</segment>
</net>
<net name="P16" class="0">
<segment>
<pinref part="R17" gate="G$1" pin="1"/>
<pinref part="Q11" gate="G$1" pin="G"/>
<wire x1="66.04" y1="71.12" x2="55.88" y2="71.12" width="0.1524" layer="91"/>
<label x="48.26" y="71.12" size="1.778" layer="95"/>
<junction x="66.04" y="71.12"/>
<pinref part="D6" gate="G$1" pin="C"/>
<wire x1="55.88" y1="71.12" x2="48.26" y2="71.12" width="0.1524" layer="91"/>
<junction x="55.88" y="71.12"/>
</segment>
</net>
<net name="BTNA" class="0">
<segment>
<pinref part="R21" gate="G$1" pin="1"/>
<pinref part="Q13" gate="G$1" pin="G"/>
<wire x1="106.68" y1="71.12" x2="96.52" y2="71.12" width="0.1524" layer="91"/>
<label x="88.9" y="71.12" size="1.778" layer="95"/>
<junction x="106.68" y="71.12"/>
<pinref part="D8" gate="G$1" pin="C"/>
<wire x1="96.52" y1="71.12" x2="88.9" y2="71.12" width="0.1524" layer="91"/>
<junction x="96.52" y="71.12"/>
</segment>
</net>
<net name="BTNB" class="0">
<segment>
<pinref part="R25" gate="G$1" pin="1"/>
<pinref part="Q15" gate="G$1" pin="G"/>
<wire x1="147.32" y1="71.12" x2="137.16" y2="71.12" width="0.1524" layer="91"/>
<label x="129.54" y="71.12" size="1.778" layer="95"/>
<junction x="147.32" y="71.12"/>
<pinref part="D10" gate="G$1" pin="C"/>
<wire x1="137.16" y1="71.12" x2="129.54" y2="71.12" width="0.1524" layer="91"/>
<junction x="137.16" y="71.12"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="DS10" gate="G$1" pin="A"/>
<pinref part="R28" gate="G$1" pin="2"/>
<pinref part="TP15" gate="G$1" pin="P$1"/>
<junction x="210.82" y="162.56"/>
<pinref part="TP15" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="DS11" gate="G$1" pin="A"/>
<pinref part="R29" gate="G$1" pin="2"/>
<pinref part="TP16" gate="G$1" pin="P$1"/>
<junction x="210.82" y="154.94"/>
<pinref part="TP16" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="DS12" gate="G$1" pin="A"/>
<pinref part="R30" gate="G$1" pin="2"/>
<pinref part="TP17" gate="G$1" pin="P$1"/>
<junction x="210.82" y="147.32"/>
<pinref part="TP17" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<pinref part="DS13" gate="G$1" pin="A"/>
<pinref part="R31" gate="G$1" pin="2"/>
<pinref part="TP18" gate="G$1" pin="P$1"/>
<junction x="210.82" y="139.7"/>
<pinref part="TP18" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="DS14" gate="G$1" pin="A"/>
<pinref part="R32" gate="G$1" pin="2"/>
<pinref part="TP19" gate="G$1" pin="P$1"/>
<junction x="210.82" y="132.08"/>
<pinref part="TP19" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="N$59" class="0">
<segment>
<pinref part="DS15" gate="G$1" pin="A"/>
<pinref part="R33" gate="G$1" pin="2"/>
<pinref part="TP20" gate="G$1" pin="P$1"/>
<junction x="210.82" y="124.46"/>
<pinref part="TP20" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="N$62" class="0">
<segment>
<pinref part="DS16" gate="G$1" pin="A"/>
<pinref part="R34" gate="G$1" pin="2"/>
<pinref part="TP21" gate="G$1" pin="P$1"/>
<junction x="210.82" y="116.84"/>
<pinref part="TP21" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="P0-L1" class="0">
<segment>
<pinref part="R29" gate="G$1" pin="1"/>
<wire x1="200.66" y1="154.94" x2="190.5" y2="154.94" width="0.1524" layer="91"/>
<label x="190.5" y="154.94" size="1.778" layer="95"/>
</segment>
</net>
<net name="P0-L3" class="0">
<segment>
<pinref part="R31" gate="G$1" pin="1"/>
<wire x1="200.66" y1="139.7" x2="190.5" y2="139.7" width="0.1524" layer="91"/>
<label x="190.5" y="139.7" size="1.778" layer="95"/>
</segment>
</net>
<net name="P0-L4" class="0">
<segment>
<pinref part="R32" gate="G$1" pin="1"/>
<wire x1="200.66" y1="132.08" x2="190.5" y2="132.08" width="0.1524" layer="91"/>
<label x="190.5" y="132.08" size="1.778" layer="95"/>
</segment>
</net>
<net name="P0-L5" class="0">
<segment>
<pinref part="R33" gate="G$1" pin="1"/>
<wire x1="200.66" y1="124.46" x2="190.5" y2="124.46" width="0.1524" layer="91"/>
<label x="190.5" y="124.46" size="1.778" layer="95"/>
</segment>
</net>
<net name="P0-L6" class="0">
<segment>
<pinref part="R34" gate="G$1" pin="1"/>
<wire x1="200.66" y1="116.84" x2="190.5" y2="116.84" width="0.1524" layer="91"/>
<label x="190.5" y="116.84" size="1.778" layer="95"/>
</segment>
</net>
<net name="P0-L0" class="0">
<segment>
<pinref part="R28" gate="G$1" pin="1"/>
<wire x1="200.66" y1="162.56" x2="190.5" y2="162.56" width="0.1524" layer="91"/>
<label x="190.5" y="162.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="P0-L2" class="0">
<segment>
<pinref part="R30" gate="G$1" pin="1"/>
<wire x1="200.66" y1="147.32" x2="190.5" y2="147.32" width="0.1524" layer="91"/>
<label x="190.5" y="147.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$63" class="0">
<segment>
<pinref part="DS24" gate="G$1" pin="A"/>
<pinref part="R42" gate="G$1" pin="2"/>
<pinref part="TP22" gate="G$1" pin="P$1"/>
<junction x="254" y="162.56"/>
<pinref part="TP22" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="N$64" class="0">
<segment>
<pinref part="DS25" gate="G$1" pin="A"/>
<pinref part="R43" gate="G$1" pin="2"/>
<pinref part="TP23" gate="G$1" pin="P$1"/>
<junction x="254" y="154.94"/>
<pinref part="TP23" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="N$65" class="0">
<segment>
<pinref part="DS26" gate="G$1" pin="A"/>
<pinref part="R44" gate="G$1" pin="2"/>
<pinref part="TP24" gate="G$1" pin="P$1"/>
<junction x="254" y="147.32"/>
<pinref part="TP24" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="N$66" class="0">
<segment>
<pinref part="DS27" gate="G$1" pin="A"/>
<pinref part="R45" gate="G$1" pin="2"/>
<pinref part="TP25" gate="G$1" pin="P$1"/>
<junction x="254" y="139.7"/>
<pinref part="TP25" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="N$67" class="0">
<segment>
<pinref part="DS28" gate="G$1" pin="A"/>
<pinref part="R46" gate="G$1" pin="2"/>
<pinref part="TP26" gate="G$1" pin="P$1"/>
<junction x="254" y="132.08"/>
<pinref part="TP26" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="N$68" class="0">
<segment>
<pinref part="DS29" gate="G$1" pin="A"/>
<pinref part="R47" gate="G$1" pin="2"/>
<pinref part="TP27" gate="G$1" pin="P$1"/>
<junction x="254" y="124.46"/>
<pinref part="TP27" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="N$69" class="0">
<segment>
<pinref part="DS30" gate="G$1" pin="A"/>
<pinref part="R48" gate="G$1" pin="2"/>
<pinref part="TP28" gate="G$1" pin="P$1"/>
<junction x="254" y="116.84"/>
<pinref part="TP28" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="N$70" class="0">
<segment>
<pinref part="DS17" gate="G$1" pin="A"/>
<pinref part="R35" gate="G$1" pin="2"/>
<pinref part="TP29" gate="G$1" pin="P$1"/>
<junction x="210.82" y="93.98"/>
<pinref part="TP29" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="N$71" class="0">
<segment>
<pinref part="DS18" gate="G$1" pin="A"/>
<pinref part="R36" gate="G$1" pin="2"/>
<pinref part="TP30" gate="G$1" pin="P$1"/>
<junction x="210.82" y="86.36"/>
<pinref part="TP30" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="N$72" class="0">
<segment>
<pinref part="DS19" gate="G$1" pin="A"/>
<pinref part="R37" gate="G$1" pin="2"/>
<pinref part="TP31" gate="G$1" pin="P$1"/>
<junction x="210.82" y="78.74"/>
<pinref part="TP31" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="N$73" class="0">
<segment>
<pinref part="DS20" gate="G$1" pin="A"/>
<pinref part="R38" gate="G$1" pin="2"/>
<pinref part="TP32" gate="G$1" pin="P$1"/>
<junction x="210.82" y="71.12"/>
<pinref part="TP32" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="N$74" class="0">
<segment>
<pinref part="DS21" gate="G$1" pin="A"/>
<pinref part="R39" gate="G$1" pin="2"/>
<pinref part="TP33" gate="G$1" pin="P$1"/>
<junction x="210.82" y="63.5"/>
<pinref part="TP33" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="N$75" class="0">
<segment>
<pinref part="DS22" gate="G$1" pin="A"/>
<pinref part="R40" gate="G$1" pin="2"/>
<pinref part="TP34" gate="G$1" pin="P$1"/>
<junction x="210.82" y="55.88"/>
<pinref part="TP34" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="N$76" class="0">
<segment>
<pinref part="DS23" gate="G$1" pin="A"/>
<pinref part="R41" gate="G$1" pin="2"/>
<pinref part="TP35" gate="G$1" pin="P$1"/>
<junction x="210.82" y="48.26"/>
<pinref part="TP35" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="P1-L0" class="0">
<segment>
<pinref part="R42" gate="G$1" pin="1"/>
<wire x1="243.84" y1="162.56" x2="233.68" y2="162.56" width="0.1524" layer="91"/>
<label x="233.68" y="162.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="P1-L1" class="0">
<segment>
<pinref part="R43" gate="G$1" pin="1"/>
<wire x1="243.84" y1="154.94" x2="233.68" y2="154.94" width="0.1524" layer="91"/>
<label x="233.68" y="154.94" size="1.778" layer="95"/>
</segment>
</net>
<net name="P1-L2" class="0">
<segment>
<pinref part="R44" gate="G$1" pin="1"/>
<wire x1="243.84" y1="147.32" x2="233.68" y2="147.32" width="0.1524" layer="91"/>
<label x="233.68" y="147.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="P1-L3" class="0">
<segment>
<pinref part="R45" gate="G$1" pin="1"/>
<wire x1="243.84" y1="139.7" x2="233.68" y2="139.7" width="0.1524" layer="91"/>
<label x="233.68" y="139.7" size="1.778" layer="95"/>
</segment>
</net>
<net name="P1-L4" class="0">
<segment>
<pinref part="R46" gate="G$1" pin="1"/>
<wire x1="243.84" y1="132.08" x2="233.68" y2="132.08" width="0.1524" layer="91"/>
<label x="233.68" y="132.08" size="1.778" layer="95"/>
</segment>
</net>
<net name="P1-L5" class="0">
<segment>
<pinref part="R47" gate="G$1" pin="1"/>
<wire x1="243.84" y1="124.46" x2="233.68" y2="124.46" width="0.1524" layer="91"/>
<label x="233.68" y="124.46" size="1.778" layer="95"/>
</segment>
</net>
<net name="P1-L6" class="0">
<segment>
<pinref part="R48" gate="G$1" pin="1"/>
<wire x1="243.84" y1="116.84" x2="233.68" y2="116.84" width="0.1524" layer="91"/>
<label x="233.68" y="116.84" size="1.778" layer="95"/>
</segment>
</net>
<net name="P2-L0" class="0">
<segment>
<pinref part="R35" gate="G$1" pin="1"/>
<wire x1="200.66" y1="93.98" x2="190.5" y2="93.98" width="0.1524" layer="91"/>
<label x="190.5" y="93.98" size="1.778" layer="95"/>
</segment>
</net>
<net name="P2-L1" class="0">
<segment>
<pinref part="R36" gate="G$1" pin="1"/>
<wire x1="200.66" y1="86.36" x2="190.5" y2="86.36" width="0.1524" layer="91"/>
<label x="190.5" y="86.36" size="1.778" layer="95"/>
</segment>
</net>
<net name="P2-L2" class="0">
<segment>
<pinref part="R37" gate="G$1" pin="1"/>
<wire x1="200.66" y1="78.74" x2="190.5" y2="78.74" width="0.1524" layer="91"/>
<label x="190.5" y="78.74" size="1.778" layer="95"/>
</segment>
</net>
<net name="P2-L3" class="0">
<segment>
<pinref part="R38" gate="G$1" pin="1"/>
<wire x1="200.66" y1="71.12" x2="190.5" y2="71.12" width="0.1524" layer="91"/>
<label x="190.5" y="71.12" size="1.778" layer="95"/>
</segment>
</net>
<net name="P2-L4" class="0">
<segment>
<pinref part="R39" gate="G$1" pin="1"/>
<wire x1="200.66" y1="63.5" x2="190.5" y2="63.5" width="0.1524" layer="91"/>
<label x="190.5" y="63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="P2-L5" class="0">
<segment>
<pinref part="R40" gate="G$1" pin="1"/>
<wire x1="200.66" y1="55.88" x2="190.5" y2="55.88" width="0.1524" layer="91"/>
<label x="190.5" y="55.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="P2-L6" class="0">
<segment>
<pinref part="R41" gate="G$1" pin="1"/>
<wire x1="200.66" y1="48.26" x2="190.5" y2="48.26" width="0.1524" layer="91"/>
<label x="190.5" y="48.26" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="190.5" y="17.78" size="2.54" layer="94">EDU:BIT</text>
<text x="269.24" y="10.16" size="2.54" layer="94">1.0.0</text>
<text x="175.26" y="68.58" size="1.778" layer="94" rot="R90" align="top-center">SRV1</text>
<text x="175.26" y="53.34" size="1.778" layer="94" rot="R90" align="top-center">SRV2</text>
<text x="175.26" y="38.1" size="1.778" layer="94" rot="R90" align="top-center">SRV3</text>
<text x="2.54" y="185.42" size="2.54" layer="94">SERVOS OUTPUT &amp; MOTOR DRIVERS</text>
</plain>
<instances>
<instance part="FRAME2" gate="G$1" x="0" y="0"/>
<instance part="FRAME2" gate="G$2" x="177.8" y="0"/>
<instance part="LOGO2" gate="G$1" x="147.32" y="2.54"/>
<instance part="JP10" gate="G$1" x="160.02" y="60.96" rot="R180"/>
<instance part="JP11" gate="G$1" x="160.02" y="35.56" rot="R180"/>
<instance part="R63" gate="G$1" x="132.08" y="55.88" rot="R270"/>
<instance part="DS34" gate="G$1" x="137.16" y="66.04" smashed="yes" rot="R270">
<attribute name="NAME" x="140.97" y="64.77" size="1.778" layer="95" rot="R270" align="bottom-center"/>
<attribute name="LABEL" x="135.128" y="64.77" size="1.778" layer="97" rot="R270" align="top-center"/>
</instance>
<instance part="DS31" gate="G$1" x="127" y="63.5" smashed="yes" rot="R90">
<attribute name="NAME" x="123.19" y="64.77" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="LABEL" x="129.032" y="64.77" size="1.778" layer="97" rot="R90" align="top-center"/>
</instance>
<instance part="Q16" gate="G$1" x="43.18" y="149.86" smashed="yes">
<attribute name="NAME" x="43.18" y="149.86" size="1.778" layer="95"/>
</instance>
<instance part="GND13" gate="1" x="43.18" y="137.16" rot="MR0"/>
<instance part="R49" gate="G$1" x="33.02" y="149.86"/>
<instance part="R53" gate="G$1" x="43.18" y="160.02" rot="R90"/>
<instance part="S2" gate="G$1" x="50.8" y="147.32" rot="MR270"/>
<instance part="U2" gate="G$1" x="104.14" y="48.26"/>
<instance part="C14" gate="G$1" x="81.28" y="48.26"/>
<instance part="GND16" gate="1" x="81.28" y="43.18" rot="MR0"/>
<instance part="GND17" gate="1" x="88.9" y="43.18" rot="MR0"/>
<instance part="R64" gate="G$1" x="132.08" y="38.1" rot="R270"/>
<instance part="DS35" gate="G$1" x="137.16" y="30.48" smashed="yes" rot="R270">
<attribute name="NAME" x="140.97" y="29.21" size="1.778" layer="95" rot="R270" align="bottom-center"/>
<attribute name="LABEL" x="135.128" y="29.21" size="1.778" layer="97" rot="R270" align="top-center"/>
</instance>
<instance part="DS32" gate="G$1" x="127" y="27.94" smashed="yes" rot="R90">
<attribute name="NAME" x="123.19" y="29.21" size="1.778" layer="95" rot="R90" align="bottom-center"/>
<attribute name="LABEL" x="129.032" y="29.21" size="1.778" layer="97" rot="R90" align="top-center"/>
</instance>
<instance part="Q17" gate="G$1" x="43.18" y="109.22" smashed="yes">
<attribute name="NAME" x="43.18" y="109.22" size="1.778" layer="95"/>
</instance>
<instance part="GND18" gate="1" x="43.18" y="96.52" rot="MR0"/>
<instance part="R50" gate="G$1" x="33.02" y="109.22"/>
<instance part="R54" gate="G$1" x="43.18" y="119.38" rot="R90"/>
<instance part="S3" gate="G$1" x="50.8" y="106.68" rot="MR270"/>
<instance part="Q18" gate="G$1" x="43.18" y="68.58" smashed="yes">
<attribute name="NAME" x="43.18" y="68.58" size="1.778" layer="95"/>
</instance>
<instance part="GND20" gate="1" x="43.18" y="55.88" rot="MR0"/>
<instance part="R51" gate="G$1" x="33.02" y="68.58"/>
<instance part="R55" gate="G$1" x="43.18" y="78.74" rot="R90"/>
<instance part="S4" gate="G$1" x="50.8" y="66.04" rot="MR270"/>
<instance part="Q19" gate="G$1" x="43.18" y="27.94" smashed="yes">
<attribute name="NAME" x="43.18" y="27.94" size="1.778" layer="95"/>
</instance>
<instance part="GND22" gate="1" x="43.18" y="15.24" rot="MR0"/>
<instance part="R52" gate="G$1" x="33.02" y="27.94"/>
<instance part="R56" gate="G$1" x="43.18" y="38.1" rot="R90"/>
<instance part="S5" gate="G$1" x="50.8" y="25.4" rot="MR270"/>
<instance part="U$6" gate="G$1" x="81.28" y="68.58"/>
<instance part="GND4" gate="1" x="50.8" y="137.16" rot="MR0"/>
<instance part="GND5" gate="1" x="50.8" y="96.52" rot="MR0"/>
<instance part="GND6" gate="1" x="50.8" y="55.88" rot="MR0"/>
<instance part="GND8" gate="1" x="50.8" y="15.24" rot="MR0"/>
<instance part="R66" gate="G$1" x="200.66" y="71.12"/>
<instance part="R67" gate="G$1" x="200.66" y="55.88"/>
<instance part="R68" gate="G$1" x="200.66" y="40.64"/>
<instance part="C17" gate="G$1" x="269.24" y="132.08"/>
<instance part="GND19" gate="1" x="269.24" y="127"/>
<instance part="U3" gate="G$1" x="177.8" y="147.32"/>
<instance part="GND21" gate="1" x="236.22" y="121.92"/>
<instance part="GND23" gate="1" x="104.14" y="165.1"/>
<instance part="C16" gate="G$1" x="106.68" y="152.4"/>
<instance part="GND74" gate="1" x="106.68" y="147.32"/>
<instance part="GND1" gate="1" x="193.04" y="30.48"/>
<instance part="JP15" gate="G$1" x="238.76" y="38.1"/>
<instance part="GND7" gate="1" x="248.92" y="27.94"/>
<instance part="R59" gate="G$1" x="109.22" y="119.38" rot="R90"/>
<instance part="GND114" gate="1" x="109.22" y="111.76"/>
<instance part="R60" gate="G$1" x="116.84" y="119.38" rot="R90"/>
<instance part="GND115" gate="1" x="116.84" y="111.76"/>
<instance part="R62" gate="G$1" x="124.46" y="119.38" rot="R90"/>
<instance part="GND116" gate="1" x="124.46" y="111.76"/>
<instance part="R61" gate="G$1" x="116.84" y="91.44"/>
<instance part="DS33" gate="G$1" x="129.54" y="91.44" smashed="yes">
<attribute name="NAME" x="130.81" y="95.25" size="1.778" layer="95" align="bottom-center"/>
<attribute name="LABEL" x="130.81" y="89.408" size="1.778" layer="97" align="top-center"/>
</instance>
<instance part="GND3" gate="1" x="137.16" y="86.36"/>
<instance part="R57" gate="G$1" x="86.36" y="132.08" rot="R90"/>
<instance part="R58" gate="G$1" x="86.36" y="121.92" rot="R90"/>
<instance part="GND119" gate="1" x="86.36" y="114.3"/>
<instance part="C15" gate="G$1" x="96.52" y="119.38"/>
<instance part="GND120" gate="1" x="96.52" y="114.3"/>
<instance part="JP12" gate="G$1" x="180.34" y="68.58"/>
<instance part="JP13" gate="G$1" x="180.34" y="53.34"/>
<instance part="JP14" gate="G$1" x="180.34" y="38.1"/>
<instance part="U$35" gate="G$1" x="43.18" y="165.1"/>
<instance part="U$72" gate="G$1" x="43.18" y="124.46"/>
<instance part="U$73" gate="G$1" x="43.18" y="83.82"/>
<instance part="U$74" gate="G$1" x="43.18" y="43.18"/>
<instance part="VM1" gate="G$1" x="190.5" y="73.66"/>
<instance part="VM2" gate="G$1" x="88.9" y="68.58"/>
<instance part="VM3" gate="G$1" x="264.16" y="104.14" rot="R270"/>
<instance part="Q21" gate="G$1" x="248.92" y="104.14" smashed="yes" rot="MR90">
<attribute name="NAME" x="243.84" y="111.76" size="1.778" layer="95" rot="MR180"/>
<attribute name="MPN" x="243.84" y="109.22" size="1.778" layer="97" rot="MR180"/>
</instance>
<instance part="VIN3" gate="G$1" x="233.68" y="104.14" rot="R90"/>
<instance part="R69" gate="G$1" x="241.3" y="99.06" rot="R90"/>
<instance part="VIN4" gate="G$1" x="86.36" y="137.16"/>
<instance part="GND46" gate="1" x="248.92" y="66.04" rot="MR0"/>
<instance part="R70" gate="G$1" x="241.3" y="73.66" rot="R90"/>
<instance part="Q22" gate="G$1" x="248.92" y="78.74" smashed="yes">
<attribute name="NAME" x="254" y="73.66" size="1.778" layer="95" rot="R90"/>
<attribute name="MPN" x="256.54" y="73.66" size="1.778" layer="97" rot="R90"/>
</instance>
<instance part="R71" gate="G$1" x="248.92" y="88.9" rot="R90"/>
<instance part="R65" gate="G$1" x="182.88" y="106.68"/>
<instance part="DS36" gate="G$1" x="195.58" y="106.68" smashed="yes">
<attribute name="NAME" x="196.85" y="110.49" size="1.778" layer="95" align="bottom-center"/>
<attribute name="LABEL" x="196.85" y="104.648" size="1.778" layer="97" align="top-center"/>
</instance>
<instance part="GND98" gate="1" x="203.2" y="101.6"/>
<instance part="Q20" gate="G$1" x="190.5" y="96.52" smashed="yes">
<attribute name="NAME" x="195.58" y="91.44" size="1.778" layer="95" rot="R90"/>
<attribute name="MPN" x="198.12" y="91.44" size="1.778" layer="97" rot="R90"/>
</instance>
<instance part="GND100" gate="1" x="190.5" y="88.9" rot="MR0"/>
<instance part="VIN5" gate="G$1" x="175.26" y="106.68" rot="R90"/>
<instance part="TP44" gate="G$1" x="132.08" y="60.96"/>
<instance part="TP45" gate="G$1" x="132.08" y="33.02"/>
<instance part="TP46" gate="G$1" x="124.46" y="91.44"/>
<instance part="TP47" gate="G$1" x="190.5" y="106.68"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="Q16" gate="G$1" pin="E"/>
<pinref part="GND13" gate="1" pin="GND"/>
<wire x1="43.18" y1="139.7" x2="43.18" y2="144.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C14" gate="G$1" pin="2"/>
<pinref part="GND16" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="GND"/>
<wire x1="91.44" y1="48.26" x2="88.9" y2="48.26" width="0.1524" layer="91"/>
<wire x1="88.9" y1="48.26" x2="88.9" y2="45.72" width="0.1524" layer="91"/>
<pinref part="GND17" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="Q17" gate="G$1" pin="E"/>
<pinref part="GND18" gate="1" pin="GND"/>
<wire x1="43.18" y1="99.06" x2="43.18" y2="104.14" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q18" gate="G$1" pin="E"/>
<pinref part="GND20" gate="1" pin="GND"/>
<wire x1="43.18" y1="58.42" x2="43.18" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q19" gate="G$1" pin="E"/>
<pinref part="GND22" gate="1" pin="GND"/>
<wire x1="43.18" y1="17.78" x2="43.18" y2="22.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="S2" gate="G$1" pin="2"/>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="50.8" y1="139.7" x2="50.8" y2="142.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="S3" gate="G$1" pin="2"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="50.8" y1="101.6" x2="50.8" y2="99.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND8" gate="1" pin="GND"/>
<pinref part="S5" gate="G$1" pin="2"/>
<wire x1="50.8" y1="17.78" x2="50.8" y2="20.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND6" gate="1" pin="GND"/>
<pinref part="S4" gate="G$1" pin="2"/>
<wire x1="50.8" y1="58.42" x2="50.8" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C17" gate="G$1" pin="2"/>
<pinref part="GND19" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="VSS@29"/>
<pinref part="GND21" gate="1" pin="GND"/>
<wire x1="233.68" y1="142.24" x2="236.22" y2="142.24" width="0.1524" layer="91"/>
<wire x1="236.22" y1="142.24" x2="236.22" y2="124.46" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND23" gate="1" pin="GND"/>
<wire x1="104.14" y1="167.64" x2="109.22" y2="167.64" width="0.1524" layer="91"/>
<wire x1="109.22" y1="167.64" x2="109.22" y2="162.56" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="VSS@6"/>
<wire x1="109.22" y1="162.56" x2="127" y2="162.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C16" gate="G$1" pin="2"/>
<pinref part="GND74" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="187.96" y1="66.04" x2="193.04" y2="66.04" width="0.1524" layer="91"/>
<wire x1="193.04" y1="66.04" x2="193.04" y2="50.8" width="0.1524" layer="91"/>
<wire x1="187.96" y1="50.8" x2="193.04" y2="50.8" width="0.1524" layer="91"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="193.04" y1="50.8" x2="193.04" y2="35.56" width="0.1524" layer="91"/>
<junction x="193.04" y="50.8"/>
<wire x1="193.04" y1="35.56" x2="193.04" y2="33.02" width="0.1524" layer="91"/>
<wire x1="187.96" y1="35.56" x2="193.04" y2="35.56" width="0.1524" layer="91"/>
<junction x="193.04" y="35.56"/>
<pinref part="JP12" gate="G$1" pin="3"/>
<pinref part="JP13" gate="G$1" pin="3"/>
<pinref part="JP14" gate="G$1" pin="3"/>
</segment>
<segment>
<pinref part="JP15" gate="G$1" pin="VSS"/>
<pinref part="GND7" gate="1" pin="GND"/>
<wire x1="246.38" y1="38.1" x2="248.92" y2="38.1" width="0.1524" layer="91"/>
<wire x1="248.92" y1="38.1" x2="248.92" y2="30.48" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND114" gate="1" pin="GND"/>
<pinref part="R59" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="GND115" gate="1" pin="GND"/>
<pinref part="R60" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="GND116" gate="1" pin="GND"/>
<pinref part="R62" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="R58" gate="G$1" pin="1"/>
<pinref part="GND119" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C15" gate="G$1" pin="2"/>
<pinref part="GND120" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="DS33" gate="G$1" pin="C"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="134.62" y1="91.44" x2="137.16" y2="91.44" width="0.1524" layer="91"/>
<wire x1="137.16" y1="91.44" x2="137.16" y2="88.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND46" gate="1" pin="GND"/>
<wire x1="248.92" y1="68.58" x2="248.92" y2="73.66" width="0.1524" layer="91"/>
<pinref part="R70" gate="G$1" pin="1"/>
<wire x1="241.3" y1="68.58" x2="248.92" y2="68.58" width="0.1524" layer="91"/>
<junction x="248.92" y="68.58"/>
<pinref part="Q22" gate="G$1" pin="S"/>
</segment>
<segment>
<pinref part="DS36" gate="G$1" pin="C"/>
<pinref part="GND98" gate="1" pin="GND"/>
<wire x1="200.66" y1="106.68" x2="203.2" y2="106.68" width="0.1524" layer="91"/>
<wire x1="203.2" y1="106.68" x2="203.2" y2="104.14" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q20" gate="G$1" pin="S"/>
<pinref part="GND100" gate="1" pin="GND"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="VCC1"/>
<pinref part="U2" gate="G$1" pin="VCC2"/>
<wire x1="81.28" y1="55.88" x2="88.9" y2="55.88" width="0.1524" layer="91"/>
<wire x1="88.9" y1="55.88" x2="91.44" y2="55.88" width="0.1524" layer="91"/>
<wire x1="91.44" y1="53.34" x2="88.9" y2="53.34" width="0.1524" layer="91"/>
<wire x1="88.9" y1="53.34" x2="88.9" y2="55.88" width="0.1524" layer="91"/>
<junction x="88.9" y="55.88"/>
<pinref part="C14" gate="G$1" pin="1"/>
<wire x1="81.28" y1="53.34" x2="81.28" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="+3V3"/>
<wire x1="81.28" y1="68.58" x2="81.28" y2="55.88" width="0.1524" layer="91"/>
<junction x="81.28" y="55.88"/>
</segment>
<segment>
<pinref part="R53" gate="G$1" pin="2"/>
<pinref part="U$35" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="R54" gate="G$1" pin="2"/>
<pinref part="U$72" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="R55" gate="G$1" pin="2"/>
<pinref part="U$73" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="R56" gate="G$1" pin="2"/>
<pinref part="U$74" gate="G$1" pin="+3V3"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="Q16" gate="G$1" pin="C"/>
<pinref part="R53" gate="G$1" pin="1"/>
<wire x1="43.18" y1="154.94" x2="50.8" y2="154.94" width="0.1524" layer="91"/>
<wire x1="50.8" y1="154.94" x2="71.12" y2="154.94" width="0.1524" layer="91"/>
<wire x1="71.12" y1="154.94" x2="71.12" y2="38.1" width="0.1524" layer="91"/>
<junction x="43.18" y="154.94"/>
<pinref part="U2" gate="G$1" pin="IN1B"/>
<wire x1="71.12" y1="38.1" x2="91.44" y2="38.1" width="0.1524" layer="91"/>
<pinref part="S2" gate="G$1" pin="1"/>
<wire x1="50.8" y1="152.4" x2="50.8" y2="154.94" width="0.1524" layer="91"/>
<junction x="50.8" y="154.94"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="Q16" gate="G$1" pin="B"/>
<pinref part="R49" gate="G$1" pin="2"/>
</segment>
</net>
<net name="PWM-2B" class="0">
<segment>
<pinref part="R49" gate="G$1" pin="1"/>
<wire x1="27.94" y1="149.86" x2="12.7" y2="149.86" width="0.1524" layer="91"/>
<label x="12.7" y="149.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="RC1/T1OSI/CCP2/P2A"/>
<wire x1="233.68" y1="152.4" x2="251.46" y2="152.4" width="0.1524" layer="91"/>
<label x="241.3" y="152.4" size="1.778" layer="95"/>
</segment>
</net>
<net name="M2B" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="OUT1A"/>
<wire x1="116.84" y1="63.5" x2="119.38" y2="63.5" width="0.1524" layer="91"/>
<wire x1="119.38" y1="63.5" x2="119.38" y2="68.58" width="0.1524" layer="91"/>
<pinref part="JP10" gate="G$1" pin="2"/>
<wire x1="152.4" y1="60.96" x2="149.86" y2="60.96" width="0.1524" layer="91"/>
<wire x1="149.86" y1="60.96" x2="149.86" y2="68.58" width="0.1524" layer="91"/>
<label x="144.78" y="68.58" size="1.778" layer="95"/>
<wire x1="119.38" y1="68.58" x2="127" y2="68.58" width="0.1524" layer="91"/>
<pinref part="DS31" gate="G$1" pin="C"/>
<wire x1="127" y1="68.58" x2="137.16" y2="68.58" width="0.1524" layer="91"/>
<junction x="127" y="68.58"/>
<pinref part="DS34" gate="G$1" pin="A"/>
<wire x1="137.16" y1="68.58" x2="149.86" y2="68.58" width="0.1524" layer="91"/>
<junction x="137.16" y="68.58"/>
</segment>
</net>
<net name="M2A" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="OUT1B"/>
<pinref part="JP10" gate="G$1" pin="1"/>
<wire x1="152.4" y1="58.42" x2="149.86" y2="58.42" width="0.1524" layer="91"/>
<wire x1="149.86" y1="58.42" x2="149.86" y2="50.8" width="0.1524" layer="91"/>
<label x="144.78" y="50.8" size="1.778" layer="95"/>
<wire x1="116.84" y1="50.8" x2="132.08" y2="50.8" width="0.1524" layer="91"/>
<pinref part="R63" gate="G$1" pin="2"/>
<wire x1="132.08" y1="50.8" x2="149.86" y2="50.8" width="0.1524" layer="91"/>
<junction x="132.08" y="50.8"/>
</segment>
</net>
<net name="M1A" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="OUT2B"/>
<wire x1="116.84" y1="30.48" x2="119.38" y2="30.48" width="0.1524" layer="91"/>
<wire x1="119.38" y1="30.48" x2="119.38" y2="25.4" width="0.1524" layer="91"/>
<pinref part="JP11" gate="G$1" pin="1"/>
<wire x1="152.4" y1="33.02" x2="149.86" y2="33.02" width="0.1524" layer="91"/>
<wire x1="149.86" y1="33.02" x2="149.86" y2="25.4" width="0.1524" layer="91"/>
<label x="144.78" y="25.4" size="1.778" layer="95"/>
<wire x1="119.38" y1="25.4" x2="127" y2="25.4" width="0.1524" layer="91"/>
<pinref part="DS35" gate="G$1" pin="C"/>
<wire x1="127" y1="25.4" x2="137.16" y2="25.4" width="0.1524" layer="91"/>
<wire x1="137.16" y1="25.4" x2="149.86" y2="25.4" width="0.1524" layer="91"/>
<junction x="137.16" y="25.4"/>
<pinref part="DS32" gate="G$1" pin="A"/>
<junction x="127" y="25.4"/>
</segment>
</net>
<net name="M1B" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="OUT2A"/>
<pinref part="JP11" gate="G$1" pin="2"/>
<wire x1="152.4" y1="35.56" x2="149.86" y2="35.56" width="0.1524" layer="91"/>
<wire x1="149.86" y1="35.56" x2="149.86" y2="43.18" width="0.1524" layer="91"/>
<label x="144.78" y="43.18" size="1.778" layer="95"/>
<wire x1="116.84" y1="43.18" x2="132.08" y2="43.18" width="0.1524" layer="91"/>
<pinref part="R64" gate="G$1" pin="1"/>
<wire x1="132.08" y1="43.18" x2="149.86" y2="43.18" width="0.1524" layer="91"/>
<junction x="132.08" y="43.18"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="Q17" gate="G$1" pin="C"/>
<pinref part="R54" gate="G$1" pin="1"/>
<wire x1="66.04" y1="40.64" x2="66.04" y2="114.3" width="0.1524" layer="91"/>
<wire x1="66.04" y1="114.3" x2="50.8" y2="114.3" width="0.1524" layer="91"/>
<junction x="43.18" y="114.3"/>
<pinref part="U2" gate="G$1" pin="IN1A"/>
<wire x1="50.8" y1="114.3" x2="43.18" y2="114.3" width="0.1524" layer="91"/>
<wire x1="91.44" y1="40.64" x2="66.04" y2="40.64" width="0.1524" layer="91"/>
<pinref part="S3" gate="G$1" pin="1"/>
<wire x1="50.8" y1="111.76" x2="50.8" y2="114.3" width="0.1524" layer="91"/>
<junction x="50.8" y="114.3"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="Q17" gate="G$1" pin="B"/>
<pinref part="R50" gate="G$1" pin="2"/>
</segment>
</net>
<net name="PWM-2A" class="0">
<segment>
<pinref part="R50" gate="G$1" pin="1"/>
<wire x1="27.94" y1="109.22" x2="12.7" y2="109.22" width="0.1524" layer="91"/>
<label x="12.7" y="109.22" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="RC2/CCP1/P1A/SEG3"/>
<wire x1="233.68" y1="154.94" x2="251.46" y2="154.94" width="0.1524" layer="91"/>
<label x="241.3" y="154.94" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="Q18" gate="G$1" pin="C"/>
<pinref part="R55" gate="G$1" pin="1"/>
<wire x1="43.18" y1="73.66" x2="50.8" y2="73.66" width="0.1524" layer="91"/>
<wire x1="50.8" y1="73.66" x2="60.96" y2="73.66" width="0.1524" layer="91"/>
<wire x1="60.96" y1="73.66" x2="60.96" y2="30.48" width="0.1524" layer="91"/>
<junction x="43.18" y="73.66"/>
<pinref part="U2" gate="G$1" pin="IN2B"/>
<wire x1="60.96" y1="30.48" x2="91.44" y2="30.48" width="0.1524" layer="91"/>
<pinref part="S4" gate="G$1" pin="1"/>
<wire x1="50.8" y1="71.12" x2="50.8" y2="73.66" width="0.1524" layer="91"/>
<junction x="50.8" y="73.66"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="Q18" gate="G$1" pin="B"/>
<pinref part="R51" gate="G$1" pin="2"/>
</segment>
</net>
<net name="PWM-1B" class="0">
<segment>
<pinref part="R51" gate="G$1" pin="1"/>
<wire x1="27.94" y1="68.58" x2="12.7" y2="68.58" width="0.1524" layer="91"/>
<label x="12.7" y="68.58" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="RE0/AN5/CCP3/P3A/SEG21"/>
<wire x1="233.68" y1="132.08" x2="251.46" y2="132.08" width="0.1524" layer="91"/>
<label x="241.3" y="132.08" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="Q19" gate="G$1" pin="C"/>
<pinref part="R56" gate="G$1" pin="1"/>
<junction x="43.18" y="33.02"/>
<pinref part="U2" gate="G$1" pin="IN2A"/>
<wire x1="50.8" y1="33.02" x2="43.18" y2="33.02" width="0.1524" layer="91"/>
<wire x1="50.8" y1="33.02" x2="91.44" y2="33.02" width="0.1524" layer="91"/>
<pinref part="S5" gate="G$1" pin="1"/>
<wire x1="50.8" y1="30.48" x2="50.8" y2="33.02" width="0.1524" layer="91"/>
<junction x="50.8" y="33.02"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="Q19" gate="G$1" pin="B"/>
<pinref part="R52" gate="G$1" pin="2"/>
</segment>
</net>
<net name="PWM-1A" class="0">
<segment>
<pinref part="R52" gate="G$1" pin="1"/>
<wire x1="27.94" y1="27.94" x2="12.7" y2="27.94" width="0.1524" layer="91"/>
<label x="12.7" y="27.94" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="RE2/AN7/CCP5/SEG23"/>
<wire x1="233.68" y1="137.16" x2="251.46" y2="137.16" width="0.1524" layer="91"/>
<label x="241.3" y="137.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="R66" gate="G$1" pin="1"/>
<wire x1="195.58" y1="71.12" x2="187.96" y2="71.12" width="0.1524" layer="91"/>
<pinref part="JP12" gate="G$1" pin="1"/>
</segment>
</net>
<net name="SRV1" class="0">
<segment>
<pinref part="R66" gate="G$1" pin="2"/>
<wire x1="205.74" y1="71.12" x2="213.36" y2="71.12" width="0.1524" layer="91"/>
<label x="208.28" y="71.12" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="RA7/OSC1/CLKIN/SEG2"/>
<wire x1="233.68" y1="144.78" x2="251.46" y2="144.78" width="0.1524" layer="91"/>
<label x="241.3" y="144.78" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="R67" gate="G$1" pin="1"/>
<wire x1="195.58" y1="55.88" x2="187.96" y2="55.88" width="0.1524" layer="91"/>
<pinref part="JP13" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="R68" gate="G$1" pin="1"/>
<wire x1="195.58" y1="40.64" x2="187.96" y2="40.64" width="0.1524" layer="91"/>
<pinref part="JP14" gate="G$1" pin="1"/>
</segment>
</net>
<net name="SRV2" class="0">
<segment>
<pinref part="R67" gate="G$1" pin="2"/>
<wire x1="205.74" y1="55.88" x2="213.36" y2="55.88" width="0.1524" layer="91"/>
<label x="208.28" y="55.88" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="RA6/OSC2/CLKOUT/VCAP/SEG1"/>
<wire x1="233.68" y1="147.32" x2="251.46" y2="147.32" width="0.1524" layer="91"/>
<label x="241.3" y="147.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="SRV3" class="0">
<segment>
<pinref part="R68" gate="G$1" pin="2"/>
<label x="208.28" y="40.64" size="1.778" layer="95"/>
<wire x1="213.36" y1="40.64" x2="205.74" y2="40.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="RC0/T1OSO/T1CKI/P2B"/>
<wire x1="233.68" y1="149.86" x2="251.46" y2="149.86" width="0.1524" layer="91"/>
<label x="241.3" y="149.86" size="1.778" layer="95"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="RC4/SDI/SDA/T1G/SEG11"/>
<wire x1="233.68" y1="170.18" x2="251.46" y2="170.18" width="0.1524" layer="91"/>
<label x="241.3" y="170.18" size="1.778" layer="95"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="RC3/SCL/SCK/SEG6"/>
<wire x1="233.68" y1="157.48" x2="251.46" y2="157.48" width="0.1524" layer="91"/>
<label x="241.3" y="157.48" size="1.778" layer="95"/>
</segment>
</net>
<net name="P0" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="SEG12/VCAP/SS/SRNQ/C2OUT/C12IN0-/AN0/RA0"/>
<wire x1="127" y1="134.62" x2="109.22" y2="134.62" width="0.1524" layer="91"/>
<label x="104.14" y="134.62" size="1.778" layer="95"/>
<pinref part="R59" gate="G$1" pin="2"/>
<wire x1="109.22" y1="134.62" x2="104.14" y2="134.62" width="0.1524" layer="91"/>
<wire x1="109.22" y1="124.46" x2="109.22" y2="134.62" width="0.1524" layer="91"/>
<junction x="109.22" y="134.62"/>
</segment>
</net>
<net name="P1" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="SEG7/C12IN1-/AN1/RA1"/>
<wire x1="127" y1="132.08" x2="116.84" y2="132.08" width="0.1524" layer="91"/>
<label x="104.14" y="132.08" size="1.778" layer="95"/>
<pinref part="R60" gate="G$1" pin="2"/>
<wire x1="116.84" y1="132.08" x2="104.14" y2="132.08" width="0.1524" layer="91"/>
<wire x1="116.84" y1="124.46" x2="116.84" y2="132.08" width="0.1524" layer="91"/>
<junction x="116.84" y="132.08"/>
</segment>
</net>
<net name="P2" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="COM2/DACOUT/VREF-/C2IN+/AN2/RA2"/>
<wire x1="127" y1="129.54" x2="124.46" y2="129.54" width="0.1524" layer="91"/>
<label x="104.14" y="129.54" size="1.778" layer="95"/>
<pinref part="R62" gate="G$1" pin="2"/>
<wire x1="124.46" y1="129.54" x2="104.14" y2="129.54" width="0.1524" layer="91"/>
<wire x1="124.46" y1="124.46" x2="124.46" y2="129.54" width="0.1524" layer="91"/>
<junction x="124.46" y="129.54"/>
</segment>
</net>
<net name="P0-L0" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="SEG0/INT/SRI/CPS0/AN12/RB0"/>
<wire x1="127" y1="157.48" x2="114.3" y2="157.48" width="0.1524" layer="91"/>
<label x="114.3" y="157.48" size="1.778" layer="95"/>
</segment>
</net>
<net name="P0-L1" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="VLCD1/CPS1/C12IN3-/AN10/RB1"/>
<wire x1="127" y1="154.94" x2="114.3" y2="154.94" width="0.1524" layer="91"/>
<label x="114.3" y="154.94" size="1.778" layer="95"/>
</segment>
</net>
<net name="P0-L2" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="VLCD2/CPS2/AN8/RB2"/>
<wire x1="127" y1="152.4" x2="114.3" y2="152.4" width="0.1524" layer="91"/>
<label x="114.3" y="152.4" size="1.778" layer="95"/>
</segment>
</net>
<net name="P0-L3" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="VLCD3/P2A/CCP2/CPS3/C12IN2-/AN9/RB3"/>
<wire x1="127" y1="149.86" x2="114.3" y2="149.86" width="0.1524" layer="91"/>
<label x="114.3" y="149.86" size="1.778" layer="95"/>
</segment>
</net>
<net name="P0-L4" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="COM0/CPS4/AN11/RB4"/>
<wire x1="127" y1="147.32" x2="114.3" y2="147.32" width="0.1524" layer="91"/>
<label x="114.3" y="147.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="P0-L5" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="COM1/T1G/P3A/CCP3/CPS5/AN13/RB5"/>
<wire x1="127" y1="144.78" x2="114.3" y2="144.78" width="0.1524" layer="91"/>
<label x="114.3" y="144.78" size="1.778" layer="95"/>
</segment>
</net>
<net name="P1-L0" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="RD0/CPS8/COM3"/>
<wire x1="233.68" y1="160.02" x2="251.46" y2="160.02" width="0.1524" layer="91"/>
<label x="241.3" y="160.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="P1-L1" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="RD1/CPS9/CCP4"/>
<wire x1="233.68" y1="162.56" x2="251.46" y2="162.56" width="0.1524" layer="91"/>
<label x="241.3" y="162.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="P1-L2" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="RD2/CPS10/P2B"/>
<wire x1="233.68" y1="165.1" x2="251.46" y2="165.1" width="0.1524" layer="91"/>
<label x="241.3" y="165.1" size="1.778" layer="95"/>
</segment>
</net>
<net name="P1-L3" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="RD3/CPS11/P2C/SEG16"/>
<wire x1="233.68" y1="167.64" x2="251.46" y2="167.64" width="0.1524" layer="91"/>
<label x="241.3" y="167.64" size="1.778" layer="95"/>
</segment>
</net>
<net name="P1-L4" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="SEG17/P2D/CPS12/RD4"/>
<wire x1="127" y1="172.72" x2="114.3" y2="172.72" width="0.1524" layer="91"/>
<label x="114.3" y="172.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="P1-L5" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="SEG18/P1B/CPS13/RD5"/>
<wire x1="127" y1="170.18" x2="114.3" y2="170.18" width="0.1524" layer="91"/>
<label x="114.3" y="170.18" size="1.778" layer="95"/>
</segment>
</net>
<net name="P1-L6" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="SEG19/P1C/CPS14/RD6"/>
<wire x1="127" y1="167.64" x2="114.3" y2="167.64" width="0.1524" layer="91"/>
<label x="114.3" y="167.64" size="1.778" layer="95"/>
</segment>
</net>
<net name="P2-L0" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="RC5/SDO/SEG10"/>
<wire x1="233.68" y1="172.72" x2="251.46" y2="172.72" width="0.1524" layer="91"/>
<label x="241.3" y="172.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="P2-L1" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="RC6/TX/CK/SEG9"/>
<wire x1="233.68" y1="175.26" x2="251.46" y2="175.26" width="0.1524" layer="91"/>
<label x="241.3" y="175.26" size="1.778" layer="95"/>
</segment>
</net>
<net name="P2-L2" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="SEG8/DT/RX/RC7"/>
<wire x1="127" y1="175.26" x2="114.3" y2="175.26" width="0.1524" layer="91"/>
<label x="114.3" y="175.26" size="1.778" layer="95"/>
</segment>
</net>
<net name="P2-L3" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="SEG20/P1D/CPS15/RD7"/>
<wire x1="127" y1="165.1" x2="114.3" y2="165.1" width="0.1524" layer="91"/>
<label x="114.3" y="165.1" size="1.778" layer="95"/>
</segment>
</net>
<net name="P2-L5" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="RA4/C1OUT/CPS6/T0CKI/SRQ/SEG4"/>
<wire x1="233.68" y1="127" x2="251.46" y2="127" width="0.1524" layer="91"/>
<label x="241.3" y="127" size="1.778" layer="95"/>
</segment>
</net>
<net name="P2-L6" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="RA5/AN4/C2OUT/CPS7/SRNQ/!SS!/VCAP/SEG5"/>
<wire x1="233.68" y1="129.54" x2="251.46" y2="129.54" width="0.1524" layer="91"/>
<label x="241.3" y="129.54" size="1.778" layer="95"/>
</segment>
</net>
<net name="P2-L4" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="SEG13/ICDDAT/ICSPDAT/RB7"/>
<wire x1="127" y1="139.7" x2="114.3" y2="139.7" width="0.1524" layer="91"/>
<label x="114.3" y="139.7" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="JP15" gate="G$1" pin="PGD"/>
<wire x1="246.38" y1="35.56" x2="261.62" y2="35.56" width="0.1524" layer="91"/>
<label x="254" y="35.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="P0-L6" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="SEG14/ICDCLK/ICSPCLK/RB6"/>
<wire x1="127" y1="142.24" x2="114.3" y2="142.24" width="0.1524" layer="91"/>
<label x="114.3" y="142.24" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="JP15" gate="G$1" pin="PGC"/>
<wire x1="246.38" y1="33.02" x2="261.62" y2="33.02" width="0.1524" layer="91"/>
<label x="254" y="33.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$77" class="0">
<segment>
<pinref part="R57" gate="G$1" pin="1"/>
<pinref part="R58" gate="G$1" pin="2"/>
<pinref part="U3" gate="G$1" pin="SEG15/VREF+/C1IN+/AN3/RA3"/>
<wire x1="86.36" y1="127" x2="96.52" y2="127" width="0.1524" layer="91"/>
<junction x="86.36" y="127"/>
<pinref part="C15" gate="G$1" pin="1"/>
<wire x1="96.52" y1="127" x2="127" y2="127" width="0.1524" layer="91"/>
<wire x1="96.52" y1="124.46" x2="96.52" y2="127" width="0.1524" layer="91"/>
<junction x="96.52" y="127"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="DS33" gate="G$1" pin="A"/>
<pinref part="R61" gate="G$1" pin="2"/>
<wire x1="121.92" y1="91.44" x2="124.46" y2="91.44" width="0.1524" layer="91"/>
<pinref part="TP46" gate="G$1" pin="P$1"/>
<wire x1="124.46" y1="91.44" x2="127" y2="91.44" width="0.1524" layer="91"/>
<junction x="124.46" y="91.44"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="DS31" gate="G$1" pin="A"/>
<pinref part="R63" gate="G$1" pin="1"/>
<wire x1="127" y1="60.96" x2="132.08" y2="60.96" width="0.1524" layer="91"/>
<pinref part="DS34" gate="G$1" pin="C"/>
<wire x1="132.08" y1="60.96" x2="137.16" y2="60.96" width="0.1524" layer="91"/>
<junction x="132.08" y="60.96"/>
<pinref part="TP44" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="DS32" gate="G$1" pin="C"/>
<pinref part="R64" gate="G$1" pin="2"/>
<wire x1="127" y1="33.02" x2="132.08" y2="33.02" width="0.1524" layer="91"/>
<pinref part="DS35" gate="G$1" pin="A"/>
<wire x1="132.08" y1="33.02" x2="137.16" y2="33.02" width="0.1524" layer="91"/>
<junction x="132.08" y="33.02"/>
<pinref part="TP45" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="VM" class="0">
<segment>
<wire x1="190.5" y1="73.66" x2="190.5" y2="68.58" width="0.1524" layer="91"/>
<wire x1="190.5" y1="68.58" x2="190.5" y2="53.34" width="0.1524" layer="91"/>
<wire x1="187.96" y1="68.58" x2="190.5" y2="68.58" width="0.1524" layer="91"/>
<junction x="190.5" y="68.58"/>
<wire x1="187.96" y1="53.34" x2="190.5" y2="53.34" width="0.1524" layer="91"/>
<wire x1="187.96" y1="38.1" x2="190.5" y2="38.1" width="0.1524" layer="91"/>
<wire x1="190.5" y1="38.1" x2="190.5" y2="53.34" width="0.1524" layer="91"/>
<junction x="190.5" y="53.34"/>
<pinref part="JP12" gate="G$1" pin="2"/>
<pinref part="JP13" gate="G$1" pin="2"/>
<pinref part="JP14" gate="G$1" pin="2"/>
<pinref part="VM1" gate="G$1" pin="VM"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="VDD1"/>
<wire x1="88.9" y1="68.58" x2="88.9" y2="63.5" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="VDD2"/>
<wire x1="88.9" y1="63.5" x2="91.44" y2="63.5" width="0.1524" layer="91"/>
<wire x1="91.44" y1="60.96" x2="88.9" y2="60.96" width="0.1524" layer="91"/>
<wire x1="88.9" y1="60.96" x2="88.9" y2="63.5" width="0.1524" layer="91"/>
<junction x="88.9" y="63.5"/>
<pinref part="VM2" gate="G$1" pin="VM"/>
</segment>
<segment>
<pinref part="VM3" gate="G$1" pin="VM"/>
<pinref part="Q21" gate="G$1" pin="D"/>
<wire x1="264.16" y1="104.14" x2="254" y2="104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VIN" class="0">
<segment>
<pinref part="Q21" gate="G$1" pin="S"/>
<pinref part="VIN3" gate="G$1" pin="VIN"/>
<wire x1="243.84" y1="104.14" x2="241.3" y2="104.14" width="0.1524" layer="91"/>
<pinref part="R69" gate="G$1" pin="2"/>
<wire x1="241.3" y1="104.14" x2="233.68" y2="104.14" width="0.1524" layer="91"/>
<junction x="241.3" y="104.14"/>
</segment>
<segment>
<pinref part="R57" gate="G$1" pin="2"/>
<pinref part="VIN4" gate="G$1" pin="VIN"/>
</segment>
<segment>
<pinref part="R65" gate="G$1" pin="1"/>
<wire x1="177.8" y1="106.68" x2="175.26" y2="106.68" width="0.1524" layer="91"/>
<pinref part="VIN5" gate="G$1" pin="VIN"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<pinref part="R69" gate="G$1" pin="1"/>
<pinref part="Q21" gate="G$1" pin="G"/>
<junction x="248.92" y="93.98"/>
<pinref part="R71" gate="G$1" pin="2"/>
<wire x1="248.92" y1="93.98" x2="248.92" y2="96.52" width="0.1524" layer="91"/>
<wire x1="248.92" y1="93.98" x2="241.3" y2="93.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$83" class="0">
<segment>
<pinref part="Q22" gate="G$1" pin="D"/>
<pinref part="R71" gate="G$1" pin="1"/>
</segment>
</net>
<net name="PWR-CTRL" class="0">
<segment>
<pinref part="R70" gate="G$1" pin="2"/>
<wire x1="241.3" y1="78.74" x2="228.6" y2="78.74" width="0.1524" layer="91"/>
<junction x="241.3" y="78.74"/>
<label x="228.6" y="78.74" size="1.778" layer="95"/>
<pinref part="Q22" gate="G$1" pin="G"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="VPP/!MCLR!/RE3"/>
<wire x1="127" y1="137.16" x2="111.76" y2="137.16" width="0.1524" layer="91"/>
<label x="111.76" y="137.16" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="JP15" gate="G$1" pin="VPP"/>
<wire x1="246.38" y1="43.18" x2="261.62" y2="43.18" width="0.1524" layer="91"/>
<label x="254" y="43.18" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Q20" gate="G$1" pin="G"/>
<wire x1="182.88" y1="96.52" x2="167.64" y2="96.52" width="0.1524" layer="91"/>
<label x="167.64" y="96.52" size="1.778" layer="95"/>
</segment>
</net>
<net name="LOWBATT-LED" class="0">
<segment>
<pinref part="R61" gate="G$1" pin="1"/>
<wire x1="111.76" y1="91.44" x2="93.98" y2="91.44" width="0.1524" layer="91"/>
<label x="93.98" y="91.44" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="RE1/AN6/P3B/SEG22"/>
<wire x1="233.68" y1="134.62" x2="256.54" y2="134.62" width="0.1524" layer="91"/>
<label x="241.3" y="134.62" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$49" class="0">
<segment>
<pinref part="DS36" gate="G$1" pin="A"/>
<pinref part="R65" gate="G$1" pin="2"/>
<wire x1="187.96" y1="106.68" x2="190.5" y2="106.68" width="0.1524" layer="91"/>
<pinref part="Q20" gate="G$1" pin="D"/>
<wire x1="190.5" y1="106.68" x2="193.04" y2="106.68" width="0.1524" layer="91"/>
<wire x1="190.5" y1="101.6" x2="190.5" y2="106.68" width="0.1524" layer="91"/>
<junction x="190.5" y="106.68"/>
<pinref part="TP47" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="UB3V3" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="VDD@7"/>
<pinref part="C16" gate="G$1" pin="1"/>
<wire x1="106.68" y1="160.02" x2="127" y2="160.02" width="0.1524" layer="91"/>
<wire x1="106.68" y1="157.48" x2="106.68" y2="160.02" width="0.1524" layer="91"/>
<label x="114.3" y="160.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="VDD@28"/>
<pinref part="C17" gate="G$1" pin="1"/>
<wire x1="269.24" y1="139.7" x2="233.68" y2="139.7" width="0.1524" layer="91"/>
<wire x1="269.24" y1="137.16" x2="269.24" y2="139.7" width="0.1524" layer="91"/>
<label x="241.3" y="139.7" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="JP15" gate="G$1" pin="VDD"/>
<wire x1="246.38" y1="40.64" x2="261.62" y2="40.64" width="0.1524" layer="91"/>
<label x="254" y="40.64" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="190.5" y="17.78" size="2.54" layer="94">EDU:BIT</text>
<text x="269.24" y="10.16" size="2.54" layer="94">1.0.0</text>
<text x="2.54" y="185.42" size="2.54" layer="94">PUSH BUTTONS</text>
<wire x1="0" y1="104.14" x2="73.66" y2="104.14" width="0.4064" layer="94"/>
<wire x1="73.66" y1="104.14" x2="73.66" y2="149.86" width="0.4064" layer="94"/>
<text x="76.2" y="185.42" size="2.54" layer="94">NEO PIXELS</text>
<text x="2.54" y="99.06" size="2.54" layer="94">POTENTIOMETER</text>
<wire x1="73.66" y1="149.86" x2="73.66" y2="190.5" width="0.4064" layer="94"/>
<wire x1="0" y1="58.42" x2="73.66" y2="58.42" width="0.4064" layer="94"/>
<wire x1="73.66" y1="58.42" x2="73.66" y2="91.44" width="0.4064" layer="94"/>
<text x="2.54" y="53.34" size="2.54" layer="94">LEDS</text>
<wire x1="73.66" y1="91.44" x2="73.66" y2="104.14" width="0.4064" layer="94"/>
<wire x1="73.66" y1="58.42" x2="73.66" y2="0" width="0.4064" layer="94"/>
<text x="76.2" y="144.78" size="2.54" layer="94">SOUND SENSOR</text>
<wire x1="73.66" y1="149.86" x2="279.4" y2="149.86" width="0.4064" layer="94"/>
<wire x1="73.66" y1="91.44" x2="190.5" y2="91.44" width="0.4064" layer="94"/>
<text x="76.2" y="86.36" size="2.54" layer="94">IR SENSOR</text>
<text x="193.04" y="86.36" size="2.54" layer="94">PIEZO BUZZER</text>
<wire x1="190.5" y1="91.44" x2="279.4" y2="91.44" width="0.4064" layer="94"/>
<wire x1="190.5" y1="91.44" x2="190.5" y2="22.86" width="0.4064" layer="94"/>
</plain>
<instances>
<instance part="FRAME4" gate="G$1" x="0" y="0"/>
<instance part="FRAME4" gate="G$2" x="177.8" y="0"/>
<instance part="LOGO4" gate="G$1" x="147.32" y="2.54"/>
<instance part="U$27" gate="G$1" x="58.42" y="175.26"/>
<instance part="GND75" gate="1" x="58.42" y="152.4"/>
<instance part="C19" gate="G$1" x="38.1" y="157.48"/>
<instance part="S6" gate="G$1" x="58.42" y="160.02" rot="R90"/>
<instance part="R78" gate="G$1" x="58.42" y="170.18" rot="R90"/>
<instance part="R76" gate="G$1" x="48.26" y="165.1"/>
<instance part="GND76" gate="1" x="38.1" y="152.4"/>
<instance part="JP16" gate="G$1" x="10.16" y="147.32"/>
<instance part="GND77" gate="1" x="20.32" y="139.7"/>
<instance part="U$28" gate="G$1" x="22.86" y="154.94"/>
<instance part="U$29" gate="G$1" x="58.42" y="137.16"/>
<instance part="GND78" gate="1" x="58.42" y="114.3"/>
<instance part="C20" gate="G$1" x="38.1" y="119.38"/>
<instance part="S7" gate="G$1" x="58.42" y="121.92" rot="R90"/>
<instance part="R79" gate="G$1" x="58.42" y="132.08" rot="R90"/>
<instance part="R77" gate="G$1" x="48.26" y="127"/>
<instance part="GND79" gate="1" x="38.1" y="114.3"/>
<instance part="U5" gate="G$1" x="147.32" y="170.18" rot="R180"/>
<instance part="U$30" gate="G$1" x="259.08" y="177.8"/>
<instance part="GND80" gate="1" x="134.62" y="154.94"/>
<instance part="C25" gate="G$1" x="160.02" y="162.56"/>
<instance part="JP20" gate="G$1" x="101.6" y="170.18"/>
<instance part="U$31" gate="G$1" x="160.02" y="177.8"/>
<instance part="U$32" gate="G$1" x="114.3" y="177.8"/>
<instance part="GND83" gate="1" x="111.76" y="154.94"/>
<instance part="GND82" gate="1" x="160.02" y="154.94"/>
<instance part="JP17" gate="G$1" x="10.16" y="81.28"/>
<instance part="GND84" gate="1" x="20.32" y="66.04"/>
<instance part="U$33" gate="G$1" x="22.86" y="86.36"/>
<instance part="C21" gate="G$1" x="55.88" y="86.36" rot="R270"/>
<instance part="VR1" gate="G$1" x="50.8" y="78.74" rot="R90"/>
<instance part="GND85" gate="1" x="50.8" y="66.04"/>
<instance part="U$34" gate="G$1" x="50.8" y="86.36"/>
<instance part="C18" gate="G$1" x="30.48" y="71.12"/>
<instance part="GND86" gate="1" x="30.48" y="66.04"/>
<instance part="GND87" gate="1" x="63.5" y="81.28"/>
<instance part="JP18" gate="G$1" x="10.16" y="22.86"/>
<instance part="GND88" gate="1" x="20.32" y="15.24"/>
<instance part="DS38" gate="G$1" x="50.8" y="33.02" smashed="yes">
<attribute name="NAME" x="52.07" y="36.83" size="1.778" layer="95" align="bottom-center"/>
<attribute name="LABEL" x="52.07" y="30.988" size="1.778" layer="97" align="top-center"/>
<attribute name="VALUE" x="52.07" y="31.242" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="DS39" gate="G$1" x="50.8" y="20.32" smashed="yes">
<attribute name="NAME" x="52.07" y="24.13" size="1.778" layer="95" align="bottom-center"/>
<attribute name="LABEL" x="52.07" y="18.288" size="1.778" layer="97" align="top-center"/>
<attribute name="VALUE" x="52.07" y="18.542" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="R72" gate="G$1" x="38.1" y="78.74"/>
<instance part="R74" gate="G$1" x="40.64" y="33.02"/>
<instance part="R75" gate="G$1" x="40.64" y="20.32"/>
<instance part="GND89" gate="1" x="60.96" y="15.24"/>
<instance part="U7" gate="G$1" x="180.34" y="170.18" rot="R180"/>
<instance part="GND90" gate="1" x="167.64" y="154.94"/>
<instance part="C28" gate="G$1" x="193.04" y="162.56"/>
<instance part="U$36" gate="G$1" x="193.04" y="177.8"/>
<instance part="GND91" gate="1" x="193.04" y="154.94"/>
<instance part="U9" gate="G$1" x="213.36" y="170.18" rot="R180"/>
<instance part="GND92" gate="1" x="200.66" y="154.94"/>
<instance part="C30" gate="G$1" x="226.06" y="162.56"/>
<instance part="U$37" gate="G$1" x="226.06" y="177.8"/>
<instance part="GND93" gate="1" x="226.06" y="154.94"/>
<instance part="JP21" gate="G$1" x="104.14" y="119.38"/>
<instance part="GND94" gate="1" x="114.3" y="109.22"/>
<instance part="U$38" gate="G$1" x="116.84" y="127"/>
<instance part="MK1" gate="G$1" x="236.22" y="116.84"/>
<instance part="GND95" gate="1" x="223.52" y="109.22"/>
<instance part="R92" gate="G$1" x="223.52" y="127" rot="R90"/>
<instance part="U$39" gate="G$1" x="223.52" y="132.08"/>
<instance part="C29" gate="G$1" x="213.36" y="119.38" rot="R270"/>
<instance part="R90" gate="G$1" x="205.74" y="119.38"/>
<instance part="R88" gate="G$1" x="182.88" y="129.54"/>
<instance part="C27" gate="G$1" x="180.34" y="139.7" rot="R270"/>
<instance part="C26" gate="G$1" x="165.1" y="106.68"/>
<instance part="U$40" gate="G$1" x="162.56" y="114.3" rot="R90"/>
<instance part="GND97" gate="1" x="165.1" y="101.6"/>
<instance part="U6" gate="G$1" x="165.1" y="45.72" smashed="yes">
<attribute name="NAME" x="156.718" y="40.386" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="R86" gate="G$1" x="167.64" y="58.42" rot="R90"/>
<instance part="R87" gate="G$1" x="175.26" y="58.42" rot="R90"/>
<instance part="R89" gate="G$1" x="182.88" y="58.42" rot="R90"/>
<instance part="U$42" gate="G$1" x="167.64" y="63.5"/>
<instance part="GND101" gate="1" x="167.64" y="30.48"/>
<instance part="R85" gate="G$1" x="160.02" y="58.42" rot="R90"/>
<instance part="GND102" gate="1" x="160.02" y="30.48"/>
<instance part="U$43" gate="G$1" x="160.02" y="63.5"/>
<instance part="U4" gate="G$1" x="134.62" y="40.64"/>
<instance part="GND103" gate="1" x="149.86" y="30.48"/>
<instance part="VR2" gate="G$1" x="129.54" y="60.96" rot="MR90"/>
<instance part="GND104" gate="1" x="129.54" y="53.34"/>
<instance part="U$44" gate="G$1" x="129.54" y="76.2"/>
<instance part="C23" gate="G$1" x="139.7" y="58.42" smashed="yes">
<attribute name="NAME" x="137.16" y="57.15" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="144.018" y="54.61" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND105" gate="1" x="139.7" y="53.34"/>
<instance part="C22" gate="G$1" x="119.38" y="30.48"/>
<instance part="GND106" gate="1" x="119.38" y="22.86"/>
<instance part="U$45" gate="G$1" x="119.38" y="45.72"/>
<instance part="JP19" gate="G$1" x="81.28" y="35.56"/>
<instance part="GND107" gate="1" x="91.44" y="22.86"/>
<instance part="U$46" gate="G$1" x="93.98" y="43.18"/>
<instance part="R81" gate="G$1" x="109.22" y="38.1" rot="R90"/>
<instance part="DS40" gate="G$1" x="109.22" y="30.48" smashed="yes" rot="R270">
<attribute name="NAME" x="105.41" y="29.21" size="1.778" layer="95" rot="R270" align="bottom-center"/>
<attribute name="LABEL" x="107.188" y="29.21" size="1.778" layer="97" rot="R270" align="top-center"/>
</instance>
<instance part="GND108" gate="1" x="109.22" y="22.86"/>
<instance part="R83" gate="G$1" x="129.54" y="71.12" rot="R90"/>
<instance part="LS1" gate="G$1" x="271.78" y="73.66"/>
<instance part="Q23" gate="G$1" x="264.16" y="66.04" smashed="yes">
<attribute name="NAME" x="268.224" y="65.278" size="1.778" layer="95"/>
</instance>
<instance part="R93" gate="G$1" x="256.54" y="60.96" rot="R270"/>
<instance part="C31" gate="G$1" x="256.54" y="76.2" rot="R270"/>
<instance part="GND44" gate="1" x="251.46" y="73.66"/>
<instance part="GND45" gate="1" x="264.16" y="53.34"/>
<instance part="U$17" gate="G$1" x="264.16" y="78.74"/>
<instance part="S8" gate="G$1" x="233.68" y="63.5" smashed="yes">
<attribute name="NAME" x="233.68" y="71.12" size="1.778" layer="95"/>
<attribute name="LABEL" x="233.68" y="68.58" size="1.778" layer="97"/>
</instance>
<instance part="JP23" gate="G$1" x="254" y="40.64" smashed="yes" rot="R180">
<attribute name="NAME" x="259.08" y="37.592" size="1.778" layer="95" rot="R180"/>
<attribute name="LABEL" x="248.92" y="43.942" size="1.778" layer="96"/>
</instance>
<instance part="GND47" gate="1" x="246.38" y="33.02"/>
<instance part="D12" gate="G$1" x="246.38" y="60.96" rot="R90"/>
<instance part="GND72" gate="1" x="246.38" y="53.34"/>
<instance part="GND73" gate="1" x="256.54" y="53.34"/>
<instance part="JP22" gate="G$1" x="195.58" y="48.26"/>
<instance part="U$65" gate="G$1" x="208.28" y="55.88"/>
<instance part="GND117" gate="1" x="205.74" y="38.1"/>
<instance part="U10" gate="G$1" x="246.38" y="170.18" rot="R180"/>
<instance part="GND81" gate="1" x="233.68" y="154.94"/>
<instance part="C32" gate="G$1" x="259.08" y="162.56"/>
<instance part="GND118" gate="1" x="259.08" y="154.94"/>
<instance part="R91" gate="G$1" x="218.44" y="45.72"/>
<instance part="C24" gate="G$1" x="147.32" y="129.54"/>
<instance part="R84" gate="G$1" x="137.16" y="132.08" rot="R90"/>
<instance part="GND121" gate="1" x="137.16" y="124.46"/>
<instance part="GND122" gate="1" x="147.32" y="124.46"/>
<instance part="U8" gate="G$1" x="180.34" y="116.84"/>
<instance part="GND96" gate="1" x="198.12" y="109.22"/>
<instance part="D11" gate="G$1" x="157.48" y="139.7" smashed="yes" rot="R90">
<attribute name="NAME" x="154.94" y="144.78" size="1.778" layer="95"/>
<attribute name="MPN" x="154.94" y="144.018" size="1.778" layer="97" align="top-left"/>
</instance>
<instance part="DS37" gate="G$1" x="50.8" y="45.72" smashed="yes">
<attribute name="NAME" x="52.07" y="49.53" size="1.778" layer="95" align="bottom-center"/>
<attribute name="LABEL" x="52.07" y="43.688" size="1.778" layer="97" align="top-center"/>
<attribute name="VALUE" x="52.07" y="43.942" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="R73" gate="G$1" x="40.64" y="45.72"/>
<instance part="R82" gate="G$1" x="127" y="162.56" rot="R90"/>
<instance part="GND124" gate="1" x="127" y="154.94"/>
<instance part="R80" gate="G$1" x="101.6" y="38.1" rot="R90"/>
<instance part="TP49" gate="G$1" x="45.72" y="45.72"/>
<instance part="TP50" gate="G$1" x="45.72" y="33.02"/>
<instance part="TP51" gate="G$1" x="45.72" y="20.32"/>
<instance part="TP52" gate="G$1" x="109.22" y="33.02"/>
</instances>
<busses>
</busses>
<nets>
<net name="+3V3" class="0">
<segment>
<pinref part="U$27" gate="G$1" pin="+3V3"/>
<pinref part="R78" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="U$29" gate="G$1" pin="+3V3"/>
<pinref part="R79" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="JP16" gate="G$1" pin="2"/>
<pinref part="U$28" gate="G$1" pin="+3V3"/>
<wire x1="17.78" y1="149.86" x2="22.86" y2="149.86" width="0.1524" layer="91"/>
<wire x1="22.86" y1="149.86" x2="22.86" y2="154.94" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP20" gate="G$1" pin="2"/>
<pinref part="U$32" gate="G$1" pin="+3V3"/>
<wire x1="109.22" y1="172.72" x2="114.3" y2="172.72" width="0.1524" layer="91"/>
<wire x1="114.3" y1="172.72" x2="114.3" y2="177.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$31" gate="G$1" pin="+3V3"/>
<wire x1="160.02" y1="177.8" x2="160.02" y2="167.64" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="VDD"/>
<wire x1="160.02" y1="167.64" x2="157.48" y2="167.64" width="0.1524" layer="91"/>
<pinref part="C25" gate="G$1" pin="1"/>
<junction x="160.02" y="167.64"/>
</segment>
<segment>
<pinref part="JP17" gate="G$1" pin="2"/>
<pinref part="U$33" gate="G$1" pin="+3V3"/>
<wire x1="17.78" y1="83.82" x2="22.86" y2="83.82" width="0.1524" layer="91"/>
<wire x1="22.86" y1="83.82" x2="22.86" y2="86.36" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$34" gate="G$1" pin="+3V3"/>
<pinref part="VR1" gate="G$1" pin="3"/>
<wire x1="50.8" y1="86.36" x2="50.8" y2="83.82" width="0.1524" layer="91"/>
<pinref part="C21" gate="G$1" pin="2"/>
<wire x1="53.34" y1="86.36" x2="50.8" y2="86.36" width="0.1524" layer="91"/>
<junction x="50.8" y="86.36"/>
</segment>
<segment>
<pinref part="U$36" gate="G$1" pin="+3V3"/>
<wire x1="193.04" y1="177.8" x2="193.04" y2="167.64" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="VDD"/>
<wire x1="193.04" y1="167.64" x2="190.5" y2="167.64" width="0.1524" layer="91"/>
<pinref part="C28" gate="G$1" pin="1"/>
<junction x="193.04" y="167.64"/>
</segment>
<segment>
<pinref part="U$37" gate="G$1" pin="+3V3"/>
<wire x1="226.06" y1="177.8" x2="226.06" y2="167.64" width="0.1524" layer="91"/>
<pinref part="U9" gate="G$1" pin="VDD"/>
<wire x1="226.06" y1="167.64" x2="223.52" y2="167.64" width="0.1524" layer="91"/>
<pinref part="C30" gate="G$1" pin="1"/>
<junction x="226.06" y="167.64"/>
</segment>
<segment>
<pinref part="U$38" gate="G$1" pin="+3V3"/>
<wire x1="116.84" y1="127" x2="116.84" y2="121.92" width="0.1524" layer="91"/>
<pinref part="JP21" gate="G$1" pin="2"/>
<wire x1="116.84" y1="121.92" x2="111.76" y2="121.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R92" gate="G$1" pin="2"/>
<pinref part="U$39" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="R89" gate="G$1" pin="2"/>
<pinref part="R87" gate="G$1" pin="2"/>
<wire x1="182.88" y1="63.5" x2="175.26" y2="63.5" width="0.1524" layer="91"/>
<pinref part="R86" gate="G$1" pin="2"/>
<wire x1="175.26" y1="63.5" x2="167.64" y2="63.5" width="0.1524" layer="91"/>
<junction x="175.26" y="63.5"/>
<pinref part="U$42" gate="G$1" pin="+3V3"/>
<junction x="167.64" y="63.5"/>
</segment>
<segment>
<pinref part="U$45" gate="G$1" pin="+3V3"/>
<pinref part="U4" gate="G$1" pin="V+"/>
<wire x1="119.38" y1="45.72" x2="119.38" y2="38.1" width="0.1524" layer="91"/>
<pinref part="C22" gate="G$1" pin="1"/>
<wire x1="119.38" y1="38.1" x2="124.46" y2="38.1" width="0.1524" layer="91"/>
<wire x1="119.38" y1="35.56" x2="119.38" y2="38.1" width="0.1524" layer="91"/>
<junction x="119.38" y="38.1"/>
</segment>
<segment>
<pinref part="U$46" gate="G$1" pin="+3V3"/>
<wire x1="93.98" y1="43.18" x2="93.98" y2="38.1" width="0.1524" layer="91"/>
<pinref part="JP19" gate="G$1" pin="2"/>
<wire x1="93.98" y1="38.1" x2="88.9" y2="38.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$43" gate="G$1" pin="+3V3"/>
<pinref part="R85" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="R83" gate="G$1" pin="2"/>
<pinref part="U$44" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="LS1" gate="G$1" pin="+"/>
<pinref part="C31" gate="G$1" pin="1"/>
<wire x1="261.62" y1="76.2" x2="264.16" y2="76.2" width="0.1524" layer="91"/>
<pinref part="U$17" gate="G$1" pin="+3V3"/>
<wire x1="264.16" y1="76.2" x2="266.7" y2="76.2" width="0.1524" layer="91"/>
<wire x1="264.16" y1="78.74" x2="264.16" y2="76.2" width="0.1524" layer="91"/>
<junction x="264.16" y="76.2"/>
</segment>
<segment>
<pinref part="U$65" gate="G$1" pin="+3V3"/>
<wire x1="208.28" y1="55.88" x2="208.28" y2="50.8" width="0.1524" layer="91"/>
<pinref part="JP22" gate="G$1" pin="2"/>
<wire x1="208.28" y1="50.8" x2="203.2" y2="50.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$30" gate="G$1" pin="+3V3"/>
<pinref part="C32" gate="G$1" pin="1"/>
<wire x1="259.08" y1="177.8" x2="259.08" y2="167.64" width="0.1524" layer="91"/>
<pinref part="U10" gate="G$1" pin="VDD"/>
<wire x1="256.54" y1="167.64" x2="259.08" y2="167.64" width="0.1524" layer="91"/>
<junction x="259.08" y="167.64"/>
</segment>
<segment>
<pinref part="U$40" gate="G$1" pin="+3V3"/>
<pinref part="U8" gate="G$1" pin="V+"/>
<wire x1="162.56" y1="114.3" x2="165.1" y2="114.3" width="0.1524" layer="91"/>
<pinref part="C26" gate="G$1" pin="1"/>
<wire x1="165.1" y1="114.3" x2="170.18" y2="114.3" width="0.1524" layer="91"/>
<wire x1="165.1" y1="111.76" x2="165.1" y2="114.3" width="0.1524" layer="91"/>
<junction x="165.1" y="114.3"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="GND75" gate="1" pin="GND"/>
<pinref part="S6" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="C19" gate="G$1" pin="2"/>
<pinref part="GND76" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND78" gate="1" pin="GND"/>
<pinref part="S7" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="C20" gate="G$1" pin="2"/>
<pinref part="GND79" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="JP16" gate="G$1" pin="1"/>
<pinref part="GND77" gate="1" pin="GND"/>
<wire x1="17.78" y1="152.4" x2="20.32" y2="152.4" width="0.1524" layer="91"/>
<wire x1="20.32" y1="152.4" x2="20.32" y2="142.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP20" gate="G$1" pin="1"/>
<pinref part="GND83" gate="1" pin="GND"/>
<wire x1="109.22" y1="175.26" x2="111.76" y2="175.26" width="0.1524" layer="91"/>
<wire x1="111.76" y1="175.26" x2="111.76" y2="157.48" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND80" gate="1" pin="GND"/>
<wire x1="134.62" y1="157.48" x2="134.62" y2="172.72" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="GND"/>
<wire x1="134.62" y1="172.72" x2="137.16" y2="172.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C25" gate="G$1" pin="2"/>
<pinref part="GND82" gate="1" pin="GND"/>
<wire x1="160.02" y1="157.48" x2="160.02" y2="160.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP17" gate="G$1" pin="1"/>
<pinref part="GND84" gate="1" pin="GND"/>
<wire x1="17.78" y1="86.36" x2="20.32" y2="86.36" width="0.1524" layer="91"/>
<wire x1="20.32" y1="86.36" x2="20.32" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="VR1" gate="G$1" pin="1"/>
<pinref part="GND85" gate="1" pin="GND"/>
<wire x1="50.8" y1="68.58" x2="50.8" y2="73.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C18" gate="G$1" pin="2"/>
<pinref part="GND86" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C21" gate="G$1" pin="1"/>
<pinref part="GND87" gate="1" pin="GND"/>
<wire x1="60.96" y1="86.36" x2="63.5" y2="86.36" width="0.1524" layer="91"/>
<wire x1="63.5" y1="86.36" x2="63.5" y2="83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP18" gate="G$1" pin="1"/>
<pinref part="GND88" gate="1" pin="GND"/>
<wire x1="17.78" y1="27.94" x2="20.32" y2="27.94" width="0.1524" layer="91"/>
<wire x1="20.32" y1="27.94" x2="20.32" y2="17.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="DS38" gate="G$1" pin="C"/>
<pinref part="GND89" gate="1" pin="GND"/>
<wire x1="55.88" y1="33.02" x2="60.96" y2="33.02" width="0.1524" layer="91"/>
<wire x1="60.96" y1="33.02" x2="60.96" y2="20.32" width="0.1524" layer="91"/>
<pinref part="DS39" gate="G$1" pin="C"/>
<wire x1="60.96" y1="20.32" x2="60.96" y2="17.78" width="0.1524" layer="91"/>
<wire x1="55.88" y1="20.32" x2="60.96" y2="20.32" width="0.1524" layer="91"/>
<junction x="60.96" y="20.32"/>
<pinref part="DS37" gate="G$1" pin="C"/>
<wire x1="55.88" y1="45.72" x2="60.96" y2="45.72" width="0.1524" layer="91"/>
<wire x1="60.96" y1="45.72" x2="60.96" y2="33.02" width="0.1524" layer="91"/>
<junction x="60.96" y="33.02"/>
</segment>
<segment>
<pinref part="GND90" gate="1" pin="GND"/>
<wire x1="167.64" y1="157.48" x2="167.64" y2="172.72" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="GND"/>
<wire x1="167.64" y1="172.72" x2="170.18" y2="172.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C28" gate="G$1" pin="2"/>
<pinref part="GND91" gate="1" pin="GND"/>
<wire x1="193.04" y1="157.48" x2="193.04" y2="160.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND92" gate="1" pin="GND"/>
<wire x1="200.66" y1="157.48" x2="200.66" y2="172.72" width="0.1524" layer="91"/>
<pinref part="U9" gate="G$1" pin="GND"/>
<wire x1="200.66" y1="172.72" x2="203.2" y2="172.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C30" gate="G$1" pin="2"/>
<pinref part="GND93" gate="1" pin="GND"/>
<wire x1="226.06" y1="157.48" x2="226.06" y2="160.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP21" gate="G$1" pin="1"/>
<pinref part="GND94" gate="1" pin="GND"/>
<wire x1="111.76" y1="124.46" x2="114.3" y2="124.46" width="0.1524" layer="91"/>
<wire x1="114.3" y1="124.46" x2="114.3" y2="111.76" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="MK1" gate="G$1" pin="-"/>
<pinref part="GND95" gate="1" pin="GND"/>
<wire x1="226.06" y1="114.3" x2="223.52" y2="114.3" width="0.1524" layer="91"/>
<wire x1="223.52" y1="114.3" x2="223.52" y2="111.76" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="CATHODE"/>
<pinref part="GND101" gate="1" pin="GND"/>
<wire x1="167.64" y1="33.02" x2="167.64" y2="38.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="V-"/>
<pinref part="GND103" gate="1" pin="GND"/>
<wire x1="144.78" y1="40.64" x2="149.86" y2="40.64" width="0.1524" layer="91"/>
<wire x1="149.86" y1="40.64" x2="149.86" y2="33.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="VR2" gate="G$1" pin="1"/>
<pinref part="GND104" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C23" gate="G$1" pin="2"/>
<pinref part="GND105" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C22" gate="G$1" pin="2"/>
<pinref part="GND106" gate="1" pin="GND"/>
<wire x1="119.38" y1="25.4" x2="119.38" y2="27.94" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP19" gate="G$1" pin="1"/>
<pinref part="GND107" gate="1" pin="GND"/>
<wire x1="88.9" y1="40.64" x2="91.44" y2="40.64" width="0.1524" layer="91"/>
<wire x1="91.44" y1="40.64" x2="91.44" y2="25.4" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="DS40" gate="G$1" pin="C"/>
<pinref part="GND108" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="EMITTER"/>
<pinref part="GND102" gate="1" pin="GND"/>
<wire x1="160.02" y1="38.1" x2="160.02" y2="33.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND44" gate="1" pin="GND"/>
<pinref part="C31" gate="G$1" pin="2"/>
<wire x1="251.46" y1="76.2" x2="254" y2="76.2" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q23" gate="G$1" pin="S"/>
<wire x1="264.16" y1="55.88" x2="264.16" y2="60.96" width="0.1524" layer="91"/>
<pinref part="GND45" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND47" gate="1" pin="GND"/>
<wire x1="246.38" y1="35.56" x2="246.38" y2="38.1" width="0.1524" layer="91"/>
<pinref part="JP23" gate="G$1" pin="S"/>
<wire x1="246.38" y1="38.1" x2="248.92" y2="38.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D12" gate="G$1" pin="A"/>
<pinref part="GND72" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R93" gate="G$1" pin="2"/>
<pinref part="GND73" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="JP22" gate="G$1" pin="1"/>
<pinref part="GND117" gate="1" pin="GND"/>
<wire x1="203.2" y1="53.34" x2="205.74" y2="53.34" width="0.1524" layer="91"/>
<wire x1="205.74" y1="53.34" x2="205.74" y2="40.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U10" gate="G$1" pin="GND"/>
<pinref part="GND81" gate="1" pin="GND"/>
<wire x1="236.22" y1="172.72" x2="233.68" y2="172.72" width="0.1524" layer="91"/>
<wire x1="233.68" y1="172.72" x2="233.68" y2="157.48" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C32" gate="G$1" pin="2"/>
<pinref part="GND118" gate="1" pin="GND"/>
<wire x1="259.08" y1="157.48" x2="259.08" y2="160.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C24" gate="G$1" pin="2"/>
<pinref part="GND122" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R84" gate="G$1" pin="1"/>
<pinref part="GND121" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C26" gate="G$1" pin="2"/>
<pinref part="GND97" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U8" gate="G$1" pin="V-"/>
<pinref part="GND96" gate="1" pin="GND"/>
<wire x1="190.5" y1="116.84" x2="198.12" y2="116.84" width="0.1524" layer="91"/>
<wire x1="198.12" y1="116.84" x2="198.12" y2="114.3" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="IN+"/>
<wire x1="198.12" y1="114.3" x2="198.12" y2="111.76" width="0.1524" layer="91"/>
<wire x1="190.5" y1="114.3" x2="198.12" y2="114.3" width="0.1524" layer="91"/>
<junction x="198.12" y="114.3"/>
</segment>
<segment>
<pinref part="R82" gate="G$1" pin="1"/>
<pinref part="GND124" gate="1" pin="GND"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<pinref part="R78" gate="G$1" pin="1"/>
<pinref part="S6" gate="G$1" pin="2"/>
<pinref part="R76" gate="G$1" pin="2"/>
<wire x1="53.34" y1="165.1" x2="58.42" y2="165.1" width="0.1524" layer="91"/>
<junction x="58.42" y="165.1"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<pinref part="R79" gate="G$1" pin="1"/>
<pinref part="S7" gate="G$1" pin="2"/>
<pinref part="R77" gate="G$1" pin="2"/>
<wire x1="53.34" y1="127" x2="58.42" y2="127" width="0.1524" layer="91"/>
<junction x="58.42" y="127"/>
</segment>
</net>
<net name="BTNA" class="0">
<segment>
<pinref part="R77" gate="G$1" pin="1"/>
<wire x1="30.48" y1="127" x2="38.1" y2="127" width="0.1524" layer="91"/>
<pinref part="C20" gate="G$1" pin="1"/>
<wire x1="38.1" y1="127" x2="43.18" y2="127" width="0.1524" layer="91"/>
<wire x1="38.1" y1="124.46" x2="38.1" y2="127" width="0.1524" layer="91"/>
<junction x="38.1" y="127"/>
<pinref part="JP16" gate="G$1" pin="4"/>
<wire x1="17.78" y1="144.78" x2="30.48" y2="144.78" width="0.1524" layer="91"/>
<wire x1="30.48" y1="144.78" x2="30.48" y2="127" width="0.1524" layer="91"/>
<label x="22.86" y="144.78" size="1.778" layer="95"/>
</segment>
</net>
<net name="BTNB" class="0">
<segment>
<wire x1="30.48" y1="147.32" x2="30.48" y2="165.1" width="0.1524" layer="91"/>
<pinref part="R76" gate="G$1" pin="1"/>
<wire x1="30.48" y1="165.1" x2="38.1" y2="165.1" width="0.1524" layer="91"/>
<pinref part="C19" gate="G$1" pin="1"/>
<wire x1="38.1" y1="165.1" x2="43.18" y2="165.1" width="0.1524" layer="91"/>
<wire x1="38.1" y1="162.56" x2="38.1" y2="165.1" width="0.1524" layer="91"/>
<junction x="38.1" y="165.1"/>
<pinref part="JP16" gate="G$1" pin="3"/>
<wire x1="17.78" y1="147.32" x2="30.48" y2="147.32" width="0.1524" layer="91"/>
<label x="22.86" y="147.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="P1" class="0">
<segment>
<pinref part="JP21" gate="G$1" pin="4"/>
<wire x1="111.76" y1="116.84" x2="127" y2="116.84" width="0.1524" layer="91"/>
<label x="119.38" y="116.84" size="1.778" layer="95"/>
<wire x1="147.32" y1="139.7" x2="137.16" y2="139.7" width="0.1524" layer="91"/>
<wire x1="137.16" y1="139.7" x2="127" y2="139.7" width="0.1524" layer="91"/>
<wire x1="127" y1="139.7" x2="127" y2="116.84" width="0.1524" layer="91"/>
<pinref part="R84" gate="G$1" pin="2"/>
<wire x1="137.16" y1="137.16" x2="137.16" y2="139.7" width="0.1524" layer="91"/>
<junction x="137.16" y="139.7"/>
<pinref part="C24" gate="G$1" pin="1"/>
<wire x1="147.32" y1="134.62" x2="147.32" y2="139.7" width="0.1524" layer="91"/>
<pinref part="D11" gate="G$1" pin="CATHODE"/>
<wire x1="149.86" y1="139.7" x2="147.32" y2="139.7" width="0.1524" layer="91"/>
<junction x="147.32" y="139.7"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<pinref part="VR1" gate="G$1" pin="2"/>
<wire x1="45.72" y1="78.74" x2="43.18" y2="78.74" width="0.1524" layer="91"/>
<pinref part="R72" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<pinref part="DS38" gate="G$1" pin="A"/>
<pinref part="R74" gate="G$1" pin="2"/>
<wire x1="48.26" y1="33.02" x2="45.72" y2="33.02" width="0.1524" layer="91"/>
<pinref part="TP50" gate="G$1" pin="P$1"/>
<junction x="45.72" y="33.02"/>
</segment>
</net>
<net name="N$47" class="0">
<segment>
<pinref part="DS39" gate="G$1" pin="A"/>
<pinref part="R75" gate="G$1" pin="2"/>
<wire x1="48.26" y1="20.32" x2="45.72" y2="20.32" width="0.1524" layer="91"/>
<pinref part="TP51" gate="G$1" pin="P$1"/>
<junction x="45.72" y="20.32"/>
</segment>
</net>
<net name="N$50" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="DOUT"/>
<wire x1="165.1" y1="172.72" x2="157.48" y2="172.72" width="0.1524" layer="91"/>
<wire x1="165.1" y1="172.72" x2="165.1" y2="167.64" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="DIN"/>
<wire x1="165.1" y1="167.64" x2="170.18" y2="167.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="DOUT"/>
<wire x1="190.5" y1="172.72" x2="198.12" y2="172.72" width="0.1524" layer="91"/>
<wire x1="198.12" y1="172.72" x2="198.12" y2="167.64" width="0.1524" layer="91"/>
<pinref part="U9" gate="G$1" pin="DIN"/>
<wire x1="198.12" y1="167.64" x2="203.2" y2="167.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$51" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="DOUT"/>
<wire x1="223.52" y1="172.72" x2="231.14" y2="172.72" width="0.1524" layer="91"/>
<wire x1="231.14" y1="172.72" x2="231.14" y2="167.64" width="0.1524" layer="91"/>
<pinref part="U10" gate="G$1" pin="DIN"/>
<wire x1="231.14" y1="167.64" x2="236.22" y2="167.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$52" class="0">
<segment>
<pinref part="MK1" gate="G$1" pin="+"/>
<pinref part="R92" gate="G$1" pin="1"/>
<wire x1="226.06" y1="119.38" x2="223.52" y2="119.38" width="0.1524" layer="91"/>
<wire x1="223.52" y1="119.38" x2="223.52" y2="121.92" width="0.1524" layer="91"/>
<pinref part="C29" gate="G$1" pin="1"/>
<wire x1="218.44" y1="119.38" x2="223.52" y2="119.38" width="0.1524" layer="91"/>
<junction x="223.52" y="119.38"/>
</segment>
</net>
<net name="N$53" class="0">
<segment>
<pinref part="C29" gate="G$1" pin="2"/>
<pinref part="R90" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$56" class="0">
<segment>
<pinref part="R89" gate="G$1" pin="1"/>
<pinref part="R87" gate="G$1" pin="1"/>
<wire x1="182.88" y1="53.34" x2="175.26" y2="53.34" width="0.1524" layer="91"/>
<pinref part="R86" gate="G$1" pin="1"/>
<pinref part="U6" gate="G$1" pin="ANODE"/>
<wire x1="175.26" y1="53.34" x2="167.64" y2="53.34" width="0.1524" layer="91"/>
<junction x="175.26" y="53.34"/>
<junction x="167.64" y="53.34"/>
</segment>
</net>
<net name="N$61" class="0">
<segment>
<pinref part="VR2" gate="G$1" pin="2"/>
<pinref part="C23" gate="G$1" pin="1"/>
<wire x1="139.7" y1="63.5" x2="147.32" y2="63.5" width="0.1524" layer="91"/>
<wire x1="147.32" y1="63.5" x2="147.32" y2="38.1" width="0.1524" layer="91"/>
<junction x="139.7" y="63.5"/>
<pinref part="U4" gate="G$1" pin="IN+"/>
<wire x1="147.32" y1="38.1" x2="144.78" y2="38.1" width="0.1524" layer="91"/>
<wire x1="134.62" y1="60.96" x2="134.62" y2="63.5" width="0.1524" layer="91"/>
<wire x1="134.62" y1="63.5" x2="139.7" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$55" class="0">
<segment>
<pinref part="R81" gate="G$1" pin="1"/>
<pinref part="DS40" gate="G$1" pin="A"/>
<pinref part="TP52" gate="G$1" pin="P$1"/>
<junction x="109.22" y="33.02"/>
<pinref part="TP52" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="P15" class="0">
<segment>
<pinref part="JP18" gate="G$1" pin="3"/>
<wire x1="17.78" y1="22.86" x2="33.02" y2="22.86" width="0.1524" layer="91"/>
<wire x1="33.02" y1="22.86" x2="33.02" y2="33.02" width="0.1524" layer="91"/>
<pinref part="R74" gate="G$1" pin="1"/>
<wire x1="33.02" y1="33.02" x2="35.56" y2="33.02" width="0.1524" layer="91"/>
<label x="22.86" y="22.86" size="1.778" layer="95"/>
</segment>
</net>
<net name="P14" class="0">
<segment>
<pinref part="JP18" gate="G$1" pin="4"/>
<wire x1="17.78" y1="20.32" x2="35.56" y2="20.32" width="0.1524" layer="91"/>
<pinref part="R75" gate="G$1" pin="1"/>
<label x="22.86" y="20.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="COLLECTOR"/>
<pinref part="R85" gate="G$1" pin="1"/>
<wire x1="160.02" y1="53.34" x2="149.86" y2="53.34" width="0.1524" layer="91"/>
<wire x1="149.86" y1="53.34" x2="149.86" y2="43.18" width="0.1524" layer="91"/>
<junction x="160.02" y="53.34"/>
<pinref part="U4" gate="G$1" pin="IN-"/>
<wire x1="149.86" y1="43.18" x2="144.78" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$60" class="0">
<segment>
<pinref part="VR2" gate="G$1" pin="3"/>
<pinref part="R83" gate="G$1" pin="1"/>
</segment>
</net>
<net name="P0" class="0">
<segment>
<label x="208.28" y="45.72" size="1.778" layer="95"/>
<pinref part="JP22" gate="G$1" pin="4"/>
<pinref part="R91" gate="G$1" pin="1"/>
<wire x1="203.2" y1="45.72" x2="213.36" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="LS1" gate="G$1" pin="-"/>
<wire x1="266.7" y1="71.12" x2="264.16" y2="71.12" width="0.1524" layer="91"/>
<pinref part="Q23" gate="G$1" pin="D"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="Q23" gate="G$1" pin="G"/>
<pinref part="R93" gate="G$1" pin="1"/>
<wire x1="256.54" y1="66.04" x2="246.38" y2="66.04" width="0.1524" layer="91"/>
<junction x="256.54" y="66.04"/>
<pinref part="D12" gate="G$1" pin="C"/>
<pinref part="S8" gate="G$1" pin="3"/>
<wire x1="238.76" y1="66.04" x2="246.38" y2="66.04" width="0.1524" layer="91"/>
<junction x="246.38" y="66.04"/>
</segment>
</net>
<net name="P8" class="0">
<segment>
<pinref part="JP19" gate="G$1" pin="4"/>
<label x="93.98" y="33.02" size="1.778" layer="95"/>
<pinref part="R80" gate="G$1" pin="1"/>
<wire x1="88.9" y1="33.02" x2="101.6" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$78" class="0">
<segment>
<pinref part="JP23" gate="G$1" pin="T"/>
<wire x1="246.38" y1="43.18" x2="248.92" y2="43.18" width="0.1524" layer="91"/>
<pinref part="JP23" gate="G$1" pin="R"/>
<wire x1="248.92" y1="40.64" x2="246.38" y2="40.64" width="0.1524" layer="91"/>
<wire x1="246.38" y1="40.64" x2="246.38" y2="43.18" width="0.1524" layer="91"/>
<junction x="246.38" y="40.64"/>
<pinref part="S8" gate="G$1" pin="1"/>
<wire x1="238.76" y1="40.64" x2="246.38" y2="40.64" width="0.1524" layer="91"/>
<wire x1="238.76" y1="60.96" x2="238.76" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$79" class="0">
<segment>
<pinref part="R91" gate="G$1" pin="2"/>
<wire x1="223.52" y1="45.72" x2="226.06" y2="45.72" width="0.1524" layer="91"/>
<wire x1="226.06" y1="45.72" x2="226.06" y2="63.5" width="0.1524" layer="91"/>
<pinref part="S8" gate="G$1" pin="2"/>
<wire x1="228.6" y1="63.5" x2="226.06" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<pinref part="R90" gate="G$1" pin="1"/>
<pinref part="U8" gate="G$1" pin="IN-"/>
<wire x1="200.66" y1="119.38" x2="198.12" y2="119.38" width="0.1524" layer="91"/>
<pinref part="C27" gate="G$1" pin="1"/>
<wire x1="198.12" y1="119.38" x2="190.5" y2="119.38" width="0.1524" layer="91"/>
<wire x1="185.42" y1="139.7" x2="198.12" y2="139.7" width="0.1524" layer="91"/>
<wire x1="198.12" y1="139.7" x2="198.12" y2="129.54" width="0.1524" layer="91"/>
<junction x="198.12" y="119.38"/>
<pinref part="R88" gate="G$1" pin="2"/>
<wire x1="198.12" y1="129.54" x2="198.12" y2="119.38" width="0.1524" layer="91"/>
<wire x1="187.96" y1="129.54" x2="198.12" y2="129.54" width="0.1524" layer="91"/>
<junction x="198.12" y="129.54"/>
</segment>
</net>
<net name="N$54" class="0">
<segment>
<pinref part="C27" gate="G$1" pin="2"/>
<wire x1="177.8" y1="139.7" x2="167.64" y2="139.7" width="0.1524" layer="91"/>
<wire x1="167.64" y1="139.7" x2="167.64" y2="137.16" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="OUT"/>
<wire x1="167.64" y1="137.16" x2="167.64" y2="129.54" width="0.1524" layer="91"/>
<wire x1="167.64" y1="129.54" x2="167.64" y2="119.38" width="0.1524" layer="91"/>
<wire x1="167.64" y1="119.38" x2="170.18" y2="119.38" width="0.1524" layer="91"/>
<pinref part="R88" gate="G$1" pin="1"/>
<wire x1="177.8" y1="129.54" x2="167.64" y2="129.54" width="0.1524" layer="91"/>
<junction x="167.64" y="129.54"/>
<pinref part="D11" gate="G$1" pin="ANODE"/>
<wire x1="167.64" y1="139.7" x2="165.1" y2="139.7" width="0.1524" layer="91"/>
<junction x="167.64" y="139.7"/>
<pinref part="D11" gate="G$1" pin="COMMON"/>
<wire x1="157.48" y1="139.7" x2="157.48" y2="137.16" width="0.1524" layer="91"/>
<wire x1="157.48" y1="137.16" x2="167.64" y2="137.16" width="0.1524" layer="91"/>
<junction x="167.64" y="137.16"/>
<junction x="157.48" y="139.7"/>
</segment>
</net>
<net name="P2" class="0">
<segment>
<pinref part="C18" gate="G$1" pin="1"/>
<wire x1="33.02" y1="78.74" x2="30.48" y2="78.74" width="0.1524" layer="91"/>
<wire x1="30.48" y1="78.74" x2="30.48" y2="76.2" width="0.1524" layer="91"/>
<pinref part="JP17" gate="G$1" pin="4"/>
<pinref part="R72" gate="G$1" pin="1"/>
<label x="22.86" y="78.74" size="1.778" layer="95"/>
<wire x1="17.78" y1="78.74" x2="30.48" y2="78.74" width="0.1524" layer="91"/>
<junction x="30.48" y="78.74"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="DS37" gate="G$1" pin="A"/>
<pinref part="R73" gate="G$1" pin="2"/>
<wire x1="48.26" y1="45.72" x2="45.72" y2="45.72" width="0.1524" layer="91"/>
<pinref part="TP49" gate="G$1" pin="P$1"/>
<junction x="45.72" y="45.72"/>
</segment>
</net>
<net name="P16" class="0">
<segment>
<pinref part="R73" gate="G$1" pin="1"/>
<wire x1="35.56" y1="45.72" x2="30.48" y2="45.72" width="0.1524" layer="91"/>
<wire x1="30.48" y1="45.72" x2="30.48" y2="25.4" width="0.1524" layer="91"/>
<pinref part="JP18" gate="G$1" pin="2"/>
<wire x1="30.48" y1="25.4" x2="17.78" y2="25.4" width="0.1524" layer="91"/>
<label x="22.86" y="25.4" size="1.778" layer="95"/>
</segment>
</net>
<net name="P13" class="0">
<segment>
<pinref part="JP20" gate="G$1" pin="4"/>
<pinref part="U5" gate="G$1" pin="DIN"/>
<wire x1="109.22" y1="167.64" x2="127" y2="167.64" width="0.1524" layer="91"/>
<label x="114.3" y="167.64" size="1.778" layer="95"/>
<pinref part="R82" gate="G$1" pin="2"/>
<wire x1="127" y1="167.64" x2="137.16" y2="167.64" width="0.1524" layer="91"/>
<junction x="127" y="167.64"/>
</segment>
</net>
<net name="N$85" class="0">
<segment>
<pinref part="R81" gate="G$1" pin="2"/>
<pinref part="U4" gate="G$1" pin="OUT"/>
<wire x1="124.46" y1="43.18" x2="109.22" y2="43.18" width="0.1524" layer="91"/>
<junction x="109.22" y="43.18"/>
<wire x1="101.6" y1="43.18" x2="109.22" y2="43.18" width="0.1524" layer="91"/>
<pinref part="R80" gate="G$1" pin="2"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
